UPDATE core_config_data SET value = 'https://local.chilecompra.com/' where path = 'web/secure/base_url';
UPDATE core_config_data SET value = 'https://local.chilecompra.com/' where path = 'web/unsecure/base_url';
UPDATE core_config_data SET value = 'https://local.chilecompra.com/' where path = 'web/unsecure/base_link_url';
UPDATE core_config_data SET value = 'https://local.chilecompra.com/' where path = 'web/secure/base_link_url';
UPDATE core_config_data SET value = REPLACE (value, 'conveniomarco.mercadopublico.cl', 'local.chilecompra.com') where value like "%conveniomarco.mercadopublico.cl%";
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/v1/usuario-tienda' WHERE path = 'dccp_login/endpoint_user_data/endpoint';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/v1/' WHERE path = 'dccp_endpoint/endpoint_status_change_data/endpoint';
UPDATE core_config_data SET value = 'd9d1f8e6dd31b236bcfd1c99a9c9a87b' WHERE path = 'dccp_endpoint/endpoint_status_change_data/token_mass_statuschange';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/v1/tienda/carga/proveedor-csv' WHERE path = 'dccp_endpoint/endpoint_status_change_data/endpoint_mass';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/api/v1/conveniomarco/' WHERE path = 'dccp_endpoint/endpoint_agreement_data/endpoint';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/api/v1/' WHERE path = 'dccp_endpoint/endpoint_warranty_data/endpoint';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/api/v1/' WHERE path = 'dccp_endpoint/endpoint_penalties_data/endpoint';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/api/v1/' WHERE path = 'dccp_endpoint/endpoint_penalties_delete/endpoint';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/v1/moneda' WHERE path = 'dccp_endpoint/greatbuy/currency_endpoint';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/v1/gran-compra/' WHERE path = 'dccp_endpoint/greatbuy/sendcart_endpoint';
UPDATE core_config_data SET value = '10' WHERE path = 'dccp_endpoint/greatbuy/minimumutm';
UPDATE core_config_data SET value = '1000' WHERE path = 'dccp_endpoint/greatbuy/utmforgb';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/api/v1/producto/{{id}}/precio-historico-pagado' WHERE path = 'dccp_endpoint/pricehistory/endpoint';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/v1/producto/oferta' WHERE path = 'dccp_endpoint/endpoint_assign_product/endpoint';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/v1/producto/oferta' WHERE path = 'dccp_endpoint/endpoint_assign_product_update/endpoint';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/v1/calendario/calculo/fecha-inicio/%s/cantidad-dias/%d' WHERE path = 'dccp_endpoint/endpoint_holidays/endpoint';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/v1/orden-de-compra' WHERE path = 'sales/erp_integration_section/generate_purchase_order_endpoint';
UPDATE core_config_data SET value = '10' WHERE path = 'dccp_endpoint/greatbuy/minimumutm';
UPDATE core_config_data SET value = '1000' WHERE path = 'dccp_endpoint/greatbuy/utmforgb';
UPDATE core_config_data SET value = 'https://servicios-dev.chilecompra.cl/v1/oferta/producto' WHERE path = 'dccp_endpoint/endpoint_assign_products_update/endpoint';
UPDATE core_config_data SET value=1 WHERE path LIKE '%system/full_page_cache/caching_application%'; /* Config for full page cache enable */
UPDATE `core_config_data` SET `value` = '0' WHERE `path` LIKE 'google/analytics/active'; /* Disabe Google Analytics */
UPDATE `customer_entity` SET `email` = CONCAT('test_',`email`) WHERE `email` NOT LIKE '%@summasolutions.net%'; /* Safe customer email*/
UPDATE `sales_order` SET `customer_email` = CONCAT('test_',`customer_email`) WHERE `customer_email` NOT LIKE '%@summasolutions.net%'; /* Safe customer Email Sales order*/
UPDATE  `core_config_data` SET  `value` = '99999' WHERE  `path` LIKE 'admin/security/session_lifetime'; /* Set new cookies lifetime for admin */
UPDATE core_config_data SET value=null WHERE path like '%cookie_domain%';

