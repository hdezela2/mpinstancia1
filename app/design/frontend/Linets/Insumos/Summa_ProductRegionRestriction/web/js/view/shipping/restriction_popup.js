define(
    [
        'uiComponent',
        'jquery',
        'ko',
        'Magento_Ui/js/modal/modal',
        'mage/url',
        'Magento_Checkout/js/model/quote'
    ],
    function(
        Component,
        $,
        ko,
        modal,
        urlBuilder,
        quote
    ) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Summa_ProductRegionRestriction/popup'
            },

            initialize: function () {
                var self = this;
                this._super();

                var options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: $.mage.__('¡Advertencia!'),
                    buttons: [{
                        text: $.mage.__('Volver'),
                        class: 'action-primary',
                        click: function () {
                            var url = urlBuilder.build("checkout/cart");
                            window.location.href = url;
                        }
                    }]
                };

                if (window.checkoutConfig.isProductRegionRestrictionEnabled) {
                    $(document).on('click', '#co-shipping-method-form button', function(e) {
                        let region_array = null;
                        let allSame = true;

                        window.checkoutConfig.quoteItemData.forEach(function(i) {
                            if (!i.region_array || !allSame) {
                                return;
                            }

                            if (!region_array) {
                                region_array = [];
                                region_array = i.region_array;
                                return;
                            }
                            // to compare both array
                            let region_comparation = region_array.every(function(v, j) { return v === i.region_array[j] } );

                            if (region_array.length!==i.region_array.length && !region_comparation) {
                                allSame = false;
                            }
                        });

                        var isRegionInArray = $.inArray(quote.shippingAddress().regionId, region_array);

                        if (!allSame) {
                            e.preventDefault();
                            e.stopPropagation();

                            $("#product-region-restriction-mixed").modal(options).modal("openModal");
                        } else if (region_array && isRegionInArray < 0) {
                            e.preventDefault();
                            e.stopPropagation();

                            $("#product-region-restriction-invalid-address").modal(options).modal("openModal");
                        }
                    });
                }
            },
        });
    }
);
