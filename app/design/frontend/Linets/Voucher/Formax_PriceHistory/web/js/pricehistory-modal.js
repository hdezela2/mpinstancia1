define([
    "jquery", "underscore", "Magento_Ui/js/modal/modal"
], function($, _){
    var PriceHistoryModal = {
        initHistoryModal: function(config, element) {
            $target = $('#pricehistory-popup');
            var $element = $('#viewPriceHistory');
            var options = {
              responsive: true,
              innerScroll: true,
              modalClass: 'pricehistory-modal',
              title: 'Historial de precios',
              closeText: 'Cerrar',
              buttons: []
            };
            $target.modal(options);

            $('#viewPriceHistory').click(function(){
              var prodId = null,
                  endpoint = config.endpoint,
                  swatchRenderer = $('[data-role=swatch-options]').data('mageSwatchRenderer');
              if(swatchRenderer){
                prodId = window.dccpProductIdFound;
              }else{
                prodId = config.dccp_productid;
              }

              var table = $target.find('table#prices-list'),
                  tbody = table.find('tbody'),
                  errText = 'Lo sentimos, hubo un problema al intentar obtener el historial de precios.',
                  errNFound = 'Este producto aún no cuenta con esta información.';

              if(prodId && endpoint != ''){
                $.ajax({
                  showLoader: true,
                  url: endpoint.replace('{{id}}',prodId),
                  method: 'GET',
                  dataType: 'json',
                  error: function(res){
                    let body = res.responseJSON;
                    let errMsg = errText;
                    if (Object.keys(res).length && body && body['errores'].length) {
                      body.errores.forEach(function(error) {
                        if (error.codigo == 404 || error.codigo == 400) {
                          errMsg = errNFound;
                        }
                      });
                    } else {
                      switch(res.status) {
                        case 500:
                          errMsg = 'Lo sentimos, hubo un problema al intentar obtener el historial.';
                          break;
                        default:
                          errMsg = 'Lo sentimos, hubo un problema al intentar obtener el historial.';
                          break;
                      }
                    }
                    table.addClass('no-records');
                    tbody.find('tr#no-records>td').text(errMsg)
                    $target.modal('openModal');
                  },
                  success: function(res){
                    table.removeClass('no-records');
                    tbody.find('tr:not(#no-records)').remove();
                    
                    res.payload.forEach(function(item){
                      var date = new Date(item.fecha);
                      var month = date.getUTCMonth() + 1,
                          day = date.getUTCDate(),
                          year = date.getUTCFullYear(),
                          newdate = year + "/" + month + "/" + day;

                      tbody.append('<tr><td>'+item.producto+'</td><td>'+item.ordeDeCompra+'</td><td>'+newdate+'</td><td>'+item.moneda+'</td><td>'+item.precio+'%</td></tr>');
                    });
                    $target.modal('openModal');
                  }
                });
              }else{
                table.addClass('no-records');
                tbody.find('tr#no-records>td').text(errText);
                $target.modal('openModal');
              }

            });
        }
    };
    return {
        'pricehistory-modal': PriceHistoryModal.initHistoryModal
    };
}
);
