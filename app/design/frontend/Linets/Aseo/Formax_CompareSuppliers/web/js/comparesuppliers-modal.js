define([
        'jquery',
        'Magento_Catalog/js/price-utils',
        'Magento_Ui/js/modal/modal',
        'mage/translate'
    ],
    function ($, priceUtils)
    {
        const CompareSuppliersModal =
            {
                initCompareModal: function (config, element)
                {
                    const inputName = 'input[name="suppliers_id_selected"]';
                    const inputAll = 'input[name="suppliers_id_select_all"]';

                    if (config.is_compare_enabled === '0') {
                        $(inputName).addClass('disabled');
                        $(inputName).attr('disabled', true);
                        $(inputAll).attr('disabled', true);
                        return;
                    }

                    const $target = $('#comparesuppliers-popup');
                    const $element = $('#viewSuppliersComparator');
                    const options =
                        {
                            responsive: true,
                            innerScroll: true,
                            modalClass: 'comparesuppliers-modal',
                            title: 'Comparador de proveedores',
                            closeText: 'Cerrar',
                            buttons: []
                        };
                    $target.modal(options);


                    $(document).on('change', inputName, function ()
                    {
                        if ($(inputName + ':checked').length > 1)
                        {
                            $element.removeClass('disabled');
                        }
                        else
                        {
                            $element.addClass('disabled');
                        }
                    });
                    $(inputName + ',' + inputAll).attr('disabled', false);
                    $element.on('click',function ()
                    {
                        $target.find('.product-info .product-sku').html($('.product-info-main .product.attribute.sku').clone());
                        $target.find('.product-info .product-name').html($('.product-info-main .page-title-wrapper.product').clone());
                        $target.find('.product-photo').html($('.product.media .fotorama__stage .fotorama__img').clone().first());
                        let ids = [];
                        $(inputName + ':checked').each(function ()
                        {
                            ids.push($(this).val());
                        });
                        if (ids.length)
                        {
                            $.ajax
                            ({
                                url: config.url,
                                showLoader: true,
                                type: 'POST',
                                dataType: 'json',
                                data: {supplierIds: ids, regionId: $('#cc-supplier_filter-by-region').val()}
                            })
                                .done(function (data)
                                {
                                    $('.suppliers-comparisontable>thead, .suppliers-comparisontable>tbody').empty();
                                    $('.suppliers-comparisontable>thead').append('<tr><th class="invisible">&nbsp;</th></tr>');
                                    data.suppliers.forEach(function (supplier, i)
                                    {
                                        const list = window.supplierFilters.list;
                                        var name = "";
                                        const _supplier = list.filter(function (obj)
                                        {
                                            return obj.id === supplier.id;
                                        });
                                            if(!supplier.abtester){
                                                if(supplier.sellerAlias){
                                                    name = 'Proveedor N� ' + supplier.sellerAlias;
                                                } else {
                                                    name = 'Proveedor Oculto';
                                                }
                                            } else {
                                                name = _supplier[0].name;
                                            }

                                            $('.suppliers-comparisontable>thead>tr').append('<th>' + name + '</th>');
                                        let index = 0;
                                        if (i === 0)
                                        {
                                            const tbody = $('.suppliers-comparisontable>tbody');
                                            $('.suppliers-comparisontable>tbody').append('<tr id="comparison-rut"> <th>' + $.mage.__('RUT') + '</th></tr>');
                                            $('.suppliers-comparisontable>tbody').append('<tr id="comparison-business-name"> <th>' + $.mage.__('Business Name') + '</th></tr>');
                                            tbody.append('<tr id="comparison-price"> <th>' + $.mage.__('Net price') + '</th></tr>');
                                        }
                                        $('.suppliers-comparisontable #comparison-rut').append('<td>' + supplier.wkv_dccp_rut + '</td>');
                                        $('.suppliers-comparisontable #comparison-business-name').append('<td>' + supplier.wkv_dccp_business_name + '</td>');
                                        let styleSpecialPrice = "font-size: 12px; " +
                                            "margin-left: 5px; " +
                                            "text-decoration: line-through; " +
                                            "color: #8798AD"
                                        if (_supplier[0].oldPrice !== null) {
                                            $('.suppliers-comparisontable #comparison-price').append('<td><span' +
                                                ' class="price" style="color: #D63649;">' + formatPrice(_supplier[0].price) + '</span><span class="price" style="' + styleSpecialPrice + '">' + formatPrice(_supplier[0].oldPrice) + '</span></td>');
                                        } else {
                                            $('.suppliers-comparisontable #comparison-price').append('<td><span class="price">' + formatPrice(_supplier[0].price) + '</span></td>');
                                        }

                                        supplier.regional_conditions.forEach(function (condition)
                                        {
                                            const conditionName = condition.name.toLowerCase().replace(/ /g, '-');
                                            condition.values.forEach(function (weight)
                                            {
                                                $('.suppliers-comparisontable #comparison-' + conditionName + '>#row-' + conditionName + '-' + i).append('<br>' + formatPrice(weight.value));
                                            });
                                        });
                                        for (attr in supplier.attrs)
                                        {
                                            if (i === 0)
                                            {
                                                $('.suppliers-comparisontable>tbody').append('<tr id="comparison-row-' + index + '">' + '<th>' + supplier.attrs[attr].label + '</th></tr>');
                                            }
                                            if(attr == 'legal_rep')
                                            {
                                                const value = supplier.abtester == false ? 'Representante Oculto' : supplier.attrs[attr].value;
                                                $('.suppliers-comparisontable #comparison-row-' + index).append('<td>' + value + '</td>');
                                            }
                                            else
                                            {
                                                $('.suppliers-comparisontable #comparison-row-' + index).append('<td>' + supplier.attrs[attr].value + '</td>');
                                            }
                                            index++;
                                        }
                                    });
                                })
                                .error(function (err)
                                {
                                    console.log('error', err);
                                });
                        }
                        $target.modal('openModal');
                    });

                    $(document).on('click', '.print-suppliercomparison', function ()
                    {
                        const suppWin = window.open('', 'PRINT', 'height=' + $(window).height() + ',width=' + $(window).width());
                        suppWin.document.write('<html><head><title>' + document.title + '</title>');
                        suppWin.document.write('<link rel="stylesheet" href="' + $('link[href*="dccp.css"]').attr('href') + '" type="text/css" />');
                        suppWin.document.write('</head><body>');
                        suppWin.document.write('<h1>' + document.title + '</h1>');
                        suppWin.document.write($('#comparesuppliers-popup').html());
                        suppWin.document.write('</body></html>');
                        suppWin.document.close();
                        suppWin.focus();
                        suppWin.print();
                        return true;
                    })
                },
            };

        /**
         * Format Price
         * @param price
         * @returns {string}
         */
        function formatPrice(price)
        {
            const element = $('div#product_list');
            const priceFormat =
                {
                    decimalSymbol: element.attr('data-decimal'),
                    groupLength: 3,
                    groupSymbol: element.attr('data-group'),
                    integerRequired: false,
                    pattern: element.attr('data-symbol') + '%s',
                    precision: element.attr('data-precision'),
                    requiredPrecision: element.attr('data-precision')
                };
            return priceUtils.formatPrice(price, priceFormat);
        }

        return {'comparesuppliers-modal': CompareSuppliersModal.initCompareModal};
    }
);
