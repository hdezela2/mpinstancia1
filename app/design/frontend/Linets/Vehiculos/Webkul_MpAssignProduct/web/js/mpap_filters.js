require([
    'jquery',
    'Magento_Catalog/js/price-utils',
    'mage/translate'
], function (
    $,
    priceUtils
) {
    $(function () {
        window.supplierFilters = {};
        window.supplierFilters.applied = {};
        const supplierTr = $('#suppliers_box .wk-table-product-list>tbody>tr:not(#no-suppliers)');
        const afterLabel = $('.product-info-price-wk .after-label');
        afterLabel.data('default', afterLabel.text());

        $(document).on('WK_AssignProduct_PricesChanged', function () {
            const supplierList = [];
            const suppStamps = window.supplierStamps;
            const conditions = window.supplierConditions;
            supplierTr.each(function () {
                const _sellerId = $(this).data('sellerid');
                const stampsArr = suppStamps && suppStamps[_sellerId];
                const conditionsArr = conditions && conditions[_sellerId];
                let _price = $(this).find('td.wk-ap-price'),
                    _priceN = _price.attr('data-base');
                _oldPriceN = null;
                if (_price.attr('data-special').length) {
                    _priceN = _price.attr('data-special');
                    _oldPriceN = _price.attr('data-base');
                }
                const obj = {
                    id: _sellerId,
                    baseid: $(this).data('id'),
                    name: $(this).find('td[data-name]').data('name'),
                    price: _priceN,
                    oldPrice: _oldPriceN ? _oldPriceN : null,
                    stamps: stampsArr ? suppStamps[_sellerId] : [],
                    conditions: conditionsArr ? conditions[_sellerId] : [],
                    hasQty: Number($(this).data('wkhqt'))
                };
                supplierList.push(obj);
            });
            supplierList.sort(orderByPrice);
            window.supplierFilters.list = supplierList;
            initStampsFilter();
            initRegionFilter();
            initPriceSlider();
            initDDaysFilter();
        });
        $(document).trigger('WK_AssignProduct_PricesChanged');

        /**
         * Init filters
         */
        $('.supplier-filters #cc-suppliers_filter-by-days').on('input', function () {
            const inpVal = $(this).val();
            $(this).closest('.filter-option').find('#days-rangeSelected').text(inpVal);
            if (inpVal !== '' && inpVal > 0) {
                window.supplierFilters.applied['deliveryDays'] = inpVal;
            } else {
                delete window.supplierFilters.applied['deliveryDays'];
            }
            $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered');
        });
        const supplierFilters = $('.supplier-filters');
        supplierFilters.on('change', 'input[name="stamps-filter"]', function () {
            const selectedStamps = [];
            $.each($('.supplier-filters input[name="stamps-filter"]:checked'), function () {
                selectedStamps.push($(this).val());
            });
            if (selectedStamps.length) {
                window.supplierFilters.applied['stamps'] = selectedStamps;
            } else {
                delete window.supplierFilters.applied['stamps'];
            }
            $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered');
        });

        /**
         * Filter by price
         */
        $('.supplier-filters #cc-suppliers_filter-by-price').on('input', function () {
            const inpVal = $(this).val();
            $(this).closest('.filter-option').find('#price-rangeSelected').text(formatPrice(inpVal));
            if (inpVal > 0) {
                window.supplierFilters.applied['price'] = inpVal;
            }
            $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered');
        });

        /**
         * Filter by name
         */
        $('.supplier-filters #cc-suppliers_filter-by-name').on('input', function () {
            const inpVal = $(this).val();
            if (inpVal !== '') {
                window.supplierFilters.applied['name'] = inpVal;
            } else {
                delete window.supplierFilters.applied['name'];
            }
            $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered');
        });

        /**
         * Filter by region
         */
        supplierFilters.on('change', '#cc-supplier_filter-by-region', function () {
            const inpVal = $(this).val();
            if (inpVal !== '') {
                window.supplierFilters.applied['region'] = inpVal;
                jQuery("#cc-suppliers_filter-by-days").removeAttr('disabled');
                jQuery("#cc-suppliers_filter-by-days").removeClass('disabled-range');
            } else {
                jQuery("#cc-suppliers_filter-by-days").attr('disabled', 'disabled');
                jQuery("#cc-suppliers_filter-by-days").addClass('disabled-range');
                delete window.supplierFilters.applied['region'];
                delete window.supplierFilters.applied['deliveryDays'];
                initDDaysFilter();
            }
            $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered');
        });

        /**
         * Filter Functionality
         * @type {*|jQuery|HTMLElement}
         */
        const supplierBox = $('#suppliers_box .wk-table-product-list');
        supplierBox.on('CC_UpdateListFiltered', function (e, isInitial) {
            const trDisplay = $(window).width() > 767 ? 'table-row' : 'inline-block';
            const list = window.supplierFilters.list;
            const applied = window.supplierFilters.applied;
            if (list !== undefined) {
                const wFilters = list.filter(function (item) {
                    delete item['currCondition'];
                    if (!item.hasQty) {
                        return false;
                    }
                    for (const key in applied) {
                        if (key === 'name') {
                            if (!(item[key].toLowerCase().indexOf(applied[key].toLowerCase()) !== -1))
                                return false;
                        } else if (key === 'price') {
                            if (Number(item[key]) > applied[key])
                                return false;
                        } else if (key === 'deliveryDays') {
                            let inDays = false;
                            // item['conditions'].forEach(function (condition) {
                            //     if (applied[key] >= condition.days_sp) {
                            //         inDays = true;
                            //     }
                            // });
                            if ("currCondition" in item) {

                                if (parseInt(applied[key]) >= parseInt(item.currCondition.days_sp)) {
                                    inDays = true;
                                    console.log(item);
                                }
                            }
                            if (!inDays) {
                                return inDays;
                            }
                        } else if (key === 'baseid') {
                            if (!applied[key].includes(item[key]))
                                return false;
                        } else if (key === 'region') {
                            let inRegion = false;
                            item['conditions'].forEach(function (condition) {
                                if (applied[key] === condition.region_id) {
                                    item['currCondition'] = condition;
                                    inRegion = true;
                                }
                            });
                            if (!inRegion) {
                                return inRegion;
                            }
                        } else if (key === 'stamps') {
                            let exists = false;
                            item[key].forEach(function (stamp) {
                                if (applied[key].includes(stamp.id)) {
                                    exists = true;
                                }
                            });
                            if (!exists) {
                                return exists;
                            }
                        } else {
                            if (item[key] === undefined || item[key] !== applied[key])
                                return false;
                        }
                    }
                    return true;
                });
                if (applied['baseid'] && applied['baseid'].length) {
                    $('#suppliers_box .wk-table-product-list tbody>tr#no-suppliers>td').text($.mage.__("There are no suppliers with the filters you have selected"));
                }
                if (wFilters.length) {
                    $('#suppliers_box .wk-table-product-list tbody>tr').hide();
                    var isAllRegion = 0;
                    if (window.productRegion) {
                        if (window.productRegion == 'all_region') {
                            isAllRegion = 1;
                        }
                    }
                    wFilters.forEach(function (row) {
                        const _row = $('#suppliers_box .wk-table-product-list tbody>tr[data-sellerid="' + row.id + '"]');
                        let hassp = 0,
                            bdays = false,
                            bdaysT = 'Seleccione una región',
                            found = false;
                        if (row.currCondition) {
                            found = true;
                            hassp = Number(row.currCondition.has_storepickup);
                            bdays = Number(row.currCondition.days_sp);
                            bdaysT = bdays + ' día' + (bdays > 1 ? 's' : '') + ' hábil' + (bdays > 1 ? 'es' : '');
                        }
                        if (isAllRegion) {
                            found = true;
                            hassp = true;
                            bdays = Number(row.currCondition.days_sp);
                            bdaysT = 'Máximo ' + bdays + ' día' + (bdays > 1 ? 's' : '') + ' hábil' + (bdays > 1 ? 'es' : '');
                        }
                        _row.find('.wk-ap-delivery-days').data('deliverydays', bdays);
                        _row.find('.wk-ap-delivery-days > .bdays').text(bdaysT);
                        _row.find('.wk-ap-delivery-days > .hassp').css('display', found ? 'block' : 'none');
                        if (hassp && found) {
                            _row.find('.wk-ap-delivery-days .hassp i').removeClass('ccicon-close');
                        } else {
                            _row.find('.wk-ap-delivery-days .hassp i').addClass('ccicon-close');
                        }
                        _row.css('display', trDisplay);
                    });
                    $('input[name="suppliers_id_select_all"]').prop('disabled', false);
                } else {
                    $('input[name="suppliers_id_select_all"]').prop('disabled', true);
                    $('#suppliers_box .wk-table-product-list tbody>tr').hide();
                    $('#suppliers_box .wk-table-product-list tbody>tr#no-suppliers').css('display', trDisplay);
                }
                $('input[name="suppliers_id_selected"], input[name="suppliers_id_select_all"]').prop('checked', false).trigger('change');
                if (!isInitial) {
                    updatePriceBox(wFilters);
                }
            }
        });
        supplierBox.trigger('CC_UpdateListFiltered', [true]);

        $(document).on('change', 'input[name="suppliers_id_select_all"]', function () {
            const input = $('#suppliers_box .wk-table-product-list tbody>tr:visible input[name="suppliers_id_selected"]');
            if ($(this).prop('checked')) {
                input.prop('checked', true).trigger('change');
            } else {
                input.prop('checked', false).trigger('change');
            }
        });

        /**
         * Order By Price
         * @param a
         * @param b
         * @returns {number}
         */
        function orderByPrice(a, b) {
            if (parseFloat(a.price) > parseFloat(b.price)) return 1;
            if (parseFloat(a.price) < parseFloat(b.price)) return -1;
            return 0;
        }

        /**
         * Update Price Box
         * @param list
         */
        function updatePriceBox(list) {
            if (list.length) {
                afterLabel.text(afterLabel.data('default'));
                $('.product-info-price-wk .minimum-price').show();
                $('.product-info-price-wk .minimum-price .price').text(formatPrice(list[0].price));
                if (list.length > 1) {
                    if (list[0].price !== list[list.length - 1].price) {
                        $('.product-info-price-wk .maximum-price').show();
                        $('.product-info-price-wk .maximum-price .price').text(formatPrice(list[list.length - 1].price));
                    } else {
                        $('.product-info-price-wk .maximum-price').hide();
                    }
                } else {
                    $('.product-info-price-wk .maximum-price').hide();
                }
            } else {
                $('.product-info-price-wk .maximum-price').hide();
                $('.product-info-price-wk .minimum-price').hide();
                afterLabel.text($.mage.__('Product not available'));
            }
        }

        /**
         * Format Price
         * @param price
         * @returns {string}
         */
        function formatPrice(price) {
            const element = $('div#product_list');
            const priceFormat = {
                decimalSymbol: element.attr('data-decimal'),
                groupLength: 3,
                groupSymbol: element.attr('data-group'),
                integerRequired: false,
                pattern: element.attr('data-symbol') + '%s',
                precision: element.attr('data-precision'),
                requiredPrecision: element.attr('data-precision')
            };
            return priceUtils.formatPrice(price, priceFormat);
        }

        /**
         * Init Stamps Filter
         */
        function initStampsFilter() {
            const list = window.supplierFilters.list;
            list.forEach(function (supplier) {
                if (supplier.stamps.length) {
                    $('.cc-stamps-filter').show();
                }
                supplier.stamps.forEach(function (stamp) {
                    if (!$('.cc-stamps-filter .stamps-box #stamps-filter-' + stamp.id).length) {
                        const input = '<input type="checkbox" name="stamps-filter" id="stamps-filter-' + stamp.id + '" value="' + stamp.id + '">',
                            span = '<span>' + stamp.name + '</span>',
                            label = '<label for="stamps-filter-' + stamp.id + '">' + input + span + '</label>';
                        $('.cc-stamps-filter .stamps-box').append(label);
                    }
                });
            });
        }

        /**
         * Init Region Filter
         */
        function initRegionFilter() {
            if ($('.cc-region-filter #cc-supplier_filter-by-region option').length > 1) {
                return;
            }
            const regions = window.chileanRegions;
            const select = $('.cc-region-filter #cc-supplier_filter-by-region');
            const defaultRegion = '11';
            if (Array.isArray(regions)) {
                select.html('');
                const optionBase = '<option value="" selected>---</option>';
                select.append(optionBase);
                regions.forEach(function (region) {
                    const option = '<option value="' + region.id + '">' + region.name + '</option>';
                    select.append(option);
                });
                if (window.productRegion) {
                    select.val(window.productRegion).change();
                    if (window.productRegion == 'all_region') {
                        select.val(defaultRegion).change();
                    }

                }
            }
        }

        /**
         * Init Days Filter
         */
        function initDDaysFilter() {
            let min = 1, max = 1;
            const list = window.supplierFilters.list;
            list.forEach(function (supplier) {
                if (supplier.conditions.length) {
                    $('.supplier-filters .cc-deliverydays-filter').show();
                }
                supplier.conditions.forEach(function (condition) {
                    const daysSp = Number(condition.days_sp);
                    if (daysSp < min) {
                        min = daysSp;
                    }
                    if (daysSp > max) {
                        max = daysSp;
                    }
                });
            });
            const inp = $('.supplier-filters .cc-deliverydays-filter input[type="range"]');
            $('.supplier-filters .cc-deliverydays-filter .range-min').text(min);
            $('.supplier-filters .cc-deliverydays-filter .range-max').text(max);
            inp.attr('min', min);
            inp.attr('max', max);
            inp.val(max);
            $('.supplier-filters .cc-deliverydays-filter #days-rangeSelected').text(max);
        }

        /**
         * Init Price Slider
         */
        function initPriceSlider() {
            let min, max;
            const list = window.supplierFilters.list;
            if (list.length) {
                min = list[0].price;
                max = list[0].price;
            }
            list.forEach(function (product) {
                if (product.hasQty) {
                    if (parseFloat(product.price) < parseFloat(min)) {
                        min = product.price;
                    }
                    if (parseFloat(product.price) > parseFloat(max)) {
                        max = product.price;
                    }
                }
            });
            const inp = $('.supplier-filters .cc-price-filter input[type="range"]');
            $('.supplier-filters .cc-price-filter .range-min').text(formatPrice(min));
            $('.supplier-filters .cc-price-filter .range-max').text(formatPrice(max));
            inp.attr('min', Number(min));
            inp.attr('max', Number(max));
            inp.val(Number(max));
            $('.supplier-filters .cc-price-filter #price-rangeSelected').text(formatPrice(max));
        }
    });
});
