/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
  'jquery',
  'Magento_Checkout/js/model/totals',
  'Magento_Ui/js/modal/modal'
], function (
  $,
  totals
) {
  'use strict';

  return {

      config: {},

      /**
       * Initialize view.
       *
       * @param {object} config 
       * @return {exports}
       */
      initialize: function (config) {
          this.config = config;
          return this;
      },

      /**
       * Initializes great buy and lower than X UTM modals.
       */
      createModal: function(){
        var self = this;
          var $targetGB = $(this.config.modalContainerA);
          var $targetLOW = $(this.config.modalContainerB);
          var $targetLIMIT = $(this.config.modalContainerC);

          var optionsA = {
            responsive: true,
            innerScroll: true,
            modalClass: 'greatbuy-modal',
            title: $.mage.__('Great Buy'),
            closeText: 'Cerrar',
            buttons: [
              {
                text: $.mage.__('Continue purchase order'),
                class: '',
                click: function(e){
                  self.hideGBModal();
                  window.dccpGBCallback();
                }
              },
              {
                text: $.mage.__('Go to Great Buy'),
                class: 'action-primary',
                click: function(e){
                  var lSItems = self.getUtils();
                  $.ajax({
                    showLoader: true,
                    url: self.config.send_url,
                    data: {
                      sellerId: lSItems.sellerId,
                      purchaseUnit: lSItems.purchaseUnit
                    },
                    method: 'POST',
                    dataType: 'json',
                    error: function(res){
                      self.hideGBModal();
                      window.dccpGBCallback();
                    },
                    success: function(res){
                      if(res.redirectUri !== ''){
                        window.location.href = res.redirectUri;
                      }
                    }
                  });
                }
              }
            ]
          };
          var optionsB = {
            responsive: true,
            innerScroll: true,
            modalClass: 'lowerutm-modal',
            title: 'Lo sentimos.',
            closeText: 'Cerrar',
            buttons: [
              {
                text: $.mage.__('Ok'),
                class: 'action-primary',
                click: function(e){
                  $targetLOW.modal('closeModal');
                }
              }
            ]
          };
          var optionsC = {
              responsive: true,
              innerScroll: true,
              modalClass: 'higerutm-modal',
              title: 'Lo sentimos.',
              closeText: 'Cerrar',
              buttons: [
                  {
                      text: 'Volver',
                      class: 'action-primary',
                      click: function(e){
                          $targetLIMIT.modal('closeModal');
                          window.location.href =  self.config.cart_url;
                      }
                  }
              ]
          };
          $targetGB.modal(optionsA);
          $targetLOW.modal(optionsB);
          $targetLIMIT.modal(optionsC);

      },

      /**
       * Triggers open Great Buy modal
       */
      openGBModal: function(){
        $(this.config.modalContainerA).modal('openModal');
      },

      /**
       * Triggers open Lower than X modal
       */
      openLOWModal: function(){
        $(this.config.modalContainerB).modal('openModal');
      },

      /**
       * Triggers close Great Buy modal
       */
      hideGBModal: function(){
        $(this.config.modalContainerA).modal('closeModal');
      },

      /**
       * Triggers close Lower than X modal
       */
      hideLOWModal: function(){
        $(this.config.modalContainerB).modal('closeModal');
      },

      /**
       * Triggers open Great Buy Limit higer  modal
       */
      openHigerLimitModal: function(){
          $(this.config.modalContainerC).modal('openModal');
      },

      /**
       * Triggers close Great Buy Limit higer modal
       */
      hideHigerLimitModal: function(){
          $(this.config.modalContainerC).modal('closeModal');

      },

      /**
       * Show error message.
       */
      showErrorMsg: function(){
        var msg = this.config.error_msg,
            gbdiv = $('#greatbuy-msg');
        if(!gbdiv.length){
          $('.page.messages').append('<div class="message-error error message" id="greatbuy-msg"><div>'+msg+'</div></div>');
          setTimeout(function(){ $('#greatbuy-msg').addClass('hide'); }, 5000);
        }else{
          gbdiv.removeClass('hide');
          setTimeout(function(){ gbdiv.addClass('hide'); }, 5000);
        }
      },

      /**
       * Validate if cart total can be a great buy or if cart total is lower than the required for purchase.
       * Receives isCart param to get total based ONLY by subtotal + taxes.
       * If isCart is not sent or is false; total will be calculated subtotal + taxes + shipping.
       * 
       * @param {object} obj 
       * @param {boolean} isCart 
       */
      validateGB: function(obj, isCart = false){
        var self = this;
        //debugger;
        var toSave = {
          sellerId: obj.sellerId,
          purchaseUnit: obj.purchaseUnit,
          validateURL: self.config.validate_url,
          sendURL: self.config.send_url
        };
        window.dccpGBCallback = obj.callback;
        self.saveUtils(toSave);
        
        var total;
        var ajax = null;

        if(!isCart){
          total = this.getTotal(isCart);
          ajax = $.ajax({
            showLoader: true,
            url: self.config.validate_url,
            method: 'GET',
            dataType: 'json',
            error: function(res){
                self.showErrorMsg();
            },
            success: function(res){
                if(res.lower || res.forgb || res.higher){
                    if(res.higher > 0 && Number(total) > res.higher){
                        self.openHigerLimitModal();
                    } else if(res.forgb > 0 && Number(total) > res.forgb){
                        self.openGBModal();
                    }else if(res.lower > 0 && Number(total) < res.lower){
                        self.openLOWModal();
                    }else{
                        window.dccpGBCallback();
                    }
                }else{
                  self.showErrorMsg();
                  window.dccpGBCallback();
                }
            }
          });
          
        }else{
          window.dccpGBCallback();
        }
        console.log(total);
        return ajax;
        
      },

      /**
       * Save great buy utils for reuse in checkout.
       * 
       * @param {string} value 
       */
      saveUtils: function(value){
        localStorage.setItem('dccpgbutils', JSON.stringify(value));
      },

      /**
       * Get saved great buy utils.
       */
      getUtils: function(){
        var utils;
        var lS = localStorage.getItem('dccpgbutils');
        if(lS === null){
          utils = {};
        }else{
          utils = JSON.parse(lS);
          utils = typeof utils === 'object' ? utils : {};
        }
        return utils;
      },

      /**
       * Returns quote total price. If is cart; avoid shipping amount.
       * Magento by default uses shipping amount in cart if it was previously choosen in another bought.
       * 
       * @param {boolean} isCart 
       */
      getTotal: function(isCart){
        var mgTotals = totals.totals();
        var base = mgTotals['grand_total'];
        var tax = mgTotals['tax_amount'];

        if(isCart){
          base = mgTotals['subtotal']
        }

        return base + tax;
      }

  };

});
