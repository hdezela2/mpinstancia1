var config = {
    map: {
        '*': {
            'pricehistory-modal': 'Formax_PriceHistory/js/pricehistory-modal',
            'chart': 'Formax_PriceHistory/js/chart',
            'graph': 'Formax_PriceHistory/js/graph',
            'logger': 'Formax_PriceHistory/js/logger'
        }
    }
};
