define([
    'jquery',
    'domReady!',
    'Magento_Theme/js/customMenuHook'
], function ($) {
    return function(config, element) {
        var checkBreadcrumbsInterval = setInterval(function() {
            checkBreadcrumbs();
        }, 250);

        function checkBreadcrumbs() {
            var homeLink = $(element).find('li.home a');
            var categoryTitle = $('.navigation li.level0:first-child > a.level-top').text();

            if (homeLink.length && categoryTitle.length) {
                homeLink.text(homeLink.text() + ' ' + categoryTitle);
                clearInterval(checkBreadcrumbsInterval);
            }
        }
    }
});