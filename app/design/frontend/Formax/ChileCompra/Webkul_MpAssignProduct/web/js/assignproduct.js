/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAssignProduct
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define([
    "jquery",
    "underscore",
    "Magento_Catalog/js/price-utils",
    "Magento_Customer/js/customer-data",
    "jquery/ui",
    "mage/translate",
    "Magento_Ui/js/modal/modal"
  ], function ($, _, utils, customerData) {
        'use strict';
        $.widget('mpassignproduct.view', {
            options: {},
            _create: function () {
                var self = this;
                var updateSupplText = $.mage.__("You have not selected attributes or no supplier meets your conditions");
                $(document).ready(function () {
                    var isConfig = self.options.isConfig;
                    var productId = self.options.productId;
                    var formKey = self.options.formKey;
                    var ajaxUrl = self.options.url;
                    var dir = self.options.dir;
                    var defaultUrl = self.options.defaultUrl;
                    var sortOrder = self.options.sortOrder;
                    //Remove not needed session validation that avoids showing addtocard button
                    var btnHtml = self.options.btnHtml;
                    var jsonResult = self.options.jsonResult;
                    var symbol = self.options.symbol;
                    var itemWidth = self.options.itemWidth;
                    var superAttribute = {};
                    var customOptions = {};
                    var jsonData = {};
                    var min_price = self.options.min;
                    var assoc_qty = self.options.qty;
                    var minPriceProduct = self.options.minPriceProduct;
                    var showMinimumPrice = self.options.showMinimumPrice;
                    var minPriceToShow = self.options.minPriceToShow;
                    var mainConfigChilds = self.options.mainConfigChilds;
                    var childsNames = self.options.childsNames;
                    var customOptionsAvail = self.options.customOptionsAvail;
                    var singleMinPriceConfigProduct = self.options.singleMinPriceConfigProduct;
                    var offerPrices = self.options.offerPrices;
                    var regionAttrId = self.options.regionAttrId;
                    window.supplierConditions = self.options.rcArray;
                    window.chileanRegions = self.options.regions;
                    window.productRegion = self.options.productRegion;

                    $(document).trigger('WK_AssignProduct_PricesChanged');

                    if (isConfig == 1) {
                        resetData(symbol, true);
                    }

                    if (minPriceToShow) {
                        setTimeout(function () {
                            $(document).find('.product-info-price .price').html(minPriceToShow);
                        },100);
                    }

                    $(document).on('click', '#product-addtocart-button', function (e) {
                        if (showMinimumPrice != 0) {
                            e.preventDefault();
                            var qty = 0;
                            var assignId = minPriceProduct;
                            var formData = $('#product_addtocart_form').serializeArray();
                            $.each(formData,function (i,v) {
                                if (v.name == 'qty') {
                                    qty = v.value;
                                }
                                if (v.name == 'form_key') {
                                    formKey = v.value;
                                }
                                if (customOptionsAvail) {
                                    var atr = v.name;
                                    var val = v.value;
                                    if (atr.indexOf('options') === 0) {
                                        atr = atr.replace(/[^\d.]/g, '');
                                        customOptions[atr] = val;
                                    }
                                }
                            });

                            if (isConfig == 1) {
                                var associateId = '';
                                if (singleMinPriceConfigProduct.length > 0) {
                                    assignId = singleMinPriceConfigProduct[0].parentId;
                                    $('.wk-ap-add-to-cart').each(function () {
                                        if ($(this).data('id') == assignId) {
                                            associateId = $(this).data('associate-id');
                                        }
                                    });
                                }
                                var data = {};
                                $.each(formData, function (key, value) {
                                    var atr = value.name;
                                    var val = value.value;
                                    if (atr.indexOf('super_attribute')===0) {
                                        atr = atr.replace(/[^\d.]/g, '');

                                        superAttribute[atr] = val;
                                    }
                                    if (atr.indexOf('options')===0) {
                                        atr = atr.replace(/[^\d.]/g, '');

                                        customOptions[atr] = val;
                                    }
                                });
                                jsonData.super_attribute = superAttribute;
                                jsonData.options = customOptions;
                                jsonData.associate_id = associateId;
                            }
                            jsonData.mpassignproduct_id = assignId;
                            jsonData.product = productId;
                            jsonData.form_key = formKey;
                            jsonData.qty = qty;
                            jsonData.options = customOptions;
                            $(".wk-loading-mask").removeClass("wk-display-none");
                            $.ajax({
                                url: ajaxUrl,
                                type: 'POST',
                                data: jsonData,
                                success: function (data) {
                                    if (data.backUrl) {
                                        location.reload();
                                    }
                                    $(".wk-loading-mask").addClass("wk-display-none");
                                    ascrollto('maincontent');
                                }
                            });
                        }
                    });

                    function addProductAfterModal($element){
                      if (isConfig == 1) {
                          var associateId = $element.attr("data-associate-id");
                          var formData = $('#product_addtocart_form').serializeArray();
                          var data = {};
                          $.each(formData, function ( key, value ) {
                              var atr = value.name;
                              var val = value.value;
                              if (atr.indexOf('super_attribute') === 0) {
                                  atr = atr.replace(/[^\d.]/g, '');

                                  superAttribute[atr] = val;
                              }
                          });
                          jsonData.super_attribute = superAttribute;
                          jsonData.associate_id = associateId;
                      }
                      var assignId = $element.attr("data-id");
                      var qty = $element.parent().find(".qty").val();
                      jsonData.mpassignproduct_id = assignId;
                      jsonData.product = productId;
                      jsonData.form_key = formKey;
                      jsonData.voucher_amount = 10000;
                      jsonData.qty = qty;
                      $(".wk-loading-mask").removeClass("wk-display-none");
                      $.ajax({
                          url: ajaxUrl,
                          type: 'POST',
                          data: jsonData,
                          success: function (data) {
                              if (data.backUrl) {
                                  location.reload();
                              }
                              $(".wk-loading-mask").addClass("wk-display-none");
                              ascrollto('maincontent');
                          }
                      });
                    }

                    $(document).on('click', '.wk-ap-add-to-cart', function (event) {
                      var $elem = $(this);
                      var assignId = $elem.data("id");
                      var pList = window.supplierFilters.list;
                      var price = 0;
                      pList.forEach(function(item){
                        if(item.baseid===Number(assignId)){
                          price = item.price;
                        }
                      });
                      var lowerPrice = false;
                      pList.forEach(function(item){
                        if(item.baseid!=assignId && parseFloat(item.price) > 0 && parseFloat(item.price)<parseFloat(price)){
                          lowerPrice = true;
                        }
                      });
                      if(lowerPrice){
                        $('#confirm-seller-popup').remove();
                        $('.page-wrapper').append('<div id="confirm-seller-popup"></div>');
                        var $target = $('#confirm-seller-popup'),
                            confirmText = $.mage.__("Before continuing, check if the supplier offers the most convenient price and <b>conditions</b> for your purchase, especially the <b>dispatch to your region.</b>");
                        $target.html("<p>"+confirmText+"</p>");
                        var options = {
                          responsive: true,
                          innerScroll: true,
                          modalClass: 'confirmseller-modal confirm',
                          title: null,
                          closeText: 'Cerrar',
                          buttons: [
                            {
                              text: $.mage.__('Go back'),
                              class: 'no-borders',
                              attr: {},
                              click: function (event) {
                                  this.closeModal(event);
                              }
                            },
                            {
                              text: $.mage.__('Continue'),
                              class: 'action-primary',
                              attr: {},
                              click: function (event) {
                                  addProductAfterModal($elem);
                                  this.closeModal(event);
                              }
                            }
                          ]
                        };
                        $target.modal(options);
                        $target.modal('openModal');
                      }else{
                        addProductAfterModal($elem);
                      }
                    });
                    $(document).on('change', '#list_sorter', function (event) {
                        var marker = "#wk_list_header";
                        var val = $(this).val();
                        if (val != "rating") {
                            val = "price";
                        }
                        var url = defaultUrl+"?list_order="+val+"&list_dir="+dir+marker;
                        location.href = url;
                    });
                    $(document).on('click', '#list_dir_asc', function (event) {
                        event.preventDefault();
                        var marker = "#wk_list_header";
                        var dir = "asc";
                        var url = defaultUrl+"?list_order="+sortOrder+"&list_dir="+dir+marker;
                        location.href = url;
                    });
                    $(document).on('click', '#list_dir_desc', function (event) {
                        event.preventDefault();
                        var marker = "#wk_list_header";
                        var dir = "desc";
                        var url = defaultUrl+"?list_order="+sortOrder+"&list_dir="+dir+marker;
                        location.href = url;
                    });
                    $('#attribute'+regionAttrId).change(function () {
                        window.productRegion = $(this).val();
                    });
                    $('#product-options-wrapper .super-attribute-select').change(function () {
                        $("#go-suppliers-btn").hide();
                        resetData(symbol);
                        var flag = 1;
                        setTimeout(function () {
                            $('#product-options-wrapper .super-attribute-select').each(function () {
                                if ($(this).val() == "") {
                                    flag = 0;
                                }
                            });
                            var $sku = $('.product.attribute.sku');
                            if (flag == 0) {
                              $sku.hide();
                            }
                            if (flag == 1) {
                              var selId = $("#product_addtocart_form input[name='selected_configurable_option']").val()
                              var configChild = getConfigChild(mainConfigChilds, selId, 'getRow');
                              $sku.find('.value').text(configChild.sku);
                              $('#form_quotesupplier .col_2quote .input_disable').text(configChild.sku);
                              if (childsNames !== null) {
                                $('#form_quotesupplier .col_1quote .input_disable').text(childsNames[configChild.sku]);
                              }
                              $("#viewPriceHistory").show();
                              $sku.show();
                              $("#product_addtocart_form input[type='hidden']").each(function () {
                                  var name = $(this).attr("name");
                                  if (name == "selected_configurable_option") {
                                      var product_id = $(this).val();
                                      if (product_id != "") {
                                          if (typeof jsonResult[product_id] != "undefined") {
                                              $(".wk-table-product-list tbody tr").each(function () {
                                                  var id = $(this).attr("data-id");
                                                  var sellerId = $(this).data('sellerid');
                                                  var priceConcat = '';
                                                  if(sellerId && typeof offerPrices[sellerId][productId] !== 'undefined' && offerPrices[sellerId][productId][product_id]){
                                                      var _offPrices = offerPrices[sellerId][productId][product_id];
                                                      var _basePrice = '<span class="base-price">'+symbol+formatPrice(_offPrices.base_price)+'</span>';
                                                      var _specialPrice = '';
                                                      if (id) {
                                                        if (typeof jsonResult[product_id][id] != "undefined") {
                                                            $(this).find(".wk-ap-price").attr('data-base', _offPrices.base_price);
                                                            $(this).find(".wk-ap-price").attr('data-special', _offPrices.special_price);
                                                        } else {
                                                            $(this).find(".wk-ap-price").attr('data-base', '0');
                                                            $(this).find(".wk-ap-price").attr('data-special', '0');
                                                        }
                                                      }
                                                      if(_offPrices.special_price != ''){
                                                          _specialPrice = '<span class="special-price">'+symbol+formatPrice(_offPrices.special_price)+'</span>';
                                                          priceConcat += _specialPrice + _basePrice;
                                                      }else{
                                                          priceConcat += _basePrice;
                                                      }
                                                  }
                                                  if (id) {
                                                    if (typeof jsonResult[product_id][id] != "undefined") {
                                                          //$(this).find(".wk-ap-product-price").html(symbol+jsonResult[productId][id]['price'].split('.')[0]);
                                                          $(this).find(".wk-ap-product-price").html(priceConcat);
                                                          $("#go-suppliers-btn").show();
                                                          var qty = jsonResult[product_id][id]['qty'];
                                                          if (qty <= 0) {
                                                              var avl = $.mage.__("OUT OF STOCK");
                                                              $(this).data('wkhqt','0');
                                                          } else {
                                                              $(this).data('wkhqt','1');
                                                              var avl = $.mage.__("IN STOCK");
                                                              $(this).find(".wk-ap-action-col").html(btnHtml);
                                                              pushAndUpdateFilters(id);
                                                              $(this).find(".wk-ap-add-to-cart").attr('data-id', id);
                                                              $(this).find(".wk-ap-add-to-cart").attr('data-associate-id', jsonResult[product_id][id]['id']);
                                                          }
                                                          $(this).find(".wk-ap-product-avl").html(avl);
                                                          if (singleMinPriceConfigProduct && singleMinPriceConfigProduct.length > 0) {
                                                              if (singleMinPriceConfigProduct[0].parentId == id) {
                                                                  var price = 0;
                                                                  $.each(singleMinPriceConfigProduct, function (i, v) {
                                                                      if (jsonResult[product_id][id]['id'] == v.id) {
                                                                          price = parseFloat(v.price).toFixed();
                                                                      }
                                                                  });
                                                                  if (singleMinPriceConfigProduct[0].parentId != 0 && singleMinPriceConfigProduct[0].id != 0) {
                                                                      $(document).find('.product-info-price .price').html(formatPrice(price));
                                                                  }
                                                              }
                                                          }
                                                      } else {
                                                        $(this).find(".wk-ap-product-price").html("");
                                                      }
                                                  } else {
                                                      var configChild = getConfigChild(mainConfigChilds, product_id, 'getRow');

                                                      //$(this).find(".wk-ap-product-price").html(utils.formatPrice(configChild.price));
                                                      $(this).find(".wk-ap-product-price").html(priceConcat);
                                                      var qty = configChild.stock;
                                                      if (qty <= 0) {
                                                          $(this).data('wkhqt','0');
                                                          var avl = $.mage.__("OUT OF STOCK");
                                                      } else {
                                                          $(this).data('wkhqt','1');
                                                          var avl = $.mage.__("IN STOCK");
                                                          $(this).find(".wk-ap-action-col").html(btnHtml);
                                                          pushAndUpdateFilters(null);
                                                          $(this).find(".wk-ap-add-to-cart").attr('data-id', '');
                                                          $(this).find(".wk-ap-add-to-cart").attr('data-associate-id', '');
                                                      }
                                                      $(this).find(".wk-ap-product-avl").html(avl);
                                                  }
                                              });
                                          }
                                          $(".wk-table-product-list tbody tr").sort(function(a, b) {
                                              var $aPrice = $(a).find('.wk-ap-price');
                                              var $bPrice = $(b).find('.wk-ap-price');

                                              return parseFloat($aPrice.attr('data-special') || $aPrice.attr('data-base')) - parseFloat($bPrice.attr('data-special') || $bPrice.attr('data-base'));
                                          }).appendTo('.wk-table-product-list tbody');
                                      }
                                  }
                              });
                            }
                        }, 0);
                    });

                    $("body").on("click", ".wk-ap-product-showcase-gallery-item img", function () {
                        $(".wk-ap-product-showcase-gallery-item").removeClass("wk-ap-active");
                        $(this).parent().addClass("wk-ap-active");
                        var src = $(this).attr("src");
                        $(".wk-ap-product-showcase-main img").attr("src", src);
                    });
                    $("body").on("click", ".wk-gallery-right", function () {
                        var currentObject = $(this);
                        var count = $(this).parent().find(".wk-ap-product-showcase-gallery-wrap").attr("data-count");
                        if (count > 5) {
                            var left = $(this).parent().find(".wk-ap-product-showcase-gallery-wrap").css("left");
                            left = left.replace('px', '');
                            left = parseFloat(left);
                            count = count-5;
                            var total = itemWidth*count;
                            var final = left+total;
                            if (final > 0) {
                                $(this).parent().find(".wk-ap-product-showcase-gallery-wrap").animate({ left: '-='+itemWidth+'px' }, 'slow', function () {
                                    checkRight(currentObject, itemWidth);
                                });
                            } else {
                                $(this).parent().find(".wk-ap-product-showcase-gallery-wrap").animate({ left: '-'+total+'px' });
                            }
                        }
                    });
                    $("body").on("click", ".wk-gallery-left", function () {
                        var currentObject = $(this);
                        var left = $(this).parent().find(".wk-ap-product-showcase-gallery-wrap").css("left");
                        left = left.replace('px', '');
                        left = parseFloat(left);
                        if (left >= 0) {
                            $(this).parent().find(".wk-ap-product-showcase-gallery-wrap").animate({ left: '0px' });
                        } else {
                            $(this).parent().find(".wk-ap-product-showcase-gallery-wrap").animate({left: '+='+itemWidth+'px' }, 'slow', function () {
                                checkLeft(currentObject, itemWidth);
                            });
                        }
                    });
                    $("body").on("click", ".wk-ap-product-image", function () {
                        var display = $(this).parent().find(".wk-ap-product-image-content").css("display");
                        $(".wk-ap-product-image-content").hide();
                        if (display == "none") {
                            $(this).parent().find(".wk-ap-product-image-content").show();
                        }
                        setTimeout(function () {
                            $(".wk-ap-product-image-content").trigger("click");
                        }, 100);
                    });
                    $("body").on("click",".mp-assign", function () {
                        $(".wk-ap-product-image-content").hide();
                    });
                    $("body").on("click", ".swatch-option", function () {
                        resetData(symbol);
                        var selected_options = {};
                        jQuery('div.swatch-attribute').each(function (k,v) {
                            var attribute_id    = jQuery(v).attr('attribute-id');
                            var option_selected = jQuery(v).attr('option-selected');
                            if (!attribute_id || !option_selected) {
                                return;
                            }
                            selected_options[attribute_id] = option_selected;
                        });

                        var product_id_index = jQuery('[data-role=swatch-options]').data('mageSwatchRenderer').options.jsonConfig.index;
                        var found_ids = [];
                        jQuery.each(product_id_index, function (product_id, attributes) {
                            var productIsSelected = function (attributes, selected_options) {
                                return _.isEqual(attributes, selected_options);
                            }
                            if (productIsSelected(attributes, selected_options)) {
                                // found_ids.push(product_id);
                                $(".wk-table-product-list tbody tr").each(function () {
                                    var id = $(this).attr("data-id");
                                    var sellerId = $(this).data('sellerid');
                                    var priceConcat = '';
                                    if(sellerId && typeof offerPrices[sellerId][productId] !== 'undefined' && offerPrices[sellerId][productId][product_id]){
                                        var _offPrices = offerPrices[sellerId][productId][product_id];
                                        var _basePrice = '<span class="base-price">'+symbol+formatPrice(_offPrices.base_price)+'</span>';
                                        var _specialPrice = '';
                                        if(_offPrices.special_price != ''){
                                            _specialPrice = '<span class="special-price">'+symbol+formatPrice(_offPrices.special_price)+'</span>';
                                            priceConcat += _specialPrice + _basePrice;
                                        }else{
                                            priceConcat += _basePrice;
                                        }
                                    }

                                    if (id) {
                                        if (typeof jsonResult[product_id] != "undefined") {
                                            //$(this).find(".wk-ap-product-price").html(symbol+jsonResult[product_id][id]['price'].split('.')[0]);
                                            $(this).find(".wk-ap-product-price").html(priceConcat);
                                            var qty = jsonResult[product_id][id]['qty'];
                                            if (qty <= 0) {
                                                $(this).data('wkhqt','0');
                                                var avl = $.mage.__("OUT OF STOCK");
                                            } else {
                                                $(this).data('wkhqt','1');
                                                var avl = $.mage.__("IN STOCK");
                                                $(this).find(".wk-ap-action-col").html(btnHtml);
                                                pushAndUpdateFilters(id);
                                                $(this).find(".wk-ap-add-to-cart").data('id', id);
                                                $(this).find(".wk-ap-add-to-cart").data('associate-id', jsonResult[product_id][id]['id']);
                                            }
                                            $(this).find(".wk-ap-product-avl").html(avl);
                                            if (singleMinPriceConfigProduct && singleMinPriceConfigProduct.length > 0) {
                                                if (singleMinPriceConfigProduct[0].parentId == id) {
                                                    var price = 0;
                                                    $.each(singleMinPriceConfigProduct, function (i, v) {
                                                        if (jsonResult[product_id][id]['id'] == v.id) {
                                                            price = parseFloat(v.price).toFixed();
                                                        }
                                                    });
                                                    if (singleMinPriceConfigProduct[0].parentId != 0 && singleMinPriceConfigProduct[0].id != 0) {
                                                        setTimeout(function () {
                                                            $(document).find('.product-info-price .price').html(formatPrice(price));
                                                        }, 1);
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        var configChild = getConfigChild(mainConfigChilds, product_id, 'getRow');
                                        $(this).find(".wk-ap-product-price").html(priceConcat);
                                        var qty = configChild.stock;
                                        if (qty <= 0) {
                                            $(this).data('wkhqt','0');
                                            var avl = $.mage.__("OUT OF STOCK");
                                        } else {
                                            $(this).data('wkhqt','1');
                                            var avl = $.mage.__("IN STOCK");
                                            $(this).find(".wk-ap-action-col").html(btnHtml);
                                            pushAndUpdateFilters(null);
                                            $(this).find(".wk-ap-add-to-cart").data('id', '');
                                            $(this).find(".wk-ap-add-to-cart").data('associate-id', '');
                                        }
                                        $(this).find(".wk-ap-product-avl").html(avl);
                                    }
                                });
                            }
                        });
                    });
                    $("body").on("click", ".swatch-attribute", function () {
                        resetData(symbol);
                        var selected_options = {};
                        jQuery('div.swatch-attribute').each(function (k,v) {
                            var attribute_id    = jQuery(v).attr('attribute-id');
                            var option_selected = jQuery(v).attr('option-selected');

                            if (!attribute_id || !option_selected) {
                                return;
                            }
                            selected_options[attribute_id] = option_selected;
                        });

                        var product_id_index = jQuery('[data-role=swatch-options]').data('mageSwatchRenderer').options.jsonConfig.index;
                        var found_ids = [];
                        jQuery.each(product_id_index, function (product_id, attributes) {
                            var productIsSelected = function (attributes, selected_options) {
                                return _.isEqual(attributes, selected_options);
                            }
                            if (productIsSelected(attributes, selected_options)) {
                                // found_ids.push(product_id);
                                $(".wk-table-product-list tbody tr").each(function () {
                                    var id = $(this).attr("data-id");
                                    var sellerId = $(this).data('sellerid');
                                    var priceConcat = '';
                                    if(sellerId && typeof offerPrices[sellerId][productId] !== 'undefined' && offerPrices[sellerId][productId][product_id]){
                                        var _offPrices = offerPrices[sellerId][productId][product_id];
                                        var _basePrice = '<span class="base-price">'+symbol+formatPrice(_offPrices.base_price)+'</span>';
                                        var _specialPrice = '';
                                        if(_offPrices.special_price != ''){
                                            _specialPrice = '<span class="special-price">'+symbol+formatPrice(_offPrices.special_price)+'</span>';
                                            priceConcat += _specialPrice + _basePrice;
                                        }else{
                                            priceConcat += _basePrice;
                                        }
                                    }

                                    if (id) {
                                        if (typeof jsonResult[product_id] != "undefined") {
                                            //$(this).find(".wk-ap-product-price").html(symbol+jsonResult[product_id][id]['price'].split('.')[0]);
                                            $(this).find('.wk-ap-product-price').html(priceConcat);
                                            var qty = jsonResult[product_id][id]['qty'];
                                            if (qty <= 0) {
                                                $(this).data('wkhqt','0');
                                                var avl = $.mage.__("OUT OF STOCK");
                                            } else {
                                                $(this).data('wkhqt','1');
                                                var avl = $.mage.__("IN STOCK");
                                                $(this).find(".wk-ap-action-col").html(btnHtml);
                                                pushAndUpdateFilters(id);
                                                $(this).find(".wk-ap-add-to-cart").data('id', id);
                                                $(this).find(".wk-ap-add-to-cart").data('associate-id', jsonResult[product_id][id]['id']);
                                            }
                                            $(this).find(".wk-ap-product-avl").html(avl);
                                            if (singleMinPriceConfigProduct && singleMinPriceConfigProduct.length > 0) {
                                                if (singleMinPriceConfigProduct[0].parentId == id) {
                                                    var price = 0;
                                                    $.each(singleMinPriceConfigProduct, function (i, v) {
                                                        if (jsonResult[product_id][id]['id'] == v.id) {
                                                            price = parseFloat(v.price).toFixed();
                                                        }
                                                    });
                                                    if (singleMinPriceConfigProduct[0].parentId != 0 && singleMinPriceConfigProduct[0].id != 0) {
                                                        setTimeout(function () {
                                                            $(document).find('.product-info-price .price').html(formatPrice(price));
                                                        },1);
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        var configChild = getConfigChild(mainConfigChilds, product_id, 'getRow');

                                        //$(this).find(".wk-ap-product-price").html(utils.formatPrice(configChild.price));
                                        $(this).find(".wk-ap-product-price").html(priceConcat);
                                        var qty = configChild.stock;
                                        if (qty <= 0) {
                                            $(this).data('wkhqt','0');
                                            var avl = $.mage.__("OUT OF STOCK");
                                        } else {
                                            $(this).data('wkhqt','1');
                                            var avl = $.mage.__("IN STOCK");
                                            $(this).find(".wk-ap-action-col").html(btnHtml);
                                            pushAndUpdateFilters(null);
                                            $(this).find(".wk-ap-add-to-cart").data('id', '');
                                            $(this).find(".wk-ap-add-to-cart").data('associate-id', '');
                                        }
                                        $(this).find(".wk-ap-product-avl").html(avl);
                                    }
                                });
                            }
                        });
                    });
                });
                function resetData(symbol, isInitial)
                {
                    if(isInitial === undefined){
                        isInitial = false;
                    }
                    var min_price = self.options.min;
                    var qty = self.options.qty;
                    var mainConfigChilds = self.options.mainConfigChilds;
                    var singleMinPriceConfigProduct = self.options.singleMinPriceConfigProduct;

                    window.supplierFilters.applied['baseid'] = [];
                    $('#suppliers_box .wk-table-product-list tbody>tr#no-suppliers>td').text(updateSupplText);
                    $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered',[isInitial]);

                    $(".wk-table-product-list tbody tr").each(function () {
                        var id = $(this).attr("data-id");

                        if (id) {
                            $(this).find(".wk-ap-action-col").html('');
                            $(this).find(".wk-ap-product-price").html(symbol+min_price[id].split('.')[0]);
                            if (qty[id]) {
                                $(this).find(".wk-ap-product-avl").html($.mage.__("IN STOCK"));
                            } else {
                                $(this).find(".wk-ap-product-avl").html($.mage.__("OUT OF STOCK"));
                            }
                        } else {
                            var configChild = getConfigChild(mainConfigChilds, '', 'getMinRow');
                            $(this).find(".wk-ap-action-col").html('');
                            $(this).find(".wk-ap-product-price").html(utils.formatPrice(configChild.price));
                            if (configChild.stock) {
                                $(this).find(".wk-ap-product-avl").html($.mage.__("IN STOCK"));
                            } else {
                                $(this).find(".wk-ap-product-avl").html($.mage.__("OUT OF STOCK"));
                            }
                        }
                        $(document).trigger('WK_AssignProduct_PricesChanged');
                    });
                    if (singleMinPriceConfigProduct && singleMinPriceConfigProduct.length > 0) {
                        var price = parseFloat(singleMinPriceConfigProduct[0].price).toFixed();
                        if (singleMinPriceConfigProduct[0].parentId != 0 && singleMinPriceConfigProduct[0].id != 0) {
                            setTimeout(function () {
                                $(document).find('.product-info-price .price').html(utils.formatPrice(price));
                            },0);
                        }
                    }
                }

                function formatPrice(price){
                    return Number(price).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
                }

                function pushAndUpdateFilters(rowId){
                  if(rowId){
                    window.supplierFilters.applied['baseid'].push(Number(rowId));
                  }
                  $(document).trigger('WK_AssignProduct_PricesChanged');
                  $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered',[false]);
                }

                function checkLeft(currentObject, itemWidth)
                {
                    var left = currentObject.parent().find(".wk-ap-product-showcase-gallery-wrap").css("left");
                    left = left.replace('px', '');
                    left = parseFloat(left);
                    if (left >= 0) {
                        currentObject.parent().find(".wk-ap-product-showcase-gallery-wrap").animate({ left: '0px' });
                    }
                }

                function checkRight(currentObject, itemWidth)
                {
                    var count = currentObject.parent().find(".wk-ap-product-showcase-gallery-wrap").attr("data-count");
                    var left = currentObject.parent().find(".wk-ap-product-showcase-gallery-wrap").css("left");
                    left = left.replace('px', '');
                    left = parseFloat(left);
                    count = count-5;
                    var total = itemWidth*count;
                    var final = left+total;
                    if (final <= 0) {
                        currentObject.parent().find(".wk-ap-product-showcase-gallery-wrap").animate({ left: '-'+total+'px' });
                    }
                }
                function ascrollto(id)
                {
                    var etop = $('#' + id).offset().top;
                    $('html, body').animate({
                      scrollTop: etop-30
                    }, 10);
                }
                function getConfigChild(array, id, action)
                {
                    var returnData;
                    if (action == 'getRow') {
                        $.each(array, function (i, v) {
                            if (v.id == id) {
                                returnData = v;
                                return false;
                            }
                        });
                    }
                    if (action == 'getMinRow') {
                        var min = '';
                        $.each(array, function (i, v) {
                            if (min == '') {
                                min = parseFloat(v.price);
                                returnData = v;
                            }
                            if (parseFloat(v.price) < min) {
                                min = parseFloat(v.price);
                                returnData = v;
                            }
                        });
                    }
                    return returnData;
                }
            }
        });
        return $.mpassignproduct.view;
    });
