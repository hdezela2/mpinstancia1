/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAssignProduct
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define([
    "jquery",
    'Magento_Ui/js/modal/confirm',
    'Magento_Ui/js/modal/alert',
    "jquery/ui",
    "mage/translate",
    ], function ($, confirmation, alert) {
        'use strict';
        $.widget('mage.list', {
            options: {},
            _create: function () {
                var self = this;
                var assignInfo = self.options.assignInfo;
                var label = self.options.label;
                var msg = self.options.msg;
                var minPrice = self.options.minPrice;
                $(document).ready(function () {
                    $(".products ol.product-items > li.product-item").each(function () {
                        var productLink = $(this).find(".product-item-link").attr("href");
                        if (assignInfo[productLink]['stock']) {
                            if (assignInfo[productLink]["minPrice"] != 0) {
                                //$(this).find(".price-container .price").html(assignInfo[productLink]["minPrice"]);
                            }
                            $(this).find(".sellers-count").html('<span><strong>'+assignInfo[productLink]['supplierCount']+'</strong> '+assignInfo[productLink]['supplierText']+'</span>');
                        }
                    });
                    $(".products-grid.wishlist ol.product-items > li.product-item").each(function () {
                        var productLink = $(this).find(".product-item-link").attr("href");
                        if (assignInfo[productLink]['stock']) {
                            $(this).find(".price-box").after('<a href="'+productLink+'">'+msg+'</a>');
                            $(this).find(".action.tocart").remove();
                        }
                    });
                    $("#product-comparison > tbody").each(function () {
                        var productLink = $(this).find(".product-item-name > a").attr("href");
                        if (assignInfo[productLink]['stock']) {
                            $(this).find(".price-box").after('<a href="'+productLink+'">'+msg+'</a>');
                            $(this).find(".action.tocart").remove();
                        }
                    });
                });
            }
        });
        return $.mage.list;
    });
