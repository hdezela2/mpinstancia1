define([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
], function($) {
    'use strict';
    /** Add validation to price input **/
    $("input.validate-zero-or-greater[name*='price']").attr('data-validate',"{'price-format':true}");
    $("input.required-entry.validate-zero-or-greater[name*='price']").attr('data-validate',"{required:true, 'price-format':true}");
    return function(currencyConfig) {

        /** Sample price **/
        // ^[0-9]{1,3}(?:[.]?[0-9]{1,3})*,[0-9]{2}$
        const samplePriceDolar = '532'+currencyConfig.decimal+'12';
        const samplePriceVoucher = '298'+currencyConfig.decimal+'5';

        // ^[0-9]{1,3}(?:[.]?[0-9]{1,3})*$
        const samplePriceChile = '532';

        // original
        // ^[0-9]{1,3}(?:[.]?[0-9]{1,3})*,?[0-9]{2}$
        // exp reg no toma numero < a 10 como validos

        /** Validate price format **/
        $.validator.addMethod(
            'price-format',
            function(value) {
                if (value === "") {
                    return true;
                }
                var validator = this;
                var result = false;
                var precision = currencyConfig.precision;
                var pattern = '^[0-9]{1,3}(?:['+currencyConfig.group+']?[0-9]{1,3})*$';
                if (currencyConfig.precision === 2) {
                    pattern = '^[0-9]{1,3}(?:['+currencyConfig.group+']?[0-9]{1,3})*'+currencyConfig.decimal+'[0-9]{'+precision+'}$';
                    validator.priceErrorMessage = $.mage.__('Price format is wrong, it should be like: %1').replace('%1', samplePriceDolar);
                }else if (currencyConfig.precision === 1) {
                    pattern = '^[0-9]{1,3}(?:['+currencyConfig.group+']?[0-9]{1,3})*'+currencyConfig.decimal+'?[0-9]{'+precision+'}$';
                    validator.priceErrorMessage = $.mage.__('Price format is wrong, it should be like: %1').replace('%1', samplePriceVoucher);
                }else if (currencyConfig.precision === 0) {
                    validator.priceErrorMessage = $.mage.__('Price format is wrong, it should be like: %1').replace('%1', samplePriceChile);
                }else{
                    validator.priceErrorMessage = $.mage.__('Price format is wrong');
                }
                let regExp = new RegExp(pattern);
                if (regExp.test(value)) {
                    result = true;
                }
                return result;
            }, function () {
                return this.priceErrorMessage;
            }
        );

        /** Validate form before submit **/
        $('form').submit(function(event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            if ($(this).valid()) {
                $(this).find('input.validate-zero-or-greater').each(function() {
                    unFormattedInput($(this));
                });
                $(this).unbind().submit();
            }

            return false;
        });

        /**
         * Un formatted price before form submit
         * @param element
         * @returns {*}
         */
        function unFormattedInput(element) {
            const val = $.trim(element.val());
            const numb = val.replace(/\./g, '').replace(/,/g, '.');
            element.val(numb === '' ? element.val() : numb);

            return element;
        }
    }
});