require(['jquery','underscore',"Magento_Ui/js/modal/modal", 'owl_carousel'],function($,_, modal){

  $(document).ready(function(){
    if($('.catalog-product-view').length && $('.action.send-msg-chat')){
      $('.action.send-msg-chat').on('click', function(e){
        $('#ask-form input,#ask-form textarea').removeClass('mage-error');
        $('#ask-form div.mage-error').remove();
        $('#wk-mp-ask-data').find('input[name="seller-id"]').val($(this).attr('data-seller-id'));
        $('#wk-mp-ask-data').modal('openModal');
        e.preventDefault();
      });
    }
  });

  $(function(){

    overlayNotHeader();
    ifCatHasImg();
    separateMenuBlocks();
    moveCategoryToolbar();
    productTabs();
    toggleCustomerMenu();
    qtyButtons();
    addToCartProductListOverride();
    makeFiltersCollapsible();
    replaceSkuInProductPage();
    onlyNumbersClass();

    $('.promo-slider .owl-products-carousel').on('initialized.owl.carousel',function(){
      adjustPromoSliderHeight();
      $(window).on('resize',function(){
        adjustPromoSliderHeight();
      });
    });

    function replaceSkuInProductPage(){
      window.dccpProductIdFound = null;
      var $sku = $('.product.attribute.sku .value');
      $sku.data('default',$sku.text());
      $(document).on('click','.swatch-option', function(){
        validateConfigurableIds($sku);
      });
      $(document).on('change','.swatch-select', function(){
        validateConfigurableIds($sku);
      });
    }

    function validateConfigurableIds($sku){
      var selected_options = {};
      $('div.swatch-attribute').each(function(k,v){
          var attribute_id    = $(v).attr('attribute-id');
          var option_selected = $(v).attr('option-selected');
          if(!attribute_id || !option_selected){ return;}
          selected_options[attribute_id] = option_selected;
      });
      var prodId = null,
          swatchRenderer = $('[data-role=swatch-options]').data('mageSwatchRenderer');
      if(swatchRenderer){
        var found_ids = [],
            jsonConfig = swatchRenderer.options.jsonConfig;
        $.each(jsonConfig.index, function(product_id, attributes){
            var productIsSelected = function(attributes, selected_options){
                return _.isEqual(attributes, selected_options);
            }
            if(productIsSelected(attributes, selected_options)){
                found_ids.push({productId: product_id});
            }
        });
        window.dccpProductIdFound = null;
        if(found_ids[0]){
          prodId = jsonConfig.dccpProductId[found_ids[0].productId];
          window.dccpProductIdFound = prodId;
        }
        $sku.text(prodId ? prodId : $sku.data('default'));
      }
    }

    function qtyButtons(){
      $(document).on('keypress', '.qty-container .qty', function(e){
        if(e.keyCode == 13){
          e.preventDefault();
        }
      });
      $(document).on('click','.qty-container .decreasing-qty', function(e){
        e.stopPropagation();
        var inp = $(this).parent().find('.qty');
        var qtyValue = parseInt(inp.val());
        if(isNaN(qtyValue)){
          qtyValue = 1;
        }else{
          qtyValue = qtyValue - 1;
          if (qtyValue <= 0) {
              qtyValue = 1;
          }
        }
        inp.val(qtyValue).trigger('keyup');
      });
      $(document).on('click','.qty-container .increasing-qty', function(e){
        e.stopPropagation();
        var inp = $(this).parent().find('.qty');
        var qtyValue = parseInt(inp.val());
        if(isNaN(qtyValue)){
          qtyValue = 1;
        }else{
          qtyValue = qtyValue + 1;
        }
        inp.val(qtyValue).trigger('keyup');
      });
    }

    function toggleCustomerMenu(){
      $('.cc-header-link.acc').on('click',function(){
        $('.cc-customer-menu', this).toggleClass('active');
        $(this).toggleClass('active');
      });
    }

    function addToCartProductListOverride(){
      $('.tocart.cc-link').on('click',function(){
        window.location.href = $(this).attr('href');
      });
    }

    function adjustPromoSliderHeight(){
      setTimeout(function(){
        if($(window).width()>767){
          var $img = $('.promo-slider .custom-slider:not(.products) .banner-image img');
          $img.css('max-height',$img.closest('.promo-slider').find('.custom-slider.products .product-item').height()+'px');
        }else{
          var $img = $('.promo-slider .custom-slider:not(.products) .banner-image img').css('max-height','');
        }
      }, 200);
    }

    function ifCatHasImg(){
      if($('.category-view .category-image').length){
        $('.category-view').addClass('with-img');
      }else{
        $('.category-view').addClass('without-img');
      }
    }

    function productTabs(){
      if($('.cc-product-tabs').length){
        $('.cc-product-tabs>ul>li').on('click',function(){
          if($(this).hasClass('active')){
            return;
          }
          $('.cc-product-tabs>ul>li').removeClass('active');
          $(this).addClass('active');
          var $idTab = $(this).attr('id').split('-');
          $('.cc-product-tabs>div.product-content').removeClass('active');
          $('.cc-product-tabs>div#content-'+$idTab[1]).addClass('active');
        });
      }
    }

    function moveCategoryToolbar(){
      $(document).on('moveCCToolbar',function(){
        var $toolbar = $('.products.wrapper + .toolbar');
        if($('.cc-after-pageMain .toolbar').length){
          $('.cc-after-pageMain .toolbar').remove();
        }
        var t = setInterval(function(){
          if($toolbar.length){
            if(!$('.cc-after-pageMain').length){
              $('.page-main').after('<div class="cc-after-pageMain"></div>');
            }
            $toolbar.appendTo('.cc-after-pageMain');
            clearInterval(t);
          }
        }, 300);
      });
      $(document).trigger('moveCCToolbar');
    }

    function makeFiltersCollapsible(){
      $(document).on('click','.block.filter .block-title',function(e){
        e.preventDefault();
        console.log('click');
        $(this).parent().toggleClass('active');
      });
    }

    function overlayNotHeader(){
      $('.page-wrapper').append('<div class="overlay-notHeader"></div>');

      $('div.block.block-minicart').on('dropdowndialogopen',$.proxy(function(){
        $('.overlay-notHeader').addClass('activatedByCart');
      }));
      $('div.block.block-minicart').on('dropdowndialogclose',$.proxy(function(){
        $('.overlay-notHeader').removeClass('activatedByCart');
      }));

    }

    function separateMenuBlocks(){
      $('.navigation ul.level1').each(function(){
        $(this).addClass('cc-menu-cols');
        var cols = 3;
        var $li = $('>li', this);
        var liPerCol = Math.ceil($li.length / cols);
        for(var i=0;i<cols;i++){
          $(this).append('<div class="cc-menu-col cc-menu-col-'+(i+1)+'"></div>');
        }
        var j = 1;
        var currentDiv = 1;
        var $thisUl = $(this);
        $li.each(function(){
          if(j>liPerCol){
            j = 1;
            currentDiv++;
          }
          var $currDiv = $thisUl.find('.cc-menu-col-'+currentDiv);
          $(this).appendTo($currDiv);
          if(!$currDiv.hasClass('has-li')){
            $currDiv.addClass('has-li');
          }
          j++;
        });
      });
    }

    function onlyNumbersClass(){
      $('.onlynumbers-input').on('input',function(){
        $(this).val($(this).val().replace(/[^0-9.]/g, ''));
      });

      $('.onlybetter-numbers').on('input',function(){
        var val = $(this).val();
        var orig = $(this).data('original');
        if(Number(val) < Number(orig)){
          $(this).val(orig);
        }
      });
    }

  });
});
