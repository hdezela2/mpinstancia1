define([
        "jquery"
    ], function($){
        var WkTaxAjax = {
            initTax: function(config) {
              
              $('select[name="rule_code"]').data('selected', $('select[name="rule_code"]').val());
              //$('select[name="rule_code"]').off().on('change',function(){
              $(document).on('change', 'select[name="rule_code"]', function(){
                let rule = $(this).val();
                let isAllow = $(this).find("option:selected").attr('is_comment_allow');
                if(isAllow == 1){
                    $(this).closest('.totals.tax').find('.wk_order_comment').show();
                    $(this).closest('.totals.tax').find('.price.default').hide();
                } else {
                    $(this).closest('.totals.tax').find('.wk_order_comment').hide();
                    $(this).closest('.totals.tax').find('.price.default').show();
                    if($(this).data('selected') != rule){
                      $('body').trigger('processStart');
                      updateQuoteTaxData(rule);
                    }else{
                      $(this).data('selected', $(this).val());
                    }
                }
              });

              //$('button.apply-tax').off().on('click',function(){
              $(document).on('click', 'button.apply-tax' ,function(){
                let textarea = $(this).parent().find('textarea');
                let rule = textarea.data('key'),
                    comment = textarea.val();
                if(comment != ''){
                  $('body').trigger('processStart');
                  updateQuoteTaxData(rule, comment);
                  $(this).parent().find('.mage-error').remove();
                }else{
                  $(this).after('<div for="tax_comment" generated="true" class="mage-error">Campo obligatorio.</div>');
                  textarea.focus();
                }
              });

              function updateQuoteTaxData(rule, comment){
                var form = $('#wktax-applier-form');
                if(!form.length){
                  var form = document.createElement('form'),
                      rule_inp = document.createElement('input'),
                      comment_inp = document.createElement('input'),
                      formkey = document.createElement('input');
                  form.method = 'POST';
                  form.action = config.formUrl;
                  form.id = 'wktax-applier-form';
                  formkey.name = 'form_key';
                  formkey.type = 'hidden';
                  formkey.value = getFormKey();
                  rule_inp.name = 'rule_code';
                  rule_inp.value = rule;
                  rule_inp.type = 'hidden';
                  comment_inp.name = 'tax_comment';
                  comment_inp.value = comment;
                  comment_inp.type = 'hidden';
                  form.appendChild(rule_inp);
                  form.appendChild(comment_inp);
                  form.appendChild(formkey);
                  document.body.appendChild(form);
                  form.submit();
                }else{
                  form.find('input[name="rule_code"]').val(rule);
                  form.find('input[name="tax_comment"]').val(comment);
                  form.submit();
                }
              }

              function getFormKey(){
                return $.cookie('form_key');
              }
            }
        };
        return WkTaxAjax.initTax;
    }
);
