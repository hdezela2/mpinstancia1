define([
    'jquery',
    'domReady!',
    'mage/translate'
], function ($) {
    return function(config, element) {
        var levelTop = $(element).children('.level0');
        var levelTopLink = levelTop.children('a.level-top');
        var levelTwoList = levelTop.children('ul.level0');
        var categoryTitle = $('.navigation li.level0:first-child > a.level-top').text();

        levelTopLink.on('click', function(e) {
            e.preventDefault();
        });
        levelTwoList.prepend('<li class="level1"><a href="' + levelTopLink.attr('href') + '">' + $.mage.__('Ver todo') + ' ' + categoryTitle + '</a></li>');
    }
});