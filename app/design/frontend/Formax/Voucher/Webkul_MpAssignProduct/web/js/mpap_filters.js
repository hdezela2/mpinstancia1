require(['jquery','mage/translate'],function($){
  $(function(){
    window.supplierFilters = {};
    window.supplierFilters.applied = {};
    var supplierTr = $('#suppliers_box .wk-table-product-list>tbody>tr:not(#no-suppliers)');
    var afterLabel = $('.product-info-price-wk .after-label');
    afterLabel.data('default',afterLabel.text());
    
    $(document).on('WK_AssignProduct_PricesChanged',function(){
      var supplierList = [];
      var suppStamps = window.supplierStamps;
      var conditions = window.supplierConditions;
      supplierTr.each(function(){
        var _sellerId = $(this).data('sellerid');
        var stampsArr = suppStamps && suppStamps[_sellerId];
        var conditionsArr = conditions && conditions[_sellerId];
        var _price = $(this).find('td[data-price]>.wk-ap-product-price'),
            _priceN = '';
        var $basePriceINP = $(this).find('.base-price');
        var $specialPriceINP = $(this).find('.special-price');

        $basePriceINP.text(formatPrice($basePriceINP.text())+'%');
        $specialPriceINP.text(formatPrice($specialPriceINP.text())+'%');

        if(_price.has('.special-price').length){
          _priceN = _price.find('.special-price').text();
        }else{
          _priceN = _price.find('.base-price').text();
        }
        _price.data('price', _priceN);
        var _priceF = Number(cleanString(_priceN));
        var obj = {
          id: _sellerId,
          baseid: $(this).data('id'),
          name: $(this).find('td[data-name]').data('name'),
          price: _priceF,
          stamps: stampsArr ? suppStamps[_sellerId] : [],
          conditions: conditionsArr ? conditions[_sellerId] : [],
          hasQty: Number($(this).data('wkhqt'))
        };
        supplierList.push(obj);
      });
      supplierList.sort(orderByPrice);
      window.supplierFilters.list = supplierList;
      initStampsFilter();
      initRegionFilter();
      initPriceSlider();
      initDDaysFilter();
    });
    $(document).trigger('WK_AssignProduct_PricesChanged');

    $('.supplier-filters #cc-suppliers_filter-by-days').on('input',function(){
      var inpVal = $(this).val();
      $(this).closest('.filter-option').find('#days-rangeSelected').text(inpVal);
      if(inpVal!='' && Number(inpVal)>0){
        window.supplierFilters.applied['deliveryDays'] = inpVal;
      }else{
        delete window.supplierFilters.applied['deliveryDays'];
      }
      $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered');
    });

    $('.supplier-filters').on('change', 'input[name="stamps-filter"]',function(){
      var selectedStamps = [];
      $.each($('.supplier-filters input[name="stamps-filter"]:checked'), function(){
        selectedStamps.push($(this).val());
      });
      if(selectedStamps.length){
        window.supplierFilters.applied['stamps'] = selectedStamps;
      }else{
        delete window.supplierFilters.applied['stamps'];
      }
      $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered');
    });

    $('.supplier-filters #cc-suppliers_filter-by-price').on('input',function(){
      var inpVal = $(this).val();
      $(this).closest('.filter-option').find('#price-rangeSelected').text(formatPrice(inpVal)+'%');
      if(inpVal!='' && Number(inpVal)>0){
        window.supplierFilters.applied['price'] = Number(inpVal);
      }else{
        delete window.supplierFilters.applied['price'];
      }
      $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered');
    });

    $('.supplier-filters #cc-suppliers_filter-by-name').on('input',function(){
      var inpVal = $(this).val();
      if(inpVal!=''){
        window.supplierFilters.applied['name'] = inpVal;
      }else{
        delete window.supplierFilters.applied['name'];
      }
      $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered');
    });

    $('.supplier-filters').on('change', '#cc-supplier_filter-by-region', function(){
      var inpVal = $(this).val();
      if(inpVal!=''){
        window.supplierFilters.applied['region'] = inpVal;
      }else{
        delete window.supplierFilters.applied['region'];
      }
      $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered');
    });

    $('#suppliers_box .wk-table-product-list').on('CC_UpdateListFiltered',function(e,isInitial){
      var trDisplay = $(window).width() > 767 ? 'table-row' : 'inline-block';
      var list = window.supplierFilters.list;
      var applied = window.supplierFilters.applied;
      if(list !== undefined){
        var wFilters = list.filter(function(item) {
          delete item['currCondition'];
          if(!item.hasQty){
            return false;
          }
          for (var key in applied) {
            if(key=='name'){
              if(!(item[key].toLowerCase().indexOf(applied[key].toLowerCase())!==-1))
                return false;
            }else if(key=='price'){
              if(Number(item[key]) > Number(applied[key]))
                return false;
            }else if(key=='deliveryDays'){
              var inDays = false;
              item['conditions'].forEach(function(condition){
                if(applied[key] >= condition.days_sp){
                  inDays = true;
                }
              });
              if(!inDays){
                return inDays;
              }
            }else if(key=='baseid'){
              if(!applied[key].includes(item[key]))
                return false;
            }else if(key=='region'){
              var inRegion = false;
              item['conditions'].forEach(function(condition){
                if(applied[key] == condition.region_id){
                  item['currCondition'] = condition;
                  inRegion = true;
                }
              });
              if(!inRegion){
                return inRegion;
              }
            }else if(key=='stamps'){
              var exists = false;
              item[key].forEach(function(stamp){
                if(applied[key].includes(stamp.id)){
                  exists = true;
                }
              });
              if(!exists){
                return exists;
              }
            }else{
              if(item[key] === undefined || item[key] != applied[key])
                return false;
            }
          }
          return true;
        });
        if(applied['baseid'] && applied['baseid'].length){
          $('#suppliers_box .wk-table-product-list tbody>tr#no-suppliers>td').text($.mage.__("There are no suppliers with the filters you have selected"));
        }
        if(wFilters.length){
          $('#suppliers_box .wk-table-product-list tbody>tr').hide();
          wFilters.forEach(function(row){
            var _row = $('#suppliers_box .wk-table-product-list tbody>tr[data-sellerid="'+row.id+'"]');
            var hassp = 0,
                bdays = false,
                bdaysT = 'Seleccione una región',
                found = false;
            if(row.currCondition){
              found = true;
              hassp = Number(row.currCondition.has_storepickup);
              bdays = Number(row.currCondition.days_sp);
              bdaysT = bdays+' día'+(bdays>1 ? 's' : '')+' hábil'+(bdays>1 ? 'es' : '');
            }
            _row.find('.wk-ap-delivery-days').data('deliverydays', bdays);
            _row.find('.wk-ap-delivery-days > .bdays').text(bdaysT);
            _row.find('.wk-ap-delivery-days > .hassp').css('display', found ? 'block' : 'none');
            if(hassp && found){
              _row.find('.wk-ap-delivery-days .hassp i').removeClass('ccicon-close');
            }else{
              _row.find('.wk-ap-delivery-days .hassp i').addClass('ccicon-close');
            }
            _row.css('display', trDisplay);
          });
          $('input[name="suppliers_id_select_all"]').prop('disabled',false);
        }else{
          $('input[name="suppliers_id_select_all"]').prop('disabled',true);
          $('#suppliers_box .wk-table-product-list tbody>tr').hide();
          $('#suppliers_box .wk-table-product-list tbody>tr#no-suppliers').css('display', trDisplay);
        }
        $('input[name="suppliers_id_selected"], input[name="suppliers_id_select_all"]').prop('checked',false).trigger('change');
        if(!isInitial){
          updatePriceBox(wFilters);
        }
      }
    });
    $('#suppliers_box .wk-table-product-list').trigger('CC_UpdateListFiltered',[true]);

    $(document).on('change', 'input[name="suppliers_id_select_all"]', function(){
      var input = $('#suppliers_box .wk-table-product-list tbody>tr:visible input[name="suppliers_id_selected"]');
      if($(this).prop('checked')){
        input.prop('checked',true).trigger('change');
      }else{
        input.prop('checked',false).trigger('change');
      }
    });

    function orderByPrice(a, b){
      if(a.price > b.price) return 1;
      if(a.price < b.price) return -1;
      return 0;
    }

    function updatePriceBox(list){
      var l = list.length;
      if(l){
        afterLabel.text(afterLabel.data('default'));
        $('.product-info-price-wk .minimum-price').show();
        $('.product-info-price-wk .minimum-price .price').text(formatPrice(list[0].price)+'%');
        if(list.length>1){
          if(list[0].price !== list[l-1].price){
            $('.product-info-price-wk .maximum-price').show();
            $('.product-info-price-wk .maximum-price .price').text(formatPrice(list[l-1].price)+'%');
          }else{
            $('.product-info-price-wk .maximum-price').hide();
          }
        }else{
          $('.product-info-price-wk .maximum-price').hide();
        }
      }else{
        $('.product-info-price-wk .maximum-price').hide();
        $('.product-info-price-wk .minimum-price').hide();
        afterLabel.text($.mage.__('Product not available'));
      }
    }

    function formatPrice(price){
      return Number(cleanString(price)).toFixed(1).toString().replace(".", ",");
    }

    function cleanString(string){
      if(string !== undefined && string !== null)
        return string.toString().replace(",", ".").replace(/[^0-9\.]+/g,"");
    }

    function initStampsFilter(){
      var list = window.supplierFilters.list;
      list.forEach(function(supplier){
        if(supplier.stamps.length){
          $('.cc-stamps-filter').show();
        }
        supplier.stamps.forEach(function(stamp){
          if(!$('.cc-stamps-filter .stamps-box #stamps-filter-'+stamp.id).length){
            var input = '<input type="checkbox" name="stamps-filter" id="stamps-filter-'+stamp.id+'" value="'+stamp.id+'">',
                span = '<span>'+stamp.name+'</span>',
                label = '<label for="stamps-filter-'+stamp.id+'">'+input+span+'</label>';
            $('.cc-stamps-filter .stamps-box').append(label);
          }
        });
      });
    }

    function initRegionFilter(){
      var regions = window.chileanRegions;
      var select = $('.cc-region-filter #cc-supplier_filter-by-region');
      if(Array.isArray(regions)){
        select.html('');
        var optionBase = '<option value="" selected>---</option>';
        select.append(optionBase);
        regions.forEach(function(region){
          var option = '<option value="'+region.id+'">'+region.name+'</option>';
          select.append(option);
        });
      }
    }

    function initDDaysFilter(){
      var min = 1, max = 1;
      var list = window.supplierFilters.list;
      list.forEach(function(supplier){
        if(supplier.conditions.length){
          $('.supplier-filters .cc-deliverydays-filter').show();
        }
        supplier.conditions.forEach(function(condition){
          var daysSp = Number(condition.days_sp);
          if(daysSp < min){
            min = daysSp;
          }
          if(daysSp > max){
            max = daysSp;
          }
        });
      });
      var inp = $('.supplier-filters .cc-deliverydays-filter input[type="range"]');
      $('.supplier-filters .cc-deliverydays-filter .range-min').text(min);
      $('.supplier-filters .cc-deliverydays-filter .range-max').text(max);
      inp.attr('min',min);
      inp.attr('max',max);
      inp.val(max);
      $('.supplier-filters .cc-deliverydays-filter #days-rangeSelected').text(max);
    }

    function initPriceSlider(){
      var min, max;
      var list = window.supplierFilters.list;
      if(list.length){
        min = list[0].price;
        max = list[0].price;
      }
      list.forEach(function(product){
        if(product.hasQty){
          if(product.price < min){
            min = product.price;
          }
          if(product.price > max){
            max = product.price;
          }
        }
      });
      var inp = $('.supplier-filters .cc-price-filter input[type="range"]');
      $('.supplier-filters .cc-price-filter .range-min').text(formatPrice(min)+'%');
      $('.supplier-filters .cc-price-filter .range-max').text(formatPrice(max)+'%');
      inp.attr('min',min);
      inp.attr('max',max);
      inp.val(max);
      $('.supplier-filters .cc-price-filter #price-rangeSelected').text(formatPrice(max)+'%');
    }

  });
});
