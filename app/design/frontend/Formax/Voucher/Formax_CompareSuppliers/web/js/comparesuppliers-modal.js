define([
    "jquery", "Magento_Ui/js/modal/modal", "mage/translate"
], function($){
    var CompareSuppliersModal = {
        initCompareModal: function(config, element) {
            var $target = $('#comparesuppliers-popup');
            var $element = $('#viewSuppliersComparator');
            var options = {
              responsive: true,
              innerScroll: true,
              modalClass: 'comparesuppliers-modal',
              title: 'Comparador de proveedores',
              closeText: 'Cerrar',
              buttons: []
            };
            $target.modal(options);

            var inputName = 'input[name="suppliers_id_selected"]';
            var inputAll = 'input[name="suppliers_id_select_all"]';
            $(document).on('change', inputName, function(){
              if($(inputName+':checked').length > 1){
                $('#viewSuppliersComparator').removeClass('disabled');
              }else{
                $('#viewSuppliersComparator').addClass('disabled');
              }
            });
            $(inputName+','+inputAll).attr('disabled',false);
            $('#viewSuppliersComparator').click(function(){
              console.log($element);
              $target.find('.product-info .product-sku').html($('.product-info-main .product.attribute.sku').clone());
              $target.find('.product-info .product-name').html($('.product-info-main .page-title-wrapper.product').clone());
              $target.find('.product-photo').html($('.product.media .fotorama__stage .fotorama__img').clone().first());
              let ids = [];
              $(inputName+':checked').each(function(){
                ids.push($(this).val());
              });
              if(ids.length){
                $.ajax({
                  url: config.url,
                  showLoader: true,
                  type: 'POST',
                  dataType: 'json',
                  data: {supplierIds: ids}
                })
                .done(function( data ) {
                  $('.suppliers-comparisontable>thead, .suppliers-comparisontable>tbody').empty();
                  $('.suppliers-comparisontable>thead').append('<tr><th class="invisible">&nbsp;</th></tr>');
                  data.suppliers.forEach(function(supplier, i){
                    var list = window.supplierFilters.list;
                    var _supplier = list.filter(function(obj){
                      return obj.id === supplier.id;
                    });
                    $('.suppliers-comparisontable>thead>tr').append('<th>'+_supplier[0].name+'</th>');
                    var index = 0;
                    if(i==0){
                      $('.suppliers-comparisontable>tbody').append('<tr id="comparison-price">'+
                        '<th>'+$.mage.__('Porcentaje Neto')+'</th></tr>');
                      $('.suppliers-comparisontable>tbody').append('<tr id="comparison-regions">'+
                        '<th>'+$.mage.__('Coverage regions')+'</th></tr>');
                    }
                    $('.suppliers-comparisontable #comparison-price').append(
                      '<td><span class="price">'+formatPrice(_supplier[0].price)+'</span></td>');
                    $('.suppliers-comparisontable #comparison-regions').append('<td>'+supplier.region_codes+'</td>');

                    for(attr in supplier.attrs){
                      if(i==0){
                        $('.suppliers-comparisontable>tbody').append('<tr id="comparison-row-'+index+'">'+
                          '<th>'+supplier.attrs[attr].label+'</th></tr>');
                      }
                      $('.suppliers-comparisontable #comparison-row-'+index).append(
                        '<td>'+supplier.attrs[attr].value+'</td>');
                      index++;
                    }
                  });
                })
                .error(function( err ) {
                  console.log('error', err);
                });
              }
              $target.modal('openModal');
            });

            $(document).on('click','.print-suppliercomparison', function(){
              var suppWin = window.open('', 'PRINT', 'height='+$(window).height()+',width='+$(window).width());
              suppWin.document.write('<html><head><title>' + document.title  + '</title>');
              suppWin.document.write('<link rel="stylesheet" href="'+$('link[href*="dccp.css"]').attr('href')+'" type="text/css" />');
              suppWin.document.write('</head><body>');
              suppWin.document.write('<h1>' + document.title  + '</h1>');
              suppWin.document.write($('#comparesuppliers-popup').html());
              suppWin.document.write('</body></html>');
              suppWin.document.close();
              suppWin.focus();
              suppWin.print();
              //suppWin.close();
              return true;
            })
        },

    };

    function formatPrice(price){
      var result = '-';
      if(!isNaN(price)){
        result = Number(price).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+"%";
      }
      return result;
    }

    return {
        'comparesuppliers-modal': CompareSuppliersModal.initCompareModal
    };
}
);