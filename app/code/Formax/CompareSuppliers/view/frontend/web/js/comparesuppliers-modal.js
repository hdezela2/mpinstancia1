define([
        'jquery',
        'Magento_Catalog/js/price-utils',
        'Magento_Ui/js/modal/modal',
        'mage/translate'
    ],
    function ($, priceUtils)
    {
        const CompareSuppliersModal =
        {
            initCompareModal: function (config, element)
            {
                const inputName = 'input[name="suppliers_id_selected"]';
                const inputAll = 'input[name="suppliers_id_select_all"]';

                if (config.is_compare_enabled === '0') {
                    $(inputName).addClass('disabled');
                    $(inputName).attr('disabled', true);
                    $(inputAll).attr('disabled', true);
                    return;
                }

                const $target = $('#comparesuppliers-popup');
                const $element = $('#viewSuppliersComparator');
                const options =
                {
                    responsive: true,
                    innerScroll: true,
                    modalClass: 'comparesuppliers-modal',
                    title: 'Comparador de proveedores',
                    closeText: 'Cerrar',
                    buttons: []
                };
                $target.modal(options);


                $(document).on('change', inputName, function ()
                {
                    if ($(inputName + ':checked').length > 1)
                    {
                        $element.removeClass('disabled');
                    }
                    else
                    {
                        $element.addClass('disabled');
                    }
                });
                $(inputName + ',' + inputAll).attr('disabled', false);
                $element.on('click',function ()
                {
                    $target.find('.product-info .product-sku').html($('.product-info-main .product.attribute.sku').clone());
                    $target.find('.product-info .product-name').html($('.product-info-main .page-title-wrapper.product').clone());
                    $target.find('.product-photo').html($('.product.media .fotorama__stage .fotorama__img').clone().first());
                    let ids = [];
                    $(inputName + ':checked').each(function ()
                    {
                        ids.push($(this).val());
                    });
                    if (ids.length)
                    {
                        $.ajax
                        ({
                            url: config.url,
                            showLoader: true,
                            type: 'POST',
                            dataType: 'json',
                            data: {supplierIds: ids}
                        })
                        .done(function (data)
                        {
                            $('.suppliers-comparisontable>thead, .suppliers-comparisontable>tbody').empty();
                            $('.suppliers-comparisontable>thead').append('<tr><th class="invisible">&nbsp;</th></tr>');
                            data.suppliers.forEach(function (supplier, i)
                            {
                                const list = window.supplierFilters.list;
                                var name = "";
                                const _supplier = list.filter(function (obj)
                                {
                                    return obj.id === supplier.id;
                                });
                                if(config.website == 'computadores' || config.website == 'computadores202101' || config.website == 'computadores202201')
                                {
                                    if(!supplier.abtester){
                                        if(supplier.sellerAlias){
                                            name = 'Proveedor N&deg; ' + supplier.sellerAlias;
                                        } else {
                                            name = 'Proveedor Oculto';
                                        }
                                    } else {
                                        name = _supplier[0].name  + '<span>RUT: ' + supplier.attrs.legal_rep.rut_id + '</span>';
                                    }
                                    
                                    $('.suppliers-comparisontable>thead>tr').append('<th>' + name + '</th>');
                                }
                                else
                                {
                                    if(!supplier.abtester){
                                        if(supplier.sellerAlias){
                                            name = 'Proveedor N� ' + supplier.sellerAlias;
                                        } else {
                                            name = 'Proveedor Oculto';
                                        }
                                    } else {
                                        name = _supplier[0].name;
                                    }
                                    
                                    $('.suppliers-comparisontable>thead>tr').append('<th>' + name + '</th>');
                                }
                                let index = 0;
                                if (i === 0)
                                {
                                    const tbody = $('.suppliers-comparisontable>tbody');
                                    if (config.website == 'emergencias202109') {
                                        $('.suppliers-comparisontable>tbody').append('<tr id="comparison-rut"> <th>' + $.mage.__('RUT') + '</th></tr>');
                                        $('.suppliers-comparisontable>tbody').append('<tr id="comparison-business-name"> <th>' + $.mage.__('Business Name') + '</th></tr>');
                                    }
                                    tbody.append('<tr id="comparison-price"> <th>' + $.mage.__('Net price') + '</th></tr>');
                                    tbody.append('<tr id="comparison-regions"> <th>' + $.mage.__('Coverage regions') + '</th></tr>');
                                }

                                if (config.website == 'emergencias202109') {
                                    $('.suppliers-comparisontable #comparison-rut').append('<td>' + supplier.wkv_dccp_rut + '</td>');
                                    $('.suppliers-comparisontable #comparison-business-name').append('<td>' + supplier.wkv_dccp_business_name + '</td>');
                                }

                                if(config.website == 'computadores' || config.website == 'computadores202101' || config.website == 'computadores202201')
                                {
                                    if (!_supplier[0].oldPrice)
                                    {
                                        $('.suppliers-comparisontable #comparison-price').append('<td><span class="price">' + formatPrice(_supplier[0].price) + '</span>');
                                    }
                                    else
                                    {
                                        $('.suppliers-comparisontable #comparison-price')
                                        .append
                                        (
                                            '<td><span class="specialPrice">' + formatPrice(_supplier[0].price) + '</span><span class="oldPrice">' + $.mage.__('Before ') + formatPrice(_supplier[0].oldPrice) + '</span>'
                                        );
                                    }
                                    if (config.website == 'emergencias202109') {
                                        $('.suppliers-comparisontable #comparison-regions').append('<td>' + supplier.region_codes + '</td>');
                                    } else {
                                        $('.suppliers-comparisontable #comparison-regions').append('<td>' + $.mage.__('National Coverage') + '</td>');
                                    }
                                }
                                else
                                {
                                    $('.suppliers-comparisontable #comparison-price').append('<td><span class="price">' + formatPrice(_supplier[0].price) + '</span></td>');
                                    $('.suppliers-comparisontable #comparison-regions').append('<td>' + supplier.region_codes + '</td>');
                                }

                                if(config.website == 'mobiliario')
                                {
                                    let zones = Object.entries(supplier.macrozones);
                                    let weightRanges = ['0-100', '100-300', '300-5000'];
                                    if (i === 0)
                                    {
                                        $('.suppliers-comparisontable>tbody').append('<tr id="urbanas"><th>Valores despacho por peso kg/zona urbana</tr>');

                                        for(let j=0; j<zones.length; j++)
                                        {
                                            $('.suppliers-comparisontable #urbanas>th').append('<br><span class="density-weight">Macrozona ' + zones[j][0] + '</span>');
                                        }

                                        $('.suppliers-comparisontable>tbody').append('<tr id="rurales"><th>Valores despacho por peso kg/zona rural</tr>');
                                        for(let j=0; j<zones.length; j++)
                                        {
                                            $('.suppliers-comparisontable #rurales>th').append('<br><span class="density-weight">Macrozona ' + zones[j][0] + '</span>');
                                        }
                                    }

                                    $('.suppliers-comparisontable #urbanas').append('<td id="urbanas-values-' + supplier.id + ' " style="padding: 0px;"><table id="tabla-urbana-' + supplier.id + '" style="border: 0px;"><tr id="urbana-ranges-' + supplier.id + '"></tr></table></td>');
                                    for(let j=0; j<weightRanges.length; j++)
                                    {
                                        let target = '.suppliers-comparisontable #urbana-ranges-' + supplier.id;
                                        let id = "urbana-" + weightRanges[j] + "-" + supplier.id;
                                        $(target).append('<td id=' + id + ' style="padding:0px; border:0px"><span><bold> ' + weightRanges[j] + '</bold></span><br/></td>');
                                    }
                                    $('.suppliers-comparisontable #urbanas-values').append('<br/>');

                                    $('.suppliers-comparisontable #rurales').append('<td id="rurales-values- ' + supplier.id + '"  style="padding: 0px;"><table id="tabla-rural-' + supplier.id + '" style="border: 0px"><tr id="rural-ranges-' + supplier.id + '"></tr></table></td>');
                                    for(let j=0; j<weightRanges.length; j++)
                                    {
                                        let target = '.suppliers-comparisontable #rural-ranges-' + supplier.id;
                                        let id = "rural-" + weightRanges[j] + "-" + supplier.id;
                                        $(target).append('<td id=' + id + ' style="padding:0px; border:0px"><span><bold> ' + weightRanges[j] + '</bold></span><br/></td>');
                                    }
                                    $('.suppliers-comparisontable #rurales-values').append('<br/>');

                                    for(let i=0; i<zones.length; i++)
                                    {
                                        let current = zones[i][1];
                                        for(let j=0; j<current.length; j++)
                                        {
                                            if(current[j].weight_range == '0-100')
                                            {
                                                let urbanaTarget = '.suppliers-comparisontable #urbana-0-100-' + supplier.id;
                                                $(urbanaTarget).append("$" + current[j].urban_factor + " <br/>");
                                                let ruralTarget = '.suppliers-comparisontable #rural-0-100-' + supplier.id;
                                                $(ruralTarget).append("$" + current[j].rural_factor + " <br/>");
                                            }
                                            if(current[j].weight_range == '100-300')
                                            {
                                                let urbanaTarget = '.suppliers-comparisontable #urbana-100-300-' + supplier.id;
                                                $(urbanaTarget).append("$" + current[j].urban_factor + " <br/>");
                                                let ruralTarget = '.suppliers-comparisontable #rural-100-300-' + supplier.id;
                                                $(ruralTarget).append("$" + current[j].rural_factor + " <br/>");
                                            }
                                            if(current[j].weight_range == '300-5000')
                                            {
                                                let urbanaTarget = '.suppliers-comparisontable #urbana-300-5000-' + supplier.id;
                                                $(urbanaTarget).append("$" + current[j].urban_factor + " <br/>");
                                                let ruralTarget = '.suppliers-comparisontable #rural-300-5000-' + supplier.id;
                                                $(ruralTarget).append("$" + current[j].rural_factor + " <br/>");
                                            }
                                        }
                                    }
                                }

                                supplier.regional_conditions.forEach(function (condition)
                                {
                                    const conditionName = condition.name.toLowerCase().replace(/ /g, '-');
                                    if (i === 0)
                                    {
                                        if(config.website != 'mobiliario')
                                        {
                                            $('.suppliers-comparisontable>tbody').append('<tr id="comparison-' + conditionName + '">' + '<th>' + $.mage.__('Dispatch Value: %1').replace('%1', condition.name) + '</th></tr>');
                                            condition.values.forEach(function (weight)
                                            {
                                                $('.suppliers-comparisontable #comparison-' + conditionName + '>th').append('<br><span class="density-weight">' + weight.name + '</span>');
                                            });
                                        }
                                    }
                                    if(config.website != 'mobiliario')
                                    {
                                        $('.suppliers-comparisontable #comparison-' + conditionName).append('<td class="row-density-values" id="row-' + conditionName + '-' + i + '">&nbsp;</td>');
                                    }

                                    condition.values.forEach(function (weight)
                                    {
                                        $('.suppliers-comparisontable #comparison-' + conditionName + '>#row-' + conditionName + '-' + i).append('<br>' + formatPrice(weight.value));
                                    });
                                });
                                for (attr in supplier.attrs)
                                {
                                    if (i === 0)
                                    {
                                        $('.suppliers-comparisontable>tbody').append('<tr id="comparison-row-' + index + '">' + '<th>' + supplier.attrs[attr].label + '</th></tr>');
                                    }
                                    if(attr == 'legal_rep')
                                    {
                                        const value = supplier.abtester == false ? 'Representante Oculto' : supplier.attrs[attr].value;
                                        $('.suppliers-comparisontable #comparison-row-' + index).append('<td>' + value + '</td>');
                                    }
                                    else
                                    {
                                        $('.suppliers-comparisontable #comparison-row-' + index).append('<td>' + supplier.attrs[attr].value + '</td>');
                                    }
                                    index++;
                                }
                            });
                        })
                        .error(function (err)
                        {
                            console.log('error', err);
                        });
                    }
                    $target.modal('openModal');
                });

                $(document).on('click', '.print-suppliercomparison', function ()
                {
                    const suppWin = window.open('', 'PRINT', 'height=' + $(window).height() + ',width=' + $(window).width());
                    suppWin.document.write('<html><head><title>' + document.title + '</title>');
                    suppWin.document.write('<link rel="stylesheet" href="' + $('link[href*="dccp.css"]').attr('href') + '" type="text/css" />');
                    suppWin.document.write('</head><body>');
                    suppWin.document.write('<h1>' + document.title + '</h1>');
                    suppWin.document.write($('#comparesuppliers-popup').html());
                    suppWin.document.write('</body></html>');
                    suppWin.document.close();
                    suppWin.focus();
                    suppWin.print();
                    return true;
                })
            },
        };

        /**
         * Format Price
         * @param price
         * @returns {string}
         */
        function formatPrice(price)
        {
            const element = $('div#product_list');
            const priceFormat =
            {
                decimalSymbol: element.attr('data-decimal'),
                groupLength: 3,
                groupSymbol: element.attr('data-group'),
                integerRequired: false,
                pattern: element.attr('data-symbol') + '%s',
                precision: element.attr('data-precision'),
                requiredPrecision: element.attr('data-precision')
            };
            return priceUtils.formatPrice(price, priceFormat);
        }

        return {'comparesuppliers-modal': CompareSuppliersModal.initCompareModal};
    }
);
