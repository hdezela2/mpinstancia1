<?php
declare(strict_types=1);

namespace Formax\CompareSuppliers\Block;

use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;

class Block extends Template
{
	const ENVASADO = 'envasado';

	/**
	 * @var Registry
	 */
	private $registry;

	/**
	 * @var CategoryRepository
	 */
	private $categoryRepository;

	/**
	 * Feature constructor.
	 * @param Template\Context $context
	 * @param Registry $registry
	 * @param CategoryRepository $categoryRepository
	 */
	public function __construct(
		Template\Context $context,
		Registry $registry,
		CategoryRepository $categoryRepository
	) {
		$this->registry = $registry;
		parent::__construct($context, []);
		$this->categoryRepository = $categoryRepository;
	}

	/**
	 * @return int
	 * @throws NoSuchEntityException
	 */
	public function isFeatureEnable(): int
	{
		$categoryIds = $this->registry->registry('current_product')->getData('category_ids');
		foreach ($categoryIds as $categoryId) {
			$category = $this->categoryRepository->get($categoryId, $this->_storeManager->getStore()->getId());
			$categoryName = strtolower($category->getName());
			if (str_contains($categoryName, self::ENVASADO)) {
				return 0;
			}
		}

		return 1;
	}

	public function getWebsiteCode(): string
	{
		return $this->_storeManager->getWebsite()->getCode();
	}
}