<?php
namespace Formax\CompareSuppliers\Utils;

use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;

class Constants
{
	/**
	 * @var array
	 */
	const ATTRIBUTES =
	[
	    	'base' => [
	      		'apc' => 'wkv_dccp_apc',
	      		'mda' => 'wkv_dccp_mda',
	      		'mda_p' => 'wkv_dccp_phone_help_desk',
	      		'percent' => 'wkv_dccp_gb_discount'
	    	],
	    	'convenio_aseo' => [
	      		'minterm' => 'wkv_dccp_vendor_minterm',
	      		'maxterm' => 'wkv_dccp_vendor_maxterm',
	      		'period' => 'wkv_dccp_replac_period',
	      		'shoptrain' => 'wkv_dccp_shop_train'
	    	],
	    	'convenio_voucher' => [
	      		'regorigen' => 'wkv_region_origen',
	      		'ewallet' => 'wkv_v_ewallet',
				'virtualcard' => 'wkv_v_virtual_card',
				'mobilecard' => 'wkv_v_mobile_card',
				'plasticcard' => 'wkv_v_plastic_card'
			],
			'emergencias' => [
				'maxterm' => 'wkv_dccp_vendor_maxterm',
				'asist' => 'wkv_dccp_free_tech_asist',
				'asist_phone' => 'wkv_dccp_asist_tech_phone',
				'url_service' => 'wkv_dccp_services_url',
			],
            'emergencias202109' => [
//                'maxterm' => 'wkv_dccp_vendor_maxterm',
//                'asist' => 'wkv_dccp_free_tech_asist',
                'asist_phone' => 'wkv_dccp_asist_tech_phone',
//                'url_service' => 'wkv_dccp_services_url',
                 'url_service' => 'wkv_web_page',
//                'date_of_entry' => 'wkv_dccp_date_of_entry',
                'lega_rep_name' => 'wkv_dccp_lega_rep_name',
                'lega_rep_phone' => 'wkv_replegal_phone',
                'lega_rep_maik' => 'wkv_replegal_mail',
                'lega_rep_maik' => 'wkv_replegal_mail',
            ],
        'aseo2022' => [
//                'maxterm' => 'wkv_dccp_vendor_maxterm',
//                'asist' => 'wkv_dccp_free_tech_asist',
//                'url_service' => 'wkv_dccp_services_url',
//                'date_of_entry' => 'wkv_dccp_date_of_entry',
            'lega_rep_name' => 'wkv_dccp_lega_rep_name',
            'lega_rep_phone' => 'wkv_replegal_phone',
            'lega_rep_maik' => 'wkv_replegal_mail',
            'lega_rep_maik' => 'wkv_replegal_mail',
        ],
	        'computadores' => [
	            'legal_rep' => 'wkv_dccp_lega_rep_name',
	        // --- no se muestran en el comparador
	        //            'address' =>'wkv_dccp_address',
	        //            'commune' =>'wkv_dccp_commune',
	        //            'regio' =>'wkv_dccp_region'
	        ],
            'computadores202101' => [
                'legal_rep' => 'wkv_dccp_lega_rep_name',
                // --- no se muestran en el comparador
                //            'address' =>'wkv_dccp_address',
                //            'commune' =>'wkv_dccp_commune',
                //            'regio' =>'wkv_dccp_region'
            ],
            'computadores202201' => [
                'legal_rep' => 'wkv_dccp_lega_rep_name',
                // --- no se muestran en el comparador
                //            'address' =>'wkv_dccp_address',
                //            'commune' =>'wkv_dccp_commune',
                //            'regio' =>'wkv_dccp_region'
            ],
	        'combustibles' => [
	            'legal_rep' => 'wkv_dccp_lega_rep_name',
	            // --- Este convenio no posee comparador -
	        ],
            'combustibles202110' => [
                'legal_rep' => 'wkv_dccp_lega_rep_name',
                // --- Este convenio no posee comparador -
            ],
	        'mobiliario' => [
	            'legal_rep' => 'wkv_dccp_lega_rep_name',
	            'assembly_price_low' => 'wkv_assembly_price_low',
	            'assembly_price_med' => 'wkv_assembly_price_med',
	            'store_pickup' => 'enable_storepickup'
	        ],
            'escritorio' => [
                'wkv_dccp_rut' => 'wkv_dccp_rut',
                'wkv_dccp_business_name' => 'wkv_dccp_business_name',
                'legal_rep'     => 'wkv_dccp_lega_rep_name',
                //    'razon_social'  => 'wkv_dccp_business_name'
            ],
			'gas' => [
				'wkv_dccp_business_name' => 'wkv_dccp_business_name',
				'wkv_dccp_rut' => 'wkv_dccp_rut',
				'wkv_dccp_name' => 'wkv_dccp_name',
				'wkv_dccp_phone' => 'wkv_dccp_phone',
				'wkv_dccp_email' => 'wkv_dccp_email',
				'wkv_dccp_ticket_duration' => 'wkv_dccp_ticket_duration',
				'wkv_dccp_put_pond_free' => 'wkv_dccp_put_pond_free',
				'wkv_dccp_pick_pond_free' => 'wkv_dccp_pick_pond_free',
			]
  	];
}
