<?php

namespace Formax\CompareSuppliers\Controller\Information;

use Cleverit\ComputadoresRenewal\Constants as ComputadoresRenewalConstants;
use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface;
use Intellicore\ComputadoresRenewal2\Constants as ComputadoresRenewal2Constants;
use Formax\GreatBuy\Helper\Data as GreatBuyHelper;
use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Catalog\Model\ProductFactory;
use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Summa\CustomerData\Block\CustomerData;
use Summa\MacrozoneFactors\Helper\Data;

class Index extends Action{

    protected $attrs = \Formax\CompareSuppliers\Utils\Constants::ATTRIBUTES;

    /**
     * @var \Formax\RegionalCondition\Block\RegionalCondition\Matriz
     */
    protected $matriz;
    /**
     * @var \Formax\RegionalCondition\Helper\Data
     */
    protected $regionalHelper;
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customer;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resourceConnection;
    /**
     * @var \Formax\AgreementInfo\Helper\Store
     */
    protected $_storeHelper;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    protected $macrozoneHelper;

    protected $customerDataBlock;

    /** @var GreatBuyHelper */
    protected $greatBuyHelper;

    /**
     * @var DispatchPriceManagementInterface
     */
    protected $dispatchPriceManagement;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @param Context                                                  $context
     * @param \Formax\RegionalCondition\Block\RegionalCondition\Matriz $matriz
     * @param \Formax\RegionalCondition\Helper\Data                    $regionalHelper
     * @param \Magento\Customer\Model\CustomerFactory                  $customer
     * @param \Magento\Framework\Controller\Result\JsonFactory         $resultJsonFactory
     * @param \Magento\Framework\App\ResourceConnection                $resourceConnection
     * @param \Formax\AgreementInfo\Helper\Store                       $store
     * @param \Magento\Framework\App\Config\ScopeConfigInterface       $scopeConfig
     * @param Data                                                     $macrozoneHelper
     * @param CustomerData                                             $customerDataBlock
     * @param GreatBuyHelper                                           $greatBuyHelper
     * @param DispatchPriceManagementInterface                         $dispatchPriceManagement
     * @param ProductFactory                                           $productFactory
     */
    public function __construct(
        Context $context,
        \Formax\RegionalCondition\Block\RegionalCondition\Matriz $matriz,
        \Formax\RegionalCondition\Helper\Data $regionalHelper,
        \Magento\Customer\Model\CustomerFactory $customer,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Formax\AgreementInfo\Helper\Store $store,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        Data $macrozoneHelper,
        CustomerData $customerDataBlock,
        GreatBuyHelper $greatBuyHelper,
        DispatchPriceManagementInterface $dispatchPriceManagement,
        ProductFactory $productFactory
    ) {
        $this->matriz = $matriz;
        $this->regionalHelper = $regionalHelper;
        $this->_customer = $customer;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_resourceConnection = $resourceConnection;
        $this->_storeHelper = $store;
        $this->scopeConfig = $scopeConfig;
        $this->macrozoneHelper = $macrozoneHelper;
        $this->customerDataBlock = $customerDataBlock;
        $this->greatBuyHelper = $greatBuyHelper;
        $this->dispatchPriceManagement = $dispatchPriceManagement;
        $this->productFactory = $productFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $post = $this->getRequest()->getParams();
        $suppliers = [];
        $ids = isset($post['supplierIds']) ? $post['supplierIds'] : [];
        $skuProduct = isset($post['skuProduct']) ? $post['skuProduct'] : '';
        $regionId = $post['regionId'] ?? 0;
        $websiteCode = $this->_storeHelper->getWebsiteCode();
        $densities = [];
        $weights = [];

        //emergencias
        if ($websiteCode != \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE
            && $websiteCode != ConstantsEmergency202109::WEBSITE_CODE
            && $websiteCode != \Summa\ComputadoresSetUp\Helper\Data::WEBSITE_CODE
            && $websiteCode != ComputadoresRenewalConstants::WEBSITE_CODE
            && $websiteCode != ComputadoresRenewal2Constants::WEBSITE_CODE
            && $websiteCode != \Summa\EscritorioSetUp\Helper\Data::WEBSITE_CODE )
        {
            $densities = $this->matriz->getDensities();
            $weights = $this->matriz->getWeight();
        }

        foreach($ids as $id)
        {
            $tableRates = $this->regionalHelper->getTableRatesbySeller($id);
            $regionalConditions = $this->regionalHelper->getRegionalConditionbySeller($id);
            $seller = $this->getSeller($id);

            if ($seller->getIsActive() == 1) {
                $obj =  [
                    'id' => (int)$id,
                    'attrs' => [],
                    'region_codes' => '-',
                'regional_conditions' => [],
                'dispatch_price' => 0
                ];
                if (isset($regionalConditions['region_id']) && count($regionalConditions['region_id']))
                {
                    if ($websiteCode == \Intellicore\EmergenciasRenewal\Constants::WEBSITE_CODE || $websiteCode == \Linets\AseoRenewalSetup\Model\AseoRenewalConstants::WEBSITE_CODE) {
                        $obj['region_codes'] = $regionalConditions['default_name'];
                    } else {
                        $obj['region_codes'] = $this->getRegionCodes($regionalConditions['region_id']);
                    }
                }

                foreach($densities as $density)
                {
                    $densityValues = [];
                    foreach($weights as $weight)
                    {
                        $_dValue = '-';
                        if (count($tableRates) && isset($tableRates['shippingcost'][(String)$density->getId()][(String)$weight->getId()]))
                        {
                            $_dValue = $tableRates['shippingcost'][(String)$density->getId()][(String)$weight->getId()];
                        }

                        $densityValues[] =
                        [
                            'name' => $weight->getDimensionLabel(),
                            'value' => $_dValue
                        ];
                    }

                    $obj['regional_conditions'][] = [
                        'name' => $density->getName(),
                        'values' => $densityValues
                    ];
                }

                if (isset($this->attrs[$websiteCode]))
                {
                    foreach($this->attrs[$websiteCode] as $key => $attr)
                    {
                        $_attr = $seller->getAttribute($attr);
                        if (!is_null($_attr))
                        {
                            $obj['attrs'][$key] =
                            [
                                'label' => $_attr->getStoreLabel(),
                                'value' => $this->getData($seller, $attr, $_attr)
                            ];
                        }
                    }
                }

                if ($websiteCode == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) {
                    $key = array_search($id, $regionalConditions['seller_id']);
                    $obj['attrs']['store_pickup'] =
                        [
                            'label' => 'Retiro en tienda',
                            'value' => $regionalConditions['enable_storepickup'][$key] === 0 ? 'No' : 'Si'
                        ];
                }

                if ($websiteCode == \Linets\VehiculosSetUp\Model\Constants::WEBSITE_CODE) {
                    $key = array_search($id, $regionalConditions['seller_id']);
                    $obj['attrs']['legal_rep']['rut_id'] = $this->getData($seller, 'wkv_dccp_rut');
                    $obj['attrs']['wkv_dccp_email_notify'] = [
                        'label' => 'Acepta correo electrónico de coordinador como medio para recibir notificaciones',
                        'value' => $this->getData($seller, 'wkv_dccp_email_notify')?'Si':'No'
                    ];
                    $obj['attrs']['dias_despacho'] = [
                        'label' => 'Días hábiles de despacho',
                        'value' => max($regionalConditions['maximum_delivery']) . ' días máximo'
                    ];
                    $obj['attrs']['store_pickup'] =
                        [
                            'label' => 'Retiro en tienda',
                            'value' => $regionalConditions['enable_storepickup'][$key] === 0 ? 'No' : 'Si'
                        ];
                    $obj['attrs']['store_pickup_max_days'] = [
                        'label' => 'Plazo maximo retiro en tienda',
                        'value' => $regionalConditions['enable_storepickup'][$key] === 0 ?
                            'No disponible' : max($regionalConditions['days_storepickup']) . ' días máximo'
                    ];
                    $obj['attrs']['wkv_dccp_warranty_months'] = [
                        'label' => 'Meses de Garantía Convencional Vehiulos',
                        'value' => $seller->getData('wkv_dccp_warranty_months')
                    ];
                    $obj['attrs']['wkv_dccp_warranty_km'] = [
                        'label' => 'Kilometraje de Garantía Convencional Vehículos',
                        'value' => $seller->getData('wkv_dccp_warranty_km')
                    ];
                }

            if ($websiteCode == InsumosConstants::WEBSITE_CODE) {
                $key = array_search($id, $regionalConditions['seller_id']);
                $obj['attrs']['legal_rep']['rut_id'] = $this->getData($seller, 'wkv_dccp_rut');
                $obj['attrs']['wkv_dccp_email_notify'] = [
                    'label' => 'Acepta correo electrónico de coordinador como medio para recibir notificaciones',
                    'value' => $this->getData($seller, 'wkv_dccp_email_notify')?'Si':'No'
                ];
                $obj['attrs']['store_pickup'] =
                    [
                        'label' => 'Retiro en tienda',
                        'value' => intval($regionalConditions['enable_storepickup'][$key]) === 0 ? 'No' : 'Si'
                    ];
                $obj['attrs']['wkv_pe_mz_norte'] = [
                    'label' => 'Plazo de Entrega Macrozona Norte (Días Hábiles)',
                    'value' => $seller->getData('wkv_pe_mz_norte')
                ];
                $obj['attrs']['wkv_pe_mz_centro'] = [
                    'label' => 'Plazo de Entrega Macrozona Centro (Días Hábiles)',
                    'value' => $seller->getData('wkv_pe_mz_centro')
                ];
                $obj['attrs']['wkv_pe_mz_sur'] = [
                    'label' => 'Plazo de Entrega Macrozona Sur (Días Hábiles)',
                    'value' => $seller->getData('wkv_pe_mz_sur')
                ];
                $obj['attrs']['wkv_pe_mz_austral'] = [
                    'label' => 'Plazo de Entrega Macrozona Austral (Días Hábiles)',
                    'value' => $seller->getData('wkv_pe_mz_austral')
                ];

                // Get dispatch Price
                $product = $this->productFactory->create();
                $productFactory = $product->loadByAttribute('sku', $skuProduct);
                $macrozone = $productFactory->getAttributeText('macrozona');
                $resultDispatch = $this->dispatchPriceManagement->getDispatchPrice((int)$id, (string)$macrozone,
                    (int)$productFactory->getAttributeSetId(), (int)$seller->getData('website_id'));

                $obj['dispatch_price'] = (int)($resultDispatch ? $resultDispatch->getPrice() : 0);
            }

                if (in_array($websiteCode, [
                    \Summa\ComputadoresSetUp\Helper\Data::WEBSITE_CODE,
                    ComputadoresRenewalConstants::WEBSITE_CODE,
                    ComputadoresRenewal2Constants::WEBSITE_CODE,
                    \Summa\EscritorioSetUp\Helper\Data::WEBSITE_CODE,
                ])) {
                    $obj['attrs']['legal_rep']['rut_id'] = $this->getData($seller, 'wkv_dccp_rut');
                    $valueDias = $regionalConditions['maximum_delivery'][0]; //primer elemento - todos tienen el mismo valor.
                    $valueDiastoShow = '';
                    if ($valueDias > 1) {
                        $valueDiastoShow = $valueDias . ' días máximo';
                    }elseif ( $valueDias == 1){
                        $valueDiastoShow = $valueDias . ' día máximo';
                    }
                    $obj['attrs']['utm_monto'] = [
                        'label' => 'Monto mínimo de compra',
                        'value' => (string)$this->greatBuyHelper->getMinimumUTMConfig() . ' UTM'
                    ];
                    $obj['attrs']['dias_despacho'] = [
                        'label' => 'Días hábiles de despacho',
                        'value' => $valueDiastoShow
                    ];
                }

                if ($websiteCode == \Linets\GasSetup\Constants::GAS_WEBSITE_CODE) {
                    $sellerRegionalConditions = $this->regionalHelper->getRegionalConditionsBySellerAndRegionId($id, $regionId);
                    $packingDispatchDays = $sellerRegionalConditions->getMaxDaysPackingDispatch();
                    $obj['attrs']['max_days_packing_dispatch'] = [
                        'label' => 'Plazo Maximo Despacho Envasado (Días)',
                        'value' => $packingDispatchDays . ' días hábiles'
                    ];
                    $bulkDispatchDays = $sellerRegionalConditions->getMaxDaysBulkDispatch();
                    $obj['attrs']['max_days_bulk_dispatch'] = [
                        'label' => 'Plazo Maximo Despacho Granel (Días)',
                        'value' => $bulkDispatchDays . ' días hábiles'
                    ];
                }

                if ($websiteCode == \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE){
                    $obj['regional_conditions'] = [];
                    $valueDias = $regionalConditions['maximum_delivery'][0]; //primer elemento - todos tienen el mismo valor.
                    $valueDiastoShow = '';
                    if ($valueDias > 1) {
                        $valueDiastoShow = $valueDias . ' días máximo';
                    }elseif ( $valueDias == 1){
                        $valueDiastoShow = $valueDias . ' día máximo';
                    }
                    $obj['attrs']['dias_despacho'] = [
                        'label' => 'Días hábiles de despacho',
                        'value' => $valueDiastoShow
                    ];

                    $valueDias = $regionalConditions['time_maximum_emergency_delivery'][0]; //primer elemento - todos tienen el mismo valor.
                    $valueDiastoShow = '';
                    if ($valueDias > 1) {
                        $valueDiastoShow = $valueDias . ' días máximo';
                    }elseif ( $valueDias == 1){
                        $valueDiastoShow = $valueDias . ' día máximo';
                    }
                    $obj['attrs']['dias_despacho_emergencia'] = [
                        'label' => 'Días hábiles de despacho en emergencia',
                        'value' => $valueDiastoShow
                    ];

                    $valueMaximo = $regionalConditions['maximum_amount'][0]; //primer elemento - todos tienen el mismo valor.
                    $obj['attrs']['monto_maximo'] = [
                        'label' => 'Monto maximo de despacho',
                        'value' => $valueMaximo
                    ];
                }

                if ($websiteCode == \Linets\AseoRenewalSetup\Model\AseoRenewalConstants::WEBSITE_CODE) {
                    $sellerRegionalConditions = $this->regionalHelper->getRegionalConditionsBySellerAndRegionId($id, $regionId);

                    $enableStorepickup = intval($sellerRegionalConditions->getEnableStorepickup());
                    $obj['attrs']['enable_storepickup'] =
                        [
                            'label' => 'Retiro en tienda',
                            'value' => $enableStorepickup === 0 ? 'No' : 'Si'
                        ];

                    $daysMaxDelivery = $sellerRegionalConditions->getMaximumDelivery();
                    $obj['attrs']['maximum_delivery'] = [
                        'label' => 'Plazo de Entrega máximo (Días Hábiles)',
                        'value' => $daysMaxDelivery <= 0 ? 'No' : $daysMaxDelivery
                    ];

                    $daysStorepickup = $sellerRegionalConditions->getDaysStorepickup();
                    $obj['attrs']['days_storepickup'] = [
                        'label' => 'Plazo Para Retiro En Tienda (Días Hábiles)',
                        'value' => $enableStorepickup === 0 ? 'No' : $daysStorepickup . ' días hábiles'
                    ];

                    $addressStorepickup = $sellerRegionalConditions->getShopAddressTakeout();
                    $obj['attrs']['shop_address_takeout'] = [
                        'label' => 'Dirección del Retiro En Tienda',
                        'value' => $enableStorepickup === 0 ? 'No' : $addressStorepickup
                    ];

                }

                    if ($websiteCode == \Intellicore\EmergenciasRenewal\Constants::WEBSITE_CODE) {
                    $obj['attrs']['url_service'] = [
                        'label' => 'Url Para Servicios De Mantención, Reparación De Repuestos (Categoria Servicios Para La Emergencia)',
                        'value' => $obj['attrs']['url_service']['value']
                    ];

                    $sellerRegionalConditions = $this->regionalHelper->getRegionalConditionsBySellerAndRegionId($id, $regionId);

//                    $shopAddressTakeout = $sellerRegionalConditions->getShopAddressTakeout();
//                    $obj['attrs']['shop_address_takeout'] = [
//                        'label' => 'Dirección Tienda/Bodega Para Retiro En Situación De Emergencias Y Prevención',
//                        'value' => $shopAddressTakeout
//                    ];

                    $productsAvailabiltyTermEmergencyHours = $sellerRegionalConditions->getProductsAvailabiltyTermEmergencyHours();
                    $obj['attrs']['products_availabilty_term_emergency_hours'] = [
                        'label' => 'Plazo Disponibilidad De Productos/Servicios En Situación De Emergencia (Horas Corridas)',
                        'value' => $productsAvailabiltyTermEmergencyHours . ' horas corridas'
                    ];

                    $daysStorepickup = $sellerRegionalConditions->getDaysStorepickup();
                    $obj['attrs']['days_storepickup'] = [
                        'label' => 'Plazo Para Retiro En Tienda (Días Hábiles)',
                        'value' => $daysStorepickup . ' días hábiles'
                    ];

                    $timeMaximumPreventionDelivery = $sellerRegionalConditions->getTimeMaximumPreventionDelivery();
                    $obj['attrs']['time_maximum_prevention_delivery'] = [
                        'label' => 'Plazo Entrega En Situación De Prevención Para La Protección Civil (Días Hábiles)',
                        'value' => $timeMaximumPreventionDelivery . ' días hábiles'
                    ];

                    $productsReplacementTermEmergencyDays = $sellerRegionalConditions->getProductsReplacementTermEmergencyDays();
                    $obj['attrs']['products_replacement_term_emergency_days'] = [
                        'label' => 'Plazo Reposición De Productos En Situación De Emergencia (Días Corridos)',
                        'value' => $productsReplacementTermEmergencyDays . ' días corridos'
                    ];

                    $productsReplacementTermPreventionDays = $sellerRegionalConditions->getProductsReplacementTermPreventionDays();
                    $obj['attrs']['products_replacement_term_prevention_days'] = [
                        'label' => 'Plazo Reposición De Productos En Situación De Prevención (Días Hábiles)',
                        'value' => $productsReplacementTermPreventionDays . ' días hábiles'
                    ];

                }

                $obj['macrozones'] = $this->macrozoneHelper->getMacrozoneFactorsBySeller($id);
                $obj['abtester'] = $this->customerDataBlock->shouldDisplayData();
                $obj['sellerAlias'] = $this->customerDataBlock->getSellerAlias($id);
                $obj['wkv_dccp_rut'] = $seller->getWkvDccpRut();
                $obj['wkv_dccp_business_name'] = $seller->getWkvDccpBusinessName();

                $suppliers[] = $obj;
            }
        }
        $result->setData(['suppliers' => $suppliers]);
        return $result;
    }


    /**
     * Get codes of all regions by ids where the seller operates
     *
     * @param array $ids
     * @return string
     */
    private function getRegionCodes($ids)
    {
        $ids = implode(',', $ids);
        $conn = $this->_resourceConnection->getConnection();
        $tableName = $this->_resourceConnection->getTableName('directory_country_region');
        $sql = "SELECT code FROM {$tableName} WHERE region_id IN ({$ids})";
        $data = $conn->fetchAll($sql);
        $arr = [];

        foreach($data as $region)
        {
            $arr[] = $region['code'];
        }

        return implode(', ', $arr);
    }

    /**
     * Get seller
     *
     * @param int $id
     * @return \Magento\Customer\Model\CustomerFactory|null
     */
    private function getSeller($id)
    {
        if ($id !== null) {
            return $this->_customer->create()->load($id);
        }

        return null;
    }

    /**
     * Get formatted data from seller
     *
     * @param \Magento\Customer\Model\CustomerFactory $seller
     * @param string $key
     * @param \Magento\Customer\Model\Customer|bool $attribute
     * @return string
     */
    private function getData($seller, $key, $attribute = false)
    {
        if ($attribute && $attribute->getFrontendInput() !== null) {
            switch ($attribute->getFrontendInput()) {
                case 'select':
                case 'dropdown':
                    if ($attribute->usesSource()) {
                        $data = $attribute->getSource()->getOptionText((int)$seller->getData($key));
                    }
                    break;
                case 'date':
                    if (!empty($seller->getData($key))) {
                        $date = new \DateTime($seller->getData($key));
                        $data = $date !== false ? $date->format('d/m/Y') : '';
                    }
                    break;
                case 'boolean':
                    $data = (int)$seller->getData($key) === 1 ? __('Yes') : __('No');
                    break;
                default:
                    $data = trim($seller->getData($key));
            }

        } else {
            $data = $seller->getData($key);
        }

        return $data ?? '-';
    }
}
