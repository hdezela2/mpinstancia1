<?php
namespace Formax\PriceCurrency\Model\Directory;

class Currency extends \Magento\Directory\Model\Currency{
    /**
    * Price precision setted in $options array
    * @param string $price
    * @param array $options
    * @return string
    */
    public function formatTxt($price, $options = [])
    {
        if (!is_numeric($price)) {
            $price = $this->_localeFormat->getNumber($price);
        }
        $price = sprintf("%F", $price);

        $options['precision'] = 0;

        return $this->_localeCurrency->getCurrency($this->getCode())->toCurrency($price, $options);
    }
}
