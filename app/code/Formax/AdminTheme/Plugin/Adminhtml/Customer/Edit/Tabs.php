<?php
namespace Formax\AdminTheme\Plugin\Adminhtml\Customer\Edit;

class Tabs{

  /**
   * setForm for modify the inputs shown in the admin panel on the seller view
   * @param \Webkul\Marketplace\Block\Adminhtml\Customer\Edit\Tabs $subject
   * @param \Magento\Framework\Data\Form $form
   */
  public function beforeSetForm($subject, $form){
    $fieldset = $form->getElement('base_fieldset');
    $fieldset->removeField('twitter_id');
    $fieldset->removeField('facebook_id');
    $fieldset->removeField('gplus_id');
    $fieldset->removeField('instagram_id');
    $fieldset->removeField('youtube_id');
    $fieldset->removeField('vimeo_id');
    $fieldset->removeField('pinterest_id');
    $fieldset->removeField('moleskine_id');
    $fieldset->removeField('contact_number');
    $fieldset->removeField('taxvat');
    $fieldset->removeField('company_locality');
    $fieldset->removeField('country_pic');
    $fieldset->removeField('return_policy');
    $fieldset->removeField('shipping_policy');
    $fieldset->removeField('privacy_policy');
    $fieldset->removeField('company_description');
  }

}
