<?php

namespace Formax\SoftwareSetUp\Setup;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Store\Model\Store;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;

class InstallData implements InstallDataInterface
{
    const FRONT_THEME = 'Formax/Software';
    /**
     * @var WebsiteFactory
     */
    private $websiteFactory;
    /**
     * @var Website
     */
    private $websiteResourceModel;
    /**
     * @var StoreFactory
     */
    private $storeFactory;
    /**
     * @var GroupFactory
     */
    private $groupFactory;
    /**
     * @var Group
     */
    private $groupResourceModel;
    /**
     * @var Store
     */
    private $storeResourceModel;
    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;
    /**
     * @var ConfigInterface
     */
    private $configInterface;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * InstallData constructor.
     * @param WebsiteFactory $websiteFactory
     * @param Website $websiteResourceModel
     * @param Store $storeResourceModel
     * @param Group $groupResourceModel
     * @param StoreFactory $storeFactory
     * @param GroupFactory $groupFactory
     * @param ManagerInterface $eventManager
     * @param CategoryFactory $categoryFactory
     * @param ConfigInterface $configInterface
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        WebsiteFactory $websiteFactory,
        Website $websiteResourceModel,
        Store $storeResourceModel,
        Group $groupResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory,
        ManagerInterface $eventManager,
        CategoryFactory $categoryFactory,
        ConfigInterface $configInterface,
        CollectionFactory $collectionFactory
    ) {
        $this->websiteFactory = $websiteFactory;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->storeFactory = $storeFactory;
        $this->groupFactory = $groupFactory;
        $this->groupResourceModel = $groupResourceModel;
        $this->storeResourceModel = $storeResourceModel;
        $this->eventManager = $eventManager;
        $this->categoryFactory = $categoryFactory;
        $this->configInterface = $configInterface;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        ##### CREATE WEBSITE #####
        /** @var \Magento\Store\Model\Website $website */
        $website = $this->websiteFactory->create();
        $website->load('convenio_software');
        if(!$website->getId()){
            $website->setCode('convenio_software');
            $website->setName('Software Website');
            $this->websiteResourceModel->save($website);
        }

        ##### CREATE ROOT CATEGORY #####
        $category = $this->createOrUpdateRootCategory();

        ##### CREATE STORE #####
        if($website->getId()){
            /** @var \Magento\Store\Model\Group $group */
            $group = $this->groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName('Software Store');
            $group->setCode('convenio_store_software');
            $group->setRootCategoryId($category->getId());
            $group->setDefaultStoreId(0);
            $this->groupResourceModel->save($group);
        }

        ##### CREATE STORE VIEW #####
        /** @var  \Magento\Store\Model\Store $store */
        $store = $this->storeFactory->create();
        $store->load('software');
        if(!$store->getId()){
            $group = $this->groupFactory->create();
            $group->load('Software Store', 'name');
            $store->setCode('software');
            $store->setName('Software Store View');
            $store->setWebsiteId($website->getId());
            $store->setGroupId($group->getId());
            $store->setIsActive(1);
            $store->save();
            // Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
            $this->eventManager->dispatch('store_add', ['store' => $store]);
        }

        ##### ASSIGN THEME TO STORE VIEW #####
        $themes = $this->collectionFactory->create()->loadRegisteredThemes();
        /**
         * @var \Magento\Theme\Model\Theme $theme
         */
        foreach ($themes as $theme) {
            if ($theme->getArea() == 'frontend' && $theme->getCode() == self::FRONT_THEME) {
                $this->configInterface->saveConfig('design/theme/theme_id', $theme->getId(), 'stores', $store->getId());
            }
        }
    }

    /**
     * @return \Magento\Catalog\Model\Category
     * @throws \Exception
     */
    private function createOrUpdateRootCategory()
    {
        $category = $this->categoryFactory->create();
        $category->setName('DCCP Software');
        $category->setIsActive(true);
        $category->setStoreId(0);
        $parentCategory = $this->categoryFactory->create();
        $parentCategory->load(\Magento\Catalog\Model\Category::TREE_ROOT_ID);
        $category->setDisplayMode(\Magento\Catalog\Model\Category::DM_PRODUCT);
        $category->setPath($parentCategory->getPath());
        $category->save();

        return $category;
    }
}