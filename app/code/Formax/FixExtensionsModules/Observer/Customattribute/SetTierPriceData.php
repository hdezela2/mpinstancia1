<?php
namespace Formax\FixExtensionsModules\Observer\Customattribute;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;


class SetTierPriceData extends \Webkul\Customattribute\Observer\SetTierPriceData{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $wholeData = $observer->getEvent()->getData();
        try {
            foreach ($wholeData as $data) {
                $tierPrices = [];
                if (isset($data['product'])) {
                    if (isset($data['product']['tier_price'])) {
                        $tierPrices = $data['product']['tier_price'];
                    }
                    $sku = $data['product']['sku'];
                    $product = $this->_productRepository->get(
                        $sku,
                        ['edit_mode' => true]
                    );
                    if(isset($data['product']['voucher_product_type'])){
                        $product->setData('voucher_product_type', $data['product']['voucher_product_type']);
                    }
                    $product->setData('tier_price', $tierPrices);
                    $this->_productRepository->save($product);
                }
            }
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__('Could not save product tier price'));
        }
    }
}
