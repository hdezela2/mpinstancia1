<?php

namespace Formax\ConfigCmSoftware\Plugin;

use Magento\Framework\App\Http\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * class CustomerSessionContext
 */
class CustomerSessionContext
{
    /**
 	* @var \Magento\Customer\Model\Session
 	*/
    protected $customerSession;

    /**
 	* @var \Magento\Framework\App\Http\Context
 	*/
	protected $httpContext;

	/**
	 * Initialize dependencies
	 * 
	 * @param \Magento\Customer\Model\Session $customerSession
	 * @param \Magento\Framework\App\Http\Context $httpContext
	 */
    public function __construct(
		CustomerSession $customerSession,
		Context $httpContext
    ) {
		$this->customerSession = $customerSession;
    	$this->httpContext = $httpContext;
    }

    /**
 	* @param \Magento\Framework\App\ActionInterface $subject
 	* @param callable $proceed
 	* @param \Magento\Framework\App\RequestInterface $request
 	* @return mixed
 	*/
    public function aroundDispatch(
    	\Magento\Framework\App\ActionInterface $subject,
    	\Closure $proceed,
    	\Magento\Framework\App\RequestInterface $request
    ) {
    	$this->httpContext->setValue(
        	'customer_id',
        	$this->customerSession->getCustomerId(),
        	false
    	);

    	$this->httpContext->setValue(
        	'customer_name',
        	$this->customerSession->getCustomer()->getName(),
        	false
    	);

    	$this->httpContext->setValue(
        	'customer_email',
        	$this->customerSession->getCustomer()->getEmail(),
        	false
    	);
		
		$addresses = $this->getCustomerAddress($this->customerSession->getCustomer());
		$company = isset($addresses[0]['company']) ? $addresses[0]['company'] : '';
		$this->httpContext->setValue(
        	'customer_company',
        	$company,
        	false
		);

    	return $proceed($request);
	}
	
	/**
	 * Get customer addresses
	 * 
	 * @param \Magento\Customer\Model\Customer $customerModel
	 * @return array
	 */
	public function getCustomerAddress($customerModel)
    {
        $customerAddress = [];
        if ($customerModel->getAddresses() !== null) {
            foreach ($customerModel->getAddresses() as $address) {
				$customerAddress[] = [
					'street' => $address->getStreet(),
					'city' => $address->getCity(),
					'region' => $address->getRegion(),
					'country' => $address->getCountryId(),
					'postcode' => $address->getPostcode(),
					'company' => $address->getCompany()
				];
            }
        }
		
        return $customerAddress;
    }
}