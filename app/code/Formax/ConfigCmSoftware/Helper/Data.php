<?php

namespace Formax\ConfigCmSoftware\Helper;

use Magento\Directory\Model\Country;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Http\Context as HttpContext;
use Formax\QuotesSupplier\Model\Source\ExecutionServiceTerm;
use Magento\Framework\Serialize\Serializer\Json as JsonSerialize;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\ScopeInterface as ScopeConfig;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Driver\File as FileDriver;
use Magento\Framework\UrlInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\HTTP\ZendClientFactory;
use Psr\Log\LoggerInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory as InfoCollectionFactory;

/**
 * Data helper
 */
class Data extends AbstractHelper
{
    /**
     * @var string
     */
    const SOFTWARE_STOREVIEW_CODE = 'software';

    /**
     * @var string
     */
    const CURRENCY_UTM_PATH = 'dccp_endpoint/greatbuy/currency_endpoint';

    /**
     * @var Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Magento\Framework\Pricing\Helper\Data
     */
    public $pricingHelper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    public $timezone;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    public $logger;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * @var Magento\Framework\Serialize\Serializer\Json
     */
    protected $jsonSerialize;

    /**
     * @var Formax\QuotesSupplier\Model\Source\ExecutionServiceTerm
     */
    protected $executionServiceTerm;

    /**
     * @var Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
     */
    protected $categoryCollectionFactory;

    /**
     * @var Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @var Magento\Framework\Filesystem\Driver\File
     */
    protected $fileDriver;

    /**
     * @var Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory
     */
    protected $infoCollectionFactory;

    /**
     * @var Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $eavAttribute;

    /**
     * @var array
     */
    public $purchaseUnit;

    protected Country $country;

    /**
     * Initialize dependencies.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param Magento\Framework\Pricing\Helper\Data $pricingHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \\Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerialize
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filesystem\Driver\File $fileDriver
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Formax\QuotesSupplier\Model\Source\ExecutionServiceTerm $executionServiceTerm
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        PricingHelper $pricingHelper,
        TimezoneInterface $timezone,
        ProductRepositoryInterface $productRepository,
        CustomerRepositoryInterface $customerRepositoryInterface,
        HttpContext $httpContext,
        JsonSerialize $jsonSerialize,
        LoggerInterface $logger,
        Filesystem $filesystem,
        FileDriver $fileDriver,
        ZendClientFactory $httpClientFactory,
        CustomerSession $customerSession,
        ExecutionServiceTerm $executionServiceTerm,
        CategoryCollectionFactory $categoryCollectionFactory,
        InfoCollectionFactory $infoCollectionFactory,
        ResourceConnection $resource,
        EavAttribute $eavAttribute,
        Country $country
    )
    {
        $this->storeManager = $storeManager;
        $this->timezone = $timezone;
        $this->pricingHelper = $pricingHelper;
        $this->httpContext = $httpContext;
        $this->jsonSerialize = $jsonSerialize;
        $this->customerSession = $customerSession;
        $this->productRepository = $productRepository;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->executionServiceTerm = $executionServiceTerm;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->filesystem = $filesystem;
        $this->fileDriver = $fileDriver;
        $this->logger = $logger;
        $this->httpClientFactory = $httpClientFactory;
        $this->infoCollectionFactory = $infoCollectionFactory;
        $this->eavAttribute = $eavAttribute;
        $this->resource = $resource;
        $this->scopeConfig = $context->getScopeConfig();
        $this->country = $country;
        parent::__construct($context);
    }

    /**
     * Get Store code
     *
     * @return string
     */
    public function getCurrentStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }

    /**
     * Get product object by ID
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProduct($productId)
    {
        $product = null;
        try {
            $product = $this->productRepository->getById($productId);
        } catch (NoSuchEntityException $e) {
            $this->logger->warning('Error message: ' . $e->getMessage());
        }

        return $product;
    }

    /**
     * Get purchase unit by customer loggedIn
     *
     * @param int|false $customerId optional
     * @return array
     */
    public function getPurchaseUnitByCustomer($customerId = false)
    {
        if (is_array($this->purchaseUnit) && count($this->purchaseUnit) > 0) {
            return $this->purchaseUnit;
        }

        if ($customerId === false) {
            if ($this->getCustomerIsLoggedIn()) {
                $customerId = $this->getCustomerId();
            }
        }

        if ((int)$customerId > 0) {
            $customer = $this->customerRepositoryInterface->getById($customerId);
            $organizationsText = $customer->getCustomAttribute('user_rest_partner_organizations')
                ? $customer->getCustomAttribute('user_rest_partner_organizations')->getValue() : '';

            if ($organizationsText) {
                $partnerOrganizations = $this->jsonSerialize->unserialize($organizationsText);
                if ($partnerOrganizations !== null && !empty($partnerOrganizations)) {
                    foreach ($partnerOrganizations as $organization) {
                        $this->purchaseUnit[$organization['idOrganizacion']] = $organization['nombre'];
                    }
                }
            }
        }

        return $this->purchaseUnit;
    }

    /**
     * Get from http context if customer is logged in
     *
     * @return bool
     */
    public function getCustomerIsLoggedIn()
    {
        return (bool)$this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    /**
     * Get from http context customer logged ID
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->httpContext->getValue('customer_id');
    }

    /**
     * Get from http context customer logged name
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->httpContext->getValue('customer_name');
    }

    /**
     * Get from http context customer logged email
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->httpContext->getValue('customer_email');
    }

    /**
     * Get from http context customer logged company
     *
     * @return string
     */
    public function getCustomerCompany()
    {
        return $this->httpContext->getValue('customer_company');
    }

    /**
     * Get formatted price
     *
     * @param float $price
     * @return string
     */
    public function currencyFormat($price)
    {
        if (is_numeric($price)) {
            return $this->pricingHelper->currency($price, true, false);
        }

        return $price;
    }

    /**
     * Get formatted number
     *
     * @param float $number
     * @return string
     */
    public function numberFormat($number)
    {
        if (is_numeric($number)) {
            return number_format($number, 1, ',', '.');
        }

        return $number;
    }

    /**
     * Get purchase unit by code
     *
     * @param string $code
     * @return string
     */
    public function getPurchaseUnitByCode($code)
    {
        $purchaseUnit = $this->getPurchaseUnitByCustomer();
        return isset($this->purchaseUnit[$code]) ? $this->purchaseUnit[$code] : '';
    }

    /**
     * Format datetime to date
     *
     * @return string
     */
    public function formatDateTime($date)
    {
        $formatedDate = '';
        try {
            if ($date) {
                if ($date != '0000-00-00 00:00:00' && $date != '0000-00-00') {
                    $formatedDate = $this->timezone->date(new \DateTime($date));
                    return $formatedDate->format('Y-m-d');
                }
            }
        } catch (\Exception $e) {
            $this->logger->warning('Error message: ' . $e->getMessage());
        }

        return $formatedDate;
    }

    public function formatStartDateTime($date)
    {
        $formatedDate = '';
        try {
            if ($date) {
                if ($date != '0000-00-00 00:00:00' && $date != '0000-00-00') {
                    $formatedDate = new \DateTime($date);
                    return $formatedDate->format('Y-m-d');
                }
            }
        } catch (\Exception $e) {
            $this->logger->warning('Error message: ' . $e->getMessage());
        }

        return $formatedDate;
    }

    public function modifyDaysFormatDateDB($date,$days = +1)
    {
        $formatedDate = '';
        $modifier = $days.' day';

        try {
            if ($date && $date != '0000-00-00 00:00:00' && $date != '0000-00-00')
            {
                $formatedDate=  new \DateTime($date);
                $formatedDate->modify($modifier);
                return $formatedDate->format('Y-m-d');
            }


        }catch (\Exception $e)
        {
            $this->logger->warning('Error message: ' . $e->getMessage());
        }

        return $formatedDate;
    }

    public function formatDateDB($date)
    {
        $formatedDate = '';
        try {
            if ($date) {
                if ($date != '0000-00-00 00:00:00' && $date != '0000-00-00') {
                    $formatedDate = new \DateTime($date);
                    return $formatedDate->format('Y-m-d');
                }
            }
        } catch (\Exception $e) {
            $this->logger->warning('Error message: ' . $e->getMessage());
        }

        return $formatedDate;
    }

    /**
     * Format date to datetime
     *
     * @return string
     */
    public function formatDateToDateTime($date)
    {
        $formatedDate = '';
        try {
            if ($date) {
                if ($date != '0000-00-00 00:00:00' && $date != '0000-00-00') {
                    $formatedDate = $this->timezone->date(new \DateTime($date));
                    return $formatedDate->format('Y-m-d H:i:s');
                }
            }
        } catch (\Exception $e) {
            $this->logger->warning('Error message: ' . $e->getMessage());
        }

        return $formatedDate;
    }

    /**
     * Get text for service term
     *
     * @param int $code
     * @return string
     */
    public function getTextServiceTerm($code)
    {
        $result = '';
        try {
            foreach ($this->executionServiceTerm->toOptionArray() as $item) {
                if ($item['value'] == $code) {
                    $result = isset($item['label']) ? $item['label']->getText() : '';
                }
            }
        } catch (\Exception $e) {
            $this->logger->warning('Error message: ' . $e->getMessage());
        }

        return $result;
    }

    /**
     * Get URL files path
     *
     * @param string|array $files
     * @return array
     */
    public function getFilesPath($files)
    {
        $urlDocs = false;

        try {
            if ($files) {
                $path = 'chilecompra/files/formattributes';
                $documents = is_string($files) ? $this->jsonSerialize->unserialize($files) : (array)$files;
                if (isset($documents['data']) && is_array($documents['data'])) {
                    foreach ($documents['data'] as $doc) {
                        $filename = $this->getPathMedia() . $path . $doc;
                        if ($this->fileDriver->isExists($filename)) {
                            $extension = substr($doc, strrpos($doc, '.') + 1);
                            $urlDocs[] = [
                                'url' => $this->getUrlMedia() . $path . $doc,
                                'name' => substr($doc, strrpos($doc, '/') + 1),
                                'ext' => $extension
                            ];
                        }
                    }
                } else {
                    $filename = $this->getPathMedia() . $path . $documents;
                    if ($this->fileDriver->isExists($filename)) {
                        $extension = substr($documents, strrpos($documents, '.') + 1);
                        $urlDocs[] = [
                            'url' => $this->getUrlMedia() . $path . $documents,
                            'name' => substr($documents, strrpos($documents, '/') + 1),
                            'ext' => $extension
                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logger->error('Error message: ' . $e->getMessage());
        }

        return $urlDocs;
    }

    /**
     * Format data serialized
     *
     * @param string $dataSerialized
     * @return string
     */
    public function formatSerializeData($dataSerialized, $columnKey = null)
    {
        $serializeDataFormated = '';
        //$itemsArray = [];
        $itemsArray2 = [];
        $data = $this->jsonSerialize->unserialize($dataSerialized);
        if ($columnKey == 'table_evaluation_criteria') {

            for ($i = 0; $i < count($data['data'])-1 ; $i++ ){
                if (isset($data['data']['evaluation_label'][$i])
                && isset($data['data'][$i]['weight'])
                && isset($data['data'][$i]['observations'])) {
                    $itemsArray2[$i] = [
                        'criterio_label' => $data['data']['evaluation_label'][$i],
                        'ponderacion' => $data['data'][$i]['weight'],
                        'observaciones' => $data['data'][$i]['observations']
                    ];
                }
            }


        }elseif ($columnKey != 'table_requirement' && !(array_key_exists('requirement', $data['data']))) {
            if (isset($data['data'])) {
                foreach ($data['data'] as $key => $row) {
                    if (is_array($row)) {
                        $itemsArray2[] = $row;

                    } else {
                        if ($row) {
                            $itemsArray2[$key] = $row;
                        }
                    }
                }
            }
        }else {
            if (isset($data['data']) && is_array($data['data'])) {
                $keysData = array_keys($data['data']);
                if (is_array($keysData)) {

                    if (in_array('requirement', $keysData)) {
                        $itemsArray2 = $data['data']['requirement'];

                    } else {
                        $sizeKeysData = sizeof($data['data'][$keysData[0]]);

                        for ($i = 0; $i < $sizeKeysData; $i++) {
                            $itemsArray2[] = [
                                'criterio_label' => $data['data']['criterio_label'][$i],
                                'ponderacion' => $data['data']['ponderacion'][$i],
                                'observaciones' => $data['data']['observaciones'][$i],
                            ];

                        }
                    }


                }

            }

        }

        $serializeDataFormated = count($itemsArray2) > 0 ? $this->jsonSerialize->serialize($itemsArray2) : '';
        return $serializeDataFormated;
    }


    /**
     * Get category name from product
     *
     * @param Magento\Catalog\Api\Data\ProductInterface $product
     * @return string
     */
    public function getCategoryName($product)
    {
        $categoryName = '';
        $categoryIds = $product->getCategoryIds();
        $categories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('name')
            ->addAttributeToFilter('level', 3)
            ->addAttributeToFilter('entity_id', $categoryIds);

        foreach ($categories as $category) {
            $categoryName = $category->getName();
        }

        return $categoryName;
    }

    /**
     * Get categories level 3 from CM Software
     *
     * @return array
     */
    public function getCategoriesCmSoftware()
    {
        $categoriesArray = [];
        $rootCategoryId = $this->storeManager->getStore()->getRootCategoryId();
        $categories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('name')
            ->addAttributeToFilter('level', 3)
            ->addAttributeToFilter('path', ['like' => '1/' . $rootCategoryId . '/%'])
            ->addAttributeToFilter('is_active', 1);

        foreach ($categories as $category) {
            $categoriesArray[] = [
                'value' => $category->getId(),
                'label' => $category->getName()
            ];
        }

        return $categoriesArray;
    }

    /**
     * Get category URL from product
     *
     * @param Magento\Catalog\Api\Data\ProductInterface $product
     * @return string
     */
    public function getCategoryUrl($product)
    {
        $categoryUrl = '';
        $categoryIds = $product->getCategoryIds();
        $categories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('url_key')
            ->addAttributeToFilter('level', 3)
            ->addAttributeToFilter('entity_id', $categoryIds);

        foreach ($categories as $category) {
            $categoryUrl = $category->getUrlKey();
        }

        return $categoryUrl;
    }

    /**
     * Get system value
     *
     * @param string $path
     * @return string
     */
    public function getSystemValue($path)
    {
        return $this->scopeConfig->getValue($path, ScopeConfig::SCOPE_STORE);
    }

    /**
     * Get URL from Media files
     *
     * @return string
     */
    public function getUrlMedia()
    {
        return $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * Get path from Media files
     *
     * @return string
     */
    public function getPathMedia()
    {
        return $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
    }

    /**
     * Get path from var files
     *
     * @return string
     */
    public function getPathVar()
    {
        return $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR)->getAbsolutePath();
    }

    /**
     * Get an instance of fileDriver
     *
     * @return Magento\Framework\Filesystem\Driver\File
     */
    public function getFileDriver()
    {
        return $this->fileDriver;
    }

    /**
     * Get an instance of jsonSerialized
     *
     * @return Magento\Framework\Serialize\Serializer\Json
     */
    public function getJsonSerialized()
    {
        return $this->jsonSerialize;
    }

    /**
     * Get an instance of StoreManagerInterface
     *
     * @return Magento\Store\Model\StoreManagerInterface
     */
    public function getStoreManager()
    {
        return $this->storeManager;
    }

    /**
     * Get UTM webservice
     *
     * @return float|bool
     */
    public function getCurrentUtmValue()
    {
        $currentUtm = $this->customerSession->getCurrentUtm();
        try {
            if (!$currentUtm) {
                $endpoint = $this->getSystemValue(self::CURRENCY_UTM_PATH);
                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $response = $client->request(\Zend_Http_Client::GET)->getBody();
                $this->logger->warning('Response UTM Webservice: ' . $response);
                $obj = json_decode($response);

                foreach ($obj->payload as $data) {
                    if ($data->moneda == 'UTM') {
                        $currentUtm = $data->valorPeso;
                    }
                }

                if ((float)$currentUtm > 0) {
                    $this->customerSession->setCurrentUtm($currentUtm);
                }
            }
        } catch (LocalizedException $e) {
            $this->logger->warning('Error message: ' . $e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical('Error message: ' . $e->getMessage());
        }

        return (float)$currentUtm;
    }

    /**
     * Get qty UTM by amount
     *
     * @param float $amount
     * @param bool|float $utm
     * @return bool|floar
     */
    public function getUtmQtyByAmount($amount, $utm = false)
    {
        if (is_numeric($amount)) {
            if ($utm === false) {
                $utm = $this->getCurrentUtmValue();
            }
            if ($utm !== null && $utm > 0) {
                return (float)$amount / $utm;
            }
        }

        return false;
    }

    /**
     * Format datetime to date
     *
     * @param \DateTime|string
     * @return bool
     */
    public function checkIfDateExpired($date)
    {
        $currentDate = $this->timezone->date();
        $result = false;

        if ($date instanceof \DateTime) {
            if ($date->format('Y-m-d') < $currentDate->format('Y-m-d')) {
                $result = true;
            }
        }

        if (is_string($date)) {
            $validator = new \Zend\Validator\Date();
            $validator->setFormat('Y-m-d H:i:s');
            if (!$validator->isValid($date)) {
                throw new LocalizedException(__('Value %1 is a invalid date.', $date));
            }

            $newDate = new \DateTime($date);
            if ($newDate->format('Y-m-d') < $currentDate->format('Y-m-d')) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Get range initial value of UTM qty for requestforquote
     *
     * @return bool
     */
    public function getIniQtyUtm()
    {
        $config = 'requestforquote/requestforquote_settings/requestforquote_utm_ini';
        return (int)$this->scopeConfig->getValue($config);
    }

    /**
     * Get range end value of UTM qty for requestforquote
     *
     * @return bool
     */
    public function getEndQtyUtm()
    {
        $config = 'requestforquote/requestforquote_settings/requestforquote_utm_end';
        return (int)$this->scopeConfig->getValue($config);
    }

    /**
     * Get maximum value for publication term for requestforquote
     *
     * @return bool
     */
    public function getPublicationDaysTerm()
    {
        $config = 'requestforquote/requestforquote_settings/requestforquote_publication_end_days';
        return (int)$this->scopeConfig->getValue($config);
    }

    /**
     * Get requestforquote ID from shopping cart
     *
     * @param int $quoteId
     * @return int
     */
    public function getRequestforquotedIdFromCart($quoteId)
    {
        $requestforquotedId = 0;
        $quoteId = (int)$quoteId;

        try {
            $connection = $this->resource->getConnection();
            if ($quoteId > 0) {
                $sql = 'SELECT qio.value FROM ' . $this->resource->getTableName('quote_item') . ' qi
                INNER JOIN ' . $this->resource->getTableName('quote_item_option') . ' qio
                ON qi.item_id = qio.item_id WHERE qi.quote_id = ' . $quoteId;
                $result = $connection->fetchOne($sql);
                $data = $this->jsonSerialize->unserialize($result);
                $id = (isset($data['quoteInfoId'])) ? (int)$data['quoteInfoId'] : 0;

                if ($id > 0) {
                    $sql = 'SELECT quote_id FROM ' . $this->resource->getTableName('requestforquote_quote_info') . ' WHERE entity_id = ' . $id;
                    $requestforquotedId = $connection->fetchOne($sql);
                }
            }

        } catch (\Exception $e) {
            $this->logger->critical('Error message: ' . $e->getMessage());
            $requestforquotedId = 0;
        }

        return $requestforquotedId;
    }

    /**
     * Get OC MP by quote ID
     *
     * @param int $quoteId
     * @param string
     */
    public function getOcByQuoteId($quoteId)
    {
        $oc = '';
        $quoteId = (int)$quoteId;
        $connection = $this->resource->getConnection();

        if ($quoteId > 0) {
            $sql = 'SELECT orden_de_compra FROM ' . $this->resource->getTableName('sales_order') .
                ' WHERE requestforquote_info = ' . $quoteId;
            $oc = $connection->fetchOne($sql);
        }

        return $oc;
    }

    /**
     * Get amount awarded
     *
     * @param int $quoteId
     * @param float
     */
    public function getAmountAwarded($quoteId)
    {
        $amount = 0;
        $quoteId = (int)$quoteId;
        $connection = $this->resource->getConnection();

        if ($quoteId > 0) {
            $sql = 'SELECT base_price FROM ' . $this->resource->getTableName('dccp_economic_evaluation') .
                ' ee INNER JOIN ' . $this->resource->getTableName('requestforquote_quote_conversation') .
                ' c ON ee.seller_quote_id = c.seller_quote_id AND c.edited = 1
            INNER JOIN ' . $this->resource->getTableName('requestforquote_quote_info') . ' i
            ON c.seller_quote_id = i.entity_id AND i.quote_id = ' . $quoteId;
            $amount = $connection->fetchOne($sql);
        }

        return (float)$amount;
    }

    /**
     * Get seller from quote
     *
     * @param int $quoteId
     * @param array
     */
    public function getSellersFromQuote($quoteId)
    {
        $sellersQuotes = [];
        $quoteId = (int)$quoteId;
        if ($quoteId > 0) {
            $infoCollectionFactory = $this->infoCollectionFactory->create();
            $resource = $infoCollectionFactory->getResource();
            $attrIdRut = $this->eavAttribute->getIdByCode('customer', 'wkv_dccp_rut');
            $attrIdBusinessName = $this->eavAttribute->getIdByCode('customer', 'wkv_dccp_business_name');

            $sellersQuotes = $infoCollectionFactory->addFieldToFilter('quote_id', $quoteId)
                ->addFieldToFilter('status', ['gteq' => 2])
                ->setOrder('cv.quote_name', 'ASC')
                ->setOrder('seller.firstname', 'ASC')
                ->setOrder('seller.lastname', 'ASC')
                ->distinct(true);
            $sellersQuotes->getSelect()
                ->join(
                    ['cv' => $resource->getTable('requestforquote_quote_conversation')],
                    'main_table.entity_id = cv.seller_quote_id AND cv.edited = 1',
                    ['quote_name']
                )->joinLeft(
                    ['seller' => $resource->getTable('customer_entity')],
                    'main_table.seller_id = seller.entity_id',
                    ['seller_name' => new \Zend_Db_Expr('CONCAT(firstname, " ", lastname)')]
                )->joinLeft(
                    ['seller_rut' => $resource->getTable('customer_entity_varchar')],
                    'main_table.seller_id = seller_rut.entity_id AND seller_rut.attribute_id = ' . $attrIdRut,
                    ['rut' => 'value']
                )->joinLeft(
                    ['seller_business_name' => $resource->getTable('customer_entity_varchar')],
                    'main_table.seller_id = seller_business_name.entity_id AND seller_business_name.attribute_id = ' . $attrIdBusinessName,
                    ['business_name' => 'value']
                )->joinLeft(
                    ['u' => $resource->getTable('marketplace_userdata')],
                    'seller.entity_id = u.seller_id',
                    ['shop_url']
                );
        }

        return $sellersQuotes;
    }

    /**
     * Get seller from quote
     *
     * @param int $quoteId
     * @param array
     */
    public function getQuoteDetail($quoteId)
    {
        $sellersQuotes = [];
        $quoteId = (int)$quoteId;
        if ($quoteId > 0) {
            $infoCollectionFactory = $this->infoCollectionFactory->create();
            $resource = $infoCollectionFactory->getResource();
            $attrIdRut = $this->eavAttribute->getIdByCode('customer', 'wkv_dccp_rut');
            $attrIdBusinessName = $this->eavAttribute->getIdByCode('customer', 'wkv_dccp_business_name');

            $sellersQuotes = $infoCollectionFactory->addFieldToFilter('quote_id', $quoteId)
                ->addFieldToFilter('status', ['gteq' => 2])
                ->setOrder('seller.firstname', 'ASC')
                ->setOrder('seller.lastname', 'ASC');
            $sellersQuotes->getSelect()->joinLeft(
                ['seller' => $resource->getTable('customer_entity')],
                'main_table.seller_id = seller.entity_id',
                ['seller_name' => new \Zend_Db_Expr('CONCAT(firstname, " ", lastname)')]
            )->joinLeft(
                ['seller_rut' => $resource->getTable('customer_entity_varchar')],
                'main_table.seller_id = seller_rut.entity_id AND seller_rut.attribute_id = ' . $attrIdRut,
                ['rut' => 'value']
            )->joinLeft(
                ['seller_business_name' => $resource->getTable('customer_entity_varchar')],
                'main_table.seller_id = seller_business_name.entity_id AND seller_business_name.attribute_id = ' . $attrIdBusinessName,
                ['business_name' => 'value']
            );
        }

        return $sellersQuotes;
    }

    /**
     * @return string
     */
    public function getMinDate()
    {
        $date = new \DateTime('1970-01-01');
        return $date->format('Y-m-d');
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getMaxDate()
    {
        $date = new \DateTime(date("Y-m-d", strtotime('+ 1year')));
        return $date->format('Y-m-d');
    }

    public function getRegionArrayLabels()
    {
        $regionOptions = [];
        $regionCollection = $this->country->loadByCode('CL')->getRegions();
        $regionsData = $regionCollection->getData();

        $regionOptions[] = 'Seleccione una Region';
        foreach ($regionsData as $regionData) {
            $regionOptions[$regionData['region_id']] = $regionData['name'];
        }

        return $regionOptions;
    }
}
