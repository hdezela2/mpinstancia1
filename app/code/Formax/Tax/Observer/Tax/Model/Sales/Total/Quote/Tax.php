<?php

namespace Formax\Tax\Observer\Tax\Model\Sales\Total\Quote;

use Webkul\WkTax\Helper\Data;
use Magento\Tax\Model\Calculation\RuleFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class Tax implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var RuleFactory
     */
	protected $taxRule;

	/**
	 * @var ScopeConfigInterface
	 */
	protected $scopeConfig;
	
    /**
     * Class constructor
     *
     * @param Data $helper
     */
    public function __construct(
		Data $helper,
		RuleFactory $taxRule,
		ScopeConfigInterface $scopeConfig
    ) {
		$this->helper = $helper;
		$this->taxRule = $taxRule;
		$this->scopeConfig = $scopeConfig;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
	public function execute(Observer $observer)
	{
		if ($this->helper->isEnabled()) {
			$quote = $observer->getEvent()->getQuote();
			$code = $quote->getWkTaxRule();
			$total = $observer->getData('total');

			if ((float)$total->getShippingAmount() > 0) {
				if (!$code) {
					$code = $this->helper->getConfigData('default_tax_rule');
				}
				
				$taxRule = $this->taxRule->create()->load($code, 'code');
				$taxClass = $taxRule->getProductTaxClasses();
				$taxClassId = isset($taxClass[0]) ? $taxClass[0] : 0;
				$percent = 0;

				if ($taxClassId) {
					$shippingTaxesClassesConfig = $this->scopeConfig->getValue(
						'wktax/general_settings/shipping_rulestaxes_code',
						\Magento\Store\Model\ScopeInterface::SCOPE_STORE
					);
					$shippingTaxesClasses = explode(',', $shippingTaxesClassesConfig);

					if (in_array($code, $shippingTaxesClasses)) {
						if ($taxRule->getCalculationModel()) {
							
							foreach ($taxRule->getCalculationModel()->getData() as $rates) {
								if (isset($rates[0]['rates'][0]['percent']) && (float)$rates[0]['rates'][0]['percent'] > 0) {
									$percent = (float)$rates[0]['rates'][0]['percent'];
								}
							}
							
							if ($percent > 0) {
								$taxAmount = round(($total->getShippingAmount()*$percent)/100);
								$totalTaxAmount = $total->getTaxAmount()+$taxAmount;
								$total->addBaseTotalAmount('tax', $totalTaxAmount);
								$total->setTaxAmount($totalTaxAmount);
								$total->setGrandTotal((float)$total->getGrandTotal() + $taxAmount);
								$total->setBaseGrandTotal((float)$total->getBaseGrandTotal() + $taxAmount);
							}
						}
					}
				}
			}
		}

		return $this;
    }
}
