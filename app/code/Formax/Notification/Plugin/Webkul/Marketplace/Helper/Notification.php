<?php

namespace Formax\Notification\Plugin\Webkul\Marketplace\Helper;

use Formax\Notification\Plugin\Webkul\Marketplace\Model\Notification as ModelNotification;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Webkul\Marketplace\Model\ResourceModel\Notification\Collection as notificationColl;
use Webkul\Marketplace\Model\ResourceModel\Notification\CollectionFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Formax\Notification\Helper\Data as FormaxHelperData;

/**
 * Marketplace helper Notification.
 */
class Notification
{
    /**
     * Collection for getting table name
     *
     * @var notificationColl
     */
    protected $notificationColl;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var MarketplaceHelper
     */
    protected $marketplaceHelper;

    /**
    * @var FormaxHelperData
    */
    protected $formaxHelperData;

    /**
     * @param NotificationColl $notificationColl
     * @param CollectionFactory $collectionFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param MarketplaceHelper $marketplaceHelper
     * @param FormaxHelperData $formaxHelperData
     */
    public function __construct(
        NotificationColl $notificationColl,
        CollectionFactory $collectionFactory,
        CustomerRepositoryInterface $customerRepository,
        MarketplaceHelper $marketplaceHelper,
        FormaxHelperData $formaxHelperData
    ) {
        $this->notificationColl = $notificationColl;
        $this->collectionFactory = $collectionFactory;
        $this->customerRepository = $customerRepository;
        $this->marketplaceHelper = $marketplaceHelper;
        $this->formaxHelperData = $formaxHelperData;
    }

    /**
     * Get all notifications ids
     *
     * @param \Webkul\Marketplace\Helper\Notification $subject
     * @param array $ids
     * @param int $sellerId
     * @return array
     */
    public function afterGetAllNotificationIds(\Webkul\Marketplace\Helper\Notification $subject, $ids, $sellerId)
    {
        try {
            /** Join marketplace_assignproduct_items */
            if ($this->formaxHelperData->isStatusProductChangeNotificationEnable()) {
                $marketplaceAssignProduct = $this->notificationColl->getTable(
                    'marketplace_assignproduct_items'
                );

                $collectionData1 = $this->collectionFactory->create();
                $collectionData1->getSelect()->join(
                    $marketplaceAssignProduct . ' as mp',
                    'main_table.notification_id = mp.id',
                    []
                )->where(
                    'main_table.type = ' . ModelNotification::TYPE_STATUS_PRODUCT
                )->where(
                    'mp.seller_id = ' . $sellerId
                );
                $ids1 = $collectionData1->getAllIds();
                $ids = array_merge($ids, $ids1);
            }

            /** Join dccp_penalty */
            if ($this->formaxHelperData->isPenaltyNotificationEnable()) {
                $dccpPenalty = $this->notificationColl->getTable(
                    'dccp_penalty'
                );

                $collectionData2 = $this->collectionFactory->create();
                $customerId = $this->marketplaceHelper->getCustomerId();
                $customer = $this->customerRepository->getById($customerId);
                $ids2 = [];

                if ($customer) {
                    $agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
                        $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : 0;
                    $loggedInSellerId = $customer->getCustomAttribute('user_rest_id') ?
                        $customer->getCustomAttribute('user_rest_id')->getValue() : 0;

                    $collectionData2->getSelect()->join(
                        $dccpPenalty . ' as p',
                        'main_table.notification_id = p.entity_id',
                        []
                    )->where(
                        'main_table.type = ' . ModelNotification::TYPE_PENALTY
                    )->where(
                        'p.dccp_seller_id = ' . $loggedInSellerId
                    )->where(
                        'p.agreement_id = ' . $agreementId
                    )->where(
                        'p.status = 1'
                    );
                    $ids2 = $collectionData2->getAllIds();
                }
                $ids = array_merge($ids, $ids2);
            }

            /** Join category_product_price */
            if ($this->formaxHelperData->isPriceReadjustmentNotificationEnable()) {
                $marketplaceCategoryProductPrice = $this->notificationColl->getTable(
                    'category_product_price'
                );

                $collectionData3 = $this->collectionFactory->create();
                $collectionData3->getSelect()->join(
                    $marketplaceCategoryProductPrice . ' as cpp',
                    'main_table.notification_id = cpp.entity_id AND cpp.seller_id = ' . $sellerId,
                    []
                )->where(
                    'main_table.type = ' . ModelNotification::TYPE_PRICE_READJUSTMENT
                );
                $ids3 = $collectionData3->getAllIds();
                $ids = array_merge($ids, $ids3);
            }

            /** Join price_dispersion */
            if ($this->formaxHelperData->isDispersionNotificationEnable()) {
                $marketplacePriceDispersion = $this->notificationColl->getTable(
                    'price_dispersion'
                );

                $collectionData4 = $this->collectionFactory->create();
                $collectionData4->getSelect()->join(
                    $marketplacePriceDispersion . ' as pd',
                    'main_table.notification_id = pd.entity_id AND pd.seller_id = ' . $sellerId,
                    []
                )->where(
                    'main_table.type = ' . ModelNotification::TYPE_DISPERSION
                );
                $ids4 = $collectionData4->getAllIds();
                $ids = array_merge($ids, $ids4);
            }

            /** Join customer_entity */
            if ($this->formaxHelperData->isSellerStatusNotificationEnable()) {
                $customerEntity = $this->notificationColl->getTable(
                    'customer_entity'
                );

                $sellerOwner = $this->customerRepository->getById($sellerId);

                $collectionData5 = $this->collectionFactory->create();
                $collectionData5->getSelect()->join(
                    $customerEntity . ' as cus',
                    'main_table.notification_id = cus.entity_id',
                    []
                )->where(
                    'main_table.type = ' . ModelNotification::TYPE_STATUS_SELLER
                )->where(
                    'main_table.notification_id = ' . $sellerId
                );
                $collectionData5->setOrder('created_at','DEST');
                $ids5 = $collectionData5->getAllIds();
                $ids = array_merge($ids, $ids5);
            }

            /** Join requestforquote_quote_info */
            if ($this->formaxHelperData->isQuoteNotificationEnable()) {
                $marketplaceQuote = $this->notificationColl->getTable(
                    'requestforquote_quote_info'
                );
                /*New Quote*/
                $collectionData6 = $this->collectionFactory->create();
                $_query = $this->formaxHelperData->getWebsiteCode() != SoftwareRenewalConstants::WEBSITE_CODE ? ' AND rfq.seller_id = ' . $sellerId : '';
                $collectionData6->getSelect()->join(
                    $marketplaceQuote . ' as rfq',
                    'main_table.notification_id = rfq.quote_id'.$_query,

                    []
                )->where(
                    'main_table.type = ' . ModelNotification::TYPE_QUOTE
                )->where(
                    'rfq.status = 0'
                );
                $ids6 = $collectionData6->getAllIds();
                $ids = array_merge($ids, $ids6);
                /*New Message*/
                $collectionData7 = $this->collectionFactory->create();
                $collectionData7->getSelect()->join(
                    $marketplaceQuote . ' as rfq',
                    'main_table.notification_id = rfq.quote_id AND rfq.seller_id = ' . $sellerId,
                    []
                )->where(
                    'main_table.type = ' . ModelNotification::TYPE_QUOTE_MESSAGE
                )->where(
                    'rfq.status = 1'
                );
                $ids7 = $collectionData7->getAllIds();
                $ids = array_merge($ids, $ids7);
                /*Approved*/
                $collectionData8 = $this->collectionFactory->create();
                $collectionData8->getSelect()->join(
                    $marketplaceQuote . ' as rfq',
                    'main_table.notification_id = rfq.quote_id AND rfq.seller_id = ' . $sellerId,
                    []
                )->where(
                    'main_table.type = ' . ModelNotification::TYPE_QUOTE_APPROVED
                )->where(
                    'rfq.status = 3'
                );
                $ids8 = $collectionData8->getAllIds();
                $ids = array_merge($ids, $ids8);
            }

        } catch (\Exception $e) {
            $ids = [];
        }

        return $ids;
    }
}
