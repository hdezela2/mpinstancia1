<?php

namespace Formax\Notification\Plugin\Webkul\Marketplace\Block\Page;

use Webkul\Marketplace\Model\ResourceModel\Notification\CollectionFactory;
use Formax\Notification\Plugin\Webkul\Marketplace\Model\Notification as FormaxNotification;
use Webkul\Marketplace\Model\Notification;
use Formax\Notification\Helper\Data as FormaxHelperData;

class Notifications
{
    /**
     * Notification collection
     *
     * @var \Webkul\Marketplace\Model\ResourceModel\Notification\Collection
     */
    protected $collection;

    /**
    * Assign Items Factory
    *
    * @var Webkul\MpAssignProduct\Model\ItemsFactory
    */
    protected $formaxHelperData;
    private CollectionFactory $collectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     * @param FormaxHelperData $formaxHelperData
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        FormaxHelperData $formaxHelperData
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->formaxHelperData = $formaxHelperData;
    }

    /**
     * Get All notifications
     *
     * @return array
     */
    public function aroundGetAllNotificationCount(\Webkul\Marketplace\Block\Page\Notifications $subject, callable $proceed)
    {
        $sellerId = $subject->helperData->getCustomerId();
        $ids = $subject->notificationHelper->getAllNotificationIds($sellerId);
        $collectionData = $this->collectionFactory->create()
        ->addFieldToFilter(
            'entity_id',
            ["in" => $ids]
        );
        
        $excludeNotification = [
            Notification::TYPE_REVIEW,
            Notification::TYPE_ORDER ,
            Notification::TYPE_TRANSACTION,
            Notification::TYPE_PRODUCT
        ];
        
        if (!$this->formaxHelperData->isPenaltyNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_PENALTY);
        }
        if (!$this->formaxHelperData->isPriceReadjustmentNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_PRICE_READJUSTMENT);
        }
        if (!$this->formaxHelperData->isDispersionNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_DISPERSION);
        }
        if (!$this->formaxHelperData->isSellerStatusNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_STATUS_SELLER);
        }
        if (!$this->formaxHelperData->isStatusProductChangeNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_STATUS_PRODUCT);
        }
        if (!$this->formaxHelperData->isQuoteNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_QUOTE);
            array_push($excludeNotification, FormaxNotification::TYPE_QUOTE_APPROVED);
            array_push($excludeNotification, FormaxNotification::TYPE_QUOTE_MESSAGE);
        }

        $collectionData->addFieldToFilter(
            'type',
            ['nin' => $excludeNotification]
        );

        return count($collectionData);
    }

    /**
     * Get All notifications
     * 
     * @param \Webkul\Marketplace\Block\Page\Notifications $subject
     * @param array $collectionData
     * @return array
     */
    public function afterGetAllNotification(\Webkul\Marketplace\Block\Page\Notifications $subject, $collectionData)
    {
        $excludeNotification = [
            Notification::TYPE_REVIEW,
            Notification::TYPE_ORDER ,
            Notification::TYPE_TRANSACTION,
            Notification::TYPE_PRODUCT
        ];
        
        if (!$this->formaxHelperData->isPenaltyNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_PENALTY);
        }
        if (!$this->formaxHelperData->isPriceReadjustmentNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_PRICE_READJUSTMENT);
        }
        if (!$this->formaxHelperData->isDispersionNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_DISPERSION);
        }
        if (!$this->formaxHelperData->isSellerStatusNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_STATUS_SELLER);
        }
        if (!$this->formaxHelperData->isStatusProductChangeNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_STATUS_PRODUCT);
        }
        if (!$this->formaxHelperData->isQuoteNotificationEnable()) {
            array_push($excludeNotification, FormaxNotification::TYPE_QUOTE);
            array_push($excludeNotification, FormaxNotification::TYPE_QUOTE_APPROVED);
            array_push($excludeNotification, FormaxNotification::TYPE_QUOTE_MESSAGE);
        }

        return $collectionData->addFieldToFilter(
            'type',
            ['nin' => $excludeNotification]
        );
    }

    /**
     * Get Notification Info
     *
     * @param \Webkul\Marketplace\Block\Page\Notifications $subject
     * @param string $message
     * @param array $rowData
     * @return string
     */
    public function afterGetNotificationInfo(\Webkul\Marketplace\Block\Page\Notifications $subject, $message, $rowData)
    {
        $timeArr = $subject->notificationHelper->getCalculatedTimeDigits(
            $rowData['created_at']
        );
        $type = $timeArr[0];
        $timedigit = $timeArr[1];
        $time = $timeArr[2];
        
        switch ($rowData['type']) {
            case FormaxNotification::TYPE_PENALTY:
                if ($this->formaxHelperData->isPenaltyNotificationEnable()) {
                    $penaltyNotificationDesc = $this->formaxHelperData->getPenaltyDescription($rowData);

                    if (!empty($penaltyNotificationDesc)) {
                        $url = '#';
                        $message = '<li class="wk-mp-notification-row wk-mp-dropdown-notification-review wk-mp-notification-review-bad">
                            <a 
                            href="' . $url . '" 
                            class="wk-mp-notification-entry-description-start"
                            title="' . __("View Penalty") . '">
                                <span>' . $penaltyNotificationDesc . '</span>
                            </a>
                            <small class="wk-mp-notification-time">' . $time . '</small>
                        </li>';
                    }
                }
                break;
            case FormaxNotification::TYPE_PRICE_READJUSTMENT:
                if ($this->formaxHelperData->isPriceReadjustmentNotificationEnable()) {
                    $priceReadjustmentNotificationDesc = $this->formaxHelperData->getReadjustmentPriceDescription($rowData);

                    if (!empty($priceReadjustmentNotificationDesc)) {
                        $url = '#';
                        $message = '<li class="wk-mp-notification-row wk-mp-dropdown-notification-transaction">
                            <a 
                            href="' . $url . '" 
                            class="wk-mp-notification-entry-description-start"
                            title="' . __("View Offer") . '">
                                <span>' . $priceReadjustmentNotificationDesc . '</span>
                            </a>
                            <small class="wk-mp-notification-time">' . $time . '</small>
                        </li>';
                    }
                }
                break;
            case FormaxNotification::TYPE_DISPERSION:
                if ($this->formaxHelperData->isDispersionNotificationEnable()) {
                    $dispersionNotificationDesc = $this->formaxHelperData->getDispersionDescription($rowData);

                    if (!empty($dispersionNotificationDesc)) {
                        $url = '#';
                        $message = '<li class="wk-mp-notification-row wk-mp-dropdown-notification-orders wk-mp-order-notification-new">
                            <a 
                            href="' . $url . '" 
                            class="wk-mp-notification-entry-description-start"
                            title="' . __("View Offer") . '">
                                <span>' . $dispersionNotificationDesc . '</span>
                            </a>
                            <small class="wk-mp-notification-time">' . $time . '</small>
                        </li>';
                    }
                }
                break;
            case FormaxNotification::TYPE_QUOTE:
                if ($this->formaxHelperData->isQuoteNotificationEnable()) {
                    $quoteNotificationDesc = $this->formaxHelperData->getQuoteDescription($rowData);

                    if (!empty($quoteNotificationDesc)) {
                        $url = '#';
                        $message = '<li class="wk-mp-notification-row wk-mp-dropdown-notification-orders wk-mp-order-notification-new">
                            <a 
                            href="' . $url . '" 
                            class="wk-mp-notification-entry-description-start"
                            title="' . __("View Quote") . '">
                                <span>' . $quoteNotificationDesc . '</span>
                            </a>
                            <small class="wk-mp-notification-time">' . $time . '</small>
                        </li>';
                    }
                }
                break;
            case FormaxNotification::TYPE_QUOTE_APPROVED:
                if ($this->formaxHelperData->isQuoteNotificationEnable()) {
                    $quoteApprovedNotificationDesc = $this->formaxHelperData->getQuoteApprovedDescription($rowData);

                    if (!empty($quoteApprovedNotificationDesc)) {
                        $url = '#';
                        $message = '<li class="wk-mp-notification-row wk-mp-dropdown-notification-orders wk-mp-order-notification-new">
                            <a 
                            href="' . $url . '" 
                            class="wk-mp-notification-entry-description-start"
                            title="' . __("View Quote") . '">
                                <span>' . $quoteApprovedNotificationDesc . '</span>
                            </a>
                            <small class="wk-mp-notification-time">' . $time . '</small>
                        </li>';
                    }
                }
                break;
            case FormaxNotification::TYPE_QUOTE_MESSAGE:
                if ($this->formaxHelperData->isQuoteNotificationEnable()) {
                    $quoteMessageNotificationDesc = $this->formaxHelperData->getQuoteMessageDescription($rowData);

                    if (!empty($quoteMessageNotificationDesc)) {
                        $url = '#';
                        $message = '<li class="wk-mp-notification-row wk-mp-dropdown-notification-orders wk-mp-order-notification-new">
                            <a 
                            href="' . $url . '" 
                            class="wk-mp-notification-entry-description-start"
                            title="' . __("View Quote") . '">
                                <span>' . $quoteMessageNotificationDesc . '</span>
                            </a>
                            <small class="wk-mp-notification-time">' . $time . '</small>
                        </li>';
                    }
                }
                break;
            case FormaxNotification::TYPE_STATUS_SELLER:
                if ($this->formaxHelperData->isSellerStatusNotificationEnable()) {
                    $sellerNotificationDesc = $this->formaxHelperData->getStatusSellerDescription($rowData);

                    if (!empty($sellerNotificationDesc)) {
                        $url = $subject->getUrl('marketplace/account/editprofile');
                        $message = '<li class="wk-mp-notification-row wk-mp-dropdown-notification-orders wk-mp-order-notification-processing">
                            <a 
                            href="' . $url . '" 
                            class="wk-mp-notification-entry-description-start"
                            title="' . __("View Profile") . '">
                                <span>' . $sellerNotificationDesc . '</span>
                            </a>
                            <small class="wk-mp-notification-time">' . $time . '</small>
                        </li>';
                    }
                }
                break;
            case FormaxNotification::TYPE_STATUS_PRODUCT:
                if ($this->formaxHelperData->isStatusProductChangeNotificationEnable()) {
                    $productNotificationDesc = $this->formaxHelperData->getStatusProductDescription($rowData);

                    if (!empty($productNotificationDesc)) {
                        $url = '#';
                        $message = '<li class="wk-mp-notification-row wk-mp-dropdown-notification-products">
                            <a 
                            href="' . $url . '" 
                            class="wk-mp-notification-entry-description-start"
                            title="' . __("View Offer") . '">
                                <span>' . $productNotificationDesc . '</span>
                            </a>
                            <small class="wk-mp-notification-time">' . $time . '</small>
                        </li>';
                    }
                }
                break;
            default:
                break;
        }

        return $message;
    }
}
