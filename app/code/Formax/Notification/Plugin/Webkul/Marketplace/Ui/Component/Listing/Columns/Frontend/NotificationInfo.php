<?php

namespace Formax\Notification\Plugin\Webkul\Marketplace\Ui\Component\Listing\Columns\Frontend;

use Webkul\Marketplace\Model\Notification;
use Formax\Notification\Plugin\Webkul\Marketplace\Model\Notification as FormaxNotification;
use Formax\Notification\Helper\Data as FormaxHelperData;
use Webkul\Marketplace\Helper\Notification as NotificationHelper;
use Magento\Framework\UrlInterface;

/**
 * Class NotificationInfo.
 */
class NotificationInfo
{
    /**
     * @var NotificationHelper
     */
    protected $_helper;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
    * @var FormaxHelperData
    */
    protected $formaxHelperData;

    /**
     * Constructor.
     *
     * @param NotificationHelper $helper
     * @param UrlInterface $urlBuilder
     * @param FormaxHelperData $formaxHelperData
     */
    public function __construct(
        NotificationHelper $helper,
        UrlInterface $urlBuilder,
        FormaxHelperData $formaxHelperData
    ) {
        $this->_helper = $helper;
        $this->urlBuilder = $urlBuilder;
        $this->formaxHelperData = $formaxHelperData;
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function afterPrepareDataSource(
        \Webkul\Marketplace\Ui\Component\Listing\Columns\Frontend\NotificationInfo $subject,
        $result,
        array $dataSource
    ) {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $subject->getData('name');

            foreach ($dataSource['data']['items'] as &$item) {
                $timeArr = $this->_helper->getCalculatedTimeDigits(
                    $item['created_at']
                );
                $type = $timeArr[0];
                $timedigit = $timeArr[1];
                $time = $timeArr[2];
                $item['created_at'] = $time;
                
                switch ($item['type']) {
                    case FormaxNotification::TYPE_PENALTY:
                        if ($this->formaxHelperData->isPenaltyNotificationEnable()) {
                            $penaltyNotificationDesc = $this->formaxHelperData->getPenaltyDescription($item);

                            if (!empty($penaltyNotificationDesc)) {
                                $url = '#';
                                $item['details'] = '<span class="wk-mp-notification-row wk-mp-dropdown-notification-review wk-mp-notification-review-bad">
                                <a 
                                href="' . $url . '" 
                                class="wk-mp-notification-entry-description-start"
                                title="'.__("View Penalty").'">
                                    <span>' . $penaltyNotificationDesc . '</span>
                                </a>
                                </span>';
                            }
                        }
                        break;
                    case FormaxNotification::TYPE_PRICE_READJUSTMENT:
                        if ($this->formaxHelperData->isPriceReadjustmentNotificationEnable()) {
                            $priceReadjustmentNotificationDesc = $this->formaxHelperData->getReadjustmentPriceDescription($item);

                            if (!empty($priceReadjustmentNotificationDesc)) {
                                $url = '#';
                                $item['details'] = '<span class="wk-mp-notification-row wk-mp-dropdown-notification-transaction">
                                <a 
                                href="' . $url . '" 
                                class="wk-mp-notification-entry-description-start"
                                title="'.__("View Readjusment").'">
                                    <span>' . $priceReadjustmentNotificationDesc . '</span>
                                </a>
                                </span>';
                            }
                        }
                        break;
                    case FormaxNotification::TYPE_DISPERSION:
                        if ($this->formaxHelperData->isDispersionNotificationEnable()) {
                            $dispersionNotificationDesc = $this->formaxHelperData->getDispersionDescription($item);

                            if (!empty($dispersionNotificationDesc)) {
                                $url = '#';
                                $item['details'] = '<span class="wk-mp-notification-row wk-mp-dropdown-notification-orders wk-mp-order-notification-new">
                                <a 
                                href="' . $url . '" 
                                class="wk-mp-notification-entry-description-start"
                                title="'.__("View Dispersion").'">
                                    <span>' . $dispersionNotificationDesc . '</span>
                                </a>
                                </span>';
                            }
                        }
                        break;
                    case FormaxNotification::TYPE_QUOTE:
                        if ($this->formaxHelperData->isQuoteNotificationEnable()) {
                            $quoteNotificationDesc = $this->formaxHelperData->getQuoteDescription($item);

                            if (!empty($quoteNotificationDesc)) {
                                $url = '#';
                                $item['details'] = '<span class="wk-mp-notification-row wk-mp-dropdown-notification-orders wk-mp-order-notification-new">
                                <a 
                                href="' . $url . '" 
                                class="wk-mp-notification-entry-description-start"
                                title="'.__("View Quote").'">
                                    <span>' . $quoteNotificationDesc . '</span>
                                </a>
                                </span>';
                            }
                        }
                        break;
                    case FormaxNotification::TYPE_QUOTE_APPROVED:
                        if ($this->formaxHelperData->isQuoteNotificationEnable()) {
                            $quoteApprovedNotificationDesc = $this->formaxHelperData->getQuoteApprovedDescription($item);

                            if (!empty($quoteApprovedNotificationDesc)) {
                                $url = '#';
                                $item['details'] = '<span class="wk-mp-notification-row wk-mp-dropdown-notification-orders wk-mp-order-notification-new">
                                <a 
                                href="' . $url . '" 
                                class="wk-mp-notification-entry-description-start"
                                title="'.__("View Quote").'">
                                    <span>' . $quoteApprovedNotificationDesc . '</span>
                                </a>
                                </span>';
                            }
                        }
                        break;
                    case FormaxNotification::TYPE_QUOTE_MESSAGE:
                        if ($this->formaxHelperData->isQuoteNotificationEnable()) {
                            $quoteMessageNotificationDesc = $this->formaxHelperData->getQuoteMessageDescription($item);

                            if (!empty($quoteMessageNotificationDesc)) {
                                $url = '#';
                                $item['details'] = '<span class="wk-mp-notification-row wk-mp-dropdown-notification-orders wk-mp-order-notification-new">
                                <a 
                                href="' . $url . '" 
                                class="wk-mp-notification-entry-description-start"
                                title="'.__("View Quote").'">
                                    <span>' . $quoteMessageNotificationDesc . '</span>
                                </a>
                                </span>';
                            }
                        }
                        break;
                    case FormaxNotification::TYPE_STATUS_SELLER:
                        if ($this->formaxHelperData->isSellerStatusNotificationEnable()) {
                            $sellerNotificationDesc = $this->formaxHelperData->getStatusSellerDescription($item);

                            if (!empty($sellerNotificationDesc)) {
                                $url = $this->urlBuilder->getUrl('marketplace/account/editprofile');
                                $item['details'] = '<span class="wk-mp-notification-row wk-mp-dropdown-notification-orders wk-mp-order-notification-processing">
                                <a 
                                href="' . $url . '" 
                                class="wk-mp-notification-entry-description-start"
                                title="'.__("View Profile").'">
                                    <span>' . $sellerNotificationDesc . '</span>
                                </a>
                                </span>';
                            }
                        }
                        break;
                    case FormaxNotification::TYPE_STATUS_PRODUCT:
                        if ($this->formaxHelperData->isStatusProductChangeNotificationEnable()) {
                            $productNotificationDesc = $this->formaxHelperData->getStatusProductDescription($item);

                            if (!empty($productNotificationDesc)) {
                                $url = '#';
                                $item['details'] = '<span class="wk-mp-notification-row wk-mp-dropdown-notification-products">
                                <a 
                                href="' . $url . '" 
                                class="wk-mp-notification-entry-description-start"
                                title="'.__("View Offer").'">
                                    <span>' . $productNotificationDesc . '</span>
                                </a>
                                </span>';
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        return $dataSource;
    }
}
