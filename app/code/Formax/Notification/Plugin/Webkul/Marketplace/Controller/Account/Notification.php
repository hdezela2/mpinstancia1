<?php

namespace Formax\Notification\Plugin\Webkul\Marketplace\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Framework\UrlInterface;
use Formax\Notification\Helper\Data as FormaxNotificationHelper;

/**
 * Webkul Marketplace Notification History Controller.
 */
class Notification
{
    /**
     * @var FormaxNotificationHelper
     */
    protected $formaxNotificationHelper;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $response;

    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * @param Context $context
     * @param UrlInterface $url
     * @param FormaxNotificationHelper $ormaxNotificationHelper
     */
    public function __construct(
        Context $context,
        UrlInterface $url,
        FormaxNotificationHelper $formaxNotificationHelper
    ) {
        $this->url = $url;
        $this->response = $context->getResponse();
        $this->formaxNotificationHelper = $formaxNotificationHelper;
    }

    /**
     * Seller's Customer history page.
     */
    public function beforeExecute()
    {
        if (!$this->formaxNotificationHelper->isModuleEnable()) {
            $norouteUrl = $this->url->getUrl('noroute');
            $this->getResponse()->setRedirect($norouteUrl);
        }
    }

    /**
     * Retrieve response object
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }
}
