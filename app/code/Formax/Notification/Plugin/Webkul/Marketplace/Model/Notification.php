<?php

namespace Formax\Notification\Plugin\Webkul\Marketplace\Model;

class Notification
{
    const TYPE_PENALTY = 6;
    const TYPE_PRICE_READJUSTMENT = 7;
    const TYPE_DISPERSION = 8;
    const TYPE_STATUS_SELLER = 9;
    const TYPE_STATUS_PRODUCT = 10;
    const TYPE_QUOTE = 11;
    const TYPE_QUOTE_APPROVED = 12;
    const TYPE_QUOTE_MESSAGE = 13;

    /**
     * Prepare Notification's Types.
     *
     * @return array
     */
    public function afterGetAllTypes(\Webkul\Marketplace\Model\Notification $subject, $result)
    {
        $newTypes = [
            self::TYPE_PENALTY => __('Penalty'),
            self::TYPE_PRICE_READJUSTMENT => __('Offer readjustment'),
            self::TYPE_DISPERSION => __('Dispersion'),
            self::TYPE_STATUS_SELLER => __('Status seller change'),
            self::TYPE_STATUS_PRODUCT => __('Status offer change'),
            self::TYPE_QUOTE => __('New Quote'),
            self::TYPE_QUOTE_APPROVED => __('Quote Approved'),
            self::TYPE_QUOTE_MESSAGE => __('New Message in Quote'),
        ];
        //$types = array_replace($result, $newTypes);
        //return $types;

        return $newTypes;
    }
}
