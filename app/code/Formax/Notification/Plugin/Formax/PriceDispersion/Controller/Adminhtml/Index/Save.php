<?php

namespace Formax\Notification\Plugin\Formax\PriceDispersion\Controller\Adminhtml\Index;

use Webkul\Marketplace\Helper\Notification as Helper;
use Psr\Log\LoggerInterface;
use Formax\Notification\Plugin\Webkul\Marketplace\Model\Notification;
use Formax\Notification\Helper\Data as FormaxNotificationHelper;

class Save
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var FormaxNotificationHelper
     */
    protected $formaxNotificationHelper;

    /**
     * @param Helper $helper
     * @param LoggerInterface $logger
     * @param FormaxNotificationHelper $formaxNotificationHelper
     */
    public function __construct(
        Helper $helper,
        LoggerInterface $logger,
        FormaxNotificationHelper $formaxNotificationHelper
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->formaxNotificationHelper = $formaxNotificationHelper;
    }

    /**
     * Insert row in table price_dispersion
     * 
     * @param \Formax\PriceDispersion\Controller\Adminhtml\Index\Save $subject
     * @param $result
     * @param \Magento\Catalog\Model\Product $product
     * @param int $itemPrice,
     * @param int $sellerId,
     * @param int $pricePercentage,
     * @param int $originalPrice,
     * @param string $allCategories
     */
    public function afterUpdateLog(
        \Formax\PriceDispersion\Controller\Adminhtml\Index\Save $subject,
        $result,
        $product,
        $itemPrice,
        $sellerId,
        $pricePercentage,
        $originalPrice,
        $allCategories
    ) {
        if ($this->formaxNotificationHelper->isDispersionNotificationEnable()) {
            try {
                $this->helper->saveNotification(Notification::TYPE_DISPERSION, $result, $product->getId());
            } catch (\Exception $e) {
                $this->logger->error('Notification DispersionSaveAfter - ' . $e->getMessage());
            }
        }

        return $result;
    }
}