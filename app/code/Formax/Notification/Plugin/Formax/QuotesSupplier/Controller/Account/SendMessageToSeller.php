<?php

namespace Formax\Notification\Plugin\Formax\QuotesSupplier\Controller\Account;

use Webkul\Marketplace\Helper\Notification as Helper;
use Psr\Log\LoggerInterface;
use Formax\Notification\Plugin\Webkul\Marketplace\Model\Notification;
use Formax\Notification\Helper\Data as FormaxNotificationHelper;

class SendMessageToSeller
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var FormaxNotificationHelper
     */
    protected $formaxNotificationHelper;

    /**
     * @param Helper $helper
     * @param LoggerInterface $logger
     * @param FormaxNotificationHelper $formaxNotificationHelper
     */
    public function __construct(
        Helper $helper,
        LoggerInterface $logger,
        FormaxNotificationHelper $formaxNotificationHelper
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->formaxNotificationHelper = $formaxNotificationHelper;
    }



    /**
     * 
     * @param \Formax\QuotesSupplier\Controller\Account\SendMessageToSeller $subject
     * @param $result,
     * @param int $quoteId
     * @param int $sellerId
     * @param int $customerId
     */
    public function afterSendMessageQuote(
        \Formax\QuotesSupplier\Controller\Account\SendMessageToSeller $subject,
        $result,
        $quoteId,
        $sellerId,
        $customerId
    ) {
        $this->helper->saveNotification(Notification::TYPE_QUOTE_MESSAGE, $quoteId, $customerId);
        return $result;
    }


    /**
     * 
     * @param \Formax\QuotesSupplier\Controller\Account\SendMessageToSeller $subject
     * @param $result,
     * @param int $quoteId
     * @param int $sellerId
     * @param int $customerId
     */
    public function afterApproveQuote(
        \Formax\QuotesSupplier\Controller\Account\SendMessageToSeller $subject,
        $result,
        $quoteId,
        $sellerId,
        $customerId
    ) {
        $this->helper->saveNotification(Notification::TYPE_QUOTE_APPROVED, $quoteId, $customerId);
        return $result;
    }
}