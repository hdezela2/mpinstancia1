<?php

namespace Formax\Notification\Plugin\Formax\QuotesSupplier\Controller\Account;

use Webkul\Marketplace\Helper\Notification as Helper;
use Psr\Log\LoggerInterface;
use Formax\Notification\Plugin\Webkul\Marketplace\Model\Notification;
use Formax\Notification\Helper\Data as FormaxNotificationHelper;

class Save
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var FormaxNotificationHelper
     */
    protected $formaxNotificationHelper;

    /**
     * @param Helper $helper
     * @param LoggerInterface $logger
     * @param FormaxNotificationHelper $formaxNotificationHelper
     */
    public function __construct(
        Helper $helper,
        LoggerInterface $logger,
        FormaxNotificationHelper $formaxNotificationHelper
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->formaxNotificationHelper = $formaxNotificationHelper;
    }

    /**
     * 
     * @param \Formax\QuotesSupplier\Controller\Account\Save $subject
     * @param $result,
     * @param object $postData
     */
    public function afterSaveQuote(
        \Formax\QuotesSupplier\Controller\Account\Save $subject,
        $result,
        $postData
    ) {
        $this->helper->saveNotification(Notification::TYPE_QUOTE, $postData['quote_id'], $postData['product_id']);
        return $result;
    }
}