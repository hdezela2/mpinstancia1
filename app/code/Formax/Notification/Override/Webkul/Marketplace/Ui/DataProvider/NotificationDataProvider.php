<?php

namespace Formax\Notification\Override\Webkul\Marketplace\Ui\DataProvider;

use Webkul\Marketplace\Model\Notification;
use Webkul\Marketplace\Model\ResourceModel\Notification\CollectionFactory;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Marketplace\Helper\Notification as NotificationHelper;

/**
 * Class NotificationDataProvider
 */
class NotificationDataProvider extends \Webkul\Marketplace\Ui\DataProvider\NotificationDataProvider
{
    /**
     * Construct
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param HelperData $helperData
     * @param NotificationHelper $notificationHelper
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        HelperData $helperData,
        NotificationHelper $notificationHelper,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $collectionFactory,
            $helperData,
            $notificationHelper,
            $meta,
            $data
        );
        
        $this->collection->addFieldToFilter(
            'type',
            ['nin' => [
                    Notification::TYPE_REVIEW,
                    Notification::TYPE_ORDER ,
                    Notification::TYPE_TRANSACTION,
                    Notification::TYPE_PRODUCT
                ]
            ]
        )->getSelect()->order('created_at DESC');
    }
}