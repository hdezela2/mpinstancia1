<?php

namespace Formax\Notification\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\Marketplace\Helper\Notification as Helper;
use Magento\Framework\Event\Observer as EventObserver;
use Psr\Log\LoggerInterface;
use Formax\Notification\Plugin\Webkul\Marketplace\Model\Notification;
use Formax\Notification\Helper\Data as FormaxNotificationHelper;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory;

/**
 * CustomerSaveAfter Observer.
 */
class CustomerSaveAfter implements ObserverInterface
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var FormaxNotificationHelper
     */
    protected $formaxNotificationHelper;

    /**
     * @param Helper $helper
     * @param LoggerInterface $logger
     * @param FormaxNotificationHelper $formaxNotificationHelper
     * @param CollectionFactory $ollectionFactory
     */
    public function __construct(
        Helper $helper,
        LoggerInterface $logger,
        FormaxNotificationHelper $formaxNotificationHelper,
        CollectionFactory $ollectionFactory
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->formaxNotificationHelper = $formaxNotificationHelper;
        $this->collectionFactory = $ollectionFactory;
    }

    /**
     * Admin customer save after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(EventObserver $observer)
    {
        if ($this->formaxNotificationHelper->isSellerStatusNotificationEnable()) {
            /** @var \Magento\Customer\Api\Data\CustomerInterface $customer */
            $customer = $observer->getEvent()->getData('customer_data_object');
            /** @var \Magento\Customer\Api\Data\CustomerInterface $prevCustomer */
            $prevCustomer = $observer->getEvent()->getData('orig_customer_data_object');

            try {
                if ($customer && $prevCustomer) {
                    if ($prevCustomer->getCustomAttribute('wkv_dccp_state_details') &&
                        $customer->getCustomAttribute('wkv_dccp_state_details')) {
                        $customerId = $customer->getId();
                        $prevStatus = $prevCustomer->getCustomAttribute('wkv_dccp_state_details')->getValue();
                        $currentStatus = $customer->getCustomAttribute('wkv_dccp_state_details')->getValue();

                        if ($this->isSeller($customerId) && $prevStatus != $currentStatus) {
                            $this->helper->saveNotification(Notification::TYPE_STATUS_SELLER, $customerId, $customerId);
                        }
                    }
                }
            } catch (\Exception $e) {
                $this->logger->error('Notification AdminhtmlCustomerSaveAfter - ' . $e->getMessage());
            }
        }

        return $this;
    }

    /**
     * Retrive if customer logged in is seller
     * 
     * @param int $customerId
     * @return bool
     */
    public function isSeller($customerId)
    {
        $sellerStatus = false;
        $model = $this->collectionFactory->create()
            ->addFieldToFilter('seller_id', $customerId)
            ->addFieldToFilter('store_id', 0);
        foreach ($model as $value) {
            $sellerStatus = $value->getIsSeller();
        }

        return (bool)$sellerStatus;
    }
}
