<?php

namespace Formax\Notification\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\Marketplace\Helper\Notification as Helper;
use Magento\Framework\Event\Observer as EventObserver;
use Psr\Log\LoggerInterface;
use Formax\Notification\Plugin\Webkul\Marketplace\Model\Notification;
use Formax\Notification\Helper\Data as FormaxNotificationHelper;

class ReadjustmentPriceSaveAfter implements ObserverInterface
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var FormaxNotificationHelper
     */
    protected $formaxNotificationHelper;

    /**
     * @param Helper $helper
     * @param LoggerInterface $logger
     * @param FormaxNotificationHelper $formaxNotificationHelper
     */
    public function __construct(
        Helper $helper,
        LoggerInterface $logger,
        FormaxNotificationHelper $formaxNotificationHelper
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->formaxNotificationHelper = $formaxNotificationHelper;
    }

    /**
     * add notification status change
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        if ($this->formaxNotificationHelper->isPriceReadjustmentNotificationEnable()) {
            $model = $observer->getEvent()->getObject();

            try {
                $this->helper->saveNotification(Notification::TYPE_PRICE_READJUSTMENT, $model->getData('entity_id'), $model->getData('mageproduct_id'));
            } catch (\Exception $e) {
                $this->logger->error('Notification ReadjustmentPriceSaveAfter - ' . $e->getMessage());
            }
        }
    }
}
