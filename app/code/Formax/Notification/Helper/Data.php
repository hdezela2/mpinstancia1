<?php

namespace Formax\Notification\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Webkul\MpAssignProduct\Model\ItemsFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as CollectionItemsFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as CollectionAssociatesFactory;
use Formax\Penalty\Model\PenaltyFactory;
use Formax\PriceDispersion\Model\ProductPriceFactory as DispersionFactory;
use Webkul\CategoryProductPrice\Model\ProductPriceFactory;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Customer\Model\CustomerFactory;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    /**
     * Assign Items Factory
     *
     * @var Webkul\MpAssignProduct\Model\ItemsFactory
     */
    protected $itemsFactory;

    /**
     * Assign Items CollectionFactory
     *
     * @var Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory
     */
    protected $collectionItemsFactory;

    /**
     * Assign Associates CollectionFactory
     *
     * @var Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory
     */
    protected $collectionAssociatesFactory;

    /**
     * Assign Items Factory
     *
     * @var Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Penalty Factory
     *
     * @var Formax\Penalty\Model\PenaltyFactory
     */
    protected $penaltyFactory;

    /**
     * Product Price Factory
     *
     * @var Webkul\CategoryProductPrice\Model\ProductPriceFactory
     */
    protected $productPriceFactory;

    /**
     * Dispersion Factory
     *
     * @var Formax\PriceDispersion\Model\ProductPriceFactory
     */
    protected $dispersionFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var Webkul\Requestforquote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param Context $context
     * @param ProductRepositoryInterface $productRepository
     * @param ItemsFactory $itemsFactory
     * @param CollectionItemsFactory $collectionItemsFactory
     * @param CollectionAssociatesFactory $collectionItemsFactory
     * @param PenaltyFactory $penaltyFactory
     * @param ProductPriceFactory $productPriceFactory
     * @param DispersionFactory $dispersionFactory
     * @param PriceCurrencyInterface $priceCurrency
     * @param CustomerFactory $customerFactory
     * @param QuoteFactory $quoteFactory
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        ItemsFactory $itemsFactory,
        CollectionItemsFactory $collectionItemsFactory,
        CollectionAssociatesFactory $collectionAssociatesFactory,
        PenaltyFactory $penaltyFactory,
        ProductPriceFactory $productPriceFactory,
        DispersionFactory $dispersionFactory,
        PriceCurrencyInterface $priceCurrency,
        CustomerFactory $customerFactory,
        QuoteFactory $quoteFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->itemsFactory = $itemsFactory;
        $this->collectionItemsFactory = $collectionItemsFactory;
        $this->collectionAssociatesFactory = $collectionAssociatesFactory;
        $this->productRepository = $productRepository;
        $this->penaltyFactory = $penaltyFactory;
        $this->productPriceFactory = $productPriceFactory;
        $this->dispersionFactory = $dispersionFactory;
        $this->priceCurrency = $priceCurrency;
        $this->customerFactory = $customerFactory;
        $this->quoteFactory = $quoteFactory;
        $this->_storeManager = $storeManager;

        parent::__construct($context);
    }

    /**
     * Get Product Status Change Notification Message
     *
     * @param array $rowData
     * @return string
     */
    public function getStatusProductDescription($rowData)
    {
        $message = '';

        if (isset($rowData['notification_row_id']) && isset($rowData['notification_id'])) {
            $product = $this->productRepository->getById((int)$rowData['notification_row_id']);
            $assignItem = $this->itemsFactory->create()->load((int)$rowData['notification_id']);
            $message = $assignItem->getData('status') == 0
                ? __('Product %1 - %2 has been Disapproved. The reason is: %3', $product->getSku(), $product->getName(), $assignItem->getData('comment'))
                : __('Product %1 - %2 has been Approved.', $product->getSku(), $product->getName());
        }

        return $message;
    }

    /**
     * Get Seller Status Change Notification Message
     *
     * @param array $rowData
     * @return string
     */
    public function getStatusSellerDescription($rowData)
    {
        $message = '';

        if (isset($rowData['notification_id'])) {
            $customerObj = $this->customerFactory->create();
            $customer = $customerObj->load((int)$rowData['notification_id']);
            $statusText = $customer->getResource()->getAttribute('wkv_dccp_state_details')->getSource()->getOptionText($customer->getData('wkv_dccp_state_details'));
            $message = __('You seller status has been changed to %1', $statusText);
        }

        return $message;
    }

    /**
     * Get Penalty Notification Message
     *
     * @param array $rowData
     * @return string
     */
    public function getPenaltyDescription($rowData)
    {
        $message = '';

        if (isset($rowData['notification_id'])) {
            $penalty = $this->penaltyFactory->create()->load((int)$rowData['notification_id']);
            $message = __('You have received penalty code %1 - Description: %2', $penalty->getPenaltyCode(), $penalty->getDescription());
        }

        return $message;
    }

    /**
     * Get Reajustment Price Notification Message
     *
     * @param array $rowData
     * @return string
     */
    public function getReadjustmentPriceDescription($rowData)
    {
        $message = '';

        if (isset($rowData['notification_row_id']) && isset($rowData['notification_id'])) {
            $product = $this->productRepository->getById((int)$rowData['notification_row_id']);
            $productPrice = $this->productPriceFactory->create()->load((int)$rowData['notification_id']);
            $message = __(
                'Offer %1 - %2  had a offer adjusment. Old offer: %3 New offer: %4',
                $product->getSku(),
                $product->getName(),
                number_format($productPrice->getMageproductPrice(), 1, ',', '.'),
                number_format($productPrice->getUpdatedproductPrice(), 1, ',', '.')
            );
        }

        return $message;
    }

    /**
     * Get Dispersion Notification Message
     *
     * @param array $rowData
     * @return string
     */
    public function getDispersionDescription($rowData)
    {
        $message = '';

        if (isset($rowData['notification_row_id']) && isset($rowData['notification_id'])) {
            $product = $this->productRepository->getById((int)$rowData['notification_row_id']);
            $dispersion = $this->dispersionFactory->create()->load((int)$rowData['notification_id']);
            if ($assignItem = $this->getAssignItem($dispersion->getMageproductId(), $dispersion->getSellerId())) {
                $dispersionStatusMsg = $assignItem->getDisDispersion() == 0 ? __('Enabled') : __('Disabled');
                $message = __(
                    'Offer %1 - %2 has been %3 by dispersion.',
                    $product->getSku(),
                    $product->getName(),
                    $dispersionStatusMsg
                );
            }
        }

        return $message;
    }

    /**
     * Get Quote Notification Message
     *
     * @param array $rowData
     * @return string
     */
    public function getQuoteDescription($rowData)
    {
        $quote = $this->quoteFactory->create();
        $quoteModel = $quote->load((int)$rowData['notification_id']);
        $message = '';
        if (isset($rowData['notification_row_id']) && isset($rowData['notification_id'])) {
            $message = __('New quote #%1 - %2', $quoteModel->getId(), $quoteModel->getDescription());
        }

        return $message;
    }

    /**
     * Get Quote Notification Message
     *
     * @param array $rowData
     * @return string
     */
    public function getQuoteMessageDescription($rowData)
    {
        $message = '';
        if (isset($rowData['notification_row_id']) && isset($rowData['notification_id'])) {
            $message = __('New message in quote');
        }

        return $message;
    }

    /**
     * Get Quote Notification Message
     *
     * @param array $rowData
     * @return string
     */
    public function getQuoteApprovedDescription($rowData)
    {
        $message = '';
        if (isset($rowData['notification_row_id']) && isset($rowData['notification_id'])) {
            $message = __('Quote approved');
        }

        return $message;
    }

    /**
     * Retrieve if module is enabled
     *
     * @return bool
     */
    public function isModuleEnable()
    {
        $config = 'seller_notification/general_settings/enable';
        return (bool)$this->scopeConfig->getValue($config, ScopeInterface::SCOPE_WEBSITES);
    }

    /**
     * Retrieve if penalty notification is enabled
     *
     * @return bool
     */
    public function isPenaltyNotificationEnable()
    {
        if ($this->isModuleEnable()) {
            $config = 'seller_notification/general_settings/enable_penalty';
            return (bool)$this->scopeConfig->getValue($config, ScopeInterface::SCOPE_WEBSITES);
        }

        return false;
    }

    /**
     * Retrieve if dispersion notification is enabled
     *
     * @return bool
     */
    public function isDispersionNotificationEnable()
    {
        if ($this->isModuleEnable()) {
            $config = 'seller_notification/general_settings/enable_dispersion';
            return (bool)$this->scopeConfig->getValue($config, ScopeInterface::SCOPE_WEBSITES);
        }

        return false;
    }

    /**
     * Retrieve if dispersion notification is enabled
     *
     * @return bool
     */
    public function isQuoteNotificationEnable()
    {
        if ($this->isModuleEnable()) {
            $config = 'seller_notification/general_settings/enable_quote';
            return (bool)$this->scopeConfig->getValue($config, ScopeInterface::SCOPE_WEBSITES);
        }

        return false;
    }

    /**
     * Retrieve if price readjustment notification is enabled
     *
     * @return bool
     */
    public function isPriceReadjustmentNotificationEnable()
    {
        if ($this->isModuleEnable()) {
            $config = 'seller_notification/general_settings/price_readjustment';
            return (bool)$this->scopeConfig->getValue($config, ScopeInterface::SCOPE_WEBSITES);
        }

        return false;
    }

    /**
     * Retrieve if status seller change notification is enabled
     *
     * @return bool
     */
    public function isSellerStatusNotificationEnable()
    {
        if ($this->isModuleEnable()) {
            $config = 'seller_notification/general_settings/seller_status';
            return (bool)$this->scopeConfig->getValue($config, ScopeInterface::SCOPE_WEBSITES);
        }

        return false;
    }

    /**
     * Retrieve if status product change notification is enabled
     *
     * @return bool
     */
    public function isStatusProductChangeNotificationEnable()
    {
        if ($this->isModuleEnable()) {
            $config = 'seller_notification/general_settings/product_status';
            return (bool)$this->scopeConfig->getValue($config, ScopeInterface::SCOPE_WEBSITES);
        }

        return false;
    }

    /**
     * Get assign item by product and seller ID
     *
     * @param int $productId
     * @param int $sellerId
     * @return array
     */
    public function getAssignItem($productId, $sellerId)
    {
        $result = false;

        if ((int)$productId > 0 && (int)$sellerId > 0) {
            $collection = $this->collectionItemsFactory->create();
            $collection->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('seller_id', $sellerId)
                ->setPageSize(1);

            if ($collection) {
                foreach ($collection as $item) {
                    $result = $item;
                }
            }
        }

        if (!$result && (int)$productId > 0 && (int)$sellerId > 0) {
            $collection = $this->collectionAssociatesFactory->create();
            $collection->addFieldToFilter('product_id', $productId)
                ->getSelect()
                ->joinLeft(
                    ['i' => $collection->getResource()->getTable('marketplace_assignproduct_items')],
                    'i.id = main_table.parent_id'
                )->where('i.seller_id=?', $sellerId);

            if ($collection) {
                foreach ($collection as $item) {
                    $result = $item;
                }
            }
        }

        return $result;
    }

    /**
     * get formatted price
     *
     * @param float $price
     * @return string
     */
    public function currencyFormat($price)
    {
        return strip_tags($this->priceCurrency->format($price));
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getWebsiteCode(){
        return $this->_storeManager->getWebsite()->getCode();
    }
}
