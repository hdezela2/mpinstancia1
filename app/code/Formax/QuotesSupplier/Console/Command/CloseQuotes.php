<?php
namespace Formax\QuotesSupplier\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Formax\ActionLogs\Helper\Data as HelperActionLog;
use Magento\Store\Model\App\Emulation;
use Formax\QuotesSupplier\Logger\Logger;
use Magento\Framework\App\ResourceConnection;
use Symfony\Component\Console\Input\InputOption;
use Magento\Framework\App\State;

class CloseQuotes extends Command{
    protected $_resource;
    protected $_logger;
    protected $_helperActionLog;
    protected $_emulate;
    const AREA = 'AREA';
    const STORE = 'STORE';
    const STATUS = 'STATUS';
    protected $_state;
    
    public function __construct(
        ResourceConnection $resource,
        Logger $logger,
        HelperActionLog $helperActionLog,
        Emulation $emulate,
        State $area
    ) {
        $this->_resource        = $resource;
        $this->_logger          = $logger;
        $this->_helperActionLog = $helperActionLog;
        $this->_emulate         = $emulate;
        $this->_state           = $area;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure() {
        $this->setName('xpec:close:quotes')
            ->setDescription('Cierra solicitudes automaticamente');
        $this->addOption(
            self::AREA,
            null,
            InputOption::VALUE_OPTIONAL,
            'Area'
        );
        $this->addOption(
            self::STORE,
            null,
            InputOption::VALUE_OPTIONAL,
            'Store'
        );
        $this->addOption(
            self::STATUS,
            null,
            InputOption::VALUE_OPTIONAL,
            'Status'
        );
    }


    /**
     * Regenerate Url Rewrites
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output,$area = 'frontend',$storeId=0,$status=2)
    {
        if ($area = $input->getOption(self::AREA)) {
            $output->writeln('<info>Area `' . $area . '`</info>');
        }
        if ($storeId = $input->getOption(self::STORE)) {
            $output->writeln('<info>Store `' . $storeId . '`</info>');
        }
        if ($status = $input->getOption(self::STATUS)) {
            $output->writeln('<info>Status `' . $status . '`</info>');
        }else{
            $status=0;
        }
        $this->_state->setAreaCode($area);

        $this->_emulate->startEnvironmentEmulation($storeId);

        $sql = "UPDATE 
                        requestforquote_quote 
                    SET 
                        status=".$status." 
                    WHERE 
                        evaluation_end <= '".date('Y-m-d')."'";
        $con = $this->_resource->getConnection();
        $con->query($sql);
        $this->_logger->info("actualice estado: ".$sql);

        $extraData      = array(
            'action'   => 'Cerrar Cotización',
            'status'   => 'success'
        );
        $actionName     ='Automatico';
        $profileName    ='N/A';
        //$this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);


        //$output->writeln(date('Y-m-d H:i:s').' ::Regenerando reporte');
        //$this->_logger->info("info log");

        
        //$output->writeln('');
        //$output->writeln('Reindexation...');
        //shell_exec('php bin/magento indexer:reindex');
        // shell_exec('php bin/magento cache:clean');
        // shell_exec('php bin/magento cache:flush');
        $this->_emulate->stopEnvironmentEmulation();
        $output->writeln(date('Y-m-d H:i:s').' ::Finished ');
    }

}
