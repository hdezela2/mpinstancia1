<?php
namespace Formax\QuotesSupplier\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\App\ObjectManager;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Serialize\SerializerInterface;
use Psr\Log\LoggerInterface;

class Relationshiporderloan implements ObserverInterface
{
    
    /**
    * @var \Magento\Framework\HTTP\ZendClientFactory
    */
    protected $_httpClientFactory;

    public $scopeConfig;

    protected $eav;

    private $_customer;

    private $countryFactory;

    private $_resource;

    private $_serializer;

    /**
     * @var Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
    * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
    */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Framework\App\ResourceConnection $resource,
        SerializerInterface $serializer,
        LoggerInterface $logger
        ){
        $this->scopeConfig = $scopeConfig;
        $this->_customer = $customer;
        $this->_resource = $resource;
        $this->_serializer = $serializer;
        $this->logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer){
        $order          = $observer->getEvent()->getOrder();
        $orderId        = $order->getId();
        $incrementalId  = $order->getIncrementId();
        $con            = $this->_resource->getConnection();
        
        try {
            
            if($order->getData('requestforquote_info')==0){
                $sql = "SELECT qio.value
                        FROM 
                            sales_order so
                        INNER JOIN quote_item qi ON(so.quote_id=qi.quote_id)
                        INNER JOIN quote_item_option qio ON(qi.item_id=qio.item_id AND qi.product_id=qio.product_id)
                        WHERE 
                            so.entity_id =".$orderId;
                $result = $con->fetchOne($sql);

                $data = $this->_serializer->unserialize($result);
                $id = (isset($data['quoteInfoId']))?$data['quoteInfoId']:0;

                $order->setData('requestforquote_info',$id);
                $order->save();
            }

        } catch (\Exception $e) {
            $this->logger->critical('Error message: ' . $e->getMessage());
        }
    }

}
