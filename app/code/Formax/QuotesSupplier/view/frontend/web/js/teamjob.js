require([
    "jquery",'domReady!','mage/calendar'
], function($){
    'use strict';
    $(document).ready(function($,ko) {
        var dateOptions = {
            minDate: 0,
            dateFormat:"yy-mm-dd",
            buttonText: "",
            changeMonth: true,
            changeYear: true,
            showOn: "both",
            monthNames: ["Enero","Febrero","Marzo","Abril","May","Junio","Julio",
            "Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
            monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
            dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
            dayNamesShort: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"],
            dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"]
        };
        $('.hasDatepicker').datepicker(dateOptions);

        $(".btn-save-team-jobs").off("click");
        $(".btn-save-team-jobs").on("click",function(e){
            e.preventDefault();
            saveTeamJob();
        });

        $(".btn_add_teamwork").off("click");
        $(".btn_add_teamwork").on("click",function(e){
            e.preventDefault();
            agregarEquipodeTrabajo();
        });

        $(".btn_add_services").off("click");
        $(".btn_add_services").on("click",function(e){
            e.preventDefault();
            agregarServicios();
        });

        eventEliminarGrupoTrabajo();
        eventEliminarServicio();

        function saveTeamJob(){
            var url = window.BASE_URL+"/quotessupplier/index/saveteam";
            var data = $("#formteamservices").serialize();
            var data = new FormData();

            //Form data
            var form_data = $('#formteamservices').serializeArray();
            $.each(form_data, function (key, input) {
                data.append(input.name, input.value);
            });

            //File data
            for(var index=0;index<=$('input[name="informe[]"]').length-1;index++){
                var file_data = $('input[name="informe[]"]')[index].files;
                for (var i = 0; i < file_data.length; i++) {
                    data.append("informe["+index+"]", file_data[i]);
                }
            }
            for(var index=0;index<=$('input[name="cv[]"]').length-1;index++){
                var file_data = $('input[name="cv[]"]')[index].files;
                for (var i = 0; i < file_data.length; i++) {
                    data.append("cv["+index+"]", file_data[i]);
                }
            }


            $.ajax({
                showLoader: true,
                url: url,
                data: data,
                method: "post",
                processData: false,
                contentType: false,
            }).done(function (data) {
                location.reload();
            });
        }

        function agregarEquipodeTrabajo(){
            var html = $(".formbankteamwork .teamwork-group").clone();
            var id = Math.floor(Math.random() * 1000);
            html.find(".inputfile").attr('id', 'cv-'+id);
            html.find(".inputfile").next().attr('for', 'cv-'+id);
            $(".formworkjob").append(html);

            eventEliminarGrupoTrabajo();
        }

        function agregarServicios(){
            var html = $( ".formservicesblank .teamwork-services" ).clone();
            var id = Math.floor(Math.random() * 1000);
            html.find(".deliver-date-new-input").attr("id",'deliver-date-'+id);
            html.find(".deliver-date-new-label").attr("for",'deliver-date-'+id);
            html.find(".informe-new-input").attr("id",'informe-'+id);
            html.find(".informe-new-label").attr("for",'informe-'+id);
            html.find(".cv-new-input").attr("id",'cv-'+id);
            html.find(".cv-new-label").attr("for",'cv-'+id);
            $(".requestforquote_quote_deliverable-services").append(html);
            var datepicker = html.find("input.hasDatepicker");
            datepicker.next().remove();
            datepicker.removeClass('_has-datepicker');
            datepicker.datepicker(dateOptions);

            eventEliminarServicio();
        }

        function eventEliminarServicio(){
            $(".btn_del_services").off("click");
            $(".btn_del_services").on("click",function(e){
                e.preventDefault();
                if($(".requestforquote_quote_deliverable-services .teamwork-services").length && $(".requestforquote_quote_deliverable-services .teamwork-services").length>1 ){
                    var obj = $(this).parents(".teamwork-services");
                    if((typeof $(this).attr('data-del') !== 'undefined') && $(this).attr('data-del').length){
                        var id = $(this).attr('data-del');
                        delService(id,obj);
                    }else{
                        $(this).parents(".teamwork-services").remove();
                    }

                }
            });
        }

        function eventEliminarGrupoTrabajo(){
            $(".btn_del_teamjob").off("click");
            $(".btn_del_teamjob").on("click",function(e){
                e.preventDefault();
                if($(".requestforquote_quote_teamwork .teamwork-group").length && $(".requestforquote_quote_teamwork .teamwork-group").length>1 ){
                    var obj = $(this).parents(".teamwork-group");
                    if((typeof $(this).attr('data-del') !== 'undefined') && $(this).attr('data-del').length){
                        var id = $(this).attr('data-del');
                        delTeam(id,obj);
                    }else{
                        $(this).parents(".teamwork-group").remove();
                    }
                }
            });
        }

        function delService(id,obj){
            var url = window.BASE_URL+"quotessupplier/index/delservices";
            $.ajax({
                showLoader: true,
                url: url,
                data: {
                    id:id,
                    action:'delservices'
                },
                method: "post"
            }).done(function (data) {
                $(obj).remove();
            });
        }

        function delTeam(id,obj){
            var url = window.BASE_URL+"quotessupplier/index/delservices";
            $.ajax({
                showLoader: true,
                url: url,
                data: {
                    id:id,
                    action:'delteam'
                },
                method: "post"
            }).done(function (data) {
                $(obj).remove();
            });
        }

    });
});
