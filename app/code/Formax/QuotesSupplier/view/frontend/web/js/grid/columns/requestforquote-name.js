define([
    'underscore',
    'Magento_Ui/js/grid/columns/select'
], function (_, Column) {
    'use strict';

    return Column.extend({
        defaults: {
            bodyTmpl: 'Formax_QuotesSupplier/ui/grid/cells/requestforquote-name'
        },
        getQuotedNameValue: function (row) {
            return row.description;
        },
        getOrganizationAndProductNameValue: function (row) {
            let subtitle;
            if (row.organization) {
                subtitle = row.organization.toString().toUpperCase() + ' / ' + row.product_name;
            } else {
                subtitle = row.product_name;
            }
            return subtitle;
        }
    });
});