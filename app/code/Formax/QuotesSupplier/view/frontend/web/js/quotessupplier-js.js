define([
        'jquery',
        'underscore',
        'mage/template'
    ], function($){
          var QuotesSupplier = {
            main: function(config, element) {
              $target_ = $('#quotessupplier-popup');
              var $element = $('.quote_button');
              var options = {
                responsive: true,
                innerScroll: true,
                modalClass: 'quotessupplier-modal',
                title: 'Cotiza un mejor precio',
                closeText: 'Cerrar',
                buttons: []
              };
              $target_.modal(options);
              $(".quote_button button").addClass("disabled");

              $(document).on('submit', config.form, function(event) {
                  event.preventDefault();
                  $target_.modal('openModal');
              })

              $(document).on('submit', "#form_quotesupplier", function(event) {
                  event.preventDefault();
                  if($(this).find("button").hasClass("disabled")){
                    return;
                  }
                  var selected_suppliers = [];
                  $.each($("input[name='suppliers_id_selected']:visible:checked"), function(){
                      selected_suppliers.push($(this).val());
                  });
                  var quoteall = 0;
                  var quotenotif = 0;
                  if($('input[name="quoteall"]').is(":checked")){
                    quoteall = 1;
                  }
                  if($('input[name="quotenotif"]').is(":checked")){
                    quotenotif = 1;
                  }
                  var productId = $('input[name="selected_configurable_option"]').val() || config.product_id;
                  
                  $.ajax({
                      showLoader: true,
                      url: config.url,
                      data: {'ajax':1,'product_id':productId,'selected_suppliers':selected_suppliers.join(","),'subject':$('#form_quotesupplier .col_1quote .input_disable').text(),'description':$('textarea[name="quotecomments"]').val(),
                              'tax_class_id':'0',                      
                              'bulk_order_qty':$('input[name="bulk_order_qty"]').val(),
                              'quotedatedelivery':$('input[name="quotedatedelivery"]').val(),
                              'quotedatequestion':$('input[name="quotedatequestion"]').val(),
                              'quoteaddress':$('input[name="quoteaddress"]').val(),
                              'quotecomments':$('textarea[name="quotecomments"]').val(),
                              'quotedatereceive':$('input[name="quotedatereceive"]').val(),
                              'quoteall':quoteall,
                      'quotenotif':quotenotif
                    },
                      type: "POST",
                      dataType: 'json'
                  }).done(function (data) {
                    $target_.modal('closeModal');
                    $("html, body").animate({ scrollTop: 0 }, 300)
                  });
              });
              $(document).on("change","input[name='suppliers_id_selected']",function(){
                $("button.quote_button").addClass("disabled");
                $.each($("input[name='suppliers_id_selected']:checked"), function(){
                  if($(this).is(":checked")){
                    $("button.quote_button").removeClass("disabled");             
                  }
                });
              })
              $("input[name='quoteall']").on("change", function(){
                if($(this).is(":checked")){
                  $("input[name='suppliers_id_select_all']").attr('checked', true);
                  $("input[name='suppliers_id_selected']:visible").attr('checked', true);
                  
                }
              })
              $(document).ready(function(){
                $(".page-header .panel.wrapper .header.links .quotenotifications").appendTo("header.page-header");
              })
            }
          }
        return {
            'quotessupplier-js': QuotesSupplier.main
        };
    }

);
