define([
    'underscore',
    'Magento_Ui/js/grid/columns/select'
], function (_, Column) {
    'use strict';

    return Column.extend({
        defaults: {
            bodyTmpl: 'Formax_QuotesSupplier/ui/grid/cells/status'
        },
        getCssClassStatusColor: function (row) {
            let cssClasscolorStaus = 'gray'
            switch(row.requestforquote_status) {
                case 'Pendiente':
                    cssClasscolorStaus = 'yellow';
                    break;
                case 'Publicada':
                    cssClasscolorStaus = 'green';
                    break;
                case 'En Evaluación':
                    cssClasscolorStaus = 'blue';
                    break;
                case 'Adjudicado':
                case 'Adjudicada':
                case 'Proveedor':
                    cssClasscolorStaus = 'dark-blue';
                    break;
                default:
                    cssClasscolorStaus = 'gray';
                    break;

            }
            return cssClasscolorStaus;
        },
        getStatusValue: function (row) {
            return row.requestforquote_status.toString().toUpperCase();
        }
    });
});
