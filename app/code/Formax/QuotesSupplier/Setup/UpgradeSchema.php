<?php
namespace Formax\QuotesSupplier\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();


        $rfq =  $setup->getTable('requestforquote_quote');
        if (version_compare($context->getVersion(), '2.0.4', '<')) {
            
            if ($setup->getConnection()->isTableExists($rfq) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $rfq,
                    'quotedatedelivery',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        'default' => 0,
                        'nullable' => false,
                        'after' => 'updated_at',
                        'comment' => 'Fecha cotizacion'
                    ]
                    );
                $connection->addColumn(
                    $rfq,
                    'quoteaddress',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => '64m',
                        'nullable' => false,
                        'after' => 'updated_at',
                        'comment' => 'Direccion'
                    ]
                );
                $connection->addColumn(
                    $rfq,
                    'quotecomments',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => '64m',
                        'nullable' => false,
                        'after' => 'updated_at',
                        'comment' => 'Comentarios'
                    ]
                );
                $connection->addColumn(
                    $rfq,
                    'quotedatereceive',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        'default' => 0,
                        'nullable' => false,
                        'after' => 'updated_at',
                        'comment' => 'Recibir cotizacion hasta'
                    ]
                );
                $connection->addColumn(
                    $rfq,
                    'quotedatequestion',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        'default' => 0,
                        'nullable' => false,
                        'after' => 'updated_at',
                        'comment' => 'Plazo para recibir preguntas'
                    ]
                );
                $connection->addColumn(
                    $rfq,
                    'quoteall',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        'unsigned' => true,
                        'default' => 0,
                        'after' => 'updated_at',
                        'comment' => 'Cotizar todos los proveedores'
                    ]
                );
                $connection->addColumn(
                    $rfq,
                    'quotenotif',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        'unsigned' => true,
                        'default' => 0,
                        'after' => 'updated_at',
                        'comment' => 'Notificacion'
                    ]
                );
            }
        }

        $rfqc =  $setup->getTable('requestforquote_quote_conversation');
        if (version_compare($context->getVersion(), '2.0.5', '<')) {
            
            if ($setup->getConnection()->isTableExists($rfqc) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $rfqc,
                    'quote_shipping_price',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'default' => 0,
                        'nullable' => false,
                        'after' => 'quote_price',
                        'comment' => 'Costo de envio'
                    ]
                    );
            }
        }

        if (version_compare($context->getVersion(), '2.0.6', '<')) {
            $this->addIndexToRFQTables($setup);
        }

        if (version_compare($context->getVersion(), '2.0.7', '<')) {
            $this->addColumnRequestforQuoteInfoOnSale($setup);
        }

        if (version_compare($context->getVersion(), '2.3.0', '<')) {
            $this->tableTeamJob($setup);
            $this->tableServices($setup);
        }

        if (version_compare($context->getVersion(), '2.4.0', '<')) {
            $this->addColumnRut($setup);
            $this->addColumnAciveTeam($setup);
            $this->addColumnAciveService($setup);
        }

        if (version_compare($context->getVersion(), '2.5.1', '<')) {
            $this->addShowNotif($setup);
        }

        if (version_compare($context->getVersion(), '2.6.1', '<')) {
            $this->addAdditionalDataTeam($setup);
        }

        $setup->endSetup();
    }

    public function tableTeamJob($setup)
    {
        $nameTable = 'xpec_teamjob_loan';
        $table = $setup->getConnection()
            ->newTable($setup->getTable($nameTable))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true,'nullable' => false,'primary' => true],
                'Id'
            )
            ->addColumn(
                'quote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Quote Id'
            )
            ->addColumn(
                'rol',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '255',
                ['nullable' => false, 'default' => ''],
                'Rol'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '255',
                ['nullable' => false, 'default' => ''],
                'Name'
            )
            ->addColumn(
                'rut',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '255',
                ['nullable' => false, 'default' => ''],
                'Rut'
            )
            ->addColumn(
                'hours',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Hours'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false, 
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ],
                'Fecha de Creación'
            )
            ->addColumn(
                'additional_data',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '4M',
                ['nullable' => false, 'default' => ''],
                'Additional Data'
            )
            ->setComment("Xpec Team Group");
        $setup->getConnection()->createTable($table);
        $setup->getConnection()->addIndex(
            $setup->getTable($nameTable),
            $setup->getIdxName($nameTable, ['quote_id']),
            ['quote_id']
        );
    }

    public function tableServices($setup)
    {
        $nameTable = 'xpec_services';
        $table = $setup->getConnection()
            ->newTable($setup->getTable($nameTable))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true,'nullable' => false,'primary' => true],
                'Id'
            )
            ->addColumn(
                'quote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Quote Id'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '255',
                ['nullable' => false, 'default' => ''],
                'Name'
            )
            ->addColumn(
                'details',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '2M',
                ['nullable' => false, 'default' => ''],
                'Details'
            )
            ->addColumn(
                'deliver_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false, 
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ],
                'Deliver Date'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false, 
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ],
                'Fecha de Creación'
            )
            ->addColumn(
                'additional_data',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '4M',
                ['nullable' => false, 'default' => ''],
                'Additional Data'
            )
            ->setComment("Xpec Deliver Services");
        $setup->getConnection()->createTable($table);
        $setup->getConnection()->addIndex(
            $setup->getTable($nameTable),
            $setup->getIdxName($nameTable, ['quote_id']),
            ['quote_id']
        );
    }

    public function addColumnRequestforQuoteInfoOnSale($setup)
    {
        $so =  $setup->getTable('sales_order');
        if ($setup->getConnection()->isTableExists($so) == true) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $so,
                'requestforquote_info',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'default' => 0,
                    'nullable' => false,
                    'comment' => 'Id requestforquote_quote_info'
                ]
            );

            $connection->addIndex(
                $setup->getTable('sales_order'),
                $setup->getIdxName('sales_order', ['requestforquote_info']),
                ['requestforquote_info']
            );
        }
    }

    public function addColumnRut($setup)
    {
        $tj =  $setup->getTable('xpec_teamjob_loan');
        if ($setup->getConnection()->isTableExists($tj) == true) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tj,
                'rut',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'default' => '',
                    'nullable' => false,
                    'comment' => 'Rut'
                ]
            );

        }
    }

    public function addColumnAciveTeam($setup)
    {
        $tj =  $setup->getTable('xpec_teamjob_loan');
        if ($setup->getConnection()->isTableExists($tj) == true) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tj,
                'active',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'default' => 0,
                    'nullable' => false,
                    'comment' => 'Active'
                ]
            );

        }
    }

    public function addColumnAciveService($setup)
    {
        $tj =  $setup->getTable('xpec_services');
        if ($setup->getConnection()->isTableExists($tj) == true) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tj,
                'active',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'default' => 0,
                    'nullable' => false,
                    'comment' => 'Active'
                ]
            );

        }
    }

    public function addShowNotif($setup)
    {
        $tj =  $setup->getTable('requestforquote_quote_info');
        if ($setup->getConnection()->isTableExists($tj) == true) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tj,
                'shownotif',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'default' => 1,
                    'nullable' => false,
                    'comment' => 'Show Notif'
                ]
            );

        }
    }

    public function addAdditionalDataTeam($setup)
    {
        $tj =  $setup->getTable('xpec_teamjob_loan');
        if ($setup->getConnection()->isTableExists($tj) == true) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tj,
                'additional_data',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'default' => '',
                    'nullable' => false,
                    'comment' => 'Additional Data'
                ]
            );

        }
    }

    public function addIndexToRFQTables($setup)
    {
        $rfqq = $setup->getTable('requestforquote_quote');
        if ($setup->getConnection()->isTableExists($rfqq) == true) {
            $connection = $setup->getConnection();
            $connection->addIndex(
                $rfqq,
                'DCCP_REQUESTFORQUOTE_QUOTE',
                [
                    'customer_id'
                ]
            );
        }
    }
}
