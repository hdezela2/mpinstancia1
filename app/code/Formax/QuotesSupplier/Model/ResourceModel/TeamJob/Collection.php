<?php
namespace Formax\QuotesSupplier\Model\ResourceModel\TeamJob;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'formax_xpec_teamjob_loan_collection';
	protected $_eventObject = 'xpec_teamjob_loan_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Formax\QuotesSupplier\Model\TeamJob', 'Formax\QuotesSupplier\Model\ResourceModel\TeamJob');
	}

}