<?php

namespace Formax\QuotesSupplier\Model\ResourceModel\Quote\Grid;

use Magento\Backend\Model\Auth\Session as AdminSession;

class Collection extends \Webkul\Requestforquote\Model\ResourceModel\Quote\Grid\Collection
{
    /**
     * @var Magento\Backend\Model\Auth\Session
     */
    protected $adminSession;

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param mixed|null $mainTable
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb $eventPrefix
     * @param mixed $eventObject
     * @param mixed $resourceModel
     * @param string $model
     * @param null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Backend\Model\Auth\Session $adminSession,
        $mainTable,
        $eventPrefix,
        $eventObject,
        $resourceModel,
        $model = \Magento\Framework\View\Element\UiComponent\DataProvider\Document::class,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct(
            $request,
            $eavConfig,
            $fetchStrategy,
            $eventManager,
            $storeManager,
            $entityFactory,
            $logger,
            $mainTable,
            $eventPrefix,
            $eventObject,
            $resourceModel,
            $model,
            $connection,
            $resource
        );

        $this->adminSession = $adminSession;
    }

    /**
     * _renderFiltersBefore function
     *
     * @return void
     */
    protected function _renderFiltersBefore()
    {
        $isCmSoftware = true;
        $userData = $this->adminSession->getUser()->getData();
        if (!isset($userData['user_rest_id_active_agreement']) ||
            (isset($userData['user_rest_id_active_agreement']) && $userData['user_rest_id_active_agreement'] != '5800275')) {
            $isCmSoftware = false;
        }

        if (!$isCmSoftware) {
            $this->getSelect()->reset(\Zend_Db_Select::COLUMNS);
            $this->getSelect()->columns(
                ['entity_id', 'subject', 'bulk_order_qty', 'created_at', 'status']
            );
            $this->addExpressionFieldToSelect(
                'quotestatus',
                new \Zend_Db_Expr('IF(status = 0, "pending", "Sent to Seller")'),
                []
            );
            $this->addFieldToFilter('main_table.status', ['neq' => 6]);
        } else {
            $this->addExpressionFieldToSelect(
                'quotestatus',
                new \Zend_Db_Expr('
                CASE
                    WHEN main_table.status = 1 THEN "Publicada"
                    WHEN main_table.status = 2 THEN "Finalizada"
                    WHEN main_table.status = 3 THEN "Evaluación"
                    WHEN main_table.status = 4 THEN "Proveedor"
                    WHEN main_table.status = 5 THEN "Desierta"
                    ELSE "Pendiente"
                END
                '),
                []
            );
        }

        $this->getSelect()->joinLeft(
            ['ce' => $this->getTable('customer_entity')],
            'main_table.customer_id = ce.entity_id',
            ['customer_name' => 'CONCAT(ce.firstname, " ",ce.lastname)']
        );

        \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection::_renderFiltersBefore();
    }
}
