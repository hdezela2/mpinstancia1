<?php
namespace Formax\QuotesSupplier\Model\ResourceModel\Services;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'formax_xpec_services_collection';
	protected $_eventObject = 'xpec_services_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Formax\QuotesSupplier\Model\Services', 'Formax\QuotesSupplier\Model\ResourceModel\Services');
	}

}