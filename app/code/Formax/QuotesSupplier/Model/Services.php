<?php
namespace Formax\QuotesSupplier\Model;
class Services extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface{
	const CACHE_TAG         = 'formax_xpec_services';
	protected $_cacheTag    = 'formax_xpec_services';
	protected $_eventPrefix = 'formax_xpec_services';

	protected function _construct(){
		$this->_init('Formax\QuotesSupplier\Model\ResourceModel\Services');
	}

	public function getIdentities(){
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues(){
		$values = [];
		return $values;
	}
}