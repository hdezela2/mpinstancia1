<?php

namespace Formax\QuotesSupplier\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class ExecutionServiceTerm
 */
class ExecutionServiceTerm implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Días corridos'),
                'value' => 1
            ],
            [
                'label' => __('Días hábiles'),
                'value' => 2
            ],
            [
                'label' => __('Semanas'),
                'value' => 3
            ],
            [
                'label' => __('Meses'),
                'value' => 4
            ]
        ];
    }
}
