<?php

namespace Formax\QuotesSupplier\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class TechnicalRequirements
 */
class TechnicalRequirements implements OptionSourceInterface
{
    private \Magento\Framework\App\RequestInterface $_request;
    private \Magento\Framework\App\ResourceConnection $resourceConnection;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->_request = $request;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if($this->getEvaluationModality()==1){
            $dataEvaluation=json_decode($this->getOfferEvaluation());
            $rows =array();
            $i=0;
            foreach ($dataEvaluation->data as $key => $value) {
                $i++;
                $rows[]=array(
                            [
                                'value' => 'requirement_label',
                                'inputFront' => 'pharagraph',
                                'label' => __($i.'. Requerimiento mínimo de la oferta'),
                                'pharagraph' => __($value)
                            ],
                            [
                                'value' => 'requirement',
                                'inputFront' => 'textarea',
                                'label' => __('Propuesta proveedor'),
                                'class' => 'validate-requirement'
                            ]
                );
            }
            return [
                'headers' => [
                    __('Requerimiento mínimo de la oferta'),
                    __('Propuesta Proveedor')
                ],
                'rows' =>
                        $rows

            ];
        }
        if($this->getEvaluationModality()==2){
            $dataEvaluation=json_decode($this->getOfferEvaluation());
            $rows =array();
            foreach ($dataEvaluation as $value) {
                foreach($value->evaluation_label as $key => $evaluation){
                    $rows[]=array(
                            [
                                'value' => 'criterio_label',
                                'inputFront' => 'label',
                                'label' => __($value->evaluation_label[$key])
                            ],
                            [
                                'value' => 'ponderacion',
                                'inputFront' => 'label',
                                'label' => __($value->$key->weight ."%")
                            ],
                            [
                                'value' => 'observaciones',
                                'inputFront' => 'textarea'
                            ],
                            [
                                'value' => 'criterio_file',
                                'inputFront' => 'file'
                            ]
                    );
                }
            }
            //print_r($rows);
            //exit;
            return [
                'headers' => [
                    __('Criterios de evaluación'),
                    __('Ponderación'),
                    __('Observaciones y consideraciones del criterio'),
                    __('Adjuntar archivo')
                ],
                'rows' =>
                    $rows

            ];
        }
        return [];
    }

    public function getEvaluationModality(){
        $quoteId=$this->_request->getParam("id");
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from(['quote' => 'requestforquote_quote'], ['evaluation_modality']);
        $select->join(
                ['quote_info' => 'requestforquote_quote_info'],
                'quote.entity_id = quote_info.quote_id',
                ['*']
            );
        $select->where('quote_info.entity_id = '. $quoteId);
        return $resourceConnection->fetchOne($select);

    }
    public function getOfferEvaluation(){
        $quoteId=$this->_request->getParam("id");
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        if($this->getEvaluationModality()==1)
            $select->from(['quote' => 'requestforquote_quote'], ['minimum_offer_required']);
        else
            $select->from(['quote' => 'requestforquote_quote'], ['table_evaluation_criteria']);
        $select->join(
                ['quote_info' => 'requestforquote_quote_info'],
                'quote.entity_id = quote_info.quote_id',
                ['*']
            );
        $select->where('quote_info.entity_id = '. $quoteId);
        return $resourceConnection->fetchOne($select);
    }
}
