<?php

namespace Formax\QuotesSupplier\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Teamwork
 */
class Teamwork implements OptionSourceInterface
{
    private \Magento\Framework\App\RequestInterface $_request;
    private \Magento\Framework\App\ResourceConnection $resourceConnection;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->_request = $request;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $dataTeamwork=json_decode($this->getTeamwork());
        $rows =array();
        if(isset($dataTeamwork->data->rol_teamwork) && is_array($dataTeamwork->data->rol_teamwork) && count($dataTeamwork->data->rol_teamwork)>0){
            foreach ($dataTeamwork->data->rol_teamwork as $key => $value) {
                $rows[]=array(
                        [
                            'value' => 'rol_teamwork',
                            'inputFront' => 'text',
                            'class' => 'rol_teamwork',
                            'placeholder' => 'Project Manager, Scrum Master...'
                        ],
                        [
                            'value' => 'rut_teamwork',
                            'inputFront' => 'text',
                            'class' => 'rut_teamwork',
                            'placeholder' => '7.777.777-7'
                        ],
                        [
                         'value' => 'name_teamwork',
                         'inputFront' => 'text',
                         'class' => 'name_teamwork',
                         'placeholder' => 'Juan Pérez López'
                        ],
                        [
                            'value' => 'duracion_teamwork',
                            'inputFront' => 'text',
                            'class' => 'duracion_teamwork',
                            'placeholder' => '80'
                        ]
                );
            }
        }else{
                 $rows[]=array(
                        [
                            'value' => 'rol_teamwork',
                            'inputFront' => 'text',
                            'class' => 'rol_teamwork',
                            'placeholder' => 'Project Manager, Scrum Master...'
                        ],
                        [
                            'value' => 'rut_teamwork',
                            'inputFront' => 'text',
                            'class' => 'rut_teamwork',
                            'placeholder' => '7.777.777-7'
                        ],
                        [
                            'value' => 'name_teamwork',
                            'inputFront' => 'text',
                            'class' => 'name_teamwork',
                            'placeholder' => 'Juan Pérez López'
                        ],
                        [
                            'value' => 'duracion_teamwork',
                            'inputFront' => 'text',
                            'class' => 'duracion_teamwork',
                            'placeholder' => '80'
                        ]
                );

        }

        return [
            'headers' => [
                __('Rol en el proyecto'),
                __('RUT'),
                __('Nombre completo trabajador'),
                __('Horas totales asignadas al proyecto')
            ],
            'rows' => $rows
        ];
    }
    public function getTeamwork(){
        $quoteId=$this->_request->getParam("id");
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from('requestforquote_quote_conversation', ['teamwork']);
        $select->where('seller_quote_id = '. $quoteId);
        $select->where('updated_at = (select max(updated_at) from requestforquote_quote_conversation where seller_quote_id = '.$quoteId.' )');
        return $resourceConnection->fetchOne($select);
    }
}
