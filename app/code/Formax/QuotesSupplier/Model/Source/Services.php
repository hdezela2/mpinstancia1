<?php

namespace Formax\QuotesSupplier\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Services
 */
class Services implements OptionSourceInterface
{
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            'headers' => [
                __('Nombre del servicio'),
                __('Detalle')
            ],
            'rows' => [
                [
                    [
                        'value' => 'service_name',
                        'inputFront' => 'text'
                    ],
                    [
                        'value' => 'detail',
                        'inputFront' => 'text'
                    ]
                ]
            ]
        ];
    }
}
