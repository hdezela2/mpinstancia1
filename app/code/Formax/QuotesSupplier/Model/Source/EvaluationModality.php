<?php

namespace Formax\QuotesSupplier\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class EvaluationModality
 */
class EvaluationModality implements OptionSourceInterface
{
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Cumplimiento de requerimientos técnicos mínimos'),
                'value' => 1
            ],
            [
                'label' => __('Evaluación de criterios técnicos adicionales'),
                'value' => 2
            ]
        ];
    }
}
