<?php

namespace Formax\QuotesSupplier\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class EvaluationCriteria
 */
class EvaluationCriteria implements OptionSourceInterface
{

    /**
     * @var StoreManagerInterface $_storeManager
     */
    private $_storeManager;

    /**
     * @var STORE_CODE_SOFTWARE
     */
    public const STORE_CODE_SOFTWARE = 'software2022';

    /**
     * Constructor
     *
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $storeCode = $this->_storeManager->getStore()->getCode();
        $label = 'Condiciones laborales del equipo';
        $fieldGarantia = [
            [
                'value' => 'evaluation_label',
                'inputFront' => 'label',
                'label' => __('Garantía del software')
            ],
            [
                'value' => 'weight',
                'inputFront' => 'text',
                'class' => 'validate-percents'
            ],
            [
                'value' => 'observations',
                'inputFront' => 'text'
            ]
        ];
        if ($storeCode == self::STORE_CODE_SOFTWARE) {
            $label = 'Antigüedad laboral del equipo de trabajo';
            ////
            return [
                'headers' => [
                    __('Criterios de evaluación'),
                    __('Ponderación (%)'),
                    __('Observaciones y consideraciones del criterio')
                ],
                'rows' => [
                    [
                        [
                            'value' => 'evaluation_label',
                            'inputFront' => 'label',
                            'label' => __('Experiencia en el rubro')
                        ],
                        [
                            'value' => 'weight',
                            'inputFront' => 'text',
                            'class' => 'validate-percents'
                        ],
                        [
                            'value' => 'observations',
                            'inputFront' => 'text'
                        ]
                    ],
                    [
                        [
                            'value' => 'evaluation_label',
                            'inputFront' => 'label',
                            'label' => __('Certificaciones del oferente')
                        ],
                        [
                            'value' => 'weight',
                            'inputFront' => 'text',
                            'class' => 'validate-percents'
                        ],
                        [
                            'value' => 'observations',
                            'inputFront' => 'text'
                        ]
                    ],
                    [
                        [
                            'value' => 'evaluation_label',
                            'inputFront' => 'label',
                            'label' => __('Requisitos formales')
                        ],
                        [
                            'value' => 'weight',
                            'inputFront' => 'text',
                            'class' => 'validate-percents'
                        ],
                        [
                            'value' => 'observations',
                            'inputFront' => 'text'
                        ]
                    ],
                    [
                        [
                            'value' => 'evaluation_label',
                            'inputFront' => 'label',
                            'label' => __('Cantidad de perfiles certificados en el equipo de trabajo')
                        ],
                        [
                            'value' => 'weight',
                            'inputFront' => 'text',
                            'class' => 'validate-percents'
                        ],
                        [
                            'value' => 'observations',
                            'inputFront' => 'text'
                        ]
                    ],
                    [
                        [
                            'value' => 'evaluation_label',
                            'inputFront' => 'label',
                            'label' => __('Planificación del proyecto')
                        ],
                        [
                            'value' => 'weight',
                            'inputFront' => 'text',
                            'class' => 'validate-percents'
                        ],
                        [
                            'value' => 'observations',
                            'inputFront' => 'text'
                        ]
                    ],
                    [
                        [
                            'value' => 'evaluation_label',
                            'inputFront' => 'label',
                            'label' => __('Oferta técnica')
                        ],
                        [
                            'value' => 'weight',
                            'inputFront' => 'text',
                            'class' => 'validate-percents'
                        ],
                        [
                            'value' => 'observations',
                            'inputFront' => 'text'
                        ]
                    ],
                    [
                        [
                            'value' => 'evaluation_label',
                            'inputFront' => 'label',
                            'label' => __($label)
                        ],
                        [
                            'value' => 'weight',
                            'inputFront' => 'text',
                            'class' => 'validate-percents'
                        ],
                        [
                            'value' => 'observations',
                            'inputFront' => 'text'
                        ]
                    ]
                ]
            ];
            ////

        }
        return [
            'headers' => [
                __('Criterios de evaluación'),
                __('Ponderación (%)'),
                __('Observaciones y consideraciones del criterio')
            ],
            'rows' => [
                [
                    [
                        'value' => 'evaluation_label',
                        'inputFront' => 'label',
                        'label' => __('Experiencia en el rubro')
                    ],
                    [
                        'value' => 'weight',
                        'inputFront' => 'text',
                        'class' => 'validate-percents'
                    ],
                    [
                        'value' => 'observations',
                        'inputFront' => 'text'
                    ]
                ],
                [
                    [
                        'value' => 'evaluation_label',
                        'inputFront' => 'label',
                        'label' => __('Certificaciones del oferente')
                    ],
                    [
                        'value' => 'weight',
                        'inputFront' => 'text',
                        'class' => 'validate-percents'
                    ],
                    [
                        'value' => 'observations',
                        'inputFront' => 'text'
                    ]
                ],$fieldGarantia,
                [
                    [
                        'value' => 'evaluation_label',
                        'inputFront' => 'label',
                        'label' => __('Requisitos formales')
                    ],
                    [
                        'value' => 'weight',
                        'inputFront' => 'text',
                        'class' => 'validate-percents'
                    ],
                    [
                        'value' => 'observations',
                        'inputFront' => 'text'
                    ]
                ],
                [
                    [
                        'value' => 'evaluation_label',
                        'inputFront' => 'label',
                        'label' => __('Cantidad de perfiles certificados en el equipo de trabajo')
                    ],
                    [
                        'value' => 'weight',
                        'inputFront' => 'text',
                        'class' => 'validate-percents'
                    ],
                    [
                        'value' => 'observations',
                        'inputFront' => 'text'
                    ]
                ],
                [
                    [
                        'value' => 'evaluation_label',
                        'inputFront' => 'label',
                        'label' => __('Planificación del proyecto')
                    ],
                    [
                        'value' => 'weight',
                        'inputFront' => 'text',
                        'class' => 'validate-percents'
                    ],
                    [
                        'value' => 'observations',
                        'inputFront' => 'text'
                    ]
                ],
                [
                    [
                        'value' => 'evaluation_label',
                        'inputFront' => 'label',
                        'label' => __('Oferta técnica')
                    ],
                    [
                        'value' => 'weight',
                        'inputFront' => 'text',
                        'class' => 'validate-percents'
                    ],
                    [
                        'value' => 'observations',
                        'inputFront' => 'text'
                    ]
                ],
                [
                    [
                        'value' => 'evaluation_label',
                        'inputFront' => 'label',
                        'label' => __($label)
                    ],
                    [
                        'value' => 'weight',
                        'inputFront' => 'text',
                        'class' => 'validate-percents'
                    ],
                    [
                        'value' => 'observations',
                        'inputFront' => 'text'
                    ]
                ]
            ]
        ];
    }
}
