<?php

namespace Formax\QuotesSupplier\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;

/**
 * Class PurchaseUnit
 */
class PurchaseUnit implements OptionSourceInterface
{
    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $helperSoftware;

    /**
     * Initialize dependency injection
     * 
     * @param Formax\ConfigCmSoftware\Helper\Data $helperSoftware
     */
    public function __construct(HelperSoftware $helperSoftware)
    {
        $this->helperSoftware = $helperSoftware;
    }
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $purchaseUnits = $this->helperSoftware->getPurchaseUnitByCustomer();
        if (is_array($purchaseUnits)) {
            foreach ($purchaseUnits as $key => $purchaseUnit) {
                $options[] = [
                    'label' => __($purchaseUnit),
                    'value' => $key
                ];
            }
        }

        return $options;
    }
}
