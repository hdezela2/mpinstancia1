<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Formax\QuotesSupplier\Ui\Component\Listing\Columns\QuoteSoftware;

class Subject extends QuoteSoftware
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item[$name])) {
                    $item[$name] = html_entity_decode($item[$name]);
                }
            }
        }

        return $dataSource;
    }
}
