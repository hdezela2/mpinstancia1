<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Formax\ConfigCmSoftware\Helper\Data as ConfigCmSoftwareHelper;

class TypeOfServiceFilter implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var ConfigCmSoftwareHelper
     */
    private $cmSoftwareHelper;

    /**
     * TypeOfserviceFilter constructor.
     * @param ConfigCmSoftwareHelper $cmSoftwareHelper
     */
    public function __construct(ConfigCmSoftwareHelper $cmSoftwareHelper)
    {
        $this->cmSoftwareHelper = $cmSoftwareHelper;
    }

    /**
     * @return array[]
     */
    public function toOptionArray() : array
    {
        $typesOfService = $this->cmSoftwareHelper->getCategoriesCmSoftware();
        $result = [];
        if (!empty($typesOfService)) {
            foreach ($typesOfService as $serviceType) {
                $result[] = ["value" =>$serviceType['label'], "label" =>$serviceType['label']];
            }
        }
        return $result;
    }
}
