<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Formax\QuotesSupplier\Ui\Component\Listing\Columns\QuoteSoftware;

class Percent extends QuoteSoftware
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                
                if (isset($item[$name])) {
                    $item[$name] = number_format($item[$name], 1, ',', '.') . '%';
                }
            }
        }

        return $dataSource;
    }
}
