<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Serialize\Serializer\Json as JsonSerialize;
use Formax\QuotesSupplier\Ui\Component\Listing\Columns\QuoteSoftware;
use Magento\Backend\Model\Auth\Session as AdminSession;

class File extends QuoteSoftware
{
    /**
     * @var Magento\Framework\Serialize\Serializer\Json
     */
    protected $jsonSerialize;

    /**
     * @var Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Initialize dependencies
     * 
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Magento\Backend\Model\Auth\Session $adminSession
     * @param Magento\Framework\Serialize\Serializer\Json
     * @param Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        AdminSession $adminSession,
        JsonSerialize $jsonSerialize,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    ) {
        $this->jsonSerialize = $jsonSerialize;
        $this->storeManager = $storeManager;

        parent::__construct(
            $context,
            $uiComponentFactory,
            $adminSession,
            $components,
            $data
        );
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                
                if (isset($item[$name])) {
                    $itemsString = '';
                    $data = $this->jsonSerialize->unserialize($item[$name]);
                    
                    if (isset($data['data']) && is_array($data['data'])) {
                        foreach ($data['data'] as $key => $row) {
                            if ($row) {
                                $itemsString .= '<a href="' . $this->getMediaBaseUrl($row) . '" target="_blank">' . __('View File') . '</a><br>';
                            }
                        }
                    }
                    $item[$name] = $itemsString;
                }
            }
        }

        return $dataSource;
    }

    /**
     * Get file url media
     * 
     * @param string $file
     * @return string
     */
    public function getMediaBaseUrl($file)
    {
        $urlFile = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        return $urlFile . 'chilecompra/files/formattributes'. $file;
    }
}
