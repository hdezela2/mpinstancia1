<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Backend\Model\Auth\Session as AdminSession;

class QuoteSoftware extends Column
{
    /**
     * @var Magento\Backend\Model\Auth\Session
     */
    protected $adminSession;

    /**
     * @param Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param Magento\Backend\Model\Auth\Session $adminSession
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        AdminSession $adminSession,
        array $components = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );
        
        $this->adminSession = $adminSession;
    }

    /**
     * Prepare component configuration
     *
     * @return void
     */
    public function prepare()
    {
        parent::prepare();
        if ($this->adminSession->getUser()) {
            $userData = $this->adminSession->getUser()->getData();
            if (!isset($userData['user_rest_id_active_agreement']) ||
                (isset($userData['user_rest_id_active_agreement']) && $userData['user_rest_id_active_agreement'] != '5800275')) {
                unset($this->_data['config']);
                unset($this->_data['name']);
                unset($this->_data['js_config']);
            }
        }
    }
}