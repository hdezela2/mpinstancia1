<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Formax\QuotesSupplier\Ui\Component\Listing\Columns\QuoteSoftware;
use Magento\Backend\Model\Auth\Session as AdminSession;

class Currency extends QuoteSoftware
{
    /**
     * @var Magento\Framework\Pricing\Helper\Data
     */
    protected $pricingHelper;

    /**
     * Initialize dependencies
     * 
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Magento\Backend\Model\Auth\Session as AdminSession
     * @param Magento\Framework\Pricing\Helper\Data $pricingHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        AdminSession $adminSession,
        PricingHelper $pricingHelper,
        array $components = [],
        array $data = []
    ) {
        $this->pricingHelper = $pricingHelper;

        parent::__construct(
            $context,
            $uiComponentFactory,
            $adminSession,
            $components,
            $data
        );
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                
                if (isset($item[$name])) {
                    $item[$name] = strip_tags($this->pricingHelper->currency($item[$name], true, false));
                }
            }
        }

        return $dataSource;
    }
}
