<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Backend\Model\Auth\Session as AdminSession;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Formax\QuotesSupplier\Ui\Component\Listing\Columns\QuoteSoftware;

class QuoteLink extends QuoteSoftware
{
    /**
     * @var UrlInterface
     */
    private $urlInterface;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var HelperSoftware
     */
    private $helperSoftware;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param AdminSession $adminSession
     * @param UrlInterface $urlInterface
     * @param StoreManagerInterface $storeManager
     * @param HelperSoftware $helperSoftware
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        AdminSession $adminSession,
        UrlInterface $urlInterface,
        StoreManagerInterface $storeManager,
        HelperSoftware $helperSoftware,
        array $components = array(),
        array $data = array()
    ) {
        parent::__construct(
            $context,
            $uiComponentFactory,
            $adminSession,
            $components,
            $data
        );

        $this->urlInterface = $urlInterface;
        $this->storeManager = $storeManager;
        $this->helperSoftware = $helperSoftware;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $storeCode =
                $this->storeManager->getStore()->getCode() == SoftwareRenewalConstants::STORE_CODE
                    ? SoftwareRenewalConstants::STORE_CODE
                    : HelperSoftware::SOFTWARE_STOREVIEW_CODE;
            $url = $this->storeManager->getStore($storeCode)->getBaseUrl();
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                $entityId = isset($item['entity_id']) ? (int)$item['entity_id'] : 0;
                if ($entityId > 0 && isset($item['publication_end']) && $item['publication_end'] !== '' && $item['publication_end'] !== null) {
                    $url .= 'publicquotes/requestforquote/lists/quote_id/' . $entityId;
                    $item[$name] = '<a title="' . __('View Quote') . '" href="' . $url . '" target="_blank">' . $entityId . '</a>';
                }
            }
        }
        return $dataSource;
    }
}
