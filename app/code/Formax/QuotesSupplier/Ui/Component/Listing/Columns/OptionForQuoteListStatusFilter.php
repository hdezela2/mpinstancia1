<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Formax\QuotesSupplier\Helper\Data as Helper;

class OptionForQuoteListStatusFilter implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            //['value' => Helper::DRAFT, 'label' => __(Helper::DRAFT_LABEL)],
            ['value' => Helper::PENDING, 'label' => __(Helper::PENDING_LABEL)],
            ['value' => Helper::PUBLISHED, 'label' => __(Helper::PUBLISHED_LABEL)],
            ['value' => Helper::PUBLICATION_CLOSED, 'label' => __(Helper::PUBLICATION_CLOSED_LABEL)],
            ['value' => Helper::EVALUATION, 'label' => __(Helper::EVALUATION_LABEL)],
            ['value' => Helper::EVALUATION_CLOSED, 'label' => __(Helper::EVALUATION_CLOSED_LABEL)],
            ['value' => Helper::AWARDED, 'label' => __(Helper::AWARDED_LABEL)],
            ['value' => Helper::DESERT, 'label' => __(Helper::DESERT_LABEL)],
            ['value' => Helper::FINISHED, 'label' => __(Helper::FINISHED_LABEL)],
            ['value' => Helper::REJECTED, 'label' => __(Helper::REJECTED_LABEL)]
        ];
    }
}
