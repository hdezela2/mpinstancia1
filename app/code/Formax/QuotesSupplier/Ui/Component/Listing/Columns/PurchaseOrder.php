<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Formax\QuotesSupplier\Helper\Data as QuoteSupplierHelper;
use Formax\QuotesSupplier\Ui\Component\Listing\Columns\QuoteSoftware;
use Magento\Backend\Model\Auth\Session as AdminSession;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

class PurchaseOrder extends QuoteSoftware
{
    /** @var QuoteSupplierHelper */
    protected $_quoteSupplierHelper;

    public function __construct(
        QuoteSupplierHelper $quoteSupplierHelper,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        AdminSession $adminSession,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $adminSession, $components, $data);
        $this->_quoteSupplierHelper = $quoteSupplierHelper;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if ($item['requestforquote_status'] == \Formax\QuotesSupplier\Helper\Data::AWARDED) {
                    $itemId = (int) $item['requestforquote_id'];
                    $quoteDetails = $this->_quoteSupplierHelper->getQuoteDetails($itemId);
                    $item[$name] = $quoteDetails->getOrdenDeCompra();
                }
            }
        }
        return $dataSource;
    }
}
