<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Formax\QuotesSupplier\Helper\Data as QuotesSupplierHelper;

class QuoteListStatus extends Column
{
    /**
     * Prepare Data Source
     *
     * @param  array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item[$name])) {
                    switch ($item[$name]) {
                        case QuotesSupplierHelper::PENDING:
                            $status = __('Pending');
                            break;
                        case QuotesSupplierHelper::PUBLISHED:
                            $status = __('Published');
                            break;
                        case QuotesSupplierHelper::FINISHED:
                            $status = __('Finished');
                            break;
                        case QuotesSupplierHelper::EVALUATION:
                            $status = __('In Evaluation');
                            break;
                        case QuotesSupplierHelper::AWARDED:
                            $status = __('Provider Selected');
                            break;
                        case QuotesSupplierHelper::DESERT:
                            $status = __('Desert');
                            break;
                        case QuotesSupplierHelper::DRAFT:
                            $status = __('Saved');
                            break;
                        case QuotesSupplierHelper::PUBLICATION_CLOSED:
                            $status = __('Publication Closed');
                            break;
                        case QuotesSupplierHelper::EVALUATION_CLOSED:
                            $status = __('Evaluation Closed');
                            break;
                        case QuotesSupplierHelper::REJECTED:
                            $status = __('Rejected');
                            break;
                        default:
                            $status = __('Pending');
                    }
                    $item[$name] = $status;
                }
            }
        }

        return $dataSource;
    }
}
