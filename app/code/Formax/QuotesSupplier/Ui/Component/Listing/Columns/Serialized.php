<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\Serialize\Serializer\Json as JsonSerialize;
use Formax\QuotesSupplier\Ui\Component\Listing\Columns\QuoteSoftware;
use Magento\Backend\Model\Auth\Session as AdminSession;

class Serialized extends QuoteSoftware
{
    /**
     * @var use Magento\Framework\Serialize\Serializer\Json
     */
    protected $jsonSerialize;

    /**
     * Initialize dependencies
     * 
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Magento\Backend\Model\Auth\Session $adminSession
     * @param Magento\Framework\Serialize\Serializer\Json
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        AdminSession $adminSession,
        JsonSerialize $jsonSerialize,
        array $components = [],
        array $data = []
    ) {
        $this->jsonSerialize = $jsonSerialize;

        parent::__construct(
            $context,
            $uiComponentFactory,
            $adminSession,
            $components,
            $data
        );
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                
                if (isset($item[$name])) {
                    $itemsArray = [];
                    $data = $this->jsonSerialize->unserialize($item[$name]);
                    if (isset($data['data']) && is_array($data['data'])) {
                        foreach ($data['data'] as $key => $row) {
                            if (is_array($row)) {
                                $count = count($row);
                                for ($i = 0; $i < $count; $i++) {
                                    if ($row[$i]) {
                                        $itemsArray[$i][$key] = $row[$i];
                                    }
                                }
                            } else {
                                if ($row) {
                                    $itemsArray[$key] = $row;
                                }
                            }
                        }
                    }
                    $item[$name] = count($itemsArray) > 0 ? $this->jsonSerialize->serialize($itemsArray) : '';
                }
            }
        }

        return $dataSource;
    }
}
