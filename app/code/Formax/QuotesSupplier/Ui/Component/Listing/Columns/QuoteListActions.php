<?php
namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Formax\QuotesSupplier\Helper\Data as QuotesSupplierHelper;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Requestforquote\Ui\Component\Listing\Columns\QuoteListActions as ParentQuoteListActions;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;

/**
 * Class QuoteListActions.
 */
class QuoteListActions extends ParentQuoteListActions{
    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;
    protected $_storeManager;
    private \Magento\Framework\App\ResourceConnection $resourceConnection;


    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        array $components = [],
        array $data = []
    ) {
        $this->_urlBuilder      = $urlBuilder;
        $this->_storeManager    = $storeManager;
        $this->resourceConnection = $resourceConnection;
        parent::__construct(
            $context,
            $uiComponentFactory,
            $urlBuilder,
            $components,
            $data
        );
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource){
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['entity_id'])) {
                    if($this->isCustomOptions()){
                        $status = $this->getQuoteStatus($item['entity_id']);
                        $customerstatus = $this->getQuoteCustomerStatus($item['entity_id']);
                        if($status == QuotesSupplierHelper::AWARDED && $customerstatus == 3)
                        {
                            $item[$this->getData('name')] = [
                                'view' => [
                                    'href' => $this->_urlBuilder->getUrl('quotessupplier/team/job',['id' => $item['entity_id'],]),
                                    'label' => __('Equipo'),
                                ],
                                'public_view' => [
                                    'href' => $this->_urlBuilder->getUrl('publicquotes/requestforquote/view',['id' => $item['quote_id'],]),
                                    'target' => '_blank',
                                    'label' => __('View Details'),
                                ]
                            ];

                        } else if ($status == QuotesSupplierHelper::PUBLISHED) {
                            $item[$this->getData('name')] = [
                                'view' => [
                                    'href' => $this->_urlBuilder->getUrl('requestforquote/seller/view',['id' => $item['entity_id'],]),
                                    'label' => __('Crear cotización'),
                                ],
                                'public_view' => [
                                    'href' => $this->_urlBuilder->getUrl('publicquotes/requestforquote/view',['id' => $item['quote_id'],]),
                                    'target' => '_blank',
                                    'label' => __('View Details'),
                                ]
                            ];
                        } else {
                            $item[$this->getData('name')] = [
                                'public_view' => [
                                    'href' => $this->_urlBuilder->getUrl('publicquotes/requestforquote/view',['id' => $item['quote_id'],]),
                                    'target' => '_blank',
                                    'label' => __('View Details'),
                                ]
                            ];
                        }
                    }else{
                        $item[$this->getData('name')] = [
                            'view' => [
                                'href' => $this->_urlBuilder->getUrl(
                                    'requestforquote/seller/view',
                                    [
                                        'id' => $item['entity_id'],
                                    ]
                                ),
                                'label' => __('View'),
                            ],
                        ];
                    }

                }
            }
        }
        return $dataSource;
    }
    public function isCustomOptions(){
        $storeName = trim($this->_storeManager->getStore()->getName());
        if($storeName=='convenio_storeview_software' || $storeName =='Software' || $storeName == SoftwareRenewalConstants::STORE_CODE || $storeName == SoftwareRenewalConstants::STORE_NAME){
            return true;
        }else{
            return false;
        }
    }
    public function getQuoteCustomerStatus($quoteId){
        //requestforquote_quote_info - entity_id
        //requestforquote_quote_conversation - seller_quote_id

        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from(['quote_info' => 'requestforquote_quote_info'], ['customer_status']);
        $select->where('quote_info.entity_id = '. $quoteId);
        return $resourceConnection->fetchOne($select);
    }
    public function getQuoteStatus($quoteId){
        //requestforquote_quote_info - entity_id
        //requestforquote_quote_conversation - seller_quote_id

        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from(['quote' => 'requestforquote_quote'], ['status']);
        $select->join(
                ['quote_info' => 'requestforquote_quote_info'],
                'quote_info.quote_id = quote.entity_id',
                ['*']
            );
        $select->where('quote_info.entity_id = '. $quoteId);
        return $resourceConnection->fetchOne($select);
    }

}
