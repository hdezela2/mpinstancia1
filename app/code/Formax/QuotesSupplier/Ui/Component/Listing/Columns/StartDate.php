<?php

namespace Formax\QuotesSupplier\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Stdlib\BooleanUtils;
use Magento\Backend\Model\Auth\Session as AdminSession;

class StartDate extends QuoteSoftware
{
    /**
     * @var Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var Magento\Framework\Stdlib\BooleanUtils
     */
    private $booleanUtils;

    /**
     * Initialize dependencies
     *
     * @param Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param Magento\Backend\Model\Auth\Session $adminSession
     * @param Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param Magento\Framework\Stdlib\BooleanUtils $booleanUtils
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        AdminSession $adminSession,
        TimezoneInterface $timezone,
        BooleanUtils $booleanUtils,
        array $components = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            $uiComponentFactory,
            $adminSession,
            $components,
            $data
        );

        $this->timezone = $timezone;
        $this->booleanUtils = $booleanUtils;
    }

    /**
     * @inheritdoc
     * @since 101.1.1
     */
    public function prepare()
    {
        $config = $this->getData('config');
        $config['filter'] = [
            'filterType' => 'dateRange',
            'templates' => [
                'date' => [
                    'options' => [
                        'dateFormat' => $this->timezone->getDateFormatWithLongYear()
                    ]
                ]
            ]
        ];
        $this->setData('config', $config);

        parent::prepare();
    }

    /**
     * @inheritdoc
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item[$this->getData('name')])
                    && $item[$this->getData('name')] !== "0000-00-00 00:00:00"
                ) {
                    $date = new \DateTime($item[$this->getData('name')]);
                    $item[$this->getData('name')] = $date->format('Y-m-d H:i:s');
                }
            }
        }
        return $dataSource;
    }
}