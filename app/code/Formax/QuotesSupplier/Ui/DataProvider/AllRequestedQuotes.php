<?php

namespace Formax\QuotesSupplier\Ui\DataProvider;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Framework\Registry;
use Webkul\Marketplace\Helper\Data as HelperData;
use Webkul\Requestforquote\Ui\DataProvider\AllRequestedQuotes as WebkulAllRequestedQuotes;
use Webkul\Marketplace\Helper\Data as HelperMarketplace;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;

/**
 * Class AllRequestedQuotes
 */
class AllRequestedQuotes extends WebkulAllRequestedQuotes
{
    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $helperSoftware;
    protected array $addFieldStrategies;
    protected array $addFilterStrategies;

    /**
     * Initialize dependencies
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection
     * @param Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $collectionFactory
     * @param Webkul\Marketplace\Helper\Data $helperData
     * @param Magento\Framework\App\RequestInterface $request
     * @param Magento\Framework\Registry $registry
     * @param Webkul\Marketplace\Helper\Data $mpHelper
     * @param Formax\ConfigCmSoftware\Helper\Data $helperSoftware
     * @param Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param array $addFieldStrategies
     * @param array $addFilterStrategies
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        ProductCollection $productCollection,
        CollectionFactory $collectionFactory,
        HelperData $helperData,
        RequestInterface $request,
        Registry $registry,
        HelperMarketplace $mpHelper,
        HelperSoftware $helperSoftware,
        Attribute $eavAttribute,
        array $addFieldStrategies = [],
        array $addFilterStrategies = [],
        array $meta = [],
        array $data = []
    ) {
        $this->helperSoftware = $helperSoftware;

        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $productCollection,
            $collectionFactory,
            $helperData,
            $request,
            $registry,
            $mpHelper,
            $eavAttribute,
            $addFieldStrategies,
            $addFilterStrategies,
            $meta,
            $data
        );

        if ($this->helperSoftware->getCurrentStoreCode() == HelperSoftware::SOFTWARE_STOREVIEW_CODE || $this->helperSoftware->getCurrentStoreCode() == SoftwareRenewalConstants::STORE_CODE) {
            $customerId = $mpHelper->getCustomerId();
            $sellerId = $mpHelper->getCustomerId();

            $quotes = $collectionFactory->create();
            $quotes->addFieldToFilter('seller_id', $sellerId);
            $rfqQuoteTable = $quotes->getResource()->getTable('requestforquote_quote');
            $productNameTable = $quotes->getResource()->getTable('catalog_product_entity_varchar');
            $quotes->getSelect()
                ->join(
                    ['rfq' => $rfqQuoteTable],
                    'main_table.quote_id = rfq.entity_id',
                    [
                        'requestforquote_id' => 'entity_id',
                        'subject',
                        'amount',
                        'award_date',
                        'description',
                        'customer_id',
                        'created_at',
                        'publication_end',
                        'organization',
                        'requestforquote_status' => 'status',
                        'evaluation_start',
                        'evaluation_end',
                        'award_date'
                    ]
                );
            $attrProductName = $eavAttribute->getIdByCode('catalog_product', 'name');
            $quotes->getSelect()
                ->join(
                    ['pn' => $productNameTable],
                    'rfq.product_row_id = pn.row_id AND pn.store_id = 0 AND pn.attribute_id = ' . $attrProductName,
                    [
                        'product_name' => 'value'
                    ]
                );
            $quotes ->getSelect()
                ->order('main_table.created_at DESC');
            $fnameid = $eavAttribute->getIdByCode('customer', 'firstname');
            $lnameid = $eavAttribute->getIdByCode('customer', 'lastname');
            $quotes->getSelect()
                ->join(
                    ['ce1' => $quotes->getResource()->getTable('customer_entity')],
                    'rfq.customer_id = ce1.entity_id',
                    [
                        'firstname',
                        'lastname'
                    ]
                )->where('ce1.entity_id = rfq.customer_id')
                ->where('rfq.status <> 6')
                ->columns(new \Zend_Db_Expr('CONCAT(ce1.firstname, " ", ce1.lastname) AS customer_name'));
            $quotes->addFilterToMap('customer_name', new \Zend_Db_Expr('CONCAT(ce1.firstname, " ", ce1.lastname)'));
            $quotes->addFilterToMap('requestforquote_status', 'rfq.status');
            $quotes->addFilterToMap('created_at', 'main_table.created_at');
            $quotes->addFilterToMap('requestforquote_id', 'rfq.entity_id');

            $this->collection = $quotes;
            $this->addFieldStrategies = $addFieldStrategies;
            $this->addFilterStrategies = $addFilterStrategies;
        }

        /** Filter requestforquote for store */
        $storeId = $this->helperSoftware->getStoreManager()->getStore()->getId();
        $this->collection->getSelect()
            ->join(
                ['seller_data' => $this->collection->getResource()->getTable('customer_entity')],
                'main_table.seller_id = seller_data.entity_id',
                []
            );
        $this->collection->addFieldToFilter(
            ['seller_data.store_id', 'seller_data.store_id'],
            [
                ['eq' => 0],
                ['eq' => $storeId]
            ]
        );
    }
}
