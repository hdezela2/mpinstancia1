<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Requestforquote
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Formax\QuotesSupplier\Block;

use Magento\Framework\View\Element\Template;
use \Magento\Store\Model\StoreManagerInterface;

/**
 * SubmitButton class
 */
class QuoteButton extends \Magento\Framework\View\Element\Template
{

    protected $storeManager;

    public function __construct(Template\Context $context, array $data = [], StoreManagerInterface $storeManager)
    {
        parent::__construct($context, $data);
        $this->storeManager = $storeManager;
    }

    /**
     * get Product
     *
     * @return Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        $id = $this->getRequest()->getParam('id');
        return $id;
    }
    public function getProductName()
    {
        $id = $this->getRequest()->getParam('id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($id);
        $name=$product->getName(); //Get Product Name
        return $name;
    }
    public function getProductSku()
    {
        $id = $this->getRequest()->getParam('id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($id);
        $sku=$product->getSku(); //Get Product Sku
        return $sku;
    }

    public function shouldRenderRequestQuoteButton()
    {
        return !in_array($this->_storeManager->getStore()->getCode(), [\Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE]);
    }

}
