<?php

namespace Formax\QuotesSupplier\Block;

class Notification extends \Magento\Framework\View\Element\Template
{
	protected $_customerSession;
    private $resourceConnection;
	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\SessionFactory $customerSession,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        array $data = []
    ) {
        $this->_customerSession = $customerSession->create();
        $this->resourceConnection = $resourceConnection;
        parent::__construct($context, $data);
    }
    public function getNotification()
    {
        if ($this->_customerSession->isLoggedIn()) {
		    $resourceConnection = $this->resourceConnection->getConnection();
		    $select = $resourceConnection->select();
		    $select->from(['quotes' => 'requestforquote_quote'], ['entity_id']);
	        $select->join(
	                ['quotesinfo' => 'requestforquote_quote_info'],
	                'quotes.entity_id = quotesinfo.quote_id',
	                ['*']
	            );
		    $select->where('quotes.customer_id = '. $this->_customerSession->getId());
		    $select->where('quotesinfo.customer_status = 1');
		    $result_quotes=$resourceConnection->fetchAll($select);
            return $result_quotes;
        }
        return false;
    }
}
