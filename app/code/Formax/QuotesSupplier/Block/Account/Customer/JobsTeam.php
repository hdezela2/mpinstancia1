<?php

namespace Formax\QuotesSupplier\Block\Account\Customer;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * JobsTeam class
 */
class JobsTeam extends Template
{

    private $_info;
    private $_idQuote;
    private $_quote;
    private $_comprador;
    private $_teamJobFactory;
    private $_servicesFactory;
    private $_dir;
    protected $_serializer;
    protected $_storeManager;
    private $_orderFactory;
    private $_resource;
    public $priceHelper;
    protected $helperMp;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Requestforquote\Model\InfoFactory $info,
        \Magento\Framework\App\RequestInterface $request,
        \Webkul\Requestforquote\Model\QuoteFactory $quote,
        \Magento\Customer\Model\CustomerFactory $comprador,
        \Formax\QuotesSupplier\Model\TeamJobFactory $teamJobFactory,
        \Formax\QuotesSupplier\Model\ServicesFactory $servicesFactory,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Sales\Api\OrderRepositoryInterface $orderFactory,
        SerializerInterface $serializer,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Webkul\Marketplace\Helper\Data $helperMp,
        array $data = []
    ) {
        $this->_idQuote         = $request->getParam('id');
        //$this->_info            = $info->create()->load($this->_idQuote);
        $this->_quote           = $quote->create()->load($this->_idQuote);
        $this->_comprador       = $comprador;
        $this->_teamJobFactory  = $teamJobFactory;
        $this->_servicesFactory = $servicesFactory;
        $this->_dir             = $dir;
        $this->_serializer      = $serializer;
        $this->_storeManager    = $storeManager;
        $this->_orderFactory    = $orderFactory;
        $this->_resource        = $resource;
        $this->priceHelper      = $priceHelper;
        $this->helperMp = $helperMp;

        parent::__construct($context, $data);

    }
    public function getInfoQuote(){
        return $this->_info;
    }
    public function getQuote(){
        return $this->_quote;
    }
    public function getComprador(){
        $customer = false;
        if (isset($this->_quote['customer_id'])) {
            $customer = $this->_comprador->create()->load($this->_quote['customer_id']);
        }
        return $customer;
    }
    public function getSellerName(){
        $customer = $this->getComprador();
        return trim($customer->getFirstName() . ' ' . $customer->getLastname());
    }
    public function getCollectionTeamJob(){
        $collection = $this->_teamJobFactory->create()->getCollection();
        $collection->addFieldToFilter(
            'quote_id',array(
                array('eq' => $this->_idQuote)
            )
        );
        return $collection;
    }
    public function getCollectionServices(){
        $collection = $this->_servicesFactory->create()->getCollection();
        $collection->addFieldToFilter(
            'quote_id',array(
                array('eq' => $this->_idQuote)
            )
        );
        return $collection;
    }
    public function getPathMedia(){
        return $this->_dir->getPath('media');
    }
    public function unSerializeData($extraData){
        $additionalData = $this->_serializer->unserialize($extraData);
        return $additionalData;
    }
    public function getUrlBaseInforme(){
        $tmp = str_replace(array("software/","software2022/"),"",$this->_storeManager->getStore()->getBaseUrl());
        return $tmp.'media/informeteam/files';
    }
    public function getUrlBaseCv(){
        $tmp = str_replace(array("software/","software2022/"),"",$this->_storeManager->getStore()->getBaseUrl());
        return $tmp.'media/cvteam/files';
    }
    public function getOrderByQuote(){
        $con = $this->_resource->getConnection();
        $sql = "SELECT entity_id FROM sales_order WHERE requestforquote_info = ".$this->_idQuote." ORDER BY entity_id DESC";
        $idOrder = $con->fetchOne($sql);
        if(isset($idOrder) && $idOrder!=null){
            $order = $this->_orderFactory->get($idOrder);
            return $order;
        }else{
            return null;
        }
    }
    public function getSellerId(){
        $con = $this->_resource->getConnection();
        $sql = "SELECT i.seller_id
                FROM requestforquote_quote q
                INNER JOIN requestforquote_quote_conversation c
                ON q.approved_seller_quote_id = c.entity_id AND c.edited = 1
                INNER JOIN requestforquote_quote_info i
                ON c.seller_quote_id = i.entity_id
                WHERE q.entity_id = " . $this->_idQuote;
        $sellerId = $con->fetchOne($sql);
        return $sellerId;
    }
    public function getValueNumber($amount) {
        return str_replace(',00','',$this->priceHelper->currency($amount,true,0));
    }

    public function getSellerNameById($sellerId)
    {
        $shopName = '';
        $customer = $this->_comprador->create()->load($sellerId);
        if ($customer->getId() > 0) {
            $shopName = $customer->getFirstname() . ' ' . $customer->getLastname();
        }

        return $shopName;
    }

}
