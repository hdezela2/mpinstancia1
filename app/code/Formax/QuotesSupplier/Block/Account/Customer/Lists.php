<?php

namespace Formax\QuotesSupplier\Block\Account\Customer;

use Formax\QuotesSupplier\Helper\Data as QuoteSupplierHelper;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\View\Element\Template\Context;
use Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory as QuoteCollectionFactory;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\Requestforquote\Helper\Data as RequestforquoteHelper;
use Formax\ConfigCmSoftware\Helper\Data as ConfigCmSoftwareHelper;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Lists class
 */
class Lists extends \Webkul\Requestforquote\Block\Account\Customer\Lists
{
    /** @var QuoteCollectionFactory */
    protected $quoteCollectionFactory;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $mpHelper;

    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $cmSoftwareHelper;

    /**
     * @var array
     */
    protected $purchaseUnitList;
    protected $_productRepository;
    protected $eavAttribute;
    protected $entity;
    protected $customerFactory;
    protected $helperMp;

    /** @var QuoteSupplierHelper */
    protected $_quoteSupplierHelper;
    private ResourceConnection $resourceConnection;
    protected RequestforquoteHelper $helper;


    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory $quoteFactory
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param Formax\ConfigCmSoftware\Helper\Data $cmSoftwareHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        QuoteCollectionFactory $quoteCollectionFactory,
        QuoteSupplierHelper $quoteSupplierHelper,
        MarketplaceHelper $mpHelper,
        RequestforquoteHelper $helper,
        ConfigCmSoftwareHelper $cmSoftwareHelper,
        ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Eav\Model\Attribute $eavAttribute,
        \Magento\Eav\Model\Entity $entity,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Webkul\Marketplace\Helper\Data $helperMp,
        array $data = []
    ) {
        $this->quoteCollectionFactory = $quoteCollectionFactory;
        $this->_quoteSupplierHelper = $quoteSupplierHelper;
        $this->resourceConnection = $resourceConnection;
        $this->mpHelper = $mpHelper;
        $this->helper = $helper;
        $this->cmSoftwareHelper = $cmSoftwareHelper;
        $this->purchaseUnitList = [];
        $this->_productRepository = $productRepository;
        $this->eavAttribute = $eavAttribute;
        $this->entity = $entity;
        $this->customerFactory = $customerFactory;
        $this->helperMp = $helperMp;

        parent::__construct(
            $context,
            $quoteCollectionFactory,
            $mpHelper,
            $helper,
            $data
        );

        $filter = '';
        $filterStatus = '';

        if ($quoteKeyword = $this->getRequest()->getParam('s')) {
            $filter = $quoteKeyword != "" ? $quoteKeyword : "";
        }
        if ($this->getRequest()->getParam('status') != '') {
            $quoteStatus = $this->getRequest()->getParam('status');
            $filterStatus = $quoteStatus != "" ? $quoteStatus : "";
        }

        if ($quotePstartFromDate = $this->getRequest()->getParam('pstart_from_date')) {
            $filterPstartDateFrom = $quotePstartFromDate;
        } else {
            $filterPstartDateFrom = "";
        }
        if ($quotePstartToDate = $this->getRequest()->getParam('pstart_to_date')) {
            $filterPstartDateTo = $quotePstartToDate." 23:59:59";
        } else {
            $filterPstartDateTo = "";
        }

        $filterEstartDateFrom = !empty($this->getRequest()->getParam('estart_from_date')) ?
            $this->getRequest()->getParam('estart_from_date') : "";
        $filterEstartDateTo = !empty($this->getRequest()->getParam('estart_to_date')) ?
            $this->getRequest()->getParam('estart_to_date')." 23:59:59" : "";

        $filterAwardDateFrom = !empty($this->getRequest()->getParam('award_from_date')) ?
            $this->getRequest()->getParam('award_from_date') : "";
        $filterAwardDateTo = !empty($this->getRequest()->getParam('award_to_date')) ?
            $this->getRequest()->getParam('award_to_date')." 23:59:59" : "";

        $filterAmountFrom = !empty($this->getRequest()->getParam('amount_from')) ?
            $this->getRequest()->getParam('amount_from') : "";
        $filterAmountTo = !empty($this->getRequest()->getParam('amount_to')) ?
            $this->getRequest()->getParam('amount_to') : "";

        $filterPname = !empty($this->getRequest()->getParam('pname')) ?
            $this->getRequest()->getParam('pname') : "";

        $filterPid = !empty($this->getRequest()->getParam('pid')) ?
            $this->getRequest()->getParam('pid') : "";

        $filterServicetype = !empty($this->getRequest()->getParam('service_type')) ?
            $this->getRequest()->getParam('service_type') : "";

        $customerId = $this->mpHelper->getCustomerId();
        $quotes = $this->quoteCollectionFactory->create();
        $quotes->getSelect()->group('main_table.entity_id');

        if ($this->cmSoftwareHelper->getCurrentStoreCode() == ConfigCmSoftwareHelper::SOFTWARE_STOREVIEW_CODE || $this->cmSoftwareHelper->getCurrentStoreCode() == SoftwareRenewalConstants::STORE_CODE) {
            if ($quotePurchaseUnit = $this->getRequest()->getParam('purchase_unit')) {
                $quotes->addFieldToFilter('organization_unit', $quotePurchaseUnit);
            } else {
                $purchaseUnitArray = [];
                $purchaseUnits = $this->cmSoftwareHelper->getPurchaseUnitByCustomer();
                if (is_array($purchaseUnits)) {
                    foreach ($purchaseUnits as $key => $purchaseUnit) {
                        $this->purchaseUnitList[$key] = $purchaseUnit;
                        $purchaseUnitArray[] = $key;
                    }
                    $quotes->addFieldToFilter('organization_unit', ['in' => $purchaseUnitArray]);
                }
            }
        } else {
            $quotes->addFieldToFilter('customer_id', $customerId);
        }

        if ($filter) {
            $quotes->addFieldToFilter(
                ['entity_id', 'description'],
                [
                    ['eq' => $filter],
                    ['like' => "%" . $filter . "%"]
                ]
            );
        }
        if ($filterStatus != '') {
            $quotes->addFieldToFilter('status', $filterStatus);
        }
        if ($filterPid != '') {
            $quotes->addFieldToFilter('product_id', $filterPid);
        }
        if ($filterServicetype != '') {
            $quotes->addFieldToFilter('subject', $filterServicetype);
        }

        //filter publication period
        if ($filterPstartDateFrom != '' && $filterPstartDateTo != '') {
            $quotes->addFieldToFilter(
                ['publication_start'],
                [
                    ['datetime' => true, 'from' => $this->cmSoftwareHelper->getMinDate(), 'to' =>  $filterPstartDateTo]

                ]
            );

            $quotes->addFieldToFilter(
                ['publication_end'],
                [
                    ['datetime' => true, 'from' => $filterPstartDateFrom, 'to' =>  $this->cmSoftwareHelper->getMaxDate()]
                ]
            );
        } elseif ($filterPstartDateFrom != '') {
            $quotes->addFieldToFilter(
                ['publication_end'],
                [
                    ['datetime' => true, 'from' => $filterPstartDateFrom, 'to' =>  $this->cmSoftwareHelper->getMaxDate()]
                ]
            );
        } elseif ($filterPstartDateTo != '') {
            $quotes->addFieldToFilter(
                ['publication_start'],
                [
                    ['datetime' => true, 'from' => $this->cmSoftwareHelper->getMinDate(), 'to' =>  $filterPstartDateTo]

                ]
            );
        }

        //filter evaluation period
        if ($filterEstartDateFrom != '' && $filterEstartDateTo != '') {
            $quotes->addFieldToFilter(
                ['evaluation_start'],
                [
                    ['datetime' => true, 'from' => $this->cmSoftwareHelper->getMinDate(), 'to' =>  $filterEstartDateTo]
                ]
            );
            $quotes->addFieldToFilter(
                ['evaluation_end'],
                [
                    ['datetime' => true, 'from' => $filterEstartDateFrom, 'to' =>  $this->cmSoftwareHelper->getMaxDate()]
                ]
            );
        } elseif ($filterEstartDateFrom != '') {
            $quotes->addFieldToFilter(
                ['evaluation_end'],
                [
                    ['datetime' => true, 'from' => $filterEstartDateFrom, 'to' =>  $this->cmSoftwareHelper->getMaxDate()]
                ]
            );
        } elseif ($filterEstartDateTo != '') {
            $quotes->addFieldToFilter(
                ['evaluation_start'],
                [
                    ['datetime' => true, 'from' => $this->cmSoftwareHelper->getMinDate(), 'to' =>  $filterEstartDateTo]

                ]
            );
        }


        //filter award period
        if ($filterAwardDateFrom != '') {
            $quotes->addFieldToFilter(
                ['award_date'],
                [
                    ['gteq' =>  $filterAwardDateFrom]
                ]
            );
        }
        if ($filterAwardDateTo != '') {
            $quotes->addFieldToFilter(
                ['award_date'],
                [
                    ['lteq' =>  $filterAwardDateTo]

                ]
            );
        }

        //filter amount
        if ($filterAmountFrom != '') {
            $quotes->addFieldToFilter(
                'amount',
                [
                    'gteq' => $filterAmountFrom,
                    'lteq' =>  $filterAmountTo
                ]
            );
        }
        if ($filterAmountTo != '') {
            $quotes->addFieldToFilter(
                'amount',
                [
                    'lteq' =>  $filterAmountTo
                ]
            );
        }

        $attrProductName = $this->eavAttribute->getIdByCode('catalog_product', 'name');

        if ($this->cmSoftwareHelper->getCurrentStoreCode() != SoftwareRenewalConstants::STORE_CODE
            && $this->cmSoftwareHelper->getCurrentStoreCode() != \Formax\ConfigCmSoftware\Helper\Data::SOFTWARE_STOREVIEW_CODE
        ) {
            $quotes->getSelect()->join(
                ['e' => $quotes->getResource()->getTable('catalog_product_entity')],
                'main_table.product_id = e.entity_id',
                ['sku', 'type_id']
            )->join(
                ['cpev' => $quotes->getResource()->getTable('catalog_product_entity_varchar')],
                'e.row_id = cpev.row_id AND cpev.store_id = 0 AND cpev.attribute_id = ' . $attrProductName,
                ['product_name' => 'cpev.value']
            )->join(
                ['qi' => 'requestforquote_quote_info'],
                'main_table.entity_id = qi.quote_id',
                []
            )->join(
                ['ce' => 'customer_entity'],
                'ce.entity_id = qi.seller_id',
                []
            );
            $quotes->addFieldToFilter('ce.store_id', $this->_storeManager->getStore()->getId());
        } else {
            $quotes->getSelect()->join(
                ['e' => $quotes->getResource()->getTable('catalog_product_entity')],
                'main_table.product_id = e.entity_id',
                ['sku', 'type_id']
            )->join(
                ['cpev' => $quotes->getResource()->getTable('catalog_product_entity_varchar')],
                'e.row_id = cpev.row_id AND cpev.attribute_id = ' . $attrProductName,
                ['product_name' => 'cpev.value']
            );
            $quotes->addFieldToFilter('cpev.store_id', $this->_storeManager->getStore()->getId());
        }

        if ($filterPname) {
            $quotes->addFieldToFilter(
                'cpev.value',
                [
                    ['eq' => $filterPname],
                    ['like' => "%" . $filterPname . "%"]
                ]
            );
        }

        $quotes->setOrder('entity_id', 'DESC');
        //echo $quotes->getSelect();
        $this->setCollection($quotes);
    }

    /**
     * Get customer's purchase unit list
     *
     * @return array
     */
    public function getPurchaseUnitList()
    {
        return $this->cmSoftwareHelper->getPurchaseUnitByCustomer();
    }

    /**
     * Get unit purchase name by code
     *
     * @param string $code
     * @return string
     */
    public function getUnitPurchaseName($code)
    {
        $name = '';
        if ($code) {
            $name = isset($this->purchaseUnitList[$code]) ? $this->purchaseUnitList[$code] : '';
        }

        return $name;
    }

    /**
     * Format datetime to date
     *
     * @return string
     */
    public function formatDateTime($date)
    {
        return $this->cmSoftwareHelper->formatDateTime($date);
    }

    public function formatStartDateTime($date)
    {
        return $this->cmSoftwareHelper->formatStartDateTime($date);
    }

    public function formatDateDB($date)
    {
        return $this->cmSoftwareHelper->formatDateDB($date);
    }

    /**
     * Statuses
     *
     * @return string
     */
    public function getQuoteInfoStatus($quoteId){
        //requestforquote_quote_info - entity_id
        //requestforquote_quote_conversation - seller_quote_id

        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from(['quote_info' => 'requestforquote_quote_info'], ['customer_status']);
        $select->join(
                ['quote_conversation' => 'requestforquote_quote_conversation'],
                'quote_conversation.seller_quote_id = quote_info.entity_id',
                ['*']
            );
        $select->where('quote_info.quote_id = '. $quoteId);
        return $resourceConnection->fetchOne($select);
    }

    public function getParams($idQuote){
        $con = $this->resourceConnection->getConnection();
        $sql = "SELECT
                    entity_id,
                    seller_id
                FROM
                    requestforquote_quote_info
                WHERE
                    quote_id=".$idQuote."
                    AND status=3
                    AND product_created=1";
        $result = $con->fetchAll($sql);
        $data = array(
            'sellerId' => 0,
            'quoteInfoId' => 0
        );
        foreach($result as $item){
            $data = array(
                'sellerId' => $item['seller_id'],
                'quoteInfoId' => $item['entity_id']
            );
        }
        return $data;
    }

    public function getAllParams($idQuote)
    {
        $con = $this->resourceConnection->getConnection();
        $sql = "SELECT q.entity_id, i.seller_id
                FROM requestforquote_quote q
                INNER JOIN requestforquote_quote_conversation c
                ON q.approved_seller_quote_id = c.entity_id AND c.edited = 1
                INNER JOIN requestforquote_quote_info i
                ON c.seller_quote_id = i.entity_id
                WHERE q.entity_id = " . $idQuote;
        $result = $con->fetchAll($sql);
        $data = [
            'sellerId' => 0,
            'quoteInfoId' => 0
        ];

        foreach ($result as $item) {
            $data = [
                'sellerId' => $item['seller_id'],
                'quoteInfoId' => $item['entity_id']
            ];
        }

        return $data;
    }

    public function getProductById($productID){
        try {
            return $this->_productRepository->getById($productID);
        } catch (NoSuchEntityException $e) {
            return null;
        }

        return null;
    }

    public function isProductEvaluated($productId)
    {
        $result = false;

        if ((int)$productId > 0) {
            $store = $this->cmSoftwareHelper->getStoreManager();
            $storeId = $store->getStore()->getId();
            $resourceConnection = $this->resourceConnection->getConnection();
            $select = $resourceConnection->select();
            $select->from(['ei' => 'catalog_product_entity_int'], ['ei.value']);
            $select->join(
                ['eav' => 'eav_attribute'],
                "ei.attribute_id = eav.attribute_id AND eav.attribute_code = 'swqa_evaluated' AND ei.store_id = " . $storeId,
                []
            )->join(
                ['e' => 'catalog_product_entity'],
                'ei.row_id = e.row_id',
                []
            );
            $select->where('e.entity_id = '. $productId);
            $result = (bool)$resourceConnection->fetchOne($select);
        }

        return $result;
    }

    public function getAttributeCollection()
    {
        $attributeCollection = $this->eavAttribute->getCollection();
        $attributeCollection->addFieldToFilter('is_user_defined', 1);
        $attributeCollection->addFieldToFilter(
            'entity_type_id',
            $this->entity->setType('catalog_product')->getTypeId()
        );
        return $attributeCollection;
    }

    public function getSellerNameById($sellerId)
    {
        $shopName = '';
        $customer = $this->customerFactory->create()->load($sellerId);
        if ($customer->getId() > 0) {
            $shopName = $customer->getFirstname() . ' ' . $customer->getLastname();
        }

        return $shopName;
    }


    /**
     * @return array
     */
    public function getCategoriesCmSoftware()
    {
        return $this->cmSoftwareHelper->getCategoriesCmSoftware();
    }

    public function getQuoteDetail($quoteId)
    {
        return $this->_quoteSupplierHelper->getQuoteDetails($quoteId);
    }
}
