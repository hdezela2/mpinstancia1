<?php
namespace Formax\QuotesSupplier\Block\Account\Seller;

use Magento\Framework\Serialize\SerializerInterface;

/**
 * Requestforquote block.
 *
 * @author Webkul Software
 */
class TeamJob extends \Magento\Framework\View\Element\Template{

    private $_info;
    private $_idQuote;
    private $_quote;
    private $_comprador;
    private $_teamJobFactory;
    private $_servicesFactory;
    private $_dir;
    protected $_serializer;
    protected $_storeManager;
    private $_orderFactory;
    private $_resource;
    public $priceHelper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Requestforquote\Model\InfoFactory $info,
        \Magento\Framework\App\RequestInterface $request,
        \Webkul\Requestforquote\Model\QuoteFactory $quote,
        \Magento\Customer\Model\CustomerFactory $comprador,
        \Formax\QuotesSupplier\Model\TeamJobFactory $teamJobFactory,
        \Formax\QuotesSupplier\Model\ServicesFactory $servicesFactory,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Sales\Api\OrderRepositoryInterface $orderFactory,
        SerializerInterface $serializer,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        array $data = []
    ) {
        $this->_idQuote         = $request->getParam('id');
        $this->_info            = $info->create()->load($this->_idQuote);
        $this->_quote           = $quote->create()->load($this->_info['quote_id']);
        $this->_comprador       = $comprador->create()->load($this->_quote['customer_id']);
        $this->_teamJobFactory  = $teamJobFactory;
        $this->_servicesFactory = $servicesFactory;
        $this->_dir             = $dir;
        $this->_serializer      = $serializer;
        $this->_storeManager    = $storeManager;
        $this->_orderFactory    = $orderFactory;
        $this->_resource        = $resource;
        $this->priceHelper      = $priceHelper;
        $this->timezone         = $timezone;

        parent::__construct($context, $data);

    }
    public function getInfoQuote(){
        return $this->_info;
    }
    public function getQuote(){
        return $this->_quote;
    }
    public function getComprador(){
        return $this->_comprador;
    }
    public function getSellerName(){
        return trim($this->_comprador['firstname'].' '.$this->_comprador['lastname']);
    }
    public function getCollectionTeamJob(){
        $collection = $this->_teamJobFactory->create()->getCollection();
        $collection->addFieldToFilter(
            'quote_id',array(
                array('eq' => $this->_info['quote_id'])
            )
        );
        return $collection;
    }
    public function getCollectionServices(){
        $collection = $this->_servicesFactory->create()->getCollection();
        $collection->addFieldToFilter(
            'quote_id',array(
                array('eq' => $this->_info['quote_id'])
            )
        );
        return $collection;
    }
    public function getPathMedia(){
        return $this->_dir->getPath('media');
    }
    public function unSerializeData($extraData){
        $additionalData = $this->_serializer->unserialize($extraData);
        return $additionalData;
    }
    public function getUrlBaseInforme(){
        $tmp = str_replace(array("software/","software2022/"),"",$this->_storeManager->getStore()->getBaseUrl());
        return $tmp.'media/informeteam/files';
    }
    public function getUrlBaseCv(){
        $tmp = str_replace(array("software/","software2022/"),"",$this->_storeManager->getStore()->getBaseUrl());
        return $tmp.'media/cvteam/files';
    }
    public function getOrderByQuote(){
        $con = $this->_resource->getConnection();
        $sql = 'SELECT o.entity_id FROM sales_order o
        INNER JOIN requestforquote_quote q
        ON o.requestforquote_info = q.entity_id
        INNER JOIN requestforquote_quote_info i
        ON q.entity_id = i.quote_id
        INNER JOIN requestforquote_quote_conversation c
        ON i.entity_id = c.seller_quote_id
        WHERE i.entity_id = ' . $this->_idQuote . ' LIMIT 1';
        $idOrder = $con->fetchOne($sql);
        if(isset($idOrder) && $idOrder!=null){
            $order = $this->_orderFactory->get($idOrder);
            return $order;
        }else{
            return null;
        }
    }
    public function getValueNumber($amount) {
        return $this->priceHelper->currency($amount, true, false);
    }

    /**
     * Retrive next day
     *
     * @return \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    public function getCurrentDay()
    {
        return $this->timezone->date();
    }

}
