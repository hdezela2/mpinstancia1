<?php

namespace Formax\QuotesSupplier\Block\Customer;

use Magento\Framework\App\ResourceConnection;
use Formax\ConfigCmSoftware\Helper\Data as CmSoftwareHelper;
use Formax\QuotesSupplier\Helper\Data as QuoteSupplierHelper;
use Magento\Framework\Filesystem\Io\File;

class View extends \Webkul\Requestforquote\Block\Account\Customer\View
{
    /**
     * @var \Formax\ConfigCmSoftware\Helper\Data
     */
    protected $cmSoftwareHelper;

    /**
     * @var \Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory
     */
    protected $quoteCollection;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var array
     */
    protected $purchaseUnitList;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $catalogProductFactory;

    /**
     * @var \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory
     */
    protected $infoCollectionFactory;

    /** @var QuoteSupplierHelper */
    protected $quoteSupplierHelper;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Catalog\Model\ProductFactory $catalogProductFactory
     * @param \Webkul\Requestforquote\Model\QuoteFactory $quote
     * @param \Webkul\Requestforquote\Model\ConversationFactory $conversation
     * @param \Webkul\Requestforquote\Model\InfoFactory $info
     * @param \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory $conversationFactory
     * @param \Webkul\Marketplace\Model\ProductFactory $product
     * @param \Webkul\Requestforquote\Helper\Data $helper
     * @param \Magento\Catalog\Helper\Image $catalogImgHelper
     * @param \Magento\Catalog\Helper\Output $catalogOpHelper
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $mpSellerColl
     * @param \Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory $quoteCollection
     * @param \Formax\ConfigCmSoftware\Helper\Data $cmSoftwareHelper
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Catalog\Model\ProductFactory $catalogProductFactory,
        \Webkul\Requestforquote\Model\QuoteFactory $quote,
        \Webkul\Requestforquote\Model\ConversationFactory $conversation,
        \Webkul\Requestforquote\Model\InfoFactory $info,
        \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoFactory,
        \Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory $conversationFactory,
        \Webkul\Marketplace\Model\ProductFactory $product,
        \Webkul\Requestforquote\Helper\Data $helper,
        \Magento\Catalog\Helper\Image $catalogImgHelper,
        \Magento\Catalog\Helper\Output $catalogOpHelper,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $mpSellerColl,
        File $file,
        \Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory $quoteCollection,
        CmSoftwareHelper $cmSoftwareHelper,
        ResourceConnection $resourceConnection,
        QuoteSupplierHelper $quoteSupplierHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $customerFactory,
            $catalogProductFactory,
            $quote,
            $conversation,
            $info,
            $infoFactory,
            $conversationFactory,
            $product,
            $helper,
            $catalogImgHelper,
            $catalogOpHelper,
            $priceHelper,
            $mpHelper,
            $mpSellerColl,
            $file,
            $data
        );

        $this->infoCollectionFactory = $infoFactory;
        $this->catalogProductFactory = $catalogProductFactory;
        $this->quoteCollection = $quoteCollection;
        $this->cmSoftwareHelper = $cmSoftwareHelper;
        $this->resourceConnection = $resourceConnection;
        $this->purchaseUnitList = [];
        $this->quoteSupplierHelper = $quoteSupplierHelper;
    }

    /**
     * Get requested quote data by id
     *
     * @param  int $quoteId
     * @return Webkul_Requestforquote_ModelQuote
     */
    public function getQuoteById($quoteId)
    {
        $connection = $this->resourceConnection->getConnection();
        $catalogTableName = $connection->getTableName('catalog_product_entity');
        $quotes = $this->quoteCollection->create();
        $quotes->addFieldToFilter('entity_id', $quoteId)->setPageSize(1)
            ->getSelect()->join(
                ['e' => $catalogTableName],
                'main_table.product_id = e.entity_id',
                ['sku']
            );

        foreach ($quotes as $quote) {
            return $quote;
        }

        return null;
    }

    /**
     * Get product info by productId
     *
     * @param  int $productRowId
     * @return Magento_Catalog_Model_Product
     */
    public function getProductRowDetails($productRowId)
    {
        return $this->catalogProductFactory->create()->load($productRowId, 'row_id');
    }

    /**
     * Get requested quote info collection by quoteId
     *
     * @param  int $quoteId
     * @return Webkul_Requestforquote_Model_ResourceInfo_Collection
     */
    public function getCmSoftwareHelper()
    {
        return $this->cmSoftwareHelper;
    }

    public function getQuoteSupplierHelper()
    {
        return $this->quoteSupplierHelper;
    }

    /**
     * Format datetime to date
     *
     * @return string
     */
    public function formatDateTime($date)
    {
        return $this->cmSoftwareHelper->formatDateTime($date);
    }

    public function formatStartDateTime($date)
    {
        return $this->cmSoftwareHelper->formatStartDateTime($date);
    }
}
