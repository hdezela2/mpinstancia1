<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Requestforquote
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Formax\QuotesSupplier\Block\Rewrite\Account\Seller;

use Magento\Catalog\Helper\Image;
use Magento\Catalog\Helper\Output;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Pricing\Helper\Data;

/**
 * Requestforquote block.
 *
 * @author Webkul Software
 */
class View extends \Webkul\Requestforquote\Block\Account\Seller\View
{

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    private $customerFactory;

    /**
     * @var \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory
     */
    private $infoFactory;

    /**
     * @var \Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory
     */
    private $conversationCollectionFactory;

    /**
     * @var \Webkul\Requestforquote\Model\QuoteFactory
     */
    private $quote;

    /**
     * @var \Webkul\Requestforquote\Model\InfoFactory
     */
    private $info;

    /**
     * @var \Webkul\Requestforquote\Model\ConversationFactory
     */
    private $conversation;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $product;

    /**
     * @var \Webkul\Requestforquote\Helper\Data
     */
    private $helper;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    private $mpHelper;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Seller\Collection
     */
    private $mpSellerColl;
    protected Output $catalogOutHelper;
    protected Image $catalogImgHelper;
    private ResourceConnection $resourceConnection;
    protected Data $priceHelper;

    /**
     * Undocumented function
     *
     * @param \Magento\Framework\View\Element\Template\Context                           $context
     * @param \Magento\Customer\Model\CustomerFactory                                    $customerFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory        $quoteFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory         $infoFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory $conversationCollectionFactory
     * @param \Webkul\Requestforquote\Model\QuoteFactory                                 $quote
     * @param \Webkul\Requestforquote\Model\InfoFactory                                  $info
     * @param \Webkul\Requestforquote\Model\ConversationFactory                          $conversation
     * @param \Magento\Catalog\Model\ProductFactory                                      $product
     * @param \Webkul\Requestforquote\Helper\Data                                        $helper
     * @param \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory           $mpSellerColl
     * @param \Webkul\Marketplace\Helper\Data                                            $mpHelper
     * @param array                                                                      $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoFactory,
        \Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory $conversationCollectionFactory,
        \Webkul\Requestforquote\Model\QuoteFactory $quote,
        \Webkul\Requestforquote\Model\InfoFactory $info,
        \Webkul\Requestforquote\Model\ConversationFactory $conversation,
        \Magento\Catalog\Model\ProductFactory $product,
        \Webkul\Requestforquote\Helper\Data $helper,
        Output $catalogOutHelper,
        Image $catalogImgHelper,
        Data $priceHelper,
        \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $mpSellerColl,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        File $file,
        ResourceConnection $resourceConnection,
        array $data = []
    ) {

        $this->customerFactory = $customerFactory;
        $this->infoFactory = $infoFactory;
        $this->conversationCollectionFactory = $conversationCollectionFactory;
        $this->quote = $quote;
        $this->info = $info;
        $this->conversation = $conversation;
        $this->product = $product;
        $this->mpSellerColl = $mpSellerColl;
        $this->mpHelper = $mpHelper;
        $this->helper = $helper;
        $this->catalogOutHelper = $catalogOutHelper;
        $this->catalogImgHelper = $catalogImgHelper;
        $this->priceHelper = $priceHelper;
        parent::__construct(
            $context,
            $customerFactory,
            $infoFactory,
            $conversationCollectionFactory,
            $quote,
            $info,
            $conversation,
            $product,
            $helper,
            $catalogOutHelper,
            $catalogImgHelper,
            $priceHelper,
            $mpSellerColl,
            $mpHelper,
            $file,
            $data
        );
        $this->resourceConnection = $resourceConnection;

        $sellerQuoteId = $this->getRequest()->getParam('id');
        $quotes = $this->conversationCollectionFactory->create()
            ->addFieldToFilter('seller_quote_id', $sellerQuoteId)
            ->setOrder('created_at', 'DESC');

        $this->setCollection($quotes);
    }

    /**
     * get filter data for rate
     *
     * @return array
     */
    public function getQuoteById($quoteId)
    {
        return $this->quote->create()->load($quoteId);
    }


    public function getQuoteCustomData($quoteId)
    {
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from('requestforquote_quote', ['quotedatedelivery', 'quotedatequestion', 'quoteaddress', 'quotecomments', 'quotedatereceive', 'quoteall', 'quotenotif']);
        $select->where('entity_id = '. $quoteId);
        $result_quote=$resourceConnection->fetchAll($select);
        return $result_quote[0];
    }

    /**
     * Get requested quote info collection by id
     *
     * @param  int $quoteId
     * @return Webkul_Requestforquote_Model_Resource_Info_Collection
     */
    public function getQuoteInfoById($id)
    {
        return $this->info->create()->load($id);
    }

    /**
     * Get requested quote info collection by id
     *
     * @param  int $quoteId
     * @return Webkul_Requestforquote_Model_Resource_Info_Collection
     */
    public function getApprovedRfqInfo($id)
    {
        return $this->conversation->create()->load($id);
    }

    /**
     * Get product info by productId
     *
     * @param  int $productId
     * @return Magento_Catalog_Model_Product
     */
    public function getProductDetails($productId)
    {
        return $this->product->create()->load($productId);
    }

    /**
     * Get seller name
     *
     * @return string
     */
    public function getSellerName()
    {
        $customerId = $this->mpHelper->getCustomerId();
        $customer = $this->customerFactory->create()->load($customerId);
        return $customer['firstname']." ".$customer['lastname'];
    }

    /**
     * Get customer name
     *
     * @return string
     */
    public function getCustomerNameById($customerId)
    {
        $customer = $this->customerFactory->create()->load($customerId);
        return $customer['firstname']." ".$customer['lastname'];
    }

    /**
     * get the country name from country code
     *
     * @param  string $countryCode
     * @return string
     */
    public function getCountryNameByCode($countryCode = "")
    {
        return $this->helper->getCountryname($countryCode);
    }

    /**
     * get the logo of seller
     *
     * @return string
     */
    public function getSellerLogoBySellerId()
    {
        $sellerId = $this->mpHelper->getCustomerId();
        $logo = 'noimage.png';
        $userdata = $this->mpSellerColl->create()
            ->addFieldToFilter('seller_id', $sellerId);
        if ($userdata->getSize()) {
            foreach ($userdata as $key => $value) {
                if ($value->getLogopic()) {
                    $logo = $value->getLogopic();
                }
            }
        }
        return $logo;
    }

    /**
     * getMediaUrl get the media url
     *
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]);
    }

    public function getHelperObj()
    {
        return $this->helper;
    }

    public function getCatalogOpHelper()
    {
        return $this->catalogOutHelper;
    }

    public function getCatalogImgHelper()
    {
        return $this->catalogImgHelper;
    }

    public function getMpHelper()
    {
        return $this->mpHelper;
    }

    public function getPriceHelper()
    {
        return $this->priceHelper;
    }

    public function getRequestObj()
    {
        return $this->getRequest();
    }
}
