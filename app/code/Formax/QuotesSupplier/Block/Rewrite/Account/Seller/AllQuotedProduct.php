<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Requestforquote
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Formax\QuotesSupplier\Block\Rewrite\Account\Seller;

use Magento\Catalog\Helper\Image;
use Magento\Framework\App\ResourceConnection;
use Webkul\Requestforquote\Helper\Data;

/**
 * Requestforquote block.
 *
 * @author Webkul Software
 */
class AllQuotedProduct extends \Webkul\Requestforquote\Block\Account\Seller\AllQuotedProduct
{
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    private $customerFactory;

    /**
     * @var \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory
     */
    private $infoFactory;

    /**
     * @var \Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory
     */
    private $conversationCollectionFactory;

    /**
     * @var \Webkul\Requestforquote\Model\QuoteFactory
     */
    private $quote;

    /**
     * @var \Webkul\Requestforquote\Model\InfoFactory
     */
    private $info;

    /**
     * @var \Webkul\Requestforquote\Model\ConversationFactory
     */
    private $conversation;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $product;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    private $mpHelper;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Seller\Collection
     */
    private $mpSellerColl;
    private Data $helper;
    private Image $helperImage;
    private ResourceConnection $resourceConnection;

    /**
     * __construct function
     *
     * @param \Magento\Framework\View\Element\Template\Context                           $context
     * @param \Magento\Customer\Model\CustomerFactory                                    $customerFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory         $infoFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory $conversationCollectionFactory
     * @param \Webkul\Requestforquote\Model\QuoteFactory                                 $quote
     * @param \Webkul\Requestforquote\Model\InfoFactory                                  $info
     * @param \Webkul\Requestforquote\Model\ConversationFactory                          $conversation
     * @param \Magento\Catalog\Model\ProductFactory                                      $product
     * @param \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory           $mpSellerColl
     * @param \Webkul\Marketplace\Helper\Data                                            $mpHelper
     * @param array                                                                      $data
     */

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoFactory,
        \Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory $conversationCollectionFactory,
        \Webkul\Requestforquote\Model\QuoteFactory $quote,
        \Webkul\Requestforquote\Model\InfoFactory $info,
        \Webkul\Requestforquote\Model\ConversationFactory $conversation,
        \Magento\Catalog\Model\ProductFactory $product,
        \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $mpSellerColl,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        Data $helper,
        Image $helperImage,
        ResourceConnection $resourceConnection,
        array $data = []
    ) {
        $this->customerFactory = $customerFactory;
        $this->infoFactory = $infoFactory;
        $this->conversationCollectionFactory = $conversationCollectionFactory;
        $this->quote = $quote;
        $this->info = $info;
        $this->conversation = $conversation;
        $this->product = $product;
        $this->mpHelper = $mpHelper;
        $this->helper = $helper;
        $this->helperImage = $helperImage;
        $this->mpSellerColl = $mpSellerColl;
        parent::__construct(
            $context,
            $customerFactory,
            $infoFactory,
            $conversationCollectionFactory,
            $quote,
            $info,
            $conversation,
            $product,
            $mpSellerColl,
            $mpHelper,
            $helper,
            $helperImage,
            $data
        );
        $this->resourceConnection = $resourceConnection;
        $sellerQuoteId = $this->getRequest()->getParam('id');
        $filter = '';
        $filterDateFrom = '';
        $filterDateTo = '';
        if ($quoteKeyword = $this->getRequest()->getParam('s')) {
            $filter = $quoteKeyword != "" ? $quoteKeyword : "";
        }
        if ($quoteFromDate = $this->getRequest()->getParam('from_date')) {
            $filterDateFrom = $quoteFromDate != "" ? $quoteFromDate : "";
        }
        if ($quoteToDate = $this->getRequest()->getParam('to_date')) {
            $filterDateTo = $quoteToDate != "" ? $quoteToDate." 23:59:59" : "";
        }
        $filterStatus = 0;
        if ($this->getRequest()->getParam('status') != '') {
            $status = $this->getRequest()->getParam('status');
            $filterStatus = $status != '' ? $status : '';
        }
        $quotes = $this->quote->create()->getCollection();
        $quotes->addFieldToFilter('product_id', ['neq' => 0]);
        if ($filter) {
            $quotes->addFieldToFilter('subject', ['like'=>"%".$filter."%"]);
        }
        if ($filterDateFrom && $filterDateTo) {
            $quotes->addFieldToFilter(
                'created_at',
                [
                                'from' => $filterDateFrom,
                                'to' => $filterDateTo,
                                'date' => true
                ]
            );
        }
        if ($filterStatus) {
            $quotes->addFieldToFilter(
                'status',
                [
                                'eq' => $filterStatus
                ]
            );
        }
        $this->setCollection($quotes);
    }

    /**
     * get filter data for rate
     *
     * @return array
     */
    public function requestData()
    {
        return $this->getRequest()->getParams();
    }

    /**
     * get the collection of all quoted product(s)
     *
     * @return \Webkul\Requestforquote\Model\Quote
     */
    public function getAllQuotedProductCollection()
    {
        return $this->getCollection();
    }

    /**
     * _prepareLayout prepare pager for rules list
     *
     * @return void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getCollection()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'requestforquote.allquotedproducts.list.pager'
            )->setCollection(
                $this->getCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCollection()->load();
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get requested quote count by status
     *
     * @param  int $quoteId
     * @param  int $status
     * @return Webkul_Requestforquote_Model_Quote
     */
    public function getRequestedQuoteCountByStatus($status)
    {
        $sellerId = $this->mpHelper->getCustomerId();
        $quotes = $this->infoFactory->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('status', $status);
        return count($quotes);
    }

    /**
     * Get requested quote data
     *
     * @return Webkul_Requestforquote_Model_Quote
     */
    public function getQuoteById($quoteId)
    {
        return $this->quote->create()->load($quoteId);
    }

    public function getQuoteCustomData($quoteId)
    {
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from('requestforquote_quote', ['quotedatedelivery', 'quotedatequestion', 'quoteaddress', 'quotecomments', 'quotedatereceive', 'quoteall', 'quotenotif']);
        $select->where('entity_id = '. $quoteId);
        $result_quote=$resourceConnection->fetchOne($select);
        return $result_quote;
    }


    /**
     * Get requested quote info collection by id
     *
     * @param  int $quoteId
     * @return Webkul_Requestforquote_Model_Resource_Info_Collection
     */
    public function getQuoteInfoById($id)
    {
        return $this->info->create()->load($id);
    }

    /**
     * Get requested quote info collection by id
     *
     * @param  int $quoteId
     * @return Webkul_Requestforquote_Model_Resource_Info_Collection
     */
    public function getApprovedRfqInfo($id)
    {
        return $this->conversation->create()->load($id);
    }

    /**
     * Get product info by productId
     *
     * @param  int $productId
     * @return Magento_Catalog_Model_Product
     */
    public function getProductDetails($productId)
    {
        return $this->product->create()->load($productId);
    }

    /**
     * Get seller name
     *
     * @return string
     */
    public function getSellerName()
    {
        $customerId = $this->mpHelper->getCustomerId();
        $customer = $this->customerFactory->create()->load($customerId);
        return $customer['firstname']." ".$customer['lastname'];
    }

    /**
     * Get customer name
     *
     * @return string
     */
    public function getCustomerNameById($customerId)
    {
        $customer = $this->customerFactory->create()->load($customerId);
        return $customer['firstname']." ".$customer['lastname'];
    }

    /**
     * get the country name from country code
     *
     * @param  string $countryCode
     * @return string
     */
    public function getCountryNameByCode($countryCode = "")
    {
        return $this->helper->getCountryname($countryCode);
    }

    /**
     * get the logo of seller
     *
     * @return string
     */
    public function getSellerLogoBySellerId()
    {
        $sellerId = $this->mpHelper->getCustomerId();
        $logo = 'noimage.png';
        $userdata = $this->mpSellerColl->create()
            ->addFieldToFilter('seller_id', $sellerId);
        if ($userdata->getSize()) {
            foreach ($userdata as $key => $value) {
                if ($value->getLogopic()) {
                    $logo = $value->getLogopic();
                }
            }
        }
        return $logo;
    }

    public function getHelperObj()
    {
        return $this->helper;
    }

    public function getHelperImage()
    {
        return $this->helperImage;
    }

    public function getRequestObj()
    {
        return $this->getRequest();
    }
}
