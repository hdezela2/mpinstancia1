<?php

namespace Formax\QuotesSupplier\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Webkul\Requestforquote\Model\QuoteFactory;
use Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory as InfoCollectionFactory;

class Data extends AbstractHelper
{
    /** @var int */
    const PENDING = 0;
    const PENDING_LABEL = 'Pending';

    /** @var int */
    const PUBLISHED = 1;
    const PUBLISHED_LABEL = 'Published';

    /** @var int */
    const EVALUATION = 3;
    const EVALUATION_LABEL = 'In Evaluation';

    /** @var int */
    const AWARDED = 4;
    const AWARDED_LABEL = 'Provider Selected';

    /** @var int */
    const DESERT = 5;
    const DESERT_LABEL = 'Desert';

    /** @var int */
    const FINISHED = 2;
    const FINISHED_LABEL = 'Finished';

    /** @var int */
    const DRAFT = 6;
    const DRAFT_LABEL = 'Saved';

    /** @var int */
    const PUBLICATION_CLOSED = 7;
    const PUBLICATION_CLOSED_LABEL = 'Publication Closed';

    /** @var int */
    const EVALUATION_CLOSED = 8;
    const EVALUATION_CLOSED_LABEL = 'Evaluation Closed';

    /** @var int */
    const REJECTED = 9;
    const REJECTED_LABEL = 'Rejected';

    /** @var int */
    const DELETED = 10;
    const DELETED_LABEL = 'Deleted';

    /** @var InfoCollectionFactory */
    protected $infoCollectionFactory;

    private $resourceConnection;

    protected $customerSession;

    protected $mpHelper;

    /** @var QuoteFactory */
    protected $_quoteFactory;


    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        InfoCollectionFactory $infoCollectionFactory,
        QuoteFactory $quoteFactory
    ) {
        $this->customerSession = $customerSession;
        $this->mpHelper = $mpHelper;
        $this->resourceConnection = $resourceConnection;
        $this->infoCollectionFactory = $infoCollectionFactory;
        $this->_quoteFactory = $quoteFactory;
        parent::__construct($context);
    }

    public function isLoggedIn()
    {
        return $this->mpHelper->isCustomerLoggedIn();
    }

    public function isSeller(){
        return $this->mpHelper->isSeller();
    }

    public function getNotification()
    {
        if ($this->isLoggedIn()) {
            $resourceConnection = $this->resourceConnection->getConnection();
            $select = $resourceConnection->select()->distinct();
            $select->from(['quotes' => 'requestforquote_quote'], ['entity_id']);
            $select->join(
                    ['quotesinfo' => 'requestforquote_quote_info'],
                    'quotes.entity_id = quotesinfo.quote_id',
                    ['quotesinfo.quote_id', 'quotes.customer_id']
                );
            $select->where('quotes.customer_id = '. $this->mpHelper->getCustomerId());
            $select->where('quotesinfo.customer_status = 2');
            $select->where('quotesinfo.shownotif = 1');
            $result_quotes=$resourceConnection->fetchAll($select);
            return $result_quotes;
        }
        return false;
    }

    /**
     * Get number of sellers that responded
     *
     * @param int $quoteId
     * @return int
     */
    public function getNumberSellersresponded($quoteId)
    {
        $quotesCount = 0;
        if ((int)$quoteId > 0) {
            $infoCollectionFactory = $this->infoCollectionFactory->create();
            $quotesCount = $infoCollectionFactory->addFieldToFilter('quote_id', $quoteId)
                ->addFieldToFilter('status', 2)
                ->getSize();

        }

        return $quotesCount;
    }

    public function getQuoteDetails($quoteId)
    {
        $resultCollection = null;
        $collection = $this->_quoteFactory->create()->getCollection()
            ->addFieldToFilter('entity_id', $quoteId);
        $collection->getSelect()
            ->joinLeft(
                ['cv' => $this->resourceConnection->getTableName('requestforquote_quote_conversation')],
                'main_table.approved_seller_quote_id = cv.entity_id AND cv.edited = 1',
                []
            )
            ->joinLeft(
                ['info' => $this->resourceConnection->getTableName('requestforquote_quote_info')],
                'cv.seller_quote_id = info.entity_id',
                []
            )
            ->joinLeft(
                ['seller' => $this->resourceConnection->getTableName('customer_entity')],
                'info.seller_id = seller.entity_id',
                ['seller_id' => 'seller.entity_id', 'seller_name' => new \Zend_Db_Expr('CONCAT(firstname, " ", lastname)')]
            )
            ->joinLeft(
                ['oc' => $this->resourceConnection->getTableName('sales_order')],
                'main_table.entity_id = oc.requestforquote_info',
                ['url_orden_de_compra', 'orden_de_compra']
            )
            ->joinLeft(
                ['ee' => $this->resourceConnection->getTableName('dccp_economic_evaluation')],
                'cv.seller_quote_id = ee.seller_quote_id',
                ['base_price', 'final_price']
            );
        $collection->setPageSize(1);
        foreach ($collection as $item) {
            $resultCollection = $item;
        }

        return $resultCollection;
    }

    public function getStatusLabel($statusId)
    {
        switch ($statusId) {
            case self::PENDING:
                return __(self::PENDING_LABEL);
            case self::PUBLISHED:
                return __(self::PUBLISHED_LABEL);
            case self::EVALUATION:
                return __(self::EVALUATION_LABEL);
            case self::AWARDED:
                return __(self::AWARDED_LABEL);
            case self::DESERT:
                return __(self::DESERT_LABEL);
            case self::DRAFT:
                return __(self::DRAFT_LABEL);
            case self::PUBLICATION_CLOSED:
                return __(self::PUBLICATION_CLOSED_LABEL);
            case self::EVALUATION_CLOSED:
                return __(self::EVALUATION_CLOSED_LABEL);
            case self::REJECTED:
                return __(self::REJECTED_LABEL);
            default:
                return __('Unknown Status');
        }
    }
}
