<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Requestforquote
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Formax\QuotesSupplier\Controller\Account;

use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Customer\Model\Session;
use Webkul\Marketplace\Model\SellerFactory;
use Webkul\Requestforquote\Helper\Data;
use Webkul\Requestforquote\Model\InfoFactory;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Store\Model\StoreManagerInterface;

class Post extends \Webkul\Requestforquote\Controller\Account\Post
{
    private DateTime $dateTime;
    private QuoteFactory $quoteFactory;
    private InfoFactory $infoFactory;
    private Data $dataHelper;
    private SellerFactory $userProfile;
    private CustomerFactory $customer;
    private SessionManagerInterface $session;
    private \Webkul\Marketplace\Helper\Data $mpHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param QuoteFactory $quoteFactory
     * @param ProductRepository $productRepository
     * @param InfoFactory $infoFactory
     * @param Data $dataHelper
     * @param SellerFactory $userProfileFactory
     * @param CustomerFactory $customerFactory
     * @param SessionManagerInterface $session
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param DateTime $date
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        QuoteFactory $quoteFactory,
        ProductRepository $productRepository,
        InfoFactory $infoFactory,
        Data $dataHelper,
        SellerFactory $userProfileFactory,
        CustomerFactory $customerFactory,
        SessionManagerInterface $session,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        DateTime $date,
        StoreManagerInterface $storeManager
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->infoFactory = $infoFactory;
        $this->dataHelper = $dataHelper;
        $this->userProfile = $userProfileFactory;
        $this->customer = $customerFactory;
        $this->session = $session;
        $this->mpHelper = $mpHelper;
        $this->dateTime = $date;
        $this->storeManager = $storeManager;
        parent::__construct(
            $context,
            $resultPageFactory,
            $quoteFactory,
            $infoFactory,
            $dataHelper,
            $productRepository,
            $userProfileFactory,
            $customerFactory,
            $session,
            $date,
            $mpHelper
        );
    }

    public function execute()
    {
        $store = $this->storeManager->getStore();
        $postData = $this->getRequest()->getParams();
        $customerId = $this->mpHelper->getCustomerId();
        $customer = $this->customer->create()->load($customerId);
        $model = $this->quoteFactory->create();
        $postData['subject'] = htmlentities($postData['subject']);
        $postData['description'] = htmlentities($postData['quotecomments']);
        $postData['selected_suppliers'] = explode(',', $postData['selected_suppliers']);
        $postData['customer_id'] = $customerId;
        $postData['quotedatedelivery'] = htmlentities($postData['quotedatedelivery']);
        $postData['quoteaddress'] = htmlentities($postData['quoteaddress']);
        $postData['quotecomments'] = htmlentities($postData['quotecomments']);
        $postData['quotedatereceive'] = htmlentities($postData['quotedatereceive']);
        $postData['quotedatequestion'] = (isset($postData['quotedatequestion']))?htmlentities($postData['quotedatequestion']):0;
        $postData['created_at'] = $this->dateTime->gmtDate('Y-m-d H:i:s');
        $postData['quoteall'] = htmlentities($postData['quoteall']);
        //Only for insumos
        if (!$this->dataHelper->getApprovalRequiredStatus() && $store->getCode() == InsumosConstants::WEBSITE_CODE){
            $postData['status'] = 1;
        }
        $model->setData($postData);
        $quoteId = $model->save()->getId();
        if (!$this->dataHelper->getApprovalRequiredStatus()) {
            $collection = $this->userProfile->create()->getCollection()
                ->addFieldToFilter('is_seller', ['eq'=>1])
                ->addFieldToFilter('seller_id', ['in'=>$postData['selected_suppliers']])
                ->addFieldToSelect('seller_id')
                ->distinct(true);
                foreach ($collection as $key => $value) {
                /* Send Quote Mail To Seller */
                $seller = $this->customer->create()->load($value['seller_id']);
                if ($seller->getId()) {
                    /* save quote info for seller */
                    $modelInfo = $this->infoFactory->create();
                    $modelInfo->setData($postData);
                    $modelInfo->setSellerId($value['seller_id']);
                    $modelInfo->setQuoteId($quoteId);
                    $modelInfo->setBulkQuoteQty($postData['bulk_order_qty']);
                    $modelInfo->setStatus(0);
                    $modelInfo->setCustomerStatus(2);
                    $modelInfo->setCreatedAt($postData['created_at']);
                    $modelInfo->setUpdatedAt($postData['created_at']);
                    $modelInfo->save();
                }
            }
        }
        $postData['quote_id']=$quoteId;
        $this->saveQuote($postData);
        $this->messageManager->addSuccess(__('Request for quote was successfully sent.'));
        $this->session->setRequestForQuoteFormData('');
        $this->session->unsetRequestForQuoteFormData();
        return;
        $this->_redirect('requestforquote/index/index');
    }

    public function saveQuote($postData){
        return true;
    }
}
