<?php

namespace Formax\QuotesSupplier\Controller\Account;

use Magento\Framework\Controller\ResultFactory;

class JobsTeam extends \Magento\Framework\App\Action\Action
{

    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
