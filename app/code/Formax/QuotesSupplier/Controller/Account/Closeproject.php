<?php

namespace Formax\QuotesSupplier\Controller\Account;

use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use Webkul\Requestforquote\Model\InfoFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * class Closeproject
 */
class Closeproject extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var InfoFactory
     */
    protected $info;

    /**
     * @var \Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory
     */
    private $quoteCollection;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param InfoFactory $info
     * @param ProductFactory $productFactory
     * @param StoreManagerInterface $storeManager
     * @param DateTime $date
     */
    public function __construct(
           Context $context,
           JsonFactory $resultJsonFactory,
           InfoFactory $info,
            \Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory $quoteCollection,
            Filter $filter
    ) {
        $this->info = $info;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->quoteCollection = $quoteCollection;
        $this->filter = $filter;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $post = $this->getRequest()->getPostValue();
        try {
            $quotes = $this->quoteCollection->create();
            $quotes->addFieldToFilter('entity_id', $post['quoteId']);
            foreach ($quotes as $quote) {
                    $quote->setStatus(2)->save();
            }
            $result->setData(['success' => true]);
            return $result;
        } catch (Exception $e) {
            $result->setData(['success' => false, 'error' => $e->getMessage()]);
            return $result;
        }
    }
}
