<?php

namespace Formax\QuotesSupplier\Controller\Account;

use Cleverit\Requestforquote\Helper\Data as RfqHelper;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Controller\AbstractAccount;
use Psr\Log\LoggerInterface;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\Requestforquote\Helper\Data as RequestforquoteHelper;
use Formax\CreateFormAttributes\Helper\Data as FormAttributesHelper;
use Formax\Offer\Helper\Data as HelperOffer;
use Formax\ActionLogs\Helper\Data as HelperActionLog;
use Formax\QuotesSupplier\Helper\Data as HelperQuoteSupplier;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;

class SaveDraft extends AbstractAccount
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var \Webkul\Requestforquote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Webkul\Requestforquote\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customer;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $mpHelper;

    /**
     * @var \Formax\CreateFormAttributes\Helper\Data
     */
    protected $fmHelper;

    /**
     * @var \Formax\Offer\Helper\Data
     */
    protected $helperOffer;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var  \Formax\ActionLogs\Helper\Data
     */
    private $_helperActionLog;

    /**
     * @var \Formax\ConfigCmSoftware\Helper\Data
     */
    protected $helperSoftware;

    /** @var Magento\Framework\Stdlib\DateTime\TimezoneInterface */
    protected $_timezoneInterface;

    /** @var RfqHelper */
    private $_rfqHelper;
    private ProductRepository $productRepository;

    public function __construct(
        Context $context,
        LoggerInterface $logger,
        QuoteFactory $quoteFactory,
        ProductRepository $productRepository,
        CustomerFactory $customerFactory,
        DateTime $dateTime,
        ScopeConfigInterface $scopeConfig,
        RequestforquoteHelper $dataHelper,
        MarketplaceHelper $mpHelper,
        FormAttributesHelper $fmHelper,
        HelperOffer $helperOffer,
        HelperActionLog $helperActionLog,
        HelperSoftware $helperSoftware,
        TimezoneInterface $timezoneInterface,
        RfqHelper $rfqHelper
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->productRepository = $productRepository;
        $this->dataHelper = $dataHelper;
        $this->customer = $customerFactory;
        $this->dateTime = $dateTime;
        $this->mpHelper = $mpHelper;
        $this->fmHelper = $fmHelper;
        $this->helperOffer = $helperOffer;
        $this->scopeConfig = $scopeConfig;
        $this->helperSoftware = $helperSoftware;
        $this->logger = $logger;
        $this->_helperActionLog = $helperActionLog;
        $this->_timezoneInterface = $timezoneInterface;
        $this->_rfqHelper = $rfqHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $editMode = false;
        $paramQuoteId = $this->getRequest()->getParam('quote_id', 0);
        if ($paramQuoteId > 0) {
            $model = $this->quoteFactory->create()->load($paramQuoteId);
            $productId = $model->getProductId();
            $editMode = true;
        } else {
            $productId = $this->getRequest()->getParam('product_id', 0);
            $model = $this->quoteFactory->create();
        }

        $postData = $this->getRequest()->getPostValue();
        //$filesData = $this->getRequest()->getFiles();
        $entity = 'requestforquote_quote';
        $currentProduct = $this->productRepository->getById($productId);
        //$data = $this->fmHelper->parsePostData($entity, $postData, $filesData);
        $data = $this->fmHelper->parsePostData($entity, $postData);
        $currentDate = $this->_timezoneInterface->date()->format('d-m-Y');
        $publicationDaysConfig = (int)$this->getSystemValue('requestforquote_publication_end_days');
        $evaluationDaysConfig = (int)$this->getSystemValue('requestforquote_evaluation_end_days');
        $questionDaysConfig = (int)$this->getSystemValue('requestforquote_questions_days');
        $keyDataPersistor = 'formattributes_' . $entity;

        // LOGS
        $actionName = 'Cotización';
        $profileName = 'Cliente';
        $extraData = [
            'action' => 'Borrador de Cotización',
            'status' => ''
        ];
        try {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

            if (!$this->dataHelper->checkForSellerExistence()
                && !$this->dataHelper->getApprovalRequiredStatus()
            ) {

                $extraData['status'] = 'Cannot request for quote. No seller associated with the current site';
                $this->_helperActionLog->saveLoggin($actionName, $profileName, $extraData);

                throw new LocalizedException(__('Cannot request for quote. No seller associated with the current site'));
            }

            if (isset($data['error']) && isset($data['msg']) && $data['error']) {
                throw new \Exception($data['msg'], FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

            if (!isset($postData['description']) || (isset($postData['description']) && $postData['description'] == '')) {
                $message = __("You must at least define requestforquote name.");
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

          //  $validateData = $this->fmHelper->validatePostData($entity, $postData, false);
            if (isset($validateData['error']) && $validateData['error']) {
                $message = isset($validateData['msg']) ? $validateData['msg'] : __('An error occured trying to save request for quote.');
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

//            if (isset($data['amount']) && !empty($data['amount'])) {
//                $iniUtmQty = $this->helperSoftware->getIniQtyUtm();
//                $endUtmQty = $this->helperSoftware->getEndQtyUtm();
//                $utmQty = $this->helperSoftware->getUtmQtyByAmount($data['amount']);
//                if ($utmQty < $iniUtmQty || $utmQty > $endUtmQty) {
//                    $message =  __(
//                        'Amount value %1 do not is between %2 and %3.',
//                        $this->helperSoftware->currencyFormat($data['amount']),
//                        $iniUtmQty,
//                        $endUtmQty
//                    );
//                    throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
//                }
//            }

            $customerId = $this->mpHelper->getCustomerId();
            $data['customer_id'] = $customerId;
            $customer = $this->customer->create()->load($customerId);
            $token = $customer->getData('user_rest_atk') ? $customer->getData('user_rest_atk') : '';

            $data['destination_country'] = 'CL';

            //Publication start day must begin at next workable day from today
            $response = $this->helperOffer->getEndBusinessDate($token, $currentDate, 1);
            if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                $date = $response['payload']['fecha_termino'];
                $currentDate = new \DateTime($date);
            } else {
                $extraData['status'] = 'An error occured trying to get the publication date';
                $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);
                throw new LocalizedException(__('An error occured trying to get the publication date.'));
            }

            // Get publication end date
            $publicationEndDate = null;
            if (!empty($data['publication_term'])) {
                $publicationTerm = (int)$data['publication_term'];

                $response = $this->helperOffer->getEndBusinessDate($token, $currentDate->format('d-m-Y'), $publicationTerm);
                if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                    $date = $response['payload']['fecha_termino'];
                    $publicationEndDate = new \DateTime($date);
                } else {
                    $extraData['status'] = 'An error occured trying to get the publication date';
                    $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);
                    throw new LocalizedException(__('An error occured trying to get the publication date.'));
                }

                //If PublicationEndDate is monday it must be replaced for the latest saturday
                if ($publicationEndDate->format('N') == "1") {
                    $publicationEndDate = $publicationEndDate->sub(new \DateInterval('P2D'));
                    // validate if previous Friday is holiday
                    $publicationEndDate = $this->_rfqHelper->validateEndDateFriday($publicationEndDate);
                }
            }

            // Get evaluation start and end dates
            $evaluationEndDate = null;
            $evaluationStartDate = null;
            if (!empty($data['evaluation_term']) && !empty($data['evaluation_start'])) {
                $evaluationTerm = (int)$data['evaluation_term'];
                $evaluationStartDate = new \DateTime($postData['evaluation_start']);

                // We add 1 day because the evaluation starts the next business day that was selected and closes at 00.00 of the following day
                $response = $this->helperOffer->getEndBusinessDate($token, $evaluationStartDate->format('d-m-Y'), $evaluationTerm + 1);
                if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                    $date = $response['payload']['fecha_termino'];
                    $evaluationEndDate = new \DateTime($date);
                } else {
                    $extraData['status'] = 'An error occured trying to get the publication date';
                    $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);
                    throw new LocalizedException(__('An error occured trying to get the evaluation date.'));
                }

                //If EvaluationEndDate is monday it must be replaced for the latest saturday
                if ($evaluationEndDate->format('N') == "1"){
                    $evaluationEndDate = $evaluationEndDate->sub(new \DateInterval('P2D'));
                    // validate if previous Friday is holiday
                    $evaluationEndDate = $this->_rfqHelper->validateEndDateFriday($evaluationEndDate);
                }

                // Validate that the evaluation start date is later than the publication end date
                if ($evaluationStartDate < $publicationEndDate) {
                    $message = __('Field evaluation start date must be later than %1 (publication end date).', $publicationEndDate->format('Y-m-d'));
                    throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
                }
            }

            // Get publication date
            $questioEndDate = null;
            if (!empty($data['make_question_term'])) {
                $makeQuestionTerm = (int)$postData['make_question_term'];

                $response = $this->helperOffer->getEndBusinessDate($token, $currentDate->format('d-m-Y'), $makeQuestionTerm);
                if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                    $date = $response['payload']['fecha_termino'];
                    $questioEndDate = new \DateTime($date);
                } else {
                    $extraData['status'] = 'An error occured trying to get the date for questions.';
                    $this->_helperActionLog->saveLoggin($actionName, $profileName, $extraData);
                    throw new LocalizedException(__('An error occured trying to get the date for questions.'));
                }
            }

            $data['subject'] = $this->fmHelper->getCategoryName($currentProduct);
            $data['product_weight'] = ($currentProduct->getWeight() != null) ? $currentProduct->getWeight() : 0;
            $data['tax_class_id'] = (int)$currentProduct->getTaxClassId();
            $data['product_id'] = $currentProduct->getId();
            $data['product_row_id'] = $currentProduct->getRowId();
            $data['unit_price'] = $currentProduct->getPrice();
            $data['bulk_order_qty'] = 1;
            $data['status'] = HelperQuoteSupplier::DRAFT;
            $data['created_at'] = $this->dateTime->gmtDate('Y-m-d H:i:s');
            $data['quoteall'] = 1;
            $data['quotecomments'] = 'NA';
            $data['quoteaddress'] = 'NA';
            $data['organization'] = $this->helperSoftware->getCustomerCompany();
            $data['publication_start'] = $currentDate->format('Y-m-d');

            if ($publicationEndDate !== null) {
                $data['publication_end'] = $publicationEndDate->format('Y-m-d');
            }
            if ($evaluationStartDate !== null) {
                $data['evaluation_start'] == $evaluationStartDate->format('Y-m-d');
            }

            if ($evaluationEndDate !== null) {
                $data['evaluation_end'] = $evaluationEndDate->format('Y-m-d');
            }
            if ($questioEndDate !== null) {
                $data['question_end'] = $questioEndDate->format('Y-m-d');
            }

            if ($data['evaluation_modality'] == 1) {
                unset($data['table_evaluation_criteria']);
            } else {
                unset($data['minimum_offer_required']);
            }

            if ($editMode) {
                $data['entity_id'] = $paramQuoteId;
            }
            $model->setData($data);
            $quoteId = $model->save()->getId();

            /** Save logs */
            $extraData['status'] = 'success';
            $extraData['request'] = 'N/A';
            $extraData['id_cotizacion'] = $quoteId;
            $extraData['response']['post_info'] = $postData;
            $extraData['response']['product'] = $data;
            $this->_helperActionLog->saveLoggin($actionName, $profileName, $extraData);
            $postData['quote_id'] = $quoteId;
            $postData['product_id'] = $data['product_id'];
            $this->messageManager->addSuccess(__('Draft for request for quote #%1 was successfully creted.', $quoteId));

            $urlReferer = $currentProduct->getProductUrl() ? $currentProduct->getProductUrl() : $this->_redirect->getRefererUrl();
            $resultRedirect->setUrl($urlReferer);
            return $resultRedirect;
        } catch (LocalizedException $e) {
            if ($e->getCode() != FormAttributesHelper::CUSTOM_ERROR_CODE) {
                $this->logger->error($e->getMessage());
            }

            // LOGS
            $extraData['status'] = $e->getMessage();
            $this->_helperActionLog->saveLoggin($actionName, $profileName, $extraData);

            $this->messageManager->addError($e->getMessage());
            $this->getDataPersistor()->set($keyDataPersistor, $postData);
            $this->_redirect('requestforquote/index/index/product_id/' . $productId);
            return;
        } catch (\Exception $e) {
            if ($e->getCode() != FormAttributesHelper::CUSTOM_ERROR_CODE) {
                $this->logger->error($e->getMessage());
            }

            // LOGS
            $extraData['status'] = $e->getMessage();
            $this->_helperActionLog->saveLoggin($actionName, $profileName, $extraData);

            $this->messageManager->addError($e->getMessage());
            $this->getDataPersistor()->set($keyDataPersistor, $postData);
            $this->_redirect('requestforquote/index/index/product_id/' . $productId);
            return;
        }
    }

    /**
    * Get system value
    *
    *@param string $field
    *@return string
    */
    private function getSystemValue($field)
    {
        return $this->scopeConfig->getValue(
            'requestforquote/requestforquote_settings/' . $field,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get Data Persistor
     *
     * @return Magento\Framework\App\Request\DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }
}
