<?php

namespace Formax\QuotesSupplier\Controller\Account;

use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use Webkul\Requestforquote\Model\InfoFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Formax\ActionLogs\Helper\Data as HelperActionLog;

/**
 * class Evaluateperformance
 */
class Evaluateperformance extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var HelperActionLog
     */
    protected $helperActionLog;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var InfoFactory
     */
    protected $info;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param InfoFactory $info
     * @param HelperActionLog $helperActionLog
     * @param ProductFactory $productFactory
     * @param StoreManagerInterface $storeManager
     * @param DateTime $date
     */
    public function __construct(
           Context $context,
           JsonFactory $resultJsonFactory,
           InfoFactory $info,
           HelperActionLog $helperActionLog,
           ProductFactory $productFactory,
           StoreManagerInterface $storeManager,
           DateTime $date
    ) {
        $this->date = $date;
        $this->info = $info;
        $this->storeManager = $storeManager;
        $this->productFactory = $productFactory;
        $this->helperActionLog = $helperActionLog;
        $this->resultJsonFactory = $resultJsonFactory;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        try {
            $post = $this->getRequest()->getPostValue();
            $result = $this->resultJsonFactory->create();
            $product = $this->productFactory->create()->load($post['productId']);
            $extraData = [
                'action' => 'Aprobación Corización',
                'status' => 'success'
            ];
            $actionName = 'Evaluación de desempeño';
            $profileName = 'Cliente';
            $product->addAttributeUpdate('swqa_date', $this->date->gmtDate(), $this->storeManager->getStore()->getId());

            foreach ($post as $attrcode => $attrvalue) {
                if (strpos($attrcode, 'swqa_') !== false) {
                    $product->addAttributeUpdate($attrcode, $attrvalue, $this->storeManager->getStore()->getId());
                }
            }

            $sellerQuote = $this->info->create()->load($post['quoteId']);
            $tmpSellerQuote = $sellerQuote->getData();
            $extraData['request'] = $tmpSellerQuote;
            $extraData['id_cotizacion'] = $post['quoteId'];
            $extraData['response']['post_info'] = $post;
            $extraData['response']['data'] = $sellerQuote->getData();
            $this->helperActionLog->saveLoggin($actionName, $profileName, $extraData);
            $result->setData(['success' => true]);
            return $result;
        } catch (Exception $e) {
            $result->setData(['success' => false, 'error' => $e->getMessage()]);
            return $result;
        }
    }
}
