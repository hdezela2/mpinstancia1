<?php

namespace Formax\QuotesSupplier\Controller\Account;

use Magento\Framework\App\Action\Context;
use Formax\ActionLogs\Helper\Data as HelperActionLog;
use Magento\Framework\App\ResourceConnection;

class Updateteam extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $_productRepository;
    protected $_helperActionLog;
    protected $info;
    protected $_resource;
    private \Magento\Catalog\Model\ProductFactory $productFactory;
    private \Magento\Store\Model\StoreManagerInterface $storeManage;
    private \Magento\Framework\Stdlib\DateTime\DateTime $date;

    public function __construct(
           Context  $context,
           \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
           \Webkul\Requestforquote\Model\InfoFactory $info,
           \Magento\Catalog\Model\ProductRepository $productRepository,
           HelperActionLog $helperActionLog,
           \Magento\Catalog\Model\ProductFactory $productFactory,
           \Magento\Store\Model\StoreManagerInterface $storeManage,
           ResourceConnection $resource,
           \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {

        $this->resultJsonFactory = $resultJsonFactory;
        $this->_productRepository = $productRepository;
        $this->info = $info;
        $this->_helperActionLog     = $helperActionLog;
        $this->productFactory = $productFactory;
        $this->storeManage = $storeManage;
        $this->date = $date;
        $this->_resource = $resource;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if(isset($post['isTeam']) && $post['isTeam']){
            $table="xpec_teamjob_loan";
        }
        if(isset($post['isService']) && $post['isService']){
            $table="xpec_services";
        }

        if(isset($table)) {
            $resourceConnection = $this->_resource->getConnection();
            $data = ['active' => $post['active']];
            $where = ['id = ?' => (int)$post['id']];
            $tableName = $resourceConnection->getTableName($table);
            $resourceConnection->update($tableName, $data, $where);
        }
        die();
    }
}
