<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Requestforquote
 * @author    Webkul Software protected Limited
 * @copyright Copyright (c) Webkul Software protected Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Formax\QuotesSupplier\Controller\Account;

use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Webkul\Requestforquote\Controller\AbstractQuotes;
use Formax\ActionLogs\Model\ActionLogsFactory;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Marketplace\Controller\Product\SaveProduct;
use Magento\Framework\Exception\LocalizedException;
use Formax\ActionLogs\Helper\Data as HelperActionLog;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;

/**
 * SendMessageToSeller class
 */
class SendMessageToSeller extends \Webkul\Requestforquote\Controller\Account\SendMessageToSeller
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Webkul\Requestforquote\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $mpHelper;

    /**
     * @var \Webkul\Requestforquote\Model\ConversationFactory
     */
    protected $conversation;

    /**
     * @var \Webkul\Requestforquote\Model\InfoFactory
     */
    protected $info;

    /**
     * @var \Webkul\Requestforquote\Model\QuoteFactory
     */
    protected $quote;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customer;


    protected $_storeManager;
    protected $directory_list;
    protected $productFactory;
    protected $urlRewrite;
    protected $saveProduct;

    protected $_helperActionLog;

    protected $_resource;

    /**
     * __construct
     *
     * @param Context                                           $context
     * @param PageFactory                                       $resultPageFactory
     * @param \Magento\Customer\Model\Session                   $customerSession
     * @param \Webkul\Requestforquote\Helper\Data               $dataHelper
     * @param \Webkul\Marketplace\Helper\Data                   $mpHelper
     * @param \Webkul\Requestforquote\Model\ConversationFactory $conversation
     * @param \Webkul\Requestforquote\Model\InfoFactory         $info
     * @param \Webkul\Requestforquote\Model\QuoteFactory        $quote
     * @param \Magento\Customer\Model\CustomerFactory           $customer
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Webkul\Requestforquote\Helper\Data $dataHelper,
        \Webkul\Requestforquote\Model\ConversationFactory $conversation,
        \Webkul\Requestforquote\Model\InfoFactory $info,
        \Webkul\Requestforquote\Model\QuoteFactory $quote,
        \Magento\Customer\Model\CustomerFactory $customer,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Magento\Customer\Model\Session $customerSession,
        HelperActionLog $helperActionLog,
        StoreManagerInterface $storeManager,
        \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory $urlRewrite,
        \Magento\Framework\App\ResourceConnection $resource,
        SaveProduct $saveProduct
    ) {

        $this->resultPageFactory    = $resultPageFactory;
        $this->customerSession      = $customerSession;
        $this->dataHelper           = $dataHelper;
        $this->mpHelper             = $mpHelper;
        $this->conversation         = $conversation;
        $this->info                 = $info;
        $this->quote                = $quote;
        $this->customer             = $customer;
        $this->directory_list       = $directory_list;
        $this->productFactory       = $productFactory;
        $this->urlRewrite           = $urlRewrite;
        $this->saveProduct          = $saveProduct;
        $this->_storeManager        = $storeManager;
        $this->_helperActionLog     = $helperActionLog;
        $this->_resource = $resource;

        parent::__construct(
            $context,
            $resultPageFactory,
            $customerSession,
            $dataHelper,
            $mpHelper,
            $conversation,
            $info,
            $quote,
            $customer
        );
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute(){
        if($this->getStoreName() == 'convenio_storeview_software' || $this->getStoreName() != 'Software' || $this->getStoreName() != SoftwareRenewalConstants::STORE_NAME || $this->getStoreName() != SoftwareRenewalConstants::STORE_CODE){
            $extraData      = array(
                'action'   => 'Aprobación Corización',
                'status'   => 'success'
            );
            $actionName     ='Cotización';
            $profileName    ='Cliente';

            $resultPage = $this->resultPageFactory->create();
            $helper = $this->dataHelper;
            $postData =$this->getRequest()->getParams();
            $sellerQuoteId = $postData['seller_quote_id'];
            $id = $postData['seller_quote_id'];

            try {
                $customerId = $this->mpHelper->getCustomerId();

                if (!$this->_loadCustomerValidSellerQuote(
                    $sellerQuoteId,
                    $customerId,
                    $this->info,
                    $this->mpHelper,
                    $this->quote
                )
                ) {
                    return;
                }
                if (isset($postData['sample_images'])) {
                    $postData['sample_images'] = implode(',', $postData['sample_images']);
                }
                $postData['sender_type'] = \Webkul\Requestforquote\Model\Conversation::SENDER_TYPE_CUSTOMER;
                if (isset($postData['approve_last_quote']) && $postData['approve_last_quote'] == 'on') {
                    $postData['approved_status'] = 1;
                }
                $modelInfo = $this->conversation->create();
                $modelInfo->setData($postData);
                $tmpmodel = $modelInfo->getData();
                $modelInfo->save()->getId();

                $sellerQuote    = $this->info->create()->load($sellerQuoteId);
                $sellerId = $sellerQuote['seller_id'];
                $seller = $this->customer->create()->load($sellerId);
                $tmpSellerQuote = $sellerQuote->getData();
                if ($sellerQuote->getStatus() != 3) {
                    $sellerQuote->setStatus(1);
                    $sellerQuote->setCustomerStatus(2);
                    $sellerQuote->save();
                }
                $quoteId = $sellerQuote['quote_id'];

                $quote = $this->quote->create()->load($quoteId);
                $this->sendMessageQuote($quoteId, $sellerId, $customerId);

                if (isset($postData['approve_last_quote']) && $postData['approve_last_quote'] == 'on') {
                    $lastSellerQuoteId=1;
                    if ($this->_storeManager->getStore()->getCode() == InsumosConstants::STORE_VIEW_CODE) {
                        $lastSellerQuoteId = $this->getSellerLastQuoteId($quoteId, $sellerQuoteId);
                    }
                    $this->quote->create()->load($quoteId)
                        ->setApprovedSellerQuoteId($lastSellerQuoteId)
                        ->save();
                    $requestforquoteInfoColls = $this->info->create()->getCollection()
                        ->addFieldToFilter('quote_id', $quoteId);
                    foreach ($requestforquoteInfoColls as $key => $value) {
                        if ($value->getId() == $sellerQuoteId) {
                            $value->setStatus(3);
                            $value->setCustomerStatus(3);
                            $value->save();
                            $this->approveQuote($quoteId, $sellerQuote['seller_id'], $this->mpHelper->getCustomerId());
                        } else {
                            $value->setStatus(4);
                            $value->setCustomerStatus(4);
                            $value->save();
                        }
                    }

                    $extraData['request']                = $tmpSellerQuote;
                    $extraData['id_cotizacion']          = $sellerQuote->getData()['quote_id'];
                    $extraData['response']['post_info']  = $postData;
                    $extraData['response']['data']       = $sellerQuote->getData();
                    $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);

                    if($this->_helperActionLog->isStoreSaveLog()){
                        $this->createNewProduct($quote,$sellerQuoteId);
                    }

                }
                /* Send Quote Mail To Seller */

                $customerId = $this->mpHelper->getCustomerId();
                $customer = $this->customer->create()->load($customerId);


                if ($seller->getId()) {
                    /* send mail to seller */
                    /* Assign values for your template variables  */
                    $emailTempVariables = [];
                    $sellerName = $seller->getFirstname()." ".$seller->getLastname();
                    $sellerEmail = $seller->getEmail();
                    $receiverInfo = [
                        'name' => $sellerName,
                        'email' => $sellerEmail
                    ];
                    $customerName = $customer->getFirstname()." ".$customer->getLastname();
                    $customerEmail = $customer->getEmail();
                    $senderInfo = [
                        'name' => $customerName,
                        'email' => $customerEmail
                    ];
                    $emailTempVariables['myvar1'] = $sellerName;
                    $emailTempVariables['myvar2'] = $quote->getSubject();
                    try {
                        $template = \Webkul\Requestforquote\Helper\Data::XML_PATH_REPLY_MAIL_TO_SELLER;
                        $this->dataHelper->customMailSendMethod(
                            $emailTempVariables,
                            $senderInfo,
                            $receiverInfo,
                            $template
                        );
                    } catch (\Exception $e) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }

                $this->messageManager->addSuccess(__('Message was successfully sent.'));
                $this->_redirect('requestforquote/account/sellerquoteview', ['id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_redirect('requestforquote/account/sellerquoteview', ['id' => $id]);
                return;
            }
        }else{
            $resultPage = $this->resultPageFactory->create();
            $helper = $this->dataHelper;
            $postData =$this->getRequest()->getParams();
            $sellerQuoteId = $postData['seller_quote_id'];
            $id = $postData['seller_quote_id'];
            try {
                $customerId = $this->mpHelper->getCustomerId();

                if (!$this->_loadCustomerValidSellerQuote(
                    $sellerQuoteId,
                    $customerId,
                    $this->info,
                    $this->mpHelper,
                    $this->quote
                )
                ) {
                    return;
                }
                if (isset($postData['sample_images'])) {
                    $postData['sample_images'] = implode(',', $postData['sample_images']);
                }
                $postData['sender_type'] = \Webkul\Requestforquote\Model\Conversation::SENDER_TYPE_CUSTOMER;
                if (isset($postData['approve_last_quote']) && $postData['approve_last_quote'] == 'on') {
                    $postData['approved_status'] = 1;
                }
                $modelInfo = $this->conversation->create();
                $modelInfo->setData($postData);
                $modelInfo->save()->getId();

                $sellerQuote = $this->info->create()->load($sellerQuoteId);
                if ($sellerQuote->getStatus() != 3) {
                    $sellerQuote->setStatus(1);
                    $sellerQuote->setCustomerStatus(2);
                    $sellerQuote->save();
                }
                $quoteId = $sellerQuote['quote_id'];

                if (isset($postData['approve_last_quote']) && $postData['approve_last_quote'] == 'on') {
                    $lastSellerQuoteId = $this->getSellerLastQuoteId($quoteId, $sellerQuoteId);
                    $this->quote->create()->load($quoteId)
                        ->setApprovedSellerQuoteId($lastSellerQuoteId)
                        ->save();
                    $requestforquoteInfoColls = $this->info->create()->getCollection()
                        ->addFieldToFilter('quote_id', $quoteId);
                    foreach ($requestforquoteInfoColls as $key => $value) {
                        if ($value->getId() == $sellerQuoteId) {
                            $value->setStatus(3);
                            $value->setCustomerStatus(3);
                            $value->save();
                        } else {
                            $value->setStatus(4);
                            $value->setCustomerStatus(4);
                            $value->save();
                        }
                    }
                }
                /* Send Quote Mail To Seller */

                $customerId = $this->mpHelper->getCustomerId();
                $customer = $this->customer->create()->load($customerId);

                $quote = $this->quote->create()->load($quoteId);

                $sellerId = $sellerQuote['seller_id'];
                $seller = $this->customer->create()->load($sellerId);

                if ($seller->getId()) {
                    /* send mail to seller */
                    /* Assign values for your template variables  */
                    $emailTempVariables = [];
                    $sellerName = $seller->getFirstname()." ".$seller->getLastname();
                    $sellerEmail = $seller->getEmail();
                    $receiverInfo = [
                        'name' => $sellerName,
                        'email' => $sellerEmail
                    ];
                    $customerName = $customer->getFirstname()." ".$customer->getLastname();
                    $customerEmail = $customer->getEmail();
                    $senderInfo = [
                        'name' => $customerName,
                        'email' => $customerEmail
                    ];
                    $emailTempVariables['myvar1'] = $sellerName;
                    $emailTempVariables['myvar2'] = $quote->getSubject();
                    try {
                        $template = \Webkul\Requestforquote\Helper\Data::XML_PATH_REPLY_MAIL_TO_SELLER;
                        $this->dataHelper->customMailSendMethod(
                            $emailTempVariables,
                            $senderInfo,
                            $receiverInfo,
                            $template
                        );
                    } catch (\Exception $e) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
                $this->messageManager->addSuccess(__('Message was successfully sent.'));
                $this->_redirect('requestforquote/account/sellerquoteview', ['id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_redirect('requestforquote/account/sellerquoteview', ['id' => $id]);
                return;
            }
        }


    }

    /**
     * getSellerLastQuoteId get the seller last quoted Id
     *
     * @param  [int] $quoteId
     * @param  [int] $sellerQuoteId
     * @return int
     */
    public function getSellerLastQuoteId($quoteId, $sellerQuoteId)
    {
        $lastSellerQuoteId = 0;
        $requestforquoteInfoColls = $this->conversation->create()->getCollection()
            ->addFieldToFilter('sender_type', 1)
            ->addFieldToFilter('seller_quote_id', $sellerQuoteId)
            ->setOrder('entity_id', 'DESC')
            ->setPageSize(1)
            ->setCurPage(1);
        foreach ($requestforquoteInfoColls as $key => $value) {
            $lastSellerQuoteId = $value->getId();
        }
        return $lastSellerQuoteId;
    }


    public function sendMessageQuote($quoteId, $sellerId, $customerId){
        return true;
    }
    public function approveQuote($quoteId, $sellerId, $customerId){
        return true;
    }
    protected function getStoreName(){
        return trim($this->_storeManager->getStore()->getName());
    }

    protected function getUrlCreateProduct(){
        return $this->_storeManager->getStore()->getUrl('requestforquote/seller/createnewproduct');
    }

    /**
     * Create new product
     *
     * @param \Webkul\Requestforquote\Model\Quote $quote
     * @param int $$sellerQuoteId
     * @return void
     */
    protected function createNewProduct($quote, $sellerQuoteId, $priceWithDiscount = false)
    {
        $post = [
            'productId' => $quote->getProductId(),
            'quoteId' => $quote->getEntityId(),
            'quantity' => $quote->getBulkOrderQty(),
            'taxClassId' => $quote->getTaxClassId(),
            'quoteInfoId' => $sellerQuoteId,
            'price' => $priceWithDiscount !== false ? $priceWithDiscount : $quote->getAmount()
        ];

        if ($post['productId']) {
            $sellerProductColls = $this->mpHelper->getSellerProductDataByProductId($post['productId']);
        }
        $imagePath = '';
        $sampleImages = [];

        if ($quote['sample_images']) {
            $sampleImages =  explode(',', $quote['sample_images']);
        }
        if (!empty($sampleImages)) {
            foreach ($sampleImages as $sampleImage) {
                $ext = pathinfo($sampleImage, PATHINFO_EXTENSION);
                if ($ext != 'pdf' && $ext != 'doc') {
                    $imagePath = $this->directory_list->getPath('media') . '/images/requestforquote/' . $sampleImage;
                    break;
                }
            }

        }

        try {
            $product = [];
            $typeProduct = '';
            $originalProduct = $this->productFactory->create()->load($post['productId']);
            $originalProductSku = $originalProduct->getSku();
            $urlKey = $quote->getEntityId()."-".$originalProductSku;
            $createdUrl = $this->createUrl($urlKey, $urlKey, 0);

            switch ($this->getStoreName()) {
                case 'convenio_storeview_software':
                case 'Software':
                case SoftwareRenewalConstants::STORE_NAME:
                case SoftwareRenewalConstants::STORE_CODE:
                    $typeProduct = 'virtual';
                    $product['voucher_product_type'] = 2; /** Discount */
                break;
                default:
                    $typeProduct = \Webkul\Requestforquote\Model\Product\Type\Quote::TYPE_ID;
                break;
            }

            $priceOffert = $this->getPriceProductOffert($sellerQuoteId);

            $product['name'] = $originalProduct->getName();
            $product['sku'] = $quote->getEntityId() . '-' . $originalProductSku;
            $product['price'] = $priceOffert;
            $product['url_key'] = $createdUrl;
            $product['stock_data'] = [
                'manage_stock' => 1,
                'use_config_manage_stock' => 1,
                'min_sale_qty' => $post['quantity'],
                'max_sale_qty' => $post['quantity']
            ];
            $product['quantity_and_stock_status'] = ['qty' => $post['quantity'], 'is_in_stock' => 1];
            $product['visibility'] = 1;
            $product['tax_class_id'] = $post['taxClassId'];
            $product['product_has_weight'] = ($originalProduct->getWeight() > 0) ? 1 : 0;
            $product['weight'] = $originalProduct->getWeight();
            $product['category_ids'] = $originalProduct->getCategoryIds();

            $wholeData = [];
            $wholeData['type'] = $typeProduct;
            $wholeData['set'] = 4;
            $wholeData['status'] = 1;
            $wholeData['product'] = $product;
            $returnArr = $this->saveProduct->saveProductData(
                $this->mpHelper->getCustomerId(),
                $wholeData
            );

            if ($returnArr['error'] == 0) {
                $newProductId = $returnArr['product_id'];
            } else {
                throw new LocalizedException(
                    __('Product Type or Attribute Set Invalid Or Not Allowed From Marketplace Configuration')
                );
            }

            $productModel= $this->productFactory->create()->load($newProductId);
            if ($imagePath != '') {
                $productModel->addImageToMediaGallery($imagePath, ['image', 'small_image', 'thumbnail'], false, true);
                $productModel->save();
            }

            $infoModel = $this->info->create()->load($post['quoteInfoId']);
            $infoModel->setProductCreated(1);
            $infoModel->save();

            $quoteModel = $this->quote->create()->load($post['quoteId']);
            $quoteModel->setProductId($newProductId);
            $quoteModel->save();
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
    }

    /**
     * Create URL product
     *
     * @param string $url
     * @param string $mainUrl
     * @param int $i
     * @return string
     */
    public function createUrl($url, $mainUrl, $i = 0)
    {
        $urlRewriteCollection = $this->urlRewrite->create();
        $urlRewriteCollection->addFieldToFilter('entity_type', ['eq' => 'product'])
            ->addFieldToFilter('store_id', ['eq' => 1])
            ->addFieldToFilter('request_path', ['eq' => $url . '.html']);
        if ($urlRewriteCollection->getSize() == 0) {
            return $url;
        }

        $url = $mainUrl;
        $i++;
        $url = $url . '-' . $i;
        $url = $this->createUrl($url, $mainUrl, $i);

        return $url;
    }
    public function getPriceProductOffert($sellerQuoteId){
        $con = $this->_resource->getConnection();
        $sql = "SELECT
                        offer_price
                    FROM
                        requestforquote_quote_conversation
                    WHERE
                        seller_quote_id=".$sellerQuoteId."
                        AND offer_price IS NOT NULL
                    ORDER BY
                        entity_id DESC";
        $priceOffert = $con->fetchOne($sql);
        return $priceOffert;
    }
}
