<?php

namespace Formax\QuotesSupplier\Controller\Account;

use Cleverit\Requestforquote\Helper\Data as RfqHelper;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Catalog\Model\ProductRepository;
use Webkul\Requestforquote\Model\InfoFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Store\Model\ScopeInterface;
use Webkul\Marketplace\Model\SellerFactory;
use Magento\Customer\Controller\AbstractAccount;
use Psr\Log\LoggerInterface;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\Requestforquote\Helper\Data as RequestforquoteHelper;
use Formax\CreateFormAttributes\Helper\Data as FormAttributesHelper;
use Formax\Offer\Helper\Data as HelperOffer;
use Formax\ActionLogs\Helper\Data as HelperActionLog;
use Formax\QuotesSupplier\Helper\Data as HelperQuoteSupplier;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Magento\Customer\Model\Session;
use Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory as RfqInfoCollectionFactory;

class Save extends AbstractAccount
{
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var \Webkul\Requestforquote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Webkul\Requestforquote\Model\InfoFactory
     */
    protected $infoFactory;

    /**
     * @var \Webkul\Requestforquote\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Webkul\Marketplace\Model\SellerFactory
     */
    protected $userProfile;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customer;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $mpHelper;

    /**
     * @var \Formax\CreateFormAttributes\Helper\Data
     */
    protected $fmHelper;

    /**
     * @var \Formax\Offer\Helper\Data
     */
    protected $helperOffer;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var  \Formax\ActionLogs\Helper\Data
     */
    private $_helperActionLog;

    /**
     * @var \Formax\ConfigCmSoftware\Helper\Data
     */
    protected $helperSoftware;

    /** @var Magento\Framework\Stdlib\DateTime\TimezoneInterface */
    protected $_timezoneInterface;

    /** @var RfqHelper */
    private $_rfqHelper;

    /**
     * @var Session
     */
    private $customerSession;
    private ProductRepository $productRepository;

    protected RfqInfoCollectionFactory $rfqInfoCollection;

    public function __construct(
        Context $context,
        ResultFactory $resultFactory,
        LoggerInterface $logger,
        QuoteFactory $quoteFactory,
        ProductRepository $productRepository,
        InfoFactory $infoFactory,
        RfqInfoCollectionFactory $rfqInfoCollection,
        CustomerFactory $customerFactory,
        DateTime $dateTime,
        ScopeConfigInterface $scopeConfig,
        RequestforquoteHelper $dataHelper,
        SellerFactory $userProfile,
        MarketplaceHelper $mpHelper,
        FormAttributesHelper $fmHelper,
        HelperOffer $helperOffer,
        HelperActionLog $helperActionLog,
        HelperSoftware $helperSoftware,
        TimezoneInterface $timezoneInterface,
        RfqHelper $rfqHelper,
        Session $customerSession
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->productRepository = $productRepository;
        $this->userProfile = $userProfile;
        $this->infoFactory = $infoFactory;
        $this->dataHelper = $dataHelper;
        $this->customer = $customerFactory;
        $this->dateTime = $dateTime;
        $this->mpHelper = $mpHelper;
        $this->fmHelper = $fmHelper;
        $this->helperOffer = $helperOffer;
        $this->scopeConfig = $scopeConfig;
        $this->helperSoftware = $helperSoftware;
        $this->logger = $logger;
        $this->_helperActionLog = $helperActionLog;
        $this->_timezoneInterface = $timezoneInterface;
        parent::__construct($context);
        $this->_rfqHelper = $rfqHelper;
        $this->customerSession = $customerSession;
        $this->rfqInfoCollection = $rfqInfoCollection;
    }

    public function execute()
    {
        $editMode = false;
        $postData = $this->getRequest()->getPostValue();
        $paramQuoteId = $this->getRequest()->getParam('quote_id', 0);

        // save draft
        if (isset($postData['draft']) && $postData['draft'] == 'draft') {
            $this->_forward('savedraft');
            return;
        }

        if ($paramQuoteId > 0) {
            $editMode = true;
            $model = $this->quoteFactory->create()->load($paramQuoteId);
            $productId = $model->getProductId();
        } else {
            $productId = $this->getRequest()->getParam('product_id', 0);
            $model = $this->quoteFactory->create();
        }

        $filesData = $this->getRequest()->getFiles();
        $entity = 'requestforquote_quote';
        $currentProduct = $this->productRepository->getById($productId);
        $data = $this->fmHelper->parsePostData($entity, $postData, $filesData);
        $currentDate = $this->_timezoneInterface->date()->format('d-m-Y');
        $publicationDaysConfig = (int)$this->getSystemValue('requestforquote_publication_end_days');
        $evaluationDaysConfig = (int)$this->getSystemValue('requestforquote_evaluation_end_days');
        $questionDaysConfig = (int)$this->getSystemValue('requestforquote_questions_days');
        $keyDataPersistor = 'formattributes_' . $entity;

        // LOGS
        $actionName = 'Cotización';
        $profileName = 'Cliente';
        $extraData = [
            'action' => 'Nueva Cotización',
            'status' => ''
        ];
        try {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

            if (!$this->dataHelper->checkForSellerExistence()
                && !$this->dataHelper->getApprovalRequiredStatus()
            ) {

                $extraData['status'] = 'Cannot request for quote. No seller associated with the current site';
                $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);

                throw new LocalizedException(__('Cannot request for quote. No seller associated with the current site'));
            }

            if (isset($data['error']) && isset($data['msg']) && $data['error']) {
                throw new \Exception($data['msg'], FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

            $validateData = $this->fmHelper->validatePostData($entity, $postData);
            if (isset($validateData['error']) && $validateData['error']) {
                $message = isset($validateData['msg']) ? $validateData['msg'] : __('An error occured trying to save request for quote.');
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

            /**
             * Se comentan las líneas a continuación debido a que se desconoce el por qué de la doble validación
             * del campo amount. A través del helper fmHelper->validatePostData se considera la validación
             * del campo amount.
             */

//            $iniUtmQty = $this->helperSoftware->getIniQtyUtm();
//            $endUtmQty = $this->helperSoftware->getEndQtyUtm();
//            $utmQty = $this->helperSoftware->getUtmQtyByAmount($data['amount']);
//            if ($utmQty < $iniUtmQty || $utmQty > $endUtmQty) {
//                $message =  __(
//                    'Amount value %1 do not is between %2 and %3.',
//                    $this->helperSoftware->currencyFormat($data['amount']),
//                    $iniUtmQty,
//                    $endUtmQty
//                );
//                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
//            }

            $customerId = $this->mpHelper->getCustomerId();
            $data['customer_id'] = $customerId;
            $customer = $this->customer->create()->load($customerId);
            $token = $customer->getData('user_rest_atk') ? $customer->getData('user_rest_atk') : '';

            $data['destination_country'] = 'CL';

            if (!$this->dataHelper->getApprovalRequiredStatus()) {
                $data['status'] = 1;
            } else {
                $data['status'] = 0;
            }
            if (!$this->dataHelper->checkForSellerExistence() && $this->dataHelper->getApprovalRequiredStatus()) {
                $data['status'] = 0;
            }

            $publicationTerm = (int)$postData['publication_term'];
            $evaluationTerm = (int)$postData['evaluation_term'];
            $makeQuestionTerm = (int)$postData['make_question_term'];

            //Publication start day must begin at next workable day from today
            $response = $this->helperOffer->getEndBusinessDate($token, $currentDate, 1);
            if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                $date = $response['payload']['fecha_termino'];
                $currentDate = new \DateTime($date);
            } else {
                $extraData['status'] = 'An error occured trying to get the publication date';
                $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);
                throw new LocalizedException(__('An error occured trying to get the next workable date.'));
            }

            // Get publication end date
            $response = $this->helperOffer->getEndBusinessDate($token, $currentDate->format('d-m-Y'), $publicationTerm);
            if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                $date = $response['payload']['fecha_termino'];
                $publicationEndDate = new \DateTime($date);
            } else {
                $extraData['status'] = 'An error occured trying to get the publication date';
                $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);
                throw new LocalizedException(__('An error occured trying to get the publication date.'));
            }

            //If PublicationEndDate is monday it must be replaced for the latest saturday
            if ($publicationEndDate->format('N') == "1") {
                $publicationEndDate = $publicationEndDate->sub(new \DateInterval('P2D'));
                // validate if previous Friday is holiday
                $publicationEndDate = $this->_rfqHelper->validateEndDateFriday($publicationEndDate);
            }

            // Get evaluation start and end dates
            $evaluationStartDate = new \DateTime($postData['evaluation_start']);
            // We add 1 day because the evaluation starts the next business day that was selected and closes at 00.00 of the following day
            $response = $this->helperOffer->getEndBusinessDate($token, $evaluationStartDate->format('d-m-Y'), $evaluationTerm + 1);
            if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                $date = $response['payload']['fecha_termino'];
                $evaluationEndDate = new \DateTime($date);
            } else {
                $extraData['status'] = 'An error occured trying to get the publication date';
                $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);
                throw new LocalizedException(__('An error occured trying to get the evaluation date.'));
            }

            //If EvaluationEndDate is monday it must be replaced for the latest saturday
            if ($evaluationEndDate->format('N') == "1"){
                $evaluationEndDate = $evaluationEndDate->sub(new \DateInterval('P2D'));
                // validate if previous Friday is holiday
                $evaluationEndDate = $this->_rfqHelper->validateEndDateFriday($evaluationEndDate);
            }

            // Validate that the evaluation start date is later than the publication end date
            if ($evaluationStartDate < $publicationEndDate) {
                $message = __('Field evaluation start date must be later than %1 (publication end date).', $publicationEndDate->format('Y-m-d'));
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

            // Get make questions date
            $response = $this->helperOffer->getEndBusinessDate($token, $currentDate->format('d-m-Y'), $makeQuestionTerm);
            if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                $date = $response['payload']['fecha_termino'];
                $questioEndDate = new \DateTime($date);
            } else {
                $extraData['status'] = 'An error occured trying to get the date for questions.';
                $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);
                throw new LocalizedException(__('An error occured trying to get the date for questions.'));
            }

            $data['subject'] = $this->fmHelper->getCategoryName($currentProduct);
            $data['product_weight'] = ($currentProduct->getWeight() != null) ? $currentProduct->getWeight() : 0;
            $data['tax_class_id'] = (int)$currentProduct->getTaxClassId();
            $data['product_id'] = $currentProduct->getId();
            $data['product_row_id'] = $currentProduct->getRowId();
            $data['unit_price'] = $currentProduct->getPrice();
            $data['bulk_order_qty'] = 1;
            $data['status'] = HelperQuoteSupplier::PUBLISHED;
            $data['created_at'] = $this->dateTime->gmtDate('Y-m-d H:i:s');
            $data['quoteall'] = 1;
            $data['quotecomments'] = 'NA';
            $data['quoteaddress'] = 'NA';
            if (!empty($this->customerSession->getCustomerOrganizationName())) {
                $data['organization'] = $this->customerSession->getCustomerOrganizationName();
            } else {
                $data['organization'] = $this->helperSoftware->getCustomerCompany();
            }
         //   $data['publication_start'] = $currentDate->format('Y-m-d');
            $publication_start = new \DateTime();
            $data['publication_start'] = $publication_start->format('Y-m-d');
            $data['publication_end'] = $publicationEndDate->format('Y-m-d');
            $data['evaluation_start'] = $evaluationStartDate->format('Y-m-d');
            $data['evaluation_end'] = $evaluationEndDate->format('Y-m-d');
            $data['question_end'] = $questioEndDate->format('Y-m-d');

            if ($data['evaluation_modality'] == 1) {
                unset($data['table_evaluation_criteria']);
            } else {
                unset($data['minimum_offer_required']);
            }

            if ($editMode) {
                $data['entity_id'] = $paramQuoteId;
            }
            $model->setData($data);
            $quoteId = $model->save()->getId();

            if (!$this->dataHelper->getApprovalRequiredStatus()) {
                $userProfile = $this->userProfile->create();
                $collection = $userProfile->getCollection();
                // DSW2-122 - removed filter is_seller = 1 so we send quote info to all sellers that has an offer on this product
                $collection->addFieldToSelect('seller_id')
                    ->distinct(true)
                    ->getSelect()->join(
                        ['assign_item' => $collection->getResource()->getTable('marketplace_assignproduct_items')],
                        'main_table.seller_id = assign_item.seller_id AND assign_item.product_id = ' . $productId,
                        []
                    );
                foreach ($collection as $key => $value) {
                    /* save quote info for seller */
                    $modelInfo = $this->infoFactory->create();
                    $modelInfo->setSellerId($value['seller_id']);
                    $modelInfo->setQuoteId($quoteId);
                    $modelInfo->setBulkQuoteQty(1);
                    $modelInfo->setStatus(0);
                    $modelInfo->setCustomerStatus(2);
                    $modelInfo->setCreatedAt($data['created_at']);
                    $modelInfo->setUpdatedAt($data['created_at']);
                    /* save or update quote infor for seller (avoid duplicates) */
                    $infoExists = $this->rfqInfoCollection->create()
                        ->addFieldToFilter('seller_id', ['eq' => $value['seller_id']])
                        ->addFieldToFilter('quote_id', ['eq' => $quoteId])
                        ->getFirstItem();

                    if ($infoExists->getData()) {
                        $infoId = $infoExists->getId();
                        $modelInfo = $this->infoFactory->create()->load($infoId);
                        $modelInfo->setUpdatedAt($data['created_at']);
                    } else {
                        $modelInfo = $this->infoFactory->create();
                        $modelInfo->setSellerId($value['seller_id']);
                        $modelInfo->setQuoteId($quoteId);
                        $modelInfo->setBulkQuoteQty(1);
                        $modelInfo->setStatus(0);
                        $modelInfo->setCustomerStatus(2);
                        $modelInfo->setCreatedAt($data['created_at']);
                        $modelInfo->setUpdatedAt($data['created_at']);
                    }
                    $modelInfo->save();
                }
            }

            /** Save logs */
            $extraData['status'] = 'success';
            $extraData['request'] = 'N/A';
            $extraData['id_cotizacion'] = $quoteId;
            $extraData['response']['data'] = $modelInfo->getData();
            $extraData['response']['post_info'] = $postData;
            $extraData['response']['product'] = $data;
            $this->_helperActionLog->saveLoggin($actionName, $profileName, $extraData);
            $postData['quote_id'] = $quoteId;
            $postData['product_id'] = $data['product_id'];
            $this->saveQuote($postData);
            $this->messageManager->addSuccess(__('Request for quote #%1 was successfully sent.', $quoteId));



            $this->getDataPersistor()->clear($keyDataPersistor);
            $urlReferer = $currentProduct->getProductUrl() ? $currentProduct->getProductUrl() : $this->_redirect->getRefererUrl();
            $resultRedirect->setUrl($urlReferer);
            return $resultRedirect;
        } catch (LocalizedException $e) {
            if ($e->getCode() != FormAttributesHelper::CUSTOM_ERROR_CODE) {
                $this->logger->error($e->getMessage());
            }

            // LOGS
            $extraData['status'] = $e->getMessage();
            $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);

            $this->messageManager->addError($e->getMessage());
            $this->getDataPersistor()->set($keyDataPersistor, $postData);
            if ($paramQuoteId > 0) {
                $this->_redirect('requestforquote/index/index/quote_id/' . $paramQuoteId);
            } else {
                $this->_redirect('requestforquote/index/index/product_id/' . $productId);
            }
            return;
        } catch (\Exception $e) {
            if ($e->getCode() != FormAttributesHelper::CUSTOM_ERROR_CODE) {
                $this->logger->error($e->getMessage());
            }

            // LOGS
            $extraData['status'] = $e->getMessage();
            $this->_helperActionLog->saveLoggin($actionName,$profileName,$extraData);

            $this->messageManager->addError($e->getMessage());
            $this->getDataPersistor()->set($keyDataPersistor, $postData);
            if ($paramQuoteId > 0) {
                $this->_redirect('requestforquote/index/index/quote_id/' . $paramQuoteId);
            } else {
                $this->_redirect('requestforquote/index/index/product_id/' . $productId);
            }
            return;
        }
    }

    /**
    * Get system value
    *
    *@param string $field
    *@return string
    */
    private function getSystemValue($field)
    {
        return $this->scopeConfig->getValue(
            'requestforquote/requestforquote_settings/' . $field,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get Data Persistor
     *
     * @return Magento\Framework\App\Request\DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }

    /**
     * Save quote
     *
     * @return bool
     */
    public function saveQuote($postData)
    {
        return true;
    }
}
