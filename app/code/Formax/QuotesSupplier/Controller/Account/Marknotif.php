<?php

namespace Formax\QuotesSupplier\Controller\Account;

use Magento\Framework\App\Action\Context;
use Formax\ActionLogs\Helper\Data as HelperActionLog;

class Marknotif extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    private $resourceConnection;

    public function __construct(
           Context  $context,
           \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
           \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {

        $this->resultJsonFactory = $resultJsonFactory;
        $this->resourceConnection = $resourceConnection;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $table="requestforquote_quote_info";
        $resourceConnection = $this->resourceConnection->getConnection();
        $data = ['shownotif' => 0];
        $where = ['quote_id = ?' => (int)$post['qid']];
        $tableName = $resourceConnection->getTableName($table);
        $resourceConnection->update($tableName, $data, $where);
        die();
    }
}
