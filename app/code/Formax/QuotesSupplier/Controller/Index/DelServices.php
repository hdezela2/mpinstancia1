<?php

namespace Formax\QuotesSupplier\Controller\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Formax\QuotesSupplier\Model\TeamJobFactory;
use Formax\QuotesSupplier\Model\ServicesFactory;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

class DelServices extends \Magento\Framework\App\Action\Action{

    protected $_resultPageFactory;
    protected $_resource;
    protected $_teamJob;
    protected $_serializer;
    protected $_services;
    protected $_dir;
    protected $_fileUploaderFactory;
    protected $_mediaDirectory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        TeamJobFactory $teamJob,
        ServicesFactory $services,
        Filesystem $filesystem
    ) {
        $this->_resultPageFactory   = $resultJsonFactory;
        $this->_resource            = $resource;
        $this->_teamJob             = $teamJob;
        $this->_services            = $services;
        parent::__construct($context);
    }
    /**
     * Index action
     *
     * @return $this
     */
    public function execute(){
        $post       = $this->getRequest()->getPostValue();
        $id         = $post['id'];
        $data       = array();
        $datajson   = $this->_resultPageFactory->create();
        if(isset($post['action'])){
            switch($post['action']){
                case 'delservices':
                    $this->delServices($id);
                    $data[]=array('status'=>'ok');
                break;
                case 'delteam':
                    $this->delTeam($id);
                    $data[]=array('status'=>'ok');
                break;
            }
        }
        $this->messageManager->addSuccess(__('Registro eliminado con exito!!.'));
        return $datajson->setData(array('result'=>$data));
    }

    private function delServices($id){
        $services   = $this->_services->create();
        $services->load($id,'id');
        $services->delete();
    }
    private function delTeam($id){
        $services   = $this->_teamJob->create();
        $services->load($id,'id');
        $services->delete();
    }
}