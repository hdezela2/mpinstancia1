<?php

namespace Formax\QuotesSupplier\Controller\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Formax\QuotesSupplier\Model\TeamJobFactory;
use Formax\QuotesSupplier\Model\ServicesFactory;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

class SaveTeam extends \Magento\Framework\App\Action\Action{

    protected $_resultPageFactory;
    protected $_resource;
    protected $_teamJob;
    protected $_serializer;
    protected $_services;
    protected $_dir;
    protected $_fileUploaderFactory;
    protected $_mediaDirectory;
    protected $_helperLog;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        TeamJobFactory $teamJob,
        ServicesFactory $services,
        SerializerInterface $serializer,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Formax\ActionLogs\Helper\Data $helperLog,
        Filesystem $filesystem
    ) {
        $this->_resultPageFactory   = $resultJsonFactory;
        $this->_resource            = $resource;
        $this->_teamJob             = $teamJob;
        $this->_serializer          = $serializer;
        $this->_services            = $services;
        $this->_helperLog           = $helperLog;
        //$this->_dir                 = $dir;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_mediaDirectory      = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        parent::__construct($context);
    }
    /**
     * Index action
     *
     * @return $this
     */
    public function execute(){
        $post       = $this->getRequest()->getPostValue();
        $files      = $this->getRequest()->getFiles();
        $status     = 'undefined';
        $quoteId    = $post['quote_id'];
        $datajson   = $this->_resultPageFactory->create();
        $con        = $this->_resource->getConnection();
        $dataLog    = array(
            'equipo_trabajo'    => array(),
            'servicio'          => array()
        );
        $dataLogan  = array(
            'equipo_trabajo'    => array(),
            'servicio'          => array()
        );
        $extraDataloag  = array(
            'action' => 'Equipo de Trabajo',
            'status' => ''
        );
        $actionName     ='Cotización';
        $profileName    ='Proveedor';
        try{
            $con->beginTransaction();
            $extraData      = array(
                'key'=>'value'
            );
            $trol           = $post['rol_teamwork'];
            $additionalData = $this->_serializer->serialize($extraData);
            //$this->cleanData($quoteId,$con);
            if(is_array($trol)){
                foreach($trol as $index => $value){
                    $rol    = isset($post['rol_teamwork'][$index])?$post['rol_teamwork'][$index]:'';
                    $name   = isset($post['name_teamwork'][$index])?$post['name_teamwork'][$index]:'';
                    $dur    = isset($post['duracion_teamwork'][$index])?$post['duracion_teamwork'][$index]:'';
                    $rut    = isset($post['rut_teamwork'][$index])?$post['rut_teamwork'][$index]:'';
                    /*if(empty($rol)){
                        throw new \Exception("El campo 'rol en proyecto' no puede estar vacio");
                    }
                    if(empty($name)){
                        throw new \Exception("El campo 'nombre completo' no puede estar vacio");
                    }
                    if(empty($dur)){
                        throw new \Exception("El campo 'horas totales' no puede estar vacio");
                    }*/
                    $teamJob        = $this->_teamJob->create();
                    if(isset($post['idproyecto'][$index])){
                        $teamJob->load($post['idproyecto'][$index],'id');
                        $dataLogan['equipo_trabajo'][]=$teamJob->getData();
                    }
                    $teamJob->setQuoteId($quoteId);
                    $teamJob->setRol($rol);
                    $teamJob->setName($name);
                    $teamJob->setHours($dur);
                    $teamJob->setRut($rut);
                    $urlfilecv = '';
                    if(isset($files['cv'][$index]) && !empty($files['cv'][$index])){
                        $uploadfilecv = $this->uploadcv( $files['cv'],$index );
                        $urlfilecv = $uploadfilecv['file'];
                    }
                    if(!empty($urlfilecv)){
                        $extraDatacv['cv'] = $urlfilecv;
                        $additionalData = $this->_serializer->serialize($extraDatacv);
                        $teamJob->setAdditionalData($additionalData);
                    }
                    $teamJob->save();
                    $dataLog['equipo_trabajo'][]=$teamJob->getData();
                }
            }else{
                $rol    = isset($post['rol_teamwork'])?$post['rol_teamwork']:'';
                $name   = isset($post['name_teamwork'])?$post['name_teamwork']:'';
                $dur    = isset($post['duracion_teamwork'])?$post['duracion_teamwork']:'';
                $rut    = isset($post['rut_teamwork'])?$post['rut_teamwork']:'';
                /*if(empty($rol)){
                    throw new \Exception("El campo 'rol en proyecto' no puede estar vacio");
                }
                if(empty($name)){
                    throw new \Exception("El campo 'nombre completo' no puede estar vacio");
                }
                if(empty($name)){
                    throw new \Exception("El campo 'horas totales' no puede estar vacio");
                }*/
                $teamJob        = $this->_teamJob->create();
                $teamJob->setQuoteId($quoteId);
                $teamJob->setRol($rol);
                $teamJob->setName($name);
                $teamJob->setHours($dur);
                $teamJob->setRut($rut);
                $teamJob->save();
            }

            $tname           = $post['name_services'];
            if(is_array($tname)){
                foreach($tname as $index => $value){
                    $name       = isset($post['name_services'][$index])?$post['name_services'][$index]:'';
                    $det        = isset($post['detail_services'][$index])?$post['detail_services'][$index]:'';
                    $del        = isset($post['deliver_date'][$index])?$post['deliver_date'][$index]:'';
                    /*if(empty($name)){
                        throw new \Exception("El campo 'nombre' no puede estar vacio");
                    }
                    if(empty($det)){
                        throw new \Exception("El campo 'detalle' no puede estar vacio");
                    }
                    if(empty($del)){
                        throw new \Exception("El campo 'fecha de entrega' no puede estar vacio");
                    }*/

                    $urlfile = '';
                    if(isset($files['informe'][$index]) && !empty($files['informe'][$index])){
                        $uploadfile = $this->upload( $files['informe'],$index );
                        $urlfile = $uploadfile['file'];
                    }

                    $services   = $this->_services->create();
                    if(isset($post['idservices'][$index])){
                        $services->load($post['idservices'][$index],'id');
                        $dataLogan['servicio'][]=$services->getData();
                    }
                    $services->setQuoteId($quoteId);
                    $services->setName($name);
                    $services->setDetails($det);
                    $services->setDeliverDate($del);
                    
                    if(!empty($urlfile)){
                        $extraData['informe'] = $urlfile;
                        $additionalData = $this->_serializer->serialize($extraData);
                        $services->setAdditionalData($additionalData);
                    }
                    
                    $services->save();
                    $dataLog['servicio'][]=$services->getData();
                }
            }else{
                $name       = isset($post['name_services'])?$post['name_services']:'';
                $det        = isset($post['detail_services'])?$post['detail_services']:'';
                $del        = isset($post['deliver_date'])?$post['deliver_date']:'';
                /*if(empty($name)){
                    throw new \Exception("El campo 'nombre' no puede estar vacio");
                }
                if(empty($det)){
                    throw new \Exception("El campo 'detalle' no puede estar vacio");
                }
                if(empty($del)){
                    throw new \Exception("El campo 'fecha de entrega' no puede estar vacio");
                }*/

                $services   = $this->_services->create();
                $services->setQuoteId($quoteId);
                $services->setName($name);
                $services->setDetails($det);
                $services->setDeliverDate($del);
                $services->setAdditionalData($additionalData);

                $services->save();
            }


            if((isset($dataLogan['equipo_trabajo']) && is_array($dataLogan['equipo_trabajo']) && count($dataLogan['equipo_trabajo'])>0) || (isset($dataLogan['servicio']) && is_array($dataLogan['servicio']) && count($dataLogan['servicio'])>0)){
                $extraDataloag['request']['equipo']     = $dataLogan['equipo_trabajo'];
                $extraDataloag['request']['servicio']   = $dataLogan['servicio'];
            }else{
                $extraDataloag['request'] = 'N/A';
            }
            $extraDataloag['response']['equipo']    = $dataLog['equipo_trabajo'];
            $extraDataloag['response']['servicio']  = $dataLog['servicio'];
            $this->_helperLog->saveLoggin($actionName,$profileName,$extraDataloag);

            $con->commit();
            $this->messageManager->addSuccess(__('Datos grabados con éxito.'));
            $status = 'succes';
        }catch(\Exception $err){
            $con->rollBack();
            $this->messageManager->addError($err->getMessage());
            $status = 'error';
        }
        $data[]=array('status'=>$status);
        return $datajson->setData(array('result'=>$data));
    }
    private function cleanData($quoteId,$con)
    {
        if($quoteId>0){
            try{
                $sql = "DELETE FROM xpec_teamjob_loan WHERE quote_id=".$quoteId;
                $con->query($sql);

                $sql = "DELETE FROM xpec_services WHERE quote_id=".$quoteId;
                $con->query($sql);
            }catch(\Exception $e){
                throw new \Exception($e->getMessage());
            }
        }else{
            throw new \Exception("No se pudo eliminar data.");
        }
    }
    public function upload($files,$index){
        try{
            $target = $this->_mediaDirectory->getAbsolutePath('informeteam/files/');
            $uploader = $this->_fileUploaderFactory->create(['fileId' => 'informe['.$index.']']);
            $uploader->setAllowedExtensions(['jpeg', 'jpg', 'png', 'gif', 'bmp' , 'pdf' , 'doc' , 'docx', 'ppt', 'pptx', 'csv']);
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            return $resultFile = $uploader->save($target);
        }catch(\Exception $er){
            throw new \Exception("Debe adjuntar informe  de servicio, la extensiones permitidas son: 'jpeg', 'jpg', 'png', 'gif', 'bmp' , 'pdf' , 'doc' , 'ppt', 'csv'");
        }
        
    }
    public function uploadcv($files,$index){
        try{
            $target = $this->_mediaDirectory->getAbsolutePath('cvteam/files/');
            $uploader = $this->_fileUploaderFactory->create(['fileId' => 'cv['.$index.']']);
            $uploader->setAllowedExtensions(['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'csv']);
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            return $resultFile = $uploader->save($target);
        }catch(\Exception $er){
            throw new \Exception("Debe adjuntar cv, la extensiones permitidas son: 'jpeg', 'jpg', 'png', 'gif', 'bmp' , 'pdf' , 'doc' , 'ppt', 'csv'");
        }
        
    }
    public function getDirectoryUpload(){
        return $this->_dir->getPath('media');
    }
    public function getDirectoryTmp(){
        return $this->_dir->getPath('tmp');
    }
}