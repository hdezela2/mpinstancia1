<?php
namespace Formax\QuotesSupplier\Controller\Team;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;

/**
 * Lists class
 */
class Job extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;
    
    /**
     * @var \Webkul\Requestforquote\Helper\Data
     */
    private $dataHelper;
    
    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    private $mpHelper;

    /**
     * __construct function
     *
     * @param Context                             $context
     * @param PageFactory                         $resultPageFactory
     * @param \Webkul\Requestforquote\Helper\Data $dataHelper
     * @param \Webkul\Marketplace\Helper\Data     $mpHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Webkul\Requestforquote\Helper\Data $dataHelper,
        \Webkul\Marketplace\Helper\Data $mpHelper
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        $this->dataHelper = $dataHelper;
        $this->mpHelper = $mpHelper;
        parent::__construct($context);
    }

    /**
     * Check customer is logged in or not ?
     *
     * @param  RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        if (!$this->dataHelper->getStatusOfModuleFromConfig()) {
            $this
                ->resultFactory
                ->create('forward')
                ->forward('noroute');
        }
        return parent::dispatch($request);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute(){
        $resultPage = $this->resultPageFactory->create();
        if ($this->mpHelper->getIsSeparatePanel()) {
            $resultPage->addHandle('quotessupplier_team_job_layout2');
        }
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
