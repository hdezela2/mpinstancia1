<?php

namespace Formax\QuotesSupplier\Controller\Quote;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Webkul\Requestforquote\Controller\AbstractQuotes;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ResourceConnection;
use Webkul\Requestforquote\Model\ConversationFactory;
use Webkul\Requestforquote\Model\InfoFactory;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Customer\Model\Customer;
use Webkul\Marketplace\Helper\Data as HelperMarketplace;
use Magento\Framework\Json\Helper\Data as HelperJson;
use Webkul\Requestforquote\Helper\Data as HelperRequestforquote;
use Formax\ActionLogs\Helper\Data as HelperActionLog;
use Formax\CreateFormAttributes\Helper\Data as HelperFormAttributes;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Webkul\Requestforquote\Controller\Quote\SendMessageToCustomer as WebkulSendMessageToCustomer;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Formax\QuotesSupplier\Helper\Data as QuoteStatusConstants;

class SendMessageToCustomer extends WebkulSendMessageToCustomer
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    private $json;

    /**
     * @var \Webkul\Requestforquote\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Webkul\Requestforquote\Model\ConversationFactory
     */
    private $conversation;

    /**
     * @var \Webkul\Requestforquote\Model\InfoFactory
     */
    private $info;

    /**
     * @var \Webkul\Requestforquote\Model\QuoteFactory
     */
    private $quote;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    private $customer;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    private $mpHelper;

    /**
     * @var \Formax\CreateFormAttributes\Helper\Data
     */
    protected $fmHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Formax\ActionLogs\Helper\Data
     */
    private $_helperActionLog;

    /**
     * @var \Formax\ConfigCmSoftware\Helper\Data
     */
    protected $helperSoftware;

    /**
     * @var Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var SoftwareRenewalConstants
     */
    protected $softwareRenewalConstants;

    /**
     * __construct
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Webkul\Requestforquote\Helper\Data $dataHelper
     * @param \Webkul\Requestforquote\Model\ConversationFactory $conversation
     * @param \Webkul\Requestforquote\Model\InfoFactory $info
     * @param \Webkul\Requestforquote\Model\QuoteFactory $quote
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Formax\ActionLogs\Helper\Data $helperActionLog
     * @param \Formax\CreateFormAttributes\Helper\Data $fmHelper
     * @param \Formax\ConfigCmSoftware\Helper\Data $helperSoftware
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        HelperJson $jsonHelper,
        HelperRequestforquote $dataHelper,
        ConversationFactory $conversation,
        InfoFactory $info,
        QuoteFactory $quote,
        Customer $customer,
        HelperMarketplace $mpHelper,
        HelperActionLog $helperActionLog,
        HelperFormAttributes $fmHelper,
        HelperSoftware $helperSoftware,
        StoreManagerInterface $storeManager,
        ResourceConnection $resource,
        SoftwareRenewalConstants $softwareRenewalConstants
    ) {

        $this->resultPageFactory = $resultPageFactory;
        $this->json = $jsonHelper;
        $this->dataHelper = $dataHelper;
        $this->conversation = $conversation;
        $this->info = $info;
        $this->quote = $quote;
        $this->customer = $customer;
        $this->mpHelper = $mpHelper;
        $this->fmHelper = $fmHelper;
        $this->helperSoftware = $helperSoftware;
        $this->_storeManager = $storeManager;
        $this->_resource = $resource;
        $this->_helperActionLog = $helperActionLog;
        $this->softwareRenewalConstants = $softwareRenewalConstants;

        parent::__construct(
            $context,
            $resultPageFactory,
            $jsonHelper,
            $dataHelper,
            $conversation,
            $info,
            $quote,
            $customer,
            $mpHelper
        );
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        //if ($this->helperSoftware->getCurrentStoreCode() == HelperSoftware::SOFTWARE_STOREVIEW_CODE ) {
        if (in_array($this->helperSoftware->getCurrentStoreCode(),[HelperSoftware::SOFTWARE_STOREVIEW_CODE, SoftwareRenewalConstants::STORE_VIEW_CODE]) ) {
            $extraData = [
                'action' => 'Respuesta de Cotización'
            ];
            $actionName = 'Oferta';
            $profileName = 'Proveedor';
            $resultPage = $this->resultPageFactory->create();
            $helper = $this->dataHelper;
            $postInfo = $this->getRequest()->getPostValue();
            $postDataMs = $this->getRequest()->getParams();
            $filesData = $this->getRequest()->getFiles();
            $entity = 'requestforquote_quote_conversation';
            $postData = $this->fmHelper->parsePostData($entity, $postInfo, $filesData);
            $paramSellerQuoteId = $this->getRequest()->getParam('seller_quote_id', 0);

            $sellerQuoteId = $paramSellerQuoteId;
            $sellerQuote = $this->info->create()->load($sellerQuoteId);
            $quoteId = $sellerQuote['quote_id'];
            $quote = $this->quote->create()->load($quoteId);

         if ((int)$quote['status'] == QuoteStatusConstants::PUBLISHED) {

             // MPMT-92 - Proveedor - al crear cotizacion debe ser obligatorio el ingreso de equipo de trabajo
             if (isset($postData['teamwork'])) {
                 $teamworkArray = json_decode($postData['teamwork'], true);
                 $isEmpty = false;
                 $notCompleted = false;
                 foreach ($teamworkArray['data'] as $key => $value) {

                     if (!(bool)$value) {
                         $isEmpty = true;
                         break;
                     }

                     $qtyOfTeamWorkers = sizeof($value);
                     foreach ($value as $k => $team) {
                         if (!(bool)$team) {
                             $notCompleted = true;
                             break 2;
                         }
                     }

                 }
                 if ($isEmpty) {
                     $message = __('You must complete at least 1 member of the teamwork');
                     $this->messageManager->addError($message);
                     $this->_redirect('requestforquote/seller/view', ['id' => $paramSellerQuoteId]);
                     return;
                 }
                 if ($notCompleted) {
                     $message = __('You must complete all input fields for each member of the teamwork');
                     $this->messageManager->addError($message);
                     $this->_redirect('requestforquote/seller/view', ['id' => $paramSellerQuoteId]);
                     return;
                 }
             }

             if (isset($postData['offer_price'])) {
                 // $iniUtmQty = $this->helperSoftware->getIniQtyUtm();
                 $iniUtmQty = $this->fmHelper->getInitialRangeQtyUTM();
                 // $endUtmQty = $this->helperSoftware->getEndQtyUtm();
                 $endUtmQty = $this->fmHelper->getEndRangeQtyUTM();
                 $utmQty = $this->helperSoftware->getUtmQtyByAmount($postData['offer_price']);

                 try {
                     if ($utmQty < $iniUtmQty || $utmQty > $endUtmQty) {
                         $message = __(
                             'Amount value %1 do not is between %2 and %3.',
                             $this->helperSoftware->currencyFormat($postData['offer_price']),
                             $iniUtmQty,
                             $endUtmQty
                         );
                         throw new \Exception($message, HelperFormAttributes::CUSTOM_ERROR_CODE);
                     }
                 } catch (\Exception $e) {
                     $this->messageManager->addError($e->getMessage());
                     $this->_redirect('requestforquote/seller/view', ['id' => $paramSellerQuoteId]);
                     return;
                 }
             }

             if (empty($postData)) {
                 $postData = $this->getRequest()->getParams();
             }

             $postData['seller_quote_id'] = $paramSellerQuoteId;
             $saveInfoConversation = $this->getDocumentFromQuote($postData['seller_quote_id']);

             //Obtengo el documento del registro anterior para volver asignarlo
             if (!isset($postInfo['__remove__offer_document']) ||
                 (isset($postInfo['__remove__offer_document']) && $postInfo['__remove__offer_document'] != '1')) {
                 $currentDocument = $this->getRequest()->getFiles('offer_document');
                 if ((isset($currentDocument['name']) && $currentDocument['name'] == '') ||
                     (isset($currentDocument[0]['name']) && $currentDocument[0]['name'] == '')) {
                     $postData['offer_document'] = $saveInfoConversation['offer_document'] ?? null;
                 }
             }
             $postData['table_requirement'] = $this->prepareTableRequirement(
                 $saveInfoConversation['table_requirement'] ?? null,
                 $postData['table_requirement']);

             $this->saveEdited($postData['seller_quote_id']);
             $postData['edited'] = 1;

             //Si algún dato que llegue en $postData es un array lo convierto a json para que pueda guardar en la DB.
             foreach ($postData as $key => $value) {
                 if (is_array($value)) {
                     $postData[$key] = json_encode($value);
                 }
             }

             //$postData['response'] será reemplazado por quote_details solamente si existe,
             //asi evitamos modificar este dato para otros convenios.
             if (isset($postData['quote_detalis'])) {
                 $postData['response'] = $postData['quote_detalis'];
             }

             $sellerQuoteId = $postData['seller_quote_id'];
             $id = $postData['seller_quote_id'];

             try {
                 $isPartner = $this->mpHelper->isSeller();
                 if ($isPartner != 1) {
                     $this->_forward('noRoute');
                     return;
                 }
                 if (!$this->_loadValidSellerQuote($this->mpHelper, $this->info, $sellerQuoteId)) {
                     return;
                 }

                 if (isset($postData['sample_images'])) {
                     $postData['sample_images'] = implode(',', $postData['sample_images']);
                 }

                 $sellerId = $this->mpHelper->getCustomerId();
                 $postData['seller_id'] = $sellerId;
                 $seller = $this->customer->load($sellerId);
                 $sellerId = $seller->getId();
                 $sellerName = $seller->getFirstname() . ' ' . $seller->getLastname();
                 $sellerEmail = $seller->getEmail();
                 $senderInfo = [
                     'name' => $sellerName,
                     'email' => $sellerEmail
                 ];
                 $postData['sender_type'] = \Webkul\Requestforquote\Model\Conversation::SENDER_TYPE_SELLER;
                 unset($postData['offer_discount_percent']);
                 unset($postData['offoffer_final_priceer_price']);
                 if (!isset($postData['created_at'])) {
                     $postData['created_at'] = date('Y-m-d H:i:s');
                 }
                 if (!isset($postData['updated_at'])) {
                     $postData['updated_at'] = date('Y-m-d H:i:s');
                 }
                 $modelInfo = $this->conversation->create();
                 $modelInfo->setData($postData);
                 $modelInfo->save();

                 $sellerQuote = $this->info->create()->load($sellerQuoteId);
                 $tmpsellerQuote = $sellerQuote;
                 if ($sellerQuote->getCustomerStatus() != 3 && $sellerQuote->getStatus() != 3) {
                     $sellerQuote->setStatus(2);
                     $sellerQuote->setCustomerStatus(1);
                     $sellerQuote->save();
                 }
                 $quoteId = $sellerQuote['quote_id'];
                 $quote = $this->quote->create()->load($quoteId);
                 $customerId = $quote['customer_id'];

                 /* Send Quote Mail To Customer */
                 $customer = $this->customer->load($customerId);
                 $postData = $postDataMs;
                 if ($sellerId) {
                     /* send mail to seller */
                     /* Assign values for your template variables  */
                     $emailTempVariables = [];
                     $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
                     $customerEmail = $customer->getEmail();
                     $receiverInfo = [
                         'name' => $customerName,
                         'email' => $customerEmail
                     ];
                     $emailTempVariables['myvar1'] = $customerName;
                     $emailTempVariables['myvar2'] = '';
                     if (isset($postData['bulk_quote_qty']) && isset($postData['quote_price'])) {
                         $emailTempVariables['myvar3'] = $postData['bulk_quote_qty'];
                         $emailTempVariables['myvar4'] = $postData['quote_price'];
                     }

                     try {
                         $template = HelperRequestforquote::XML_PATH_REPLY_MAIL_TO_CUSTOMER;
                         $this->dataHelper->customMailSendMethod(
                             $emailTempVariables,
                             $senderInfo,
                             $receiverInfo,
                             $template
                         );
                     } catch (\Exception $e) {
                         $this->messageManager->addError($e->getMessage());
                     }
                 }

                 $extraData['status'] = 'success';
                 $extraData['request'] = $tmpsellerQuote->getData();
                 $extraData['id_cotizacion'] = $sellerQuote->getQuoteId();
                 $extraData['response']['data'] = $sellerQuote->getData();
                 $this->_helperActionLog->saveLoggin($actionName, $profileName, $extraData);
                 $this->messageManager->addSuccess(__('Message was successfully send.'));
                 $this->_redirect('requestforquote/seller/view', ['id' => $id]);
             } catch (\Exception $e) {
                 $this->messageManager->addError($e->getMessage());
                 $this->_redirect('requestforquote/seller/view', ['id' => $id]);
                 return;
             }
         }else{
             $this->messageManager->addError(__('The quote is no longer published'));
             $this->_redirect('requestforquote/seller/lists');
         }

        } else {
            $resultPage = $this->resultPageFactory->create();
            $helper = $this->dataHelper;
            $postData =$this->getRequest()->getParams();
            $sellerQuoteId = $postData['seller_quote_id'];
            $id = $postData['seller_quote_id'];

            try {
                $isPartner = $this->mpHelper->isSeller();
                if ($isPartner != 1) {
                    $this->_forward('noRoute');
                    return;
                }
                if (!$this->_loadValidSellerQuote($this->mpHelper, $this->info, $sellerQuoteId)) {
                    return;
                }
                if (isset($postData['sample_images'])) {
                    $postData['sample_images'] = implode(',', $postData['sample_images']);
                }

                $sellerId = $this->mpHelper->getCustomerId();
                $postData['seller_id'] = $sellerId;
                $seller = $this->customer->load($sellerId);
                $sellerId = $seller->getId();
                $sellerName = $seller->getFirstname() . ' ' . $seller->getLastname();
                $sellerEmail = $seller->getEmail();
                $senderInfo = [
                    'name' => $sellerName,
                    'email' => $sellerEmail
                ];
                $postData['sender_type'] = \Webkul\Requestforquote\Model\Conversation::SENDER_TYPE_SELLER;
//                $modelInfo = $this->conversation->create();
//                $modelInfo->setData($postData);
//                $modelInfo->save();

                $sellerQuote = $this->info->create()->load($sellerQuoteId);
                if ($sellerQuote->getCustomerStatus() != 3 && $sellerQuote->getStatus() != 3) {
                    $sellerQuote->setStatus(2);
                    $sellerQuote->setCustomerStatus(1);
                    $sellerQuote->save();
                }
                $quoteId = $sellerQuote['quote_id'];
                $quote = $this->quote->create()->load($quoteId);
                $customerId = $quote['customer_id'];

                /* Send Quote Mail To Customer */
                $customer = $this->customer->load($customerId);
                if ($sellerId) {
                    /* send mail to seller */
                    /* Assign values for your template variables  */
                    $emailTempVariables = [];
                    $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
                    $customerEmail = $customer->getEmail();
                    $receiverInfo = [
                        'name' => $customerName,
                        'email' => $customerEmail
                    ];
                    $emailTempVariables['myvar1'] = $customerName;
                   // $emailTempVariables['myvar2'] = $postData['response'];
                    if (isset($postData['bulk_quote_qty']) && isset($postData['quote_price'])) {
                        $emailTempVariables['myvar3'] = $postData['bulk_quote_qty'];
                        $emailTempVariables['myvar4'] = $postData['quote_price'];
                    }
                    try {
                        $template = HelperRequestforquote::XML_PATH_REPLY_MAIL_TO_CUSTOMER;
                        $this->dataHelper->customMailSendMethod(
                            $emailTempVariables,
                            $senderInfo,
                            $receiverInfo,
                            $template
                        );
                    } catch (\Exception $e) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
                $this->messageManager->addSuccess(__('Message was successfully send.'));
                $this->_redirect('requestforquote/seller/view', ['id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_redirect('requestforquote/seller/view', ['id' => $id]);
                return;
            }
        }

    }

    /**
     * Update old rows to edited zero
     *
     * @param int $quoteId
     * @return void
     */
    protected function saveEdited($quoteId)
    {
        $resourceConnection = $this->_resource->getConnection();
        $data = ['edited' => '0'];
        $where = ['seller_quote_id = ?' => (int)$quoteId];
        $tableName = $resourceConnection->getTableName('requestforquote_quote_conversation');
        $resourceConnection->update($tableName, $data, $where);
    }

    /**
     * Get document from seller quote
     *
     * @param int $quoteId
     * @return void
     */
    protected function getDocumentFromQuote($quoteId)
    {
        $connection  = $this->_resource->getConnection();
        $tableName = $connection->getTableName('requestforquote_quote_conversation');

        $select = $connection->select()
            ->from($tableName, ['table_requirement', 'offer_document'])
            ->where('seller_quote_id = :seller_quote_id')
            ->where('edited = :edited');

        $bind = [':seller_quote_id' => $quoteId, ':edited' => 1];
        $document = $connection->fetchRow($select, $bind);

        return $document;
    }

    /**
     * Prepera data save in table_requirement
     *
     * @param array $oldData
     * @param array $newData
     * @return string
     */
    protected function prepareTableRequirement($oldData, $newData)
    {
        $criterioFileArray = [];
        if ($oldData && $newData) {
            $oldDataArray = $this->json->jsonDecode($oldData);
            $newDataArray = $this->json->jsonDecode($newData);

            for ($i = 0; $i < 8; $i++) {
                if (isset($oldDataArray['data']['criterio_file'][$i]) &&
                    (!isset($newDataArray['data']['criterio_file'][$i]) || (isset($newDataArray['data']['criterio_file'][$i]) && $newDataArray['data']['criterio_file'][$i] == ''))) {
                    $criterioFileArray[$i] = $oldDataArray['data']['criterio_file'][$i];
                } elseif (isset($newDataArray['data']['criterio_file'][$i])) {
                    $criterioFileArray[$i] = $newDataArray['data']['criterio_file'][$i];
                }
            }
            if (count($criterioFileArray) > 0) {
                $newDataArray['data']['criterio_file'] = $criterioFileArray;
            }
            $newData = $this->json->jsonEncode($newDataArray);
        }

        return $newData;
    }

    /**
     * Get current store name
     *
     * @return string
     */
    protected function getStoreName()
    {
        return trim($this->_storeManager->getStore()->getName());
    }

}
