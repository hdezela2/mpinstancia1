<?php

namespace Formax\QuotesSupplier\Plugin\Requestforquote\Controller\Seller;

use Magento\Framework\App\Cache\TypeListInterface;
use Webkul\Requestforquote\Controller\Seller\Lists as WebkulSellerLists;

/**
 * Lists class
 */
class Lists
{
    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    private $cacheTypeList;

    /**
     * Initialize dependencies inyection
     *
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     */
    public function __construct(
        TypeListInterface $cacheTypeList
    ) {
    
        $this->cacheTypeList = $cacheTypeList;
    }

    /**
     * before plugin Execute method
     * 
     * @return void
     */
    public function beforeExecute(WebkulSellerLists $subject)
    {
        $types = ['config'];
        foreach ($types as $type) {
            $this->cacheTypeList->cleanType($type);
        }
    }
}
