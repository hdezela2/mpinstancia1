<?php

namespace Formax\EvaluateRequestforquote\Controller\Requestforquote;

use Cleverit\Requestforquote\Helper\Data as RequestforquoteHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Exception\LocalizedException;
use Formax\ActionLogs\Model\ActionLogsFactory;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Marketplace\Controller\Product\SaveProduct;
use Webkul\Requestforquote\Model\ConversationFactory;
use Webkul\Requestforquote\Model\InfoFactory;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory;
use Webkul\Requestforquote\Model\Conversation;
use Webkul\Requestforquote\Model\Product\Type\Quote;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Webkul\Marketplace\Helper\Data as HelperMarketplace;
use Magento\Customer\Model\Session as CustomerSession;
use Formax\ActionLogs\Helper\Data as HelperActionLog;
use Webkul\Requestforquote\Helper\Data as HelperRequestforquote;
use Formax\EvaluateRequestforquote\Model\EconomicEvaluationFactory;
use Formax\CreateFormAttributes\Helper\Data as FormAttributesHelper;
use Formax\EvaluateRequestforquote\Helper\Data as HelperEvaluation;
use Formax\QuotesSupplier\Controller\Account\SendMessageToSeller;
use Formax\QuotesSupplier\Helper\Data as QuotesSupplierHelper;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory as ConversationCollectionFactory;

/**
 * SendMessageToSeller class
 */
class AwardSeller extends SendMessageToSeller
{
    /**
     * @var Formax\CreateFormAttributes\Helper\Data
     */
    protected $formAttributesHelper;

    /**
     * @var Formax\EvaluateRequestforquote\Helper\Data
     */
    protected $helperEvaluation;

    /**
     * @var Formax\EvaluateRequestforquote\Model\EconomicEvaluationFactory
     */
    protected $economicEvaluationFactory;

    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $helperSoftware;

    /**
     * @var Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     * @var ConversationCollectionFactory
     */
    protected $conversationCollectionFactory;

    /** @var RequestforquoteHelper */
    protected $rfqHelper;

    protected FormAttributesHelper $softwareHelper;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Webkul\Requestforquote\Helper\Data $dataHelper
     * @param \Webkul\Requestforquote\Model\ConversationFactory $conversation
     * @param \Webkul\Requestforquote\Model\InfoFactory $info
     * @param \Webkul\Requestforquote\Model\QuoteFactory $quote
     * @param \Magento\Customer\Model\CustomerFactory $customer
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Formax\ActionLogs\Helper\Data $helperActionLog
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory
     * @param \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory $urlRewrite
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Webkul\Marketplace\Controller\Product\SaveProduct $saveProduct
     * @param FormAttributesHelper $formAttributesHelper
     * @param \Formax\EvaluateRequestforquote\Helper\Data $helperEvaluation
     * @param \Formax\ConfigCmSoftware\Helper\Data $helperSoftware
     * @param \Formax\EvaluateRequestforquote\Model\EconomicEvaluationFactory $economicEvaluationFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        HelperRequestforquote $dataHelper,
        ConversationFactory $conversation,
        InfoFactory $info,
        QuoteFactory $quote,
        CustomerFactory $customer,
        HelperMarketplace $mpHelper,
        CustomerSession $customerSession,
        HelperActionLog $helperActionLog,
        StoreManagerInterface $storeManager,
        DirectoryList $directoryList,
        ProductInterfaceFactory $productFactory,
        UrlRewriteCollectionFactory $urlRewrite,
        ResourceConnection $resource,
        SaveProduct $saveProduct,
        FormAttributesHelper $formAttributesHelper,
        HelperEvaluation $helperEvaluation,
        HelperSoftware $helperSoftware,
        EconomicEvaluationFactory $economicEvaluationFactory,
        DateTime $dateTime,
        ConversationCollectionFactory $conversationCollectionFactory,
        RequestforquoteHelper $rfqHelper,
        FormAttributesHelper $softwareHelper
    ) {
        $this->formAttributesHelper = $formAttributesHelper;
        $this->helperEvaluation = $helperEvaluation;
        $this->economicEvaluationFactory = $economicEvaluationFactory;
        $this->_resource = $resource;
        $this->_customerSession = $customerSession;
        $this->helperSoftware = $helperSoftware;
        $this->dateTime = $dateTime;
        $this->conversationCollectionFactory = $conversationCollectionFactory;
        $this->rfqHelper = $rfqHelper;
        parent::__construct(
            $context,
            $resultPageFactory,
            $dataHelper,
            $conversation,
            $info,
            $quote,
            $customer,
            $mpHelper,
            $customerSession,
            $helperActionLog,
            $storeManager,
            $directoryList,
            $productFactory,
            $urlRewrite,
            $resource,
            $saveProduct
        );
        $this->softwareHelper = $softwareHelper;
    }

    /**
     * @return \Magento\Framework\View\Result\Page|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $extraData = [
            'action' => 'Selección de oferta económica de proveedor',
            'status' => 'success'
        ];
        $actionName = 'Evaluación Paso 3';
        $profileName = 'Cliente';
        $resultPage = $this->resultPageFactory->create();
        $helper = $this->dataHelper;
        $postData = $this->getRequest()->getPostValue();
        $filesData = $this->getRequest()->getFiles();
        $entity = 'dccp_economic_evaluation';
        $data = $this->formAttributesHelper->parsePostData($entity, $postData, $filesData);
        $postQuoteId = $this->getRequest()->getParam('quote_id', 0);
        $postSellerId = isset($postData['seller_quote_id']) ? (int)$postData['seller_quote_id'] : 0;
        $sellerQuoteId = $this->getQuoteSellerId($postQuoteId, $postSellerId);
        $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
        $valQuote = $this->helperEvaluation->getQuoteById($postQuoteId);

        try {
            if ($postSellerId === 0) {
                $message = __('You must select the seller to award.');
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

            if ((int)$sellerQuoteId === 0) {
                $message = __('An error occured trying to award seller.');
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

            if ($valQuote->getApprovedSellerQuoteId() > 0) {
                $message = __('Quote #%1 has already been awarded.', $postQuoteId);
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

            if ($this->_helperActionLog->isStoreSaveLog()) {
                $customerId = $this->mpHelper->getCustomerId();

                if (isset($data['error']) && isset($data['msg']) && $data['error']) {
                    throw new \Exception($data['msg'], FormAttributesHelper::CUSTOM_ERROR_CODE);
                }

                $validateData = $this->formAttributesHelper->validatePostData($entity, $postData);
                if (isset($validateData['error']) && $validateData['error']) {
                    $message = isset($validateData['msg']) ? $validateData['msg'] : __('An error occured trying to award seller.');
                    throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
                }

                $ranking = trim($data['ranking']);
                $basePrice = trim($data['base_price']);
                $discountAmount = trim($data['discount_amount']);
                $discountPercent = trim($data['discount_percent']);
                $finalPrice = trim($data['final_price']);
                $iniUtmQty = $this->helperSoftware->getIniQtyUtm();
                $endUtmQty = $this->softwareHelper->getEndRangeQtyUTM();
                $utmQty = $this->helperSoftware->getUtmQtyByAmount($basePrice);

                if ($utmQty < $iniUtmQty || $utmQty > $endUtmQty) {
                    $message =  __(
                        'Amount value selected %1 do not is between %2 and %3.',
                        $this->helperSoftware->currencyFormat($basePrice),
                        $iniUtmQty,
                        $endUtmQty
                    );
                    throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
                }

                if ($postSellerId > 0) {
                    $model = $this->economicEvaluationFactory->create()
                        ->deleteBySellerQuote($sellerQuoteId, $websiteId);
                    $model = $this->economicEvaluationFactory->create();
                    $data['seller_quote_id'] = $sellerQuoteId;
                    $data['website_id'] = $websiteId;
                    $model->setData($data);
                    $model->save()->getId();

                    /** update final price offer by sellers */
                    $this->setFinalPriceToSellerDiscard($postQuoteId);

                    /** Update requestforquote_quote_conversation table with quote selected */
                    $newPostData['sender_type'] = Conversation::SENDER_TYPE_CUSTOMER;
                    $newPostData['bulk_quote_qty'] = 1;
                    $newPostData['quote_price'] = $data['final_price'];
                    $newPostData['seller_quote_id'] = $sellerQuoteId;

                    $conversationCollectionFactory = $this->conversationCollectionFactory->create()
                        ->addFieldToFilter('seller_quote_id', $sellerQuoteId)
                        ->addFieldToFilter('edited', 1)
                        ->setPageSize(1);

                    $conversationId = 0;
                    if ($conversationCollectionFactory) {
                        foreach ($conversationCollectionFactory as $modelInfo) {
                            //$modelInfo = $this->conversation->create()->load($sellerQuoteId, 'seller_quote_id');
                            $conversationId = $modelInfo->getId();
                            $newPostData['entity_id'] = $conversationId;
                            $modelInfo->setData($newPostData);
                            $modelInfo->save();
                        }
                    }

                    /** Update requestforquote_quote_info table with quote selected */
                    $sellerQuote = $this->info->create()->load($sellerQuoteId);
                    $tmpSellerQuote = $sellerQuote->getData();
                    if ($sellerQuote->getStatus() != 3) {
                        $sellerQuote->setStatus(1);
                        $sellerQuote->setCustomerStatus(2);
                        $sellerQuote->save();
                    }

                    /** Update requestforquote_quote table with seller quote ID selected */
                    if ($conversationId > 0) {
                        $quoteId = $sellerQuote['quote_id'];
                        $quote = $this->quote->create()->load($quoteId);
                        $quote->setApprovedSellerQuoteId($conversationId)
                            ->save();
                        $requestforquoteInfoColls = $this->info->create()->getCollection()
                            ->addFieldToFilter('quote_id', $quoteId);
                        foreach ($requestforquoteInfoColls as $key => $value) {
                            if ($value->getId() == $sellerQuoteId) {
                                $value->setStatus(3);
                                $value->setCustomerStatus(3);
                                $value->save();
                            } else {
                                $value->setStatus(4);
                                $value->setCustomerStatus(4);
                                $value->save();
                            }
                        }
                    }

                    $extraData['request'] = null;
                    $extraData['id_cotizacion'] = $sellerQuote->getData()['quote_id'];
                    $extraData['response']['data'] = $sellerQuote->getData();
                    $extraData['response']['post_info'] = $postData;
                    $this->_helperActionLog->saveLoggin($actionName, $profileName, $extraData);
                    if (!is_numeric($finalPrice) || $finalPrice < 0) {
                        $finalPrice = 1;
                    }
                    if (!empty($quote)){
                        $this->createNewProduct($quote, $sellerQuoteId, $finalPrice);
                    }
                    $this->helperEvaluation->destroyAllSessions($postQuoteId);

                    /* Send Quote Mail To Seller */
                    $customerId = $this->mpHelper->getCustomerId();
                    $customer = $this->customer->create()->load($customerId);
                    $sellerId = $sellerQuote['seller_id'];
                    $seller = $this->customer->create()->load($sellerId);

                    if ($seller->getId()) {
                        /* Send mail to seller */
                        /* Assign values for your template variables  */
                        $emailTempVariables = [];
                        $sellerName = $seller->getFirstname() . ' ' . $seller->getLastname();
                        $sellerEmail = $seller->getEmail();
                        $receiverInfo = [
                            'name' => $sellerName,
                            'email' => $sellerEmail
                        ];
                        $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
                        $customerEmail = $customer->getEmail();
                        $senderInfo = [
                            'name' => $customerName,
                            'email' => $customerEmail
                        ];
                        $emailTempVariables['myvar1'] = $sellerName;
                        if (!empty($quote)) {
                            $emailTempVariables['myvar2'] = $quote->getSubject();
                        } else {
                            $emailTempVariables['myvar2'] = "";
                        }

                        try {
                            $template = HelperRequestforquote::XML_PATH_REPLY_MAIL_TO_SELLER;
                            $this->dataHelper->customMailSendMethod(
                                $emailTempVariables,
                                $senderInfo,
                                $receiverInfo,
                                $template
                            );
                        } catch (\Exception $e) {
                            $this->messageManager->addError($e->getMessage());
                        }
                    }
                    if (!empty($quoteId)) {
                        $this->rfqHelper->updateQuoteStatus($quoteId, QuotesSupplierHelper::EVALUATION_CLOSED);
                    }
                    $this->_redirect('evaluate/requestforquote/success', ['id' => $sellerQuoteId]);
                }
            }
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $this->_redirect('evaluate/requestforquote/index', ['id' => $postQuoteId, 'step' => 3]);
            return;
        }
    }

    /**
     * Get seller quote ID
     *
     * @param  int $quoteId
     * @param  int $sellerId
     * @return int
     */
    public function getQuoteSellerId($quoteId, $sellerId)
    {
        $SellerQuoteId = 0;
        $requestforquoteInfoColls = $this->info->create()->getCollection()
            ->addFieldToFilter('quote_id', $quoteId)
            ->addFieldToFilter('seller_id', $sellerId)
            ->setPageSize(1);
        foreach ($requestforquoteInfoColls as $value) {
            $SellerQuoteId = $value->getId();
        }

        return $SellerQuoteId;
    }

    /**
     * Set final price to seller evaluation
     *
     * @param int $quoteId
     * @return void
     */
    public function setFinalPriceToSellerDiscard($quoteId)
    {
        if ($quoteId) {
            $connection = $this->_resource->getConnection();
            $table = $this->_resource->getTableName('requestforquote_quote_conversation');
            $quote = $this->helperEvaluation->getSellersSelectedCollection($quoteId, false);
            if (isset($quote['data']) && $quote['data']) {
                foreach ($quote['data'] as $item) {
                    $sql = 'UPDATE ' . $table . ' SET offer_discount_percent = ' . (float)$item->getDiscountPercent() .
                    ', offer_final_price = ' . (float)$item->getFinalPrice() .
                    ' WHERE seller_quote_id = ' . $item->getEntityId();
                    $connection->query($sql);
                }
            }
        }
    }
}
