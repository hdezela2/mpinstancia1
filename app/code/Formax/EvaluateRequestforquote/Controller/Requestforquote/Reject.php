<?php

namespace Formax\EvaluateRequestforquote\Controller\Requestforquote;

use Cleverit\Requestforquote\Helper\Data as RfqHelper;
use Formax\EvaluateRequestforquote\Helper\Data as EvaluateHelper;
use Formax\QuotesSupplier\Helper\Data as QsHelper;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Reject extends AbstractAccount
{
    /** @var PageFactory */
    protected $_resultPageFactory;

    /** @var EvaluateHelper */
    protected $_evaluateHelper;

    /** @var RfqHelper */
    protected $_helper;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        EvaluateHelper $evaluateHelper,
        RfqHelper $helper
    ) {
        parent::__construct($context);
        $this->_resultPageFactory = $pageFactory;
        $this->_evaluateHelper = $evaluateHelper;
        $this->_helper = $helper;
    }

    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $quoteId = (int)$this->getRequest()->getParam('quote_id');
        if (!$quoteId) {
            $quoteId = $this->getRequest()->getParams()[0];
        }
        $quoteModel = $this->_evaluateHelper->getQuoteById($quoteId);

        if ($quoteModel->getId() && $quoteModel->getApprovedSellerQuoteId() > 0) {
            $this->messageManager->addNotice(__('Quote #%1 has already been evaluated.', $quoteId));
            return $this->_redirect('requestforquote/account/lists/');
        }

        if (!$quoteModel->getId()) {
            return $this->_redirect('requestforquote/reject/quote_id/', [$quoteId]);
        }

        return $resultPage;
    }
}
