<?php

namespace Formax\EvaluateRequestforquote\Controller\Requestforquote;

use Psr\Log\LoggerInterface;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Request\DataPersistorInterface;
use Formax\EvaluateRequestforquote\Model\TechnicalEvaluationFactory;
use Formax\CreateFormAttributes\Helper\Data as FormAttributesHelper;
use Formax\EvaluateRequestforquote\Helper\Data as HelperEvaluation;
use Formax\ActionLogs\Helper\Data as HelperActionLog;

class SaveStep2 extends AbstractAccount
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var Magento\Framework\App\Request\DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Formax\EvaluateRequestforquote\Model\TechnicalEvaluationFactory
     */
    protected $technicalEvaluationFactory;

    /**
     * @var Formax\CreateFormAttributes\Helper\Data
     */
    protected $fmHelper;

    /**
     * @var Formax\EvaluateRequestforquote\Helper\Data
     */
    protected $helperEval;

    /**
     * @var Formax\ActionLogs\Helper\Data
     */
    private $helperActionLog;

    /**
     * Constructor dependecy injection
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Formax\CreateFormAttributes\Helper\Data $fmHelper
     * @param Formax\EvaluateRequestforquote\Helper\Data $helperEval
     * @param \Formax\ActionLogs\Helper\Data $helperActionLog
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Formax\EvaluateRequestforquote\Model\TechnicalEvaluationFactory $technicalEvaluationFactory
     */

    public function __construct(
        Context $context,
        LoggerInterface $logger,
        FormAttributesHelper $fmHelper,
        HelperEvaluation $helperEval,
        HelperActionLog $helperActionLog,
        StoreManagerInterface $storeManager,
        TechnicalEvaluationFactory $technicalEvaluationFactory
    ) {
        $this->storeManager = $storeManager;
        $this->fmHelper = $fmHelper;
        $this->helperEval = $helperEval;
        $this->logger = $logger;
        $this->helperActionLog = $helperActionLog;
        $this->technicalEvaluationFactory = $technicalEvaluationFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $actionName = 'Evaluación Paso 2';
        $profileName = 'Cliente';
        $quoteId = $this->getRequest()->getParam('quote_id', 0);
        $postData = $this->getRequest()->getPostValue();
        $filesData = $this->getRequest()->getFiles();
        $entity = 'dccp_technical_evaluation';
        $data = $this->fmHelper->parsePostData($entity, $postData, $filesData);
        $keyDataPersistor = 'formattributes_' . $entity;
        $websiteId = (int)$this->storeManager->getStore()->getWebsiteId();

        // LOGS
        $extraData = [
            'action' => 'Evaluación de cotización - Paso 2',
            'status' => ''
        ];

        try {
            if (isset($postData['seller_id']) && $postData['seller_id'] == '') {
                $message = __('You must select at least one seller.');
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

            if (isset($data['error']) && isset($data['msg']) && $data['error']) {
                throw new \Exception($data['msg'], FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

            $validateData = $this->fmHelper->validatePostData($entity, $postData);
            if (isset($validateData['error']) && $validateData['error']) {
                $message = isset($validateData['msg']) ? $validateData['msg'] : __('An error occured trying to save request for quote.');
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }

            if (isset($data['seller_id']) && isset($data['warranty_months'])) {
                $sellersTmp = trim($data['seller_id']);
                $sellers = explode(',', $sellersTmp);
                $warranties = explode(',', trim($data['warranty_months']));
                if (is_array($sellers) && is_array($warranties)
                    && count($sellers) == count($warranties)) {
                    $i = 0;
                    $this->technicalEvaluationFactory->create()->deleteByQuote($quoteId, $websiteId);
                    foreach ($sellers as $seller) {
                        $model = $this->technicalEvaluationFactory->create();
                        $data['quote_id'] = $quoteId;
                        $data['website_id'] = $websiteId;
                        $data['seller_id'] = $seller;
                        $data['warranty_months'] = $warranties[$i];
                        $model->setData($data);
                        $entityId = $model->save()->getId();

                        $extraData['status'] = 'success';
                        $extraData['request'] = null;
                        $extraData['id_cotizacion'] = $entityId;
                        $extraData['response']['data'] = $model->getData();
                        $extraData['response']['post_info'] = $postData;
                        $this->helperActionLog->saveLoggin($actionName, $profileName, $extraData);
                    }

                    $this->helperEval->setEvaluationStepTwo($quoteId);
                    $this->helperEval->setSellersSelected($quoteId, $sellersTmp);
                }
            }

            $this->getDataPersistor()->clear($keyDataPersistor);
            return $this->_redirect(
                'evaluate/requestforquote/index',
                ['id' => $quoteId, 'step' => 3]
            );
        } catch (LocalizedException $e) {
            if ($e->getCode() != FormAttributesHelper::CUSTOM_ERROR_CODE) {
                $this->logger->error($e->getMessage());
            }

            // LOGS
            $extraData['status'] = $e->getMessage();
            $this->helperActionLog->saveLoggin($actionName, $profileName, $extraData);
            $this->messageManager->addError($e->getMessage());
            $this->getDataPersistor()->set($keyDataPersistor, $postData);
            $this->_redirect(
                'evaluate/requestforquote/index',
                ['id' => $quoteId, 'step' => 2]
            );
            return;
        } catch (\Exception $e) {
            if ($e->getCode() != FormAttributesHelper::CUSTOM_ERROR_CODE) {
                $this->logger->error($e->getMessage());
            }

            // LOGS
            $extraData['status'] = $e->getMessage();
            $this->helperActionLog->saveLoggin($actionName, $profileName, $extraData);
            $this->messageManager->addError($e->getMessage());
            $this->getDataPersistor()->set($keyDataPersistor, $postData);
            $this->_redirect(
                'evaluate/requestforquote/index',
                ['id' => $quoteId, 'step' => 2]
            );
            return;
        }
    }

    /**
     * Get Data Persistor
     *
     * @return Magento\Framework\App\Request\DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }
}
