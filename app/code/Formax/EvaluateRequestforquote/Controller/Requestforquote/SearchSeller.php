<?php

namespace Formax\EvaluateRequestforquote\Controller\Requestforquote;

use Magento\Customer\Controller\AbstractAccount;

class SearchSeller extends AbstractAccount
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $quoteId = (int)$this->getRequest()->getParam('quote-id-search-sellers', 0);
        $q = $this->getRequest()->getParam('q');
        $qParam = $q ? $q : '';

        return $this->_redirect(
            'evaluate/requestforquote/index',
            ['id' => $quoteId, 'step' => 2, 'q' => $qParam]
        );
    }
}