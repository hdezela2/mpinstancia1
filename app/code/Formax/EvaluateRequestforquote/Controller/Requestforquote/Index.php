<?php

namespace Formax\EvaluateRequestforquote\Controller\Requestforquote;

use Formax\Offer\Helper\Data as HelperOffer;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Controller\AbstractAccount;
use Formax\EvaluateRequestforquote\Helper\Data;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\Requestforquote\Model\QuoteFactory;
use Formax\ConfigCmSoftware\Helper\Data as HelperCmSoftware;
use Formax\QuotesSupplier\Helper\Data as HelperQuoteSupplier;
use Formax\ActionLogs\Helper\Data as HelperActionLog;

class Index extends AbstractAccount
{
    /**
     * @var Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Formax\EvaluateRequestforquote\Helper\Data
     */
    protected $helper;

    /**
     * @var Formax\ActionLogs\Helper\Data
     */
    protected $helperActionLog;

    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $helperCmSoftware;

    /**
     * @var Webkul\Requestforquote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var Formax\QuotesSupplier\Helper\Data
     */
    protected $helperQuoteSupplier;

    /** @var DateTime */
    protected $dateTime;

    /** @var CustomerSession $session, */
    protected $customerSession;

    /** @var HelperOffer */
    protected $helperOffer;

    /** @var CustomerRepositoryInterface */
    protected $customerRepositoryInterface;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Data $helper,
        HelperCmSoftware $helperCmSoftware,
        QuoteFactory $quoteFactory,
        HelperActionLog $helperActionLog,
        HelperQuoteSupplier $helperQuoteSupplier,
        CustomerSession $session,
        HelperOffer $helperOffer,
        CustomerRepositoryInterface $customerRepositoryInterface,
        DateTime $dateTime
    ) {
        $this->helper = $helper;
        $this->quoteFactory = $quoteFactory;
        $this->helperCmSoftware = $helperCmSoftware;
        $this->resultPageFactory = $resultPageFactory;
        $this->helperActionLog = $helperActionLog;
        $this->helperQuoteSupplier = $helperQuoteSupplier;
        $this->dateTime = $dateTime;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->customerSession = $session;
        $this->helperOffer = $helperOffer;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $step = (int)$this->getRequest()->getParam('step', 0);
        $quoteId = (int)$this->getRequest()->getParam('id', 0);
        $quoteModel = $this->helper->getQuoteById($quoteId);
        $extraData = [
            'action' => 'Evaluación de Cotización - Paso 1',
            'status' => 'success'
        ];
        $actionName = 'Evaluación Paso 1';
        $profileName = 'Cliente';

        if (!$quoteModel->getId()) {
            $this->messageManager->addError(__("Quote #%1 doesn't exist.", $quoteId));
            return $this->_redirect('requestforquote/account/lists/');
        } else {
            if ($quoteModel->getApprovedSellerQuoteId() > 0) {
                $this->messageManager->addNotice(__('Quote #%1 has already been evaluated.', $quoteId));
                return $this->_redirect('requestforquote/account/lists/');
            }
            if ($this->helperCmSoftware->checkIfDateExpired($quoteModel->getEvaluationEnd())) {
                $this->messageManager->addNotice(__(
                        'Information: The evaluation period for quote #%1 expired on %2',
                        $quoteId,
                        $quoteModel->getEvaluationEnd())
                    );
                //return $this->_redirect('requestforquote/account/lists/');
            }
        }

        if ($step == 1) {
            $this->helper->setEvaluationStepOne($quoteId);
//            if ($this->helperQuoteSupplier->getNumberSellersresponded($quoteId) > 0) {

//                $customerId = $this->getCustomerLoggedId();
//                $customer = $this->customerRepositoryInterface->getById($customerId);
//                $token = $customer->getCustomAttribute('user_rest_atk') ? $customer->getCustomAttribute('user_rest_atk')->getValue() : '';
//
//                $quote = $this->quoteFactory->create()->load($quoteId);
//                $currentDate = new \DateTime($this->dateTime->gmtDate('d-m-Y'));
//                $evaluationTerm = $quote->getEvaluationTerm();
//                $newEvaluationEnd = null;
//
//                $response = $this->helperOffer->getEndBusinessDate($token, $currentDate->format('d-m-Y'), $evaluationTerm);
//                if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
//                    $date = $response['payload']['fecha_termino'];
//                    $newEvaluationEnd = new \DateTime($date);
//                } else {
//                    $this->helperActionLog->saveLoggin($actionName,$profileName,$extraData);
//                    throw new LocalizedException(__('An error occured trying to get the evaluation date.'));
//                }
//
//                //If EvaluationEndDate is monday it must be replaced for the latest saturday
//                if ($newEvaluationEnd->format('N') == "1"){
//                    $newEvaluationEnd = $newEvaluationEnd->sub(new \DateInterval('P2D'));
//                }

//                $this->quoteFactory->create()
//                    ->load($quoteId)
//                    ->setStatus(HelperQuoteSupplier::EVALUATION)
//                    ->setEvaluationStart($currentDate)
//                    ->setEvaluationEnd($newEvaluationEnd)
//                    ->save();
//            }

            $extraData['request'] = null;
            $extraData['id_cotizacion'] = $quoteId;
            $extraData['response'] = null;
            $this->helperActionLog->saveLoggin($actionName, $profileName, $extraData);
        } else if ($step == 2) {
            if (!$this->helper->isStepOneVisited($quoteId)) {
                $this->messageManager->addNotice(__('You must review step %1 before.', __('one')));
                return $this->_redirect(
                    'evaluate/requestforquote/index',
                    ['id' => $quoteId, 'step' => 1]
                );
            } else {
                $resultPage->addHandle('evaluate_requestforquote_index_step2');
            }
        } else if ($step == 3) {
            if (!$this->helper->isStepTwoVisited($quoteId) || !$this->helper->getSellersSelected($quoteId)) {
                $this->messageManager->addNotice(__('You must review step %1 before.', __('two')));
                return $this->_redirect(
                    'evaluate/requestforquote/index',
                    ['id' => $quoteId, 'step' => 2]
                );
            } else {
                $resultPage->addHandle('evaluate_requestforquote_index_step3');
            }
        }

        return $resultPage;
    }

    protected function getCustomerLoggedId()
    {
        $customerId = 0;
        if ($this->customerSession->isLoggedIn()) {
            $customerId = (int) $this->customerSession->getCustomerId();
        }
        return $customerId;
    }
}