<?php
namespace Formax\EvaluateRequestforquote\Controller\Requestforquote;

use Cleverit\Requestforquote\Helper\Data as RequestforquoteHelper;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Action\Context;
use Formax\CreateFormAttributes\Helper\Data as FormAttributesHelper;
use Formax\QuotesSupplier\Helper\Data as QuotesSupplierHelper;
use Formax\EvaluateRequestforquote\Model\RejectedQuoteInfoFactory;
use Formax\EvaluateRequestforquote\Model\Repository\RejectedQuoteInfoRepository;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\StoreManagerInterface;

class RejectAction extends AbstractAccount
{
    /** @var FormAttributesHelper */
    protected $_formAttributesHelper;

    /** @var RequestforquoteHelper */
    protected $_rfqHelper;

    /** @var RejectedQuoteInfoFactory */
    protected $_rejectedQuoteInfoFactory;

    /** @var RejectedQuoteInfoRepository */
    protected $_rejectedQuoteInfoRepository;

    /** @var StoreManagerInterface */
    protected $_storeManager;

    public function __construct(
        Context $context,
        FormAttributesHelper $formAttributesHelper,
        RequestforquoteHelper $rfqHelper,
        RejectedQuoteInfoFactory $rejectedQuoteInfo,
        RejectedQuoteInfoRepository $rejectedQuoteInfoRepository,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_formAttributesHelper = $formAttributesHelper;
        $this->_rfqHelper = $rfqHelper;
        $this->_rejectedQuoteInfoFactory = $rejectedQuoteInfo;
        $this->_rejectedQuoteInfoRepository = $rejectedQuoteInfoRepository;
        $this->_storeManager = $storeManager;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $filesData = $this->getRequest()->getFiles();
        $entity = 'requestforquote_quote_reject';
        $data = $this->_formAttributesHelper->parsePostData($entity, $params, $filesData);
        $quoteId = $this->getRequest()->getParam('quote_id');
        try {
            if (isset($data['error']) && isset($data['msg']) && $data['error']) {
                throw new \Exception($data['msg'], FormAttributesHelper::CUSTOM_ERROR_CODE);
            }
            $validateData = $this->_formAttributesHelper->validatePostData($entity, $params);
            if (isset($validateData['error']) && $validateData['error']) {
                $message = isset($validateData['msg']) ? $validateData['msg'] : __('An error occurred trying to reject quote.');
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }
            $updateData = [];
            if ($this->getRequest()->getParam('quote_id')) {
                $rejectQuoteInfo = $this->_rejectedQuoteInfoRepository->getByQuoteId(
                    (int)$this->getRequest()->getParam('quote_id')
                );
                $updateData['id'] = $rejectQuoteInfo->getId();
            } else {
                $rejectQuoteInfo = $this->_rejectedQuoteInfoFactory->create();
                unset($params['quote_id']);
            }
            $updateData['quote_id'] = $quoteId;
            $updateData['website_id'] = $this->_storeManager->getStore()->getWebsiteId();
            if (isset($data['justification'])) {
                $updateData['justification'] = $data['justification'];
            }
            if (isset($data['rejected_report_documents'])) {
                $updateData['rejected_report_documents'] = $data['rejected_report_documents'];
            }
            if (isset($params['__remove__rejected_report_documents'])) {
                $updateData['rejected_report_documents'] = $data['rejected_report_documents'];
            }
            $rejectQuoteInfo->setData($updateData);
            $this->_rejectedQuoteInfoRepository->save($rejectQuoteInfo);
            $this->_rfqHelper->updateQuoteStatus($quoteId, QuotesSupplierHelper::REJECTED);
            $this->messageManager->addSuccessMessage(__('Quote ID %1 was successfully rejected.', $quoteId));
            $this->_redirect('requestforquote/account/lists');
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());

            $this->_redirect('requestforquote/account/lists');
        }
    }
}
