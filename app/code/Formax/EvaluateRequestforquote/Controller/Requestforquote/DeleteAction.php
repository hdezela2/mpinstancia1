<?php
namespace Formax\EvaluateRequestforquote\Controller\Requestforquote;

use Cleverit\Requestforquote\Helper\Data as RequestforquoteHelper;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Action\Context;
use Formax\CreateFormAttributes\Helper\Data as FormAttributesHelper;
use Formax\QuotesSupplier\Helper\Data as QuotesSupplierHelper;
use Formax\EvaluateRequestforquote\Model\DeletedQuoteInfoFactory;
use Formax\EvaluateRequestforquote\Model\Repository\DeletedQuoteInfoRepository;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\StoreManagerInterface;

class DeleteAction extends AbstractAccount
{
    /** @var FormAttributesHelper */
    protected $_formAttributesHelper;

    /** @var RequestforquoteHelper */
    protected $_rfqHelper;

    /** @var DeletedQuoteInfoFactory */
    protected $_deletedQuoteInfoFactory;

    /** @var DeletedQuoteInfoRepository */
    protected $_deletedQuoteInfoRepository;

    /** @var StoreManagerInterface */
    protected $_storeManager;

    public function __construct(
        Context $context,
        FormAttributesHelper $formAttributesHelper,
        RequestforquoteHelper $rfqHelper,
        DeletedQuoteInfoFactory $deletedQuoteInfo,
        DeletedQuoteInfoRepository $deletedQuoteInfoRepository,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_formAttributesHelper = $formAttributesHelper;
        $this->_rfqHelper = $rfqHelper;
        $this->_deletedQuoteInfoFactory = $deletedQuoteInfo;
        $this->_deletedQuoteInfoRepository = $deletedQuoteInfoRepository;
        $this->_storeManager = $storeManager;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $filesData = $this->getRequest()->getFiles();
        $entity = 'requestforquote_quote_delete';
        $data = $this->_formAttributesHelper->parsePostData($entity, $params, $filesData);
        $quoteId = $this->getRequest()->getParam('quote_id');
        try {
            if (isset($data['error']) && isset($data['msg']) && $data['error']) {
                throw new \Exception($data['msg'], FormAttributesHelper::CUSTOM_ERROR_CODE);
            }
            $validateData = $this->_formAttributesHelper->validatePostData($entity, $params);
            if (isset($validateData['error']) && $validateData['error']) {
                $message = isset($validateData['msg']) ? $validateData['msg'] : __('An error occurred trying to delete quote.');
                throw new \Exception($message, FormAttributesHelper::CUSTOM_ERROR_CODE);
            }
            $updateData = [];
            if ($this->getRequest()->getParam('quote_id')) {
                $deleteQuoteInfo = $this->_deletedQuoteInfoRepository->getByQuoteId(
                    (int)$this->getRequest()->getParam('quote_id')
                );
                $updateData['id'] = $deleteQuoteInfo->getId();
            } else {
                $deleteQuoteInfo = $this->_deletedQuoteInfoFactory->create();
                unset($params['quote_id']);
            }
            $updateData['quote_id'] = $quoteId;
            $updateData['website_id'] = $this->_storeManager->getStore()->getWebsiteId();
            if (isset($data['justification'])) {
                $updateData['justification'] = $data['justification'];
            }
            if (isset($data['deleted_report_documents'])) {
                $updateData['deleted_report_documents'] = $data['deleted_report_documents'];
            }
            if (isset($params['__remove__deleted_report_documents'])) {
                $updateData['deleted_report_documents'] = $data['deleted_report_documents'];
            }
            $deleteQuoteInfo->setData($updateData);
            $this->_deletedQuoteInfoRepository->save($deleteQuoteInfo);
            $this->_rfqHelper->updateQuoteStatus($quoteId, QuotesSupplierHelper::DELETED);
            $this->messageManager->addSuccessMessage(__('Quote ID %1 was successfully deleted.', $quoteId));
            $this->_redirect('requestforquote/account/lists');
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());

            $this->_redirect('requestforquote/account/lists');
        }
    }
}
