<?php

namespace Formax\EvaluateRequestforquote\Controller\Requestforquote;

use Formax\PublicQuote\Extend\DCCPPDF;
use Formax\PublicQuote\Helper\Data as PublicQuoteHelper;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory as CollectionQuoteFactory;
use Formax\ActionLogs\Helper\Data as HelperActionLog;
use Magento\Framework\Filesystem;
use Formax\EvaluateRequestforquote\Helper\Data as HelperEvaluate;


class Download extends AbstractAccount
{
    /**
     * @var Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    /**
     * @var Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directory;

    /**
     * @var Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $eavAttribute;

    /**
     * @var Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionQuoteFactory
     */
    protected $collectionQuoteFactory;

    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $helperSoftware;

    /**
     * @var \Formax\ActionLogs\Helper\Data
     */
    protected $helperActionLog;

    /** @var \Formax\PublicQuote\Helper\Data */
    protected $_publicQuoteHelper;

    /**
     * @var array
     */
    protected $includeColumns = [
        'requestquote_id' => ['label' => 'Número Cotización'],
        'description' => ['label' => 'Nombre cotización cliente'],
        'subject' => ['label' => 'Categoría producto'],
        'status' => ['label' => 'Estado'],
        'beneficiary' => ['label' => 'Beneficiario'],
        'compliance_guarantee' => ['label' => 'Garantía de cumplimiento', 'format' => 'percent'],
        'amount' => ['label' => 'Presupuesto máximo', 'format' => 'currency'],
        'award_date' => ['label' => 'Fecha en que se adjudicó', 'format' => 'date'],
        'email' => ['label' => 'Correo Electrónico'],
        'deliverable_services' => ['label' => 'Servicios y/o productos entregables', 'format' => 'serialize'],
        'execution_services_term_number' => ['label' => 'Plazo máximo disponible para la ejecución de los servicios (numero)'],
        'execution_services_term_text' => ['label' => 'Plazo máximo disponible para la ejecución de los servicios (texto)'],
        'evaluation_start' => ['label' => 'Inicio Evaluación'],
        'evaluation_term' => ['label' => 'Plazo de evaluación (Días)'],
        'evaluation_end' => ['label' => 'Fecha límite de evaluación', 'format' => 'date'],
        'file' => ['label' => 'Documentos Cotizacion', 'format' => 'serialize'],
        'match_criterias' => ['label' => 'Criterios de desempate'],
        'minimum_offer_required' => ['label' => 'Cumplimiento de requerimientos técnicos mínimos', 'format' => 'serialize'],
        'organization' => ['label' => 'Organismo'],
        'organization_unit' => ['label' => 'Unidad de compra'],
        'publication_start' => ['label' => 'Inicio Publicación'],
        'publication_term' => ['label' => 'Plazo de publicación (Días)'],
        'publication_end' => ['label' => 'Fecha límite publicación', 'format' => 'date'],
        'question_end' => ['label' => 'Fecha límite para hacer preguntas', 'format' => 'date'],
        'recruitment_target' => ['label' => 'Objetivo de contratación'],
        'especific_requirement' => ['label' => 'Requerimientos específicos'],
        'engagement_scope' => ['label' => 'Alcance de la contratación'],
        'make_question_term' => ['label' => 'Plazo para realizar preguntas (Días)'],
        'tic' => ['label' => 'Código comité TIC (Si tiene)'],
        'sku' => ['label' => 'SKU producto'],
        'product_name' => ['label' => 'Nombre producto'],
        'rut' => ['label' => 'Rut proveedor'],
        'seller_name' => ['label' => 'Nombre proveedor'],
        'quote_name' => ['label' => 'Nombre cotización proveedor'],
        'quote_detalis' => ['label' => 'Detalle de la oferta'],
        'evaluation_modality' => ['label' => 'Modalidad de evaluación'],
        'table_requirement' => ['label' => 'Cumplimiento de requerimientos técnicos', 'format' => 'serialize'],
        'duracion_number' => ['label' => 'Duración estimada para servicios', 'format' => 'concat', 'concatWith' => 'duracion_text'],
        'teamwork' => ['label' => 'Equipo de trabajo', 'format' => 'serialize'],
        'offer_document' => ['label' => 'Documentos', 'format' => 'serialize'],
        'created_at' => ['label' => 'Fecha de creación oferta técnica', 'format' => 'date'],
        'quote_price' => ['label' => 'Valor cotización', 'format' => 'currency'],
        'approved_seller_quote_id' => ['label' => 'ID Cotización Aprovada'],
        'entity_id' => ['label' => 'ID Cotización Proveedor'],
        'offer_price' =>['label' => 'Valor de Oferta','format'=>'currency'],
        'score' => ['label' => 'Puntaje mínimo oferta', 'format' => 'percent'],
    ];


    protected $helperEvaluate;

    /**
     * Initialize dependencies
     *
     * @param Magento\Framework\App\Action\Context $context
     * @param Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param Magento\Framework\Filesystem $filesystem
     * @param Formax\ConfigCmSoftware\Helper\Data $helperSoftware
     * @param \Formax\ActionLogs\Helper\Data $helperActionLog
     * @param Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionQuoteFactory $collectionQuoteFactory
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        EavAttribute $eavAttribute,
        Filesystem $filesystem,
        HelperSoftware $helperSoftware,
        HelperActionLog $helperActionLog,
        CollectionQuoteFactory $collectionQuoteFactory,
        PublicQuoteHelper $publicQuoteHelper,
        HelperEvaluate $helperEvaluate
    ) {
        $this->fileFactory = $fileFactory;
        $this->eavAttribute = $eavAttribute;
        $this->helperSoftware = $helperSoftware;
        $this->helperActionLog = $helperActionLog;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->collectionQuoteFactory = $collectionQuoteFactory;
        $this->_publicQuoteHelper = $publicQuoteHelper;
        $this->helperEvaluate = $helperEvaluate;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $post = $this->getRequest()->getPost();

        if (isset($post) && (count($post) > 0) && isset($post['bulkaction'])) {
            $quoteId = isset($post['quote_id'][0]) ? $post['quote_id'][0] : 0;
            $sellerIds = [];

            if (isset($post['seller_id']) && is_array($post['seller_id'])) {
                foreach ($post['seller_id'] as $sellerId) {
                    $sellerIds[] = $sellerId;
                }
            }
        } else {
            $sellerIds[] = (int)$this->getRequest()->getParam('seller_id', 0);
            $quoteId = (int)$this->getRequest()->getParam('quote_id', 0);
        }

        $extraData = [
            'action' => 'Descarga documentos oferta técnica proveedor',
            'status' => 'success'
        ];
        $actionName = 'Evaluación descarga de oferta técnica proveedor';
        $profileName = 'Cliente';
        $extraData['request'] = json_encode(['IDs_proveedores' => $sellerIds]);
        $extraData['id_cotizacion'] = $quoteId;
        $extraData['response'] = null;
        $this->helperActionLog->saveLoggin($actionName, $profileName, $extraData);

        if (count($sellerIds) == 0) {
            $this->messageManager->addNotice(__('You must select a seller.'));
            return $this->_redirect('evaluate/requestforquote/index', ['id' => $quoteId, 'step' => 1]);
        } else {
            return $this->createFileCsv($quoteId, $sellerIds);
        }
    }

    /**
     * Create bulk CSV file
     *
     * @param int $quoteId
     * @param array $sellerIds
     * @return \Magento\Framework\App\Response\Http\FileFactory
     */
    public function createFileCsv($quoteId, $sellerIds)
    {
        /** Create zip file */
        $zip = new \ZipArchive;
        $fileDriver = $this->helperSoftware->getFileDriver();
        $zipName = 'ofertas_tecnicas_' . $quoteId . '.zip';
        $path = 'chilecompra/files/formattributes';
        $pathZip = 'export/' . $zipName;
        $destinationZip = $this->helperSoftware->getPathVar() . $pathZip;
        if (!$zip->open($destinationZip, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)) {
            $this->messageManager->addError(__('Zip Archive Could not create'));
            $this->_redirect('evaluate/requestforquote/index', ['id' => $quoteId]);
        }

        /* Open file */
        //$csvName = date('Ymd_Hi');
        $pdfName = date('Ymd_Hi');
        //$csvFilePath = 'export/custom' . $csvName . '.csv';
        $pdfFilePath = 'export/custom' . $pdfName . '.pdf';
        $this->directory->create('export');
        $data = [];
        $collection = $this->getCollection($quoteId, $sellerIds);

        //$csvStream = $this->directory->openFile($csvFilePath, 'w+');
        $pdfStream = $this->directory->openFile($pdfFilePath, 'w+');
        //$csvStream->lock();
        $pdfStream->lock();

        $quote_ =   $this->helperEvaluate->getSellersSelectedCollection($quoteId,true,1,5);

        /* Write Header */
        //$csvStream->writeCsv($header);

        /* Create PDF File */
        /** @var DCCPPDF $pdf */
        $pdf = $this->_publicQuoteHelper->createPdfFile();
        $firstPageCreated = false;
        foreach ($collection as $item) {
            foreach ($this->includeColumns as $key => $value) {
                $concatValue = isset($value['concatWith']) && $item->getData($value['concatWith']) ? $item->getData($value['concatWith']) : '';
                $data[$key] = $this->formatValue($key, $item->getData($key), $concatValue);
            }

            //$csvStream->writeCsv($data);
            if (!$firstPageCreated) {
                $this->_publicQuoteHelper->addQuoteInformationToPdf($pdf, $data);
                $firstPageCreated = true;
            }
            $this->_publicQuoteHelper->addPdfSellerInformation($pdf, $data);

            /** Attach documents uploaded by seller to zip */
            $createNewFolder = true;
            $newFolder = 'Documentos Adjuntos - ' . str_replace('.', '', $item->getSellerName());
            if (isset($data['offer_document']) && $item->getData('offer_document')) {
                $this->addFileToZip($item->getData('offer_document'), $newFolder, $zip);
                $createNewFolder = false;
            }

            /** Files evaluation modality = 2 */
            if ($this->filterText($item->getData('evaluation_modality')) == 'evaluacion_de_criterios_tecnicos_adicionales') {
                $newSerialized = $this->helperSoftware->formatSerializeData($item->getData('table_requirement'));
                $filesSerialized = $this->helperSoftware->getJsonSerialized()->unserialize($newSerialized);
                if (is_array($filesSerialized)) {
                    $k = 0;
                    foreach ($filesSerialized as $keyLastFile => $itemFile) {
                        if (isset($itemFile['criterio_file']) && $itemFile['criterio_file']) {
                            $fileSellerName = $this->helperSoftware->getPathMedia() . $path . $itemFile['criterio_file'];
                            if ($fileDriver->isExists($fileSellerName)) {
                                $onlyFileName = explode('/', $itemFile['criterio_file']);
                                $fileNameWithExtension = explode('.', $onlyFileName[count($onlyFileName)-1]);
                                $fileSellerNewName = isset($itemFile['criterio_label']) ? $this->filterText($itemFile['criterio_label']) : 'criterio_' . ($k+1);
                                $fileSellerNewName .= '.'  . $fileNameWithExtension[count($fileNameWithExtension)-1];

                                /** Create folder with seller RUT if doesn't exist */
                                if ($createNewFolder) {
                                    $zip->addEmptyDir($newFolder);
                                }
                                $zip->addFile($fileSellerName, $newFolder . '/' . $fileSellerNewName);
                            }
                            $k++;
                        }
                    }
                }
            }
        }
        $pdfStream->write($pdf->getPDFData());

        /** Attach csv and pdf created to zip */
        //$csvFileSellerName = $this->helperSoftware->getPathVar() . $csvFilePath;
        $pdfFileSellerName = $this->helperSoftware->getPathVar() . $pdfFilePath;
        //$csvFileSellerNewName = 'ofertas.csv';
        $pdfFileSellerNewName = 'ofertas.pdf';
        //$zip->addFile($csvFileSellerName, $csvFileSellerNewName);
        $zip->addFile($pdfFileSellerName, $pdfFileSellerNewName);
        $zip->close();

        /** Download zip file */
        $content['type'] = 'filename';
        $content['value'] = $pathZip;
        $content['rm'] = '1';
        return $this->fileFactory->create($zipName, $content, DirectoryList::VAR_DIR);
    }

    /**
     * Get quotes collection
     *
     * @param int $quoteId
     * @param array $sellerIds
     * @return array
     */
    public function getCollection($quoteId, $sellerIds)
    {
        $collection = [];
        if ((int)$quoteId > 0 && is_array($sellerIds)) {
            $collection = $this->collectionQuoteFactory->create();
            $resource = $collection->getResource();
            $quoteInfoTable = $resource->getTable('requestforquote_quote_info');
            $quoteTable = $resource->getTable('requestforquote_quote');
            $customerTable = $resource->getTable('customer_entity');
            $productTable = $resource->getTable('catalog_product_entity');
            $productVarcharTable = $resource->getTable('catalog_product_entity_varchar');
            $customerVarcharTable = $resource->getTable('customer_entity_varchar');
            $attrProductName = $this->eavAttribute->getIdByCode('catalog_product', 'name');
            $attrRut = $this->eavAttribute->getIdByCode('customer', 'wkv_dccp_rut');

            $collection->addFieldToFilter('q.entity_id', $quoteId)
                ->addFieldToFilter('edited', 1)
                ->addFieldToFilter('info.seller_id', ['in' => $sellerIds]);
            $collection->getSelect()->join(
                    ['info' => $quoteInfoTable],
                    'main_table.seller_quote_id = info.entity_id',
                    []
                )->joinLeft(
                    ['q' => $quoteTable],
                    'info.quote_id = q.entity_id',
                    [
                        'requestquote_id' => 'q.entity_id',
                        'description',
                        'subject',
                        'evaluation_modality' => new \Zend_Db_Expr('IF(evaluation_modality = 1, "Cumplimiento de requerimientos técnicos mínimos", "Evaluación de criterios técnicos adicionales")'),
                        'status',
                        'beneficiary',
                        'compliance_guarantee',
                        'created_at',
                        'award_date',
                        'deliverable_services',
                        'email',
                        'execution_services_term_number',
                        'execution_services_term_text',
                        'evaluation_start',
                        'evaluation_end',
                        'evaluation_term',
                        'file',
                        'match_criterias',
                        'minimum_offer_required',
                        'organization',
                        'publication_start',
                        'publication_end',
                        'publication_term',
                        'question_end',
                        'make_question_term',
                        'organization',
                        'organization_unit',
                        'recruitment_target',
                        'especific_requirement',
                        'engagement_scope',
                        'tic',
                        'amount',
                        'approved_seller_quote_id',
                        'score'
                    ]
                )->joinLeft(
                    ['c' => $customerTable],
                    'info.seller_id = c.entity_id',
                    ['seller_name' => 'c.firstname']
                )->joinLeft(
                    ['r' => $customerVarcharTable],
                    'c.entity_id = r.entity_id AND r.attribute_id = ' . $attrRut,
                    ['rut' => 'r.value']
                )->joinLeft(
                    ['e' => $productTable],
                    'q.product_id = e.entity_id',
                    ['sku']
                )->joinLeft(
                    ['n' => $productVarcharTable],
                    'q.product_row_id = n.row_id AND n.store_id = 0 AND n.attribute_id = ' . $attrProductName,
                    ['product_name' => 'n.value']
                );
        }

        return $collection;
    }

    /**
     * Get value formated
     *
     * @param string $columnKey
     * @param string $value
     * @param string $concatWith [optional]
     * @return string
     */
    public function formatValue($columnKey, $value, $concatWith = '')
    {
        $formatedValue = $value;
        if (isset($this->includeColumns[$columnKey]['format'])) {
            switch ($this->includeColumns[$columnKey]['format']) {
                case 'date':
                    $formatedValue = $this->helperSoftware->formatDateTime($value);
                    break;
                case 'datetime':
                    $formatedValue = $this->helperSoftware->formatDateToDateTime($value);
                    break;
                case 'currency':
                    $formatedValue = $this->helperSoftware->currencyFormat($value);
                    break;
                case 'number':
                    $formatedValue = $this->helperSoftware->numberFormat($value);
                    break;
                case 'percent':
                    $formatedValue = number_format($value, 1, '.', ',');
                    break;
                case 'concat':
                    if (isset($this->includeColumns[$columnKey]['concatWith'])
                        && $this->includeColumns[$columnKey]['concatWith'] == 'duracion_text') {
                        $formatedValue = $value . ' ' . $this->helperSoftware->getTextServiceTerm($concatWith);
                    } else {
                        $formatedValue = $value . ' ' . $concatWith;
                    }
                    break;
                case 'link':
                    $path = 'chilecompra/files/formattributes';
                    $formatedValue = '';
                    if ($value) {
                        $filename = $this->helperSoftware->getPathMedia() . $path . $value;
                        if ($this->helperSoftware->getFileDriver()->isExists($filename)) {
                            $formatedValue = $this->helperSoftware->getUrlMedia() . $path .  $value;
                        }
                    }
                    break;
                case 'serialize':
                    if ($value) {
                        $columnKeySend = $columnKey =='table_requirement' ? $columnKey : null;
                        $formatedValue = $this->helperSoftware->formatSerializeData($value,$columnKeySend);
                    }
                    break;
                default:
                    break;
            }
        }

        return $formatedValue;
    }

    /**
     * Get value of text filter only by letter
     *
     * @return string
     */
    private function filterText($text)
    {
        if ($text) {
            return str_replace(['á', 'é', 'í', 'ó', 'ú', ' '], ['a', 'e', 'i', 'o', 'u', '_'], strtolower(trim($text)));
        }

        return '';
    }

    /**
     *
     * @param string $columnData
     * @param string $fileType
     * @param \ZipArchive $zip
     * @return void
     */
    protected function addFileToZip($columnData, $filteType, $zip)
    {
        if ($columnData !== null && !empty($columnData) && !empty($filteType) && $zip) {
            $newSerialized = $this->helperSoftware->formatSerializeData($columnData);
            $filesSerialized = $this->helperSoftware->getJsonSerialized()->unserialize($newSerialized);
            $fileDriver = $this->helperSoftware->getFileDriver();
            $path = 'chilecompra/files/formattributes';
            if (is_array($filesSerialized)) {
                foreach ($filesSerialized as $itemFile) {
                    $fileName = $this->helperSoftware->getPathMedia() . $path . $itemFile;
                    if ($fileDriver->isExists($fileName)) {
                        /** Create folder with file type */
                        $onlyFileName = explode('/', $itemFile);
                        $fileNewName = $onlyFileName[count($onlyFileName)-1];
                        $zip->addEmptyDir($filteType);
                        $zip->addFile($fileName, $filteType . '/' . $fileNewName);
                    }
                }
            }
        }
    }
}
