<?php

namespace Formax\EvaluateRequestforquote\Controller\Requestforquote;

use Cleverit\Requestforquote\Helper\Data as RfqHelper;
use Formax\QuotesSupplier\Helper\Data as QsHelper;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Action\Context;

class Desert extends AbstractAccount
{
    /** @var RfqHelper */
    protected $_helper;

    public function __construct(
        Context $context,
        RfqHelper $helper
    ) {
        parent::__construct($context);
        $this->_helper = $helper;
    }

    public function execute()
    {
        $quoteId = (int)$this->getRequest()->getParam('quote_id', 0);
        if (!$quoteId) {
            $this->messageManager->addError(__("Quote #%1 doesn't exist.", $quoteId));
            return $this->_redirect('requestforquote/account/lists/');
        }
        $this->_helper->updateQuoteStatus($quoteId, QsHelper::DESERT);
        return $this->_redirect('requestforquote/account/lists/');
    }
}
