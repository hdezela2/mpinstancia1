<?php

namespace Formax\EvaluateRequestforquote\Plugin\Controller\Account;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\UrlInterface;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Magento\Framework\App\Request\Http as HttpRequest;
use Webkul\Requestforquote\Controller\Account\QuoteList as WebkulQuoteList;

/**
 * QuoteList class
 */
class QuoteList
{
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var Magento\Framework\App\Request\Http
     */
    private $request;

    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    private $helperSoftware;

    /**
     * @var Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Framework\App\Request\Http $request
     * @param Formax\ConfigCmSoftware\Helper\Data $helperSoftware
     */
    public function __construct(
        RedirectFactory $redirectFactory,
        UrlInterface $url,
        HttpRequest $request,
        HelperSoftware $helperSoftware
    ) {
        $this->redirectFactory = $redirectFactory;
        $this->url = $url;
        $this->request = $request;
        $this->helperSoftware = $helperSoftware;
    }

    /**
     * Plugin before execute method
     *
     * @param Webkul\Requestforquote\Controller\Account\QuoteList $subject
     * @return \Magento\Framework\View\Result\Page
     */
    public function afterExecute(WebkulQuoteList $subject, $result)
    {
        if ($this->helperSoftware->getCurrentStoreCode() == HelperSoftware::SOFTWARE_STOREVIEW_CODE  || $this->helperSoftware->getCurrentStoreCode() == SoftwareRenewalConstants::STORE_CODE) {
            $redirectFactory = $this->redirectFactory->create();
            $id = $this->request->getParam('id', 0);
            $url = $id > 0 ? $this->url->getUrl('evaluate/requestforquote/index', ['id' => $id])
                : $this->url->getUrl('requestforquote/account/lists');
            $redirectFactory->setUrl($url);
            return $redirectFactory;
        }

        return $result;
    }
}
