<?php
namespace Formax\EvaluateRequestforquote\Api;

use Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoInterface;
use Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface RejectedQuoteInfoRepositoryInterface
{
    /**
     * @param RejectedQuoteInfoInterface $rejectedQuoteInfo
     * @param int|null $storeId
     * @return void
     */
    public function save(RejectedQuoteInfoInterface $rejectedQuoteInfo, ?int $storeId = 0);

    /**
     * @param $rejectedQuoteInfoId
     * @return \Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoInterface
     */
    public function getById($rejectedQuoteInfoId);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return \Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param RejectedQuoteInfoInterface $rejectedQuoteInfo
     * @return bool
     */
    public function delete(RejectedQuoteInfoInterface $rejectedQuoteInfo);

    /**
     * @param $rejectedQuoteInfoId
     * @return void
     */
    public function deleteById($rejectedQuoteInfoId);
}
