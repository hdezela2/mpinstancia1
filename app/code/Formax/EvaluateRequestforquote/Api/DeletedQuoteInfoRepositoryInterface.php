<?php
namespace Formax\EvaluateRequestforquote\Api;

use Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoInterface;
use Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface DeletedQuoteInfoRepositoryInterface
{
    /**
     * @param DeletedQuoteInfoInterface $deletedQuoteInfo
     * @param int|null $storeId
     * @return void
     */
    public function save(DeletedQuoteInfoInterface $deletedQuoteInfo, ?int $storeId = 0);

    /**
     * @param $deletedQuoteInfoId
     * @return \Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoInterface
     */
    public function getById($deletedQuoteInfoId);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return \Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param DeletedQuoteInfoInterface $deletedQuoteInfo
     * @return bool
     */
    public function delete(DeletedQuoteInfoInterface $deletedQuoteInfo);

    /**
     * @param $deletedQuoteInfoId
     * @return void
     */
    public function deleteById($deletedQuoteInfoId);
}
