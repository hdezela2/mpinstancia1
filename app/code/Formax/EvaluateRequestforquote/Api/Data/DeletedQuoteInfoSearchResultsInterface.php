<?php
namespace Formax\EvaluateRequestforquote\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface DeletedQuoteInfoSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoInterface[]
     */
    public function getItems();

    /**
     * @param \Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoInterface[] $items
     * @return \Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoSearchResultsInterface
     */
    public function setItems(array $items);
}