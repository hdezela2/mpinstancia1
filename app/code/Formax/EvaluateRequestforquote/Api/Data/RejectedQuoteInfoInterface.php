<?php

namespace Formax\EvaluateRequestforquote\Api\Data;

interface RejectedQuoteInfoInterface
{
    /**
     * @param $id
     */
    public function setId($id);

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param int $quoteId
     */
    public function setQuoteId(int $quoteId);

    /**
     * @return int|null
     */
    public function getQuoteId();

    /**
     * @param int $websiteId
     */
    public function setWebsiteId(int $websiteId);

    /**
     * @return int|null
     */
    public function getWebsiteId();

    /**
     * @param string $justification
     */
    public function setJustification(string $justification);

    /**
     * @return string
     */
    public function getJustification();

    /**
     * @param string $rejectedReportDocuments
     */
    public function setRejectedReportDocuments(string $rejectedReportDocuments);

    /**
     * @return string|null
     */
    public function getRejectedReportDocuments();

    /**
     * @param \DateTime $value
     */
    public function setCreatedAt(\DateTime $value);

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt();

    /**
     * @param \DateTime $value
     */
    public function setUpdatedAt(\DateTime $value);

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt();
}