<?php
namespace Formax\EvaluateRequestforquote\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface RejectedQuoteInfoSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoInterface[]
     */
    public function getItems();

    /**
     * @param \Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoInterface[] $items
     * @return \Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoSearchResultsInterface
     */
    public function setItems(array $items);
}