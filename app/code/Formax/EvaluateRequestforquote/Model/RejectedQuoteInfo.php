<?php
namespace Formax\EvaluateRequestforquote\Model;

use Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoInterface;
use Formax\EvaluateRequestforquote\Model\ResourceModel\RejectedQuoteInfo as RejectedQuoteInfoResource;
use Magento\Framework\Model\AbstractModel;

class RejectedQuoteInfo extends AbstractModel implements RejectedQuoteInfoInterface
{
    protected $_eventPrefix = 'rejectedquoteinfo';

    protected function _construct()
    {
        $this->_init(RejectedQuoteInfoResource::class);
    }

    public function setQuoteId(int $value)
    {
        $this->setData('quote_id', $value);
    }

    public function getQuoteId()
    {
        return (int)$this->getData('quote_id');
    }

    public function setWebsiteId(int $value)
    {
        $this->setData('website_id', $value);
    }

    public function getWebsiteId()
    {
        return (int)$this->getData('website_id');
    }

    public function setJustification(string $value)
    {
        $this->setData('justification', $value);
    }

    public function getJustification()
    {
        return (string)$this->getData('justification');
    }

    public function setRejectedReportDocuments(string $value)
    {
        $this->setData('rejected_report_documents', $value);
    }

    public function getRejectedReportDocuments()
    {
        return (string)$this->getData('rejected_report_documents');
    }

    public function setCreatedAt(\DateTime $value)
    {
        $this->setData('created_at', $value);
    }

    public function getCreatedAt()
    {
        return new \DateTime($this->getData('created_at'));
    }

    public function setUpdatedAt(\DateTime $value)
    {
        $this->setData('updated_at', $value);
    }

    public function getUpdatedAt()
    {
        return new \DateTime($this->getData('updated_at'));
    }
}