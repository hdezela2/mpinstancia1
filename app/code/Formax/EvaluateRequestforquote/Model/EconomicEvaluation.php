<?php

namespace Formax\EvaluateRequestforquote\Model;

use Magento\Framework\Model\AbstractModel;

class EconomicEvaluation extends AbstractModel
{
    /**
     * @var string
     */
    const CACHE_TAG = 'formax_economic_evaluation';
    
    /**
     * @var string
     */
    protected $_cacheTag = 'formax_economic_evaluation';
    
    /**
     * @var string
     */
	protected $_eventPrefix = 'formax_economic_evaluation';

    /**
     * _constructor
     * 
     * @return void
     */
    protected function _construct()
    {
		$this->_init(
            \Formax\EvaluateRequestforquote\Model\ResourceModel\EconomicEvaluation::class
        );
	}

    /**
     * Get cache identities
     * 
     * @return array
     */
    public function getIdentities()
    {
		return [
            self::CACHE_TAG . '_' . $this->getId()
        ];
    }
    
    /**
     * Retrive one row by unique keys (seller_quote_id, websiteId)
     * 
     * @param int $sellerQuoteId
     * @param int $websiteId
     * @return array
     */
    public function loadUniqueRow($sellerQuoteId, $websiteId)
    {
        if (!$sellerQuoteId) {
            $sellerQuoteId = $this->getSellerQuoteId();
        }
        if (!$websiteId) {
            $websiteId = $this->getWebsiteId();
        }
        $id = $this->getResource()->loadUniqueRow($sellerQuoteId, $websiteId);
        
        return isset($id['id']) && (int)$id['id'] > 0 ? $this->load($id['id']) : false;
    }

    /**
     * Delete row by seller quote ID (seller_quote_id, websiteId)
     * 
     * @param int $sellerQuoteId
     * @param int $websiteId
     * @return array
     */
    public function deleteBySellerQuote($sellerQuoteId, $websiteId)
    {
        if (!$sellerQuoteId) {
            $sellerQuoteId = $this->getSellerQuoteId();
        }
        if (!$websiteId) {
            $websiteId = $this->getWebsiteId();
        }
        return $this->getResource()->deleteBySellerQuote($sellerQuoteId, $websiteId);
    }
}