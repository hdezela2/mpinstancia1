<?php

namespace Formax\EvaluateRequestforquote\Model\Repository;

use Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoInterface;
use Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoSearchResultsInterface;
use Formax\EvaluateRequestforquote\Api\Data\RejectedQuoteInfoSearchResultsInterfaceFactory as RejectedQuoteInfoSearchResultsFactory;
use Formax\EvaluateRequestforquote\Api\RejectedQuoteInfoRepositoryInterface;
use Formax\EvaluateRequestforquote\Model\RejectedQuoteInfoFactory;
use Formax\EvaluateRequestforquote\Model\ResourceModel\RejectedQuoteInfo as RejectedQuoteInfoResource;
use Formax\EvaluateRequestforquote\Model\ResourceModel\RejectedQuoteInfo\CollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class RejectedQuoteInfoRepository implements RejectedQuoteInfoRepositoryInterface
{
    /** @var RejectedQuoteInfoResource */
    protected $resource;

    /** @var RejectedQuoteInfoFactory */
    protected $rejectedQuoteInfoFactory;

    /** @var CollectionFactory */
    protected $collectionFactory;

    /** @var RejectedQuoteInfoSearchResultsFactory */
    protected $searchResultsFactory;

    /** @var CollectionProcessorInterface */
    private $collectionProcessor;

    public function __construct(
        RejectedQuoteInfoResource $resource,
        RejectedQuoteInfoFactory $rejectedQuoteInfoFactory,
        CollectionFactory $collectionFactory,
        RejectedQuoteInfoSearchResultsFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->rejectedQuoteInfoFactory = $rejectedQuoteInfoFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    public function save(RejectedQuoteInfoInterface $rejectedQuoteInfo, ?int $storeId = 0)
    {
        try {
            $this->resource->save($rejectedQuoteInfo);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $rejectedQuoteInfo;
    }

    public function getById($rejectedQuoteInfoId)
    {
        $rejectedQuoteInfo = $this->rejectedQuoteInfoFactory->create();
        $this->resource->load($rejectedQuoteInfo, $rejectedQuoteInfoId);
        if (!$rejectedQuoteInfo->getId()) {
            throw new NoSuchEntityException(__('The rejected quote info with the "%1" ID doesn\'t exist.', $rejectedQuoteInfoId));
        }
        return $rejectedQuoteInfo;
    }

    public function getByQuoteId($quoteId)
    {
        $rejectedQuoteInfo = $this->rejectedQuoteInfoFactory->create();
        $this->resource->load($rejectedQuoteInfo, $quoteId, 'quote_id');
        //if (!$rejectedQuoteInfo->getId()) {
        //    throw new NoSuchEntityException(__('The rejected quote info with the quote_id "%1" ID doesn\'t exist.', $quoteId));
        //}
        return $rejectedQuoteInfo;
    }

    public function getList(SearchCriteriaInterface $criteria)
    {
        /** @var \Formax\EvaluateRequestforquote\Model\ResourceModel\RejectedQuoteInfo\Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var RejectedQuoteInfoSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param RejectedQuoteInfoInterface $rejectedQuoteInfo
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(RejectedQuoteInfoInterface $rejectedQuoteInfo)
    {
        try {
            $this->resource->delete($rejectedQuoteInfo);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    public function deleteById($rejectedQuoteInfoId)
    {
        return $this->delete($this->getById($rejectedQuoteInfoId));
    }
}
