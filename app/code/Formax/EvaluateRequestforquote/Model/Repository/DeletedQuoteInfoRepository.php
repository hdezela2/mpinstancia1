<?php

namespace Formax\EvaluateRequestforquote\Model\Repository;

use Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoInterface;
use Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoSearchResultsInterface;
use Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoSearchResultsInterfaceFactory as DeletedQuoteInfoSearchResultsFactory;
use Formax\EvaluateRequestforquote\Api\DeletedQuoteInfoRepositoryInterface;
use Formax\EvaluateRequestforquote\Model\DeletedQuoteInfoFactory;
use Formax\EvaluateRequestforquote\Model\ResourceModel\DeletedQuoteInfo as DeletedQuoteInfoResource;
use Formax\EvaluateRequestforquote\Model\ResourceModel\DeletedQuoteInfo\CollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class DeletedQuoteInfoRepository implements DeletedQuoteInfoRepositoryInterface
{
    /** @var DeletedQuoteInfoResource */
    protected $resource;

    /** @var DeletedQuoteInfoFactory */
    protected $deletedQuoteInfoFactory;

    /** @var CollectionFactory */
    protected $collectionFactory;

    /** @var DeletedQuoteInfoSearchResultsFactory */
    protected $searchResultsFactory;

    /** @var CollectionProcessorInterface */
    private $collectionProcessor;

    public function __construct(
        DeletedQuoteInfoResource $resource,
        DeletedQuoteInfoFactory $deletedQuoteInfoFactory,
        CollectionFactory $collectionFactory,
        DeletedQuoteInfoSearchResultsFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->deletedQuoteInfoFactory = $deletedQuoteInfoFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    public function save(DeletedQuoteInfoInterface $deletedQuoteInfo, ?int $storeId = 0)
    {
        try {
            $this->resource->save($deletedQuoteInfo);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $deletedQuoteInfo;
    }

    public function getById($deletedQuoteInfoId)
    {
        $deletedQuoteInfo = $this->deletedQuoteInfoFactory->create();
        $this->resource->load($deletedQuoteInfo, $deletedQuoteInfoId);
        if (!$deletedQuoteInfo->getId()) {
            throw new NoSuchEntityException(__('The deleted quote info with the "%1" ID doesn\'t exist.', $deletedQuoteInfoId));
        }
        return $deletedQuoteInfo;
    }

    public function getByQuoteId($quoteId)
    {
        $deletedQuoteInfo = $this->deletedQuoteInfoFactory->create();
        $this->resource->load($deletedQuoteInfo, $quoteId, 'quote_id');
        //if (!$deletedQuoteInfo->getId()) {
        //    throw new NoSuchEntityException(__('The deleted quote info with the quote_id "%1" ID doesn\'t exist.', $quoteId));
        //}
        return $deletedQuoteInfo;
    }

    public function getList(SearchCriteriaInterface $criteria)
    {
        /** @var \Formax\EvaluateRequestforquote\Model\ResourceModel\DeletedQuoteInfo\Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var DeletedQuoteInfoSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param DeletedQuoteInfoInterface $deletedQuoteInfo
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(DeletedQuoteInfoInterface $deletedQuoteInfo)
    {
        try {
            $this->resource->delete($deletedQuoteInfo);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    public function deleteById($deletedQuoteInfoId)
    {
        return $this->delete($this->getById($deletedQuoteInfoId));
    }
}
