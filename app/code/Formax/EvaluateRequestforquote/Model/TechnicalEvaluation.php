<?php

namespace Formax\EvaluateRequestforquote\Model;

use Magento\Framework\Model\AbstractModel;

class TechnicalEvaluation extends AbstractModel
{
    /**
     * @var string
     */
    const CACHE_TAG = 'formax_technical_evaluation';
    
    /**
     * @var string
     */
    protected $_cacheTag = 'formax_technical_evaluation';
    
    /**
     * @var string
     */
	protected $_eventPrefix = 'formax_technical_evaluation';

    /**
     * _constructor
     * 
     * @return void
     */
    protected function _construct()
    {
		$this->_init(
            \Formax\EvaluateRequestforquote\Model\ResourceModel\TechnicalEvaluation::class
        );
	}

    /**
     * Get cache identities
     * 
     * @return array
     */
    public function getIdentities()
    {
		return [
            self::CACHE_TAG . '_' . $this->getId()
        ];
    }
    
    /**
     * Retrive one row by unique keys (quote_id, seller_id, website_id)
     * 
     * @param int $quoteId
     * @param int $sellerId
     * @param int $websiteId
     * @return array
     */
    public function loadUniqueRow($quoteId, $sellerId, $websiteId)
    {
        if (!$quoteId) {
            $quoteId = $this->getQuoteId();
        }
        if (!$sellerId) {
            $sellerId = $this->getSellerId();
        }
        if (!$websiteId) {
            $websiteId = $this->getWebsiteId();
        }
        $id = $this->getResource()->loadUniqueRow($quoteId, $sellerId, $websiteId);
        
        return isset($id['id']) && (int)$id['id'] > 0 ? $this->load($id['id']) : false;
    }

    /**
     * Deletes rows by quote and website (quote_id, website_id)
     * 
     * @param int $quoteId
     * @param int $websiteId
     * @return array
     */
    public function deleteByQuote($quoteId, $websiteId)
    {
        if (!$quoteId) {
            $quoteId = $this->getQuoteId();
        }
        if (!$websiteId) {
            $websiteId = $this->getWebsiteId();
        }
        return $this->getResource()->deleteByQuote($quoteId, $websiteId);
    }
}