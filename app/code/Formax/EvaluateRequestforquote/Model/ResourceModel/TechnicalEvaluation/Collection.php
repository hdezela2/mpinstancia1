<?php

namespace Formax\EvaluateRequestforquote\Model\ResourceModel\TechnicalEvaluation;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * @var string
     */
    protected $_eventPrefix = 'formax_technical_evaluation';
    
    /**
     * @var string
     */
	protected $_eventObject = 'formax_technical_evaluation_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init(
            \Formax\EvaluateRequestforquote\Model\TechnicalEvaluation::class, 
            \Formax\EvaluateRequestforquote\Model\ResourceModel\TechnicalEvaluation::class
        );
	}
}