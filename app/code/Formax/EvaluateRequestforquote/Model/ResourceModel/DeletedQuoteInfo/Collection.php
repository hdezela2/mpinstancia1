<?php

namespace Formax\EvaluateRequestforquote\Model\ResourceModel\DeletedQuoteInfo;

use Formax\EvaluateRequestforquote\Model\DeletedQuoteInfo as DeletedQuoteInfoModel;
use Formax\EvaluateRequestforquote\Model\ResourceModel\DeletedQuoteInfo as DeletedQuoteInfoResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	protected function _construct()
	{
		$this->_init(
            DeletedQuoteInfoModel::class,
            DeletedQuoteInfoResourceModel::class
        );
	}
}