<?php

namespace Formax\EvaluateRequestforquote\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class DeletedQuoteInfo extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('dccp_deleted_quote_info', 'id');
    }
}