<?php

namespace Formax\EvaluateRequestforquote\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class EconomicEvaluation extends AbstractDb
{
    protected function _construct()
    {
		$this->_init('dccp_economic_evaluation', 'id');
	}

	/**
     * Retrive one row by unique keys (seller_quote_id, website_id)
     * 
     * @param int $sellerQuoteId
     * @param int $websiteId
     * @return array
     */
    public function loadUniqueRow($sellerQuoteId, $websiteId)
    {
        $table = $this->getMainTable();
        $sellerQuoteQuoted = $this->getConnection()->quoteInto('seller_quote_id = ?', $sellerQuoteId);
        $websiteQuoted = $this->getConnection()->quoteInto('website_id = ?', $websiteId);
        $sql = $this->getConnection()->select()->from($table, ['id'])
            ->where($sellerQuoteQuoted)->where($websiteQuoted);
        $id = $this->getConnection()->fetchRow($sql);
        
        return $id;
    }

    /**
     * Delete by seller quote ID (seller_quote_id, website_id)
     * 
     * @param int $sellerQuoteId
     * @param int $websiteId
     * @return array
     */
    public function deleteBySellerQuote($sellerQuoteId, $websiteId)
    {
        $table = $this->getMainTable();
        $where[] = $this->getConnection()->quoteInto('seller_quote_id = ?', $sellerQuoteId);
        $where[] = $this->getConnection()->quoteInto('website_id = ?', $websiteId);
        return $this->getConnection()->delete($table, $where);
    }
}