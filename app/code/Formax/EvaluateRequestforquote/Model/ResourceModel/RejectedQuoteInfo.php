<?php

namespace Formax\EvaluateRequestforquote\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class RejectedQuoteInfo extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('dccp_rejected_quote_info', 'id');
    }
}