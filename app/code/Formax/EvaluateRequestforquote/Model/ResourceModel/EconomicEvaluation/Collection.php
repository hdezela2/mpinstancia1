<?php

namespace Formax\EvaluateRequestforquote\Model\ResourceModel\EconomicEvaluation;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * @var string
     */
    protected $_eventPrefix = 'formax_economic_evaluation';
    
    /**
     * @var string
     */
	protected $_eventObject = 'formax_economic_evaluation_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init(
            \Formax\EvaluateRequestforquote\Model\EconomicEvaluation::class, 
            \Formax\EvaluateRequestforquote\Model\ResourceModel\EconomicEvaluation::class
        );
	}
}