<?php

namespace Formax\EvaluateRequestforquote\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class TechnicalEvaluation extends AbstractDb
{
    protected function _construct()
    {
		$this->_init('dccp_technical_evaluation', 'id');
	}

	/**
     * Retrive one row by unique keys (quote_id, seller_id, website_id)
     * 
     * @param int $quoteId
     * @param int $sellerId
     * @param int $websiteId
     * @return array
     */
    public function loadUniqueRow($quoteId, $sellerId, $websiteId)
    {
        $table = $this->getMainTable();
        $quoteQuoted = $this->getConnection()->quoteInto('quote_id = ?', $quoteId);
        $sellerQuoted = $this->getConnection()->quoteInto('seller_id = ?', $sellerId);
        $websiteQuoted = $this->getConnection()->quoteInto('website_id = ?', $websiteId);
        $sql = $this->getConnection()->select()->from($table, ['id'])
            ->where($quoteQuoted)->where($sellerQuoted)->where($websiteQuoted);
        $id = $this->getConnection()->fetchRow($sql);
        
        return $id;
    }

    /**
     * Deletes rows by quote and website (quote_id, website_id)
     * 
     * @param int $quoteId
     * @param int $websiteId
     * @return array
     */
    public function deleteByQuote($quoteId, $websiteId)
    {
        $table = $this->getMainTable();
        $where[] = $this->getConnection()->quoteInto('quote_id = ?', $quoteId);
        $where[] = $this->getConnection()->quoteInto('website_id = ?', $websiteId);
        return $this->getConnection()->delete($table, $where);
    }
}