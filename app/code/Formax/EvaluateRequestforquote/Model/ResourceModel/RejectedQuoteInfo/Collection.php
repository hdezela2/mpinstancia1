<?php

namespace Formax\EvaluateRequestforquote\Model\ResourceModel\RejectedQuoteInfo;

use Formax\EvaluateRequestforquote\Model\RejectedQuoteInfo as RejectedQuoteInfoModel;
use Formax\EvaluateRequestforquote\Model\ResourceModel\RejectedQuoteInfo as RejectedQuoteInfoResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	protected function _construct()
	{
		$this->_init(
            RejectedQuoteInfoModel::class,
            RejectedQuoteInfoResourceModel::class
        );
	}
}