<?php
namespace Formax\EvaluateRequestforquote\Model;

use Formax\EvaluateRequestforquote\Api\Data\DeletedQuoteInfoInterface;
use Formax\EvaluateRequestforquote\Model\ResourceModel\DeletedQuoteInfo as DeletedQuoteInfoResource;
use Magento\Framework\Model\AbstractModel;

class DeletedQuoteInfo extends AbstractModel implements DeletedQuoteInfoInterface
{
    protected $_eventPrefix = 'deletedquoteinfo';

    protected function _construct()
    {
        $this->_init(DeletedQuoteInfoResource::class);
    }

    public function setQuoteId(int $value)
    {
        $this->setData('quote_id', $value);
    }

    public function getQuoteId()
    {
        return (int)$this->getData('quote_id');
    }

    public function setWebsiteId(int $value)
    {
        $this->setData('website_id', $value);
    }

    public function getWebsiteId()
    {
        return (int)$this->getData('website_id');
    }

    public function setJustification(string $value)
    {
        $this->setData('justification', $value);
    }

    public function getJustification()
    {
        return (string)$this->getData('justification');
    }

    public function setDeletedReportDocuments(string $value)
    {
        $this->setData('deleted_report_documents', $value);
    }

    public function getDeletedReportDocuments()
    {
        return (string)$this->getData('deleted_report_documents');
    }

    public function setCreatedAt(\DateTime $value)
    {
        $this->setData('created_at', $value);
    }

    public function getCreatedAt()
    {
        return new \DateTime($this->getData('created_at'));
    }

    public function setUpdatedAt(\DateTime $value)
    {
        $this->setData('updated_at', $value);
    }

    public function getUpdatedAt()
    {
        return new \DateTime($this->getData('updated_at'));
    }
}