<?php

namespace Formax\EvaluateRequestforquote\Helper;

use Formax\CreateFormAttributes\Helper\Data as HelperFormAttributes;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Customer\Model\Session as CustomerSession;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Formax\ConfigCmSoftware\Helper\Data as CmSoftwareHelper;
use Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory as InfoCollectionFactory;

class Data extends AbstractHelper
{
    /**
     * @var \Webkul\Requestforquote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory
     */
    protected $infoCollectionFactory;

    /**
     * @var Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $eavAttribute;

    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $cmSoftwareHelper;
    private HelperFormAttributes $fmHelper;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webkul\Requestforquote\Model\QuoteFactory $quoteFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param \Formax\ConfigCmSoftware\Helper\Data $cmSoftwareHelper
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        QuoteFactory $quoteFactory,
        InfoCollectionFactory $infoCollectionFactory,
        EavAttribute $eavAttribute,
        HelperFormAttributes $fmHelper,
        CmSoftwareHelper $cmSoftwareHelper
    ) {
        $this->customerSession = $customerSession;
        $this->quoteFactory = $quoteFactory;
        $this->infoCollectionFactory = $infoCollectionFactory;
        $this->eavAttribute = $eavAttribute;
        $this->cmSoftwareHelper = $cmSoftwareHelper;

        parent::__construct($context);
        $this->fmHelper = $fmHelper;
    }

    /**
     * Get quote by ID
     *
     * @param int $quoteId
     * @return \Webkul\Requestforquote\Model\QuoteFactory
     */
    public function getQuoteById($quoteId)
    {
        return $this->quoteFactory->create()->load($quoteId);
    }

    /**
     * Get if evaluation step one was visited
     *
     * @return bool
     */
    public function isStepOneVisited($quoteId)
    {
        if ($this->customerSession->isLoggedIn()
            && $this->customerSession->getData('evaluation1_' . $quoteId) == $quoteId) {
                return true;
        }

        return false;
    }

    /**
     * Get if evaluation step two was visited
     *
     * @return bool
     */
    public function isStepTwoVisited($quoteId)
    {
        if ($this->customerSession->isLoggedIn()
            && $this->customerSession->getData('evaluation2_' . $quoteId) == $quoteId) {
                return true;
        }

        return false;
    }

    /**
     * Get sellers selected in step two
     *
     * @param int $quoteId
     * @return string
     */
    public function getSellersSelected($quoteId)
    {
        return $this->customerSession->getData('seller_selected_step2_' . $quoteId);
    }

    /**
     * Set flag evaluation step one
     *
     * @param int $quoteId
     * @return void
     */
    public function setEvaluationStepOne($quoteId)
    {
        if ($this->customerSession->isLoggedIn()) {
            $this->customerSession->setData('evaluation1_' . $quoteId, $quoteId);
        }
    }

    /**
     * Set sellers selected in step two
     *
     * @param int $quoteId
     * @param string $sellers
     * @return void
     */
    public function setSellersSelected($quoteId, $sellers)
    {
        if ($this->customerSession->isLoggedIn()) {
            $this->customerSession->setData('seller_selected_step2_' . $quoteId, $sellers);
        }
    }

    /**
     * Set flag evaluation step two
     *
     * @param int $quoteId
     * @return void
     */
    public function setEvaluationStepTwo($quoteId)
    {
        if ($this->customerSession->isLoggedIn()) {
            $this->customerSession->setData('evaluation2_' . $quoteId, $quoteId);
        }
    }

    /**
     * Unset all session from current quote
     *
     * @param int $quoteId
     * @return void
     */
    public function destroyAllSessions($quoteId)
    {
        $this->customerSession->unsetData('evaluation1_' . $quoteId);
        $this->customerSession->unsetData('evaluation2_' . $quoteId);
        $this->customerSession->unsetData('seller_selected_step2_' . $quoteId);
    }

    /**
     * Get seller selected in technical evaluation
     *
     * @param int $quoteId
     * @param bool $includeStatus
     * @param mixed int|bool $page
     * @param mixed int|bool $pageSize
     * @return mixed array
     * @throws \Exception
     */
    public function getSellersSelectedCollection($quoteId, $includeStatus = true, $page = false, $pageSize = false)
    {
        $product = null;
        $quotes = null;
        $result = [];
        $error = false;

        if ($quoteId) {
            $quote = $this->getQuoteById($quoteId);
            if ($quote) {
                $product = $this->cmSoftwareHelper->getProduct($quote->getProductId());
            }

            if ($product) {
                $categoryUrl = $this->cmSoftwareHelper->getCategoryUrl($product);
                $utms = $this->cmSoftwareHelper->getCurrentUtmValue();
                $sellers = $this->getSellersSelected($quoteId);
                $sellerIds = explode(',', $sellers);
                $iniUtmQty = $this->fmHelper->getInitialRangeQtyUTM();
                $endUtmQty = $this->fmHelper->getEndRangeQtyUTM();
                $utms === 0 ? false : $utms;
                $attrTramo1 = 0;
                $attrTramo2 = 0;
                $attrTramo3 = 0;
                $subqueryTramo = 0;
                $quote = '';
                foreach ($sellerIds as $sellerId){

                    if ($utms !== false && is_array($sellerIds) && count($sellerIds) > 0) {
                        if ($categoryUrl == 'desarrollo-y-mantencion-de-software' || $categoryUrl == 'servicios-de-desarrollo-y-mantencion-de-software') {
                            $attrTramo1 = $this->eavAttribute->getIdByCode('customer', 'wkv_sw_desarrollo_tramo1');
                            $attrTramo2 = $this->eavAttribute->getIdByCode('customer', 'wkv_sw_desarrollo_tramo2');
                            $attrTramo3 = $this->eavAttribute->getIdByCode('customer', 'wkv_sw_desarrollo_tramo3');
                        } elseif ($categoryUrl == 'servicios-profesionales-ti' || $categoryUrl == 'proyectos-de-servicios-profesionales-ti') {
                            $attrTramo1 = $this->eavAttribute->getIdByCode('customer', 'wkv_sw_profesional_tramo1');
                            $attrTramo2 = $this->eavAttribute->getIdByCode('customer', 'wkv_sw_profesional_tramo2');
                            $attrTramo3 = $this->eavAttribute->getIdByCode('customer', 'wkv_sw_profesional_tramo3');
                        } else {
                            $error = __('The listed product is not in the Software Development and Maintenance or IT Professional Services categories.');
                        }

                        $subqueryTramo = '(SELECT CASE WHEN offer_price/' . $utms . ' >= ' . $iniUtmQty . ' AND offer_price/' . $utms . ' <= 100 THEN ' . $attrTramo1 .
                            ' WHEN offer_price/' . $utms . ' > 100 AND offer_price/' . $utms . ' <= 300 THEN ' . $attrTramo2 .
                            ' WHEN offer_price/' . $utms . ' > 300 AND offer_price/' . $utms . ' <= ' . $endUtmQty . ' THEN ' . $attrTramo3 . ' ELSE 0 END FROM requestforquote_quote_conversation rqc Inner Join requestforquote_quote_info rqi on rqc.seller_quote_id = rqi.entity_id WHERE rqi.seller_id = ' . $sellerId . ' and status = 2 and quote_id = '. $quoteId .' and edited = 1)';
                    } else {
                        if ($utms === false) {
                            $error = __('Could not get current value of utm.');
                        } else {
                            $error = __('No provider selected, please go to step 2');
                        }
                    }

                    if ($error === false) {
                        unset($infoCollectionFactory);
                        $infoCollectionFactory = $this->infoCollectionFactory->create();
                        $resource = $infoCollectionFactory->getResource();
                        $attrIdRut = $this->eavAttribute->getIdByCode('customer', 'wkv_dccp_rut');
                        unset($quote);
                        $quote = $infoCollectionFactory->addFieldToFilter('quote_id', $quoteId)
                            ->addFieldToFilter('seller_id', ['in' => $sellerId])
                            ->addFieldToFilter('edited', 1)
                            ->setOrder('prices.updated_at', 'ASC')
                            ->setOrder('seller.firstname', 'ASC');

                        if ($includeStatus) {
                            $quote->addFieldToFilter('status', 2);
                        }

                        $quote->getSelect()->joinLeft(
                            ['seller' => $resource->getTable('customer_entity')],
                            'main_table.seller_id = seller.entity_id',
                            ['seller_name' => new \Zend_Db_Expr('CONCAT(firstname, " ", lastname)')]
                        )->joinLeft(
                            ['seller_rut' => $resource->getTable('customer_entity_varchar')],
                            'main_table.seller_id = seller_rut.entity_id AND seller_rut.attribute_id = ' . $attrIdRut,
                            ['rut' => 'value']
                        )->joinLeft(
                            ['seller_section_dscto' => $resource->getTable('customer_entity_varchar')],
                            'main_table.seller_id = seller_section_dscto.entity_id AND seller_section_dscto.attribute_id = ' . $subqueryTramo,
                            ['discount_percent' => 'value']
                        )->joinLeft(
                            ['prices' => $resource->getTable('requestforquote_quote_conversation')],
                            'main_table.entity_id = prices.seller_quote_id',
                            [
                                'seller_updated_at' => 'prices.updated_at',
                                'ranking' => new \Zend_Db_Expr('(SELECT 1)'),
                                'base_price' => 'offer_price',
                                'discount_amount' => new \Zend_Db_Expr('(SELECT ((prices.offer_price*CAST(seller_section_dscto.value AS DECIMAL(8, 2)))/100))'),
                                'final_price' => new \Zend_Db_Expr('(SELECT prices.offer_price-((prices.offer_price*CAST(seller_section_dscto.value AS DECIMAL(8, 2)))/100))'),
                            ]
                        );

                        if ($pageSize && $page) {
                            $quote->setPageSize($pageSize);
                            $quote->setCurPage($page);
                        }
                        if ($quotes === null){
                            $quotes = clone $quote;
                        } else {
                            foreach ($quote as $item)
                            {
                                $quotes->addItem($item);
                            }
                        }
                    }
                }
            }
        } else {
            $error = __('Quote ID not found.');
        }

        return ['error' => $error, 'data' => $quotes];
    }
}
