<?php

namespace Formax\EvaluateRequestforquote\Block\Account\Customer;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\RequestInterface;
use Webkul\MpAssignProduct\Helper\Data as helperSeller;

/**
 * Evaluate class
 */
class EvaluateSuccess extends Template{

    protected $_resource;
    protected $_request;
    protected $_helperSeller;

    public function __construct(
        Context $context,
        ResourceConnection $resource,
        RequestInterface $request,
        helperSeller $helperSeller,
        array $data = []
    ) {
        $this->_helperSeller    = $helperSeller;
        $this->_resource        = $resource;
        $this->_request         = $request;
        parent::__construct($context, $data);
    }

    public function getValueAddToCart(){
        $sellerQuoteId = $this->_request->getParam('id',0);
        $con = $this->_resource->getConnection();
        $sql = "SELECT 
                    seller_id,
                    quote_id,
                    q.product_id,
                    q.amount
                FROM 
                    requestforquote_quote_info qi
                INNER JOIN requestforquote_quote q ON(q.entity_id=qi.quote_id)
                WHERE 
                    qi.entity_id=".$sellerQuoteId;
        $result = $con->fetchAll($sql);
        $data = array();
        foreach($result as $row){
            $data = array(
                'seller_id'     => $row['seller_id'],
                'quote_id'      => $row['quote_id'],
                'product_id'    => $row['product_id'],
                'amount'        => $row['amount'],
            );
            // $this->catalogSession->setSellerId($row['seller_id']);
            // $this->catalogSession->setIdCotizacion($row['quote_id']);
            // $this->catalogSession->setProductId($row['product_id']);
            // $this->catalogSession->setValor($row['amount']);
        }
        return $data;
    }
    public function getParams(){
        return $this->_request->getParam('id',0);
    }
    public function getSeller($sellerId){
        return $this->_helperSeller->getCustomer($sellerId);// se carga el proveedor
    }

}
