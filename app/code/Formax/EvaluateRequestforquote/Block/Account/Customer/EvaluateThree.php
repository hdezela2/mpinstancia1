<?php

namespace Formax\EvaluateRequestforquote\Block\Account\Customer;

use Formax\CreateFormAttributes\Helper\Data as HelperFormAttributes;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Framework\Registry;
use Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory as InfoCollectionFactory;
use Formax\EvaluateRequestforquote\Block\Account\Customer\Evaluate;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Formax\ConfigCmSoftware\Helper\Data as CmSoftwareHelper;
use Formax\EvaluateRequestforquote\Helper\Data as HelperEvaluate;

/**
 * Evaluate class
 */
class EvaluateThree extends Evaluate
{
    /**
     * @var Formax\EvaluateRequestforquote\Helper\Data
     */
    protected $helperEvaluate;

    /**
     * @var array
     */
    private $error;
    private StoreManagerInterface $storeManager;
    private HelperFormAttributes $fmHelper;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Webkul\Requestforquote\Model\QuoteFactory $quoteFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param \Formax\ConfigCmSoftware\Helper\Data $cmSoftwareHelper
     * @param \Formax\EvaluateRequestforquote\Helper\Data $helperEvaluate
     * @param StoreManagerInterface $storeManager
     * @param HelperFormAttributes $fmHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        QuoteFactory $quoteFactory,
        InfoCollectionFactory $infoCollectionFactory,
        EavAttribute $eavAttribute,
        CmSoftwareHelper $cmSoftwareHelper,
        HelperEvaluate $helperEvaluate,
        StoreManagerInterface $storeManager,
        HelperFormAttributes $fmHelper,
        array $data = []
    ) {
        $this->helperEvaluate = $helperEvaluate;
        $this->error = false;

        parent::__construct(
            $context,
            $registry,
            $quoteFactory,
            $infoCollectionFactory,
            $eavAttribute,
            $cmSoftwareHelper,
            $storeManager,
            $fmHelper,
            $data
        );
        $this->storeManager = $storeManager;
        $this->fmHelper = $fmHelper;
    }

    /**
     * Get sellers
     *
     * @return bool|\Webkul\Requestforquote\Model\ResourceModel\Info\Collection
     */
    public function getSellers()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 10;
        $cacheKey = 'formax_evaluation_selected_sellers_list';
        $quotes = $this->registry->registry($cacheKey);

        if ($quotes === null) {
            $quoteId = $this->getQuotedId();
            $quotes = $this->helperEvaluate->getSellersSelectedCollection($quoteId, true, $page, $pageSize);

            if (isset($quotes['error']) && $quotes['error'] !== false) {
                $this->error = $quotes['error'];
                $quotes = null;
            }

            if (isset($quotes['error']) && $quotes['error'] === false &&
                isset($quotes['data']) && $quotes['data'] !== null) {
                $quotes = $quotes['data'];
                $this->registry->register($cacheKey, $quotes);
            }
        }

        if ($quotes === false || (is_array($quotes) && count($quotes) == 0)) {
            $this->error = __('No provider selected, please go to step 2');
        }

        return $quotes;
    }

    /**
     * Get error message
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->error;
    }
}
