<?php

namespace Formax\EvaluateRequestforquote\Block\Account\Customer;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory as InfoCollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Formax\ConfigCmSoftware\Helper\Data as CmSoftwareHelper;

class Reject extends Template
{
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function getQuoteId()
    {
        if (!(int)$this->getRequest()->getParam('quote_id')){
            return (int)$this->getRequest()->getParams()[0];
        }
        return (int)$this->getRequest()->getParam('quote_id');
    }
}
