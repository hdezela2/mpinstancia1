<?php

namespace Formax\EvaluateRequestforquote\Block\Account\Customer;

use Formax\CreateFormAttributes\Helper\Data as HelperFormAttributes;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory as InfoCollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Formax\ConfigCmSoftware\Helper\Data as CmSoftwareHelper;

/**
 * Evaluate class
 */
class Evaluate extends Template
{
    /**
     * @var Webkul\Requestforquote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory
     */
    protected $infoCollectionFactory;

    /**
     * @var Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $eavAttribute;

    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $cmSoftwareHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $manager;

    /**
     * @var HelperFormAttributes
     */
    private HelperFormAttributes $fmHelper;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Webkul\Requestforquote\Model\QuoteFactory $quoteFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param \Formax\ConfigCmSoftware\Helper\Data $cmSoftwareHelper
     * @param StoreManagerInterface $manager
     * @param HelperFormAttributes $fmHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        QuoteFactory $quoteFactory,
        InfoCollectionFactory $infoCollectionFactory,
        EavAttribute $eavAttribute,
        CmSoftwareHelper $cmSoftwareHelper,
        StoreManagerInterface $manager,
        HelperFormAttributes $fmHelper,
        array $data = []
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->registry = $registry;
        $this->infoCollectionFactory = $infoCollectionFactory;
        $this->eavAttribute = $eavAttribute;
        $this->cmSoftwareHelper = $cmSoftwareHelper;

        parent::__construct($context, $data);
        $this->manager = $manager;
        $this->fmHelper = $fmHelper;
    }

    /**
     * Get Helper from ConfigCmSoftware
     *
     * @return \Formax\ConfigCmSoftware\Helper\Data
     */
    public function getCmSoftwareHelper()
    {
        return $this->cmSoftwareHelper;
    }

    /**
     * Get requestforquoted ID param
     *
     * @return int
     */
    public function getQuotedId()
    {
        return (int)$this->getRequest()->getParam('id', 0);
    }

    /**
     * Get step param ID
     *
     * @return int
     */
    public function getCurrentStep()
    {
        return (int)$this->getRequest()->getParam('step', 1);
    }

    /**
     * Get product object by ID
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProduct()
    {
        $quoteId = $this->getQuotedId();
        $product = null;

        try {
            $quote = $this->getQuoteById($quoteId);
            if ($quote) {
                $product = $this->cmSoftwareHelper->getProduct($quote->getProductId());
            }
        } catch (NoSuchEntityException $e) {}

        return $product;
    }

    /**
     * Get Quote by ID Maximum amount for hiring
     *
     * @param int $quoteId
     * @return Webkul\Requestforquote\Model\Quote
     */
    public function getQuoteById($quoteId)
    {
        $quote = false;
        if ($quoteId) {
            $cacheKey = 'formax_quote_by_id_' . $quoteId;
            $quote = $this->registry->registry($cacheKey);
            if ($quote === null) {
                $quote = $this->quoteFactory->create()->load($quoteId);
                $this->registry->register($cacheKey, $quote);
            }
        }

        return $quote;
    }

    /**
     * Get sellers
     *
     * @return bool|\Webkul\Requestforquote\Model\ResourceModel\Info\Collection
     */
    public function getSellers()
    {
        $page = $this->getRequest()->getParam('p') ? $this->getRequest()->getParam('p') : 1;
        $q = $this->getRequest()->getParam('q') ? $this->getRequest()->getParam('q') : '';
        $pageSize = $this->getRequest()->getParam('limit') ? $this->getRequest()->getParam('limit') : 5;
        $cacheKey = 'formax_evaluation_sellers_list';
        $quotes = $this->registry->registry($cacheKey);
        $utms = $this->cmSoftwareHelper->getCurrentUtmValue();
        $iniUtmQty = $this->fmHelper->getInitialRangeQtyUTM();
        $endUtmQty = $this->fmHelper->getEndRangeQtyUTM();

        if ($quotes === null) {
            $quoteId = $this->getQuotedId();
            $infoCollectionFactory = $this->infoCollectionFactory->create();
            $resource = $infoCollectionFactory->getResource();
            $attrIdRut = $this->eavAttribute->getIdByCode('customer', 'wkv_dccp_rut');
            $attrIdWarranty = $this->eavAttribute->getIdByCode('customer', 'wkv_sw_desarrollo_garantia');
            $attrIdWarrantyTramo1 = $this->eavAttribute->getIdByCode('customer', 'wkv_sw_desarrollo_meses_garantia_tramo1');
            $attrIdWarrantyTramo2 = $this->eavAttribute->getIdByCode('customer', 'wkv_sw_desarrollo_meses_garantia_tramo2');
            $attrIdWarrantyTramo3 = $this->eavAttribute->getIdByCode('customer', 'wkv_sw_desarrollo_meses_garantia_tramo3');
            if ($this->getWebsiteCode() == "software2022"){
                $attrIdWarranty = '(SELECT CASE WHEN offer_price/' . $utms . ' >= ' . $iniUtmQty . ' AND offer_price/' . $utms . ' <= 100 THEN ' . $attrIdWarrantyTramo1 .
                    ' WHEN offer_price/' . $utms . ' > 100 AND offer_price/' . $utms . ' <= 300 THEN ' . $attrIdWarrantyTramo2 .
                    ' WHEN offer_price/' . $utms . ' > 300 AND offer_price/' . $utms . ' <= ' . $endUtmQty . ' THEN ' . $attrIdWarrantyTramo3 . ' ELSE 0 END FROM requestforquote_quote_conversation WHERE seller_quote_id = main_table.entity_id and edited = 1 LIMIT 1)';
            }

            $quotes = $infoCollectionFactory->addFieldToFilter('quote_id', $quoteId)
                ->addFieldToFilter('status', 2)
                ->addFieldToFilter('edited', 1)
                ->setOrder('rqc.updated_at', 'ASC')
                ->setOrder('seller.firstname', 'ASC');
            $quotes->getSelect()->joinLeft(
                    ['seller' => $resource->getTable('customer_entity')],
                    'main_table.seller_id = seller.entity_id',
                    ['seller_name' => new \Zend_Db_Expr('CONCAT(firstname, " ", lastname)')]
                )->joinLeft(
                    ['seller_rut' => $resource->getTable('customer_entity_varchar')],
                    'main_table.seller_id = seller_rut.entity_id AND seller_rut.attribute_id = ' . $attrIdRut,
                    ['rut' => 'value']
                )->joinLeft(
                    ['seller_warranty' => $resource->getTable('customer_entity_varchar')],
                    'main_table.seller_id = seller_warranty.entity_id AND seller_warranty.attribute_id = ' . $attrIdWarranty,
                    ['warranty' => 'value']
                )->joinLeft(
                    ['rqc' => $resource->getTable('requestforquote_quote_conversation')],
                    'main_table.entity_id = rqc.seller_quote_id',
                    ['seller_updated_at' => 'rqc.updated_at']
                );
            $quotes->setPageSize($pageSize);
            $quotes->setCurPage($page);

            if ($q) {
                $quotes->addFieldToFilter('seller.value', ['like' => '%' . $q . '%']);
            }
            $this->registry->register($cacheKey, $quotes);
        }

        return $quotes;
    }

    /**
     * _prepareLayout
     *
     * @return void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $step = $this->getCurrentStep();
        $collection = $this->getSellers();

        if ($collection) {
            $blockName = 'requestforquote.evaluate.step' . $step . '.list.pager';
            if ($this->getLayout()->getBlock($blockName)) {
                $pager = $this->getLayout()->getBlock($blockName)
                    ->setAvailableLimit([10 => 10])
                    ->setShowPerPage(false)
                    ->setCollection($collection);
            } else {
                $pager = $this->getLayout()->createBlock(
                    'Magento\Theme\Block\Html\Pager',
                    $blockName
                )
                ->setAvailableLimit([10 => 10])
                ->setShowPerPage(false)
                ->setCollection($collection);
            }

            $this->setChild('pager', $pager);
            $collection->load();
        }
        return $this;
    }

    /**
     * Get paginator
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get q param
     *
     * @return string
     */
    public function getQParam()
    {
        return $this->getRequest()->getParam('q');
    }


    /**
     * Get download URL for files
     *
     * @param object $quote
     * @param int $step
     * @return string
     */
    public function getDownloadUrl($quote)
    {
        return $this->getUrl(
            'evaluate/requestforquote/download',
            [
                'seller_id' => $quote->getSellerId(),
                'quote_id' => $quote->getQuoteId()
            ]
        );
    }

    /**
     * Get search sellers URL
     *
     * @return string
     */
    public function getSearchFormUrl()
    {
        return $this->getUrl('evaluate/requestforquote/searchseller');
    }

    /**
     * Get download URL for all files
     *
     * @return string
     */
    public function getDownloadAllUrl()
    {
        return $this->getUrl('evaluate/requestforquote/download', []);
    }

    public function getDeclareDesertUrl()
    {
        return $this->getUrl('evaluate/requestforquote/desert', []);
    }

    /**
     * @param object $quote
     * @return string
     */
    public function getStep2Url()
    {
        $id = $this->getQuotedId();
        return $this->getUrl('evaluate/requestforquote/index', ['id' => $id, 'step' => 2]);
    }

    /**
     * Get table's button text
     *
     * @return string
     */
    public function getButtonText()
    {
        $step = $this->getCurrentStep();
        switch ($step) {
            case 1:
                $buttonText = __('Download selected offers and comparative table');
            break;
            case 2:
                $buttonText = __('Select sellers');
            break;
            case 3:
                $buttonText = __('select sellers to award');
            break;
            default:
                $buttonText = __('Download selected offers and comparative table');

        }

        return $buttonText;
    }
    public function getWebsiteCode(): ?string
    {
        try
        {
            $websiteCode = $this->manager->getWebsite()->getCode();
        }
        catch (LocalizedException $localizedException)
        {
            $websiteCode = null;
            $this->_logger->error($localizedException->getMessage());
        }
        return $websiteCode;
    }
}
