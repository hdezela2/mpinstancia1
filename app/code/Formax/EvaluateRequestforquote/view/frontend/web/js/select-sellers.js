define([
    "jquery",
    "jquery/ui",
    "Formax_EvaluateRequestforquote/js/cache"
    ], function ($, ui, customStorage) {
        "use strict";

        $.widget("evaluateRequestforquote.customStorage", {
            options: {
                tableContainer: ".table-seller-selected",
                tableTitle: ".title-table-seller-selected",
                tableCheckSelector: ".table-evaluation-requestforquoted input.seller-check[type=checkbox]",
                removeRowSelector: ".table-seller-selected tr td a.view",
                sellerInput: '.custom-attributes form input.seller_id',
                warrantyInput: '.custom-attributes form input[name="warranty_months"]',
                rankingInput: '.custom-attributes form input[name="ranking"]',
                basePriceInput: '.custom-attributes form input[name="base_price"]',
                discountAmountInput: '.custom-attributes form input[name="discount_amount"]',
                discountPercentInput: '.custom-attributes form input[name="discount_percent"]',
                finalPriceInput: '.custom-attributes form input[name="final_price"]',
                submitForm: '#dccp_economic_evaluation',
                evaluationQuotedId: '#evaluationQuotedId'
            },
    
            _create: function () {
                this._bind();
            },    
    
            _bind: function () {
                var self = this;
                self._selectSellerEvent();
                self._removeElementEvent();
                self._submitEvaluationEvent();

                $(document).ready(function () {
                    $(self.options.tableContainer).html("");
                    $(self.options.tableTitle).hide();
                    self._checkInput();
                    self._createTable();
                });
            },

            _selectSellerEvent: function() {
                var self = this;
                $(self.options.tableCheckSelector).on("click", function () {
                    var selectedOptions = [];
                    var valChecked = parseInt(this.value);
                    var selectedSeller = $(this).closest(".select-seller");
                    var sw = self._getTableStep();
                    
                    if (sw == 2) {
                        var currentWarrantyTd = selectedSeller.siblings(".warranty").find("span:first").html();
                        var warranty = parseInt(currentWarrantyTd.match(/\d/g).join(""));
                    } else if (sw == 3) {
                        var currentRankingTd = selectedSeller.siblings(".ranking").find("span:first").html();
                        var ranking = parseFloat(Number(currentRankingTd.replace(",", ".").replace(/[^0-9\.]+/g, "")));
                        ranking = ranking != '-' ? ranking : 0;
                        var currentBasePriceTd = selectedSeller.siblings(".base-price").find("span:first").html();
                        var basePrice = parseInt(currentBasePriceTd.match(/\d/g).join(""));
                        var currentDiscountAmountTd = selectedSeller.siblings(".discount-amount").find("span:first").text();
                        var discountAmount = $.trim(currentDiscountAmountTd) != '-' ? parseInt(currentDiscountAmountTd.match(/\d/g).join("")) : 0;
                        var currentDiscountPercentTd = selectedSeller.siblings(".discount-amount").find("span.discount-percent:first").html();
                        var discountPercent = discountAmount > 0 ? parseFloat(Number(currentDiscountPercentTd.replace(",", ".").replace(/[^0-9\.]+/g, ""))) : 0;
                        var currentFinalPriceTd = selectedSeller.siblings(".final-price").find("span:first").html();
                        var finalPrice = parseInt(currentFinalPriceTd.match(/\d/g).join(""));
                    }

                    if ($(this).prop("checked")) {
                        $(this).closest("tr").addClass("checked");
                        if (sw != 1) {
                            if (!customStorage.hasData()) {
                                if (sw == 2) {
                                    selectedOptions = [{
                                        sellerId: parseInt($(this).val()),
                                        warranties: warranty,
                                        row: $(this).closest("tr")[0].outerHTML
                                    }];
                                }
                                if (sw == 3) {
                                    selectedOptions = [{
                                        sellerId: parseInt($(this).val()),
                                        ranking: ranking,
                                        basePrice: basePrice,
                                        discountAmount: discountAmount,
                                        discountPercent: discountPercent,
                                        finalPrice: finalPrice,
                                        row: $(this).closest("tr")[0].outerHTML
                                    }];
                                }

                                customStorage.setSellerSelected(selectedOptions);
                                customStorage.setHasData(true);
                            } else {
                                if (!self._sellerIdExist(this.value)) {
                                    selectedOptions = customStorage.getSellerSelected();
                                    if (sw == 2) {
                                        selectedOptions.push({
                                            sellerId: parseInt(this.value),
                                            warranties: warranty,
                                            row: $(this).closest("tr")[0].outerHTML
                                        });
                                    }
                                    
                                    customStorage.setSellerSelected(selectedOptions);
                                    customStorage.setHasData(true);
                                }

                                if (sw == 3) {
                                    selectedOptions = [{
                                        sellerId: parseInt(this.value),
                                        ranking: ranking,
                                        basePrice: basePrice,
                                        discountAmount: discountAmount,
                                        discountPercent: discountPercent,
                                        finalPrice: finalPrice,
                                        row: $(this).closest("tr")[0].outerHTML
                                    }];
                                    customStorage.setSellerSelected(selectedOptions);
                                    customStorage.setHasData(true);
                                }
                            }
                            
                            if (sw == 3) {
                                self._unCheckedInput($(this).val());
                            }
                        }
                    } else {
                        if (sw != 1) {
                            var hasData = false;
                            var data = null;
                            var selectedOptions = customStorage.getSellerSelected();

                            if (customStorage.hasData()) {
                                var jsonObject = selectedOptions.filter(d => d.sellerId != valChecked);
                                $(this).closest("tr").removeClass("checked");
                                if (jsonObject.length > 0) {
                                    hasData = true;
                                    data = jsonObject;
                                }
                                customStorage.setSellerSelected(data);
                                customStorage.setHasData(hasData);
                            }
                        }
                    }
                    self._createTable();
                });
            },

            _createTable: function() {
                if (customStorage.hasData()) {
                    var htmlTableOpen = '<div class="table-wrapper"><table class="data-table table"><tbody>';
                    var htmlTableClose = "</tbody></table></div>";
                    var htmlTable = "";
                    var sellers = customStorage.getSellerSelected();
                    var sellersId = [];
                    var warranties = [];
                    var ranking = [];
                    var basePrice = [];
                    var discountAmount = [];
                    var discountPercent = [];
                    var finalPrice = [];
                    var sw = this._getTableStep();
                    var tr = "";
                    var i;
                    
                    for (i = 0; i < sellers.length; i++) {
                        sellersId[i] = sellers[i].sellerId;

                        if (sw == 2) {
                            warranties[i] = sellers[i].warranties;
                        }
                        if (sw == 3) {
                            ranking[i] = sellers[i].ranking;
                            basePrice[i] = sellers[i].basePrice;
                            discountAmount[i] = sellers[i].discountAmount;
                            discountPercent[i] = sellers[i].discountPercent;
                            finalPrice[i] = sellers[i].finalPrice;
                        }

                        tr += sellers[i].row;
                    }
                    htmlTable = htmlTableOpen + tr + htmlTableClose;
                    $(this.options.tableContainer).html("");
                    $(this.options.tableContainer).append(htmlTable);
                    this._prepareSecundaryTable();
                    $(this.options.tableTitle).show();
                    
                    /** Set values selected to inputs hidden **/
                    $(this.options.sellerInput).val(sellersId.join());
                    if (sw == 2) {
                        $(this.options.warrantyInput).val(warranties.join());
                    }
                    if (sw == 3) {
                        $(this.options.rankingInput).val(ranking.join());
                        $(this.options.basePriceInput).val(basePrice.join());
                        $(this.options.discountAmountInput).val(discountAmount.join());
                        $(this.options.discountPercentInput).val(discountPercent.join());
                        $(this.options.finalPriceInput).val(finalPrice.join());
                    }
                } else {
                    $(this.options.tableTitle).hide();
                    $(this.options.tableContainer).html("");

                    /** Set empty to inputs hidden **/
                    $(this.options.sellerInput).val("");
                    if (sw == 2) {
                        $(this.options.warrantyInput).val("");
                    }
                    if (sw == 3) {
                        $(this.options.rankingInput).val("");
                        $(this.options.basePriceInput).val("");
                        $(this.options.discountAmountInput).val("");
                        $(this.options.discountPercentInput).val("");
                        $(this.options.finalPriceInput).val("");
                    }
                }
            },

            _sellerIdExist(sellerId) {
                var exist = false;
                var sellers = customStorage.getSellerSelected();
                var i;
                for (i = 0; i < sellers.length; i++) {
                    if (sellerId == sellers[i].sellerId) {
                        exist = true;
                        return true;
                    }
                }

                return exist;
            },

            _checkInput() {
                if (customStorage.hasData()) {
                    var sellers = customStorage.getSellerSelected();
                    var i;
                    for (i = 0; i < sellers.length; i++) {
                        var element = $("#seller-table-" + sellers[i].sellerId);
                        element.prop("checked", true);
                        element.closest("tr").addClass("checked");
                    }
                }
            },

            _prepareSecundaryTable() {
                var trDom = $(".table-seller-selected table.table tr");
                var inputDom = trDom.find('td.select-seller').find("input");
                var linkDom = trDom.find("td.download a.view");
                trDom.addClass("checked");
                inputDom.attr("type", "hidden");
                inputDom.attr("id", "selected-" + inputDom.attr("id"));
                linkDom.attr("href", "#");
                linkDom.attr("title", "Eliminar");
                linkDom.find("span").html("Eliminar");
                trDom.find("td.download").removeClass("download");
                linkDom.show();
            },

            _removeElementEvent() {
                var self = this;
                $(document).on("click", self.options.removeRowSelector, function () {
                    var hasData = false;
                    var data = null;
                    var linkChecked = $(this).closest("td").siblings("td.select-seller").find("input:first").val();
                    var selectedOptions = customStorage.getSellerSelected();

                    if (customStorage.hasData()) {
                        var jsonObject = selectedOptions.filter(d => d.sellerId != linkChecked);
                        
                        if (jsonObject.length > 0) {
                            hasData = true;
                            data = jsonObject;
                        }
                        customStorage.setSellerSelected(data);
                        customStorage.setHasData(hasData);
                    }
                    
                    $(self.options.tableCheckSelector).each(function() {
                        if ($(this).val() == linkChecked) {
                            $(this).prop("checked", false);
                            $(this).closest("tr").removeClass("checked")
                        }
                    });
                    $(this).closest("tr").remove();
                    self._createTable();
                });
            },

            _submitEvaluationEvent() {
                var self = this;
                $(this.options.submitForm).submit(function(e) {
                    e.preventDefault();
                    self._clearLocalStorage();
                    this.submit();
                });
            },

            _unCheckedInput(val) {
                $(this.options.tableCheckSelector).each(function() {
                    var currentValue = $(this).val();
                    if (val != currentValue) {
                        var element = $("#seller-table-" + currentValue);
                        element.prop("checked", false);
                        element.closest("tr").removeClass("checked");
                    }
                });
            },

            _clearLocalStorage() {
                var quotedId = $(this.options.evaluationQuotedId).val();
                localStorage.removeItem('fx-evaluation-requestedquote-table_' + quotedId + '_1');
                localStorage.removeItem('fx-evaluation-requestedquote-table_' + quotedId + '_2');
                localStorage.removeItem('fx-evaluation-requestedquote-table_' + quotedId + '_3');
            },

            _getTableStep() {
                if ($('.table-step-2').length > 0) {
                    return 2;
                }
                if ($('.table-step-3').length > 0) {
                    return 3;
                }

                return 1;
            }
        });
    
        return $.evaluateRequestforquote.customStorage;
    }
);