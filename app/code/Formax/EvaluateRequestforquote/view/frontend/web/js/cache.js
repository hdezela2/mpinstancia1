define([
    "jquery",
    "mage/storage",
    "jquery/jquery-storageapi"
    ], function ($) {
        "use strict";
        var cacheKey = $('.table-evaluation-requestforquoted').attr('id'),
        storage = $.initNamespaceStorage('fx-' + cacheKey).localStorage,
    
        saveData = function (data) {
            storage.set(cacheKey, data);
        },

        getData = function () {
            if (!storage.get(cacheKey)) {
                reset();
            }
            return storage.get(cacheKey);
        },    
    
        reset = function () {
            var data = {
                hasData: false,
                sellerSelected: null
            };
            saveData(data);
        };
    
        return {
            reset : function () {
                reset();
            },    
        
            hasData: function () {
                var obj = getData();
                return obj.hasData;
            },
        
            setHasData: function (hasData) {
                var obj = getData();
                obj.hasData = hasData;
                saveData(obj);
            },
        
            getSellerSelected: function () {
                var obj = getData();
                return obj.sellerSelected;
            },
        
            setSellerSelected: function (sellerSelected) {
                var obj = getData();
                obj.sellerSelected = sellerSelected;
                saveData(obj);
            }
        };
    }
);