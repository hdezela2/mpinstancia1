define([
    "jquery",
    "jquery/ui",
    "jquery/validate",
    "mage/translate"
 ], function($) {
    "use strict";
    return function() {
        $.validator.addMethod(
            'isEmptyField', function (value) {
                return !$.mage.isEmpty(value);
            }, $.mage.__('This is a required field.'));

        $.validator.addMethod(
            'phoneGlobal', function (value) {
                return $.mage.isEmptyNoTrim(value) || /^[\+\d]+(?:[\d-.\s()]*)$/.test(value);
        }, $.mage.__('Please enter a valid phone.'));

        $.validator.addMethod(
            'validate-letter-whitespace', function (value) {
                return $.mage.isEmptyNoTrim(value) ||  /^[a-zA-ZÑñ\s]*$/.test(value);
        }, $.mage.__('Solo se aceptan letras (a-z o A-Z) y/o espacios en este campo.'));
        
        $.validator.addMethod(
            'validate-letter-latin-whitespace', function (value) {
                return $.mage.isEmptyNoTrim(value) ||  /^[a-z áéíóúñüÁÉÍÓÚÑÜ]+$/i.test(value);
        }, $.mage.__('Solo se aceptan letras (a-z o A-Z) y/o espacios en este campo.'));

        $.validator.addMethod(
            'validate-letter-digits-whitespace', function (value) {
                return $.mage.isEmptyNoTrim(value) ||  /^[a-zA-Z0-9Ññ\s]*$/.test(value);
        }, $.mage.__('Solo se aceptan letras (a-z o A-Z) n\xfameros (0-9) y/o espacios en este campo.'));
        
        $.validator.addMethod(
            'validate-decimal', function (value) {
                return $.mage.isEmptyNoTrim(value) ||  /^\d+(\.\d{1,2})?$/.test(value);
        }, $.mage.__('Solo se aceptan n\u00fameros y dos decimales. El signo decimal es el punto.'));

        $.validator.addMethod(
            'phoneCL', function (value) {
                if ($.trim(value) != '') {
                    if (value.length < 8 || value.length > 10) {
                        return false;
                    }
                }
                return $.mage.isEmptyNoTrim(value) ||  /^[269][0-9]*$/.test(value);
        }, $.mage.__('Solo se aceptan d\xedgitos con longitud entre 8 y 10 caracteres y deben iniciar en 2, 6 o 9.'));

        $.validator.addMethod(
            'max-entrega-emerg-cl', function(value) {
                return $.mage.isEmptyNoTrim(value) ||  /^(4[0-8]|[1-3][0-9]|[1-9])$/.test(value);
            }, $.mage.__('The maximum emergency delivery time cannot exceed 48 hours')
        );

        $.validator.addMethod(
            'max-contingency-deliver-cl', function(value) {
                return $.mage.isEmptyNoTrim(value) || /^(7[0-2]|[1-6][0-9]|[1-9])$/.test(value)
            }, $.mage.__('The maximum contingency delivery time cannot exceed 72 hours')
        );

        $.validator.addMethod(
            'max-prevention-deliver-cl', function(value) {
                return $.mage.isEmptyNoTrim(value) || /^(1[0-5]|[1-9])$/.test(value)
            }, $.mage.__('The maximum delivery time in prevention cannot exceed 15 days')
        )

        $.validator.addMethod(
            'number-unlimited', function (value) {
                var txt = $.trim(value).toLowerCase();
                return $.mage.isEmptyNoTrim(value) || txt === 'ilimitado' || txt === 'ilimitados' || txt === 'ilimitada' || txt === 'ilimitadas' || !/[^\d]/.test(value);
        }, $.mage.__('Please enter a valid number or unlimited word.'));
    
        $.validator.addMethod(
            'rutCL', function (value, element) {
                return this.optional(element) || validateRutCL(value);
        }, $.mage.__('Please enter a valid RUT.'));

        function validateRutCL (value) {
            if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(value)) {
                return false;
            }
            var tmp = value.split('-');
            var digv = tmp[1];
            var rut = tmp[0];
            if (rut.length < 7 || rut.length > 8) {
                return false;
            }
            if (digv == 'K') {
                digv = 'k' ;
            }
            return (dv(rut) == digv);
        }

        function dv(T) {
            var M = 0, S = 1;
            for(; T; T=Math.floor(T/10))
                S = (S+T%10*(9-M++%6))%11;

            return S ? S-1 : 'k';
        }
    }
 });
 
