<?php
namespace Formax\Certification\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context){
        $setup->startSetup();
        $this->createCertifictionTable($setup);
        $this->createCertifictionCategoryTable($setup);
        $setup->endSetup();
    }
    protected function createCertifictionTable(SchemaSetupInterface $setup){
        $tableName = 'xpec_certificaction';
        $table = $setup->getConnection()
            ->newTable($setup->getTable($tableName))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true,'nullable' => false,'primary' => true],
                'Id'
            )
            ->addColumn(
                'id_user',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Id User'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '255',
                ['nullable' => false, 'default' => ''],
                'Name User'
            )
            ->addColumn(
                'details',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '2M',
                ['nullable' => false, 'default' => ''],
                'Details'
            )
            ->addColumn(
                'category',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Id Category'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Status'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false, 
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ],
                'Fecha de Creación'
            )
            ->addColumn(
                'data_serialize',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '4M',
                ['nullable' => false, 'default' => ''],
                'Data Serialize'
            )
            ->setComment("Xpec Table Certification");
        $setup->getConnection()->createTable($table);

        $column = 'id_user';
        $setup->getConnection()->addIndex(
            $tableName,
            'indx_'.$column,
            [$column]
        );
        
    }
    protected function createCertifictionCategoryTable(SchemaSetupInterface $setup){
        $tableName = 'xpec_certificaction_category';
        $table = $setup->getConnection()
            ->newTable($setup->getTable($tableName))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true,'nullable' => false,'primary' => true],
                'Id'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '255',
                ['nullable' => false, 'default' => ''],
                'Name Category'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Status'
            )
            ->addColumn(
                'data_serialize',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '4M',
                ['nullable' => false, 'default' => ''],
                'Data Serialize'
            )
            ->setComment("Xpec Table Certification Category");
        $setup->getConnection()->createTable($table);

        $column = 'status';
        $setup->getConnection()->addIndex(
            $tableName,
            'indx_'.$column,
            [$column]
        );
        
    }
}
