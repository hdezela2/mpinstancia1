<?php
namespace Formax\Certification\Model\ResourceModel\CertificationCategory;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'formax_xpec_certification_category_collection';
	protected $_eventObject = 'certification_category_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Formax\Certification\Model\CertificationCategory', 'Formax\Certification\Model\ResourceModel\CertificationCategory');
	}

}