<?php
namespace Formax\Certification\Model\ResourceModel;
class CertificationCategory extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	){
		parent::__construct($context);
	}
	
	protected function _construct(){
		$this->_init('xpec_certificaction_category', 'id');
	}
	
}
