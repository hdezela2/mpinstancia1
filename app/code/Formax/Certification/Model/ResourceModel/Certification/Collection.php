<?php
namespace Formax\Certification\Model\ResourceModel\Certification;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'formax_xpec_certification_collection';
	protected $_eventObject = 'certification_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Formax\Certification\Model\Certification', 'Formax\Certification\Model\ResourceModel\Certification');
	}

}