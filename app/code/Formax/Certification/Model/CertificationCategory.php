<?php
namespace Formax\Certification\Model;
class CertificationCategory extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface{
	const CACHE_TAG         = 'formax_certification';
	protected $_cacheTag    = 'formax_certification';
	protected $_eventPrefix = 'formax_certification';

	protected function _construct(){
		$this->_init('Formax\Certification\Model\ResourceModel\CertificationCategory');
	}

	public function getIdentities(){
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues(){
		$values = [];
		return $values;
	}
}