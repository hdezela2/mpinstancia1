<?php
namespace Formax\Certification\Model;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider{
    /**
     * Product collection
     *
     * @var \Webkul\Marketplace\Model\ResourceModel\Product\Collection
     */
    protected $collection;

    private $_certification;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    private $_helperWebkul;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = [],
        \Formax\Certification\Model\CertificationFactory $certification,
        \Webkul\SellerSubAccount\Helper\Data $helperWebkul
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );

        $this->_certification = $certification;
        $this->_helperWebkul        = $helperWebkul;

        $this->collection = $this->_certification->create()->getCollection();
        $this->collection->addFieldToFilter(
            'id_user',array(
                array('eq' => $this->_helperWebkul->getSubAccountSellerId()  )
            )
        );
    }
}
