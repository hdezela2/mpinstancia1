<?php

namespace Formax\Certification\Controller\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Formax\Certification\Model\CertificationFactory;
use Magento\Framework\Serialize\SerializerInterface;

class SaveCertification extends \Magento\Framework\App\Action\Action{

    protected $_resultPageFactory;
    protected $_resource;
    protected $_certification;
    protected $_serializer;
    protected $_services;
    private $_customerSession;
    private $_helperWebkul;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\SellerSubAccount\Helper\Data $helperWebkul,
        CertificationFactory $certification,
        SerializerInterface $serializer
    ) {
        $this->_resultPageFactory   = $resultJsonFactory;
        $this->_resource            = $resource;
        $this->_certification       = $certification;
        $this->_serializer          = $serializer;
        $this->_customerSession     = $customerSession;
        $this->_helperWebkul        = $helperWebkul;
        parent::__construct($context);
    }
    /**
     * Index action
     *
     * @return $this
     */
    public function execute(){
        $datajson   = $this->_resultPageFactory->create();
        $post       = $this->getRequest()->getPostValue();
        if(!isset($post['name']) || empty($post['name'])){
            $this->messageManager->addError('Digite nombre');
            $data[]=array('status'=>'Advertencia');
            return $datajson->setData(array('result'=>$data));
        }
        if(!isset($post['detail']) || empty($post['detail'])){
            $this->messageManager->addError('Digite Detalle');
            $data[]=array('status'=>'Advertencia');
            return $datajson->setData(array('result'=>$data));
        }
        if(!isset($post['category']) || empty($post['category'])){
            $this->messageManager->addError('Seleccione Categoria');
            $data[]=array('status'=>'Advertencia');
            return $datajson->setData(array('result'=>$data));
        }
        try{
            $certification  = $this->_certification->create();
            if(isset($post['id'])){
                $certification->load($post['id'],'id');
            }
            $idCustomer     = $this->_customerSession->getCustomer()->getId();
            $certification->setName($post['name']);
            $certification->setIdUser( $this->_helperWebkul->getSubAccountSellerId() );
            $certification->setDetails($post['detail']);
            $certification->setCategory($post['category']);
            $certification->save();
            $this->messageManager->addSuccess(__('Datos grabados con éxito.'));
            $data[]=array('status'=>'Exito');
            return $datajson->setData(array('result'=>$data));
        } catch(\Exception $e){
            $this->messageManager->addError($e->getMessage());
            $data[]=array('status'=>'Error');
            return $datajson->setData(array('result'=>$data));
        }
        
    }

}