<?php
namespace Formax\Certification\Controller\Seller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Formax\Certification\Model\CertificationFactory;
use Magento\Store\Model\StoreManagerInterface;


class Eliminar extends \Magento\Customer\Controller\AbstractAccount{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;
    private CertificationFactory $_certificationFactory;
    private StoreManagerInterface $storeManager;

    /**
     * __construct function
     *
     * @param Context                             $context
     * @param PageFactory                         $resultPageFactory
     * @param \Webkul\Requestforquote\Helper\Data $dataHelper
     * @param \Webkul\Marketplace\Helper\Data     $mpHelper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        RequestInterface $request,
        CertificationFactory $certificationFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->resultPageFactory        = $resultPageFactory;
        $this->_request                 = $request;
        $this->_certificationFactory    = $certificationFactory;
        parent::__construct($context);
        $this->storeManager = $storeManager;
    }


    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute(){
        $BaseURL = '/' . $this->storeManager->getWebsite()->getCode() . '/certification/seller/lists';
        $certification = $this->getCertification();
        $certification->delete();
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($BaseURL);
        return $resultRedirect;
    }
    public function getId(){
        $id = $this->_request->getParam('id',0);
        if(isset($id) && is_numeric($id) && $id>0){
            return $id;
        }else{
            return null;
        }
    }
    public function getCertification(){
        $id = $this->getId();
        $certification = $this->_certificationFactory->create();
        $certification->load($id,'id');
        return $certification;
    }
}
