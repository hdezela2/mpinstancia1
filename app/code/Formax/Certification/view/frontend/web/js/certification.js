require([
    "jquery",'domReady!'
], function($){
    'use strict';
    $(document).ready(function($,ko) {
        $(".btn-save-certification").off("click");
        $(".btn-save-certification").on("click",function(e){
            e.preventDefault();
            saveCertification();
        });

        function saveCertification(){
            var url = window.BASE_URL+"certification/index/savecertification";
            var data = $("#formcertification").serialize();
            $.ajax({
                showLoader: true,
                url: url,
                data: data,
                type: "POST",
                dataType: 'json'
            }).done(function (data) {
                //$target_.modal('closeModal');
                //$("html, body").animate({ scrollTop: 0 }, 300)
            });
        }


    });



});
