<?php
namespace Formax\Certification\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\RequestInterface;
use Formax\Certification\Model\CertificationFactory;

class FormCertification extends Template{

    protected $_request;
    protected $_certificationFactory;
    
    public function __construct(
        Context $context,
        RequestInterface $request,
        CertificationFactory $certificationFactory,
        array $data = []
    ) {
        $this->_request                 = $request;
        $this->_certificationFactory    = $certificationFactory;
        parent::__construct($context, $data);
    }

    public function getUrlFormNew(){
        return $this->getUrl('certification/seller/nuevo', []);
    }
    public function getId(){
        $id = $this->_request->getParam('id',0);        
        if(isset($id) && is_numeric($id) && $id>0){
            return $id;
        }else{
            return null;
        }
    }
    public function getCertification(){
        $id = $this->getId();
        $certification = $this->_certificationFactory->create();
        $certification->load($id,'id');
        return $certification;

    }

}
