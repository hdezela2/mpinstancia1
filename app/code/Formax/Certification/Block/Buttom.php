<?php
namespace Formax\Certification\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Buttom extends Template{
    
    public function __construct(
        Context $context,
        array $data = []
    ) {

        parent::__construct($context, $data);
    }


    public function getButtonText(){
        $buttonText = __('Nuevo');

        return $buttonText;
    }
    public function getUrlFormNew(){
        return $this->getUrl('certification/seller/nuevo', []);
    }
}
