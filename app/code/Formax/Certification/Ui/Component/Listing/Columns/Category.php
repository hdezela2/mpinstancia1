<?php
namespace Formax\Certification\Ui\Component\Listing\Columns;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class QuoteListActions.
 */
class Category extends Column{
    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;
    protected $_storeManager;


    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    ) {
        $this->_urlBuilder      = $urlBuilder;
        $this->_storeManager    = $storeManager;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }


    public function prepareDataSource(array $dataSource){
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                //$item[$this->getData('name')] = $this->priceFormatter->format($item[$this->getData('name')],false,0);
                $category = '';
                $tmp = $item[$this->getData('name')];
                switch( $item[$this->getData('name')] ){
                    case 1:
                        $category = 'SERVICIOS DE DESARROLLO Y MANTENCIÓN DE SOFTWARE';
                    break;
                    case 2:
                        $category = 'PROYECTOS DE SERVICIOS PROFESIONALES TI';
                    break;
                    default:
                        $category = $item[$this->getData('name')];
                    break;
                }
                $item[$this->getData('name')] = $category;
            }
        }
        return $dataSource;
    }
}
