<?php

namespace Formax\CreateFormAttributes\Controller\Test;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Formax\CreateFormAttributes\Model\Config;
use Formax\CreateFormAttributes\Helper\Data;

class Render extends Action
{
    /**
     * @var Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Formax\CreateFormAttributes\Model\Config
     */
    protected $config;

    /**
     * @var Formax\CreateFormAttributes\Helper\Data
     */
    protected $helper;

    /**
     * Constructor dependency injection
     * 
     * @param Magento\Framework\App\Action\Context $context
     * @param Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param Formax\CreateFormAttributes\Model\Config $config
     * @param Formax\CreateFormAttributes\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Config $config,
        Data $helper
    ) {
        $this->config = $config;
        $this->helper = $helper;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
