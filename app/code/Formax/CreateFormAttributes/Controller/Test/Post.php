<?php

namespace Formax\CreateFormAttributes\Controller\Test;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Formax\CreateFormAttributes\Model\Config;
use Formax\CreateFormAttributes\Helper\Data;

class Post extends Action
{
    /**
     * @var Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Formax\CreateFormAttributes\Model\Config
     */
    protected $config;

    /**
     * @var Formax\CreateFormAttributes\Helper\Data
     */
    protected $helper;

    /**
     * Constructor dependency injection
     *
     * @param Magento\Framework\App\Action\Context $context
     * @param Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param Formax\CreateFormAttributes\Model\Config $config
     * @param Formax\CreateFormAttributes\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Config $config,
        Data $helper
    ) {
        $this->config = $config;
        $this->helper = $helper;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $postData = $this->getRequest()->getPostValue();
        $filesData = $this->getRequest()->getFiles();

        /*echo 'Post: '; echo "<br>";
        echo '<pre>'; print_r($postData); echo '</pre>';
        echo 'Parse Post: '; echo '<br>';
        $data = $this->helper->parsePostData('sample', $postData, $filesData);
        echo '<pre>'; print_r($data); echo '</pre>';
        exit;*/
    }
}
