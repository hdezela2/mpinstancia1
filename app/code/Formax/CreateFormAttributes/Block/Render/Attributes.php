<?php

namespace Formax\CreateFormAttributes\Block\Render;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Response\RedirectInterface;
use Formax\CreateFormAttributes\Model\Config;
use Formax\CreateFormAttributes\Helper\Data;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Formax\GreatBuy\Helper\Data as GreatBuyHelper;
use Magento\Framework\Serialize\Serializer\Json as JsonSerialize;

class Attributes extends Template
{
    /**
     * @var Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @var Magento\Framework\Serialize\Serializer\Json
     */
    protected $jsonSerialize;

    /**
     * @var Formax\CreateFormAttributes\Model\Config
     */
    protected $config;

    /**
     * @var Formax\CreateFormAttributes\Helper\Data
     */
    protected $helper;

    /** @var GreatBuyHelper */
    protected $greatBuyHelper;

    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $helperSoftware;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /** @var HelperSoftware */
    protected $swHelper;

    public function __construct(
        Context $context,
        Config $config,
        RedirectInterface $redirect,
        JsonSerialize $jsonSerialize,
        Data $helper,
        GreatBuyHelper $greatBuyHelper,
        HelperSoftware $helperSoftware,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Formax\ConfigCmSoftware\Helper\Data $swHelper,
        array $data = []
    )
    {
        $this->config = $config;
        $this->helper = $helper;
        $this->greatBuyHelper = $greatBuyHelper;
        $this->redirect = $redirect;
        $this->jsonSerialize = $jsonSerialize;
        $this->helperSoftware = $helperSoftware;
        $this->customerSession = $customerSession;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->swHelper = $swHelper;
        parent::__construct($context, $data);
    }

    /**
     * Get form fields from config file
     *
     * @return array
     */
    public function getAttributesByEntity()
    {
        $attributes = [];
        $entity = $this->getData('entity');

        if (!empty($entity)) {
            $attributes = $this->config->getByEntity($entity);
        }

        return $attributes;
    }

    /**
     * Get url form action
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        $url = $this->getData('url_action') ? $this->getUrl(trim($this->getData('url_action')), ['_secure' => true]) : '#';
        return $url;
    }

    /**
     * Get array options form select and radiobox
     *
     * @param string $class
     * @return array
     */
    public function getOptions($class)
    {
        $options = [];
        if (!empty($class)) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $options = $objectManager->get($class)->toOptionArray();
        }

        return $options;
    }

    /**
     * Get value from POST by key
     *
     * @param string $key
     * @param int $index
     * @param int $subIndex
     * @return string
     */
    public function getPostValue($key, $index = -1, $subIndex = -1)
    {
        $value = [];
        $entity = $this->getData('entity');
        if (!empty($entity)) {
            $value = $this->helper->getPostValue($entity, $key, $index, $subIndex);
        }

        return $value;
    }

    /**
     * @param $key
     * @return array|string
     */
    public function getPostValueMultiArray($key)
    {
        $value = "";
        $entity = $this->getData('entity');
        if (!empty($entity)) {
            $value = $this->helper->getPostValueMultiArray($entity, $key);
        }

        return $value;
    }

    /**
     * Get serialized form fields from config file
     *
     * @return string
     */
    public function getSerializedFormFields()
    {
        return $this->jsonSerialize->serialize($this->getAttributesByEntity());
    }

    /**
     * Get URL referer
     *
     * @return string
     */
    public function getUrlReferer()
    {
        return $this->redirect->getRefererUrl();
    }

    /**
     * Get data from model
     *
     * @return object
     */
    public function getDataFromModel()
    {
        $model = null;
        $loadFiled = $this->getData('load_field');
        $column = isset($loadFiled['column']) ? $loadFiled['column'] : '';
        $param = isset($loadFiled['param']) ? $this->getRequest()->getParam($loadFiled['param'], '') : '';
        $modelClass = isset($loadFiled['model_object']) ? $loadFiled['model_object'] : '';

        if (!empty($column) && !empty($param) && !empty($modelClass)) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $model = $objectManager->create($modelClass)->load($param, $column);
        }

        return $model;
    }

    /**
     * Get data value formated
     *
     * @param object $model
     * @param string $column
     * @param int $index
     * @param int $subIndex
     * @return string
     */
    public function getDataValue($model, $column, $index = -1, $subIndex = -1)
    {
        $resultValue = '';
        $value = '';
        try {
            if ($model !== null && $column && $index === -1) {
                $value = $model->getData($column);
            } else if ($model && $column && $index !== -1) {
                $jsonValue = json_decode($model->getData($column), true);
                if (isset($jsonValue['data']) && is_array($jsonValue['data'])) {
                    if ($subIndex !== -1) {
                        foreach ($jsonValue['data'] as $key => $rows) {
                            foreach ($rows as $key2 => $item) {
                                if ($key == $index && $key2 == $subIndex) {
                                    $value = $item;
                                    return (is_numeric($value) && (float)$value == 0) ? '' : $value;
                                }
                            }
                        }
                    } else {
                        foreach ($jsonValue['data'] as $key => $item) {
                            if ($key == $index) {
                                $value = $item;
                                return (is_numeric($value) && (float)$value == 0) ? '' : $value;
                            }
                        }
                    }
                }
            }

            $resultValue = is_numeric($value) && (float)$value == 0 ? '' : $value;
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        return $resultValue;
    }

    /**
     * Get count of rows from matrix
     *
     * @param array $array
     * @return int
     */
    public function getRowsCount($array)
    {
        if (is_array($array)) {
            foreach ($array as $value) {
                if (is_array($value)) {
                    return count($value);
                }
            }
        }

        return 0;
    }

    /**
     * Get object of Helper Software
     *
     * @return \Formax\ConfigCmSoftware\Helper\Data
     */
    public function getHelperSoftware()
    {
        return $this->helperSoftware;
    }

    public function getToken()
    {
        $customerId = $this->customerSession->getCustomer()->getId();
        $customer = $this->customerRepositoryInterface->getById($customerId);
        $token = '';
        if ($customer) {
            $token = $customer->getCustomAttribute('user_rest_atk') ?
                $customer->getCustomAttribute('user_rest_atk')->getValue() : '';
        }
        return $token;
    }

    public function renderNote($field, $keyField, $format)
    {
        if (isset($field['translate']) && $field['translate']) {
            switch ($keyField) {
                case 'publication_term':
                    $field['noteText'] = __($field['noteText'], $this->helper->getMinValuePublication());
                    break;
                case 'evaluation_term':
                    $field['noteText'] = __($field['noteText'], $this->helper->getMinValueEvaluation());
                    break;
                case 'make_question_term':
                    $field['noteText'] = __($field['noteText'], $this->helper->getMinValueMakeQuestion());
                    break;
                case 'amount':
                case 'offer_price':
                    $field['noteText'] = __($field['noteText'], $this->helper->getInitialRangeQtyUTM(), $this->helper->getEndRangeQtyUTM());
                    break;
            }
        }
        return '<span class="note note-' . $keyField . ' ' . $format . '">' . $field['noteText'] . '</span>';
    }

    public function getSettingsForInputMask($field, $keyField)
    {
        $settings = [];
        if (isset($field['enabled']) && (bool)$field['enabled']) {
            switch ($keyField) {
                case 'amount':
                case 'offer_price':
                    //$currentUtm = $this->swHelper->getCurrentUtmValue();
                    //$settings['min'] =isset($field['min']) ? (int)$field['min'] : (int)$this->helper->getInitialRangeQtyUTM() * $currentUtm;
                    //$settings['max'] = isset($field['max']) ? (int)$field['max'] : (int)$this->helper->getEndRangeQtyUTM() * $currentUtm;
                    $settings['alias'] = isset($field['alias']) ? (string)$field['alias'] : '';
                    $settings['prefix'] = isset($field['prefix']) ? (string)$field['prefix'] : '';
                    $settings['digits'] = isset($field['digits']) ? (int)$field['digits'] : '';
                    $settings['autoGroup'] = isset($field['autoGroup']) ? (bool)$field['autoGroup'] : '';
                    $settings['allowMinus'] = isset($field['allowMinus']) ? (bool)$field['allowMinus'] : '';
                    $settings['rightAlign'] = isset($field['rightAlign']) ? (bool)$field['rightAlign'] : '';
                    $settings['removeMaskOnSubmit'] = isset($field['removeMaskOnSubmit']) ? (bool)$field['removeMaskOnSubmit'] : '';
                    $settings['groupSeparator'] = isset($field['groupSeparator']) ? (string)$field['groupSeparator'] : '';
                    break;
                default:
                    $settings['min'] = isset($field['min']) ? (int)$field['min'] : '';
                    $settings['max'] = isset($field['max']) ? (int)$field['max'] : '';
                    $settings['alias'] = isset($field['alias']) ? (string)$field['alias'] : '';
                    $settings['prefix'] = isset($field['prefix']) ? (string)$field['prefix'] : '';
                    $settings['digits'] = isset($field['digits']) ? (int)$field['digits'] : '';
                    $settings['autoGroup'] = isset($field['autoGroup']) ? (bool)$field['autoGroup'] : '';
                    $settings['allowMinus'] = isset($field['allowMinus']) ? (bool)$field['allowMinus'] : '';
                    $settings['rightAlign'] = isset($field['rightAlign']) ? (bool)$field['rightAlign'] : '';
                    $settings['removeMaskOnSubmit'] = isset($field['removeMaskOnSubmit']) ? (bool)$field['removeMaskOnSubmit'] : '';
                    $settings['groupSeparator'] = isset($field['groupSeparator']) ? (string)$field['groupSeparator'] : '';
            }
        }
        return $settings;
    }

}
