<?php

namespace Formax\CreateFormAttributes\Helper;

use Formax\EvaluateRequestforquote\Model\RejectedQuoteInfoFactory;
use Formax\EvaluateRequestforquote\Model\Repository\RejectedQuoteInfoRepository;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Helper\Context;
use Formax\CreateFormAttributes\Model\Config;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json as JsonSerialize;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Formax\ConfigCmSoftware\Helper\Data as SoftwareHelper;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;

class Data extends AbstractHelper
{
    /**
     * @var int
     */
    const CUSTOM_ERROR_CODE = 555;

    const CONFIG_MIN_QUESTION_DAYS = 'requestforquote/requestforquote_settings/requestforquote_questions_days';
    const CONFIG_MIN_EVALUATION_DAYS = 'requestforquote/requestforquote_settings/requestforquote_evaluation_end_days';
    const CONFIG_MIN_PUBLICATION_DAYS = 'requestforquote/requestforquote_settings/requestforquote_publication_end_days';
    const INITIAL_RANGE_QTY_UTM = 'requestforquote/requestforquote_settings/requestforquote_utm_ini';
    const END_RANGE_QTY_UTM = 'requestforquote/requestforquote_settings/requestforquote_utm_end';

    /**
     * @var use Formax\CreateFormAttributes\Model\Config
     */
    protected $config;

    /**
     * @var Magento\Framework\Serialize\Serializer\Json
     */
    protected $jsonSerialize;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $fileUploaderFactory;

    /**
     * @var Filesystem
     */
    protected $mediaDirectory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $softwareHelper;

    /**
     * @var Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var
     */
    protected $scopeConfig;

    /**
     * @var Magento\Framework\App\Request\DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var array
     */
    private $postData = null;

    /** @var RejectedQuoteInfoFactory */
    protected $_rejectedQuoteInfo;

    /** @var RejectedQuoteInfoRepository */
    protected $_rejectedQuoteInfoRepository;
    private ResourceConnection $resourceConnection;


    protected $_httpClientFactory;

    /**
     * Constructor dependency injection
     *
     * @param Magento\Framework\App\Helper\Context
     * @param Formax\CreateFormAttributes\Model\Config $config
     * @param Magento\Framework\Serialize\Serializer\Json $jsonSerialize
     * @param Formax\ConfigCmSoftware\Helper\Data $softwareHelper
     * @param Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param Psr\Log\LoggerInterface $logger
     * @param Filesystem $filesystem
     */
    public function __construct(
        Context $context,
        Config $config,
        JsonSerialize $jsonSerialize,
        SoftwareHelper $softwareHelper,
        UploaderFactory $fileUploaderFactory,
        CategoryCollectionFactory $categoryCollectionFactory,
        LoggerInterface $logger,
        Filesystem $filesystem,
        \Magento\Framework\App\RequestInterface $request,
        ResourceConnection $resourceConnection,
        RejectedQuoteInfoFactory $rejectedQuoteInfo,
        RejectedQuoteInfoRepository $rejectedQuoteInfoRepository,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
    )
    {
        $this->config = $config;
        $this->logger = $logger;
        $this->jsonSerialize = $jsonSerialize;
        $this->softwareHelper = $softwareHelper;
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_request = $request;
        $this->resourceConnection = $resourceConnection;
        $this->scopeConfig = $context->getScopeConfig();
        $this->_rejectedQuoteInfo = $rejectedQuoteInfo;
        $this->_rejectedQuoteInfoRepository = $rejectedQuoteInfoRepository;
        $this->_httpClientFactory   = $httpClientFactory;
        parent::__construct($context);
    }

    /**
     * Get Colums to create in DB from custom_attribute.xml
     *
     * @return array
     */
    public function getColumnsTable()
    {
        $result = [];
        $entity = $this->config->getAllAttributes();
        foreach ($entity as $column) {
            $result[$column['entity']]['version'] = isset($column['version']) ? $column['version'] : '';
            foreach ($column['fieldset'] as $fieldsets) {
                foreach ($fieldsets['fields'] as $key => $field) {
                    if (isset($field['type'])) {
                        $dataType = isset($field['multiple']) && $field['multiple'] ? 'text' : $field['type'];
                        if ($dataType != 'none') {
                            if (isset($field['inputFront']) && $field['inputFront'] != 'label' && $field['inputFront'] != 'button') {

                                $nulleable = isset($field['validationRule']['required']) ? (bool)$field['validationRule']['required'] : true;
                                $default = $nulleable ? null : '';
                                $len = isset($field['length']) && (int)$field['length'] > 0 ? (int)$field['length'] : false;

                                if (isset($dataType)) {
                                    switch ($dataType) {
                                        case 'date':
                                        case 'datetime':
                                            $len = null;
                                            $default = null;
                                            break;
                                        case 'integer':
                                            $len = null;
                                            $default = 0;
                                            break;
                                        case 'smallint':
                                            $len = $len ?: 2;
                                            $default = 0;
                                            break;
                                        case 'decimal':
                                            $len = '12,2';
                                            $default = 0;
                                            break;
                                        case 'varchar':
                                            $len = $len ?: 255;
                                            $dataType = 'text';
                                            break;
                                        case 'text':
                                            $len = '64K';
                                            $dataType = 'text';
                                            break;
                                        case 'boolean':
                                            $len = 1;
                                            $default = 0;
                                            break;
                                        default:
                                            $len = null;
                                            break;
                                    }
                                }

                                $attributes = [
                                    'name' => $key,
                                    'type' => $dataType,
                                    'nullable' => true,
                                    'default' => $default,
                                    'length' => $len
                                ];
                                $result[$column['entity']]['columns'][$key] = $attributes;
                            }
                        }
                    } else {
                        if (!isset($field['inputFront']) ||
                            (isset($field['inputFront']) && $field['inputFront'] != 'label' && $field['inputFront'] != 'button')) {
                            $attributes = [
                                'name' => $key,
                                'type' => 'text',
                                'nullable' => true,
                                'default' => null,
                                'length' => 255
                            ];
                            $result[$column['entity']]['columns'][$key] = $attributes;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get array form fields by entity
     *
     * @param string $entity
     * @return array
     */
    public function getByEntity($entity)
    {
        $result = [];
        $form = $this->config->getByEntity($entity);
        foreach ($form as $column) {
            foreach ($column['fieldset'] as $fieldsets) {
                foreach ($fieldsets['fields'] as $key => $field) {
                    if ((isset($field['inputFront']) && $field['inputFront'] != 'label'
                            && $field['inputFront'] != 'button') || !isset($field['inputFront'])) {
                        $result[$key] = [
                            'label' => isset($field['label']) ? trim($field['label']) : '',
                            'placeholder' => isset($field['placeholder']) ? trim($field['placeholder']) : '',
                            'type' => isset($field['inputFront']) ? $field['inputFront'] : 'text',
                            'multiple' => isset($field['multiple']) ? (bool)$field['multiple'] : false,
                            'depend' => isset($fieldsets['depend']) ? trim($fieldsets['depend']) : '',
                            'format' => isset($field['format']) ? trim($field['format']) : '',
                            'multipleFiles' => isset($field['multipleFiles']) ? (bool)$field['multipleFiles'] : false,
                            'validationRule' => isset($field['validationRule']) ? $field['validationRule'] : []
                        ];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Parse post data for saving
     *
     * @param string $entity
     * @param array $post
     * @param array|bool $files optional
     * @param array
     */
    public function parsePostData($entity, $post, $files = false)
    {
        try {
            $filteredData = [];
            $forms = $this->getByEntity($entity);
            foreach ($forms as $key => $value) {
                if (array_key_exists($key, $forms) && isset($post[$key])) {
                    if (((isset($value['type']) && $value['type'] == 'table') ||
                            (isset($value['multiple']) && $value['multiple'])) && $key != 'table_requirement') {
                        $filteredData[$key] = $this->jsonSerialize->serialize(['data' => $post[$key]]);
                    } else {
                        $filteredData[$key] = $post[$key];
                    }
                }
            }

            if ($files !== false) {
                $filesData = $this->uploadFile($entity, $files, $post, $forms);
                if (isset($filesData['error']) && isset($filesData['msg']) && $filesData['error']) {
                    throw new \Exception($filesData['msg']);
                } else {
                    foreach ($filesData as $keyFilesData => $fileData) {
                        if (array_key_exists($keyFilesData, $filteredData)) {
                            $post[$keyFilesData]['criterio_file'] = $fileData;
                        }

                        $filteredData[$keyFilesData] = $fileData;
                    }
                }
            }

            if (array_key_exists('table_requirement', $post)) {
                foreach ($forms as $key => $value) {
                    if (array_key_exists($key, $forms) && isset($post[$key])) {
                        if (((isset($value['type']) && $value['type'] == 'table') ||
                            (isset($value['multiple']) && $value['multiple']))) {
                            $filteredData[$key] = $this->jsonSerialize->serialize(['data' => $post[$key]]);
                        } else {
                            $filteredData[$key] = $post[$key];
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $filteredData = ['error' => true, 'msg' => $e->getMessage()];
        }

        return $filteredData;
    }

    /**
     * Upload file
     *
     * @param string $entity
     * @param array $files
     * @param array $post
     * @param array|bool $forms optional
     * @return array
     */
    public function uploadFile($entity, $files, $post, $forms = false)
    {
        $filteredData = [];
        if ($files) {
            try {
                $forms = $forms !== false ? $forms : $this->getByEntity($entity);
                $target = $this->mediaDirectory->getAbsolutePath('chilecompra/files/formattributes');
                foreach ($files as $key => $file) {
                    if (array_key_exists($key, $forms) && isset($forms[$key]['type']) && $forms[$key]['type'] == 'file') {
                        if ((isset($forms[$key]['multiple']) && $forms[$key]['multiple']) ||
                            (isset($forms[$key]['multipleFiles']) && $forms[$key]['multipleFiles'])) {
                            $docs = [];
                            foreach ($file as $keyItem => $item) {
                                if (isset($forms[$key]['validationRule']['required'])
                                    && $forms[$key]['validationRule']['required'] &&
                                    (!isset($item['name']) || (isset($item['name']) && empty($item['name'])))) {
                                    $label = isset($forms[$key]['label']) && $forms[$key]['label'] ? __('Field %1 is required.', $forms[$key]['label']) : __('Document field is required.');
                                    throw new LocalizedException($label);
                                }

                                /** If file was deleted */
                                $deleteFlag = false;
                                if (isset($forms[$key]['multiple']) && $forms[$key]['multiple']) {
                                    $indexDelFlag = '__remove__' . $key;
                                    $deleteFlag = isset($post[$indexDelFlag][$keyItem]) && $post[$indexDelFlag][$keyItem] == '1' ? true : false;
                                }
                                if (isset($forms[$key]['multipleFiles']) && $forms[$key]['multipleFiles']) {
                                    $indexDelFlag = '__remove__' . $key;
                                    $deleteFlag = isset($post[$indexDelFlag]) && $post[$indexDelFlag] == '1' ? true : false;
                                }

                                if (isset($item['name']) && !empty($item['name'])) {
                                    if (!$deleteFlag) {
                                        $uploader = $this->fileUploaderFactory->create(['fileId' => $item]);
                                        $fileSize = isset($item['size']) && (int)$item['size'] > 0 ? (int)$item['size'] : false;

                                        if (isset($forms[$key]['validationRule']['allowedTypes'])
                                            && is_array($forms[$key]['validationRule']['allowedTypes'])
                                            && count($forms[$key]['validationRule']['allowedTypes']) > 0) {
                                            $uploader->setAllowedExtensions($forms[$key]['validationRule']['allowedTypes']);
                                        }
                                        if ($fileSize && isset($forms[$key]['validationRule']['maxSize'])
                                            && (int)$forms[$key]['validationRule']['maxSize'] > 0) {

                                            $size = round($fileSize / 1024 / 1024.4);
                                            if ($size > (int)$forms[$key]['validationRule']['maxSize']) {
                                                throw new LocalizedException(__('File size exceed allowed. %1MB', (int)$forms[$key]['validationRule']['maxSize']));
                                            }
                                        }

                                        $uploader->setAllowRenameFiles(true);
                                        $uploader->setFilesDispersion(true);
                                        $resultFile = $uploader->save($target);

                                        if (isset($resultFile['file']) && !empty($resultFile['file'])) {
                                            $docs[$keyItem] = $resultFile['file'];
                                            $filteredData[$key] = $this->jsonSerialize->serialize(['data' => $docs]);
                                        }
                                    }
                                } else {
                                    if ($deleteFlag) {
                                        $filteredData[$key] = null;
                                    }
                                }
                            }
                        } else {
                            if (isset($forms[$key]['validationRule']['required'])
                                && $forms[$key]['validationRule']['required'] &&
                                (!isset($file['name']) || (isset($file['name']) && empty($file['name'])))) {
                                $label = isset($forms[$key]['label']) && $forms[$key]['label'] ? __('Field %1 is required.', $forms[$key]['label']) : __('Document field is required.');
                                throw new LocalizedException($label);
                            }

                            /** If file was deleted */
                            $indexDelFlag = '__remove__' . $key;
                            $deleteFlag = isset($post[$indexDelFlag]) && $post[$indexDelFlag] == '1' ? true : false;

                            if (isset($file['name']) && !empty($file['name'])) {
                                if (!$deleteFlag) {
                                    $uploader = $this->fileUploaderFactory->create(['fileId' => $key]);
                                    $fileSize = isset($file['size']) && (int)$file['size'] > 0 ? (int)$file['size'] : false;

                                    if (isset($forms[$key]['validationRule']['allowedTypes'])
                                        && is_array($forms[$key]['validationRule']['allowedTypes'])
                                        && count($forms[$key]['validationRule']['allowedTypes']) > 0) {
                                        $uploader->setAllowedExtensions($forms[$key]['validationRule']['allowedTypes']);
                                    }
                                    if ($fileSize && isset($forms[$key]['validationRule']['maxSize'])
                                        && (int)$forms[$key]['validationRule']['maxSize'] > 0) {
                                        $size = round($fileSize / 1024 / 1024.4);
                                        if ($size > (int)$forms[$key]['validationRule']['maxSize']) {
                                            throw new LocalizedException(__('File size exceed allowed. %1MB', (int)$forms[$key]['validationRule']['maxSize']));
                                        }
                                    }

                                    $uploader->setAllowRenameFiles(true);
                                    $uploader->setFilesDispersion(true);

                                    $resultFile = $uploader->save($target);

                                    if (isset($resultFile['file']) && !empty($resultFile['file'])) {
                                        $filteredData[$key] = $resultFile['file'];
                                    }
                                }
                            } else {
                                if ($deleteFlag) {
                                    $filteredData[$key] = null;
                                }
                            }
                        }
                    } elseif (is_array($file)) {
                        foreach ($file as $fileKey => $filesArray) {
                            foreach ($filesArray as $fileArrayKey => $filedata) {
                                if (isset($forms[$fileArrayKey]['validationRule']['required'])
                                    && $forms[$fileArrayKey]['validationRule']['required'] &&
                                    (!isset($filedata['name']) || (isset($filedata['name']) && empty($filedata['name'])))) {
                                    $label = isset($forms[$fileArrayKey]['label']) && $forms[$fileArrayKey]['label'] ? __('Field %1 is required.', $forms[$fileArrayKey]['label']) : __('Document field is required.');
                                    throw new LocalizedException($label);
                                }

                                if (isset($filedata['name']) && !empty($filedata['name'])) {
                                    $uploader = $this->fileUploaderFactory->create(['fileId' => $filedata]);
                                    $fileSize = isset($filedata['size']) && (int)$filedata['size'] > 0 ? (int)$filedata['size'] : false;
                                    if (isset($forms[$fileArrayKey]['validationRule']['allowedTypes'])
                                        && is_array($forms[$fileArrayKey]['validationRule']['allowedTypes'])
                                        && count($forms[$fileArrayKey]['validationRule']['allowedTypes']) > 0) {
                                        $uploader->setAllowedExtensions($forms[$fileArrayKey]['validationRule']['allowedTypes']);
                                    }
                                    if ($fileSize && isset($forms[$fileArrayKey]['validationRule']['maxSize'])
                                        && (int)$forms[$fileArrayKey]['validationRule']['maxSize'] > 0) {
                                        $size = round($fileSize / 1024 / 1024.4);
                                        if ($size > (int)$forms[$fileArrayKey]['validationRule']['maxSize']) {
                                            throw new LocalizedException(__('File size exceed allowed. %1MB', (int)$forms[$fileArrayKey]['validationRule']['maxSize']));
                                        }
                                    }

                                    $uploader->setAllowRenameFiles(true);
                                    $uploader->setFilesDispersion(true);

                                    $resultFile = $uploader->save($target);

                                    if (isset($resultFile['file']) && !empty($resultFile['file'])) {
                                        $filteredData[$key][$fileArrayKey] = $resultFile['file'];

                                    }
                                }
                            }
                        }

                    }
                }
            } catch (LocalizedException $e) {
                $this->logger->critical($e->getMessage());
                $filteredData = ['error' => true, 'msg' => $e->getMessage()];
            } catch (FileSystemException $e) {
                $this->logger->critical($e->getMessage());
                $filteredData = ['error' => true, 'msg' => $e->getMessage()];
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
                $filteredData = ['error' => true, 'msg' => $e->getMessage()];
            }
        }
        return $filteredData;
    }

    /**
     * Get category name from product
     *
     * @param Magento\Catalog\Api\Data\ProductInterface $product
     * @return string
     */
    public function getCategoryName($product)
    {
        $categoryName = '';
        $categoryIds = $product->getCategoryIds();
        $categories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('name')
            ->addAttributeToFilter('level', 3)
            ->addAttributeToFilter('entity_id', $categoryIds);

        foreach ($categories as $category) {
            $categoryName = $category->getName();
        }

        return $categoryName;
    }

    /**
     * Validate post data
     *
     * @param string $entity
     * @param array $postData
     * @param bool $validateEmptyValues
     * @return array
     */
    public function validatePostData($entity, $postData, $validateEmptyValues = true)
    {
        $result = ['error' => false, 'msg' => ''];

        try {
            $forms = $this->getByEntity($entity);
            foreach ($postData as $key => $value) {
                $label = isset($forms[$key]['label']) && $forms[$key]['label'] ? $forms[$key]['label']
                    : (isset($forms[$key]['placeholder']) && $forms[$key]['placeholder'] ? $forms[$key]['placeholder'] : $key);

                if (isset($forms[$key]['validationRule']) && is_array($forms[$key]['validationRule'])) {
                    foreach ($forms[$key]['validationRule'] as $keyRule => $rule) {
                        if (!$forms[$key]['multiple'] && $forms[$key]['type'] != 'table' && !is_array($value)) {
                            $postValue = trim($rule);

                            // Check if current field depends of another value's field
                            $depend = !empty($forms[$key]['depend']) ? explode('__', trim($forms[$key]['depend'])) : '';
                            $dependField = '';
                            $dependValue = '';
                            $mustValidate = true;
                            $countDepend = is_array($depend) ? count($depend) : 0;

                            if (is_array($depend) && $countDepend > 0) {
                                $dependField = $depend[0];
                                if (!isset($postData[$dependField])) {
                                    throw new LocalizedException(__('Depend value do not exist.'), new \Exception());
                                }

                                if ($countDepend == 2) {
                                    $dependValue = $depend[1];
                                    $mustValidate = $postData[$dependField] == $dependValue ? true : false;
                                } elseif ($countDepend == 1) {
                                    $mustValidate = $postData[$dependField] != '' ? true : false;
                                }
                            }

                            switch ($keyRule) {
                                case 'required':
                                    // Validate required
                                    if ((bool)$rule && $validateEmptyValues) {
                                        $validator = new \Zend\Validator\NotEmpty();

                                        if ($key == 'compliance_guarantee'){
                                            $currencyUTM = $this->getUTMCurrencyRate();

                                            $minUTMCLP = 300 * (int)$currencyUTM;
                                            $maxUTMCLP = $this->getEndRangeQtyUTM() * (int)$currencyUTM;
                                            if (($postData['amount'] > $minUTMCLP) && $postData['amount'] <= $maxUTMCLP){

                                                if (!$validator->isValid($value) && $mustValidate){
                                                    $msg = __('Field %1 is required.', $label);
                                                    throw new LocalizedException($msg, new \Exception());

                                                }

                                            }
                                        }else if (!$validator->isValid($value) && $mustValidate){

                                            $msg = __('Field %1 is required.', $label);
                                            throw new LocalizedException($msg, new \Exception());

                                        }

                                    }
                                    break;
                                case 'min':
                                    // Validate min value
                                    if (in_array($key, ['publication_term', 'evaluation_term', 'make_question_term'])) {
                                        switch ($key) {
                                            case "publication_term":
                                                $rule = $this->getMinValuePublication();
                                                break;
                                            case "evaluation_term":
                                                $rule = $this->getMinValueEvaluation();
                                                break;
                                            case "make_question_term":
                                                $rule = $this->getMinValueMakeQuestion();
                                                break;
                                        }
                                        if ((int)$value < (int)$rule) {
                                            $msg = __('MinValue of field %1 is %2 business days.', $label, $rule);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    } else if($key =='amount'){
                                        $currencyUTM = $this->getUTMCurrencyRate();

                                        $minUTMCLP = $this->getInitialRangeQtyUTM() * (int)$currencyUTM;
                                        if ($value <= $minUTMCLP){
                                            $msg = __('MinValue of field %1 is $%2 CLP', $label, $this->softwareHelper->currencyFormat($minUTMCLP));
                                            throw new LocalizedException($msg, new \Exception());
                                        }


                                    }else if ($validateEmptyValues) {
                                        if (!empty($value) && (int)$value < (int)$rule && $mustValidate) {
                                            $msg = __('MinValue of field %1 is %2.', $label, $rule);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    }
                                    break;
                                case 'max':
                                    // Validate max value
                                    if ($key == 'amount'){
                                        $currencyUTM = $this->getUTMCurrencyRate();
                                        $maxUTMCLP = $this->getEndRangeQtyUTM() * (int)$currencyUTM;
                                        if ($value > $maxUTMCLP){

                                            $msg = __('MaxValue of field %1 is $%2 CLP', $label, $this->softwareHelper->currencyFormat($maxUTMCLP));
                                            throw new LocalizedException($msg, new \Exception());
                                        }

                                    }else{
                                        if ($validateEmptyValues) {
                                            if ((int)$value > (int)$rule && $mustValidate) {
                                                if ($key == 'publication_term' || $key == 'evaluation_term' || $key == 'make_question_term') {
                                                    $msg = __('MaxValue of field %1 is %2 business days.', $label, $rule);
                                                } else {
                                                    $msg = __('MaxValue of field %1 is %2.', $label, $rule);
                                                }
                                                throw new LocalizedException($msg, new \Exception());
                                            }
                                        } else {
                                            if (!empty($value) && (int)$value > (int)$rule && $mustValidate) {
                                                if ($key == 'publication_term' || $key == 'evaluation_term' || $key == 'make_question_term') {
                                                    $msg = __('MaxValue of field %1 is %2 business days.', $label, $rule);
                                                } else {
                                                    $msg = __('MaxValue of field %1 is %2.', $label, $rule);
                                                }
                                                throw new LocalizedException($msg, new \Exception());
                                            }
                                        }
                                    }

                                    break;
                                case 'lowerThan':
                                    // Lower than another field or number
                                    if (ctype_digit($postValue)) {
                                        if ($validateEmptyValues) {
                                            if ((int)$value >= $rule && $mustValidate) {
                                                if ($key == 'publication_term' || $key == 'evaluation_term' || $key == 'make_question_term') {
                                                    $msg = __('Field %1 must be lower than %2 business days.', $label, $rule);
                                                } else {
                                                    $msg = __('Field %1 must be lower than %2.', $label, $rule);
                                                }
                                                throw new LocalizedException($msg, new \Exception());
                                            }
                                        } else {
                                            if (!empty($value) && (int)$value >= $rule && $mustValidate) {
                                                if ($key == 'publication_term' || $key == 'evaluation_term' || $key == 'make_question_term') {
                                                    $msg = __('Field %1 must be lower than %2 business days.', $label, $rule);
                                                } else {
                                                    $msg = __('Field %1 must be lower than %2.', $label, $rule);
                                                }
                                                throw new LocalizedException($msg, new \Exception());
                                            }
                                        }
                                    } else {
                                        if (isset($postData[$postValue]) && $postData[$postValue] != '') {
                                            if ($validateEmptyValues) {
                                                if ((int)$value >= (int)$postData[$postValue] && $mustValidate) {
                                                    $msg = __('Field %1 must be lower than %2.', $label, $forms[$postValue]['label']);
                                                    throw new LocalizedException($msg, new \Exception());
                                                }
                                            } else {
                                                if (!empty($value) && (int)$value >= (int)$postData[$postValue] && $mustValidate) {
                                                    $msg = __('Field %1 must be lower than %2.', $label, $forms[$postValue]['label']);
                                                    throw new LocalizedException($msg, new \Exception());
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case 'greatherThan':
                                    // Greather than another field or number
                                    if (ctype_digit($postValue)) {
                                        if ($validateEmptyValues) {
                                            if ((int)$value <= $rule && $mustValidate) {
                                                $msg = __('Field %1 must be greather than %2.', $label, $rule);
                                                throw new LocalizedException($msg, new \Exception());
                                            }
                                        } else {
                                            if (!empty($value) && (int)$value <= $rule && $mustValidate) {
                                                $msg = __('Field %1 must be greather than %2.', $label, $rule);
                                                throw new LocalizedException($msg, new \Exception());
                                            }
                                        }
                                    } else {
                                        if (isset($postData[$postValue]) && $postData[$postValue] != '') {
                                            if ($validateEmptyValues) {
                                                if ((int)$value <= (int)$postData[$postValue] && $mustValidate) {
                                                    $msg = __('Field %1 must be greather than %2.', $label, $forms[$postValue]['label']);
                                                    throw new LocalizedException($msg, new \Exception());
                                                }
                                            } else {
                                                if (!empty($value) && (int)$value <= (int)$postData[$postValue] && $mustValidate) {
                                                    $msg = __('Field %1 must be greather than %2.', $label, $forms[$postValue]['label']);
                                                    throw new LocalizedException($msg, new \Exception());
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case 'regex':
                                    // Regex expression
                                    $validator = new \Zend\Validator\Regex(['pattern' => $postValue]);
                                    if ($validateEmptyValues) {
                                        if (!$validator->isValid($value) && $mustValidate) {
                                            $msg = __('Field %1 not allow these characters.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    } else {
                                        if (!empty($value) && !$validator->isValid($value) && $mustValidate) {
                                            $msg = __('Field %1 not allow these characters.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    }
                                    break;
                                case 'rut':
                                    // Chilenean RUT
                                    if ($validateEmptyValues) {
                                        if (!$this->validateRut($value) && $mustValidate) {
                                            $msg = __('Field %1 value is invalid.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    } else {
                                        if (!empty($value) && !$this->validateRut($value) && $mustValidate) {
                                            $msg = __('Field %1 value is invalid.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    }
                                    break;;
                                case 'email':
                                    //Email address
                                    $validator = new \Zend\Validator\EmailAddress();
                                    if ($validateEmptyValues) {
                                        if (!$validator->isValid($value) && $mustValidate) {
                                            $msg = __('Field %1 must be an email address.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    } else {
                                        if (!empty($value) && !$validator->isValid($value) && $mustValidate) {
                                            $msg = __('Field %1 must be an email address.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    }
                                    break;
                                case 'letter':
                                    // Only letters
                                    $validator = new \Zend\Validator\Regex(['pattern' => "/^[a-z áéíóúñüÁÉÍÓÚÑÜ]+$/i"]);
                                    if ($validateEmptyValues) {
                                        if (!$validator->isValid($value) && $mustValidate) {
                                            $msg = __('Field %1 must be only letters.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    } else {
                                        if (!empty($value) && !$validator->isValid($value) && $mustValidate) {
                                            $msg = __('Field %1 must be only letters.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    }
                                    break;
                                case 'integer':
                                    // Integer greather than zero
                                    $validator = new \Zend\Validator\Regex(['pattern' => "/[^1-9]/"]);
                                    if ($validateEmptyValues) {
                                        if (!$validator->isValid($value) && $mustValidate) {
                                            $msg = __('Field %1 must be only numbers greather than zero.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    } else {
                                        if (!empty($value) && !$validator->isValid($value) && $mustValidate) {
                                            $msg = __('Field %1 must be only numbers greather than zero.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    }
                                    break;
                                case 'digits':
                                    // Integer greather or equal than zero
                                    $validator = new \Zend\Validator\Digits();
                                    if ($validateEmptyValues) {
                                        if (!$validator->isValid($value) && $mustValidate) {
                                            $msg = __('Field %1 must be only numbers between 0 - 9.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    } else {
                                        if (!empty($value) && !$validator->isValid($value) && $mustValidate) {
                                            $msg = __('Field %1 must be only numbers between 0 - 9.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    }
                                    break;
                                default:
                            }
                        } else if (is_array($value)) {

                            switch ($key) {
                                case 'deliverable_services':
                                    foreach ($value as $key_value => $val) {
                                        foreach ($val as $key_v => $v) {
                                            $validator = new \Zend\Validator\NotEmpty();
                                            if (!$validator->isValid($v)) {
                                                $msg = __('Field %1 is incomplete and is required', $label);
                                                throw new LocalizedException($msg, new \Exception());
                                            }

                                        }
                                    }
                                    break;
                                case 'minimum_offer_required':
                                    /**
                                     * TODO
                                     * Mejorar con traducción y especificando quien falta después de arrglar el storage de la data al
                                     * regresar al formulario después de un error
                                     */
                                    if ($postData['evaluation_modality'] == "1"){
                                        foreach ($value as $val)
                                            $validator = new \Zend\Validator\NotEmpty();
                                        if (!$validator->isValid($val)) {

                                            $msg = __('Field %1 is required.', $label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    }else{
                                        $evaluation_criterio_data = $this->processDataEvaluationCriteria($postData['table_evaluation_criteria']);
                                        if ($evaluation_criterio_data['error']){
                                            $msg = __($evaluation_criterio_data['msg'],$label);
                                            throw new LocalizedException($msg, new \Exception());
                                        }
                                    }

                                    break;

                            }
                        }
                    }
                }
            }
        } catch (LocalizedException $e) {
            $result['error'] = true;
            $result['msg'] = $e->getMessage();
        } catch (\Exception $e) {
            $result['error'] = true;
            $result['msg'] = $e->getMessage();
        }

        return $result;
    }


    public function processDataEvaluationCriteria($data){
        $labels = $data['evaluation_label'];
        unset($data['evaluation_label']);
        $sumpercent = 0;
        $noempty_fields_weight = 0;
        $noempty_fields_obs = 0;

        $labelSection = 'Criterios de Evalución';

        foreach ($data as $key => $value) {
            if (isset($labels[$key])) {
                $label = $labels[$key];
                if (!empty($value['weight']) || !empty($value['observations'])) {
                    !empty($value['weight']) ? $noempty_fields_weight ++ : $noempty_fields_weight;
                    !empty($value['observations']) ? $noempty_fields_obs ++ : $noempty_fields_obs;

                    if (!is_numeric($value['weight'])) {
                        $msg = __('Field weight in %1 must be numeric.', $label, $labelSection );
                        return ["error"=>true,"msg"=>$msg];
                    } else {
                        $sumpercent = $sumpercent + (int)$value['weight'];

                    }
                }
            }
        }

        if (($noempty_fields_weight != $noempty_fields_obs) || ($noempty_fields_obs == 0 && $noempty_fields_weight == 0)){
            $msg = __('It must weigh at least one acceptance criterion and be complete with its respective evaluation.');

             return ["error"=>true,"msg"=>$msg];


        } else if ($sumpercent == 100) {
            return ["error"=>false];
        }else{
            $msg = __('The weight sum of the %1 section must be 100.', $labelSection );
            return ["error"=>true,"msg"=>$msg];
        }
    }

    /**
     * Validate RUT
     *
     * @param string $rut
     * @return bool
     */
    public function validateRut($rut)
    {
        $validate = true;

        if ($rut != '') {
            if (!preg_match("/^[0-9]+[-]{1}+[0-9kK]{1}/", $rut)) {
                $validate = false;
            }

            $rut = preg_replace('/[\.\-]/i', '', $rut);
            $dv = substr($rut, -1);
            $numero = substr($rut, 0, strlen($rut) - 1);
            $i = 2;
            $suma = 0;

            foreach (array_reverse(str_split($numero)) as $v) {
                if ($i == 8) {
                    $i = 2;
                }

                $suma += $v * $i;
                ++$i;
            }

            $dvr = 11 - ($suma % 11);
            if ($dvr == 11) {
                $dvr = 0;
            }
            if ($dvr == 10) {
                $dvr = 'K';
            }
            if ($dvr != strtoupper($dv)) {
                $validate = false;
            }
        }

        return $validate;
    }

    /**
     * Get Data Persistor
     *
     * @return Magento\Framework\App\Request\DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }

    /**
     * @param $entity
     * @param $key
     * @return array|string
     */
    public function getPostValueMultiArray($entity, $key)
    {
        $postValue = "";

        if (isset($this->postData[$key])) {
            if (is_array($this->postData[$key])) {
                $postValue = $this->postData[$key];
            }
        }

        return $postValue;
    }

    /**
     * Get value from POST by key
     *
     * @param string $entity
     * @param string $key
     * @param int $index
     * @param int $subIndex
     * @return string
     */
    public function getPostValue($entity, $key, $index = -1, $subIndex = -1)
    {
        $postValue = '';
        $keyDataPersistor = 'formattributes_' . $entity;

        if (null === $this->postData) {
            $this->postData = (array)$this->getDataPersistor()->get($keyDataPersistor);
            $this->getDataPersistor()->clear($keyDataPersistor);
        }

        $dataVal = $this->getByEntity($entity);
        if (isset($this->postData[$key])) {
            if (is_array($this->postData[$key]) && $index !== -1) {
                if ($subIndex !== -1) {
                    if (isset($this->postData[$key][$index][$subIndex])) {
                        $postValue = $this->postData[$key][$index][$subIndex];
                    }
                } else {
                    if (isset($this->postData[$key][$index])) {
                        $postValue = $this->postData[$key][$index];
                    }
                }
            } else {
                $postValue = $this->postData[$key];
            }
        }

        return $postValue;
    }

    public function getLoadValue($entity, $key, $index = -1, $subIndex = -1, $isLoad = false)
    {
        if ($isLoad) {
            if ($entity == "requestforquote_quote_conversation") {
                $quoteId = $this->_request->getParam("id");
                $resourceConnection = $this->resourceConnection->getConnection();
                $select = $resourceConnection->select();
                $select->from($entity, $key);
                $select->where('seller_quote_id = ' . $quoteId);
                $select->where('updated_at = (select max(updated_at) from ' . $entity . ' where seller_quote_id = ' . $quoteId . ' )');
                if ($index >= 0) {
                    $values = $resourceConnection->fetchOne($select);
                    $values = json_decode($values, true);
                    return (isset($values['data'][$index][$subIndex]) ? $values['data'][$index][$subIndex] : '');
                }
                $ret = $resourceConnection->fetchOne($select);
                if ($this->isJSON($ret)) {
                    $ret = json_decode($ret);
                }
                return $ret;
            }
            if ($entity == "requestforquote_quote_reject") {
                $quoteId = $this->_request->getParam("quote_id");
                if (sizeof($this->_rejectedQuoteInfoRepository->getByQuoteId($quoteId)->getData())) {
                    $rejectQuoteInfo = $this->_rejectedQuoteInfoRepository->getByQuoteId($quoteId);
                    $ret = $rejectQuoteInfo->getRejectedReportDocuments();
                    if ($this->isJSON($ret)) {
                        $ret = json_decode($ret);
                    }
                    return $ret;
                }
            }
        }

        return false;
    }

    /**
     * Get if value is json format
     *
     * @return bool
     */
    public function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }

    /**
     * Get min value form publication term Setting Config
     *
     * @return bool
     */
    public function getMinValuePublication()
    {
        return (int)$this->getConfig(self::CONFIG_MIN_PUBLICATION_DAYS);
    }

    /**
     * Get min value form evaluation term Setting Config
     *
     * @return int
     */
    public function getMinValueEvaluation()
    {
        return (int)$this->getConfig(self::CONFIG_MIN_EVALUATION_DAYS);
    }

    /**
     * Get min value form make quuestion term Setting Config
     *
     * @return int
     */
    public function getMinValueMakeQuestion()
    {
        return (int)$this->getConfig(self::CONFIG_MIN_QUESTION_DAYS);
    }

    public function getInitialRangeQtyUTM()
    {
        return (int)$this->getConfig(self::INITIAL_RANGE_QTY_UTM);
    }

    public function getEndRangeQtyUTM()
    {
        return (int)$this->getConfig(self::END_RANGE_QTY_UTM);
    }

    protected function getConfig($configPath)
    {
        return $this->scopeConfig->getValue($configPath, ScopeInterface::SCOPE_STORE);
    }

    public function getUTMCurrencyRate($currency = 'CLP'): float
    {
        $rate = 1.0;

        try {
            $endpoint = $this->getConfig('dccp_endpoint/greatbuy/currency_endpoint');
            $client = $this->_httpClientFactory->create();
            $client->setUri($endpoint);
            $response = $client->request(\Zend_Http_Client::GET)->getBody();
            $obj = json_decode($response);

            foreach($obj->payload as $data){
                if ($currency == 'CLP') {
                    if ($data->moneda == 'UTM'){
                        $rate = round($data->valorPeso, $data->decimales);
                    }
                } else {
                    if ($data->moneda == 'USD'){
                        $rate = round($data->valorPeso, $data->decimales);
                    }
                }
            }
        } catch(\Exception $err) {
            ($currency == 'CLP') ? $this->logger->info("UTM Price in the day: ".$err->getMessage()) : $this->logger->info("USD Price in the day: ".$err->getMessage());
        }

        return $rate;
    }

}
