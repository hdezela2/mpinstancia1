define([
    'jquery',
    'Formax_CreateFormAttributes/js/vendor/dist/jquery.inputmask.min',
    'domReady!'
], function ($) {
    "use strict";
    function main(config) {
        var settings = config.settings;
        var formId = config.formId;
        var input = $('#' + config.input);
        input.inputmask(settings);
    }
    return main;
});

