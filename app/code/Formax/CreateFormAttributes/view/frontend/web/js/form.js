define([
  "jquery",
  "ko",
  "Magento_Ui/js/modal/confirm"
], function($, ko, modal) {
	"use strict";
  	(function($, window, document, undefined) {
		function getReadableFileSizeString(fileSizeInBytes) {
			var i = -1;
			var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
			do {
				fileSizeInBytes = fileSizeInBytes / 1024;
				i++;
			} while (fileSizeInBytes > 1024);

			return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
		};

    	$(".inputfile").each(function() {
      		var $input = $(this),
        	$label = $input.next("label"),
        	labelVal = $label.html(),
      		filesElement = $input.parent().siblings('.pending-files-area');

      		var fileName = "";
      		var files;
      		$input.on("change", function(e) {
				var id = $input.attr("id");
				if (this.files && this.files.length) {
					files = Array.from(this.files).map(function(item) {
						return '<div class="file-item" rel="' + item.name + '">' +
						'<span>' + item.name + ' (' + getReadableFileSizeString(item.size) + ') </span></div>';
					});
					fileName = this.files.length + " archivos cargados";
					$("#__remove__" + id).prop("checked", false);
					$("#__remove__" + id).val("");
				}


				if (fileName) {
					$label.find("span").html(fileName);
					if (files) {
						var uploadedFiles = '';
						var alreadyExists = 0;
						if (id === 'offer_document') {
							$('.uploaded-files-area .download-file').each(function() {
								uploadedFiles += ' ' +$(this).find('span').html();
							});
							files.forEach(function(item) {
								var name = $(item).attr('rel');
								var nameWithoutExtension = name.split('.').splice(0, name.split('.').length - 1).join('.');
								if (uploadedFiles.indexOf(nameWithoutExtension) > -1) {
									alreadyExists += 1;
								}
							});
						}
						if (!alreadyExists) {
							filesElement.show().children('.pending-files').html('').append(files.join(''));
							filesElement.find('.remove').show();
							filesElement.find('.error').hide();
							if (id !== 'offer_document' && id !== 'file' && id !== 'rejected_report_documents') {
								$(this).parent('.control').hide();
							}
						} else {
							filesElement.show().find('.error').show();
							filesElement.find('.remove').hide();
							$('#offer_document').val("");
							filesElement.children('.pending-files').html('');
						}
					}
				} else {
					$label.html(labelVal);
				}
			});

			// Firefox bug fix
			$input.on("focus", function() {
				$input.addClass("has-focus");
			}).on("blur", function() {
				$input.removeClass("has-focus");
			});
		});

    	$('.pending-files-area').on('click', function(e) {
    		if (e.target.className.indexOf('__remove__pending-files') > -1) {
				$('#offer_document').val("");
				$(this).hide().children('.pending-files').html("");
				$(this).find('.error').hide();
			}
			if (e.target.className.indexOf('__remove__pending-file') > -1) {
				$(this).hide().children('.pending-files').html("");
				$(this).siblings('.control').show().find('.inputfile').val("");
			}
		})

		// delete button of input files
		$(".uploaded-files-area .__remove__").on("change", function() {
			var objThis = $(this);
			var id = objThis.attr("id");
			objThis.prop("checked", false);
			objThis.val("");

			modal({
				title: $.mage.__("Confirmaci\xf3n"),
				content: "<p>" + $.mage.__("\xbfEst\xe1 seguro de eliminar los archivos?") + "</p>",
				actions: {
					confirm: function() {
						var _id = id.split("__remove__");
						objThis.prop("checked", true);
						objThis.val("1");
						$("#" + _id[1]).val("");
						$(".__files__" + _id[1]).hide();
					},
					cancel: function() {},
					always: function() {}
				},
				buttons: [{
					text: $.mage.__('Cancel'),
					class: "action-secondary action-dismiss",
					click: function (event) {
						this.closeModal(event);
					}
				}, {
					text: $.mage.__("OK"),
					class: "action-primary action-accept",
					click: function (event) {
						this.closeModal(event, true);
					}
				}]
			});
		});
	})(jQuery, window, document);

	var FormFields = {
		initForm: function(config) {
      		//console.log(config.fields);
    	}
  	};
  	return {
    	"formattributes": FormFields.initForm
  	};
});
