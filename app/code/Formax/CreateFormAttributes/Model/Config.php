<?php

namespace Formax\CreateFormAttributes\Model;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Logging configuration
 */
class Config
{
    /**
     * @var \Magento\Framework\Config\DataInterface
     */
    protected $_dataStorage;

    /**
     * @param \Magento\Framework\Config\DataInterface $dataStorage
     */
    public function __construct(\Magento\Framework\Config\DataInterface $dataStorage)
    {
        $this->_dataStorage = $dataStorage;
    }

    /**
     * Get all array config from custom_attributes.xml
     * 
     * @return array
     */
    public function getAllAttributes()
    {
        return $this->_dataStorage->get('custom_attributes');
    }

    /**
     * Get all array config from custom_attributes.xml
     * by entity type
     * 
     * @param string $entity
     * @return array
     */
    public function getByEntity($entity)
    {
        $newAttributes = [];
        $attributes = $this->_dataStorage->get('custom_attributes');
        foreach ($attributes as $key => $attribute) {
            if (isset($attribute['entity']) && $attribute['entity'] == $entity) {
                $newAttributes[$key] = $attribute;
            }
        }

        return $newAttributes;
    }   

    /**
     * Get all array config from custom_attributes.xml
     * by form and entity type
     * 
     * @param string $entity
     * @param string $form
     * @return array
     */
    public function getByForm($entity, $form)
    {
        $newAttributes = [];
        $attributes = $this->_dataStorage->get('custom_attributes');
        foreach ($attributes as $key => $attribute) {
            if ($key == $form && isset($attribute['entity']) && $attribute['entity'] == $entity) {
                $newAttributes[$key] = $attribute;
            }
        }

        return $newAttributes;
    }

    /**
     * Get all array config from custom_attributes.xml
     * by form and entity type
     * 
     * @param string $entity
     * @param string $form
     * @param array $fieldsets
     * @return array
     */
    public function getByFieldset($entity, $form, $fieldsets)
    {
        $newAttributes = [];
        $attributes = $this->_dataStorage->get('custom_attributes');
        foreach ($attributes as $key => $attribute) {
            if ($key == $form && isset($attribute['entity']) && $attribute['entity'] == $entity) {
                $newAttributes[$key]['entity'] = $attribute['entity'];
                if (isset($attribute['fieldset']) && is_array($fieldsets)) {
                    foreach ($attribute['fieldset'] as $keyFieldset => $fieldsetsValue) {
                        if (in_array($keyFieldset, $fieldsets)) {
                            $newAttributes[$key]['fieldset'][$keyFieldset]['label'] = $fieldsetsValue['label'];
                            $newAttributes[$key]['fieldset'][$keyFieldset]['sortOrder'] = $fieldsetsValue['sortOrder'];
                            $newAttributes[$key]['fieldset'][$keyFieldset]['depend'] = $fieldsetsValue['depend'];
                            $newAttributes[$key]['fieldset'][$keyFieldset]['class'] = $fieldsetsValue['class'];
                            $newAttributes[$key]['fieldset'][$keyFieldset]['fields'] = $fieldsetsValue['fields'];
                        }
                    }
                }
            }
        }

        return $newAttributes;
    }
}
