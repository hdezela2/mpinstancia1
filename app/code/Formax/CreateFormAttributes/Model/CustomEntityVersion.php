<?php

namespace Formax\CreateFormAttributes\Model;

use Magento\Framework\Model\AbstractModel;

class CustomEntityVersion extends AbstractModel
{
    /**
     * Cache tag.
     */
    const CACHE_TAG = 'formax_custom_entity_version';

    /**
     * @var string
     */
    protected $_cacheTag = 'formax_custom_entity_version';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'formax_custom_entity_version';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\CreateFormAttributes\Model\ResourceModel\CustomEntityVersion::class);
    }

    /**
     * Retrive row by entity name
     * 
     * @param string $entity
     * @return array
     */
    public function loadByEntity($entity)
    {
        if ($entity) {
            $row = $this->getResource()->loadByEntity($entity);
        }
        
        return isset($row['entity_id']) && (int)$row['entity_id'] > 0
            ? $row : false;
    }
}