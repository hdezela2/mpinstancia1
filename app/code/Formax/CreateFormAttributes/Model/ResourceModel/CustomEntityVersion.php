<?php

namespace Formax\CreateFormAttributes\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CustomEntityVersion extends AbstractDb
{
    public function _construct()
    {
        $this->_init('formax_custom_entity_version', 'entity_id');
    }

    /**
     * Retrive row by entity name
     * 
     * @param string $entity
     * @return array
     */
    public function loadByEntity($entity)
    {
        $table = $this->getMainTable();
        $entityQuote = $this->getConnection()->quoteInto('entity_name = ?', $entity);
        $sql = $this->getConnection()->select()->from($table, ['entity_id', 'entity_name', 'version'])
            ->where($entityQuote);
        $row = $this->getConnection()->fetchRow($sql);
        
        return $row;
    }
}