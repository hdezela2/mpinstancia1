<?php

namespace Formax\CreateFormAttributes\Model\ResourceModel\CustomEntityVersion;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            \Formax\CreateFormAttributes\Model\CustomEntityVersion::class,
            \Formax\CreateFormAttributes\Model\ResourceModel\CustomEntityVersion::class
        );
    }
}
