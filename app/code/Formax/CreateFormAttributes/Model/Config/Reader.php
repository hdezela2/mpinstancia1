<?php

namespace Formax\CreateFormAttributes\Model\Config;

class Reader extends \Magento\Framework\Config\Reader\Filesystem
{
    /**
     * List of identifier attributes for merging
     *
     * @var array
     */
    protected $_idAttributes = [
        '/custom_attributes/form' => 'id',
        '/custom_attributes/form/fieldset' => 'id',
        '/custom_attributes/form/fieldset/field' => 'id',
    ];
}