<?php

namespace Formax\CreateFormAttributes\Model\Config;

class Converter implements \Magento\Framework\Config\ConverterInterface
{
    /**
    * @var array
    */
    protected $validationAttr = [
        'required',
        'min',
        'max',
        'regex',
        'allowedTypes',
        'maxSize',
        'lowerThan',
        'greatherThan',
        'alertMinValue',
        'alertMaxValue',
        'rut',
        'email'
    ];

    protected $noteAttr = [
        'translate',
        'noteText'
    ];

    /**
    * @var array
    */
    protected $inputFrontAttr = [
        'text',
        'textmask',
        'label',
        'textarea',
        'hidden',
        'select',
        'radiobox',
        'checkbox',
        'file',
        'table',
        'button',
        'pharagraph',
        'span',
        'date'
    ];

    /**
     * @var array
     */
    protected $inputMaskAttr = [
        'enabled',
        'digits',
        'groupSeparator',
        'autoGroup',
        'allowMinus',
        'rightAlign',
        'removeMaskOnSubmit',
        'prefix',
        'alias',
        'min',
        'max'
    ];

    /**
    * @var array
    */
    protected $loadAttr = [
        true,
        false
    ];

    /**
    * @var array
    */
    protected $columnTypeAttr = [
        'varchar',
        'text',
        'integer',
        'smallint',
        'decimal',
        'datetime',
        'date',
        'boolean',
        'none'
    ];

    /**
    * @var array
    */
    protected $formatAttr = [
        'currency',
        'percent',
        'date',
        'datetime',
        'decimal'
    ];

    /**
    * @var array
    */
    protected $buttonTypeAttr = [
        'add',
        'addLink',
        'draft',
        'submit',
        'primary',
        'secundary',
        'none'
    ];

    /**
     * Convert dom node tree to array
     *
     * @param \DOMDocument $source
     * @return array
     * @throws \InvalidArgumentException
     */
    public function convert($source)
    {
        $fields = [];
        $fieldsForm = ['custom_attributes' => []];
        $xpath = new \DOMXPath($source);
        $formArray = [];

        foreach ($xpath->query('/custom_attributes/form') as $form) {
            $fieldsetArray = [];
            $formAttributes = $form->attributes;
            $formId = $formAttributes->getNamedItem('id') !== null ? $formAttributes->getNamedItem('id')->nodeValue : '';
            $entity = $formAttributes->getNamedItem('entity') !== null ? $formAttributes->getNamedItem('entity')->nodeValue : '';
            $version = $formAttributes->getNamedItem('version') !== null ? $formAttributes->getNamedItem('version')->nodeValue : '1.0.0';

            if (empty($formId)) {
                throw new \LogicException(sprintf('"form" node must have "id" attribute.'));
            }
            if (!$this->validateIdAttribute($formId)) {
                throw new \LogicException(sprintf('"form id" node must only have letters and underscore. You define "%s"', $formId));
            }
            if (empty($entity)) {
                throw new \LogicException(sprintf('"form" node must have "entity" attribute.'));
            }
            if (!$this->validateIdAttribute($entity)) {
                throw new \LogicException(sprintf('"entity id" node must only have letters and underscore. You define "%s"', $entity));
            }

            $fieldsetArray = $this->_convertFieldsets($form);
            $keys = array_column($fieldsetArray, 'sortOrder');
            array_multisort($keys, SORT_ASC, $fieldsetArray);
            $formArray[$formId] = [
                'entity' => $entity,
                'version' => $version,
                'fieldset' => $fieldsetArray
            ];
        }
        $fieldsForm = ['custom_attributes' => $formArray];

        return $fieldsForm;
    }

    /**
     * Convert dom node tree to array
     *
     * @param \DOMDocument $source
     * @return array
     * @throws \InvalidArgumentException
     */
    protected function _convertFieldsets($source)
    {
        $fieldsetArray = [];
        foreach ($source->childNodes as $fieldset) {
            if ($fieldset->nodeName === '#text' || $fieldset->nodeName === '#comment') {
                continue;
            }

            $subNodeAttributes = $fieldset->attributes;
            $fieldsetId = $subNodeAttributes->getNamedItem('id') !== null ? $subNodeAttributes->getNamedItem('id')->nodeValue : '';

            if (empty($fieldsetId)) {
                throw new \LogicException(sprintf('"fieldset" node must have "id" attribute.'));
            }
            if (!$this->validateIdAttribute($fieldsetId)) {
                throw new \LogicException(sprintf('"fieldset id" node must only have letters and underscore. You define "%s"', $fieldsetId));
            }

            $sortOrder = $subNodeAttributes->getNamedItem('sortOrder') !== null ? $subNodeAttributes->getNamedItem('sortOrder')->nodeValue : 0;
            $label = $subNodeAttributes->getNamedItem('label') !== null ? $subNodeAttributes->getNamedItem('label')->nodeValue : '';
            $depend = $subNodeAttributes->getNamedItem('depend') !== null ? $subNodeAttributes->getNamedItem('depend')->nodeValue : '';
            $class = $subNodeAttributes->getNamedItem('class') !== null ? $subNodeAttributes->getNamedItem('class')->nodeValue : '';

            $fieldsArray = $this->_convertFields($fieldset);
            $keys = array_column($fieldsArray, 'sortOrder');
            array_multisort($keys, SORT_ASC, $fieldsArray);

            $fieldsetArray[$fieldsetId]['label'] = $label;
            $fieldsetArray[$fieldsetId]['sortOrder'] = (int)$sortOrder;
            $fieldsetArray[$fieldsetId]['depend'] = $depend;
            $fieldsetArray[$fieldsetId]['class'] = $class;
            $fieldsetArray[$fieldsetId]['fields'] = $fieldsArray;
        }

        return $fieldsetArray;
    }

    /**
     * Convert dom field node to Magento array
     *
     * @param \DOMNode $source
     * @return array
     * @throws \LogicException
     */
    protected function _convertFields($source)
    {
        $fieldArray = [];
        foreach ($source->childNodes as $formFields) {
            if ($formFields->nodeName === '#text' || $formFields->nodeName === '#comment') {
                continue;
            }

            if ($formFields->nodeName == 'field') {
                $subNodeAttributes = $formFields->attributes;
                $fieldId = $subNodeAttributes->getNamedItem('id') !== null ? $subNodeAttributes->getNamedItem('id')->nodeValue : '';

                if (empty($fieldId)) {
                    throw new \LogicException(sprintf('"field" node must have "id" attribute.'));
                }
                if (!$this->validateIdAttribute($fieldId)) {
                    throw new \LogicException(sprintf('"field id" node must only have letters and underscore. You define "%s"', $fieldId));
                }

                $sortOrder = $subNodeAttributes->getNamedItem('sortOrder') !== null ? $subNodeAttributes->getNamedItem('sortOrder')->nodeValue : 0;
                $multiple = $subNodeAttributes->getNamedItem('multiple') !== null ? true : false;
                $group = $subNodeAttributes->getNamedItem('group') !== null ? $subNodeAttributes->getNamedItem('group')->nodeValue : false;
                $length = $subNodeAttributes->getNamedItem('length') !== null ? (int)$subNodeAttributes->getNamedItem('length')->nodeValue : false;
                $class = $subNodeAttributes->getNamedItem('class') !== null ? $subNodeAttributes->getNamedItem('class')->nodeValue : false;
                $fieldArray[$fieldId]['sortOrder'] = (int)$sortOrder;
                $fieldArray[$fieldId]['multiple'] = $multiple;
                $fieldArray[$fieldId]['group'] = $group;
                $fieldArray[$fieldId]['length'] = (int)$length;
                $fieldArray[$fieldId]['class'] = $class;

                foreach ($formFields->childNodes as $formTag) {
                    switch ($formTag->nodeName) {
                        case 'inputFront':
                            if (in_array($formTag->nodeValue, $this->inputFrontAttr)) {
                                $fieldArray[$fieldId]['inputFront'] = $formTag->nodeValue;
                            } else {
                                throw new \LogicException(sprintf('Invalid value "%s" for "%s" tag', $formTag->nodeValue, $formTag->nodeName));
                            }
                            break;
                        case 'load':
                            if (in_array($formTag->nodeValue, $this->loadAttr)) {
                                $fieldArray[$fieldId]['load'] = $formTag->nodeValue;
                            } else {
                                throw new \LogicException(sprintf('Invalid value "%s" for "%s" tag', $formTag->nodeValue, $formTag->nodeName));
                            }
                            break;
                        case 'type':
                            if (in_array($formTag->nodeValue, $this->columnTypeAttr)) {
                                $fieldArray[$fieldId]['type'] = $formTag->nodeValue;
                            } else {
                                throw new \LogicException(sprintf('Invalid value "%s" for "%s" tag', $formTag->nodeValue, $formTag->nodeName));
                            }
                            break;
                        case 'label':
                            $fieldArray[$fieldId]['label'] = $formTag->nodeValue;
                            break;
                        case 'multipleFiles':
                            $fieldArray[$fieldId]['multipleFiles'] = filter_var($formTag->nodeValue, FILTER_VALIDATE_BOOLEAN);
                            break;
                        case 'textValue':
                            $fieldArray[$fieldId]['textValue'] = $formTag->nodeValue;
                            break;
                        case 'class':
                            $fieldArray[$fieldId]['class'] = $formTag->nodeValue;
                            break;
                        case 'validationRule':
                            $fieldArray[$fieldId]['validationRule'] = $this->_convertValidationRules($formTag);
                            break;
                        case 'placeholder':
                            $fieldArray[$fieldId]['placeholder'] = $formTag->nodeValue;
                            break;
                        case 'format':
                            if (in_array($formTag->nodeValue, $this->formatAttr)) {
                                $fieldArray[$fieldId]['format'] = $formTag->nodeValue;
                            } else {
                                throw new \LogicException(sprintf('Invalid value "%s" for "%s" tag', $formTag->nodeValue, $formTag->nodeName));
                            }
                            break;
                        case 'readOnly':
                            $fieldArray[$fieldId]['readOnly'] = filter_var($formTag->nodeValue, FILTER_VALIDATE_BOOLEAN);
                            break;
                        case 'readOnlyOnEdit':
                            $fieldArray[$fieldId]['readOnlyOnEdit'] = filter_var($formTag->nodeValue, FILTER_VALIDATE_BOOLEAN);
                            break;
                        case 'note':
                            $fieldArray[$fieldId]['note'] = $this->_convertNote($formTag);
                            break;
                        case 'inputMask':
                            $fieldArray[$fieldId]['inputMask'] = $this->_convertInputMask($formTag);
                            break;
                        case 'comment':
                            $fieldArray[$fieldId]['comment'] = $formTag->nodeValue;
                            break;
                        case 'options':
                            $class = trim($formTag->nodeValue);
                            $fieldArray[$fieldId]['options'] = $class;
                            break;
                        case 'optionDefault':
                            $fieldArray[$fieldId]['optionDefault'] = trim($formTag->nodeValue);
                            break;
                        case 'checked':
                            $fieldArray[$fieldId]['checked'] = filter_var($formTag->nodeValue, FILTER_VALIDATE_BOOLEAN);
                            break;
                        case 'fixed':
                            $fieldArray[$fieldId]['fixed'] = filter_var($formTag->nodeValue, FILTER_VALIDATE_BOOLEAN);
                            break;
                        case 'buttonType':
                            if (in_array($formTag->nodeValue, $this->buttonTypeAttr)) {
                                $fieldArray[$fieldId]['buttonType'] = $formTag->nodeValue;
                            } else {
                                throw new \LogicException(sprintf('Invalid value "%s" for "%s" tag', $formTag->nodeValue, $formTag->nodeName));
                            }
                            break;
                    }
                }
            }
        }

        return $fieldArray;
    }

    /**
     * Convert dom field node to Magento array
     *
     * @param \DOMNode $source
     * @return array
     * @throws \LogicException
     */
    protected function _convertValidationRules($source)
    {
        $validationRulesArray = [];
        foreach ($source->childNodes as $validationRule) {
            if ($validationRule->nodeName === '#text' || $validationRule->nodeName === '#comment') {
                continue;
            }
            if (in_array($validationRule->nodeName, $this->validationAttr)) {
                switch ($validationRule->nodeName) {
                    case 'required':
                        $validationRulesArray['required'] = filter_var($validationRule->nodeValue, FILTER_VALIDATE_BOOLEAN);
                        break;
                    case 'min':
                        $validationRulesArray['min'] = (float)$validationRule->nodeValue;
                        break;
                    case 'max':
                        $validationRulesArray['max'] = (float)$validationRule->nodeValue;
                        break;
                    case 'alertMaxValue':
                        $validationRulesArray['alertMaxValue'] = (float)$validationRule->nodeValue;
                        break;
                    case 'alertMinValue':
                        $validationRulesArray['alertMinValue'] = (float)$validationRule->nodeValue;
                        break;
                    case 'lowerThan':
                        $validationRulesArray['lowerThan'] = (string)$validationRule->nodeValue;
                        break;
                    case 'greatherThan':
                        $validationRulesArray['greatherThan'] = (string)$validationRule->nodeValue;
                        break;
                    case 'allowedTypes':
                        $validationRulesArray['allowedTypes'] = explode(',', $validationRule->nodeValue);
                        break;
                    case 'maxSize':
                        $validationRulesArray['maxSize'] = (float)$validationRule->nodeValue;
                        break;
                    case 'email':
                        $validationRulesArray['email'] = filter_var($validationRule->nodeValue, FILTER_VALIDATE_BOOLEAN);
                        break;
                    case 'rut':
                        $validationRulesArray['rut'] = filter_var($validationRule->nodeValue, FILTER_VALIDATE_BOOLEAN);
                        break;
                    case 'regex':
                        $validationRulesArray['regex'] = $validationRule->nodeValue;
                        break;
                }
            } else {
                throw new \LogicException(sprintf('Invalid "%s" node for validationRule tag', $validationRule->nodeName));
            }
        }

        return $validationRulesArray;
    }

    protected function _convertNote($source)
    {
        $noteArray = [];
        foreach ($source->childNodes as $note) {
            if ($note->nodeName === '#text' || $note->nodeName === '#comment') {
                continue;
            }
            if (in_array($note->nodeName, $this->noteAttr)) {
                switch ($note->nodeName) {
                    case 'translate':
                        $noteArray['translate'] = filter_var($note->nodeValue, FILTER_VALIDATE_BOOLEAN);
                        break;
                    case 'noteText':
                        $noteArray['noteText'] = (string)$note->nodeValue;
                        break;
                }
            } else {
                throw new \LogicException(sprintf('Invalid "%s" node for note tag', $note->nodeName));
            }
        }
        return $noteArray;
    }

    protected function _convertInputMask($source)
    {
        $inputMaskArray = [];
        foreach ($source->childNodes as $maskRule) {
            if ($maskRule->nodeName === '#text' || $maskRule->nodeName === '#comment') {
                continue;
            }
            if (in_array($maskRule->nodeName, $this->inputMaskAttr)) {
                switch ($maskRule->nodeName) {
                    case 'enabled':
                        $inputMaskArray['enabled'] = filter_var($maskRule->nodeValue, FILTER_VALIDATE_BOOLEAN);
                        break;
                    case 'digits':
                        $inputMaskArray['digits'] = (int)$maskRule->nodeValue;
                        break;
                    case 'groupSeparator':
                        $inputMaskArray['groupSeparator'] = (string)$maskRule->nodeValue;
                        break;
                    case 'autoGroup':
                        $inputMaskArray['autoGroup'] = filter_var($maskRule->nodeValue, FILTER_VALIDATE_BOOLEAN);
                        break;
                    case 'allowMinus':
                        $inputMaskArray['allowMinus'] = filter_var($maskRule->nodeValue, FILTER_VALIDATE_BOOLEAN);
                        break;
                    case 'rightAlign':
                        $inputMaskArray['rightAlign'] = filter_var($maskRule->nodeValue, FILTER_VALIDATE_BOOLEAN);
                        break;
                    case 'removeMaskOnSubmit':
                        $inputMaskArray['removeMaskOnSubmit'] = filter_var($maskRule->nodeValue, FILTER_VALIDATE_BOOLEAN);
                        break;
                    case 'prefix':
                        $inputMaskArray['prefix'] = (string)$maskRule->nodeValue;
                        break;
                    case 'alias':
                        $inputMaskArray['alias'] = (string)$maskRule->nodeValue;
                        break;
                    case 'min':
                        $inputMaskArray['min'] = (int)$maskRule->nodeValue;
                        break;
                    case 'max':
                        $inputMaskArray['max'] = (int)$maskRule->nodeValue;
                        break;
                }
            } else {
                throw new \LogicException(sprintf('Invalid "%s" node for input mask tag', $maskRule->nodeName));
            }
        }
        return $inputMaskArray;
    }

    /**
     * Sort array values by sortOrder key
     *
     * @param array $a
     * @param array $b
     * @return array
     */
    protected function sortArrayByOrder($a, $b)
    {
        return $a['sortOrder'] - $b['sortOrder'];
    }

    /**
     * Validate only letters and underscore
     *
     * @param string $value
     * @param bool
     */
    protected function validateIdAttribute($value)
    {
        if (!empty($value)) {
            return preg_match("/^[a-z_]+$/", $value);
        }

        return false;
    }
}
