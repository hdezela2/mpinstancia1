<?php

namespace Formax\CreateFormAttributes\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Formax\CreateFormAttributes\Helper\Data;
use Formax\CreateFormAttributes\Model\CustomEntityVersionFactory;

class Recurring implements InstallSchemaInterface
{
    /**
     * @var Formax\CreateFormAttributes\Helper\Data
     */
    private $helper;

    /**
     * @var Formax\CreateFormAttributes\Model\CustomEntityVersionFactory
     */
    private $customEntityVersion;

    /**
     * @param Formax\CreateFormAttributes\Helper\Data $helper
     * @param Formax\CreateFormAttributes\Model\CustomEntityVersionFactory $customEntityVersion
     */
    public function __construct(
        Data $helper,
        CustomEntityVersionFactory $customEntityVersion
    ) {
        $this->helper = $helper;
        $this->customEntityVersion = $customEntityVersion;
    }

    /**
     * @param Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param Magento\Framework\Setup\ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->createAndUpdateSchema($setup);

        $setup->endSetup();
    }

    /**
     * Create and update schema from config custom_attributes.xml
     * 
     * @param Magento\Framework\Setup\SchemaSetupInterface $setup
     * @return void
     */
    private function createAndUpdateSchema(SchemaSetupInterface $setup)
    {
        $tables = $this->helper->getColumnsTable();
        $connection = $setup->getConnection();
        
        foreach ($tables as $keyTableName => $table) {
            $tableName = $setup->getTable($keyTableName);
            $entityId = $this->isModificable($tableName, $table['version']);

            if (isset($table['version']) && ($entityId === null || $entityId > 0)) {
                if ($connection->isTableExists($tableName)) {
                    foreach ($table['columns'] as $keyColumn => $column) {
                        if ($connection->tableColumnExists($tableName, $keyColumn)) {
                            if ($column['type'] != 'none') {
                                $connection->changeColumn(
                                    $tableName,
                                    $keyColumn,
                                    $keyColumn,
                                    [
                                        'type' => $column['type'],
                                        'length' => $column['length'],
                                        'nullable' => $column['nullable'],
                                        'default' => $column['default'],
                                        'comment' => 'auto generated ' . $keyColumn
                                    ]
                                );
                            }
                        } else {
                            if ($column['type'] != 'none') {
                                $connection->addColumn(
                                    $tableName,
                                    $keyColumn,
                                    [
                                        'type' => $column['type'],
                                        'length' => $column['length'],
                                        'nullable' => $column['nullable'],
                                        'default' => $column['default'],
                                        'comment' => 'auto generated ' . $keyColumn
                                    ]
                                );
                            }
                        }
                    }
                } else {
                    $newTable = $connection->newTable($tableName)
                        ->addColumn(
                            'entity_id',
                            Table::TYPE_INTEGER,
                            null,
                            ['identity' => true, 'nullable' => false, 'primary' => true]
                        );
                    
                    foreach ($table['columns'] as $keyColumn => $column) {
                        if ($column['type'] != 'none') {
                            $newTable->addColumn(
                                $keyColumn,
                                $column['type'],
                                $column['length'],
                                [
                                    'nullable' => $column['nullable'],
                                    'default' => $column['default'],
                                    'comment' => 'auto generated ' . $keyColumn
                                ]
                            );
                        }
                    }
                    $connection->createTable($newTable);
                }

                $model = $this->customEntityVersion->create();
                if ($entityId === null) {
                    $model->setEntityName($tableName);
                    $model->setVersion($table['version']);
                } else {
                    $model->setEntityId($entityId);
                    $model->setEntityName($tableName);
                    $model->setVersion($table['version']);
                }
                $model->save();
            }
        }
    }

    /**
     * Get if entity schema is modificable
     * 
     * @param string $entity
     * @param string $version
     * @return bool
     */
    public function isModificable($entity, $version)
    {
        $result = null;
        if ($entity && $version) {
            $model = $this->customEntityVersion->create();
            $customEntityVersion = $model->loadByEntity($entity);
            
            if (isset($customEntityVersion['entity_id']) && isset($customEntityVersion['version'])
                && (int)$customEntityVersion['entity_id'] > 0) {
                $result = (version_compare($customEntityVersion['version'], $version) < 0)
                    ? (int)$customEntityVersion['entity_id'] : false;
            }
        } else {
            $result = false;
        }
        
        return $result;
    }
}
