<?php

namespace Formax\RegionComuna\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Comuna extends AbstractSource
{
    /**
     * @var \Formax\RegionComuna\Model\ResourceModel\Comuna\CollectionFactory
     */
    public $collectionFactory;

    /**
     * @param \Formax\RegionComuna\Model\ResourceModel\Comuna\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Formax\RegionComuna\Model\ResourceModel\Comuna\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getAllOptions()
    {
        $collection = $this->collectionFactory->create()
            ->addFieldToFilter('status', 1)
            ->addOrder('name', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        return $collection->toOptionArray();
    }
}
