<?php

namespace Formax\RegionComuna\Model\ResourceModel\ComunaDensity;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Formax\RegionComuna\Model\ComunaDensity::class,
            \Formax\RegionComuna\Model\ResourceModel\ComunaDensity::class
        );

        $this->addOrder('density_id', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
    }

    /**
     * Initialize select object
     *
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        
        $this->addBindParam(':country', 'CL');
        $this->getSelect()->join(
            ['c' => $this->getTable('dccp_comuna')],
            'main_table.comuna_id = c.id',
            ['comuna_name' => 'name']
        )->join(
            ['r' => $this->getTable('directory_country_region')],
            'c.region_id = r.region_id AND r.country_id = :country',
            ['r.region_id', 'region_name' => 'default_name']
        );

        return $this;
    }

    /**
     * Filter by comuna_id
     *
     * @param string|array $comunaId
     * @return $this
     */
    public function addComunaFilter($comunaId)
    {
        if (!empty($comunaId)) {
            if (is_array($comunaId)) {
                $this->addFieldToFilter('main_table.comuna_id', ['in' => $comunaId]);
            } else {
                $this->addFieldToFilter('main_table.comuna_id', $comunaId);
            }
        }

        return $this;
    }

    /**
     * Filter by Density ID
     *
     * @param string|array $densityId
     * @return $this
     */
    public function addDensityFilter($densityId)
    {
        if (!empty($densityId)) {
            if (is_array($densityId)) {
                $this->addFieldToFilter('main_table.density_id', ['in' => $densityId]);
            } else {
                $this->addFieldToFilter('main_table.density_id', $densityId);
            }
        }

        return $this;
    }

    /**
     * Filter by Website ID
     *
     * @param string|array $websiteId
     * @return $this
     */
    public function addWebsiteFilter($websiteId)
    {
        if (!empty($websiteId)) {
            if (is_array($websiteId)) {
                $this->addFieldToFilter('main_table.website_id', ['in' => $websiteId]);
            } else {
                $this->addFieldToFilter('main_table.website_id', $websiteId);
            }
        }

        return $this;
    }
}
