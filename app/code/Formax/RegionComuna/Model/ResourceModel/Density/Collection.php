<?php

namespace Formax\RegionComuna\Model\ResourceModel\Density;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define main, region, locale province name tables
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Formax\RegionComuna\Model\Density::class,
            \Formax\RegionComuna\Model\ResourceModel\Density::class
        );
        
        $this->addOrder('sort', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
    }
}
