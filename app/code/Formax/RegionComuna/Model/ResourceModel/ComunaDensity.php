<?php

namespace Formax\RegionComuna\Model\ResourceModel;

class ComunaDensity extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main and comuna name tables
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('dccp_comuna_density', 'id');
    }
}