<?php

namespace Formax\RegionComuna\Model\ResourceModel\Comuna;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Region table name
     *
     * @var string
     */
    protected $_regionTable;

    /**
     * Define main, region, locale province name tables
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Formax\RegionComuna\Model\Comuna::class,
            \Formax\RegionComuna\Model\ResourceModel\Comuna::class
        );
        
        $this->_regionTable = $this->getTable('directory_country_region');

        $this->addOrder('default_name', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        $this->addOrder('name', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
    }

    /**
     * Initialize select object
     *
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();

        $this->addBindParam(':country', 'CL');
        $this->getSelect()->joinLeft(
            ['rname' => $this->_regionTable],
            'main_table.region_id = rname.region_id AND rname.country_id = :country',
            ['default_name']
        );

        return $this;
    }

    /**
     * Filter by region_id
     *
     * @param string|array $regionId
     * @return $this
     */
    public function addRegionFilter($regionId)
    {
        if (!empty($regionId)) {
            if (is_array($regionId)) {
                $this->addFieldToFilter('main_table.region_id', ['in' => $regionId]);
            } else {
                $this->addFieldToFilter('main_table.region_id', $regionId);
            }
        }

        return $this;
    }

    /**
     * Filter by Comuna ID
     *
     * @param string|array $comunaId
     * @return $this
     */
    public function addComunaIdFilter($comunaId)
    {
        if (!empty($comunaId)) {
            if (is_array($comunaId)) {
                $this->addFieldToFilter('main_table.id', ['in' => $comunaId]);
            } else {
                $this->addFieldToFilter('main_table.id', $comunaId);
            }
        }

        return $this;
    }

    /**
     * Convert collection items to select options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $propertyMap = [
            'value' => 'id',
            'title' => 'name',
            'region_id' => 'region_id',
        ];

        foreach ($this as $item) {
            $option = [];
            foreach ($propertyMap as $code => $field) {
                $option[$code] = $item->getData($field);
            }
            $option['label'] = $item->getName();
            $options[] = $option;
        }

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['title' => '', 'value' => '', 'label' => __('Please select a comuna')]
            );
        }
        
        return $options;
    }
}
