<?php

namespace Formax\RegionComuna\Model;

class Density extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\RegionComuna\Model\ResourceModel\Density::class);
    }
}
