var config = {
    "map": {
        "*": {
            "Magento_Checkout/template/shipping-address/address-renderer/default.html": 
                "Formax_RegionComuna/template/shipping-address/address-renderer/default.html",
            "Magento_Checkout/template/shipping-information/address-renderer/default.html": 
                "Formax_RegionComuna/template/shipping-information/address-renderer/default.html",
            "Magento_Checkout/template/billing-address/details.html": 
                "Formax_RegionComuna/template/billing-address/details.html",
            "Magento_NegotiableQuote/template/shipping-address/address-renderer/default.html":
                "Formax_RegionComuna/template/shipping-address/address-renderer/negotiable-quote/default.html"
        }
    }
};