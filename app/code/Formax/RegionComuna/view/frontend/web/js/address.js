require([
    "jquery", "domReady!"
], function($){
    "use strict";
    $(document).ready(function($, ko) {
        var baseUrl = document.location.origin;
        
        $(document).on("change", "select[name=region_id]", function(e) {
            var comuna = $("select[name='comuna']");
            var regionId = $(this).val();
            
            $.ajax({
                url: baseUrl + "/comunas/index/index/",
                data: {
                    region_id: regionId
                },
                showLoader: true,
                type: "POST",
                dataType: "json"
            }).done(function (data) {
                var options = "";
                if (data.length) {
                    $.each(data, function(index, item){
                        options = options + "<option value='" + item.value + "'>" + item.label + "</option>";
                    });
                }
                comuna.html(options);
            });
        });

        $("select[name='comuna']").change(function() {
            $("input[name='city']").val($(this).find("option:selected").text()).change();
            $("input[name='postcode']").val($(this).val()).change();
        });

        $('.account.customer-address-form .field.street').insertAfter('.account.customer-address-form .field-comuna');
        $('.form-address-edit .field.street').insertAfter('.form-address-edit .field.field-comuna');
    });
});
