define([
    'jquery',
    'uiComponent',
    'uiRegistry'
], function (
    $,
    Component,
    uiRegistry
) {
    $(document).ready(function () {
        var baseUrl = document.location.origin;
        
        $(document).on("change", "select[name=region_id]", function(e) {
            var comuna = $("select[name='custom_attributes[comuna]']");
            var regionId = $(this).val();
            
            $.ajax({
                url: baseUrl + "/comunas/index/index/",
                data: {
                    region_id: regionId
                },
                showLoader: true,
                type: "POST",
                dataType: "json"
            }).done(function (data) {
                var options = "";
                if (data.length) {
                    $.each(data, function(index, item){
                        options = options + "<option value='" + item.value + "'>" + item.label + "</option>";
                    });
                }
                comuna.html(options);
            });
        });
        
        $(document).on('change', "select[name='custom_attributes[comuna]']", function(e) {
            $("input[name='city']").val($(this).find("option:selected").text()).change();
            $("input[name='postcode']").val($(this).val()).change();
        });
    });
    
    return Component;
});
