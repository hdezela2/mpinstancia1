<?php

namespace Formax\RegionComuna\Setup\Patch\Data;

use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

class UpdateSortAttributes implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * UpdateCustomerAttributesMetadata constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $this->updateAttributes($customerSetup);

        return $this;
    }

    /**
     * @param CustomerSetup $customerSetup
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function updateAttributes($customerSetup)
    {
        $customerSetup->updateAttribute(
            'customer_address',
            'country_id',
            'sort_order',
            '70'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'country_id',
            'position',
            '70'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'region_id',
            'sort_order',
            '80'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'region_id',
            'position',
            '80'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'region',
            'sort_order',
            '80'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'region',
            'position',
            '80'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'city',
            'sort_order',
            '110'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'city',
            'position',
            '110'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'street',
            'sort_order',
            '120'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'street',
            'position',
            '120'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'postcode',
            'sort_order',
            '130'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'postcode',
            'position',
            '130'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'telephone',
            'sort_order',
            '140'
        );
        $customerSetup->updateAttribute(
            'customer_address',
            'telephone',
            'position',
            '140'
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.0.1';
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            UpdateComunaAttribute::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
