<?php

namespace Formax\RegionComuna\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

class InitializeDensityData implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * UpdateCustomerAttributesMetadata constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $densities = [
            [
                'id' => 1,
                'name' => 'Densidad baja',
                'sort' => 1
            ],
            [
                'id' => 2,
                'name' => 'Densidad media',
                'sort' => 2
            ],
            [
                'id' => 3,
                'name' => 'Densidad alta',
                'sort' => 3
            ],
            [
                'id' => 4,
                'name' => 'Periferia',
                'sort' => 4
            ]
        ];

        /**
         * Insert density
         */
        foreach ($densities as $density) {
            $connection->insertForce(
                $this->moduleDataSetup->getTable('dccp_density'),
                [
                    'id' => $density['id'],
                    'name' => $density['name']
                ]
            );
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.0.0';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
