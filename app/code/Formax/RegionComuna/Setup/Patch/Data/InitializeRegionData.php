<?php

namespace Formax\RegionComuna\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

class InitializeRegionData implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * UpdateCustomerAttributesMetadata constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();

        $regions = [
            [
                'region_id' => 15,
                'country_id' => 'CL',
                'code' => '001',
                'default_name' => 'Región de Arica y Parinacota'
            ],
            [
                'region_id' => 1,
                'country_id' => 'CL',
                'code' => '002',
                'default_name' => 'Región de Tarapacá'
            ],
            [
                'region_id' => 2,
                'country_id' => 'CL',
                'code' => '003',
                'default_name' => 'Región de Antofagasta'
            ],
            [
                'region_id' => 3,
                'country_id' => 'CL',
                'code' => '004',
                'default_name' => 'Región de Atacama'
            ],
            [
                'region_id' => 4,
                'country_id' => 'CL',
                'code' => '005',
                'default_name' => 'Región de Coquimbo'
            ],
            [
                'region_id' => 5,
                'country_id' => 'CL',
                'code' => '006',
                'default_name' => 'Región de Valparaíso'
            ],
            [
                'region_id' => 13,
                'country_id' => 'CL',
                'code' => '007',
                'default_name' => 'Región Metropolitana de Santiago'
            ],
            [
                'region_id' => 6,
                'country_id' => 'CL',
                'code' => '008',
                'default_name' => "Región del Libertador General Bernardo O'Higgins"
            ],
            [
                'region_id' => 7,
                'country_id' => 'CL',
                'code' => '009',
                'default_name' => 'Región del Maule'
            ],
            [
                'region_id' => 16,
                'country_id' => 'CL',
                'code' => '010',
                'default_name' => 'Región del Ñuble'
            ],
            [
                'region_id' => 8,
                'country_id' => 'CL',
                'code' => '011',
                'default_name' => 'Región del Biobío'
            ],
            [
                'region_id' => 9,
                'country_id' => 'CL',
                'code' => '012',
                'default_name' => 'Región de la Araucanía'
            ],
            [
                'region_id' => 14,
                'country_id' => 'CL',
                'code' => '013',
                'default_name' => 'Región de Los Ríos'
            ],
            [
                'region_id' => 10,
                'country_id' => 'CL',
                'code' => '014',
                'default_name' => 'Región de los Lagos'
            ],
            [
                'region_id' => 11,
                'country_id' => 'CL',
                'code' => '015',
                'default_name' => 'Región Aysén del General Carlos Ibáñez del Campo'
            ],
            [
                'region_id' => 12,
                'country_id' => 'CL',
                'code' => '016',
                'default_name' => 'Región de Magallanes y de la Antártica'
            ]
        ];

        /**
         * Insert regions
         */
        foreach ($regions as $region) {
            $connection->insertForce(
                $this->moduleDataSetup->getTable('directory_country_region'),
                [
                    'region_id' => $region['region_id'],
                    'country_id' => $region['country_id'],
                    'code' => $region['code'],
                    'default_name' => $region['default_name']
                ]
            );
        }

        $regions = [];
        $regions = [
            [
                'locale' => 'es_CL',
                'region_id' => 15,
                'name' => 'Región de Arica y Parinacota'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 1,
                'name' => 'Región de Tarapacá'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 2,
                'name' => 'Región de Antofagasta'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 3,
                'name' => 'Región de Atacama'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 4,
                'name' => 'Región de Coquimbo'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 5,
                'name' => 'Región de Valparaíso'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 13,
                'name' => 'Región Metropolitana de Santiago'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 6,
                'name' => "Región del Libertador General Bernardo O'Higgins"
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 7,
                'name' => 'Región del Maule'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 16,
                'name' => 'Región del Ñuble'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 8,
                'name' => 'Región del Biobío'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 9,
                'name' => 'Región de la Araucanía'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 14,
                'name' => 'Región de Los Ríos'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 10,
                'name' => 'Región de los Lagos'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 11,
                'name' => 'Región Aysén del General Carlos Ibáñez del Campo'
            ],
            [
                'locale' => 'es_CL',
                'region_id' => 12,
                'name' => 'Región de Magallanes y de la Antártica'
            ]
        ];

        /**
         * Insert regions name
         */
        foreach ($regions as $region) {
            $connection->insertForce(
                $this->moduleDataSetup->getTable('directory_country_region_name'),
                [
                    'locale' => $region['locale'],
                    'region_id' => $region['region_id'],
                    'name' => $region['name']
                ]
            );
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            TruncateRegionsTables::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.0.0';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
