<?php

namespace Formax\RegionComuna\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonResultFactory;

    /**
     * @var \Formax\RegionComuna\Model\ResourceModel\Comuna\CollectionFactory
     */
    protected $comunaCollection;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Formax\RegionComuna\Model\ResourceModel\Comuna\CollectionFactory $comunaCollection
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Formax\RegionComuna\Model\ResourceModel\Comuna\CollectionFactory $comunaCollection
    ) {
        $this->jsonResultFactory = $resultJsonFactory;
        $this->comunaCollection = $comunaCollection;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->jsonResultFactory->create();
        $regionId = $this->_request->getParam('region_id');
        $options = [];

        $comunas = $this->comunaCollection->create();

        if ($regionId) {
            $comunas->addRegionFilter($regionId);
        }

        $comunasResult = $comunas->toArray();
        
        if (isset($comunasResult['items'])) {
            foreach($comunasResult['items'] as $comuna) {
                $options[] = [
                    'value' => $comuna['id'],
                    'label' => $comuna['name']
                ];
            }
        }

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['value' => '', 'label' => __('Please select a comuna')]
            );
        }

        $result->setData($options);

        return $result;
    }
}
