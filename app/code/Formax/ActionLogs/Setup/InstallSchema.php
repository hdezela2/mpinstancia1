<?php
namespace Formax\ActionLogs\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->createLogsTable($setup);
        $setup->endSetup();
    }
    protected function createLogsTable(SchemaSetupInterface $setup){
        $tableName = 'xpec_actions_logs';
        $table = $setup->getConnection()
            ->newTable($setup->getTable($tableName))
            ->addColumn(
                'id_log',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true,'nullable' => false,'primary' => true],
                'Id Log'
            )
            ->addColumn(
                'id_user',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Id User'
            )
            ->addColumn(
                'name_user',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '255',
                ['nullable' => false, 'default' => ''],
                'Name User'
            )
            ->addColumn(
                'action_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '255',
                ['nullable' => false, 'default' => ''],
                'Action Name'
            )
            ->addColumn(
                'profile_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '255',
                ['nullable' => false, 'default' => ''],
                'Profile Name'
            )
            ->addColumn(
                'ip',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '50',
                ['nullable' => false, 'default' => ''],
                'Ip'
            )
            ->addColumn(
                'store_view',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '50',
                ['nullable' => false, 'default' => ''],
                'StoreVIew'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false, 
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ],
                'Fecha de Creación'
            )
            ->addColumn(
                'data_serialize',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '4M',
                ['nullable' => false, 'default' => ''],
                'Data Serialize'
            )
            ->addColumn(
                'after_data',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '4M',
                ['nullable' => false, 'default' => ''],
                'Data After'
            )
            ->addColumn(
                'before_data',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '4M',
                ['nullable' => false, 'default' => ''],
                'Data Before'
            )
            ->setComment("Xpec Index Orders");
        $setup->getConnection()->createTable($table);

        $column = 'profile_name';
        $setup->getConnection()->addIndex(
            $tableName,
            'indx_'.$column,
            [$column]
        );
        $column = 'name_user';
        $setup->getConnection()->addIndex(
            $tableName,
            'indx_'.$column,
            [$column]
        );
        $column = 'id_user';
        $setup->getConnection()->addIndex(
            $tableName,
            'indx_'.$column,
            [$column]
        );
        $column = 'action_name';
        $setup->getConnection()->addIndex(
            $tableName,
            'indx_'.$column,
            [$column]
        );
    }
    
}
