<?php
namespace Formax\ActionLogs\Controller\Adminhtml\ActionLogs;

class Detail extends \Magento\Backend\App\Action{

    /**
    * @var \Magento\Framework\View\Result\PageFactory
    */
    protected $resultPageFactory;
    private $_resultPage;

    /**
        * Constructor
        *
        * @param \Magento\Backend\App\Action\Context $context
        * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
        */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
        * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
        *
        * @return \Magento\Framework\View\Result\Page
        */
    public function execute(){
        $this->_setPageData();
        return  $resultPage = $this->getResultPage();
    }
    public function getResultPage(){
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->resultPageFactory->create();
        }
        return $this->_resultPage;
    }
    protected function _setPageData(){
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Formax_ActionLogs::formax_reportes_actiones');
        $resultPage->getConfig()->getTitle()->prepend((__('Detalle de Acciones')));

        //Add bread crumb
        $resultPage->addBreadcrumb(__('Formax'), __('Formax'));
        $resultPage->addBreadcrumb(__('Formax'), __('Detalle de Acciones'));

        return $this;
    }
}
  