<?php
namespace Formax\ActionLogs\Block\Adminhtml;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Serialize\SerializerInterface;


class Details extends \Magento\Backend\Block\Widget\Container{
    protected $_currentEevent = null;
    protected $_eventUser = null;
    protected $_coreRegistry = null;
    protected $_userFactory;
    private   $json;
    protected $_request;
    protected $_actionLogs;
    protected $_idlog;
    protected $_actionLog;
    private $_serializer;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\User\Model\UserFactory $userFactory,
        \Magento\Framework\App\Request\Http $request,
        \Formax\ActionLogs\Model\ActionLogsFactory $actionLogsFactory,
        SerializerInterface $serializer,
        array $data = [],
        Json $json = null
    ) {
        parent::__construct($context, $data);

        $this->_coreRegistry        = $registry;
        $this->_request             = $request;
        $this->_userFactory         = $userFactory;
        $this->_idlog               = $this->_request->getParam('id');
        $this->json                 = $json ?: ObjectManager::getInstance()->get(Json::class);
        $this->_actionLogs          = $actionLogsFactory->create();
        $this->_serializer          = $serializer;
        $this->_actionLogs->load($this->_idlog,'id_log');
    }
    protected function _construct(){
        parent::_construct();
        $this->buttonList->add(
            'back',
            [
                'label' => __('Back'),
                'onclick' => "setLocation('" . $this->_urlBuilder->getUrl('*/*') . "')",
                'class' => 'back'
            ]
        );
    }
    public function getLogId(){
        return $this->_idlog;
    }
    public function getActionLog(){
        return $this->_actionLogs;
    }
    public function getDataUnserialize(){
        return $this->_serializer->unserialize( $this->getActionLog()->getDataSerialize() );
    }
    public function serializeData($data){
        return $this->_serializer->serialize($data);
    }
    public function serializeDataHtml($data){
        return $this->getKeyValueDataTable($data);
    }
    private function getKeyValueDataTable($array){
        $html = '';
        if(isset($array) && is_array($array)){
            foreach($array as $key => $value){
                if(is_array($value)){
                    $tmpkay = (is_numeric($key))?'indice: '.$key:$key;
                    $html = $html.'
                        <dt class="parameter">'.$tmpkay.'</dt>
                        <dd class="value">
                            <dl class="list-parameters">'.$this->getKeyValueDataTable($value).'</dl>
                        </dd>';
                }else{

                    $subkey = (strlen($value)>100)?substr($value,0,100).'...':$value;
                    $html = $html.'
                    <dt class="parameter">'.$key.'</dt>
                    <dd class="value">'.$subkey.'</dd>';
                }
                
            }
        }
        return $html;
    }
}
