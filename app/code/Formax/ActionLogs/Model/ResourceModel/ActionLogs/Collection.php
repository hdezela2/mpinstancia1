<?php
namespace Formax\ActionLogs\Model\ResourceModel\ActionLogs;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	protected $_idFieldName = 'post_id';
	protected $_eventPrefix = 'formax_xpec_actions_logs_collection';
	protected $_eventObject = 'actionlogs_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Formax\ActionLogs\Model\ActionLogs', 'Formax\ActionLogs\Model\ResourceModel\ActionLogs');
	}

}