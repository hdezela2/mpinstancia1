<?php
namespace Formax\ActionLogs\Model\ResourceModel;
class ActionLogs extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	){
		parent::__construct($context);
	}
	
	protected function _construct(){
		$this->_init('xpec_actions_logs', 'id_log');
	}
	
}
