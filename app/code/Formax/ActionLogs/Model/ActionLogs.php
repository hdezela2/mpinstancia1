<?php
namespace Formax\ActionLogs\Model;
class ActionLogs extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface{
	const CACHE_TAG         = 'formax_xpec_actions_logs';
	protected $_cacheTag    = 'formax_xpec_actions_logs';
	protected $_eventPrefix = 'formax_xpec_actions_logs';

	protected function _construct(){
		$this->_init('Formax\ActionLogs\Model\ResourceModel\ActionLogs');
	}

	public function getIdentities(){
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues(){
		$values = [];
		return $values;
	}
}