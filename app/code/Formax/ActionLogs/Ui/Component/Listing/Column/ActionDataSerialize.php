<?php
namespace Formax\ActionLogs\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Price
 */
class ActionDataSerialize extends Column{
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceFormatter;
    protected $_serializer;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param PriceCurrencyInterface $priceFormatter
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        PriceCurrencyInterface $format,
        SerializerInterface $serializer,
        array $data = []
    ) {
        $this->priceFormatter   = $format;
        $this->_serializer      = $serializer;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource){
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $data = $this->_serializer->unserialize( $item[$this->getData('name')] );
                $item[$this->getData('name')] = (isset($data['action']))?$data['action']:'';
            }
        }
        return $dataSource;
    }
}
