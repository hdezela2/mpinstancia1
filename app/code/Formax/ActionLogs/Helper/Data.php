<?php
namespace Formax\ActionLogs\Helper;

use Formax\ActionLogs\Model\ActionLogsFactory;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Customer\Model\Session as customerSession;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\Context;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;

class Data extends \Magento\Framework\App\Helper\AbstractHelper{
    private $_actionPermitidosProvedores;
    private $_actionLogsFactory;
    private $_customerSession;
    private $_storeManager;
    private $_serializer;
    public $_remoteAddress;
    /**
     * @var null
     */
    private $_isStoreValid;

    public function __construct(
        Context $context,
        ActionLogsFactory $actionLogsFactory,
        SerializerInterface $serializer,
        RemoteAddress $remoteAddress,
        customerSession $customerSession,
        StoreManagerInterface $storeManager
    ){
        parent::__construct($context);
        $this->_actionLogsFactory   = $actionLogsFactory;
        $this->_customerSession     = $customerSession;
        $this->_storeManager        = $storeManager;
        $this->_serializer          = $serializer;
        $this->_isStoreValid        = null;
        $this->_remoteAddress       = $remoteAddress;
        $this->_actionPermitidosProvedores = array(
            'marketplace_account_dashboard',
            'marketplace_account_editprofile',
            'mpassignproduct_product_productlist',
            'mpassignproduct_product_productlist',
            'mpassignproduct_product_edit',
            'mpassignproduct_product_view',
            'penalty_index_index',
            'requestforquote_seller_lists',
            'requestforquote_seller_view',
            'requestforquote_seller_allquotedproduct'
        );
    }
    public function getActionPermitidosProvedores(){
        return $this->_actionPermitidosProvedores;
    }
    public function saveLoggin($actionName='',$profileName='',$extraData=array()){
        if($this->isStoreSaveLog()){
            try{
                $customer   = $this->_customerSession->getCustomer();
                $actionLogs = $this->_actionLogsFactory->create();
                $nameUser = 'N/A';
                if($customer->getId()!=0){
                    $nameUser = trim($customer->getFirstname().' '.$customer->getLastname());
                }
                $actionLogs->setIdUser($customer->getId());
                $actionLogs->setNameUser($nameUser);
                $actionLogs->setActionName($actionName);
                $actionLogs->setProfileName($profileName);
                $actionLogs->setIp($this->_remoteAddress->getRemoteAddress());
                $actionLogs->setStoreView($this->getStoreName());
                $additionalData = $this->_serializer->serialize($extraData);
                $actionLogs->setDataSerialize($additionalData);
                $actionLogs->save();
                return true;
            }catch(\Exception $err){
                throw new \Exception($err->getMessage());
            }
            return false;
        }
    }
    private function getStoreName(){
        return trim($this->_storeManager->getStore()->getName());
    }
    public function isStoreSaveLog(){
        if($this->_isStoreValid==null){
            if((trim($this->_storeManager->getStore()->getName())=='convenio_storeview_software') || trim($this->_storeManager->getStore()->getName())=='Software' || ((trim($this->_storeManager->getStore()->getName())== SoftwareRenewalConstants::WEBSITE_CODE) || trim($this->_storeManager->getStore()->getName())== SoftwareRenewalConstants::STORE_NAME)  ){
                $this->_isStoreValid = true;
            }else{
                $this->_isStoreValid = false;
            }
        }
        return $this->_isStoreValid;
    }


}
