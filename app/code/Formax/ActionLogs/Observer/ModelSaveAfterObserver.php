<?php
namespace Formax\ActionLogs\Observer;

use Magento\Framework\Event\ObserverInterface;

class ModelSaveAfterObserver implements ObserverInterface{
    private $_logger;
    private $_actionLogsFactory;

    public function __construct(
        \Formax\ActionLogs\Logger\Logger $logger,
        \Formax\ActionLogs\Helper\Data $helperData,
        \Formax\ActionLogs\Model\ActionLogsFactory $actionLogsFactory
    ) {
        $this->_logger              = $logger;
        $this->_actionLogsFactory   = $actionLogsFactory;
        $actionLogs                 = $this->_actionLogsFactory->create();
    }

    public function execute(\Magento\Framework\Event\Observer $observer){
        $obj = $observer->getEvent()->getObject();
        //$this->_logger->info("save:  ".json_encode( $obj->getData() ));
        // if(!is_null($obj->getOrigData())){
        //     $this->_logger->info("save:  ".json_encode( $obj->getOrigData() )." -- model: ". $observer->getEvent()->getName().'  -- class: '.get_class($obj) );
        // }
        
        //$this->_processor->modelActionAfter($observer->getEvent()->getObject(), 'save');
    }
}
