<?php
namespace Formax\ActionLogs\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Store\Api\Data\GroupInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Api\Data\WebsiteInterface;
use Magento\Framework\Serialize\SerializerInterface;


class ActionLog implements ObserverInterface{
    protected $_logger;
    protected $_actionPermitidosProvedores;
    protected $_actionLogsFactory;
    protected $_customerSession;
    private $_remoteAddress;
    private $_helperDataWebkul;
    private $_blockWebkul;
    private $_serializer;

    public function __construct(
        \Formax\ActionLogs\Logger\Logger $logger,
        \Formax\ActionLogs\Helper\Data $helperData,
        \Formax\ActionLogs\Model\ActionLogsFactory $actionLogsFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \Webkul\Marketplace\Helper\Data $helperDataWebkul,
        \Webkul\Marketplace\Block\Page\Header $blockWebkul,
        SerializerInterface $serializer
    ) {
        $this->_logger                      = $logger;
        $this->_actionPermitidosProvedores  = $helperData;
        $this->_actionLogsFactory           = $actionLogsFactory;
        $this->_customerSession             = $customerSession;
        $this->_remoteAddress               = $remoteAddress;
        $this->_helperDataWebkul            = $helperDataWebkul;
        $this->_blockWebkul                 = $blockWebkul;
        $this->_serializer                  = $serializer;
    }


    public function execute(\Magento\Framework\Event\Observer $observer){
        if ($observer->getEvent()->getControllerAction()->getRequest()->isDispatched()) {
            $request        = $observer->getEvent()->getRequest();
            $actionFullName = strtolower( $request->getFullActionName() );
            if(in_array($actionFullName,$this->_actionPermitidosProvedores->getActionPermitidosProvedores()) && $this->_customerSession->isLoggedIn()){
                $sellerShopName = $this->_blockWebkul->getSellerShopName();
                $sellerId       = $this->_helperDataWebkul->getCustomerId();
                $actionLogs     = $this->_actionLogsFactory->create();
                $customerId     = $this->_customerSession->getCustomerId();
                $data           = array(
                                    'sellerId'      => $sellerId,
                                    'sellerName'    => $sellerShopName,
                                    'customerId'    => $customerId,
                                    'action'        => 'Ver'
                                );
                $additionalData = $this->_serializer->serialize($data);
                $actionLogs->setIdUser($customerId);
                $actionLogs->setNameUser($sellerShopName);
                $actionLogs->setActionName($actionFullName);
                $actionLogs->setProfileName('Proveedor');
                $actionLogs->setIp($this->_remoteAddress->getRemoteAddress());
                $actionLogs->setDataSerialize($additionalData);
                $actionLogs->save();
                
                $this->_logger->info("logeo: ".$actionFullName." -- sellerId: ".$sellerId."   --- nameSeller: ".$sellerShopName );
            }
            

        }   
    }
}
