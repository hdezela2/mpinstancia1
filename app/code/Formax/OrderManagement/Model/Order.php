<?php
namespace Formax\OrderManagement\Model;

use Magento\Framework\Webapi\Rest\Request as RestRequest;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;

class Order implements \Formax\OrderManagement\Api\OrderInterface  {

    protected $_request;
    protected $_quoteCollectionFactory;
    protected $_orderCollectionFactory;
    protected $_actionLogsFactory;
    private $_serializer;
    private $_remoteAddress;
    private $_storeManager;
    private $_isStoreValid;

    public function __construct(
        \Magento\Quote\Model\ResourceModel\Quote\CollectionFactory $quoteCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        RestRequest $request,
        \Formax\ActionLogs\Model\ActionLogsFactory $actionLogsFactory,
        SerializerInterface $serializer,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        StoreManagerInterface $storeManager
	) {

        $this->_quoteCollectionFactory  = $quoteCollectionFactory;
        $this->_orderCollectionFactory  = $orderCollectionFactory;
        $this->_request                 = $request;
        $this->_actionLogsFactory       = $actionLogsFactory;
        $this->_serializer              = $serializer;
        $this->_remoteAddress           = $remoteAddress;
        $this->_storeManager            = $storeManager;
        $this->_isStoreValid            = null;
    }

     /**
     * Returns OK or Error
     *
     * @api
     * @return \Formax\OrderManagement\Api\Data\ResponseInterface
     */
    public function UpdateStatus($odc) {
        $actionLogs     = $this->_actionLogsFactory->create();
        $actionLogs->setIdUser(0);
        $actionLogs->setNameUser('Odc: '.$odc);
        $actionLogs->setActionName('update_status_order');
        $actionLogs->setProfileName('Web Service');
        $actionLogs->setStoreView($this->getStoreName());
        $actionLogs->setIp($this->_remoteAddress->getRemoteAddress());
        $requestLog = '';
        $responseLog = '';

        $request = json_decode($this->_request->getContent());

        if (!isset($request->status) || !isset($request->comments)) {
            $data           = array(
                'odc'      => $odc,
                'action'   => 'Actualizar Status Orden',
                'status'   => 'Bad request.'
            );
            $additionalData = $this->_serializer->serialize($data);
            $actionLogs->setDataSerialize($additionalData);
            if($this->isStoreSaveLog()){
                $actionLogs->save();
            }
			return $this->toResponse(400, "Bad request.");
        }

        /*$quote = null;
        $collection = $this->_quoteCollectionFactory->create()->addFieldToFilter('orden_de_compra', $odc);
        foreach($collection as $item) {
            $quote = $item;
        }
        if (!$quote)
            return $this->toResponse(404, "Orders not found");
        */

        $orders = $this->_orderCollectionFactory->create()->addFieldToFilter('orden_de_compra', $odc);
        if ($orders->getSize() == 0) {
            $data           = array(
                'odc'      => $odc,
                'action'   => 'Actualizar Status Orden',
                'status'   => 'Orders not found'
            );
            $additionalData = $this->_serializer->serialize($data);
            $actionLogs->setDataSerialize($additionalData);
            if($this->isStoreSaveLog()){
                $actionLogs->save();
            }
            return $this->toResponse(404, "Orders not found");
        }
        foreach($orders as $order) {
            $requestLog = $order->getData();
            $order->setState($request->status);
            $order->addStatusToHistory($request->status, $request->comments);
            $order->save();
            $responseLog = $order->getData();
        }

        $data           = array(
            'odc'      => $odc,
            'action'   => 'Actualizar Status Orden',
            'status'   => 'Orden actualizada con éxito',
            'request'       => $requestLog,
            'response'      => $responseLog
        );
        $additionalData = $this->_serializer->serialize($data);
        $actionLogs->setDataSerialize($additionalData);
        if($this->isStoreSaveLog()){
            $actionLogs->save();
        }
        return $this->toResponse(200, "OK");
    }

    public function toResponse($code, $message) {
		$response = new \Formax\OrderManagement\Api\Data\Impl\Response();
		$response->setCode($code);
		$response->setMessage($message);
        return $response;
    }
    private function getStoreName(){
        return trim($this->_storeManager->getStore()->getName());
    }
    private function isStoreSaveLog(){
        if($this->_isStoreValid==null){
            $storeName = trim($this->_storeManager->getStore()->getName());
            if($storeName=='convenio_storeview_software' || $storeName=='Software' || $storeName == SoftwareRenewalConstants::STORE_NAME || $storeName == SoftwareRenewalConstants::STORE_CODE){
                $this->_isStoreValid = true;
            }else{
                $this->_isStoreValid = false;
            }
        }
        return $this->_isStoreValid;
    }

}
