<?php
namespace Formax\OrderManagement\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;

class InstallData implements InstallDataInterface
{
    /**
     * @var Magento\Sales\Setup\SalesSetupFactory
     */
    protected $_salesSetupFactory;

    /**
     * @var Magento\Quote\Setup\QuoteSetupFactory
     */
    protected $_quoteSetupFactory;

    /**
     * @param SalesSetupFactory $salesSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     */
    public function __construct(
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory
    ) {
        $this->_salesSetupFactory = $salesSetupFactory;
        $this->_quoteSetupFactory = $quoteSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {

        /** @var \Magento\Quote\Setup\QuoteSetup $quoteInstaller */
        $quoteInstaller = $this->_quoteSetupFactory
                        ->create(
                            [
                                'resourceName' => 'quote_setup',
                                'setup' => $setup
                            ]
                        );
        $salesInstaller = $this->_salesSetupFactory
                        ->create(
                            [
                                'resourceName' => 'sales_setup',
                                'setup' => $setup
                            ]
                        );
        $type = \Magento\Framework\DB\Ddl\Table::TYPE_TEXT;
        $this->addAttributes("quote", "orden_de_compra", $type, $quoteInstaller);
        $this->addAttributes("quote", "url_orden_de_compra", $type, $quoteInstaller);
        $this->addAttributes("order", "orden_de_compra", $type, $salesInstaller);
        $this->addAttributes("order", "url_orden_de_compra", $type, $salesInstaller);
    }

    public function addAttributes($entity, $code, $type, $installer) {
        $installer->addAttribute($entity, $code, ['type' => $type, 'length'=> 255, 'visible' => false, 'nullable' => true]);
    }
}