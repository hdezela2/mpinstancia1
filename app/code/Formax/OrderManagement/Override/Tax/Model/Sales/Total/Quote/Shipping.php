<?php

namespace Formax\OrderManagement\Override\Tax\Model\Sales\Total\Quote;

class Shipping extends \Magento\Tax\Model\Sales\Total\Quote\Shipping
{
    /**
     * Collect tax totals for shipping. The result can be used to calculate discount on shipping
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Address\Total $total
     * @return $this
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        $storeId = $quote->getStoreId();
        $items = $shippingAssignment->getItems();
        if (!$items) {
            return $this;
        }

        //Add shipping
        $shippingDataObject = $this->getShippingDataObject($shippingAssignment, $total, false);
        $baseShippingDataObject = $this->getShippingDataObject($shippingAssignment, $total, true);
        if ($shippingDataObject == null || $baseShippingDataObject == null) {
            return $this;
        }

        $quoteDetails = $this->prepareQuoteDetails($shippingAssignment, [$shippingDataObject]);
        $taxDetails = $this->taxCalculationService
            ->calculateTax($quoteDetails, $storeId);
        $taxDetailsItems = $taxDetails->getItems()[self::ITEM_CODE_SHIPPING];

        $baseQuoteDetails = $this->prepareQuoteDetails($shippingAssignment, [$baseShippingDataObject]);
        $baseTaxDetails = $this->taxCalculationService
            ->calculateTax($baseQuoteDetails, $storeId);
        $baseTaxDetailsItems = $baseTaxDetails->getItems()[self::ITEM_CODE_SHIPPING];

        if ($quote->getIsMultiShipping()) {
            $address = $shippingAssignment->getShipping()->getAddress();
            $address->setShippingAmount($taxDetailsItems->getRowTotal());
            $address->setBaseShippingAmount($taxDetailsItems->getRowTotal());
        } else {
            $quote->getShippingAddress()
                ->setShippingAmount($taxDetailsItems->getRowTotal());
            $quote->getShippingAddress()
                ->setBaseShippingAmount($baseTaxDetailsItems->getRowTotal());
        }

        $this->processShippingTaxInfo(
            $shippingAssignment,
            $total,
            $taxDetailsItems,
            $baseTaxDetailsItems
        );

        return $this;
    }
}
