<?php
namespace Formax\OrderManagement\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Controller\ResultFactory;

class MultishippingCheckoutOnepageControllerSuccessObserver implements ObserverInterface
{
    /**
     * @var \Formax\OrderManagement\Helper\Data
     */
    protected $_helper;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollectionFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    protected $_responseFactory;

    /**
    * @param \Formax\OrderManagement\Helper\Data $helper
    * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    * @param \Magento\Framework\UrlInterface $url
    * @param \Magento\Framework\App\ResponseFactory $responseFactory
    */
    public function __construct(
        \Formax\OrderManagement\Helper\Data $helper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\ResponseFactory $responseFactory
    ) {
        $this->_helper = $helper;
        $this->_scopeConfig = $scopeConfig;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_responseFactory = $responseFactory;
        $this->_url = $url;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $enabled = $this->_scopeConfig->getValue(
            'sales/erp_integration_section/erp_integration_enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($enabled) {
            $orderIds = $observer->getEvent()->getOrderIds();
            $collection = $this->_orderCollectionFactory->create()
                            ->addFieldToFilter('entity_id', ['in' => $orderIds]);

            foreach ($collection as $order) {
                $result = $this->_helper->execute($order);
                if (!$result) {
                    $CustomRedirectionUrl = $this->_url->getUrl('checkout/onepage/failure');
                    $redirect = $this->_responseFactory->create()->setRedirect($CustomRedirectionUrl)->sendResponse();
                    exit();
                }
            }
        }
    }
}
