<?php
namespace Formax\OrderManagement\Observer;

use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;

class CheckoutSubmitBeforeObserver implements ObserverInterface
{

    protected $_mpModel;
    protected $_mpProduct;
    protected $_customerRepository;
    protected $_httpClientFactory;
    protected $_logger;
    protected $_scopeConfig;
    protected $_messageManager;
    protected $_storeManager;
    const WEBKUL_QUOTE_ID = \Webkul\Requestforquote\Model\Product\Type\Quote::TYPE_ID;

    /**
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Formax\OrderManagement\Logger\Logger $logger
     */
    public function __construct(
        \Webkul\MpAssignProduct\Model\Items $mpModel,
        \Webkul\Marketplace\Model\Product $mpProduct,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Customer $customerRepository,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Formax\OrderManagement\Logger\Logger $logger,
        StoreManagerInterface $storeManager
    ) {
        $this->_mpModel = $mpModel;
        $this->_mpProduct = $mpProduct;
        $this->_scopeConfig = $scopeConfig;
        $this->_customerRepository = $customerRepository;
        $this->_httpClientFactory = $httpClientFactory;
        $this->_messageManager = $messageManager;
        $this->_logger = $logger;
        $this->_storeManager = $storeManager;
    }

    public function getProducts($quote)
    {
        $shippingAddress = $quote->getShippingAddress();
        $items = $quote->getItems();
        $products = [];
        $isStorePickup = $this->isStorePickup($quote);
        foreach($items as $item)
        {
            $sku = $item->getSku();
            if($item->getProductType() == self::WEBKUL_QUOTE_ID)
            {
                $sku = explode('-', $sku);
                unset($sku[0]);
                $sku = implode('-', $sku);
            }
            $product = [
                "idpedidomagento" => intval($quote->getId()),
                "idproducto" => intval($sku),
                "cantidad" => $item->getQty(),
                "retiro_tienda" => $isStorePickup,
                "precio_neto" => $this->getItemPrice($item),
                "total_cargos" => $this->getShippingPrice($item),
                "region" => intval($shippingAddress->getRegionCode()),
                "comuna" => intval($shippingAddress->getComuna()),
                "direccion" => $shippingAddress->getStreet()[0]
            ];
            array_push($products, $product);
        }
        return $products;
    }

    protected function getItemPrice($item)
    {
        if(!$this->isEmergenciasWebsite())
            return $item->getPrice();

        $model = $this->_mpModel->load($item->getBuyRequest()->getMpassignproductId());
        return $model->getBasePrice();
    }

    private function getShippingPrice($item)
    {
        if(!$this->isEmergenciasWebsite())
            return 0;

        $model = $this->_mpModel->load($item->getBuyRequest()->getMpassignproductId());
        return $shippingPrice = ($model->getShippingPrice() == null) ? 0 : $model->getShippingPrice() * $item->getQty();
    }

    public function getCustomer($quote)
    {
        $customer = $quote->getCustomer();
        return $this->_customerRepository->load($customer->getId());
    }

    public function getSellerId($quote)
    {
        foreach ($quote->getAllVisibleItems() as $item)
        {
            if (!$item->hasParentItemId())
            {
                $options = $item->getBuyRequest()->getData();
                $sellerId = null;
                $collectionProduct = null;

                if(isset($options['mpassignproduct_id'])):
                    $model = $this->_mpModel->load($options['mpassignproduct_id']);
                    if ($model->getSellerId()):
                        $sellerId = $model->getSellerId();
                    endif;
                elseif(array_key_exists('seller_id', $options)):
                    $sellerId = $options['seller_id'];
                else:
                    $collectionProduct = $this->_mpProduct
                        ->getCollection()
                        ->addFieldToFilter(
                            'mageproduct_id',
                            $item->getProductId()
                        );
                    foreach ($collectionProduct as $value):
                        if($value->getSellerId()):
                            $sellerId = $value->getSellerId();
                        endif;
                    endforeach;
                endif;

                $this->_logger->info('[OK]CheckoutSubmitBeforeObserver::getSellerId ' . $sellerId);
                return $sellerId;
            }
        }
        $this->_logger->info('[NOK]CheckoutSubmitBeforeObserver::getSellerId');
        return null;
    }

    public function isStorePickup($quote)
    {
        $shippingMethod = $quote->getShippingAddress()->getShippingMethod();
        return $shippingMethod == 'dccp_storepickup';
    }

    public function getProviderId($quote)
    {
        $sellerId = $this->getSellerId($quote);
        if (!$sellerId)
        {
            throw new \Exception(__("Error al obtener el proveedor de su pedido."));
        }
        $seller = $this->_customerRepository->load($sellerId);
        return $seller->getUserRestIdOrganization();
    }

    public function getTaxAmount($quote)
    {
        return $quote->getShippingAddress()->getTaxAmount();
    }

    public function getShippingAmount($quote)
    {
        if($this->isEmergenciasWebsite())
            return 0;

        return $quote->getShippingAddress()->getShippingAmount();
    }

    protected function isEmergenciasWebsite()
    {
        return in_array($this->_storeManager->getStore()->getCode(), [\Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE, ConstantsEmergency202109::WEBSITE_CODE]);
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function isCombustiblesWebsite(): bool
    {
        return in_array($this->_storeManager->getStore()->getCode(),
            [
                \Summa\CombustiblesSetUp\Helper\Data::WEBSITE_CODE,
                \Chilecompra\CombustiblesRenewal\Model\Constants::WEBSITE_CODE
            ]);
    }

    public function createErrorMessage($errors)
    {
        $message = "";
        foreach($errors as $error)
        {
            $message .= "Error: ".$error->codigo.". ".$error->descripcion."<br>";
        }
        return $message;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent();
        $this->_logger->info(json_encode($event->getQuote(), true));
        $quote = $event->getQuote();

        if ($this->isCombustiblesWebsite()){
            //hotFix Combustibles
            $quote->setWkTaxRule('SINVALOR');
        }
        $idimpuesto = 1;
        if ($quote->getWkTaxRule() == 'EXENTO' || $quote->getWkTaxRule() == 'SINVALOR'){
            $idimpuesto = 40;
        };

        $this->_logger->info("DATA: ".json_encode($quote->getData()));
        $quoteData = $quote->getData();

        $enabled = $this->_scopeConfig->getValue('sales/erp_integration_section/erp_integration_enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($enabled) {
            $customer = $this->getCustomer($quote);
            //$token = $customer->getUserRestId();
            $token = $customer->getUserRestAtk();
            $this->_logger->info("["."] Token: ".$token);
            $request = [
                "idproveedor" => $this->getProviderId($quote),
                "idorganizacion_compradora" => $quote->getPurchaseUnit(),
                "moneda" => $quoteData['quote_currency_code'],
                "instruccion_envio" => "",
                "total_descuentos" => $quoteData['custom_discount'],
                "total_cargos" => $this->getShippingAmount($quote),
                "total_impuesto" => $this->getTaxAmount($quote),
                "idimpuesto" => $idimpuesto,
                "comentario_descuento" => "",
                "productos" => $this->getProducts($quote)
            ];
            $this->_logger->info("["."] Service Request: ".json_encode($request));
            $response = $this->sendRequest($request, $token);
            $this->_logger->info("["."] Service Response: ".$response);
            $response = json_decode($response);
            if (isset($response->success) && $response->success == 'OK') {
                $odc = $response->payload->codigoOrdenDeCompra;
                $this->_logger->info("["."] ODC: ".$odc);
                $quote->setOrdenDeCompra($odc);
                $quote->setUrlOrdenDeCompra($response->payload->urlSimplificacionOC);
                $quote->save();
            } else {
                //BAD
                $excDet = "";
                if (isset($response->errores) && is_array($response->errores)) {
                    $excDet = $this->createErrorMessage($response->errores);
                }
                throw new \Exception(__("No se ha podido procesar su pedido, intente de nuevo más tarde. ".$excDet));
            }
        }
    }

    public function sendRequest($request, $token)
    {
        try{
            $endpoint = $this->_scopeConfig->getValue('sales/erp_integration_section/generate_purchase_order_endpoint', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $client = $this->_httpClientFactory->create();
            $client->setUri($endpoint);
            $client->setHeaders([
                'Authorization' => "Bearer ".$token,
                'Content-Type' => 'application/json'
            ]);
            $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
            $client->setRawData(json_encode($request));
            return $client->request(\Zend_Http_Client::POST)->getBody();
        }catch(\Exception $ex){
            return null;
        }
    }

}
