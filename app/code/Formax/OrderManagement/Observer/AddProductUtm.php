<?php
namespace Formax\OrderManagement\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class AddProductUtm implements ObserverInterface
{


	private $customerSession;
	public $customerRepository;
    private $_checkoutSession;
    private $_logger;

	public function __construct(
        \Magento\Customer\Model\Session $customerSession,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Checkout\Model\Session $_checkoutSession,
        \Formax\OrderManagement\Logger\Logger $logger
	){
        $this->_logger              = $logger;
		$this->customerSession      = $customerSession;
		$this->customerRepository   = $customerRepository;
		$this->_checkoutSession     = $_checkoutSession;
	}

	public function execute(\Magento\Framework\Event\Observer $observer) {
        $this->_logger->info("init observer");
		// $item = $observer->getEvent()->getData('quote_item');         
		// $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
		// $customerId = $this->customerSession->getCustomer()->getId();
        // $customer = $this->customerRepository->getById($customerId);
        
        // $item->setCustomPrice($price);
        // $item->setOriginalCustomPrice($price);
        // $item->getProduct()->setIsSuperMode(true);

        //$this->_checkoutSession->getQuote()->collectTotals()->save();
		
    }


}
