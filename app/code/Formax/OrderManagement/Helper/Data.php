<?php

namespace Formax\OrderManagement\Helper;

use Intellicore\ComputadoresRenewal2\Constants as ComputadoresRenewal2Constants;
use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Chilecompra\CombustiblesRenewal\Model\Constants;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;
use Linets\GasSetup\Constants as GasConstants;
use Linets\InsumosSetup\Model\InsumosConstants;
use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Linets\VoucherSetup\Model\Constants as LinetsVoucherConstants;
use Linets\VoucherSetup\Model\Constants as Voucher2021Constants;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;
use Summa\CombustiblesSetUp\Helper\Data as CombustiblesHelper;
use Webkul\Requestforquote\Model\Product\Type\Quote as WebkulQuote;

class Data extends AbstractHelper
{
    /**
     * @var int
     */
    const WEBKUL_QUOTE_ID = WebkulQuote::TYPE_ID;

    const TYPE_CONFIGURABLE = "configurable";

    /**
     * @var \Webkul\MpAssignProduct\Model\Items
     */
    protected $_mpModel;

    /**
     * @var \Webkul\Marketplace\Model\Product
     */
    protected $_mpProduct;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $_customer;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_httpClientFactory;

    /**
     * @var \Formax\OrderManagement\Logger\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var VoucherHelper
     */
    protected $_voucherHelper;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $_assignProductHelper;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $_quoteFactory;

    /**
    * @var \Magento\Framework\Registry
    */
    protected $_registry;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param \Formax\ActionLogs\Model\ActionLogsFactory
     */
    protected $_actionLogsFactory;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    protected $_remoteAddress;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $_resource;

    /**
     * @var HelperSoftware
     */
    protected $helperSoftware;

    /**
     * @var Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     * @var OrderItemRepositoryInterface
     */
    protected $orderItemRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var string
     */
    const CONVENIO_VOUCHER = 'voucher';
    private SerializerInterface $_serializer;

    /**
     * @var \Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface
     */
    private $dispatchPriceManagement;

    /**
     * @param Context $context,
     * @param \Webkul\MpAssignProduct\Model\Items $mpModel
     * @param \Webkul\Marketplace\Model\Product $mpProduct
     * @param \Magento\Customer\Model\Customer $customerRepository
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Formax\OrderManagement\Logger\Logger $logger
     * @param \Webkul\MpAssignProduct\Helper\Data $assignProductHelper
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Framework\Registry $registry
     * @param VoucherHelper $voucherHelper
     * @param StoreManagerInterface $storeManager
     * @param \Formax\ActionLogs\Model\ActionLogsFactory $actionLogsFactory
     * @param \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param HelperSoftware $helperSoftware
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        Context $context,
        \Webkul\MpAssignProduct\Model\Items $mpModel,
        \Webkul\Marketplace\Model\Product $mpProduct,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Formax\OrderManagement\Logger\Logger $logger,
        \Webkul\MpAssignProduct\Helper\Data $assignProductHelper,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\Registry $registry,
        VoucherHelper $voucherHelper,
        StoreManagerInterface $storeManager,
        \Formax\ActionLogs\Model\ActionLogsFactory $actionLogsFactory,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        SerializerInterface $serializer,
        \Magento\Framework\App\ResourceConnection $resource,
        DateTime $dateTime,
        HelperSoftware $helperSoftware,
        OrderItemRepositoryInterface $orderItemRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        DispatchPriceManagementInterface $dispatchPriceManagement
    ) {
        $this->_assignProductHelper = $assignProductHelper;
        $this->_voucherHelper = $voucherHelper;
        $this->_mpModel = $mpModel;
        $this->_mpProduct = $mpProduct;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_customer = $customer;
        $this->_httpClientFactory = $httpClientFactory;
        $this->_messageManager = $messageManager;
        $this->_logger = $logger;
        $this->_quoteFactory = $quoteFactory;
        $this->_actionLogsFactory = $actionLogsFactory;
        $this->_registry = $registry;
        $this->_storeManager = $storeManager;
        $this->_remoteAddress = $remoteAddress;
        $this->_serializer = $serializer;
        $this->_resource = $resource;
        $this->dateTime = $dateTime;
        $this->helperSoftware = $helperSoftware;
        $this->orderItemRepository = $orderItemRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->dispatchPriceManagement = $dispatchPriceManagement;

        parent::__construct($context);
    }


    /**
     * Get products ordered
     *
     * @param \Magento\Sales\Model\Order $order
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProducts($order)
    {
        $shippingAddress = $order->getShippingAddress();
        $items = $order->getItems();
        $products = [];
        $isStorePickup = $this->isStorePickup($order);

        foreach ($items as $item) {
            if ($item->getProductType() == Type::TYPE_SIMPLE || $item->getProductType() == Type::TYPE_VIRTUAL || $item->getProductType() == 'quote') {
                $parentItem = $item->getParentItem();
                $parentItem = !$parentItem || $parentItem === null ? $item : $parentItem;
                $productObj = $parentItem->getProduct();
                $sku = $productObj->getSku();
                if (!$item->getParentItemId() &&
                    ((int)$this->getItemPrice($item) == 0) &&
                    in_array($this->_voucherHelper->getCurrentStoreCode(),
                        [
                            GasConstants::GAS_WEBSITE_CODE,
                            ConstantsEmergency202109::WEBSITE_CODE,
                            InsumosConstants::WEBSITE_CODE
                        ]
                    )
                ) {
                    $parentItem = $this->loadOrderParentItem($item);
                }

                if ($parentItem->getProductType() == self::TYPE_CONFIGURABLE){
                    $sku = $item->getProduct()->getSku();
                }
                if ($item->getProductType() == self::WEBKUL_QUOTE_ID ||
                    (($this->getStoreCode() == HelperSoftware::SOFTWARE_STOREVIEW_CODE || $this->getStoreCode() == SoftwareRenewalConstants::STORE_CODE) && $item->getProductType() == Type::TYPE_VIRTUAL)) {
                    $sku = explode('-', $sku);
                    unset($sku[0]);
                    $sku = implode('-', $sku);
                    $this->logData("sku: ".$sku);
                }
                $itemPrice = '';
                if ($this->isMobiliarioWebsite()){
                    if ($parentItem->getProductType() == 'quote'){
                        $cargos = $this->_assignProductHelper->getAssemblyPriceByRequestedSku($parentItem->getSku(), $this->getSellerId($order)) * $parentItem->getQtyOrdered();
                    }else{
                        $cargos = $this->getAssemblyPrice($parentItem);
                    }
                    $itemPrice = $this->getItemPrice($parentItem) - ($cargos / $parentItem->getQtyOrdered());
                } elseif ($this->isInsumosWebsite()) {
                    $includedShippingAmount = $this->getInsumosShippingAmountPerProduct($item, $this->getSellerId($order));
                    $cargos = $includedShippingAmount * $item->getQtyOrdered();
                    $itemPrice = $this->getItemPrice($parentItem) - $includedShippingAmount;
                }else{
                    $cargos = $this->getShippingPrice($parentItem);
                }

                $product = [
                    'idpedidomagento' => (int)$order->getId(),
                    'idproducto' => $sku,
                    'cantidad' => $parentItem->getQtyOrdered(),
                    'retiro_tienda' => $isStorePickup,
                    'precio_neto' => !empty($itemPrice)  ? $itemPrice : $this->getItemPrice($parentItem),
                    'total_cargos' => $cargos,
                    'region' => $shippingAddress ? (int)$shippingAddress->getRegionCode() : 007,
                    'comuna' => $shippingAddress ? (int)$shippingAddress->getComuna() : 346,
                    'direccion' => $shippingAddress ? $shippingAddress->getStreet()[0] : "Direccion no aplica a productos virtuales"
                ];

                if (in_array($this->_voucherHelper->getCurrentStoreCode(),[
                    VoucherHelper::VOUCHER_STOREVIEW_CODE,
                    Voucher2021Constants::STORE_VIEW_CODE
                ])) {
                    if ($productObj->getTypeId() == 'virtual') {
                        $options = $item->getBuyRequest()->getData();
                        $price = 0;
                        if (array_key_exists('mpassignproduct_id', $options)) {
                            $mpAssignId = $options['mpassignproduct_id'];
                            $price = $this->_assignProductHelper->getAssignProductPrice($mpAssignId);
                        }

                        $voucherProductType = (int)$productObj->getData('voucher_product_type');
                        if ($voucherProductType === VoucherHelper::BONIFICATION || $voucherProductType === VoucherHelper::DISCOUNT) {
                            $msg = $voucherProductType === VoucherHelper::BONIFICATION ? 'Bonificacion ' : 'Descuento ';
                            $msg .= number_format($price, 1, ',', '.') . '%';
                            $product = array_merge($product, ['esp_proveedor' => $msg]);
                        }

                        // MPMT-20: CM Voucher - Descuentos en OC
                        if ((int)$productObj->getData('voucher_product_type') === VoucherHelper::DISCOUNT)
                        {
                            $actualPrice = $this->getItemPrice($parentItem);
                            /**
                             * Update the value of $product['precio_neto'] to the real price of the product without the discount applied
                             *
                             * FORMULA
                             * x = realPrice;
                             * y = discount (the value of the discount is stored on the price variable);
                             * z = actualPrice;
                             *
                             * actualPrice = realPrice - [realPrice * (discount / 100)]
                             * z = x - [x * (y / 100)]
                             *
                             * realPrice = actualPrice / [1 - (discount / 100)]
                             * x = z / [1 - (y / 100)]
                             *
                             */
                            $realPrice = $actualPrice / (1 - ($price/100));
                            $product['precio_neto'] = $realPrice;
                        }
                    }
                }
                if (
                    $this->_voucherHelper->getCurrentStoreCode() == CombustiblesHelper::CODE_STORE_AGREEMENT_FUELS ||
                    $this->_voucherHelper->getCurrentStoreCode() == Constants::STORE_CODE
                ) {
                    if ($productObj->getTypeId() == 'virtual') {
                        $options = $item->getBuyRequest()->getData();
                        $price = 0;
                        if (array_key_exists('mpassignproduct_id', $options)) {
                            $mpAssignId = $options['mpassignproduct_id'];
                            $price = $this->_assignProductHelper->getAssignProductPrice($mpAssignId);
                        }
                        $msg = 'Descuento $';
                        $msg .= number_format($price, 0, ',', '.');
                        $product = array_merge($product, ['esp_proveedor' => $msg]);
                    }
                }

                array_push($products, $product);
            }
        }

        return $products;
    }

    /**
     * Get customer order
     *
     * @param \Magento\Sales\Model\Order $order
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer($order)
    {
        return $this->_customer->load($order->getCustomerId());
    }

    /**
     * Get seller ID order
     *
     * @param \Magento\Sales\Model\Order $order
     * @return int
     */
    public function getSellerId($order)
    {
        $quote = $this->getQuoteById($order->getQuoteId());

        foreach ($quote->getAllVisibleItems() as $item) {
            if (!$item->hasParentItemId()) {
                $options = $item->getBuyRequest()->getData();
                $sellerId = null;
                $collectionProduct = null;

                if (isset($options['mpassignproduct_id'])) {
                    $model = $this->_mpModel->load($options['mpassignproduct_id']);
                    if ($model->getSellerId()) {
                        $sellerId = $model->getSellerId();
                    }
                } elseif (array_key_exists('seller_id', $options)) {
                    $sellerId = $options['seller_id'];
                } else {
                    $collectionProduct = $this->_mpProduct
                        ->getCollection()
                        ->addFieldToFilter(
                            'mageproduct_id',
                            $item->getProductId()
                        );
                    foreach ($collectionProduct as $value) {
                        if ($value->getSellerId()) {
                            $sellerId = $value->getSellerId();
                        }
                    }
                }
                $this->logData('[OK]CheckoutSubmitBeforeObserver::getSellerId ' . $sellerId);
                return $sellerId;
            }
        }
        $this->logData('[NOK]CheckoutSubmitBeforeObserver::getSellerId');

        return null;
    }

    /**
     * Get if shipping order is storepickup
     *
     * @param \Magento\Sales\Model\Order $order
     * @return bool
     */
    public function isStorePickup($order)
    {
        $shippingMethod = 'NA';
        if ($order->getShippingMethod()) {
            $shippingMethod = $order->getShippingMethod();
        }

        return $shippingMethod == 'dccp_storepickup';
    }

    /**
     * Get user rest ID organization order
     *
     * @param \Magento\Sales\Model\Order $order
     * @return int
     */
    public function getProviderId($order)
    {
        $sellerId = $this->getSellerId($order);
        if (!$sellerId) {
            throw new \Exception(__('Error al obtener el proveedor de su pedido.'));
        }

        $seller = $this->_customer->load($sellerId);
        return $seller->getUserRestIdOrganization();
    }

    /**
     * Get tax amount ordered
     *
     * @param \Magento\Sales\Model\Order $order
     * @return float
     */
    public function getTaxAmount($order)
    {
        return $order->getTaxAmount();
    }

    /**
     * Get shipping amount ordered
     *
     * @param \Magento\Sales\Model\Order $order
     * @return float
     */
    public function getShippingAmount($order)
    {
        if ($this->isEmergenciasWebsite() || $this->isInsumosWebsite()) {
            return 0;
        }
        return $order->getShippingAmount();
    }

    /**
     * Get error message
     *
     * @param array $errors
     * @return string
     */
    public function createErrorMessage($errors)
    {
        $message = '';
        foreach ($errors as $error) {
            $message .= 'Error: ' . $error->codigo . '. ' . $error->descripcion . '<br>';
        }

        return $message;
    }

    /**
     * Call Chilecompra ERP service
     *
     * @param \Magento\Sales\Model\Order $order
     * @return bool
     */
    public function execute($order)
    {
        try {
            $incrementId = $order->getIncrementId();
            $quote = $this->getQuoteById($order->getQuoteId());
            $requestforquoteId = $this->helperSoftware->getRequestforquotedIdFromCart($quote->getId());

            $incrementId = '';
            if ($quote !== null) {

                if ($this->isZeroTaxWebSite()){
                    //hotFix Combustibles
                    $quote->setWkTaxRule('SINVALOR');
                }

                $idimpuesto = 1;
                if ($quote->getWkTaxRule() == 'EXENTO' || $quote->getWkTaxRule() == 'SINVALOR'){
                    $idimpuesto = 40;
                };

                $this->logData('DATA: ' . json_encode($order->getData()));
                $orderData = $order->getData();
                $customer = $this->getCustomer($order);
                $token = $customer->getUserRestAtk();
                //$this->logData('['.'] Token: ' . $token);

                $tic = '';
                if ($this->getStoreCode() == HelperSoftware::SOFTWARE_STOREVIEW_CODE || $this->getStoreCode() == SoftwareRenewalConstants::STORE_CODE) {
                    if ($this->getRequestForquoteData($requestforquoteId)) {
                        $tic = 'Código comité TIC: ' . $this->getRequestForquoteData($requestforquoteId);
                    }
                }

                $request = [
                    'idproveedor' => $this->getProviderId($order),
                    'idorganizacion_compradora' => $quote->getPurchaseUnit(),
                    'moneda' => $orderData['base_currency_code'],
                    'instruccion_envio' => $tic,
                    'total_descuentos' => $orderData['discount_amount'],
                    'total_cargos' => $this->getShippingAmount($order),
                    'total_impuesto' => $this->getTaxAmount($order),
                    'idimpuesto' => ($this->getStoreCode() == HelperSoftware::SOFTWARE_STOREVIEW_CODE || $this->getStoreCode() == SoftwareRenewalConstants::STORE_CODE) ? 40 : $idimpuesto,
                    'comentario_descuento' => '',
                    'productos' => $this->getProducts($order)
                ];

                // MPMT-20: CM Voucher - Descuentos en OC
                // check if store is CM Voucher, then update the $request['total_descuentos'] value with the right value
                $storeCode = $this->getStoreCode();
                if (in_array($storeCode,[self::CONVENIO_VOUCHER, LinetsVoucherConstants::STORE_VIEW_CODE])) {
                    $request['total_descuentos'] = $this->getTotalDiscountCMVoucher($order);
                }
                // Change symbol currency orden Computadores
                if (ComputadoresRenewal2Constants::WEBSITE_CODE == $this->_storeManager->getWebsite()->getCode()) {
                    $request['moneda'] = $orderData['order_currency_code'];
                }

                $this->logData('['.'] Service Request: ' . json_encode($request));
                $response = $this->sendRequest($request, $token);
                $this->logData('['.'] Service Response: ' . $response);
                $response = json_decode($response);

                $actionLogs = $this->_actionLogsFactory->create();
                $data = [
                    'action' => 'Consumo Servicio',
                    'request' => $request,
                    'response' => $response
                ];

                $additionalData = $this->_serializer->serialize($data);
                $actionLogs->setIdUser($order->getCustomerId());
                $actionLogs->setNameUser('');
                $actionLogs->setActionName($incrementId);
                $actionLogs->setProfileName('Comprador');
                $actionLogs->setIp($this->_remoteAddress->getRemoteAddress());
                $actionLogs->setDataSerialize($additionalData);
                $actionLogs->setStoreView($this->getStoreName());
                $actionLogs->save();

                if (isset($response->success) && $response->success == 'OK') {
                    $odc = trim($response->payload->codigoOrdenDeCompra);
                    $usoc = trim($response->payload->urlSimplificacionOC);
                    $this->logData('['.'] ODC: ' . $odc);

                    $quote->setOrdenDeCompra($odc);
                    $quote->setUrlOrdenDeCompra($usoc);
                    $quote->save();

                    $order->setOrdenDeCompra($odc);
                    $order->setUrlOrdenDeCompra($usoc);
                    $order->setPurchaseUnit($quote->getPurchaseUnit());
                    $order->addStatusHistoryComment('ODC MP: ' . $odc);
                    $order->save();
                    $this->updateRequestforquoteIdIntoOrder($requestforquoteId, $order->getId());
                    $this->changeStatusRequestforquote($requestforquoteId);

                    return true;
                } else {
                    //BAD
                    $excDet = '';
                    if (isset($response->errores) && is_array($response->errores)) {
                        $excDet = $this->createErrorMessage($response->errores);
                    }
                    $additionalData = $this->_serializer->serialize($data);
                    $actionLogs->setIdUser($order->getCustomerId());
                    $actionLogs->setNameUser('');
                    $actionLogs->setActionName($order->getIncrementId());
                    $actionLogs->setProfileName('Comprador');
                    $actionLogs->setIp($this->_remoteAddress->getRemoteAddress());
                    $actionLogs->setDataSerialize($additionalData);
                    $actionLogs->setStoreView($this->getStoreName());
                    $actionLogs->save();
                    throw new \Exception(__('No se ha podido procesar su pedido, intente de nuevo más tarde. ' . $excDet));
                }
            } else {
                throw new \Exception(__('No se ha podido procesar su pedido, intente de nuevo más tarde. Información vacía.'));
            }
        } catch(\Exception $ex) {
            $order->cancel()->save();
            $this->logData($order->getIncrementId() . ' -- ' . $ex->getMessage());
        }

        return false;
    }

    /**
     * Execute webservice
     *
     * @param string $request
     * @param string $token
     * @return object
     */
    public function sendRequest($request, $token)
    {
        try {
            $endpoint = $this->_scopeConfig->getValue(
                'sales/erp_integration_section/generate_purchase_order_endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
            $client = $this->_httpClientFactory->create();
            $client->setUri($endpoint);
            $client->setHeaders([
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
            ]);

            $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
            $client->setRawData(json_encode($request));
            return $client->request(\Zend_Http_Client::POST)->getBody();
        } catch(\Exception $ex) {
            return null;
        }
    }

    /**
     * Get quote by quote ID
     *
     * @param int $quoteId
     * @return \Magento\Quote\Model\QuoteFactory
     */
    public function getQuoteById($quoteId)
    {
        $quote = $this->_quoteFactory->create()->load($quoteId);
        return $quote;
    }

    /**
     * Get item price
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return float
     */
    protected function getItemPrice($item)
    {

        if (in_array(
                $this->getStoreCode(),
                [
                    \Summa\CombustiblesSetUp\Helper\Data::CODE_STORE_AGREEMENT_FUELS,
                    \Chilecompra\CombustiblesRenewal\Model\Constants::STORE_CODE
                ]
            ))
        {
            if ($item->getPrice() > 99999999){
                //retornamos el valor ya que Combustibles no posee impuestos.
                return   $item->getPriceInclTax();
            }
            else{
                return $item->getBasePrice();
            }
        }
        //En caso de Vucher, si se limita la cantidad a 1 ->
        //es posible retornar el valor de RowTotalId
        //que no posee impuestos pero tiene que ser solo un item

        if ($this->isMobiliarioWebsite()){
            if($item->getProductType() == 'quote'){
                if ($item->getProductOptionByCode('info_buyRequest') && isset($item->getProductOptionByCode('info_buyRequest')['base_price'])){
                    return $item->getProductOptionByCode('info_buyRequest')['base_price'];
                }else{
                    $assemblyPrice = $this->_assignProductHelper->getAssemblyPriceByRequestedSku($item->getSku(), $item->getSellerId());
                    return (float)$item->getPrice()- (float)$assemblyPrice;
                }

            }
            else{
                $model = $this->_mpModel->load($item->getBuyRequest()->getMpassignproductId());
                if($model->getPrice() != ''){
                    return $model->getPrice();
                }
                else{
                 return $model->getBasePrice();
                }
            }
        }

        if ($this->isEmergenciasWebsite()) {
            if ($item->getProductType() == 'configurable') {
                return $item->getPrice();
            }
            $model = $this->_mpModel->load($item->getBuyRequest()->getMpassignproductId());
            return $model->getPrice() - $model->getShippingPrice();
        }

        if ($this->getStoreCode() == \Linets\InsumosSetup\Model\InsumosConstants::WEBSITE_CODE) {
            if ($item->getProductType() == 'configurable') {
                return $item->getPrice();
            }
            $model = $this->_mpModel->load($item->getBuyRequest()->getMpassignproductId());
            return $model->getPrice();
        }

        return $item->getPrice();
    }

    /**
     * Get shipping price
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return float
     */
    private function getShippingPrice($item)
    {
        if (!$this->isEmergenciasWebsite())
            return 0;

        $model = $this->_mpModel->load($item->getBuyRequest()->getMpassignproductId());
        return $shippingPrice = ($model->getShippingPrice() == null) ? 0 : $model->getShippingPrice() * $item->getQtyOrdered();
    }

    private function getAssemblyPrice($item)
    {
        if(!$this->isMobiliarioWebsite()){
            return 0;

        }
        //we could add fix here for assebleprice bug-j-173
        $model = $this->_mpModel->load($item->getBuyRequest()->getMpassignproductId());
        return $shippingPrice = ($model->getAssemblyPrice() == null) ? 0 : $model->getAssemblyPrice() * $item->getQtyOrdered();
    }

    /**
     * Get if current store if Emergencias
     *
     * @return bool
     */
    protected function isEmergenciasWebsite()
    {
        return in_array($this->_storeManager->getStore()->getCode(), [\Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE, ConstantsEmergency202109::WEBSITE_CODE]);
    }

    /**
     * Get if current store if Insumos Medicos
     *
     * @return bool
     */
    protected function isInsumosWebsite(): bool
    {
        try {
            $result = $this->_storeManager->getStore()->getCode() == InsumosConstants::WEBSITE_CODE;
        } catch(\Exception $e){
            $result = false;
        }
        return $result;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return float|int
     */
    protected function getInsumosShippingAmount(Order $order): float
    {
        $shippingAmount = 0;
        foreach ($order->getItems() as $item) {
            if (!$item->getParentItem()) {
                continue;
            }
            $sellerId = $this->getSellerId($order);
            $shippingAmount += $this->getInsumosShippingAmountPerProduct($item, $sellerId) * $item->getQtyOrdered();
        }
        return $shippingAmount;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderItemInterface $item
     * @param int $sellerId
     * @return int
     */
    protected function getInsumosShippingAmountPerProduct(OrderItemInterface $item, int $sellerId): int
    {
        try {
            $product = $item->getProduct();
            $macrozoneAttribute = $product->getResource()->getAttribute('macrozona');
            $macrozone = $macrozoneAttribute->getSource()->getOptionText($product->getMacrozona());
            $attributeSetId = $product->getAttributeSetId();
            $websiteId = $this->_storeManager->getWebsite()->getId();
            $dispatchPrice = $this->dispatchPriceManagement->getDispatchPrice($sellerId, $macrozone, $attributeSetId, $websiteId);
            $result = (int)($dispatchPrice ? $dispatchPrice->getPrice() : 0);
        }catch(LocalizedException $e) {
            $result = 0;
        }
        return $result;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function isZeroTaxWebSite(): bool
    {
        return in_array(
            $this->_storeManager->getStore()->getCode(),
            [
                \Summa\CombustiblesSetUp\Helper\Data::WEBSITE_CODE,
                \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE,
                \Chilecompra\CombustiblesRenewal\Model\Constants::WEBSITE_CODE
            ]
        );
    }

    protected function isMobiliarioWebsite()
    {
        return $this->_storeManager->getStore()->getCode() == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE;
    }


    /**
     * Write log message into custom file
     *
     * @param string $data
     */
    public function logData($data)
    {
        $this->_logger->info($data);
    }

    /**
     * Get Store Code
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }

    /**
     * Get the Total Discount amount on CM Voucher for voucher_product_type discount
     *
     * @param \Magento\Sales\Model\Order $order
     * @return float|int
     */
    public function getTotalDiscountCMVoucher($order)
    {
        $discountAmount = 0;
        $items = $order->getItems();
        foreach ($items as $item) {
            if ($item->getProductType() == Type::TYPE_VIRTUAL) {
                $parentItem = $item->getParentItem();
                $parentItem = !$parentItem || $parentItem === null ? $item : $parentItem;
                $productObj = $parentItem->getProduct();
                if ($productObj->getTypeId() == Type::TYPE_VIRTUAL) {
                    $options = $item->getBuyRequest()->getData();
                    $discount = 0;
                    if (array_key_exists('mpassignproduct_id', $options)) {
                        $mpAssignId = $options['mpassignproduct_id'];
                        $discount = $this->_assignProductHelper->getAssignProductPrice($mpAssignId);
                    }
                    if ((int)$productObj->getData('voucher_product_type') === VoucherHelper::DISCOUNT) {
                        $actualPrice = $this->getItemPrice($parentItem);
                        /**
                         * Update the value of $request['total_descuentos'] with the value of the discount applied to the product
                         *
                         * FORMULA
                         * x = discountAmount;
                         * y = discount;
                         * z = actualPrice
                         *
                         * x = (y * z) / (100 - y)
                         * discountAmount = (discount * actualPrice) / (100 - discount)
                         */
                        $discountAmount += ($discount * $actualPrice * $item->getQtyOrdered()) / (100 - $discount);
                    }
                }
            }
        }
        return $discountAmount;
    }

    /**
     * Get current store name
     *
     * @return string
     */
    public function getStoreName()
    {
        return trim($this->_storeManager->getStore()->getName());
    }

    /**
     * Change status for requestforquote
     *
     * @param int $idCotizacion
     * @return void
     */
    public function changeStatusRequestforquote($idCotizacion)
    {
        $con = $this->_resource->getConnection();
        $status = \Formax\QuotesSupplier\Helper\Data::AWARDED;
        $dateQuote = $con->quote($this->dateTime->gmtDate());
        $sql = "UPDATE requestforquote_quote SET award_date = " . $dateQuote . ", status = " . $status . " WHERE entity_id = " . $idCotizacion;
        $con->query($sql);
    }

    /**
     * get TIC from requestforquote
     *
     * @param int $requestforquoteId
     * @return string
     */
    public function getRequestForquoteData($requestforquoteId)
    {
        $con = $this->_resource->getConnection();
        $sql = 'SELECT tic FROM requestforquote_quote WHERE entity_id = '. (int)$requestforquoteId;
        return $con->fetchOne($sql);
    }

    /**
     * Update requestforquote_id into sales_order
     *
     * @param int $requestforquoteId
     * @param int $orderId
     * @return void
     */
    public function updateRequestforquoteIdIntoOrder($requestforquoteId, $orderId)
    {
        $requestforquoteId = (int)$requestforquoteId;
        $orderId = (int)$orderId;

        if ($requestforquoteId > 0 && $orderId > 0) {
            $con = $this->_resource->getConnection();
            $sql = 'UPDATE sales_order SET requestforquote_info = ' . $requestforquoteId . ' WHERE entity_id = ' . $orderId;
            $con->query($sql);
        }
    }

    /**
     * @param OrderItemInterface $item
     * @return OrderItemInterface
     */
    protected function loadOrderParentItem($item)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('sku', $item->getSku(), 'eq')
            ->addFilter('order_id', $item->getOrderId(), 'eq')
            ->create();
        $items = $this->orderItemRepository->getList($searchCriteria)->getItems();
        foreach ($items as $loadedItem) {
            $price = (int) $this->getItemPrice($loadedItem);
            if ($price != 0) {
                $item = $loadedItem;
            }
        }

        return $item;
    }
}
