<?php

namespace Formax\OrderManagement\Test\Unit\Model;

class OrderTest extends  \PHPUnit\Framework\TestCase {

    protected $_objectManager;
    protected $_desiredResult;
    protected $_actulResult;
    protected $_calculator;
    private $sut;
    private $_order;

    /**
     * unset the variables and objects after use
     *
     * @return void
     */
    public function tearDown() {

    }

    /**
     * used to set the values to variables or objects.
     *
     * @return void
     */
    public function setUp() {
        $this->_objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->_order = $this->_objectManager->getObject("Formax\OrderManagement\Model\Order");
        //can do stuff
    }
  /**
    * Test if the order update status
    *
    * @param string $abc
    * @param object $result
    *
    * @dataProvider dataProviderEmails
    */
  public function testUpdateStatus(string $abc, object $result)
  {
      $expected = $this->_order->UpdateStatus($abc);
      $this->assertEquals($expected->getCode(), $result->code);
  }

  /**
    * @return array
    */
  public function dataProviderEmails(): array
  {
    return [
      [2, (object)['code'=> 200]],
      /*[2, (object)['code'=> 400]]*/
    ];
  }}
