<?php
namespace Formax\OrderManagement\Api;

interface OrderInterface {
    /**
     * Returns OK or Error
     *
     * @api
     * @param string $odv
     * @return \Formax\OrderManagement\Api\Data\ResponseInterface
    */
    public function UpdateStatus($odv);
}