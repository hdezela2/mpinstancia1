<?php
namespace Formax\OrderManagement\Api\Data\Impl;
class Response implements \Formax\OrderManagement\Api\Data\ResponseInterface
{
    protected $code;
    protected $message;
    /**
    * @return int
    **/
    public function getCode() {
        return $this->code;
    }
    /**
    * @param int $code
    * @return $this
    **/
    public function setCode($code) {
        $this->code = $code;
        return $this;
    }
    /**
    * @return string
    **/
    public function getMessage() {
        return $this->message;
    }
    /**
    * @param string $message
    * @return $this
    **/
    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }
}
