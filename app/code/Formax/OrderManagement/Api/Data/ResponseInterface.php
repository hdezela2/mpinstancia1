<?php
namespace Formax\OrderManagement\Api\Data;
interface ResponseInterface
{
    /**
    * @return int
    **/
    public function getCode();
    /**
    * @param int $code
    * @return $this
    **/
    public function setCode($code);
    /**
    * @return string
    **/
    public function getMessage();
    /**
    * @param string $message
    * @return $this
    **/
    public function setMessage($message);
}
