<?php
namespace Formax\AllowSvg\Plugin;

class UploadPlugin{

  /**
   * setAllowedExtensions for modify the extensions array to allow SVG
   * @param \Magento\Framework\File\Uploader $subject
   * @param \Closure $proceed
   * @param array $extensions
   * @return array
   */
  public function aroundSetAllowedExtensions(\Magento\Framework\File\Uploader $subject, \Closure $proceed,  $extensions = [])
    {
        if (!in_array('svg', $extensions)) {
            $extensions[] = 'svg';
        }
        return $proceed($extensions);
    }

}
