<?php
/**
 * @category Formax
 * @package Formax_ProductVisibility
 * @author Formax
 */

namespace Formax\ProductVisibility\Block\Product;

use Formax\AssignProduct\Model\LinkedProductFactory;
use Linets\AseoRenewalSetup\Api\Setup\AttributeSetInterface as AttributeSetInterfaceAseo2022;
use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Linets\GasSetup\Constants;
use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\Customer\Model\Session;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\StoreManagerInterface;
use Summa\EmergenciasSetUp\Helper\Data;
use Webkul\Marketplace\Helper\Data as marketplaceHelper;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as AssignProductCollection;
use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;
use Summa\AlimentosSetUp\Helper\Data as AlimentosHelper;
use Summa\ProductPackages\Helper\Data as PackagesHelper;
use Magento\Store\Model\ResourceModel\Group\CollectionFactory as StoreGroupCollection;
use Zend_Db_Expr;
use Magento\Eav\Api\Data\AttributeSetInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Api\AttributeSetRepositoryInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Linets\InsumosSetup\Api\Setup\AttributeSetInterface as AttributeSetInterfaceData;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory as AttributeOptionCollectionFactory;

class ProductList extends \Webkul\MpAssignProduct\Block\Product\ProductList
{

    const CODE_ATTRIBUTE_MARCA = 'marca';

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Attribute
     */
    protected $eavAttribute;

    /**
     * @var Collection
     */
    protected $_voucherProductsList;

    /**
     * @var LinkedProductFactory
     */
    protected $linkedProductFactory;

    /**
     * @var VoucherHelper
     */
    protected $voucherHelper;

    /**
     * @var AlimentosHelper
     */
    protected $alimentosHelper;

    /**
     * @var PackagesHelper
     */
    protected $packagesHelper;

    /**
     * @var StoreGroupCollection
     */
    protected $_storeGroupCollection;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var AttributeSetRepositoryInterface
     */
    protected $attributeSetRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var AttributeRepositoryInterface
     */
    protected $eavAttributeRepository;

    /**
     * @var AttributeOptionCollectionFactory
     */
    protected $attributeOptionFactory;

    /**
     * ProductList constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     * @param ProductCollection $productCollectionFactory
     * @param CollectionFactory $mpProductCollectionFactory
     * @param Status $productStatus
     * @param Visibility $productVisibility
     * @param AssignProductCollection $assignProductCollection
     * @param TimezoneInterface $timezone
     * @param ResourceConnection $resource
     * @param StoreManagerInterface $storeManager
     * @param Attribute $eavAttribute
     * @param VoucherHelper $voucherHelper
     * @param AlimentosHelper $alimentosHelper
     * @param PackagesHelper $packagesHelper
     * @param LinkedProductFactory $linkedProductFactory
     * @param StoreGroupCollection $storeGroupCollection
     * @param CategoryFactory $categoryFactory
     * @param CategoryRepository $categoryRepository
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param AttributeRepositoryInterface $eavAttributeRepository
     * @param array $data
     * @param AttributeOptionCollectionFactory $attributeOptionFactory
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        \Webkul\MpAssignProduct\Helper\Data $helper,
        ProductCollection $productCollectionFactory,
        CollectionFactory $mpProductCollectionFactory,
        Status $productStatus,
        Visibility $productVisibility,
        AssignProductCollection $assignProductCollection,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        marketplaceHelper $marketplaceHelper,
        \Magento\Catalog\Helper\Image $catalogImage,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceInterface,
        \Magento\Checkout\Helper\Cart $checkoutHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        TimezoneInterface $timezone,
        ResourceConnection $resource,
        StoreManagerInterface $storeManager,
        Attribute $eavAttribute,
        VoucherHelper $voucherHelper,
        AlimentosHelper $alimentosHelper,
        PackagesHelper $packagesHelper,
        LinkedProductFactory $linkedProductFactory,
        StoreGroupCollection $storeGroupCollection,
        CategoryFactory $categoryFactory,
        CategoryRepository $categoryRepository,
        AttributeSetRepositoryInterface $attributeSetRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        AttributeRepositoryInterface $eavAttributeRepository,
        array $data = [],
        AttributeOptionCollectionFactory $attributeOptionFactory
    )
    {
        $this->timezone = $timezone;
        $this->resource = $resource;
        $this->_storeManager = $storeManager;
        $this->eavAttribute = $eavAttribute;
        $this->voucherHelper = $voucherHelper;
        $this->alimentosHelper = $alimentosHelper;
        $this->packagesHelper = $packagesHelper;
        $this->linkedProductFactory = $linkedProductFactory;
        $this->_storeGroupCollection = $storeGroupCollection;
        $this->categoryFactory = $categoryFactory;
        $this->categoryRepository = $categoryRepository;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->eavAttributeRepository = $eavAttributeRepository;
        $this->attributeOptionFactory = $attributeOptionFactory;

        parent::__construct(
            $context,
            $customerSession,
            $helper,
            $productCollectionFactory,
            $mpProductCollectionFactory,
            $productStatus,
            $productVisibility,
            $assignProductCollection,
            $pricingHelper,
            $marketplaceHelper,
            $catalogImage,
            $priceInterface,
            $checkoutHelper,
            $jsonHelper,
            $data
        );
    }

    /**
     * @return bool|Collection
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getAllProducts()
    {
        if (!$this->_productList) {
            $queryString = $this->_assignHelper->getQueryString();
            $attributeSetId = $this->_assignHelper->getAttributeSetId();
            $marcaId = $this->_assignHelper->getMarcaId();
            $prodId = $this->_assignHelper->getProdId();
            $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
            $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 1;
            $connection = $this->resource->getConnection();
            $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
            $currentDate = $connection->quote($this->timezone->date()->format('Y-m-d'));
            $tableConfigurable = $this->resource->getTableName('marketplace_assignproduct_associated_products');
            $tableSimple = $this->resource->getTableName('marketplace_assignproduct_items');
            $tableUserData = $this->resource->getTableName('marketplace_userdata');
            $tableOffer = $this->resource->getTableName('dccp_offer_product');
            $priceCol = $this->_storeManager->getWebsite()->getCode() === Data::WEBSITE_CODE ? 'base_price' : 'price';
            $priceFnc = $this->_storeManager->getWebsite()->getCode() === Data::WEBSITE_CODE ? 'AVG' : 'MIN';
            $rootCategoryId = $this->_storeGroupCollection->create()->addFieldToFilter('website_id', ["eq" => $websiteId])->getFirstItem()->getRootCategoryId();
            $validateActiveSellersOnly = "";
            $websiteCode = $this->_storeManager->getWebsite()->getCode();
            if (in_array($websiteCode, [InsumosConstants::WEBSITE_CODE, AseoRenewalConstants::WEBSITE_CODE])) {
                $validateActiveSellersOnly = "INNER JOIN customer_entity_varchar AS cev ON i.seller_id = cev.entity_id AND cev.attribute_id = 604 AND cev.value = '70' ";
            }


            $minPrice = "(SELECT " . $priceFnc . "(IFNULL(o.base_price, IF(i.type = 'configurable', aso.price, i." . $priceCol . "))) AS custom_price
            FROM " . $tableSimple . " i
            LEFT JOIN " . $tableConfigurable . " aso ON i.id = aso.parent_id
            INNER JOIN " . $tableUserData . " u ON i.seller_id = u.seller_id AND u.is_seller = 1
            $validateActiveSellersOnly
            LEFT JOIN " . $tableOffer . " o ON i.id = o.assign_id AND o.status = 3 AND o.website_id = " . $websiteId .
                " AND o.special_price < o.base_price WHERE i.status = 1 AND IF(i.type = 'configurable', aso.qty > 0, i.qty > 0) AND i.dis_dispersion = 0 AND i.product_id = e.entity_id) AS custom_price";

            if ($this->areGetParamValid()) {
                $queryString = str_replace(' ', '%', $queryString);
                $customerId = $this->_assignHelper->getCustomerId();
                $sellercollection = $this->_mpProductCollection
                    ->create()
                    ->addFieldToFilter('seller_id', ['eq' => $customerId])
                    ->addFieldToSelect('mageproduct_id');
                $products = [];
                foreach ($sellercollection as $data) {
                    array_push($products, $data->getMageproductId());
                }

                $sellerAssigncollection = $this->_assignProductCollection
                    ->create()
                    ->addFieldToFilter('seller_id', $customerId)
                    ->addFieldToSelect('product_id');

                if ($this->_storeManager->getWebsite()->getCode() == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) {
                    $linkedProduct = $this->linkedProductFactory->create();
                    $linkedPending = $linkedProduct->getCollection()
                        ->addFieldToFilter('seller_id', $customerId)
                        ->addFieldToFilter('status', ['eq' => 0])
                        ->addFieldToSelect('product_id')
                        ->getItems();
                    foreach ($linkedPending as $data) {
                        array_push($products, $data->getProductId());
                    }
                }

                foreach ($sellerAssigncollection as $data) {
                    array_push($products, $data->getProductId());
                }

                if ($this->_storeManager->getWebsite()->getCode() === AlimentosHelper::WEBSITE_CODE) {
                    array_push($products, $this->packagesHelper->getAllPackagesIds());
                }

                if ($this->_storeManager->getStore()->getCode() == AseoRenewalConstants::STORE_CODE) {
                    $allowedTypes = ['configurable'];
                } else {
                    $allowedTypes = $this->_assignHelper->getAllowedProductTypes();
                }

                $collection = $this->_productCollection
                    ->create()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('name', ['like' => $queryString . '%']);
                $collection->addFieldToFilter('type_id', ['in' => $allowedTypes]);
                if ($attributeSetId) {
                    $collection->addFieldToFilter('attribute_set_id', $attributeSetId);
                }
                if ($marcaId) {
                    $collection->addFieldToFilter('marca', $marcaId);
                }
                if ($prodId) {
                    $collection->addFieldToFilter('sku', ['like' => '%' . $prodId . '%']);
                }

                if (count($products) > 0) {
                    $collection->addFieldToFilter('entity_id', ['nin' => $products]);
                }

                if ($this->_storeManager->getWebsite()->getCode() == Constants::GAS_WEBSITE_CODE) {
                    // TODO get category value from core_config_data
                    $categoryCollection = $this->categoryFactory->create()->getCollection()
                        ->addAttributeToFilter('name', ['in' => 'GAS LICUADO DE PETRÓLEO EN MODALIDAD GRANEL'])
                        ->setPageSize(1);

                    if ($categoryCollection->getSize()) {
                        $categoryId = $categoryCollection->getFirstItem()->getId();
                        $collection->addCategoriesFilter(['in' => $categoryId]);
                    }
                }

                $collection->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()]);
                $collection->addAttributeToFilter('entity_id', [
                    'nin' => new Zend_Db_Expr('SELECT product_id FROM catalog_product_super_link')
                ]);
                $collection->setOrder('created_at', 'desc');
            } else {
                $collection = $this->_productCollection
                    ->create()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('row_id', 0);
            }

            $collection->getSelect()->join(
                ['cap' => 'catalog_category_product'],
                'e.entity_id = cap.product_id',
                []
            )->join(
                ['cat' => 'catalog_category_entity'],
                'cap.category_id = cat.entity_id and cat.path like "1/' . (int)$rootCategoryId . '/%"',
                []
            );

            $collection->getSelect()->columns($minPrice);
            $collection->distinct(true);
            $collection->setPageSize($pageSize);
            $collection->setCurPage($page);
            $this->_productList = $collection;
        }

        return $this->_productList;
    }

    /**
     * @return bool|Collection
     * @throws NoSuchEntityException|LocalizedException
     */
    public function getAllProductsVoucher()
    {
        if (!$this->_voucherProductsList) {
            $queryString = $this->_assignHelper->getQueryString();
            $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
            $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 1;
            $connection = $this->resource->getConnection();
            $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
            $currentDate = $connection->quote($this->timezone->date()->format('Y-m-d'));
            $tableSimple = $this->resource->getTableName('marketplace_assignproduct_items');
            $tableOffer = $this->resource->getTableName('dccp_offer_product');
            $tableCatalogProductInt = $this->resource->getTableName('catalog_product_entity_int');
            $attrVoucherProductTypeId = $this->eavAttribute->getIdByCode('catalog_product', 'voucher_product_type');
            $customerId = $this->_assignHelper->getCustomerId();

            $avgPrice = '(SELECT AVG(IFNULL(o.base_price, i.price)) AS custom_price FROM ' . $tableSimple . ' i ' .
                'LEFT JOIN ' . $tableCatalogProductInt . ' AS vpt ' .
                'ON i.product_row_id = vpt.row_id AND vpt.store_id = 0 AND
            vpt.attribute_id = ' . $attrVoucherProductTypeId . ' ' .
                'LEFT JOIN ' . $tableOffer . ' o ON i.id = o.assign_id AND o.status = 3 ' .
                'AND o.website_id = ' . $websiteId . ' AND o.special_price > o.base_price WHERE i.status = 1 AND i.qty > 0 AND i.dis_dispersion = 0 ' .
                'AND i.seller_id = ' . $customerId . ' AND vpt.value = ' . VoucherHelper::BONIFICATION . ')';

            if ($queryString != '') {
                $sellerAssigncollection = $this->_assignProductCollection
                    ->create()
                    ->addFieldToFilter('seller_id', $customerId)
                    ->addFieldToSelect('product_id');

                $products = [];
                foreach ($sellerAssigncollection as $data) {
                    array_push($products, $data->getProductId());
                }

                $collection = $this->_productCollection
                    ->create()
                    ->addAttributeToSelect('*')
                    ->addWebsiteFilter($this->_storeManager->getWebsite()->getId())
                    ->addAttributeToFilter('name', ['like' => '%' . $queryString . '%'])
                    ->addFieldToFilter('type_id', 'virtual')
                    ->addAttributeToFilter(
                        'status', [
                            'in' => $this->_productStatus->getVisibleStatusIds()
                        ]
                    )
                    ->setOrder('created_at', 'desc');

                if (count($products) > 0) {
                    $collection->addFieldToFilter('entity_id', ['nin' => $products]);
                }
            } else {
                $collection = $this->_productCollection
                    ->create()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('row_id', 0);
            }

            $collection->getSelect()->columns(['custom_price' => new Zend_Db_Expr($avgPrice)]);
            $collection->setPageSize($pageSize);
            $collection->setCurPage($page);
            $this->_voucherProductsList = $collection;
        }

        return $this->_voucherProductsList;
    }

    /**
     * @return $this
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $collection = $this->voucherHelper->getCurrentStoreCode() == VoucherHelper::VOUCHER_STOREVIEW_CODE
            ? $this->getAllProductsVoucher() : $this->getAllProducts();

        if ($collection) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'mpassignproduct.product.list.voucher.pager'
            )
                ->setShowPerPage(true)
                ->setCollection($collection);
            $this->setChild('pager', $pager);
            $collection->load();
        }

        return $this;
    }

    /**
     * list attribute set
     *
     * @return AttributeSetInterface[]|null
     */
    public function listAttributeSet()
    {
        try {
            $storeCode = $this->_storeManager->getStore()->getCode();
            if ($storeCode == InsumosConstants::WEBSITE_CODE) {
                $this->searchCriteriaBuilder->addFilter('attribute_set_name', AttributeSetInterfaceData::ATTRIBUTE_SETS, 'in');
            }
            if ($storeCode == AseoRenewalConstants::WEBSITE_CODE) {
                $this->searchCriteriaBuilder->addFilter('attribute_set_name', AttributeSetInterfaceAseo2022::ATTRIBUTE_SETS, 'in');
            }

            $searchCriteria = $this->searchCriteriaBuilder->create();
            $attributeSetList = $this->attributeSetRepository->getList($searchCriteria)->getItems();
        } catch (\Exception $exception) {
            $attributeSetList = null;
        }

        return $attributeSetList;
    }

    /**
     * @return boolean
     * @throws LocalizedException
     */
    public function areGetParamValid()
    {
        $paramsAreValid = false;
        $queryString = $this->_assignHelper->getQueryString();
        $attributeSetId = $this->_assignHelper->getAttributeSetId();
        $marcaId = $this->_assignHelper->getMarcaId();
        $prodId = $this->_assignHelper->getProdId();
        $websiteCode = $this->_storeManager->getWebsite()->getCode();

        if ($queryString != '') {
            $paramsAreValid = true;
        } elseif (in_array($websiteCode,[InsumosConstants::WEBSITE_CODE, AseoRenewalConstants::WEBSITE_CODE])) {
            if ($attributeSetId != '') {
                $paramsAreValid = true;
            } elseif ($marcaId != '') {
                $paramsAreValid = true;
            } elseif ($prodId != '') {
                $paramsAreValid = true;
            }
        }

        return $paramsAreValid;
    }

    /**
     * @return array
     */
    public function getMarcaValues()
    {
        try {
            $attribute_id = $this->eavAttribute->getIdByCode(\Magento\Catalog\Api\Data\ProductAttributeInterface::ENTITY_TYPE_CODE, self::CODE_ATTRIBUTE_MARCA);
            $store_id = $this->_storeManager->getWebsite()->getId();

            $collection = $this->attributeOptionFactory->create();
            $table = $this->resource->getTableName('eav_attribute_option_value');
            $collection->getSelect()->join(
                ['tdv' => $table],
                "main_table.option_id = tdv.option_id", ['value' => 'tdv.option_id', 'label' => 'tdv.value'])
                ->where('main_table.attribute_id = ?', $attribute_id)
                ->where('tdv.store_id = ?', $store_id);

        } catch (\Exception $ex) {
            return [];
        }

        return $collection->getData();
    }
}
