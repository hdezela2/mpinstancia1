<?php

namespace Formax\ProductVisibility\Console\Command;

use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Formax\ProductVisibility\Helper\Data as Helper;

class ProductCatalogVisibility extends Command
{
    /**
     * @var string
     */
    const OPTION_SKU_CODE = 'skus';

    /**
     * @var string
     */
    const OPTION_SELLER_EMAIL = 'email_sellers';

    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @param \Magento\Framework\App\State $appState
     * @param Helper $helper
     * @param string $name
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        Helper $helper,
        $name = null
    ) {
        $this->helper = $helper;
        $this->appState = $appState;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('product:visibilityinstore');
        $this->setDescription('You can pass as parameter SKUs comma-separated or Emails seller comma-separated');
        $this->addOption(
            self::OPTION_SKU_CODE,
            null,
            InputOption::VALUE_OPTIONAL,
            'SKUs must be string comma-separated'
        );
        $this->addOption(
            self::OPTION_SELLER_EMAIL,
            null,
            InputOption::VALUE_OPTIONAL,
            'Email sellers must be string comma-separated'
        );

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $paramSku = $input->getOption(self::OPTION_SKU_CODE);
        $paramEmailSeller = $input->getOption(self::OPTION_SELLER_EMAIL);
        $skus = $paramSku !== null ? explode(',', $paramSku) : null;
        $emailSellers = $paramEmailSeller !== null ? explode(',', $paramEmailSeller) : null;
        $result = $this->helper->setVisibilityProduct($skus, $emailSellers);
        $output->writeln($result['msg']);

        return Cli::RETURN_SUCCESS;
    }
}
