<?php

namespace Formax\ProductVisibility\Observer\Adminhtml;

use Magento\Framework\Event\ObserverInterface;
use Formax\ProductVisibility\Helper\Data as HelperData;
use Formax\ProductVisibility\Logger\Logger;

class CustomerSaveAfter implements ObserverInterface
{
    /**
     * @var HelperData
     */
    protected $helper;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Logger $logger
     * @param HelperData $helper
     */
    public function __construct(
        Logger $logger,
        HelperData $helper
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
    }

    /**
     * admin customer save after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer = $observer->getCustomer();
        $customerId = $customer->getId();
        $postData = $observer->getRequest()->getPostValue();
        
        if (isset($postData['customer']['user_rest_id_active_agreement']) &&
            !empty($postData['customer']['user_rest_id_active_agreement'])) {
            try {
                $result = $this->helper->setVisibilityProduct(null, $customerId);
                if (isset($result['error']) && $result['error'] && $result['msg']) {
                    $this->logger->error('CustomerSaveAfter - ' . $result['msg']);
                }
            } catch (\Exception $e) {
                $this->logger->error('CustomerSaveAfter - ' . $e->getMessage());
            }
        }

        return $this;
    }
}
