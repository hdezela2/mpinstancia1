<?php

namespace Formax\ProductVisibility\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Formax\ProductVisibility\Logger\Logger;
use Formax\ProductVisibility\Helper\Data as Helper;
use Magento\Framework\Event\Observer as EventObserver;

class AssignProductItemsSaveAfterCommit implements ObserverInterface
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Helper $helper
     * @param Logger $logger
     */
    public function __construct(
        Helper $helper,
        Logger $logger
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * Update price of product
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        $model = $observer->getEvent()->getObject();        
        $productId = $model->getProductId();
        
        try {
            $sku = $this->helper->getSkuByProductId($productId);
            $result = $this->helper->setVisibilityProduct($sku);

            if (isset($result['error']) && $result['error'] && $result['msg']) {
                $this->logger->error('AssignProductItemsSaveAfterCommit - ' . $result['msg']);
            }
        } catch (\Exception $e) {
            $this->logger->error('AssignProductItemsSaveAfterCommit - ' . $e->getMessage());
        }
    }
}
