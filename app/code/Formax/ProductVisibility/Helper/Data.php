<?php
/**
 * Formax Software.
 *
 * @category Formax
 * @package  Formax_ProductVisibility
 * @author   Formax
 */
namespace Formax\ProductVisibility\Helper;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemsCollection;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollection;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Magento\Catalog\Model\Product\Visibility;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Formax\ProductVisibility\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $connection;

    /**
     * @var ProductCollection
     */
    protected $productCollection;

    /**
     * @var ItemsCollection
     */
    protected $itemsCollection;

    /**
     * @var AssociatesCollection
     */
    protected $associatesCollection;

    /**
     * @var EavAttribute
     */
    protected $eavAttribute;

    /**
     * __construct
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Formax\ProductVisibility\Logger\Logger $logger
     * @param ProductCollection $productCollection
     * @param ItemsCollection $itemsCollection
     * @param AssociatesCollection $associatesCollection
     * @param EavAttribute $eavAttribute
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        \Formax\ProductVisibility\Logger\Logger $logger,
        ProductCollection $productCollection,
        ItemsCollection $itemsCollection,
        AssociatesCollection $associatesCollection,
        EavAttribute $eavAttribute
    ) {
        $this->eavAttribute = $eavAttribute;
        $this->productCollection = $productCollection;
        $this->itemsCollection = $itemsCollection;
        $this->associatesCollection = $associatesCollection;
        $this->resource = $resource;
        $this->logger = $logger;
        $this->connection = $this->resource->getConnection();

        parent::__construct($context);
    }

    /**
     * Get all catalog products
     * 
     * @param string/array $sku optional
     * @param int/string/array $sellerIdentifier optional
     * @return array
     */
    public function getAllProducts($sku = null, $sellerIdentifier = null)
    {
        $result = [];
        $collection = $this->productCollection->create();
        $collection->addAttributeToFilter('type_id', ['neq' => 'quote'])
            ->addAttributeToFilter('entity_id', [
                'nin' => new \Zend_Db_Expr('SELECT product_id FROM catalog_product_super_link')
            ]
        )->getSelect()->distinct()->join(
            ['w' => $this->resource->getTableName('catalog_product_website')],
            'e.entity_id = w.product_id',
            []
        )->join(
            ['s' => $this->resource->getTableName('store')],
            'w.website_id = s.website_id',
            ['store_dccp' => 'store_id']
        );

        if ($sellerIdentifier !== null) {
            $collection->getSelect()->joinLeft(
                ['i' => $this->resource->getTableName('marketplace_assignproduct_items')],
                'e.entity_id = i.product_id',
                []
            );
            
            if (is_array($sellerIdentifier) && count($sellerIdentifier) > 0) {
                $collection->getSelect()->where('seller_id in (?)', $sellerIdentifier);
            } else {
                if (!empty($sellerIdentifier)) {
                    $collection->getSelect()->where('seller_id = ?', $sellerIdentifier);
                }
            }
        }
        
        if ($sku !== null) {
            if (is_array($sku) && count($sku) > 0) {
                $collection->addAttributeToFilter('sku', ['in' => $sku]);
            } else {
                if (!empty($sku)) {
                    $collection->addAttributeToFilter('sku', $sku);
                }
            }
        }
        
        foreach ($collection as $entity) {
            $rowId = $entity->getRowId();
            $result[$rowId] = $rowId . '_' . $entity->getStoreDccp();
        }
        
        return $result;
    }

    /**
     * Get sellers count by all products
     * 
     * @param string/array $sku optional 
     * @param int/string/array $sellerIdentifier optional
     * @param bool $byRowId optional
     * @param bool $excludeSellerValidation optional
     * @return array
     */
    public function getCountSellerByProducts($sku = null, $sellerIdentifier = null, $byRowId = false)
    {
        $result = [];
        $select =  $this->connection->select()->distinct()
            ->from(
                ['main_table' => $this->resource->getTableName('marketplace_assignproduct_items')],
                ['type']
            )->joinLeft(
                ['a' => $this->resource->getTableName('marketplace_assignproduct_associated_products')],
                'main_table.id = a.parent_id',
                []
            )->join(
                ['e' => $this->resource->getTableName('catalog_product_entity')],
                'main_table.product_id = e.entity_id',
                ['entity_id', 'row_id']
            )->join(
                ['u' => $this->resource->getTableName('marketplace_userdata')],
                'main_table.seller_id = u.seller_id AND u.is_seller = 1',
                []
            )->join(
                ['c' => $this->resource->getTableName('customer_entity')],
                'main_table.seller_id = c.entity_id',
                []
            )->join(
                ['w' => $this->resource->getTableName('catalog_product_website')],
                'e.entity_id = w.product_id',
                []
            )->join(
                ['s' => $this->resource->getTableName('store')],
                'w.website_id = s.website_id',
                ['store' => 'store_id']
            )->where(new \Zend_Db_Expr("IF(main_table.type = 'configurable', a.status, main_table.status) = 1"))
            ->where('main_table.dis_dispersion = ?', 0)
            ->where(new \Zend_Db_Expr("IF(main_table.type = 'configurable', a.qty, main_table.qty) > 0"));

        if ($sellerIdentifier !== null) {
            if (is_array($sellerIdentifier) && count($sellerIdentifier) > 0) {
                $select->where('c.email IN (?)', $sellerIdentifier);
            } else {
                if ((int)$sellerIdentifier > 0) {
                    $select->where('c.entity_id = ?', $sellerIdentifier);
                } else {
                    if (!empty($sellerIdentifier)) {
                        $select->where('c.email = ?', $sellerIdentifier);
                    }
                }
            }
        }

        if ($sku !== null) {
            if (!$byRowId) {
                if (is_array($sku) && count($sku) > 0) {
                    $select->where('e.sku IN (?)', $sku);
                } else {
                    if (!empty($sku)) {
                        $select->where('e.sku = ?', $sku);
                    }
                }
            } else {
                if (is_array($sku) && count($sku) > 0) {
                    $select->where('e.row_id IN (?)', $sku);
                } else {
                    if (!empty($sku)) {
                        $select->where('e.row_id = ?', $sku);
                    }
                }
            }
        }
        
        $collection = $this->connection->fetchAll($select);
        if (is_array($collection)) {
            foreach ($collection as $entity) {
                $result[] = $entity['row_id'] . '_' . $entity['store'];
            }
        }
        
        return $result;
    }

    /**
     * Set visibility product depends seller assign
     * 
     * @return void
     */
    public function setVisibilityProduct($sku = null, $sellerIdentifier = null)
    {
        $result = ['error' => false, 'msg' => ''];
        $startTime = microtime(true);
        $this->connection->beginTransaction();
        $i = 0;
        $j = 0;
        $k = 0;

        try {
            $products = $this->getAllProducts($sku, $sellerIdentifier);
            if ($sellerIdentifier !== null) {
                $newProducts = array_keys($products);
                $assigProducts = $this->getCountSellerByProducts($newProducts, null, true);
            } else {
                $assigProducts = $this->getCountSellerByProducts($sku);
            }
            
            $attrVisibility = $this->eavAttribute->getIdByCode('catalog_product', 'visibility');
            foreach ($products as $p) {
                if (in_array($p, $assigProducts)) {
                    $visibility = Visibility::VISIBILITY_BOTH;
                    $j++;
                } else {
                    $visibility = Visibility::VISIBILITY_NOT_VISIBLE;
                    $k++;
                }

                $entityId = explode('_', $p);
                if (isset($entityId[0]) && isset($entityId[1])) {
                    $sql = 'INSERT INTO ' . $this->resource->getTableName('catalog_product_entity_int') .
                    ' (attribute_id, store_id, value, row_id) VALUES (' . $attrVisibility . ', ' . $entityId[1] . ', ' . $visibility . ', ' . $entityId[0] .
                    ') ON DUPLICATE KEY UPDATE value = ' . $visibility;
                    $this->connection->query($sql);
                    $i++;
                }
            }

            $this->connection->commit();
            $resultTime = microtime(true) - $startTime;
            $result = ['error' => false, 'msg' => sprintf('Process was executed successfully at: %s Enabled: %s Disabled: %s Total: %s', gmdate('H:i:s', $resultTime), $j, $k, $i)];
        } catch (\Exception $e) {
            $this->connection->rollBack();
            $this->logger->critical($e->getMessage());
            $result = ['error' => true, 'msg' => $e->getMessage()];
        }

        return $result;
    }

    /**
     * Get SKU product by ID
     * 
     * @param int $id
     * @return string
     */
    public function getSkuByProductId($id)
    {
        $sku = false;
        $collection = $this->productCollection->create()
            ->addAttributeToFilter('entity_id', $id)
            ->setPageSize(1);
        
        if ($collection) {
            foreach ($collection as $item) {
                $sku = $item->getSku();
            }
        }

        return $sku;
    }

    /**
     * Retrive if some childs are active from configurable
     * 
     * @param int $parentProductId
     * @return bool
     */
    public function isChildsActiveFromConfigurable($parentProductId)
    {
        $active = false;
        $collection = $this->associatesCollection->create()
            ->addFieldToFilter('parent_product_id', $parentProductId)
            ->addFieldToFilter('qty', ['gt' => 0])
            ->addFieldToFilter('dis_dispersion', 0)
            ->setPageSize(1);

        if ($collection) {
            foreach ($collection as $item) {
                $active = true;
            }
        }

        return $active;
    }
}