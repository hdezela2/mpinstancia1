<?php

namespace Formax\ProductVisibility\Cron;

use Formax\ProductVisibility\Helper\Data as Helper;

class ProductCatalogVisibility
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param Helper $helper
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        Helper $helper
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
    }

    public function execute()
    {
        $result = $this->helper->setVisibilityProduct();
        $msg = isset($result['msg']) ? $result['msg'] : '';

        if (!$result['error']) {
            $this->logger->info('Cron Task Success Product Visibility In Catalog => ' . $msg);
        } else {
            $this->logger->critical('Cron Task Error Product Visibility In Catalog => ' . $msg);
        }
    }
}
