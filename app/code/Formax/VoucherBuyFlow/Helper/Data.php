<?php

namespace Formax\VoucherBuyFlow\Helper;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\App\ObjectManager;

/**
 * Data helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var int
     */
    const BONIFICATION = 1;

    /**
     * @var int
     */
    const DISCOUNT = 2;

    /**
     * @var string
     */
    const VOUCHER_STOREVIEW_CODE = 'voucher';

    /**
     * @var string
     */
    const SOFTWARE_STOREVIEW_CODE = 'software';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
    * @var \Magento\Framework\Registry
    */
    protected $registry;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * Initialize dependencies.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\Registry $registry
     * @param Json $serializer
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        Json $serializer
    ) {
        $this->storeManager = $storeManager;
        $this->registry = $registry;
        $this->productRepository = $productRepository;
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);

        parent::__construct($context);
    }

    /**
     * Return voucher product types
     *
     * @return array
     */
    public function getVoucherProductTypes()
    {
        return [
            self::BONIFICATION => __('Bonification'),
            self::DISCOUNT => __('Discount')
        ];
    }

    /**
     * Get Store code
     *
     * @return string
     */
    public function getCurrentStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }

    /**
     * Get current product item
     *
     * @param int $id
     * @return \Magento\Catalog\Model\ProductRepository
     */
    public function getCurrentProduct($id)
    {
        $key = 'voucher_product_' . $id;
        $product = $this->registry->registry($key);

        if ($product === null) {
            $product = $this->productRepository->getById($id);
            $this->registry->register($key, $product);
        }

        return $product;
    }

    /**
     * Get custom amount typed from item quote
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return int
     */
    public function getAmount($item)
    {
        $product = $item->getProduct();
        $data = $product->getTypeInstance(true)->getOrderOptions($product);
        $amount = isset($data['info_buyRequest']['voucher_amount'])
            ? (int)$data['info_buyRequest']['voucher_amount'] : 0;

        return $amount;
    }
}
