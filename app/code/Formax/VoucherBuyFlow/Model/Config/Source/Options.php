<?php

namespace Formax\VoucherBuyFlow\Model\Config\Source;

class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var \Formax\VoucherBuyFlow\Helper\Data
     */
    protected $helper;

    /**
     * @param \Formax\VoucherBuyFlow\Helper\Data $helper
     */
    public function __construct(
        \Formax\VoucherBuyFlow\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
    * Get all options
    *
    * @return array
    */
    public function getAllOptions()
    {
        foreach ($this->helper->getVoucherProductTypes() as $key => $value) {
            $this->_options[] = ['label' => $value, 'value' => $key];
        }

        return $this->_options;
    }
}