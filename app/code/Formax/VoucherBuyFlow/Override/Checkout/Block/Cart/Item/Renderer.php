<?php

namespace Formax\VoucherBuyFlow\Override\Checkout\Block\Cart\Item;

use Linets\VoucherSetup\Model\Constants as Voucher2021Constants;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Message\InterpretationStrategyInterface;
use Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface;
use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;

class Renderer extends \Magento\Checkout\Block\Cart\Item\Renderer
{
    /**
     * @var VoucherHelper
     */
    protected $voucherHelper;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $assignProductHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Helper\Product\Configuration $productConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param InterpretationStrategyInterface $messageInterpretationStrategy
     * @param \Webkul\MpAssignProduct\Helper\Data $assignProductHelper
     * @param VoucherHelper $voucherHelper
     * @param array $data
     * @param ItemResolverInterface|null $itemResolver
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Product\Configuration $productConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Module\Manager $moduleManager,
        InterpretationStrategyInterface $messageInterpretationStrategy,
        \Webkul\MpAssignProduct\Helper\Data $assignProductHelper,
        VoucherHelper $voucherHelper,
        array $data = [],
        ItemResolverInterface $itemResolver = null
    ) {
        parent::__construct(
            $context,
            $productConfig,
            $checkoutSession,
            $imageBuilder,
            $urlHelper,
            $messageManager,
            $priceCurrency,
            $moduleManager,
            $messageInterpretationStrategy,
            $data,
            $itemResolver
        );

        $this->assignProductHelper = $assignProductHelper;
        $this->voucherHelper = $voucherHelper;
    }

    /**
     * Get row total
     *
     * @param object $item
     * @return false|string
     */
    public function getRowTotal($item)
    {
        $price = false;
        if (in_array($this->voucherHelper->getCurrentStoreCode(),[
            VoucherHelper::VOUCHER_STOREVIEW_CODE,
            Voucher2021Constants::STORE_VIEW_CODE
        ])) {
            $amount = 1;
            $productId = $item->getProduct()->getId();
            $product = $this->voucherHelper->getCurrentProduct($productId);
            if ($product->getTypeId() == 'virtual') {
                $options = $item->getBuyRequest()->getData();
                $price = 0;
                if (array_key_exists('mpassignproduct_id', $options)) {
                    $mpAssignId = $options['mpassignproduct_id'];
                    $price = $this->assignProductHelper->getAssignProductPrice($mpAssignId);
                    $amount = $this->voucherHelper->getAmount($item);
                }

                if ((int)$product->getData('voucher_product_type') === VoucherHelper::BONIFICATION) {
                    $price = $item->getQty()*$amount;
                    $price = $this->priceCurrency->format($price);
                } elseif ((int)$product->getData('voucher_product_type') === VoucherHelper::DISCOUNT) {
                    $price = ($amount-(($amount*$price)/100))*$item->getQty();
                    $price = $this->priceCurrency->format($price);
                }

            }
        }

        return $price;
    }

    /**
     * Get unit price total
     *
     * @param object $item
     * @return false|string
     */
    public function getUnitPrice($item)
    {
        $price = false;
        $productId = $item->getProduct()->getId();
        $product = $this->voucherHelper->getCurrentProduct($productId);

        if (in_array($this->voucherHelper->getCurrentStoreCode(),[VoucherHelper::VOUCHER_STOREVIEW_CODE, Voucher2021Constants::STORE_VIEW_CODE])
            && $product->getTypeId() == 'virtual' &&
                ((int)$product->getData('voucher_product_type') === VoucherHelper::BONIFICATION
                    || (int)$product->getData('voucher_product_type') === VoucherHelper::DISCOUNT)) {
                    $options = $item->getBuyRequest()->getData();
                    $price = 0;
                    if (array_key_exists('mpassignproduct_id', $options)) {
                        $mpAssignId = $options['mpassignproduct_id'];
                        $price = $this->assignProductHelper->getAssignProductPrice($mpAssignId);
                    }
                    $price = number_format($price, 1, ',', '.') . '%';
        }

        return $price;
    }

    /**
     * Get earned profit
     *
     * @param object $item
     * @return false|string
     */
    public function getProfit($item)
    {
        $amount = 1;
        $profit = false;
        $productId = $item->getProduct()->getId();
        $product = $this->voucherHelper->getCurrentProduct($productId);

        if (in_array($this->voucherHelper->getCurrentStoreCode(),[VoucherHelper::VOUCHER_STOREVIEW_CODE, Voucher2021Constants::STORE_VIEW_CODE])
            && $product->getTypeId() == 'virtual') {
            $options = $item->getBuyRequest()->getData();
            $price = 0;
            if (array_key_exists('mpassignproduct_id', $options)) {
                $mpAssignId = $options['mpassignproduct_id'];
                $price = $this->assignProductHelper->getAssignProductPrice($mpAssignId);
                $amount = $this->voucherHelper->getAmount($item);
            }

            if ((int)$product->getData('voucher_product_type') === VoucherHelper::BONIFICATION) {
                $price = ($amount+(($price*$amount)/100))*$item->getQty();
                $profit = __('Pay %1 and earn %2', $this->priceCurrency->format($amount*$item->getQty()), $this->priceCurrency->format($price));
            } elseif ((int)$product->getData('voucher_product_type') === VoucherHelper::DISCOUNT) {
                $price = ($amount-(($price*$amount)/100))*$item->getQty();
                $profit = __('Pay %1 and earn %2', $this->priceCurrency->format($price), $this->priceCurrency->format($amount*$item->getQty()));
            }
        }

        return $profit;
    }

    /**
     * Get amount
     *
     * @param object $item
     * @return float
     */
    public function getAmount($item)
    {
        return $this->voucherHelper->getAmount($item);
    }

    /**
     * Get Voucher product type
     *
     * @param object $product
     * @return int
     */
    public function getVoucherProductType($product)
    {
        $productId = $product->getId();
        $product = $this->voucherHelper->getCurrentProduct($productId);
        return (int)$product->getData('voucher_product_type');
    }
}
