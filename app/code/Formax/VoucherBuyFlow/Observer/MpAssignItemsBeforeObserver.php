<?php

namespace Formax\VoucherBuyFlow\Observer;

use Linets\VoucherSetup\Model\Constants as Voucher2021Constants;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;
use Psr\Log\LoggerInterface;

class MpAssignItemsBeforeObserver implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var VoucherHelper
     */
    protected $voucherHelper;

    /**
     * @param VoucherHelper $voucherHelper
     * @param LoggerInterface $logger
     */
    public function __construct(
        VoucherHelper $voucherHelper,
        LoggerInterface $logger
    ) {
        $this->voucherHelper = $voucherHelper;
        $this->logger = $logger;
    }

    /**
     * add notification status change
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        if (in_array($this->voucherHelper->getCurrentStoreCode(),[
            VoucherHelper::VOUCHER_STOREVIEW_CODE,
            Voucher2021Constants::STORE_VIEW_CODE
        ])) {
            $model = $observer->getEvent()->getObject();
            try {
                $price = str_replace(',', '.', $model->getData('price'));
                $model->setData('price', $price);
            } catch (\Exception $e) {
                $this->logger->error('Notification AssignProductItemsSaveAfter - ' . $e->getMessage());
            }
        }
    }
}
