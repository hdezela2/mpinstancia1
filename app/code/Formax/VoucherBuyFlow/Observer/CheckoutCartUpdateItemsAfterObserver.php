<?php

namespace Formax\VoucherBuyFlow\Observer;

use Linets\VoucherSetup\Model\Constants as Voucher2021Constants;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;
use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;

class CheckoutCartUpdateItemsAfterObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
    * @var Json
    */
    private $_serializer;

    /**
     * @var VoucherHelper
     */
    private $_voucherHelper;

    /**
     * @param \Magento\Framework\View\LayoutInterface $layout
     * @param VoucherHelper $voucherHelper
     * @param Json $serializer
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        VoucherHelper $voucherHelper,
        json $serializer = null
    )
    {
        $this->_voucherHelper = $voucherHelper;
        $this->_request = $request;
        $this->_serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
    }

    /**
     * Add order information into GA block to render on checkout success pages
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        if (in_array($this->_voucherHelper->getCurrentStoreCode(),[
            VoucherHelper::VOUCHER_STOREVIEW_CODE,
            Voucher2021Constants::STORE_VIEW_CODE
        ])) {
            $quoteItems = $observer->getCart()->getQuote()->getAllVisibleItems();
            $post = $this->_request->getPostValue();
            $additionalOptions = [];

            if (isset($post['cart']) && is_array($post['cart'])) {
                foreach ($post['cart'] as $keyItemToUpdate => $itemToUpdate) {
                    foreach ($quoteItems as $item) {
                        if ((int)$item->getId() === (int)$keyItemToUpdate) {
                            $product = $item->getProduct();
                            if ($product->getTypeId() == 'virtual') {
                                $price = isset($itemToUpdate['amount']) ? (int)$itemToUpdate['amount'] : 0;
                                if ($price > 0) {
                                    if ($additionalOption = $item->getOptionByCode('info_buyRequest')) {
                                        $additionalOptions = (array) $this->_serializer->unserialize($additionalOption->getValue());
                                        $newOptions = [];
                
                                        foreach ($additionalOptions as $key => $option) {
                                            $newOptions[$key] = $key == 'voucher_amount' ? $price : $option;
                                        }
                                        
                                        $item->addOption([
                                            'code' => 'info_buyRequest',
                                            'value' => $this->_serializer->serialize($newOptions)
                                        ]);
                                        
                                    }
                                } else {
                                    $error = __('You must enter an amount.');
                                    throw new \Magento\Framework\Exception\LocalizedException($error);
                                }
                            }
                        }
                    }
                }
            }
            
            
            /*foreach ($quoteItems as $item) {
                $product = $item->getProduct();
                if ($product->getTypeId() == 'virtual') {
                    if (isset($post['cart'][$item->getId()]) && $item->getId() == $post['cart'][$item->getId()] )
                    if ($additionalOption = $item->getOptionByCode('info_buyRequest')) {
                        $additionalOptions = (array) $this->_serializer->unserialize($additionalOption->getValue());
                        $newOptions = [];
                        $price = isset($post['cart'][$item->getId()]['amount']) ? (int)$post['cart'][$item->getId()]['amount'] : 0;

                        foreach ($additionalOptions as $key => $option) {
                            $newOptions[$key] = $key == 'voucher_amount' ? $price : $option;
                        }
                    }
                    //echo '<pre>';print_r($newOptions); echo '</pre>';
                    
                    if (isset($newOptions['voucher_amount']) && (int)$newOptions['voucher_amount'] > 0) {
                        $item->addOption([
                            'code' => 'info_buyRequest',
                            'value' => $this->_serializer->serialize($newOptions)
                        ]);
                    } else {
                        $error = __('You must enter an amount.');
                        throw new \Magento\Framework\Exception\LocalizedException($error);
                    }
                }
            }*/
        }
    }
}