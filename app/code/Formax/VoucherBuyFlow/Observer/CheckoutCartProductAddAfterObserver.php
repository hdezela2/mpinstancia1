<?php

namespace Formax\VoucherBuyFlow\Observer;

use Linets\VoucherSetup\Model\Constants as Voucher2021Constants;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;
use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;

class CheckoutCartProductAddAfterObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
    * @var Json
    */
    private $_serializer;

    /**
     * @var VoucherHelper
     */
    private $_voucherHelper;

    /**
     * @param \Magento\Framework\View\LayoutInterface $layout
     * @param VoucherHelper $voucherHelper
     * @param Json $serializer
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        VoucherHelper $voucherHelper,
        json $serializer = null
    )
    {
        $this->_voucherHelper = $voucherHelper;
        $this->_request = $request;
        $this->_serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
    }

    /**
     * Add order information into GA block to render on checkout success pages
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        if (in_array($this->_voucherHelper->getCurrentStoreCode(),[
            VoucherHelper::VOUCHER_STOREVIEW_CODE,
            Voucher2021Constants::STORE_VIEW_CODE
        ])) {
            /* @var \Magento\Quote\Model\Quote\Item $item */
            $item = $observer->getQuoteItem();
            $product = $item->getProduct();
            $price = 0;

            if ($product->getTypeId() == 'virtual') {
                $data = $product->getTypeInstance(true)->getOrderOptions($product);
                $options = isset($data['info_buyRequest']) ? $data : ['info_buyRequest' => []];
                
                $price = (int)$this->_request->getParam('voucher_amount');
                if ($price > 0 && is_array($options) && count($options) > 0) {
                    array_merge($options['info_buyRequest'], ['voucher_amount' => $price]);
                    $item->setProductOptions($options);
                }
                
                if ((int)$options['info_buyRequest']['voucher_amount'] == 0) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('You must enter an amount.'));
                }
            }
        }
    }
}