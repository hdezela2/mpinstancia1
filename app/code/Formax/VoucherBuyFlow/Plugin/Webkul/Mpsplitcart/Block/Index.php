<?php

namespace Formax\VoucherBuyFlow\Plugin\Webkul\Mpsplitcart\Block;

use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;
use Linets\VoucherSetup\Model\Constants as Voucher2021Constants;

/**
 * Mpsplitcart Block
 */
class Index
{
    /**
     * @var \Webkul\Mpsplitcart\Helper\Data
     */
    private $helper;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cartModel;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    private $priceHelper;

    /**
     * @var \Formax\VoucherBuyFlow\Helper\Data
     */
    private $voucherHelper;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    private $taxCalculation;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    private $assignProductHelper;

    /**
     * @param \Webkul\Mpsplitcart\Helper\Data $helper
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     * @param \Formax\VoucherBuyFlow\Helper\Data $voucherHelper
     * @param \Magento\Tax\Api\TaxCalculationInterface
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Webkul\MpAssignProduct\Helper\Data $assignProductHelper
     */
    public function __construct(
        \Webkul\Mpsplitcart\Helper\Data $helper,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Formax\VoucherBuyFlow\Helper\Data $voucherHelper,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculation,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webkul\MpAssignProduct\Helper\Data $assignProductHelper
    ) {
        $this->assignProductHelper = $assignProductHelper;
        $this->helper = $helper;
        $this->cartModel = $cart;
        $this->priceHelper = $priceHelper;
        $this->voucherHelper = $voucherHelper;
        $this->taxCalculation = $taxCalculation;
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManager;
    }

    /**
     * getSellerData get seller array in order to
     * show items at shopping cart accr. to sellers
     *
     * @param \Webkul\Mpsplitcart\Block\Index $subject
     * @param array $cartArray
     * @return array
     */
    public function afterGetSellerData(\Webkul\Mpsplitcart\Block\Index $subject, $cartArray)
    {
        if (in_array($this->voucherHelper->getCurrentStoreCode(),[
            VoucherHelper::VOUCHER_STOREVIEW_CODE,
            Voucher2021Constants::STORE_VIEW_CODE
        ])) {
            $cartArray = [];
            $customerId = $this->customerSession->getCustomer()->getId();
            $storeId = $this->storeManager->getStore()->getId();

            try {
                $cart = $this->cartModel->getQuote();
                foreach ($cart->getAllItems() as $item) {
                    if (!$item->hasParentItemId()) {
                        $options = $item->getBuyRequest()->getData();

                        if (array_key_exists("mpassignproduct_id", $options)) {
                            $mpAssignId = $options["mpassignproduct_id"];
                            $sellerId = $this->helper->getSellerIdFromMpassign(
                                $mpAssignId
                            );
                        } else {
                            $sellerId = $this->helper->getSellerId($item->getProductId());
                        }

                        $productId = $item->getProduct()->getId();
                        $product = $this->voucherHelper->getCurrentProduct($productId);
                        if (isset($mpAssignId)) {
                            $price = $this->assignProductHelper->getAssignProductPrice($mpAssignId);
                        } else {
                            $price = 0;
                        }
                        $amount = $this->voucherHelper->getAmount($item);

                        $taxAmount = $item->getTaxAmount();
                        if ($this->helper->getCatalogPriceIncludingTax()) {
                            $price = $item->getRowTotalInclTax();
                            $taxAmount = 0;
                        }

                        if ($product->getTypeId() == 'virtual') {
                            $rate = $this->taxCalculation->getCalculatedRate(
                                $item->getProduct()->getTaxClassId(),
                                $customerId,
                                $storeId
                            );
                            if ((int)$product->getData('voucher_product_type') === VoucherHelper::BONIFICATION) {
                                $price = $amount*$item->getQty();
                                $taxAmount = ($rate*$price)/100;
                            } elseif ((int)$product->getData('voucher_product_type') === VoucherHelper::DISCOUNT) {
                                $price = ($amount-(($price*$amount)/100))*$item->getQty();
                                $taxAmount = ($rate*$price)/100;
                            }
                        }

                        $formattedPrice = $this->priceHelper->currency(
                            $price,
                            true,
                            false
                        );
                        $cartArray[$sellerId][$item->getId()] = $formattedPrice;

                        if (!isset($cartArray[$sellerId]['total'])
                            || $cartArray[$sellerId]['total'] == null
                        ) {
                            $cartArray[$sellerId]['total'] = $price;
                        } else {
                            $cartArray[$sellerId]['total'] += $price;
                        }
                        if (!isset($cartArray[$sellerId]['tax'])
                            || $cartArray[$sellerId]['tax'] == null
                        ) {
                            $cartArray[$sellerId]['tax'] = $taxAmount;
                        } else {
                            $cartArray[$sellerId]['tax'] += $taxAmount;
                        }
                        $formattedPrice = $this->priceHelper->currency(
                            $cartArray[$sellerId]['total'],
                            true,
                            false
                        );
                        $formattedTaxPrice = $this->priceHelper->currency(
                            $cartArray[$sellerId]['tax'],
                            true,
                            false
                        );
                        $formattedOrderTotal = $this->priceHelper->currency(
                            $cartArray[$sellerId]['tax'] + $cartArray[$sellerId]['total'],
                            true,
                            false
                        );
                        $cartArray[$sellerId]['formatted_total'] = $formattedPrice;
                        $cartArray[$sellerId]['formatted_tax'] = $formattedTaxPrice;
                        $cartArray[$sellerId]['formatted_order_total'] = $formattedOrderTotal;
                    }
                }
            } catch (\Exception $e) {
                $this->helper->logDataInLogger(
                    "Block_Index_getSellerData Exception : " . $e->getMessage()
                );
            }
        }

        return $cartArray;
    }
}
