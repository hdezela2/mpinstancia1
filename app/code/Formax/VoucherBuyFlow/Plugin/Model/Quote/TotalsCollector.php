<?php

namespace Formax\VoucherBuyFlow\Plugin\Model\Quote;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Linets\VoucherSetup\Model\Constants as Voucher2021Constants;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote\TotalsCollector as Collector;
use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;
use Webkul\MpAssignProduct\Helper\Data as AssignProductHelper;
use Formax\Coupon\Helper\Data as helperCoupon;
use Magento\Framework\Registry;

class TotalsCollector
{
    /**
     * @var VoucherHelper
     */
    protected $voucherHelper;

    /**
     * @var AssignProductHelper
     */
    protected $assignProductHelper;

    private $_helperCoupon;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @param VoucherHelper $voucherHelper
     * @param AssignProductHelper $assignProductHelper
     * @param helperCoupon $helperCoupon
     * @param Registry $registry
     */
    public function __construct(
        VoucherHelper $voucherHelper,
        AssignProductHelper $assignProductHelper,
        helperCoupon $helperCoupon,
        Registry $registry,
        ProductRepository $productRepository
    ) {
        $this->voucherHelper = $voucherHelper;
        $this->assignProductHelper = $assignProductHelper;
        $this->_helperCoupon = $helperCoupon;
        $this->registry = $registry;
        $this->productRepository = $productRepository;
    }

    public function aroundCollect(
        Collector $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote $quote
    ) {
        $t = $this->voucherHelper->getCurrentStoreCode();
        if (in_array($this->voucherHelper->getCurrentStoreCode(),[
            VoucherHelper::VOUCHER_STOREVIEW_CODE,
            Voucher2021Constants::STORE_VIEW_CODE
        ])) {
            foreach ($quote->getAllItems() as $item) {
                $productId = $item->getProduct()->getId();
                $product = $this->voucherHelper->getCurrentProduct($productId);
                $amount = 0;
                if ($product->getTypeId() == 'virtual') {
                    $options = $item->getBuyRequest()->getData();
                    $amount = $this->voucherHelper->getAmount($item);
                    $priceBySeller = 0;
                    if (array_key_exists('mpassignproduct_id', $options)) {
                        $mpAssignId = $options['mpassignproduct_id'];
                        $priceBySeller = $this->assignProductHelper->getAssignProductPrice($mpAssignId);
                        $amount = $this->voucherHelper->getAmount($item);
                    }

                    if ($amount > 0 && $priceBySeller > 0) {
                        if ((int)$product->getData('voucher_product_type') === VoucherHelper::BONIFICATION) {
                            $item->setCustomPrice($amount);
                            $item->setOriginalCustomPrice($amount);
                            $item->setRowTotal($amount*$item->getQty());
                            $item->getProduct()->setIsSuperMode(true);
                        } elseif ((int)$product->getData('voucher_product_type') === VoucherHelper::DISCOUNT) {
                            $price = $amount-(($amount*$priceBySeller)/100);
                            $item->setCustomPrice($price);
                            $item->setOriginalCustomPrice($price);
                            $item->setRowTotal($amount*$item->getQty());
                            $item->getProduct()->setIsSuperMode(true);
                        }
                    }
                }
            }
        }

        if (VoucherHelper::SOFTWARE_STOREVIEW_CODE == $this->voucherHelper->getCurrentStoreCode() || SoftwareRenewalConstants::STORE_CODE == $this->voucherHelper->getCurrentStoreCode()) {
            $this->_helperCoupon->createAndApplyCoupon();
        }

        $result = $proceed($quote);

        return $result;
    }

    /**
     * Get current product item
     *
     * @param $id
     * @return ProductInterface|mixed|null
     * @throws NoSuchEntityException
     */
    public function getCurrentProduct($id)
    {
        $key = 'voucher_product_' . $id;
        $product = $this->registry->registry($key);

        if ($product === null) {
            $product = $this->productRepository->getById($id);
            $this->registry->register($key, $product);
        }

        return $product;
    }
}
