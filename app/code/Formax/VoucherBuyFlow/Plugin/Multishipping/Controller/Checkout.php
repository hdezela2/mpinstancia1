<?php

namespace Formax\VoucherBuyFlow\Plugin\Multishipping\Controller;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\StateException;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Multishipping\Controller\Checkout\Success;

/**
 * Multishipping checkout controller
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Checkout
{
    const FLAG_NO_DISPATCH = 'no-dispatch';

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    protected $_actionFlag;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $_redirect;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var UrlInterface
     */
    private $_url;

    /**
     * @var Success
     */
    protected $checkoutSuccess;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param RequestInterface $request
     * @param \Magento\Framework\App\ResponseInterface $response
     * @param \Magento\Framework\App\Response\Http $redirect
     * @param \Magento\Customer\Model\Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $accountManagement
     * @param UrlInterface $url
     * @param \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
     * @param Success $checkoutSuccess
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Framework\App\Response\Http $redirect,
        \Magento\Customer\Model\Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $accountManagement,
        UrlInterface $url,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        Success $checkoutSuccess
    ) {
        $this->_url = $url;
        $this->_actionFlag = $context->getActionFlag();
        $this->messageManager = $context->getMessageManager();
        $this->_objectManager = $context->getObjectManager();
        $this->_redirect = $redirect;
        $this->_request = $request;
        $this->_response = $response;
        $this->_customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->accountManagement = $accountManagement;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->checkoutSuccess = $checkoutSuccess;
    }

    /**
     * Dispatch request
     *
     * @param \Magento\Multishipping\Controller\Checkout $subject
     * @param \Closure $proceed
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function aroundDispatch(
        \Magento\Multishipping\Controller\Checkout $subject,
        \Closure $proceed,
        RequestInterface $request
    ) {
        $this->_request = $request;
        if ($this->_actionFlag->get('', 'redirectLogin')) {
            return $this->checkoutSuccess->dispatch($request);
        }

        $action = $this->_request->getActionName();

        $checkoutSessionQuote = $this->_getCheckoutSession()->getQuote();
        /**
         * Catch index action call to set some flags before checkout/type_multishipping model initialization
         */
        if ($action == 'index') {
            $checkoutSessionQuote->setIsMultiShipping(true);
            $this->_getCheckoutSession()->setCheckoutState(\Magento\Checkout\Model\Session::CHECKOUT_STATE_BEGIN);
        } elseif (!$checkoutSessionQuote->getIsMultiShipping() && !in_array(
            $action,
            ['login', 'register', 'success']
        )
        ) {
            $customRedirectionUrl = $this->_redirect->setRedirect('*/*/index');
            $this->_url->getUrl($customRedirectionUrl);
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
            return $this->checkoutSuccess->dispatch($request);
        }

        if (!in_array($action, ['login', 'register'])) {
            $customerSession = $this->_objectManager->get(\Magento\Customer\Model\Session::class);
            if (!$customerSession->authenticate($this->_getHelper()->getMSLoginUrl())) {
                $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
            }

            if (!$this->_objectManager->get(
                \Magento\Multishipping\Helper\Data::class
            )->isMultishippingCheckoutAvailable()) {
                $error = $this->_getCheckout()->getMinimumAmountError();
                $this->messageManager->addError($error);
                $this->getResponse()->setRedirect($this->_getHelper()->getCartUrl());
                $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
                return $this->checkoutSuccess->dispatch($request);
            }
        }

        $result = $this->_preDispatchValidateCustomer();
        if ($result instanceof \Magento\Framework\Controller\ResultInterface) {
            return $result;
        }

        if (!$result) {
            return $this->getResponse();
        }

        if ($this->_getCheckoutSession()->getCartWasUpdated(true) &&
            !in_array($action, ['index', 'login', 'register', 'addresses', 'success', 'addressesPost', 'shipping', 'billing', 'overview', 'shippingPost', 'billingPost', 'overviewPost'])) {
            $this->getResponse()->setRedirect($this->_getHelper()->getCartUrl());
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
            return $this->checkoutSuccess->dispatch($request);
        }

        if ($action == 'success' && $this->_getCheckout()->getCheckoutSession()->getDisplaySuccess(true)) {
            return $this->checkoutSuccess->dispatch($request);
        }

        try {
            $checkout = $this->_getCheckout();
        } catch (StateException $e) {
            $this->getResponse()->setRedirect($this->_getHelper()->getMSNewShippingUrl());
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
            return $this->checkoutSuccess->dispatch($request);
        }

        $quote = $checkout->getQuote();
        if (!$quote->hasItems() || $quote->getHasError() || $quote->isVirtual()) {
            $this->getResponse()->setRedirect($this->_getHelper()->getCartUrl());
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }

        return $proceed($request);
    }

    /**
     * Retrieve checkout model
     *
     * @return \Magento\Multishipping\Model\Checkout\Type\Multishipping
     */
    protected function _getCheckout()
    {
        return $this->_objectManager->get(\Magento\Multishipping\Model\Checkout\Type\Multishipping::class);
    }

    /**
     * Retrieve checkout url helper
     *
     * @return \Magento\Multishipping\Helper\Url
     */
    protected function _getHelper()
    {
        return $this->_objectManager->get(\Magento\Multishipping\Helper\Url::class);
    }

    /**
     * Retrieve checkout session
     *
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckoutSession()
    {
        return $this->_objectManager->get(\Magento\Checkout\Model\Session::class);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function getResponse()
    {
        return $this->_response;
    }

    /**
     * Make sure customer is valid, if logged in
     *
     * By default will add error messages and redirect to customer edit form
     *
     * @param bool $redirect - stop dispatch and redirect?
     * @param bool $addErrors - add error messages?
     * @return bool|\Magento\Framework\Controller\Result\Redirect
     */
    protected function _preDispatchValidateCustomer($redirect = true, $addErrors = true)
    {
        try {
            $customer = $this->customerRepository->getById($this->_customerSession->getCustomerId());
        } catch (NoSuchEntityException $e) {
            return true;
        }

        $validationResult = $this->accountManagement->validate($customer);
        if (!$validationResult->isValid()) {
            if ($addErrors) {
                foreach ($validationResult->getMessages() as $error) {
                    $this->messageManager->addErrorMessage($error);
                }
            }
            if ($redirect) {
                $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
                return $this->resultRedirectFactory->create()->setPath('customer/account/edit');
            }
            return false;
        }

        return true;
    }
}
