<?php
namespace Formax\CategoryProductPrice\Helper;

use Cleverit\CategoryProductPrice\Model\ProductPriceHistory;
use Cleverit\CategoryProductPrice\Model\ProductPriceHistoryFactory;
use Cleverit\CategoryProductPrice\Model\Repository\ProductPriceHistoryRepository;
use Magento\Catalog\Model\CategoryFactory as CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Api\Filter;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\MpAssignProduct\Model\AssociatesFactory as AssociatesFactory;
use Webkul\MpAssignProduct\Model\ItemsFactory as AssignItemsFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollectionFactory;

class Data extends \Webkul\CategoryProductPrice\Helper\Data
{

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     *
     */
    protected $coreSession;

    /** @var ProductPriceHistoryFactory */
    protected $_productPriceHistoryFactory;

    /** @var ProductPriceHistoryRepository */
    protected $_productPriceHistoryRepository;

    /** @var SearchCriteria */
    protected $_searchCriteria;

    /** @var FilterGroup */
    protected $_filterGroup;

    /** @var Filter */
    protected $_filter;

    /** @var AssociatesCollectionFactory */
    protected $_associatesCollection;

    /** @var DateTime */
    protected $date;

    /** @var RequestInterface */
    protected $request;

    public function __construct(
        Context $context,
        ProductCollectionFactory $productCollectionFactory,
        CategoryCollectionFactory $categoryCollectionFactory,
        CategoryFactory $categoryFactory,
        AssignItemsFactory $assignItems,
        AssociatesFactory $assignAssociates,
        SessionManagerInterface $coreSession,
        StoreManagerInterface $storeManager,
        ResourceConnection $resourceConnection,
        ProductPriceHistoryFactory $productPriceHistoryFactory,
        ProductPriceHistoryRepository $productPriceHistoryRepository,
        SearchCriteria $searchCriteria,
        FilterGroup $filterGroup,
        Filter $filter,
        DateTime $date
    )
    {
        $this->request = $context->getRequest();
        $this->storeManager = $storeManager;
        $this->coreSession = $coreSession;
        $this->assignAssociates = $assignAssociates;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->categoryFactory = $categoryFactory;
        $this->assignItems = $assignItems;
        $this->resourceConnection = $resourceConnection;
        $this->_productPriceHistoryFactory = $productPriceHistoryFactory;
        $this->_productPriceHistoryRepository = $productPriceHistoryRepository;
        $this->_searchCriteria = $searchCriteria;
        $this->_filterGroup = $filterGroup;
        $this->_filter = $filter;
        $this->date = $date;
        parent::__construct(
            $context,
            $productCollectionFactory,
            $categoryCollectionFactory,
            $categoryFactory,
            $assignItems,
            $assignAssociates,
            $coreSession,
            $storeManager
        );
    }


    public function getPostData()
    {
        $productPriceHistoryId = $this->request->getParam('product_price_history_id');

        if ($productPriceHistoryId) {
            try {
                $productPriceHistory = $this->_productPriceHistoryRepository->getById($productPriceHistoryId);
                $categories = $productPriceHistory->getCategories();
                $percentage = $productPriceHistory->getPricePercentage();
                $allCategories = $this->getAllCategories($categories);

                $response['pricePercentage'] = $percentage;
                $response['allCategories'] = $allCategories;
                $response['assignProductIds'] = $productPriceHistory->getAssignProductIds();
                $response['productPriceHistoryId'] = $productPriceHistoryId;
                $response['newIndex'] = $productPriceHistory->getProductCountActual();
            } catch (NoSuchEntityException $e) {
                $response['error'] = $e->getMessage();
            }
            return $response;
        } else {

            $categories = $this->request->getParam('categories');
            $percentage = $this->request->getParam('update_percentage');
            $allCategories = $this->getAllCategories($categories);

            $response['pricePercentage'] = $percentage;
            $response['allCategories'] = implode(',', $allCategories);
            $response['newIndex'] = 0;
            // Search by Category and websiteId
            $collection = $this->productCollectionFactory->create()
                ->addFieldToSelect('*')
                ->addCategoriesFilter(['in' => $allCategories])
                ->load();
            //Get the list of SKUs in the collection
            $skuList = $collection->getFirstItem()->getSku();
            foreach ($collection as $product) {
                if (!empty($product->getLinkedProducts())) {
                    $skuList = $skuList . ',' . $product->getLinkedProducts();
                } else {
                    $skuList = $skuList . ',' . $product->getSku();
                }
            }
            $collectionWithLinkeds = $this->productCollectionFactory->create()
                ->addFieldToSelect('id')
                ->addFieldToFilter('sku', ['in' => $skuList])
                ->load();

            $allProductIds = $collectionWithLinkeds->getAllIds();
            $assignProducts = $this->assignItems->create()
                ->getCollection()
                ->addFieldToFilter('product_id', ['in' => [$allProductIds]]);
            $response['assignProductIds'] = [];
            $i = 0;
            foreach ($assignProducts as $assignProductItem) {
//                if ($assignProductItem->getStatus() == 1 || ($assignProductItem->getStatus() == 0 && $this->getDispersion($assignProductItem->getProductId(), $assignProductItem->getId()) == 1)) {
                    if ($assignProductItem->getType() == 'configurable') {
                        $associateAssignProducts = $this->assignAssociates->create()->getCollection()
                            ->addFieldToFilter('parent_product_id', $assignProductItem->getProductId())
                            ->addFieldToFilter('parent_id', $assignProductItem->getId());
                        foreach ($associateAssignProducts as $associateAssignProductItem) {
                            if (!in_array($associateAssignProductItem->getStatus(), [0, 2]) ) {
                                $response['assignProductIds'][$i]['id'] = $associateAssignProductItem->getId();
                                $response['assignProductIds'][$i]['child'] = true;
                                $i++;
                            }
                        }
                    } else {
                        if (!in_array($assignProductItem->getStatus(), [0,2])) {
                            $response['assignProductIds'][$i]['id'] = $assignProductItem->getId();
                            $response['assignProductIds'][$i]['child'] = false;
                            $i++;
                        }
                    }
//                }
            }

            $response = $this->createProductPriceHistory($categories, $response);
        }

        return $response;
    }

    public function createProductPriceHistory($categories, $response)
    {
        $productCount = count($response['assignProductIds']);

        $productPriceHistory = $this->_productPriceHistoryFactory->create();
        $productPriceHistory->setCategories($categories);
        $allCategories = explode(',', $response['allCategories']);
        $productPriceHistory->setAllCategories($allCategories);
        $productPriceHistory->setPricePercentage($response['pricePercentage']);
        $productPriceHistory->setFirstAssignProductIds($response['assignProductIds']);
        $productPriceHistory->setAssignProductIds($response['assignProductIds']);
        $productPriceHistory->setProductCountTotal($productCount);
        $productPriceHistory->setProductCountActual(0);
        $productPriceHistory->setStatus(ProductPriceHistory::STATUS_IN_PROGRESS);

        $this->_productPriceHistoryRepository->save($productPriceHistory);

        $response['productPriceHistoryId'] = $productPriceHistory->getId();
        return $response;
    }

    public function getNotCompletedCategoryProductPriceHistory()
    {
        $filter = $this->_filter->setField('status')->setValue(ProductPriceHistory::STATUS_INCOMPLETED);
        $filterGroups = $this->_filterGroup->setFilters([$filter]);
        $searchCriteria = $this->_searchCriteria->setFilterGroups([$filterGroups])->setPageSize(1)->setCurrentPage(1);
        return $this->_productPriceHistoryRepository->getList($searchCriteria)->getItems();
    }

    public function getInProgressCategoryProductPriceHistory()
    {
        $filter = $this->_filter->setField('status')->setValue(ProductPriceHistory::STATUS_IN_PROGRESS);
        $filterGroups = $this->_filterGroup->setFilters([$filter]);
        $searchCriteria = $this->_searchCriteria->setFilterGroups([$filterGroups]);
        return $this->_productPriceHistoryRepository->getList($searchCriteria)->getItems();
    }

    public function updateCategoryProductPriceHistory($categoryProductPriceId, $status)
    {
        $categoryProductPrice = $this->_productPriceHistoryRepository->getById($categoryProductPriceId);
        $categoryProductPrice->setStatus($status);
        $this->_productPriceHistoryRepository->save($categoryProductPrice);
    }

    public function getAllCategories($cat)
    {
        $allCat = [];
        foreach ($cat as $catIds) {
            array_push($allCat, $catIds);
            $categories = $this->categoryCollectionFactory->create()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('path', ["like" => '%/'.$catIds.'/%']);
            foreach ($categories as $category){
                array_push($allCat, $category->getId());
            }
        }
        return $allCat;
    }
    public function getDispersion($productId, $productRowId)
    {
        //$isdispersion=true;
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from('marketplace_assignproduct_items', ['dis_dispersion']);
        $select->where('id = '. $productRowId);
        $select->where('product_id = '. $productId);
        $res=$resourceConnection->fetchOne($select);
        return $res;
    }
}
