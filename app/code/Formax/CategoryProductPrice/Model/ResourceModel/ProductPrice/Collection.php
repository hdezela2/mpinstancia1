<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Formax\CategoryProductPrice\Model\ResourceModel\ProductPrice;

/**
 * Webkul CategoryProductPrice ResourceModel ProductPrice collection
 */
class Collection extends \Webkul\CategoryProductPrice\Model\ResourceModel\ProductPrice\Collection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Formax\CategoryProductPrice\Model\ProductPrice',
            'Formax\CategoryProductPrice\Model\ResourceModel\ProductPrice'
        );
        $this->_map['fields']['entity_id'] = 'main_table.entity_id';
    }
}
