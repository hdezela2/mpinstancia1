<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_CategoryProductPrice
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Formax\CategoryProductPrice\Model;

use Magento\Framework\Model\AbstractModel;
use Webkul\CategoryProductPrice\Api\Data\ProductPriceInterface;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * CategoryProductPrice Product Model.
 *
 * @method \Webkul\CategoryProductPrice\Model\ResourceModel\ProductPrice _getResource()
 * @method \Webkul\CategoryProductPrice\Model\ResourceModel\ProductPrice getResource()
 */
class ProductPrice extends \Webkul\CategoryProductPrice\Model\ProductPrice
{
    /**
     * No route page id.
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**
     * CategoryProductPrice Product cache tag.
     */
    const CACHE_TAG = 'category_product_price';

    /**
     * @var string
     */
    protected $_cacheTag = 'category_product_price';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'category_product_price';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Formax\CategoryProductPrice\Model\ResourceModel\ProductPrice');
    }

    /**
     * Load object data.
     *
     * @param int|null $id
     * @param string   $field
     *
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteProduct();
        }

        return parent::load($id, $field);
    }

    /**
     * Load No-Route Product.
     *
     * @return \Webkul\Marketplace\Model\Product
     */
    public function noRouteProduct()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Get ID.
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\Marketplace\Api\Data\ProductInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
}
