<?php
namespace Formax\ProductData\Observer;

use Magento\Framework\Event\ObserverInterface;
use Formax\Coupon\Helper\Data as helperCoupon;


class ActionPredispatch implements ObserverInterface{
    protected $_helperCoupon;

    public function __construct(
        helperCoupon $helperCoupon
    ) {
        $this->_helperCoupon = $helperCoupon;
    }


    public function execute(\Magento\Framework\Event\Observer $observer){
        if ($observer->getEvent()->getControllerAction()->getRequest()->isDispatched()) {
            $request        = $observer->getEvent()->getRequest();
            //$actionFullName = strtolower( $request->getFullActionName() );
            if ($request->getRouteName() == 'checkout') {
                $this->_helperCoupon->createAndApplyCoupon();
            }
        }   
    }
}
