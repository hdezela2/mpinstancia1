<?php
namespace Formax\ProductData\Block\Plugin\Navigation\Renderer;

use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Json\DecoderInterface;
use Magento\Store\Model\StoreManagerInterface;

class Slider
{
    /** @var EncoderInterface */
    protected $jsonEncoder;

    /** @var DecoderInterface */
    protected $jsonDecoder;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        EncoderInterface $jsonEncoder,
        DecoderInterface $jsonDecoder,
        StoreManagerInterface $storeManager
    ) {
        $this->jsonEncoder = $jsonEncoder;
        $this->jsonDecoder = $jsonDecoder;
        $this->storeManager = $storeManager;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundGetJsonConfig(\Smile\ElasticsuiteCatalog\Block\Navigation\Renderer\Slider $subject, callable $proceed)
    {
        $returnValue = $proceed();
        $config = $this->jsonDecoder->decode($returnValue);
        $config['rate'] = "1";
        if( in_array($this->storeManager->getWebsite()->getCode(), [
            \Formax\Offer\Helper\Data::CONVENIO_VOUCHER_WEBSITE,
            \Linets\VoucherSetup\Model\Constants::WEBSITE_CODE
        ])) {
            $config['fieldFormat']['pattern'] = "%s%";
        }
        return $this->jsonEncoder->encode($config);
    }
}