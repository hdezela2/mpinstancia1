<?php

namespace Formax\ProductData\Block\Product;

use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Linets\VoucherSetup\Model\Constants as LinetsVoucherConstants;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Chilecompra\CombustiblesRenewal\Model\Constants;

class Products extends \Webkul\MpAssignProduct\Block\Product\Products
{
    /**
     * @var \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory
     */
    protected $_regionalCondition;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $country;

    /**
     * @var bool
     */
    protected $showProfile;

    /**
     * @var array
     */
    protected $associatedOptions;

    /**
     * @var string
     */
    const CONVENIO_VOUCHER = 'voucher';

    /** @var ProductAttributeRepositoryInterface */
    protected $productAttributeRepository;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $collection
     * @param \Magento\Directory\Model\Country $country
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Webkul\MpAssignProduct\Helper\Data $helper,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $collection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\Country $country,
        ProductAttributeRepositoryInterface $productAttributeRepository,
        array $data = []
    ) {
        $this->_regionalCondition = $collection;
        $this->_storeManager = $storeManager;
        $this->country = $country;
        $this->productAttributeRepository = $productAttributeRepository;
        parent::__construct($context, $helper, $priceHelper, $mpHelper, $mediaConfig, $data);

        $this->showProfile = $helper->showProfile();
        $this->associatedOptions = $this->_assignHelper->getAssociatedOptions($this->getProduct()->getId());
    }

    /**
     * Get sellers conditions by sellers array
     *
     * @param array $sellers
     * @return \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory
     */
    public function getSellerConditions($sellers)
    {
        return $this->_regionalCondition->create()
            ->addWebsiteFilter($this->getWebsiteId())
            ->addSellerFilter($sellers);
    }

    /**
     * Get website identifier
     *
     * @return string|int|null
     */
    public function getWebsiteId()
    {
        return $this->_storeManager->getStore()->getWebsiteId();
    }

    /**
     * Get Chilean regions
     *
     * @return \Magento\Directory\Model\RegionFactory
     */
    public function getRegions()
    {
        $regionCollection = $this->country->loadByCode('CL')->getRegions();
        $regionsArray = [];
        foreach($regionCollection as $region){
            $regionsArray[] = [
                'id' => $region->getRegionId(),
                'name' => $region->getName()
            ];
        }
        return $regionsArray;
    }

    /**
     * @return bool|\Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getAssignedProducts()
    {
        $storeCode = $this->getStoreCode();
        $productId = $this->getProduct()->getId();
        $collection = $this->_assignHelper->getAssignProductCollection($productId);
        //Warning: Conditional only apply for SIMPLE and VIRTUAL product.
        if ($this->getProduct()->getTypeId() != "configurable"){
            $collection->addFieldToFilter('main_table.status', 1);
        }

        if ($this->getSortOrder() == "rating") {
            if (
                ($this->getDirection() == "desc") ||
                in_array($storeCode,[
                    self::CONVENIO_VOUCHER,
                    LinetsVoucherConstants::STORE_VIEW_CODE,
                    Constants::STORE_CODE
                ])
            ) {
                $collection->getSelect()->order('rating DESC');
            } else {
                $collection->getSelect()->order('rating ASC');
            }
        } else {
            if ($storeCode == HelperSoftware::SOFTWARE_STOREVIEW_CODE || $storeCode == SoftwareRenewalConstants::STORE_CODE) {
                $collection->getSelect()->order('mpud.shop_title ASC');
            } else {
                if(
                    ($this->getDirection() == "desc") ||
                    in_array($storeCode,[
                        self::CONVENIO_VOUCHER,
                        LinetsVoucherConstants::STORE_VIEW_CODE,
                        Constants::STORE_CODE
                    ])
                ){
                    $collection->getSelect()->order('main_table.price DESC');
                } else {
                    $collection->getSelect()->order('price ASC');
                }
            }
        }

        $storeId = $this->_storeManager->getStore()->getId();
        // FOODS and SoftwareRenewal only with supplier with status 70 (Active)
        if (in_array($storeCode, [
            \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE,
            \Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants::WEBSITE_CODE
        ])) {
            $collection->getSelect()->joinInner(
                ["centvar" => $collection->getTable('customer_entity_varchar')],
                "centvar.entity_id = cent.entity_id AND centvar.value = 70",
                []);
        }
        $collection->getSelect()
            ->joinInner(
                ["mu_0" => $collection->getTable('marketplace_userdata')],
                "mu_0.seller_id = main_table.seller_id AND mu_0.is_seller = 1 AND mu_0.store_id = 0",
                []
            )
            ->joinLeft(
                ["mu_{$storeId}" => $collection->getTable('marketplace_userdata')],
                "mu_{$storeId}.seller_id = main_table.seller_id AND mu_{$storeId}.is_seller = 1 AND mu_{$storeId}.store_id = {$storeId}",
                []
            )
            ->joinLeft(
                ["mad" => $collection->getTable('marketplace_assignproduct_data')],
                "mad.assign_id = main_table.id AND mad.is_default = 1 AND mad.type = 1 AND mad.store_view = {$storeId}",
                ['base_images' => new \Zend_Db_Expr("GROUP_CONCAT(mad.value SEPARATOR ',')")]
            )
            ->joinLeft(
                ["mad_images" => $collection->getTable('marketplace_assignproduct_data')],
                "mad_images.assign_id = main_table.id AND mad_images.type = 1 AND mad_images.store_view = {$storeId}",
                ['assign_images' => new \Zend_Db_Expr("GROUP_CONCAT(mad_images.value SEPARATOR ',')")]
            )
            ->joinLeft(
                ["md" => $collection->getTable('marketplace_datafeedback')],
                "md.seller_id = main_table.seller_id AND md.status = 0",
                ['feedcount' => new \Zend_Db_Expr("COUNT(md.seller_id)"), 'totalfeed' => new \Zend_Db_Expr("SUM(md.feed_price + md.feed_value + md.feed_quality) / (COUNT(md.seller_id) * 3)")]
            )
            ->columns(['seller_logo' => new \Zend_Db_Expr("IF(mu_{$storeId}.logo_pic IS NULL, mu_0.logo_pic, mu_{$storeId}.logo_pic)")]);

        return $collection;
    }

    /**
     * Rewrite \Webkul\MpAssignProduct\Block\Product\Products::getFormatedArray
     * @param object $product
     * @return array $formatedArray
     */
    public function getFormatedArray($product)
    {
        $formatedArray = [];
        $productId = $this->getProduct()->getId();
        $sellerId = $product->getSellerId();
        $mediaUrl = $this->_assignHelper->getBaseMediaUrl();

        $formatedArray['showProfile'] = $this->showProfile;
        $formatedArray['additionalColumnInfo'] = '';
        $formatedArray['assignId'] = $product->getId();
        $formatedArray['productType'] = $product->getType();
        $formatedArray['price'] = $this->priceHelper->currency($product->getPrice(), true, false);
        $formatedArray['base_price'] = $product->getBasePrice();
        $formatedArray['shipping_price'] = $product->getShippingPrice();
        $formatedArray['sellerId'] = $sellerId;
        $formatedArray['shopTitle'] = $product->getShopTitle() ? $product->getShopTitle() : $product->getShopUrl();
        $formatedArray['logo'] = $product->getSellerLogo() ? $mediaUrl.'avatar/'.$product->getSellerLogo() : $mediaUrl.'avatar/noimage.png';
        $formatedArray['shopUrl'] = $this->mpHelper->getRewriteUrl('marketplace/seller/profile/shop/'.$product->getShopUrl());
        $formatedArray['feedbackUrl'] = $this->mpHelper->getRewriteUrl('marketplace/seller/feedback/shop/'.$product->getShopUrl()).'#customer-reviews';
        $formatedArray['baseImage'] = $product->getBaseImages() ? end(@explode(',', $product->getBaseImages())) : '';
        $formatedArray['condition'] = ($product->getCondition() == 1) ? __("New") : __("Used");
        $rate = 0;
        $percent = $product->getTotalfeed();
        if ($this->showProfile) {
            if ($product->getFeedcount() > 0) {
                $rate = $percent / 20;
                $rate = number_format($rate, 1);
            }
        }
        $formatedArray['percent'] = $percent;
        $formatedArray['rate'] = $rate;
        $formatedArray['reviewLink'] = $this->mpHelper->getRewriteUrl('marketplace/seller/feedback/shop/'.$product->getShopUrl());
        $formatedArray['collectionUrl'] = $this->getUrl("marketplace/seller/collection")."shop/".$product->getShopUrl();
        if ($product->getQty() > 0) {
            $availability = __("IN STOCK");
            $availabilityClass = " wk-in-stock";
            $displyAddToCart = true;
        } else {
            $availability = __("OUT OF STOCK");
            $availabilityClass = " wk-out-of-stock";
            $displyAddToCart = false;
        }
        $jsonResult = '';
        if ($product->getType() == "configurable") {
            $availability = "-";
            $availabilityClass = " wk-in-stock";
            $displyAddToCart = false;
            $jsonResult = $this->associatedOptions;
        }
        $formatedArray['availability'] = $availability;
        $formatedArray['availabilityClass'] = $availabilityClass;
        $formatedArray['displyAddToCart'] = $displyAddToCart;
        $formatedArray['jsonResult'] = $jsonResult;
        $formatedArray['images'] = $product->getAssignImages() ? @explode(',', $product->getAssignImages()) : [];
        $formatedArray['baseImageUrl'] = $this->_assignHelper->getBaseImageUrl($product->getId());
        $formatedArray['assembly_price'] = $product->getAssemblyPrice();
        return $formatedArray;
    }

    /**
     * Get Store code
     *
     * @return string
     */
    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }

    /**
     * Get configurable product childs names
     *
     * @return array
     */
    public function getChildsNames($product) {
        $productTypeInstance = $product->getTypeInstance();
        $usedProducts = $productTypeInstance->getUsedProducts($product);

        $data = [];
        foreach ($usedProducts  as $child) {
            $data[$child->getSku()] = $child->getName();
        }

        return $data;
    }

    /**
     * Get region attribute id
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getRegionAttrId(){
        $attribute = $this->productAttributeRepository->get('region');
        return $attribute->getAttributeId();
    }

    public function getSituationAttrId()
    {
        $attribute = $this->productAttributeRepository->get('situation');
        return $attribute->getAttributeId();
    }

    /**
     * Method list regiones status delivery
     *
     * @param int|array $sellers
     * @return array
     */
    public function getConditionBySeller($sellers)
    {
        $collection = $this->_regionalCondition->create()
            ->addWebsiteFilter($this->getWebsiteId())
            ->addSellerFilter($sellers);
        $sql = $collection->getSelect()->__toString();
        $data = [];
        foreach ($collection as $item) {
            $data[$item->getRegionId()] = $item->getEnableStorepickup();
        }
        return $data;
    }

    public function getConditionBySellertoHtml($data)
    {
        $html = '';
        foreach ($data as $key => $value) {
            $html = $html. '<input type="hidden" class"cR" data-id="'.$key.'" value="'.$value.'">';
        }
        return $html;
    }
}
