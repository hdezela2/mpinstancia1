<?php
namespace Formax\ProductData\Plugin\Ui\Component\Listing\Columns;

class QuoteListStatus{

    /**
     * Prepare Data Source & translate it
     *
     * @param  array $dataSource
     * @return array
     */
    public function afterPrepareDataSource($subject, $dataSource)
    {   
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item['status'] = __($item['status']);
            }
        }
        return $dataSource;
    }

}
