<?php
namespace Formax\ProductData\Plugin\Ui\Component\Listing\Columns;

class Status{

    /**
     * Prepare Data Source & translation
     *
     * @param  array $dataSource
     * @return array
     */
    public function afterPrepareDataSource($subject, $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item['status'] = __($item['status']);
            }
        }
        return $dataSource;
    }

}
