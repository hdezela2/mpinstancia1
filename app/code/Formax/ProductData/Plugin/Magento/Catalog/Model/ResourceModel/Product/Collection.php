<?php

namespace Formax\ProductData\Plugin\Magento\Catalog\Model\ResourceModel\Product;

use Magento\Framework\DB\Select;

class Collection
{
    public function afterGetSelectCountSql(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $subject,
        $result
    ) {
        if (count($result->getPart(Select::GROUP))) {
            $group = $result->getPart(Select::GROUP);

            $result->reset(Select::GROUP);
            $result->reset(Select::COLUMNS);

            $result->columns(new \Zend_Db_Expr(("COUNT(DISTINCT " . implode(", ", $group) . ")")));
        }
        return $result;
    }
}