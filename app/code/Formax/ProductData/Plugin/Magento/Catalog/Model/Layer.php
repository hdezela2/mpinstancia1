<?php

namespace Formax\ProductData\Plugin\Magento\Catalog\Model;

class Layer
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->resource = $resource;
    }

    public function afterGetProductCollection(
        \Magento\Catalog\Model\Layer $subject,
        $result
    ) {
        $select = $result->getSelect();
        $selectSource = $select->getPart(\Magento\Framework\DB\Select::FROM);

        if (!array_key_exists('vendor_products', $selectSource)) {
            $connection = $this->resource->getConnection();
            $query = $connection->select();
            $subQuery = clone $query;

            $subQuery->from(
                ['mu' => $result->getTable('marketplace_userdata')],
                ['seller_id']
            )->where('is_seller = ?', 1);

            $query
                ->from(
                    ['mai' => $result->getTable('marketplace_assignproduct_items')],
                    ['id','product_id']
                )
                ->where("mai.seller_id IN ({$subQuery})")
                ->group('mai.seller_id')
                ->group('mai.product_id');

            $select
                ->joinLeft(
                    ["vendor_products" => $query],
                    "vendor_products.product_id=e.entity_id",
                    ['total_seller' => new \Zend_Db_Expr('COUNT(vendor_products.id)')]
                )
                ->group('e.entity_id');
        }
        return $result;
    }
}