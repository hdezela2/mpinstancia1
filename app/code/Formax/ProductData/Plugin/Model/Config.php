<?php
namespace Formax\ProductData\Plugin\Model;

class Config{

    /**
     * Get attributes used for sorting removing position index
     *
     * @param \Magento\Catalog\Model\Config $subject
     * @param  array $array
     * @return array
     */
    public function afterGetAttributeUsedForSortByArray($subject, $array){
        if (isset($array['position'])) {
            unset($array['position']);
        }
        return $array;
    }

}
