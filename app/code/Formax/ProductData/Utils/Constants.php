<?php
namespace Formax\ProductData\Utils;

class Constants{

  /**
   * @var array
   */
  const TABLES = [
    'ap' => 'marketplace_assignproduct_items',
    'ud' => 'marketplace_userdata'
  ];

}