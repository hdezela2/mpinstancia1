<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Requestforquote
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Formax\ProductData\Controller\Seller;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Webkul\Marketplace\Controller\Product\SaveProduct;
use Magento\Framework\Exception\LocalizedException;

/**
 * CreateNewProduct class
 */
class CreateNewProduct extends \Webkul\Requestforquote\Controller\Seller\CreateNewProduct
{

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $jsonResultFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Webkul\Requestforquote\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    private $marketplaceHelper;

    /**
     * @var \Webkul\Requestforquote\Model\QuoteFactory
     */
    private $quote;

    /**
     * @var \Webkul\Requestforquote\Model\InfoFactory
     */
    private $info;

    /**
     * @var \Magento\Catalog\Api\Data\ProductInterfaceFactory
     */
    private $productFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory
     */
    private $urlRewrite;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    private $directory_list;

    /**
     * @var SaveProductt
     */
    private $saveProduct;

    protected $_storeManager;

    /**
     * Constructor
     *
     * @param Context                                                             $context
     * @param \Magento\Framework\Controller\Result\JsonFactory                    $jsonResultFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                         $date
     * @param \Webkul\Requestforquote\Helper\Data                                 $dataHelper
     * @param \Webkul\Marketplace\Helper\Data                                     $marketplaceHelper
     * @param \Webkul\Requestforquote\Model\QuoteFactory                          $quote
     * @param \Webkul\Requestforquote\Model\InfoFactory                           $info
     * @param \Magento\Catalog\Api\Data\ProductInterfaceFactory                   $productFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                     $productRepository
     * @param \Magento\Framework\App\Filesystem\DirectoryList                     $directory_list
     * @param \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory $urlRewrite
     * @param SaveProduct                                                         $saveProduct
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\Requestforquote\Helper\Data $dataHelper,
        \Webkul\Marketplace\Helper\Data $marketplaceHelper,
        \Webkul\Requestforquote\Model\QuoteFactory $quote,
        \Webkul\Requestforquote\Model\InfoFactory $info,
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory $urlRewrite,
        SaveProduct $saveProduct,
        File $file,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->jsonResultFactory = $jsonResultFactory;
        $this->date = $date;
        $this->dataHelper = $dataHelper;
        $this->marketplaceHelper = $marketplaceHelper;
        $this->quote = $quote;
        $this->info = $info;
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->urlRewrite = $urlRewrite;
        $this->saveProduct = $saveProduct;
        $this->directory_list = $directory_list;
        $this->_storeManager = $storeManager;

        parent::__construct(
            $context,
            $jsonResultFactory,
            $date,
            $dataHelper,
            $marketplaceHelper,
            $quote,
            $info,
            $productFactory,
            $productRepository,
            $directory_list,
            $urlRewrite,
            $saveProduct,
            $file
        );
    }

    /**
     * @return \Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute(){
        $result = $this->jsonResultFactory->create();
        $post = $this->getRequest()->getparams();
        if ($post['productId']) {
            $sellerProductColls = $this->marketplaceHelper->getSellerProductDataByProductId($post['productId']);
            if ($sellerProductColls->getSize()) {
                return $result->setData(
                    [
                        'error' => true,
                        'message'=> "The quote product already added to your account."
                    ]
                );
            }
        }
        $imagePath = "";
        $sampleImages = [];
        $quote = $this->quote->create()->load($post['quoteId']);
        if ($quote['sample_images']) {
            $sampleImages =  explode(',', $quote['sample_images']);
        }
        if (!empty($sampleImages)) {
            foreach ($sampleImages as $sampleImage) {
                $ext = pathinfo($sampleImage, PATHINFO_EXTENSION);
                if ($ext != 'pdf' && $ext != 'doc') {
                    $imagePath = $this->directory_list->getPath('media')."/images/requestforquote/".$sampleImage;
                    break;
                }
            }

        }
        $originalProduct = $this->productFactory->create()->load($post['productId']);
        $originalProductSku = $originalProduct->getSku();
        $urlKey = $quote->getEntityId()."-".$originalProductSku;
        $createdUrl = $this->createUrl($urlKey, $urlKey, 0);
        try {

            $product = [];
            $typeProduct = '';
            switch($this->getStoreName()){
                case 'convenio_storeview_software':
                case 'Software':
                case SoftwareRenewalConstants::STORE_NAME:
                case SoftwareRenewalConstants::STORE_CODE:
                    $typeProduct = 'virtual';
                    $product['voucher_product_type'] = 2;// Discount
                break;
                default:
                    $typeProduct = \Webkul\Requestforquote\Model\Product\Type\Quote::TYPE_ID;
                break;
            }
            //$typeProduct = \Webkul\Requestforquote\Model\Product\Type\Quote::TYPE_ID;

            $product['name'] = $quote->getSubject();
            $product['sku'] = $quote->getEntityId()."-".$originalProductSku;
            $product['price'] = $post['price'];
            if ($this->dataHelper->getStore()->getWebsite()->getCode() == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE){
                $product['base_price'] = $post['price'];
                $product['assembly_price'] = $post['assemblyPrice'];
                $product['price'] = $product['assembly_price']+$product['base_price'];
            }
            $product['url_key'] = $createdUrl;
            $product['stock_data'] = [
                'manage_stock'=>1,
                'use_config_manage_stock'=>1,
                'min_sale_qty'=>$post['quantity'],
                'max_sale_qty' => $post['quantity']
            ];
            $product['quantity_and_stock_status'] = ['qty'=>$post['quantity'],'is_in_stock'=>1];
            $product['visibility'] = 1;
            $product['tax_class_id'] = $post['taxClassId'];
            $product['product_has_weight'] = ($originalProduct->getWeight() > 0)?1:0;
            $product['weight'] = $originalProduct->getWeight();
            $wholeData = [];
            $wholeData['type'] = $typeProduct;
            $wholeData['set'] = 4;
            $wholeData['status'] = 1;
            $wholeData['product'] = $product;
            $returnArr = $this->saveProduct->saveProductData(
                $this->marketplaceHelper->getCustomerId(),
                $wholeData
            );
            if ($returnArr['error'] == 0) {
                $newProductId = $returnArr['product_id'];
            } else {
                throw new LocalizedException(
                    __(
                        "Product Type or Attribute Set Invalid Or Not Allowed From Marketplace Configuration"
                    )
                );
            }
            $productModel= $this->productFactory->create()->load($newProductId);
            if ($imagePath!="") {
                $productModel->addImageToMediaGallery($imagePath, ['image', 'small_image', 'thumbnail'], false, true);
                $productModel->save();
            }
            $infoModel = $this->info->create()->load($post['quoteInfoId']);
            $infoModel->setProductCreated(1);
            $infoModel->save();

            $quoteModel = $this->quote->create()->load($post['quoteId']);
            $quoteModel->setProductId($newProductId);
            if($this->getStoreName() != 'convenio_storeview_software' || $this->getStoreName() != 'Software' || $this->getStoreName() != SoftwareRenewalConstants::STORE_CODE || $this->getStoreName() != SoftwareRenewalConstants::STORE_NAME){
                $quoteModel->save();
            }
            $message = __("You created a product name %1 of type quote in to your account.", $quote->getSubject());
            return $result->setData(['success' => true, 'message'=> $message]);
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
            return $result->setData(['error' => true, 'message'=> $e->getMessage()]);
        }
    }

    public function getStoreName(){
        return trim($this->_storeManager->getStore()->getName());
    }

}
