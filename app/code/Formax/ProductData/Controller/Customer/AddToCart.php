<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Requestforquote
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Formax\ProductData\Controller\Customer;

use \Formax\ProductData\Logger\Logger;
use \Magento\Framework\App\Action\Context;
use Magento\Checkout\Helper\Cart;
use \Webkul\Requestforquote\Model\InfoFactory;
use \Magento\Framework\Exception\StateException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Checkout\Model\Session as checkoutSession;
use \Magento\Framework\App\Action\HttpPostActionInterface;
use \Magento\Framework\DataObjectFactory;
use Magento\Framework\Exception\LocalizedException;
use \Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;

class AddToCart extends \Webkul\Requestforquote\Controller\Customer\AddToCart implements HttpPostActionInterface
{
    private const HTTP_OK = 200;

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    private $formKey;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cart;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    private $product;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $jsonResultFactory;

    protected $objectFactory;

    /**
     * @var \Formax\ProductData\Logger\Logger
     */
    protected $logger;

    protected $_storeManager;

    private $_helperCoupon;
    protected $infoFactory;
    private $_checkoutSession;

    /**
     * @var Cart
     */
    private Cart $cartHelper;


    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\Cart $cart,
        Cart $cartHelper,
        \Magento\Catalog\Model\Product $product,
        \Webkul\Requestforquote\Helper\Data $dataHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Formax\Coupon\Helper\Data $helperCoupon,
        DataObjectFactory $objectFactory,
        InfoFactory $infoFactory,
        checkoutSession $checkoutSession,
        Logger $logger,
       array $data = []
    ) {

        $this->formKey = $formKey;
        $this->cart = $cart;
        $this->cartHelper = $cartHelper;
        $this->product = $product;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->url = $context->getUrl();
        $this->_helperCoupon = $helperCoupon;
        $this->infoFactory = $infoFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->logger = $logger;
        $this->_storeManager = $storeManager;
        $this->objectFactory = $objectFactory;

        parent::__construct(
            $context,
            $jsonResultFactory,
            $formKey,
            $cart,
            $product,
            $dataHelper,
            $data
        );
    }

    public function execute()
    {
        $this->logger->info('Formax_ProductData_Controller_Customer_AddToCart Begin');

        $responseData = [];

        $post = $this->getRequest()->getPostValue();
        try {
            if($this->existsCart($post, $responseData)) {
                throw new LocalizedException(__('item already added to the cart'), new \Exception());
            }
            if ($this->cartHelper->getItemsCount() !== 0) {
                throw new LocalizedException(__('You have a PO in your shopping cart, so you cannot add another OC. You will be redirected to your shopping cart to complete or delete that PO.'), new \Exception());
            }
            $this->addToCart($post, $responseData);
        } catch(LocalizedException $e) {
            $this->logger->critical('Add To cart Quote Error message: ' . $e->getMessage());
            $this->messageManager->addError($e->getMessage());
            $cartUrl = $this->url->getUrl('checkout/cart/index');
            $responseData = ['success' => true,'Url'=> $cartUrl,'message'=>$e->getMessage()];
        } catch (\Exception $e) {
            $this->logger->critical('Formax_ProductData_Controller_Customer_AddToCart: Exception message: ' . $e->getMessage());
            $this->messageManager->addError(_($e->getMessage()));
            $responseData = ['error' => true,'message'=>$e->getMessage()];
        }


        $this->logger->info('Formax_ProductData_Controller_Customer_AddToCart End');

        $resultJson = $this->jsonResultFactory->create();
        $resultJson->setHttpResponseCode(self::HTTP_OK);
        $resultJson->setData($responseData);

        return $resultJson;
    }


    private function existsCart($post, &$responseData){
        $exists = false;

        $this->logger->info('Formax_ProductData_Controller_Customer_AddToCart existsCart function');

        try {
            $cart = $this->cart->getItems();

            if (!empty($cart)) {
                foreach ($cart as $cartItem)
                {
                    $infoBuyRequest = $cartItem->getBuyRequest();
                    if (isset($infoBuyRequest['quoteInfoId']))
                    {
                        if ($infoBuyRequest['quoteInfoId'] == $post['quoteInfoId'])
                        {
                            // Add link to the cart page
                            $cartUrl = $this->url->getUrl('checkout/cart');

                            $this->logger->info('Formax_ProductData_Controller_Customer_AddToCart already exists');

                            $responseData = [
                                'success' => true,
                                'Url'=> $cartUrl,
                                'message'=> __("item already added to the cart")
                            ];

                            $exists = true;
                        }
                    }
                }
            }
        } catch (NoSuchEntityException $e) {
        } catch (StateException $e) {
        } catch (\Exception $e) {
        }

        return $exists;
    }

    private function addToCart($post, &$responseData){
        $this->logger->info('Formax_ProductData_Controller_Customer_AddToCart addToCart function');

        try {
            $params = [
                'form_key' => $this->formKey->getFormKey(),
                'product' => $post['productId'],
                'qty' => $post['quantity'],
                'quoteInfoId' => $post['quoteInfoId'],
                'price' => $post['price']
            ];

            if (in_array($this->getStoreName(), ['convenio_storeview_software', 'Software', SoftwareRenewalConstants::STORE_CODE, SoftwareRenewalConstants::STORE_NAME])) {
                if (isset($post['sellerId']) && is_numeric($post['sellerId'])){
                    $params['seller_id'] = $post['sellerId'];
                }
            }

            if (isset($post['base_price'])){
                $params['base_price'] = $post['base_price'];
            }
            if (isset($post['assembly_price'])){
                $params['assembly_price'] = $post['assembly_price'];
            }

            $request = $this->objectFactory->create(['data' => $params]);
            $product = $this->product->load($post['productId']);
            $cartModel = $this->cart->addProduct($product, $request);

            $this->cart->save();
            $this->_checkoutSession->getQuote()->collectTotals()->save();
            $this->_helperCoupon->createAndApplyCoupon();
            $cartUrl = $this->url->getUrl('checkout/cart/index');

            $this->messageManager->addSuccess(__("quote product successfully added to the cart"));
            $this->logger->info('Formax_ProductData_Controller_Customer_AddToCart: success id: ' . $cartModel->getId());

            $responseData = [
                'success' => true,
                'Url'=> $cartUrl,
                'message'=> __('quote product successfully added to the cart')
            ];
        } catch (LocalizedException $e) {
            $this->logger->critical('Formax_ProductData_Controller_Customer_AddToCart: LocalizedException Error message: ' . $e->getMessage());
            $this->messageManager->addError(_($e->getMessage()));
            $responseData = ['error' => true,'message'=>$e->getMessage()];
        } catch (NoSuchEntityException $e) {
            $this->logger->critical('Formax_ProductData_Controller_Customer_AddToCart: NoSuchEntityException Error message: ' . $e->getMessage());
            $this->messageManager->addError(_($e->getMessage()));
            $responseData = ['error' => true,'message'=>$e->getMessage()];
        } catch (StateException $e) {
            $this->logger->critical('Formax_ProductData_Controller_Customer_AddToCart: StateExceptionError message: ' . $e->getMessage());
            $this->messageManager->addError(_($e->getMessage()));
            $responseData = ['error' => true,'message'=>$e->getMessage()];
        } catch (\Exception $e) {
            $this->logger->critical('Formax_ProductData_Controller_Customer_AddToCart: Exception message: ' . $e->getMessage());
            $this->messageManager->addError(_($e->getMessage()));
            $responseData = ['error' => true,'message'=>$e->getMessage()];
        }
    }

    public function getStoreName(){
        return trim($this->_storeManager->getStore()->getName());
    }
}
