<?php
namespace Formax\ProductData\Model\Plugin;

class PriceCurrency
{
    public function aroundConvert(\Magento\Directory\Model\PriceCurrency $subject, callable $proceed, $amount, ...$args)
    {
        return (float)$amount;
    }
}