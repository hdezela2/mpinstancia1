<?php
namespace Formax\ProductData\Helper;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class IsNew extends \Magento\Framework\Url\Helper\Data{

    /**
     * @var TimezoneInterface
     */
    protected $localeDate;

    /**
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     */
    public function __construct( TimezoneInterface $localeDate ){
        $this->localeDate = $localeDate;
    }

    /**
     * Check if a product is whether or not new
     *
     * @param \Magento\Catalog\Block\Product\AbstractProduct $product
     * @return bool
     */
    public function isNewProduct($product){
        $newsFromDate = $product->getNewsFromDate();
        $newsToDate = $product->getNewsToDate();
        if (!$newsFromDate && !$newsToDate) {
            return false;
        }

        return $this->localeDate->isScopeDateInInterval(
            $product->getStore(),
            $newsFromDate,
            $newsToDate
        );
    }
}
