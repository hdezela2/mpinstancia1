<?php
namespace Formax\ProductData\Helper;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Linets\VehiculosSetUp\Model\Constants;
use Magento\Framework\Exception\NoSuchEntityException;

class AssignProduct extends \Formax\AssignProduct\Helper\MpAssignProduct
{

    /**
     * Get Minimum and Maximum Price with Currency
     *
     * @param int $productId
     * @param string $type
     * @return string
     */
    public function getSupplierPricesHtml($productId, $type = '-', $isVoucher = false)
    {
        $prices = [];
        $priceHtml = (object)['minPrice'=>null,'maxPrice'=>null];
        if ($type == "configurable") {
            $model = $this->_associates->create();
            $collection = $model->getCollection()
                ->addFieldToFilter("parent_product_id", $productId);
            $collection->getSelect()->where('main_table.status = ?', 1);

            $assignItemsTable = 'marketplace_assignproduct_items as mai';
            $customerEntityTable = 'customer_entity_varchar as cev';
            $eavAttrTable = 'eav_attribute as ea';
            $collection->getSelect()
                ->join($assignItemsTable,'mai.id = main_table.parent_id', ['parent'=>'id'])
                ->join($customerEntityTable,'mai.seller_id = cev.entity_id', ['seller_id'=>'entity_id', 'status'=>'value'])
                ->join($eavAttrTable,"ea.attribute_id=cev.attribute_id", ['attribute_code'])
            ->where('ea.attribute_code = ?', 'wkv_dccp_state_details')
            ->where('main_table.qty > ?', 0)
            ->where('cev.value = ?', 70);
            foreach ($collection as $key => $item) {
                $prices[$key] = $item->getPrice();
            }
        } else {
            $totalProducts = $this->getTotalProducts($productId);
            foreach ($totalProducts as $key => $product) {
                if(!$product['seller_id'] || !$product['qty'])
                  continue;
                $prices[$key] = $product['price'];
            }
        }
        sort($prices);
        if(count($prices)){
            $priceHtml->minPrice = ($isVoucher?$prices[0]:$this->_currency->currency($prices[0], true, false));
            $last = count($prices)-1;
            if(count($prices)>1 && $prices[0]!=$prices[$last]){
              $priceHtml->maxPrice = ($isVoucher?$prices[$last]:$this->_currency->currency($prices[$last], true, false));
            }
        }
        return $priceHtml;
    }

    /**
     * Get prices formatted with discount in HTML
     *
     * @param array $prices
     * @param int $productId
     * @return string
     */
    public function getPricesWithOfferHtml($prices, $productId, $isVoucher=false){
        $_html = '';
        if(is_array($prices) && array_key_exists($productId, $prices)){
            $obj = $prices[$productId];
            $priceBox = '<div class="price-box cc-pricebox">%s</div>';
            $priceContainer = '<div class="cc-pricecontainer">%s</div>';
            $basePrice = '<span class="price base-price">%s</span>';
            $basePriceN = ($isVoucher?number_format($obj['base_price'], 1, ',', '')."%":$this->_currency->currency($obj['base_price'], true, false));
            $specialPrice = '<span class="price special-price">%s</span>';
            if($obj['special_price']){
                $specialPriceN = ($isVoucher?number_format($obj['special_price'], 1, ',', '')."%":$this->_currency->currency($obj['special_price'], true, false));
                $prices = sprintf($specialPrice.$basePrice, $specialPriceN, $basePriceN);
            }else{
                $prices = sprintf($basePrice, $basePriceN);
            }
            $_html = sprintf($priceBox, sprintf($priceContainer, $prices));
        }
        return $_html;
    }


	/**
	 * Get Store code
	 *
	 * @return string
	 * @throws NoSuchEntityException
	 */
	public function getStoreCode(): string
	{
		return $this->_storeManager->getStore()->getCode();
	}

    /**
     * @throws NoSuchEntityException
     */
    public function shouldRenderPage()
    {
        $agreementsNotAllowed = [
            SoftwareRenewalConstants::STORE_VIEW_CODE
        ];

        return !in_array($this->_storeManager->getStore()->getCode(), $agreementsNotAllowed);
    }
}
