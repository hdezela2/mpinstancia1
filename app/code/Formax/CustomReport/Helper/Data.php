<?php

namespace Formax\CustomReport\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $eavAttribute;

    /**
     * @var \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory
     */
    protected $itemCollectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;
    private StoreManagerInterface $storeManager;
    private \Summa\ComputadoresSetUp\Helper\Data $computadoresHelper;
    private \Summa\FormatCurrency\Helper\Data $formatCurrencyHelper;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory $itemCollectionFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory $itemCollectionFactory,
        \Summa\ComputadoresSetUp\Helper\Data $computadoresHelper,
        \Summa\FormatCurrency\Helper\Data $formatCurrencyHelper,
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
        $this->computadoresHelper = $computadoresHelper;
        $this->formatCurrencyHelper = $formatCurrencyHelper;
        $this->resource = $resource;
        $this->eavAttribute = $eavAttribute;
        $this->timezone = $timezone;
        $this->itemCollectionFactory = $itemCollectionFactory;

        parent::__construct($context);
    }

    /**
     * Exec SQL query for report
     *
     * @return array
     */
    public function execSqlQuery()
    {
        $collection = $this->itemCollectionFactory->create();

        $connection = $this->resource->getConnection();
        $currentDate = $connection->quote($this->timezone->date()->format('Y-m-d'));
        $proAttId = $this->eavAttribute->getIdByCode('catalog_product', 'name');
        $proPriceAttId = $this->eavAttribute->getIdByCode('catalog_product', 'price');
        $agreementAtt = $this->eavAttribute->getIdByCode('customer', 'user_rest_id_active_agreement');
        $brandAtt = $this->eavAttribute->getIdByCode('catalog_product', 'marca');
        $modelAtt = $this->eavAttribute->getIdByCode('catalog_product', 'modelo');
        $measureAtt = $this->eavAttribute->getIdByCode('catalog_product', 'medidas');
        $descriptionAtt = $this->eavAttribute->getIdByCode('catalog_product', 'description');
        $quimicoAtt = $this->eavAttribute->getIdByCode('catalog_product', 'is_quimico');
        $mayoristaAtt = $this->eavAttribute->getIdByCode('catalog_product', 'is_mayorista');
        $statusAtt = $this->eavAttribute->getIdByCode('catalog_product', 'status');
        $productTypeAtt = $this->eavAttribute->getIdByCode('catalog_product', 'tipo_producto');

        $offer = $this->resource->getTableName('dccp_offer_product');
        $customerGridFlat = $this->resource->getTableName('customer_grid_flat');
        $catalogProductEntity = $this->resource->getTableName('catalog_product_entity');
        $catalogProductEntityVarchar = $this->resource->getTableName('catalog_product_entity_varchar');
        $catalogProductEntityInt = $this->resource->getTableName('catalog_product_entity_int');
        $catalogProductEntityDecimal = $this->resource->getTableName('catalog_product_entity_decimal');
        $catalogProductEntityText = $this->resource->getTableName('catalog_product_entity_text');
        $customerEntityVarchar = $this->resource->getTableName('customer_entity_varchar');
        $optionValue = $this->resource->getTableName('eav_attribute_option_value');

        $sql = $offer.' as o';
        $cond = 'main_table.product_id = o.product_id AND main_table.seller_id = o.seller_id
        AND main_table.id = o.assign_id AND o.status = 1 AND ' . $currentDate . ' >= DATE(o.start_date)
        AND ' . $currentDate . ' <= DATE(o.end_date) AND o.special_price < o.base_price';
        $fields = [];

        $collection->getSelect()
            ->reset(\Zend_Db_Select::COLUMNS)
            ->columns([
                'IdOfertaProveedor' => 'main_table.description',
                'PrecioBaseOferta' => new \Zend_Db_Expr('CAST(IFNULL(o.base_price, main_table.price) AS DECIMAL(12,2))'),
                'PrecioOfertaSpecial' => new \Zend_Db_Expr('CAST(IFNULL(o.special_price, NULL) AS DECIMAL(12,2))'),
                'CantidadOfertaProveedor' => 'main_table.qty',
                'EstadoOferta' => new \Zend_Db_Expr("CASE WHEN main_table.status = 1 THEN 'Aprobado' WHEN main_table.status = 0 THEN 'Desaprobado' ELSE 'Pendiente' END"),
                'FechaCreacionOferta' => 'main_table.created_at',
                'FechaActualizacionOferta' => 'main_table.updated_at',
                'DeshabilitadoDispersion' => new \Zend_Db_Expr('IF(main_table.dis_dispersion = 1, "Sí", "No")')
            ])->joinLeft($sql, $cond, $fields);

        $sql = $customerGridFlat.' as cgf';
        $cond = 'main_table.seller_id = cgf.entity_id';
        $fields = [
            'NombreProveedor' => 'cgf.name',
            'RutProveedor' => 'wkv_dccp_rut',
            'EmailProveedor' => 'cgf.email'
        ];
        $collection->getSelect()
            ->join($sql, $cond, $fields);

        $sql = $customerEntityVarchar.' as cev';
        $cond = 'cgf.entity_id = cev.entity_id AND cev.attribute_id = ' . $agreementAtt;
        $fields = ['CodigoConvenio' => 'cev.value'];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $optionValue.' as eaov';
        $cond = 'cgf.wkv_dccp_state_details = eaov.option_id';
        $fields = ['EstadoProveedorConvenio' => 'eaov.value'];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $catalogProductEntity.' as cpe';
        $cond = 'main_table.product_id = cpe.entity_id';
        $fields = ['IdOfertaDCCP' => 'sku'];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $catalogProductEntityVarchar.' as cpev';
        $cond = 'cpe.row_id = cpev.row_id AND cpev.store_id = 0 AND cpev.attribute_id = ' . $proAttId;
        $fields = ['NombreOferta' => 'cpev.value'];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $catalogProductEntityText.' as cpet';
        $cond = 'cpe.row_id = cpet.row_id AND cpet.store_id = 0 AND cpet.attribute_id = ' . $descriptionAtt;
        $fields = ['DescripcionOferta' => 'cpet.value'];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $catalogProductEntityInt.' as cpevBrand';
        $cond = 'cpe.row_id = cpevBrand.row_id AND cpevBrand.store_id = 0 AND cpevBrand.attribute_id = ' . $brandAtt;
        $fields = [];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $optionValue.' as eaovMarca';
        $cond = 'cpevBrand.value = eaovMarca.option_id';
        $fields = ['MarcaOferta' => 'eaovMarca.value'];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $catalogProductEntityVarchar.' as cpevModel';
        $cond = 'cpe.row_id = cpevModel.row_id AND cpevModel.store_id = 0 AND cpevModel.attribute_id = ' . $modelAtt;
        $fields = ['ModeloOferta' => 'cpevModel.value'];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $catalogProductEntityVarchar.' as cpevMeasure';
        $cond = 'cpe.row_id = cpevMeasure.row_id AND cpevMeasure.store_id = 0 AND cpevMeasure.attribute_id = ' . $measureAtt;
        $fields = ['MedidasOferta' => 'cpevMeasure.value'];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $catalogProductEntityVarchar.' as cpevTipoProducto';
        $cond = 'cpe.row_id = cpevTipoProducto.row_id AND cpevTipoProducto.store_id = 0 AND cpevTipoProducto.attribute_id = ' . $productTypeAtt;
        $fields = ['TipoProducto' => 'cpevTipoProducto.value'];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $catalogProductEntityInt.' as cpei';
        $cond = 'cpe.row_id = cpei.row_id AND cpei.store_id = 0 AND cpei.attribute_id = ' . $quimicoAtt;
        $fields = ['EsQuimico' => new \Zend_Db_Expr('IF(cpei.value = 1, "Sí", "No")')];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $catalogProductEntityInt.' as cpeiMayorista';
        $cond = 'cpe.row_id = cpeiMayorista.row_id AND cpeiMayorista.store_id = 0 AND cpeiMayorista.attribute_id = ' . $mayoristaAtt;
        $fields = ['EsMayorista' => new \Zend_Db_Expr('IF(cpeiMayorista.value = 1, "Sí", "No")')];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $catalogProductEntityInt.' as cpevStatus';
        $cond = 'cpe.row_id = cpevStatus.row_id AND cpevStatus.store_id = 0 AND cpevStatus.attribute_id = ' . $statusAtt;
        $fields = ['StatusProducto' => new \Zend_Db_Expr('IF(cpevStatus.value = 1, "Activo", "Inactivo")')];
        $collection->getSelect()
            ->joinLeft($sql, $cond, $fields)
            ->group('main_table.id');

        $items = $collection->toArray();

        #### HOT FIX COMPUTADORES ####
        $computadoresConvenioId = \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_COMPUTERS;
        $computadoresRenewalConvenioId = \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_COMPUTERS_2021_01;
        $computadoresRenewal2ConvenioId = \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_COMPUTERS_2021_12;
        #### CONFIGURACION DE DECIMALES POR TIPO DE MONEDA ####
//        $computadoresCCurrencyCode = $this->storeManager->getStore(\Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_COMPUTERS)->getCurrentCurrency()->getCode();
//        $currencyConf = $this->formatCurrencyHelper->getConfigValue($computadoresCCurrencyCode);

        foreach($items['items']  as $key => $item) {
            if (isset($item['CodigoConvenio']) && $item['CodigoConvenio'] != $computadoresConvenioId && $item['CodigoConvenio'] != $computadoresRenewalConvenioId && $item['CodigoConvenio'] != $computadoresRenewal2ConvenioId) {
                $basePrice = $item['PrecioBaseOferta'];
                $offerPrice = $item['PrecioOfertaSpecial'];
                $items['items'][$key]['PrecioBaseOferta'] = round($basePrice);
                $items['items'][$key]['PrecioOfertaSpecial'] = round($offerPrice);
            }
        }
        #### -------------------- ####

        return $items;
    }
}
