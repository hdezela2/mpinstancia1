<?php

namespace Formax\CustomReport\Controller\Adminhtml\Report;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;
use Formax\CustomReport\Helper\Data as Helper;

class Post extends \Magento\Backend\App\Action
{
    /**
     * @var FileFactory
     */
    private $fileFactory;

    private $directory;

    private $fileSystem;

    /**
     * @var Helper
     */
    private $helper;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Helper $helper
     * @param FileFactory $fileFactory
     * @param Filesystem $filesystem
     */
    public function __construct(
        Context $context,
        Helper $helper,
        FileFactory $fileFactory,
        Filesystem $filesystem
    ) {
        $this->fileFactory = $fileFactory;
        $this->fileSystem = $filesystem;
        $this->helper = $helper;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        try {
            $directory = $this->fileSystem->getDirectoryWrite(DirectoryList::VAR_DIR);
            $filePath = 'export/Ofertas.csv';
            $directory->create('export');
            $stream = $directory->openFile($filePath, 'w+');
            $stream->lock();
            $result = $this->helper->execSqlQuery();
            $i = 0;

            if (isset($result['items'])) {
                $header = [];
                foreach ($result['items'] as $column) {
                    $itemData = [];
                    foreach ($column as $key => $item) {
                        if ($i == 0) {
                            $header[] = $key;
                        }
                        $itemData[] = $item;
                    }

                    if ($i == 0) {
                        $stream->writeCsv($header);
                    }

                    $stream->writeCsv($itemData);
                    $i++;
                }

                $content = [];
                $content['type'] = 'filename';
                $content['value'] = $filePath;
                $content['rm'] = '1';
                $csvfilename = 'Ofertas.csv';

                return $this->fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);
            } else {
                echo __('No result was found');
            }
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
}
