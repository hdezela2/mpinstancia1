<?php
namespace Formax\CustomReport\Controller\Adminhtml\Report;

class Index extends \Magento\Backend\App\Action
{
    /**
    * @var \Magento\Framework\View\Result\PageFactory
    */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
         parent::__construct($context);
         $this->resultPageFactory = $resultPageFactory;
    }

  /**
   * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
   *
   * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
   */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
