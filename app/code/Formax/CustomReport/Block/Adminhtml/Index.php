<?php

namespace Formax\CustomReport\Block\Adminhtml;

class Index extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function getUrlData()
    {
        return $this->getUrl('customreport/report/post');
    }
}
