<?php
namespace Formax\GreatBuy\Controller\Send;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Index extends Action{

  /**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
  protected $scope;
  /**
   * @var \Magento\Store\Model\ScopeInterface
   */
  protected $_scopeInterface = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
  /**
   * @var string
   */
  protected $systemPath = 'dccp_endpoint/greatbuy/';
  /**
   * @var \Magento\Framework\Controller\Result\JsonFactory
   */
  protected $resultJsonFactory;
  /**
   * @var \Magento\Framework\App\ResourceConnection
   */
  protected $_resourceConnection;
  /**
   * @var \Magento\Framework\HTTP\ZendClientFactory
   */
  protected $_httpClientFactory;
  /**
   * @var \Magento\Customer\Model\Session
   */
  protected $customerSession;
  /**
   * @var \Magento\Customer\Model\Session
   */
  protected $customerSessionData;
  /**
   * @var \Magento\Framework\View\LayoutFactory
   */
  protected $layoutFactory;
  /**
   * @var \Magento\Checkout\Model\Cart
   */
  protected $cart;
  /**
   * @var \Formax\GreatBuy\Logger\Logger
   */
  protected $logger;
  /**
   * @var \Magento\Customer\Model\CustomerFactory
   */
  protected $_customer;

  const WEBKUL_QUOTE_ID = \Webkul\Requestforquote\Model\Product\Type\Quote::TYPE_ID;

  /**
   * @param Context $context
   * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
   * @param \Magento\Framework\App\ResourceConnection $resourceConnection
   * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
   * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
   * @param \Magento\Customer\Model\Session $customerSession
   * @param \Magento\Framework\View\LayoutFactory $layoutFactory
   * @param \Formax\GreatBuy\Logger\Logger $logger
   * @param \Magento\Checkout\Model\Cart $cart
   * @param \Magento\Customer\Model\CustomerFactory $customer
   */
  public function __construct(
      Context $context,
      \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
      \Magento\Framework\App\ResourceConnection $resourceConnection,
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
      \Magento\Customer\Model\Session $customerSession,
      \Magento\Framework\View\LayoutFactory $layoutFactory,
      \Formax\GreatBuy\Logger\Logger $logger,
      \Magento\Checkout\Model\Cart $cart,
      \Magento\Customer\Model\CustomerFactory $customer
  ) {
      $this->resultJsonFactory = $resultJsonFactory;
      $this->_resourceConnection = $resourceConnection;
      $this->scope = $scopeConfig;
      $this->_httpClientFactory = $httpClientFactory;
      $this->customerSession = $customerSession;
      $this->customerSessionData = $customerSession->getCustomer();
      $this->layoutFactory = $layoutFactory;
      $this->logger = $logger;
      $this->cart = $cart;
      $this->_customer = $customer;
      parent::__construct($context);
  }

  public function execute(){
    $result = $this->resultJsonFactory->create();
    $post = $this->getRequest()->getParams();
    $isLoggedIn = $this->customerSession->isLoggedIn();
    $redirectUri = '';
    $this->logger->info("Starting process...");
    if($post && isset($post['sellerId']) && isset($post['purchaseUnit']) && $isLoggedIn){
      $this->logger->info("Customer is successfully logged in.");
      $cart = $this->formatCart($post['sellerId'], $post['purchaseUnit']);
      $this->logger->info("Cart: ".json_encode($cart));
      $redirectUri = $this->sendToGreatBuy($cart);
    }
    $this->logger->info("Redirect Uri: ".$redirectUri."\r\n\r\n");
    $result->setData(['redirectUri'=>$redirectUri]);
    return $result;
  }

  /**
   * Format cart object by post variables
   *
   * @param string $sellerId
   * @param string $purchaseUnit
   * @return array
   */
  private function formatCart($sellerId, $purchaseUnit){
    $this->logger->info("Getting seller with ID: ".$sellerId);
    $customer = $this->getCustomerById($sellerId);
    $obj = [
      'idOrganismo' => (int)$customer->getData('user_rest_id_organization'),
      'idConvenioMarco' => (int)$customer->getData('user_rest_id_active_agreement'),
      'unidadCompra' => (int)$purchaseUnit,
      'productos' => []
    ];

    $mpBlock = $this->layoutFactory->create()->createBlock('Webkul\Mpsplitcart\Block\Index');
    $sellerData = $mpBlock->getSellerData();
    $cart = $this->cart->getQuote();
    if(array_key_exists($sellerId, $sellerData)){
      foreach($cart->getAllItems() as $item){
        if(array_key_exists($item->getId(), $sellerData[$sellerId])){
          $sku = $item->getSku();
          if($item->getProductType() == self::WEBKUL_QUOTE_ID){
              $sku = explode('-', $sku);
              unset($sku[0]);
              $sku = implode('-', $sku);
          }
          $obj['productos'][] = [
            'idProducto' => intval($sku),
            'cantidad' => (int)$item->getQty(),
            'precioReferencia' => $this->formatPrice($item->getPrice()),
            'totalProducto' => $this->formatPrice($item->getPrice() * $item->getQty())
          ];
        }
      }

    }
    return $obj;
  }

  /**
   * Send cart to MercadoPublico and receive response to redirect
   *
   * @param array $cart
   * @return string
   */
  private function sendToGreatBuy($cart){
    $url = '';
    try{
      $endpoint = $this->getSystemValue('sendcart_endpoint');
      $this->logger->info("Endpoint: ".$endpoint);
      $client = $this->_httpClientFactory->create();
      $client->setUri($endpoint);
      $client->setRawData(json_encode($cart));
      $httpHeaders = [
         'Authorization' => 'Bearer '.$this->getCustomerData('user_rest_atk'),
         'Accept' => 'application/json',
         'Content-Type' => 'application/json'
      ];
      $client->setHeaders($httpHeaders);
      $response = $client->request(\Zend_Http_Client::POST)->getBody();
      $this->logger->info("MP Raw Response: ".$response);
      $obj = json_decode($response);
      if(isset($obj->payload) && isset($obj->payload->url)){
        $url = $this->getSystemValue('checkout_url').$obj->payload->url;
      }
    }catch(\Exception $err){
      $this->logger->info("[Error] Sending POST: ".$err->getMessage());
    }
    return $url;
  }

  /**
   * Get system value
   *
   * @param string $field
   * @return string
   */
  private function getSystemValue($field){
    return $this->scope->getValue($this->systemPath.$field,$this->_scopeInterface);
  }

  /**
   * Get customer data from logged in
   *
   * @param string $field
   * @return string
   */
  private function getCustomerData($field){
    return $this->customerSessionData->getData($field);
  }

  /**
   * Get customer from id
   *
   * @param int $id
   * @return \Magento\Customer\Model\CustomerFactory
   */
  private function getCustomerById($id){
    return $this->_customer->create()->load($id);
  }

  /**
   * Get price formatted for MP service
   *
   * @param string $price
   * @return float
   */
  public function formatPrice($price){
    return (float)number_format($price,1,'.','');
  }

}
