<?php
namespace Formax\GreatBuy\Controller\Validate;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

class Index extends Action{

  /**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
  protected $scope;
  /**
   * @var \Magento\Store\Model\ScopeInterface
   */
  protected $_scopeInterface = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
  /**
   * @var string
   */
  protected $systemPath = 'dccp_endpoint/greatbuy/';
  /**
   * @var \Magento\Framework\Controller\Result\JsonFactory
   */
  protected $resultJsonFactory;
  /**
   * @var \Magento\Framework\App\ResourceConnection
   */
  protected $_resourceConnection;
  /**
  * @var \Magento\Framework\HTTP\ZendClientFactory
  */
  protected $_httpClientFactory;
  /**
   * @var \Formax\GreatBuy\Logger\Logger
   */
   protected $logger;

    const BASE_CURRENCY = 'CLP';

    /**
     * @var CurrencyFactory
     */
    protected $_currencyFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

  /**
   * @param Context $context
   * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
   * @param \Magento\Framework\App\ResourceConnection $resourceConnection
   * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
   * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
   * @param \Formax\GreatBuy\Logger\Logger $logger
   * @param CurrencyFactory $currencyFactory
   * @param StoreManagerInterface $storeManager
   */
  public function __construct(
      Context $context,
      \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
      \Magento\Framework\App\ResourceConnection $resourceConnection,
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
      \Formax\GreatBuy\Logger\Logger $logger,
      CurrencyFactory $currencyFactory,
      StoreManagerInterface $storeManager
  ) {
      $this->resultJsonFactory = $resultJsonFactory;
      $this->_resourceConnection = $resourceConnection;
      $this->scope = $scopeConfig;
      $this->_httpClientFactory = $httpClientFactory;
      $this->logger = $logger;
      $this->_currencyFactory = $currencyFactory->create();
      $this->_storeManager = $storeManager;
      parent::__construct($context);
  }

  public function execute(){
    $result = $this->resultJsonFactory->create();

    $utmRate = $this->getCLPValue();

    $forgb = $this->getSystemValue('utmforgb');
    $lower = $this->getSystemValue('minimumutm');
    $higher = $this->getSystemValue('maximumutm');

    $currencyCode = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();

    if ($currencyCode == "USD") {
        // Convert UTM rate in CLP by default to USD
        $utmRate = round($utmRate / $this->getUSDRate(), 2);
    }

    $response = [
        'lower' => $utmRate * $lower,
        'higher' => $utmRate * $higher,
        'forgb' => $utmRate * $forgb
    ];

    $result->setData($response);
    return $result;
  }

  /**
   * Get CLP value of the day
   *
   * @param array $ids
   * @return string
   * @throws NoSuchEntityException
   * @throws LocalizedException
   */
  public function getCLPValue()
  {
    $rate = 1.0;

    try {
        $endpoint = $this->getSystemValue('currency_endpoint');
        $client = $this->_httpClientFactory->create();
        $client->setUri($endpoint);
        $response = $client->request(\Zend_Http_Client::GET)->getBody();
        $obj = json_decode($response);
        foreach($obj->payload as $data){
            if($data->moneda == 'UTM'){
                $rate = round($data->valorPeso, $data->decimales);
            }
        }
    } catch(\Exception $err) {
        $this->logger->info("UTM Price in the day: ".$err->getMessage());
    }

    return $rate;
  }

  public function getUSDRate()
  {
      $rate = 1.0;

      try {
          $endpoint = $this->getSystemValue('currency_endpoint');
          $client = $this->_httpClientFactory->create();
          $client->setUri($endpoint);
          $response = $client->request(\Zend_Http_Client::GET)->getBody();
          $obj = json_decode($response);

          foreach($obj->payload as $data){
              if ($data->moneda == 'USD'){
                  $rate = round($data->valorPeso, $data->decimales);
              }
          }
      } catch(\Exception $err) {
          $this->logger->info("USD Price in the day: ".$err->getMessage());
      }

      return $rate;
  }

    /**
     * Get system value
     *
     * @param string $field
     * @return string
     */
    private function getSystemValue($field){
        return $this->scope->getValue($this->systemPath.$field,$this->_scopeInterface);
    }

}