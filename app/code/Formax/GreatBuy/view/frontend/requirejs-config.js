var config = {
    map: {
        '*': {
            'greatbuy-init': 'Formax_GreatBuy/js/greatbuy-init',
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/view/payment/default': {
                'Formax_GreatBuy/js/view/payment/default': true
            }
        }
    }
};
