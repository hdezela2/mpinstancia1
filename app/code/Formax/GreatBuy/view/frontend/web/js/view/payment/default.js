define([
  'jquery',
  'Formax_GreatBuy/js/action/greatbuy-modal',
  'Magento_Checkout/js/model/payment/additional-validators',
  'Magento_Checkout/js/action/redirect-on-success'
], function(
    $,
    gbModal,
    additionalValidators,
    redirectOnSuccessAction
  ){

  'use strict';

  var mixin = {

    /**
     * Place order.
     */
    placeOrder: function (data, event) {
        var self = this;
        var config = gbModal.getUtils();

        config.callback = function(){
          self.basePlaceOrder(data, event);
        };
        if(config.sellerId){
          gbModal.validateGB(config);
        }
        
        return false;
        
    },

    /**
     * Original place order.
     */
    basePlaceOrder: function(data, event){
        var self = this;
        if (event) {
          event.preventDefault();
        }
        if (this.validate() && additionalValidators.validate()) {
          this.isPlaceOrderActionAllowed(false);
          this.getPlaceOrderDeferredObject().fail(function () {
              self.isPlaceOrderActionAllowed(true);
          }).done(function () {
              self.afterPlaceOrder();
              if (self.redirectAfterPlaceOrder) {
                  redirectOnSuccessAction.execute();
              }
          });
          return true;
        }
        return false;
    }

  };

  return function (target) {
      return target.extend(mixin);
  };
    
});