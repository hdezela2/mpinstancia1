define([
        'jquery',
        'Formax_GreatBuy/js/action/greatbuy-modal',
        'mage/translate'
    ], function($, gbModal)
    {
        var GreatBuy =
            {
                initProcess: function(config)
                {
                    gbModal.initialize(config);
                    gbModal.createModal();

                    $(document).on('click', config.cartButton, function(event)
                    {
                        event.preventDefault();
                        var form = $(this).closest('form');

                        var sellerId = form.find('input[name="wk-mp-seller-id[]"]').val(),
                            purchaseUnit = form.find('select[name="unit_code"]').val(),
                            total = form.find('input[name="wk-mp-crt-sbt"]').val();

                        var obj =
                            {
                                callback: function()
                                {
                                    form.submit();
                                },
                                sellerId: sellerId,
                                purchaseUnit: purchaseUnit,
                                cartTotal: Number(total)
                            };

                        //Validate GreatBuy uses true if will be triggered in cart.
                        gbModal.validateGB(obj, true);

                    });
                }
            };

        return {
            'greatbuy-init': GreatBuy.initProcess
        };
    }
);
