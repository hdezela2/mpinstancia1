<?php
/**
 *
 * @category   MaxMage
 * @author     MaxMage Core Team <maxmagedev@gmail.com>
 * @date       3/14/2018
 * @copyright  Copyright © 2018 MaxMage. All rights reserved.
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @file       Data.php
 */

namespace Formax\GreatBuy\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    const MODULE_ENABLED = 'dccp_endpoint/greatbuymultishipping/enabled';
    const MINIMUM_UTM_SHIPPING = 'dccp_endpoint/greatbuymultishipping/minimumutmshipping';
    const UTM_FOR_GB_SHIPPING   = 'dccp_endpoint/greatbuymultishipping/utmforgbshipping';
    const MAXIMUM_UTM_SHIPPING   = 'dccp_endpoint/greatbuymultishipping/maximumutmshipping';
    const MINIMUM_UTM = 'dccp_endpoint/greatbuy/minimumutm';
    const MAXIMUM_UTM = 'dccp_endpoint/greatbuy/maximumutm';
    const UTM_FOR_GB = 'dccp_endpoint/greatbuy/utmforgb';
    const MULTI_CHECKOUT_MINIMUM_UTM = 'dccp_endpoint/greatbuy/multicheckoutminimumutm';
    const CURRENCY_ENDPOINT = 'dccp_endpoint/greatbuy/currency_endpoint';

    /** @var StoreManagerInterface */
    protected $_storeManager;

    /** @var ZendClientFactory */
    protected $_httpClientFactory;

    public function __construct(
        Context $context,
        ZendClientFactory $httpClientFactory,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_httpClientFactory = $httpClientFactory;
    }

    /**
     * Get CLP value of the day
     *
     * @param array $ids
     * @return string
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function getCLPValue()
    {
        $rate = 1.0;
        try {
            $endpoint = $this->getConfig(self::CURRENCY_ENDPOINT);
            $client = $this->_httpClientFactory->create();
            $client->setUri($endpoint);
            $response = $client->request(\Zend_Http_Client::GET)->getBody();
            $obj = json_decode($response);
            foreach($obj->payload as $data){
                if($data->moneda == 'UTM'){
                    $rate = round($data->valorPeso, $data->decimales);
                }
            }
        } catch(\Exception $err) {
            $this->logger->info("UTM Price in the day: ".$err->getMessage());
        }
        return $rate;
    }

    public function getUSDRate()
    {
        $rate = 1.0;
        try {
            $endpoint = $this->getConfig(self::CURRENCY_ENDPOINT);
            $client = $this->_httpClientFactory->create();
            $client->setUri($endpoint);
            $response = $client->request(\Zend_Http_Client::GET)->getBody();
            $obj = json_decode($response);

            foreach($obj->payload as $data){
                if ($data->moneda == 'USD'){
                    $rate = round($data->valorPeso, $data->decimales);
                }
            }
        } catch(\Exception $err) {
            $this->logger->info("USD Price in the day: ".$err->getMessage());
        }
        return $rate;
    }

    /**
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function getCurrencyCode()
    {
        return $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
    }

    public function isModuleEnabled()
    {
        return $this->getConfig(self::MODULE_ENABLED);
    }

    public function minimumUtmShipping()
    {
        return $this->getConfig(self::MINIMUM_UTM_SHIPPING);
    }

    public function  utmforgbShipping()
    {
        return $this->getConfig(self:: UTM_FOR_GB_SHIPPING );
    }

    public function getMinimumUTMConfig()
    {
        return $this->getConfig(self::MINIMUM_UTM);
    }

    public function getMaximumUTMConfig()
    {
        return $this->getConfig(self::MAXIMUM_UTM);
    }

    public function getUTMForGbConfig()
    {
        return $this->getConfig(self::UTM_FOR_GB);
    }

    public function getMultiCheckoutMinimumUtmConfig()
    {
        return $this->getConfig(self::MULTI_CHECKOUT_MINIMUM_UTM);
    }

    /**
     * @param $configPath
     * @return mixed
     */
    protected function getConfig($configPath)
    {
        return $this->scopeConfig->getValue($configPath, ScopeInterface::SCOPE_STORE);
    }

}

