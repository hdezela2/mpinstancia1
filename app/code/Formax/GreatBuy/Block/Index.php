<?php
namespace Formax\GreatBuy\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;

class Index extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scope;

    /**
     * @var string
     */
    protected $systemPath = 'dccp_endpoint/greatbuy/';

    /**
     * @var \Magento\Store\Model\ScopeInterface
     */
    protected $interface = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * View constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        array $data = []
    ){
        $this->scope = $context->getScopeConfig();
        parent::__construct($context, $data);
        $this->_storeManager = $storeManager;
    }

    /**
     * Get higher UTM value
     * @return string
     */
    public function getHigherUTM(){
        return $this->getSystemValue('utmforgb');
    }

    /**
     * Get lower UTM value
     * @return string
     */
    public function getLowerUTM(){
        return $this->getSystemValue('minimumutm');
    }

    /**
     * Get higher UTM value
     * @return string
     */
    public function getHigherLimitUTM()
    {
        return $this->getSystemValue('maximumutm');
    }

    /**
     * Get system value
     *
     * @param string $field
     * @return string
     */
    private function getSystemValue($field){
        return $this->scope->getValue($this->systemPath.$field,$this->interface);
    }

    public function getWebSite()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }
}
