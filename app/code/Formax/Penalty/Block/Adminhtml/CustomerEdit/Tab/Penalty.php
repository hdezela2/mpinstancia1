<?php

namespace Formax\Penalty\Block\Adminhtml\CustomerEdit\Tab;

class Penalty extends \Magento\Backend\Block\Template
{
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var \Formax\Penalty\Model\ServiceData
     */
    protected $serviceData;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $currentCustomer;

    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var string
     */
    protected $_template = 'tab/penalties.phtml';
    private bool $showTab;

    /**
     * View constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Formax\Penalty\Model\ServiceData $serviceData
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Formax\Penalty\Model\ServiceData $serviceData,
        array $data = []
    ) {
        parent::__construct($context, $data);
        
        $this->storeManager = $storeManager;
        $this->serviceData = $serviceData;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->_coreRegistry = $registry;

        $customerId = $this->getCustomerId();
        if ((int)$customerId > 0) {
            $customer = $this->customerRepositoryInterface->getById($customerId);
            if ($customer) {
                $this->currentCustomer = $customer;
                $isSeller = $customer->getCustomAttribute('is_vendor_group') ?
                    (int)$customer->getCustomAttribute('is_vendor_group')->getValue() : 0;
                $this->showTab = $isSeller === 1 ? true : false;
            }
        }
    }

    /**
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(
            \Magento\Customer\Controller\RegistryConstants::CURRENT_CUSTOMER_ID
        );
    }

    /**
     * Retrieve penalties info from session or web service
     * 
     * @return array
     */
    public function getPenalties()
    {
        $customer = $this->currentCustomer;
        $result = null;

        if ($customer) {
            $agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
                $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : '';
            $userId = $customer->getCustomAttribute('user_rest_id') ?
                $customer->getCustomAttribute('user_rest_id')->getValue() : '';
            $token = $customer->getCustomAttribute('user_rest_atk') ?
                $customer->getCustomAttribute('user_rest_atk')->getValue() : '';
            
            if ($agreementId !== '' && $userId !== '') {
                $result = $this->serviceData->getPenaltyData(trim($userId), trim($agreementId));
            }
        }
        
        return $result;
    }

    /**
     * Retrieve current agreement and seller info
     * 
     * @return array
     */
    public function getCurrentSellerAndAgreement()
    {
        $customer = $this->currentCustomer;
        $result = null; 

        if ($customer) {
            $sellerId = $customer->getCustomAttribute('user_rest_id') ?
                $customer->getCustomAttribute('user_rest_id')->getValue() : '';
            $sellerName = $customer->getCustomAttribute('wkv_dccp_business_name') ?
                $customer->getCustomAttribute('wkv_dccp_business_name')->getValue() : '';
            $agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
                $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : '';
            
            if ($sellerId && $sellerId && $agreementId) {
                $result = $this->serviceData->getCurrentSellerAndAgreement(trim($sellerName), trim($agreementId), trim($sellerId));
            } else {
                $this->setTemplate('tab/error_message.phtml');
            }
        }
        
        return $result;
    }

    /**
     * Get Url media files
     * 
     * @return string
     */
    public function getUrlFile()
    {
        $urlFile = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $urlFile . 'chilecompra/files';
    }

    /**
     * Get Url media icons
     * 
     * @return string
     */
    public function getUrlIcons()
    {
        $urlFile = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $urlFile . 'icons/';
    }

    /**
     * Get file extension
     * 
     * @return string
     */
    public function getExtFile($file)
    {
        if ($file) {
            $str = substr(strrchr($file, '.'), 1);
            return $str;
        }

        return '';
    }
}
