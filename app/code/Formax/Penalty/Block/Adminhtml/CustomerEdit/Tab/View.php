<?php

namespace Formax\Penalty\Block\Adminhtml\CustomerEdit\Tab;

use Magento\Ui\Component\Layout\Tabs\TabInterface;
use Magento\Backend\Block\Widget\Form;
use Magento\Backend\Block\Widget\Form\Generic;

class View extends Generic implements TabInterface
{
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var bool
     */
    protected $showTab = false;

    /**
     * @var \Formax\Penalty\Model\Config\Source\PenaltyType
     */
    protected $penaltyType;

    /**
     * @var \Magento\Framework\Data\FormFactory
     */
    protected $formFactory;

    /**
     * @var \Formax\Penalty\Model\Config\Source\Status
     */
    protected $status;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var string
     */
    protected $agreementId;

    /**
     * @var string
     */
    protected $sellerId;

    /**
     * @var string
     */
    protected $sellerName;

    /**
     * View constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Formax\Penalty\Model\Config\Source\PenaltyType
     * @param \Formax\Penalty\Model\Config\Source\Status $status
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Backend\Model\UrlInterface $urlBuilder
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Formax\Penalty\Model\Config\Source\PenaltyType $penaltyType,
        \Formax\Penalty\Model\Config\Source\Status $status,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Backend\Model\UrlInterface $urlBuilder,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $formFactory,
            $data
        );
        
        $this->urlBuilder = $urlBuilder;
        $this->formFactory = $formFactory;
        $this->penaltyType = $penaltyType;
        $this->status = $status;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->_coreRegistry = $registry;

        $customerId = $this->getCustomerId();
        if ((int)$customerId > 0) {
            $customer = $this->customerRepositoryInterface->getById($customerId);
            if ($customer) {
                $isSeller = $customer->getCustomAttribute('is_vendor_group') ?
                    (int)$customer->getCustomAttribute('is_vendor_group')->getValue() : 0;
                $this->agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
                    $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : '';
                $this->sellerId = $customer->getCustomAttribute('user_rest_id') ?
                    $customer->getCustomAttribute('user_rest_id')->getValue() : '';
                $this->sellerName = $customer->getCustomAttribute('wkv_dccp_business_name') ?
                    $customer->getCustomAttribute('wkv_dccp_business_name')->getValue() : '';
                $this->showTab = $this->agreementId && $this->sellerId && $this->sellerName ? true : false;
            }
        }
    }

    /**
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(
            \Magento\Customer\Controller\RegistryConstants::CURRENT_CUSTOMER_ID
        );
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Penalties');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Penalties');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        if ($this->getCustomerId()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        if ($this->getCustomerId()) {
            return !$this->showTab;
        }
        return true;
    }

    /**
     * Tab class getter
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return '';
    }

    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return false;
    }

    public function initForm()
    {
        if (!$this->canShowTab()) {
            return $this;
        }

        if (!$this->showTab) {
            return $this;
        }

        /**@var \Magento\Framework\Data\Form $form */
        $form = $this->formFactory->create();
        $form->setHtmlIdPrefix('formax_penalties_');
        $customerId = $this->_coreRegistry->registry($this->getCustomerId());
        $currentDate = new \DateTime('now +1 day');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Penalty Form')]
        );

        $fieldset->addField(
            'id',
            'hidden',
            [
                'name' => 'id',
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'penalty_id',
            'hidden',
            [
                'name' => 'penalty_id',
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'dccp_seller_id',
            'hidden',
            [
                'name' => 'dccp_seller_id',
                'value' => $this->sellerId,
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'agreement_id',
            'hidden',
            [
                'name' => 'agreement_id',
                'value' => $this->agreementId,
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'penalty_code',
            'text',
            [
                'name' => 'penalty_code',
                'label' => __('Code'),
                'title' => __('Code'),
                'data-form-part' => $this->getData('target_form'),
                'readonly' => true
            ]
        );
        $fieldset->addField(
            'penalty_type_id',
            'select',
            [
                'name' => 'penalty_type_id',
                'label' => __('Type'),
                'title' => __('Type'),
                'values' => $this->penaltyType->toOptionArray(),
                'class' => 'select required-entry',
                'required' => true,
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'resolution_number',
            'text',
            [
                'name' => 'resolution_number',
                'label' => __('Resolution Number'),
                'title' => __('Resolution Number'),
                'class' => 'required-entry',
                'required' => true,
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'required' => true,
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'start_date',
            'date',
            [
                'name' => 'start_date',
                'label' => __('Start Date'),
                'title' => __('Start Date'),
                'required' => true,
                'readonly' => true,
                'date_format' => 'Y-m-d',
                'class' => 'admin__control-text',
                'data-form-part' => $this->getData('target_form'),
                'after_element_html' => '<script>
                require(["jquery","mage/calendar"], function($){
                    $("#formax_penalties_start_date").calendar({
                        minDate: "' . $currentDate->format('Y-m-d') . '",
                        dateFormat:"yyyy-mm-dd",
                        yearRange: "-120y:c+nn",
                        buttonText: "Selecciona fecha", changeMonth: true, changeYear: true, showOn: "both",
                        monthNames: ["Enero","Febrero","Marzo","Abril","May","Junio","Julio",
                        "Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
                        monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
                        dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
                        dayNamesShort: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"],
                        dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"],
                        buttonImage: "'. $this->getViewFileUrl("Magento_Theme::calendar.png") . '"
                    });
                });
              </script>'
            ]
        );
        $fieldset->addField(
            'penalty_days',
            'text',
            [
                'name' => 'penalty_days',
                'label' => __('Days'),
                'title' => __('Days'),
                'required' => true,
                'class' => 'validate-greater-than-zero',
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'include_holidays',
            'radios',
            [
                'name' => 'include_holidays',
                'label' => __('Include'),
                'title' => __('Include'),
                'class' => 'admin__control-radio',
                'values' => [
                    ['value' => 0, 'label' => __('Bussines Days')],
                    ['value' => 1, 'label' => __('Holidays')]
                ],
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'file',
            'file', 
            [
                'name' => 'file',
                'label' => __('Upload File'),
                'title'  => __('Upload File'),
                'after_element_html' => '<div style="margin-top:5px"><label style="width:100%;">
                    Allowed File Type : [pdf]
                </label></div><div class="manage-files">
                <a class="file-chilecompra icon-file file" href="javascript:;" target="_blank">' . __('No File') . '</a></div>',
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'delete_file',
            'checkbox', 
            [
                'name' => 'delete_file',
                'label' => __('Delete File'),
                'title'  => __('Delete File'),
                'note' => __('Check this option if you want to delete the file'),
                'data-form-part' => $this->getData('target_form')
            ]
        );
        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Status'),
                'title' => __('Status'),
                'values' => $this->status->toOptionArray(),
                'class' => 'select required-entry',
                'required' => true,
                'data-form-part' => $this->getData('target_form')
            ]
        ); 

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare the layout.
     *
     * @return $this
     */
    public function getFormHtml()
    {
        $html = parent::getFormHtml();
        $html .= $this->getLayout()->createBlock(
            'Formax\Penalty\Block\Adminhtml\CustomerEdit\Tab\Penalty'
        )->toHtml();

        return $html;
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->canShowTab()) {
            $this->initForm();

            return parent::_toHtml();
        } else {
            return '';
        }
    }
}