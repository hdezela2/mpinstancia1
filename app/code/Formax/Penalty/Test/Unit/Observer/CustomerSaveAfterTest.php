<?php

namespace Formax\Penalty\Test\Unit\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Formax\Penalty\Model\PenaltyFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Formax\Penalty\Helper\Data;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Formax\Penalty\Model\Config\Source\PenaltyType;
use Formax\StatusChange\Helper\Data as HelperStatusSeller;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Backend\Model\Auth\Session as AuthSession;
use Psr\Log\LoggerInterface;
use PHPUnit\Framework\TestCase;
use Formax\Penalty\Observer\CustomerSaveAfter;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

class CustomerSaveAfterTest extends TestCase
{
    /**
     * @var CustomerSaveAfter
     */
    private $observer;

    /**
     * @var ManagerInterface
     */
    private $messageManagerMock;
    
    /**
     * @var PenaltyFactory
     */
    private $penaltyFactoryMock;
    
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepositoryMock;
    
    /**
     * @var Data
     */
    private $helperMock;
    
    /**
     * @var PenaltyType
     */
    private $penaltyTypeMock;
    
    /**
     * @var UploaderFactory
     */
    private $fileUploaderFactoryMock;
    
    /**
     * @var Filesystem
     */
    private $filesystemMock;
    
    /**
     * @var LoggerInterface
     */
    private $loggerMock;

    /**
     * @var TimezoneInterface
     */
    private $timezoneMock;

    /**
     * @var AuthSession
     */
    private $authSessionMock;

    /**
     * @var RequestInterface
     */
    private $requestMock;
    
    /**
     * @var HelperStatusSeller
     */
    private $helperStatusSellerMock;

    /**
     * @var \Magento\Framework\Event\Observer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $eventObserver;

    /**
     * @var \Magento\Framework\App\RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $request;

    /**
     * @var \Magento\Framework\App\RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestFiles;

    /**
     * @var \Magento\Customer\Api\Data\CustomerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $customer;

    /**
     * Test setup
     */
    protected function setUp()
    {
        $this->messageManagerMock = $this->getMockBuilder(ManagerInterface::class)
            ->getMockForAbstractClass();
        $this->penaltyFactoryMock = $this->getMockBuilder(PenaltyFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->customerRepositoryMock = $this->getMockBuilder(CustomerRepositoryInterface::class)
            ->getMockForAbstractClass();
        $this->helperMock = $this->getMockBuilder(Data::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->penaltyTypeMock = $this->getMockBuilder(PenaltyType::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->fileUploaderFactoryMock = $this->getMockBuilder(UploaderFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->filesystemMock = $this->getMockBuilder(Filesystem::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->getMockForAbstractClass();
        $this->timezoneMock = $this->getMockBuilder(TimezoneInterface::class)
            ->getMockForAbstractClass();
        $this->authSessionMock = $this->getMockBuilder(AuthSession::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->requestMock = $this->getMockForAbstractClass(
            RequestInterface::class,
            [],
            '',
            false
        );
        $this->helperStatusSellerMock = $this->getMockBuilder(HelperStatusSeller::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        $this->observer = new CustomerSaveAfter(
            $this->messageManagerMock,
            $this->penaltyFactoryMock,
            $this->customerRepositoryMock,
            $this->helperMock,
            $this->penaltyTypeMock,
            $this->fileUploaderFactoryMock,
            $this->filesystemMock,
            $this->loggerMock,
            $this->timezoneMock,
            $this->authSessionMock,
            $this->requestMock,
            $this->helperStatusSellerMock,
            true
        );
    }

    /**
     * @return void
     */
    public function testPenalty()
    {
        $postData = [
            'penalty_type_id' => '2',
            'dccp_seller_id' => '21099',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
            'agreement_id' => '5800254',
            'penalty_code' => 'Terrmino anticipado',
            'status' => '1',
            'resolution_number' => '234324-SA',
            'start_date' => date('Y-m-dTH:i:s'),
            'include_holidays' => '3',
            'penalty_days' => '1'
        ];

        $this->eventObserver = $this->createPartialMock(
            \Magento\Framework\Event\Observer::class,
            ['getCustomer', 'getRequest']
        );
        $this->request = $this->getMockForAbstractClass(
            \Magento\Framework\App\RequestInterface::class,
            [],
            '',
            false,
            false,
            true,
            ['getPostValue']
        );
        $this->customer = $this->getMockForAbstractClass(
            \Magento\Customer\Api\Data\CustomerInterface::class,
            [],
            '',
            false
        );
        
        $this->eventObserver->expects($this->once())
            ->method('getRequest')
            ->willReturn($this->request);
        $this->request->expects($this->once())
            ->method('getPostValue')
            ->willReturn($postData);
        $this->eventObserver->expects($this->once())
            ->method('getCustomer')
            ->willReturn($this->customer);
        $this->observer->execute($this->eventObserver);
    }
}