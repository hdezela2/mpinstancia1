<?php

namespace Formax\Penalty\Model;

use Magento\Framework\Model\AbstractModel;

class Penalty extends AbstractModel
{
    /**
     * @var string
     */
    protected $_cacheTag = 'dccp_penalty';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'dccp_penalty';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\Penalty\Model\ResourceModel\Penalty::class);
    }
}