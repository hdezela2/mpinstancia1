<?php

namespace Formax\Penalty\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Penalty extends AbstractDb
{
    public function _construct()
    {
        $this->_init('dccp_penalty', 'entity_id');
    }
}