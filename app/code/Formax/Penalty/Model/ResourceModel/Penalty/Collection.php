<?php

namespace Formax\Penalty\Model\ResourceModel\Penalty;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Formax\Penalty\Model\Penalty',
            'Formax\Penalty\Model\ResourceModel\Penalty'
        );
    }
}
