<?php

namespace Formax\Penalty\Model\ResourceModel\Seller\Grid;

class Collection extends \Webkul\Marketplace\Model\ResourceModel\Seller\Grid\Collection
{
    /**
     * Join store relation table if there is store filter
     *
     * @return void
     */
    protected function _renderFiltersBefore()
    {
        $joinTable = $this->getTable('customer_grid_flat');
        $this->getSelect()->join(
            $joinTable.' as cgf',
            'main_table.seller_id = cgf.entity_id',
            [
                'user_rest_id' => 'user_rest_id',
                'name' => 'name',
                'email' => 'email',
                'billing_telephone' => 'billing_telephone',
                'billing_postcode' => 'billing_postcode',
                'billing_country_id' => 'billing_country_id',
                'billing_region' => 'billing_region',
                'website_id' => 'website_id',
                'wkv_dccp_state_details' => 'wkv_dccp_state_details',
                'wkv_dccp_rut' => 'wkv_dccp_rut'
            ]
        )->where('main_table.store_id = 0');
    }

    /**
     * This method is missing from original class
     * and this leads to strange SELECT condition when viewing sellers in admin.
     * We need this method to exist; nothing else
     * @param $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        return $this;
    }
}
