<?php

namespace Formax\Penalty\Model;

class ServiceData
{
    /**
     * @var \Formax\Penalty\Model\penalityCollectionFactory
     */
    protected $penalityCollectionFactory;

    /**
     * @var \Formax\AgreementInfo\Model\AgreementFactory
     */
    protected $agreementFactory;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @param \Formax\Penalty\Model\ResourceModel\Penalty\CollectionFactory $penalityCollectionFactory
     * @param \Formax\AgreementInfo\Model\AgreementFactory $agreementFactory
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     */
    public function __construct(
        \Formax\Penalty\Model\ResourceModel\Penalty\CollectionFactory $penalityCollectionFactory,
        \Formax\AgreementInfo\Model\AgreementFactory $agreementFactory,
        \Magento\Framework\DataObjectFactory $dataObjectFactory
    ) {
        $this->agreementFactory = $agreementFactory;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->penalityCollectionFactory = $penalityCollectionFactory;
    }

    /**
     * @param string $sellerId
     * @param string $agreementId
     * @return mixed \Formax\Penalty\Model\ResourceModel\Penalty\CollectionFactory
     */
    public function getPenaltyData($sellerId, $agreementId)
    {
        $result = null;
        if ($sellerId !== '' && $agreementId !== '') {
            $penalty = $this->penalityCollectionFactory->create();
            $result = $penalty->addFieldToFilter('agreement_id', $agreementId)
                ->addFieldToFilter('dccp_seller_id', $sellerId);
        }

        return $result;
    }

    /**
     * @param string $sellerName
     * @param string $agreementId
     * @param string $sellerId
     * @param \Magento\Framework\Data\Object
     */
    public function getCurrentSellerAndAgreement($sellerName, $agreementId, $sellerId)
    {
        $result = null;

        if ($sellerName != '') {
            $dataObject = $this->dataObjectFactory->create();
            $dataObject->setData('seller_name', $sellerName);
            
            if ($agreementId && $sellerId) {
                $agreement = $this->agreementFactory->create();
                $agreementData = $agreement->load($agreementId, 'agreement_id');
                $dataObject->setData('seller_id', $sellerId);
                $dataObject->setData('agreement_id', $agreementId);
                if ($agreementData) {
                    $dataObject->setData('agreement_name', $agreementData->getAgreementName());
                } else {
                    $dataObject->setData('agreement_name', '');
                }
            }

            $result = $dataObject;
        }
        
        return $result;
    }
}