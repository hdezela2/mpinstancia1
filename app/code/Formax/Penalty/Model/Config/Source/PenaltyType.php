<?php

namespace Formax\Penalty\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class PenaltyType implements ArrayInterface
{
    const EARLY_TERM  = 1;
    const CHARGE_GUARANTEE = 2;
    const WARNING = 3;
    const TEMPORY_SUSPENSION = 4;
    const PRODUCTS_BLOCKING = 5;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            '' => __('Select a penalty type'),
            self::EARLY_TERM => __('Early term'),
            self::CHARGE_GUARANTEE => __('Payment guarantee ticket'),
            self::WARNING => __('Warning'),
            self::TEMPORY_SUSPENSION => __('Tempory Suspension'),
            self::PRODUCTS_BLOCKING => __('Product Lock')
        ];

        return $options;
    }
}
