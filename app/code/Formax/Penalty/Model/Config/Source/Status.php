<?php

namespace Formax\Penalty\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Status implements ArrayInterface
{
    const ENABLE = 1;
    const DISABLE = 2;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            self::ENABLE => __('Enabled'),
            self::DISABLE => __('Deleted')
        ];

        return $options;
    }
}
