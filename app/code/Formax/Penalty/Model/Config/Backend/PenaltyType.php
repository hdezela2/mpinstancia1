<?php

namespace Formax\Penalty\Model\Config\Backend;

use Magento\Framework\Option\ArrayInterface;

class PenaltyType implements ArrayInterface
{
    const EARLY_TERM  = 1;
    const CHARGE_GUARANTEE = 2;
    const WARNING = 3;
    const TEMPORY_SUSPENSION = 4;
    const PRODUCTS_BLOCKING = 5;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['value' => '', 'label' => __('Select a penalty type')],
            ['value' => self::EARLY_TERM, 'label' => __('Early term')],
            ['value' => self::CHARGE_GUARANTEE, 'label' => __('Payment guarantee ticket')],
            ['value' => self::WARNING, 'label' => __('Warning')],
            ['value' => self::TEMPORY_SUSPENSION, 'label' => __('Tempory Suspension')],
            ['value' => self::PRODUCTS_BLOCKING, 'label' => __('Product Lock')]
        ];

        return $options;
    }
}
