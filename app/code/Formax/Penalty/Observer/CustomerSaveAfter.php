<?php

namespace Formax\Penalty\Observer;

use Magento\Framework\Event\ObserverInterface;
use Formax\Penalty\Model\PenaltyFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Formax\Penalty\Helper\Data;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Formax\Penalty\Model\Config\Source\PenaltyType;
use Magento\Framework\Filesystem\Driver\File;
use Formax\StatusChange\Helper\Data as HelperStatusSeller;

class CustomerSaveAfter implements ObserverInterface
{   
    /**
     * @var int
     */
    public $statusEnable;

    /**
     * @var int
     */
    public $statusEarlyTerm;

    /**
     * @var int
     */
    public $statusTemporySuspension;

    /**
     * File Uploader factory.
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var Filesystem
     */
    protected $_mediaDirectory;

    /**
     * @var PenaltyFactory
     */
    protected $penaltyFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $_messageManager;

    /**
     * @var \Formax\Penalty\Helper\Data
     */
    protected $helper;

    /**
     * @var \Formax\Penalty\Model\Config\Source\PenaltyType
     */
    protected $penaltyType;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $file;

    /**
     * @var bool
     */
    protected $enableTest;

    /**
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param PenaltyFactory $penaltyFactoryFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param \Formax\Penalty\Helper\Data $helper
     * @param \Formax\Penalty\Model\Config\Source\PenaltyType $penaltyType
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param Filesystem $filesystem
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Framework\App\RequestInterface $request
     * @param HelperStatusSeller $helperStatusSeller
     * @param \Magento\Framework\Filesystem\Driver\File
     * @param bool $enableTest
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        PenaltyFactory $penaltyFactory,
        CustomerRepositoryInterface $customerRepository,
        \Formax\Penalty\Helper\Data $helper,
        \Formax\Penalty\Model\Config\Source\PenaltyType $penaltyType,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        Filesystem $filesystem,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\App\RequestInterface $request,
        HelperStatusSeller $helperStatusSeller,
        File $file,
        $enableTest = false
    ) {
        $this->authSession = $authSession;
        $this->timezone = $timezone;
        $this->_customerRepository = $customerRepository;
        $this->logger = $logger;
        $this->penaltyType = $penaltyType;
        $this->helper = $helper;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_messageManager = $messageManager;
        $this->penaltyFactory = $penaltyFactory;
        $this->request = $request;
        $this->file = $file;
        $this->enableTest = $enableTest;

        $statusSeller = $helperStatusSeller->getMagentoStatusSeller();
        $this->statusEnable = isset($statusSeller[1]) ? (int)$statusSeller[1] : null;
        $this->statusEarlyTerm = isset($statusSeller[6]) ? (int)$statusSeller[6] : null;
        $this->statusTemporySuspension = isset($statusSeller[4]) ? (int)$statusSeller[4] : null;
    }

    /**
     * admin customer save after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer = $observer->getCustomer();
        $postData = $observer->getRequest()->getPostValue();
        $authSession = $this->authSession->getUser();
        $fileUploaded = $this->enableTest ? [] : $this->request->getFiles();

        if ($this->statusEnable !== null && $this->statusEarlyTerm !== null && $this->statusTemporySuspension !== null) {
            if ($authSession && isset($postData['penalty_type_id']) && isset($postData['dccp_seller_id']) && isset($postData['description']) &&
                isset($postData['agreement_id']) && isset($postData['penalty_code']) && isset($postData['status'])
                && $postData['resolution_number']) {

                $penalty = null;
                $inlcudeHoliDays = 0;
                $dateIni = null;
                $fileExists = true;
                $resultFile = null;
                $b64Doc = '';
                $penaltyDays = 0;
                $dccpSellerId = $postData['dccp_seller_id'];
                $token = $authSession->getUserRestAtk();
                
                // Upload file
                if (isset($fileUploaded['file']['name']) && $fileUploaded['file']['name'] != '') {
                    try {
                        $target = $this->_mediaDirectory->getAbsolutePath('chilecompra/files/');
                        $uploader = $this->_fileUploaderFactory->create(['fileId' => 'file']);
                        $uploader->setAllowedExtensions(['pdf']);
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(true);
                        $resultFile = $uploader->save($target);

                        if (isset($resultFile['path']) && isset($resultFile['file']) && isset($resultFile['type'])) {
                            $path = rtrim($resultFile['path'], '/') . $resultFile['file'];
                            $b64Doc = base64_encode($this->file->fileGetContents($path));
                            $b64Doc = 'data:' . $resultFile['type'] . ';base64,' . $b64Doc;
                        }
                    } catch(\Exception $e) {
                        $this->logger->critical($e->getMessage());
                        $this->_messageManager->addError(
                            __('This file type is not allowed. Only Pdf')
                        );
                        return $this;
                    }
                }

                try {
                    $penaltyTypeArray = $this->penaltyType->toOptionArray();
                    $penaltyType = isset($penaltyTypeArray[$postData['penalty_type_id']]) ?
                            $penaltyTypeArray[$postData['penalty_type_id']] : '';
                    
                    if ($postData['penalty_type_id'] == PenaltyType::TEMPORY_SUSPENSION) {
                        $dateIni = new \DateTime($postData['start_date']);
                        $inlcudeHoliDays = isset($postData['include_holidays']) && $postData['include_holidays'] == '0' ? 0 : 1;
                        $penaltyDays = (int)$postData['penalty_days'];
                    }

                    // If Edit penalty and change status to deleted
                    if ((int)$postData['id'] > 0 && $postData['status'] == 2) {
                        $penalty = $this->penaltyFactory->create()->load((int)$postData['id']);

                        // Service delete penalty
                        $responseDel = $this->helper->deletePenalty(
                            $token,
                            $dccpSellerId,
                            $postData['agreement_id'],
                            $postData['penalty_code']
                        );
            
                        if (isset($responseDel['success']) && $responseDel['success'] == 'OK') {
                            // If penalty type was EARLY_TERM or TEMPORY_SUSPENSION but now is deleted
                            if ($penalty->getPenaltyTypeId() == PenaltyType::EARLY_TERM ||
                                $penalty->getPenaltyTypeId() == PenaltyType::TEMPORY_SUSPENSION) {
                                $customer->setCustomAttribute('is_vendor_group', 1);
                                $customer->setCustomAttribute('wkv_dccp_state_details', $this->statusEnable);
                                $this->_customerRepository->save($customer);
                                $this->helper->setStatusSeller($customer->getId(), 1);
                            }
                            $penalty->setStatus($postData['status']);
                            $penalty->save();

                            $this->_messageManager->addSuccess(
                                __('Penalty was deleted succesfully.')
                            );
                        } else {
                            $this->logger->critical('Error servicio eliminacion de sanciones');
                            $this->_messageManager->addError(
                                __("Penalty couldn't save.")
                            );
                        }

                        return $this;
                    }

                    //If file is empty
                    if ((int)$postData['id'] === 0 && (!isset($fileUploaded['file']['name'])
                        || (isset($fileUploaded['file']['name']) && $fileUploaded['file']['name'] == ''))) {
                        $this->logger->critical('Archivo vacío');
                        $this->_messageManager->addError(
                            __('You must upload a file.')
                        );
                        return $this;
                    }
                    
                    // If edit penalty files not uploaded but It is in DB, so load It
                    if ((!isset($fileUploaded['file']['name']) || (isset($fileUploaded['file']['name'])
                        && $fileUploaded['file']['name'] == '')) && (int)$postData['id'] > 0) {
                        $penalty = $this->penaltyFactory->create()->load((int)$postData['id']);
                        $penaltyTypeOriginal = $penalty->getPenaltyTypeId();
                        
                        if ($penalty->getFile() !== null && $penalty->getFile() !== '') {
                            $target = $this->_mediaDirectory->getAbsolutePath('chilecompra/files/');
                            $path = rtrim($target, '/') . $penalty->getFile();
                            $mimeType = mime_content_type($path);
                            $b64Doc = base64_encode($this->file->fileGetContents($path));
                            $b64Doc = 'data:' . $mimeType . ';base64,' . $b64Doc;
                        }
                    }

                    $payLoad = [
                        'idproveedor' => $dccpSellerId,
                        'idconveniomarco' => $postData['agreement_id'],
                        'idtiposancion' => $postData['penalty_type_id'],
                        'tiposancion' => $penaltyType,
                        'numeroresolucion' => $postData['resolution_number'],
                        'descripcion' => $postData['description'],
                        'fechainicio' => $dateIni !== null ? $dateIni->format('Y-m-d\TH:i:s') : '',
                        'dias' => $penaltyDays,
                        'eshabil' => $inlcudeHoliDays == '0' ? 1 : 0,
                        'adjunto' => $b64Doc
                    ];

                    if ((int)$postData['id'] > 0) {
                        $payLoad['id'] = $postData['penalty_id'];
                    }

                    // Call DCCP Webservice
                    $response = $this->helper->setPenaltyInfo(
                        $token,
                        $dccpSellerId,
                        $postData['agreement_id'],
                        $payLoad
                    );

                    if (isset($response['success']) && isset($response['payload']) && $response['success'] == 'OK') {

                        if ((int)$postData['id'] > 0) {
                            $penalty = $penalty === null ?
                                $this->penaltyFactory->create()->load((int)$postData['id']) : $penalty;
                            $penaltyTypeOriginal = $penalty->getPenaltyTypeId();
                        } else {
                            $penaltyTypeOriginal = '';
                            $dateCreated = isset($response['payload']['fechaingreso']) && $response['payload']['fechaingreso'] != ''
                            ? new \DateTime($response['payload']['fechaingreso']) : new \DateTime();
                            $penalty = $this->penaltyFactory->create();
                            $penalty->setPenaltyId($response['payload']['id']);
                            $penalty->setPenaltyCode($response['payload']['codigosancion']);
                            $penalty->setCreatedAt($dateCreated);
                        }
                        
                        $dateEnd = $postData['penalty_type_id'] == PenaltyType::TEMPORY_SUSPENSION && isset($response['payload']['fechatermino']) && $response['payload']['fechatermino'] != ''
                            ? new \DateTime($response['payload']['fechatermino']) : null;
                        $dateIni = $dateIni !== null ? $dateIni->format('Y-m-d') : null;
                        $dateEnd = $dateEnd != null ? $dateEnd->format('Y-m-d') : null;
                        $penalty->setDccpSellerId($response['payload']['idproveedor']);
                        $penalty->setAgreementId($response['payload']['idconveniomarco']);
                        $penalty->setPenaltyTypeId($response['payload']['idtiposancion']);
                        $penalty->setPenaltyType($penaltyType);
                        $penalty->setResolutionNumber($response['payload']['numeroresolucion']);
                        $penalty->setDescription($response['payload']['descripcion']);
                        $penalty->setStartDate($dateIni);
                        $penalty->setEndDate($dateEnd);
                        $penalty->setPenaltyDays($penaltyDays);
                        $penalty->setIncludeHolidays($inlcudeHoliDays);
                        $penalty->setStatus($postData['status']);

                        if ($resultFile !== null && isset($resultFile['file'])) {
                            $penalty->setFile($resultFile['file']);
                        }
                        $penalty->save();

                        // If penalty type is EARLY_TERM
                        if ($penalty->getPenaltyTypeId() == PenaltyType::EARLY_TERM) {
                            $customer->setCustomAttribute('is_vendor_group', 0);
                            $customer->setCustomAttribute('wkv_dccp_state_details', $this->statusEarlyTerm);
                            $this->_customerRepository->save($customer);
                            $this->helper->setStatusSeller($customer->getId(), 0);
                        } else {
                            // If penalty type was EARLY_TERM but now change
                            if ($penaltyTypeOriginal == PenaltyType::EARLY_TERM) {
                                $customer->setCustomAttribute('is_vendor_group', 1);
                                $customer->setCustomAttribute('wkv_dccp_state_details', $this->statusEnable);
                                $this->_customerRepository->save($customer);
                                $this->helper->setStatusSeller($customer->getId(), 1);
                            }
                        }

                        $this->_messageManager->addSuccess(
                            __('You have saved the penalty successfully.')
                        );
                    } else {
                        $this->logger->critical('Webservice response error');
                        $this->_messageManager->addError(
                            __('An error occurred when you trying to save the penalty.')
                        );
                    }
                } catch(\Exception $e) {
                    $this->logger->critical($e->getMessage());
                    $this->_messageManager->addError(
                        __('An error occurred when you trying to save the penalty.')
                    );
                }
            }
        } else {
            $errorMsg = 'Table dccp_seller_status does not status ENABLE, EARLY TERM and/or TEMPORARY SUSPENSION.';
            $this->logger->critical($errorMsg);
            $this->_messageManager->addError(
                __($errorMsg)
            );
        }

        return $this;
    }
}
