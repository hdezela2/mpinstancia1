<?php
 
namespace Formax\Penalty\Plugin\Block\Adminhtml\Customer\Tab\Edit;
 
class Tabs
{
    /**
     * @return bool
     */
    public function afterCanShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function afterIsHidden()
    {
        return true;
    }
}