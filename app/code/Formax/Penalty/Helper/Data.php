<?php

namespace Formax\Penalty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;

class Data extends AbstractHelper
{
    /**
     *
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     *
     * @var \Formax\Penalty\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;
    private Context $context;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Formax\Penalty\Logger\Logger $logger
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        Context $context,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Formax\Penalty\Logger\Logger $logger,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->storeManager = $storeManager;
        $this->httpClientFactory = $httpClientFactory;
        $this->logger = $logger;
        $this->context = $context;
        $this->jsonHelper = $jsonHelper;
        $this->resource = $resource;
        $this->connection = $this->resource->getConnection();

        parent::__construct($context);
    }

    /**
     * Set Penalty data from REST service
     *      
     * @param string $token
     * @param string $userId
     * @param string $agreementId
     * @param array $bodyParams
     * @return Zend_Http_Response
     */
    public function setPenaltyInfo($token, $userId, $agreementId, $bodyParams)
    {
        $response = null;

        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_penalties_data/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');
            
            if (!empty($endpoint) && !empty($token) && !empty($userId) && !empty($agreementId)
                && is_array($bodyParams) && count($bodyParams)) {
                $httpHeaders = [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . trim($token)
                ];

                $data = $this->jsonHelper->jsonEncode($bodyParams);
                $endpoint .= '/proveedor/convenio/sancion';
                $this->logger->info('setPenaltyInfo REQUEST: ' . $data);

                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                    'timeout' => 30]
                );
                $client->setHeaders($httpHeaders);
                $client->setRawData($data);
                $response = $client->request(\Zend_Http_Client::POST)->getBody();
                
                $this->logger->info('setPenaltyInfo RESPONSE: Seller ID: ' . $userId . ' Agreement ID: ' . $agreementId . ' Service Response: ' . $response);
                $response = $this->jsonHelper->jsonDecode($response);
            } else {
                throw new LocalizedException(__('Define endpoint and token penalties webservice'));
            }
        } catch(LocalizedException $e) {
            $this->logger->critical('setPenaltyInfo Seller ID: ' . $userId . ' Agreement ID: ' . $agreementId . ' Error message: ' . $e->getMessage());
        } catch(\Exception $e) {
            $this->logger->critical('setPenaltyInfo Seller ID: ' . $userId . ' Agreement ID: ' . $agreementId . ' Error message: ' . $e->getMessage());
        }

        return $response;
    }

    /**
     * Delete Penalty from REST service
     *      
     * @param string $token
     * @param string $userId
     * @param string $agreementId
     * @param string $penaltyCode
     * @return Zend_Http_Response
     */
    public function deletePenalty($token, $userId, $agreementId, $penaltyCode)
    {
        $response = null;

        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_penalties_delete/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');
            
            if (!empty($endpoint) && !empty($token) && !empty($userId)
                && !empty($agreementId) && !empty($penaltyCode)) {
                $httpHeaders = new \Zend\Http\Headers();
                $httpHeaders->addHeaders([
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . trim($token)
                ]);
                
                $endpoint .= '/proveedor/' . $userId . '/convenio/' . $agreementId . '/sancion/' . $penaltyCode;
                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(                    
                    [
                        'maxredirects' => 0,
                        'timeout' => 30
                    ]
                );

                $request = new \Zend\Http\Request();
                $request->setHeaders($httpHeaders);
                $request->setUri($endpoint);
                $request->setMethod(\Zend\Http\Request::METHOD_DELETE);
                $client = new \Zend\Http\Client();
                $options = [
                    'adapter'   => 'Zend\Http\Client\Adapter\Curl',
                    'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
                    'maxredirects' => 0,
                    'timeout' => 30
                ];
                $client->setOptions($options);
                $response = $client->send($request);
                $response = $response->getContent();

                $this->logger->info('deletePenalty RESPONSE: Seller ID: ' . $userId . ' Agreement ID: ' . $agreementId . ' Penalty Code: ' . $penaltyCode . ' Service Response: ' . $response);
                $response = $this->jsonHelper->jsonDecode($response);
            } else {
                throw new LocalizedException(__('Define endpoint and token delete penalties webservice'));
            }
        } catch(LocalizedException $e) {
            $this->logger->critical('deletePenalty Seller ID: ' . $userId . ' Agreement ID: ' . $agreementId . ' Penalty Code: ' . $penaltyCode . ' Error message: ' . $e->getMessage());
        } catch(\Exception $e) {
            $this->logger->critical('deletePenalty Seller ID: ' . $userId . ' Agreement ID: ' . $agreementId . ' Penalty Code: ' . $penaltyCode . ' Error message: ' . $e->getMessage());
        }

        return $response;
    }

    /**
     * Get file url media
     * 
     * @param string $file
     * @return string
     */
    public function getMediaBaseUrl($file)
    {
        $urlFile = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $urlFile . 'chilecompra/files'. $file;
    }

    /**
     * Set value to column is_seller into marketplace_userdata table
     * 
     * @param int $sellerId
     * @param int $value
     * @return bool
     */
    public function setStatusSeller($sellerId, $value)
    {
        $where = ['seller_id = ?' => (int)$sellerId];
        return $this->connection->update(
            $this->resource->getTableName('marketplace_userdata'),
            ['is_seller' => (int)$value],
            $where
        );
    }
}
