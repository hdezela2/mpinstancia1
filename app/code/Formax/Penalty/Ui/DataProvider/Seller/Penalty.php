<?php

namespace Formax\Penalty\Ui\DataProvider\Seller;

/**
 * Class Penalty
 */
class Penalty extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * Penalty Collection
     *
     * @var \Formax\Penalty\Model\ResourceModel\Penalty\CollectionFactory
     */
    public $collection;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepositoryInterface;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    private $marketplaceHelper;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Formax\Penalty\Model\ResourceModel\Penalty\CollectionFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Webkul\Marketplace\Helper\Data $marketplaceHelper
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Formax\Penalty\Model\ResourceModel\Penalty\CollectionFactory $collectionFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Webkul\Marketplace\Helper\Data $marketplaceHelper,
        array $meta = [],
        array $data = []
    ) {
        $this->marketplaceHelper = $marketplaceHelper;
        $this->customerRepositoryInterface = $customerRepositoryInterface;

        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        
        $customerId = $this->marketplaceHelper->getCustomerId();
        $customer = $this->customerRepositoryInterface->getById($customerId);
            
        if ($customer) {
            $agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
                $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : 0;
            $sellerId = $customer->getCustomAttribute('user_rest_id') ?
                $customer->getCustomAttribute('user_rest_id')->getValue() : 0;
                
            $collectionData = $collectionFactory->create()
                        ->addFieldToFilter('status', 1)
                        ->addFieldToFilter('dccp_seller_id', $sellerId)
                        ->addFieldToFilter('agreement_id', $agreementId);
            $this->collection = $collectionData;
        }
    }
}
