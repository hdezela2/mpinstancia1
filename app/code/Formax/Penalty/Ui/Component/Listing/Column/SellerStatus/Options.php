<?php

namespace Formax\Penalty\Ui\Component\Listing\Column\SellerStatus;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Options
 */
class Options implements OptionSourceInterface
{
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * @param \Magento\Eav\Model\Config $eavConfig
     */
    public function __construct(\Magento\Eav\Model\Config $eavConfig)
    {
        $this->eavConfig = $eavConfig;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $attributeCode = "wkv_dccp_state_details";
		$attribute = $this->eavConfig->getAttribute('customer', $attributeCode);
		$options = $attribute->getSource()->getAllOptions();
        $arr = [];
        
		foreach ($options as $option) {
		    if ($option['value'] > 0) {
		        $arr[] = $option;
		    }
        }
        
        return $arr;
    }
}