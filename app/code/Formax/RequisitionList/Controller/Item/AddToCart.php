<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Formax\RequisitionList\Controller\Item;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Api\CartManagementInterface;
use Magento\RequisitionList\Api\RequisitionListManagementInterface;
use Magento\RequisitionList\Model\Action\RequestValidator;
use Psr\Log\LoggerInterface;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Data\Form\FormKey;
use Magento\Checkout\Model\Cart;
use Magento\Catalog\Model\Product;
/**
 * Add specified items from requisition list to cart.
 */
class AddToCart extends \Magento\RequisitionList\Controller\Item\AddToCart
{
    protected $formKey;
    protected $cart;
    protected $product;
    private RequestValidator $requestValidator;
    private UserContextInterface $userContext;
    private LoggerInterface $logger;
    private RequisitionListManagementInterface $listManagement;
    private CartManagementInterface $cartManagement;
    private \Magento\Store\Model\StoreManagerInterface $storeManager;
    private \Magento\RequisitionList\Model\RequisitionList\ItemSelector $itemSelector;

    public function __construct(
        Context $context,
        RequestValidator $requestValidator,
        UserContextInterface $userContext,
        LoggerInterface $logger,
        RequisitionListManagementInterface $listManagement,
        CartManagementInterface $cartManagement,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\RequisitionList\Model\RequisitionList\ItemSelector $itemSelector,
        FormKey $formKey,
        Cart $cart,
        Product $product
    ) {
        $this->requestValidator = $requestValidator;
        $this->userContext = $userContext;
        $this->logger = $logger;
        $this->listManagement = $listManagement;
        $this->cartManagement = $cartManagement;
        $this->storeManager = $storeManager;
        $this->itemSelector = $itemSelector;
        $this->formKey = $formKey;
        $this->cart = $cart;
        $this->product = $product;
        parent::__construct(
            $context,
            $requestValidator,
            $userContext,
            $logger,
            $listManagement,
            $cartManagement,
            $storeManager,
            $itemSelector
        );
    }

    /**
     * @inheritdoc
     */

    /*public function execute()
    {
        $productId =1279;
        $params = array(
                    'mpassignproduct_id' => 27514,
                    'form_key' => $this->formKey->getFormKey(),
                    'product' => $productId,
                    'qty'   =>1
                );
        $product = $this->product->load($productId);
        $this->cart->addProduct($product, $params);
        $this->cart->save();
    }*/

    public function execute()
    {
        //echo "<pre>";
        //var_dump($this->getRequest());
        //exit;
        $result = $this->requestValidator->getResult($this->getRequest());
        if ($result) {
            return $result;
        }

        try {
             //@var \Magento\Framework\Controller\Result\Redirect $resultRedirect
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        } catch (\InvalidArgumentException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->_redirect(
                'requisition_list/requisition/view',
                ['requisition_id' => $this->getRequest()->getParam('requisition_id')]
            );
        }
        $resultRedirect->setRefererUrl();

        $isReplace = $this->getRequest()->getParam('is_replace', false);

        try {
            $cartId = $this->cartManagement->createEmptyCartForCustomer($this->userContext->getUserId());
            $listId = $this->getRequest()->getParam('requisition_id');
            $itemIds = explode(',', $this->_request->getParam('selected'));
            $items = $this->itemSelector->selectItemsFromRequisitionList(
                $listId,
                $itemIds,
                $this->storeManager->getWebsite()->getId()
            );
            $addedItems = $this->listManagement->placeItemsInCart($cartId, $itemIds, $isReplace, $listId);

            $this->messageManager->addSuccess(
                __('You added %1 item(s) to your shopping cart.', count($addedItems))
            );
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Something went wrong.'));
            $this->logger->critical($e);
        }

        return $resultRedirect;
    }
}
