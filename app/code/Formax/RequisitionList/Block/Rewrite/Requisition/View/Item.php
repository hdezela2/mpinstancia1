<?php

namespace Formax\RequisitionList\Block\Rewrite\Requisition\View;

use Magento\Catalog\Block\Product\Context;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Helper\Data as CatalogHelper;
use Magento\Tax\Helper\Data as TaxHelper;
use Magento\RequisitionList\Model\Checker\ProductChangesAvailability;
use Magento\RequisitionList\Api\Data\RequisitionListItemInterface;
use Magento\RequisitionList\Model\RequisitionListItem\Validator\Sku as SkuValidator;
use Magento\RequisitionList\Model\RequisitionListItemProduct;
use Magento\Framework\App\ObjectManager;
use Magento\RequisitionList\Model\RequisitionListItemOptionsLocator;
use Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface;
use Magento\Framework\App\ResourceConnection;


class Item extends \Magento\RequisitionList\Block\Requisition\View\Item

{

    private Image $imageHelper;
    private TaxHelper $taxHelper;
    private CatalogHelper $catalogHelper;
    private PriceCurrencyInterface $priceCurrency;
    private ProductChangesAvailability $productChangesAvailabilityChecker;
    private RequisitionListItemProduct $requisitionListItemProduct;
    /**
     * @var RequisitionListItemOptionsLocator|mixed
     */
    private $itemOptionsLocator;
    /**
     * @var ItemResolverInterface|mixed
     */
    private $itemResolver;
    private ResourceConnection $resourceConnection;

    public function __construct(
        Context $context,
        PriceCurrencyInterface $priceCurrency,
        ProductChangesAvailability $productChangesAvailabilityChecker,
        RequisitionListItemProduct $requisitionListItemProduct,
        array $data = [],
        RequisitionListItemOptionsLocator $itemOptionsLocator = null,
        ItemResolverInterface $itemResolver = null,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->imageHelper = $context->getImageHelper();
        $this->taxHelper = $context->getTaxData();
        $this->catalogHelper = $context->getCatalogHelper();
        $this->priceCurrency = $priceCurrency;
        $this->productChangesAvailabilityChecker = $productChangesAvailabilityChecker;
        $this->requisitionListItemProduct = $requisitionListItemProduct;

        $this->itemOptionsLocator = $itemOptionsLocator
            ?? ObjectManager::getInstance()->get(RequisitionListItemOptionsLocator::class);
        $this->itemResolver = $itemResolver
            ?? ObjectManager::getInstance()->get(ItemResolverInterface::class);
        parent::__construct(
        	$context,
        	$priceCurrency,
        	$productChangesAvailabilityChecker,
        	$requisitionListItemProduct,
        	$data,
        	$itemOptionsLocator,
        	$itemResolver
        );
        $this->resourceConnection = $resourceConnection;

    }

    public function getRequisitionListProduct()
    {
        $item = $this->getItem();
        if (!$this->requisitionListItemProduct->isProductAttached($item) && !$this->isOptionsUpdated()) {
            return null;
        }

        try {
            $product = $this->requisitionListItemProduct->getProduct($item);
            return $product;
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return null;
        }
    }

    /*public function displayBothPrices()
    {
        return 0;
    }*/

    public function getAssigId()
    {
    	$item = $this->getItem();
        $itemId = $item->getId();
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
	    $select->from('requisition_list_item', ['options']);
	    $select->where('item_id = '. $itemId);
	    $optionsRes=$resourceConnection->fetchOne($select);
	    $options=json_decode($optionsRes);
        return $options->info_buyRequest->assignId;
    }

    public function getAddUrl()
    {
    	$item = $this->getItem();
        $itemId = $item->getId();
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
	    $select->from('requisition_list_item', ['options']);
	    $select->where('item_id = '. $itemId);
	    $optionsRes=$resourceConnection->fetchOne($select);
	    $options=json_decode($optionsRes);
        return $options->info_buyRequest->addurl;
    }

    public function getProductId()
    {
    	$item = $this->getItem();
        $itemId = $item->getId();
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
	    $select->from('requisition_list_item', ['options']);
	    $select->where('item_id = '. $itemId);
	    $optionsRes=$resourceConnection->fetchOne($select);
	    $options=json_decode($optionsRes);
        return $options->info_buyRequest->product;
    }

    public function getFormattedPrice()
    {
    	$item = $this->getItem();
        $itemId = $item->getId();
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
	    $select->from('requisition_list_item', ['options']);
	    $select->where('item_id = '. $itemId);
	    $optionsRes=$resourceConnection->fetchOne($select);
	    $options=json_decode($optionsRes);
        return $this->formatProductPrice($options->info_buyRequest->price);
    }


    public function getFormattedSubtotal()
    {
    	$item = $this->getItem();
        $itemId = $item->getId();
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
	    $select->from('requisition_list_item', ['options']);
	    $select->where('item_id = '. $itemId);
	    $optionsRes=$resourceConnection->fetchOne($select);
	    $options=json_decode($optionsRes);
        return $this->formatProductPrice(
            $options->info_buyRequest->price * $this->getItem()->getQty()
        );
    }

    private function formatProductPrice($value)
    {
        return $this->priceCurrency->convertAndFormat(
            $value,
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $this->_storeManager->getStore()
        );
    }
}
