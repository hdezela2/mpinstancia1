<?php

namespace Formax\RegionalCondition\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use Formax\RegionalCondition\Helper\Config as HelperConfig;

class InitializeValidationDataAseo implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var int
     */
    private $websiteId;

    /**
     * @var HelperConfig
     */
    private $helperConfig;

    /**
     * UpdateCustomerAttributesMetadata constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param HelperConfig $helperConfig
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        HelperConfig $helperConfig
    ) {
        $this->helperConfig = $helperConfig;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->websiteId = $this->helperConfig->getWebsiteIdByWebsiteCode(HelperConfig::WEBSITE_CODE_ASEO);
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $validations = [
            [
                'postname' => 'shippingcost',
                'label' => 'Costos de despacho',
                'website_id' => $this->websiteId,
                'validation' => 'number',
                'regex_value' => null,
                'required' => 1,
                'value_must_be' => 'lower_equal',
                'max_length' => 0,
                'format_value' => 'price',
                'sort_order' => 0
            ],
            [
                'postname' => 'region_id',
                'label' => 'Región',
                'website_id' => $this->websiteId,
                'validation' => 'none',
                'regex_value' => null,
                'required' => 1,
                'value_must_be' => 'none',
                'max_length' => 0,
                'format_value' => 'none',
                'sort_order' => 1
            ],
            [
                'postname' => 'contact_name',
                'label' => 'Nombre del contacto',
                'website_id' => $this->websiteId,
                'validation' => 'letter',
                'regex_value' => null,
                'required' => 1,
                'value_must_be' => 'none',
                'max_length' => 255,
                'format_value' => 'none',
                'sort_order' => 2
            ],
            [
                'postname' => 'contact_phone',
                'label' => 'Teléfono de contacto',
                'website_id' => $this->websiteId,
                'validation' => 'regex',
                'regex_value' => "/^[269][0-9]*$/", //"/^[\+\d]+(?:[\d-.\s()]*)$/",
                'required' => 1,
                'value_must_be' => 'none',
                'max_length' => 10,
                'format_value' => 'none',
                'sort_order' => 3
            ],
            [
                'postname' => 'contact_email',
                'label' => 'Correo de contacto',
                'website_id' => $this->websiteId,
                'validation' => 'email',
                'regex_value' => null,
                'required' => 1,
                'value_must_be' => 'none',
                'max_length' => 150,
                'format_value' => 'none',
                'sort_order' => 4
            ],
            [
                'postname' => 'enable_storepickup',
                'label' => 'Retiro en tienda',
                'website_id' => $this->websiteId,
                'validation' => 'none',
                'regex_value' => null,
                'required' => 1,
                'value_must_be' => 'greather_equal',
                'max_length' => 0,
                'format_value' => 'none',
                'sort_order' => 5
            ],
            [
                'postname' => 'comuna_storepickup',
                'label' => 'Comuna del Local de Retiro en Tienda',
                'website_id' => $this->websiteId,
                'validation' => 'none',
                'regex_value' => null,
                'required' => 1,
                'value_must_be' => 'none',
                'max_length' => 0,
                'format_value' => 'none',
                'sort_order' => 6
            ],
            [
                'postname' => 'maximum_delivery',
                'label' => 'Plazo máximo de entrega',
                'website_id' => $this->websiteId,
                'validation' => 'numeric_ilimited',
                'regex_value' => null,
                'required' => 1,
                'value_must_be' => 'lower_equal',
                'max_length' => 0,
                'format_value' => 'number',
                'sort_order' => 7
            ],
            [
                'postname' => 'urban_factor',
                'label' => 'Factor Urbano',
                'website_id' => $this->websiteId,
                'validation' => 'regex',
                'regex_value' => "/^\d+(\.\d{1,2})?$/",
                'required' => 0,
                'value_must_be' => 'lower_equal',
                'max_length' => 0,
                'format_value' => 'none',
                'sort_order' => 8
            ],
            [
                'postname' => 'rural_factor',
                'label' => 'Factor Rural',
                'website_id' => $this->websiteId,
                'validation' => 'regex',
                'regex_value' => "/^\d+(\.\d{1,2})?$/",
                'required' => 0,
                'value_must_be' => 'lower_equal',
                'max_length' => 0,
                'format_value' => 'none',
                'sort_order' => 9
            ]
        ];

        /**
         * Insert validations
         */
        foreach ($validations as $validation) {
            $connection->insertForce(
                $this->moduleDataSetup->getTable('dccp_validation_regional_condition'),
                [
                    'postname' => $validation['postname'],
                    'label' => $validation['label'],
                    'website_id' => $validation['website_id'],
                    'validation' => $validation['validation'],
                    'regex_value' => $validation['regex_value'],
                    'required' => $validation['required'],
                    'value_must_be' => $validation['value_must_be'],
                    'max_length' => $validation['max_length'],
                    'format_value' => $validation['format_value'],
                    'sort_order' => $validation['sort_order']
                ]
            );
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '1.0.0';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
