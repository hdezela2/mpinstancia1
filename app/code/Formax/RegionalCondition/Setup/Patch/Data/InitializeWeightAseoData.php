<?php

namespace Formax\RegionalCondition\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use Formax\RegionalCondition\Helper\Config as HelperConfig;

class InitializeWeightAseoData implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * int
     */
    private $websiteId;

    /**
     * @var HelperConfig
     */
    private $helperConfig;

    /**
     * UpdateCustomerAttributesMetadata constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param HelperConfig $helperConfig
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        HelperConfig $helperConfig
    ) {
        $this->helperConfig = $helperConfig;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->websiteId = $this->helperConfig->getWebsiteIdByWebsiteCode(HelperConfig::WEBSITE_CODE_ASEO);
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $weight = [
            [
                'dimension_label' => 'Pequeño',
                'weight_from' => 0.0001,
                'weight_to' => 34.9999,
                'website_id' => $this->websiteId
            ],
            [
                'dimension_label' => 'Mediano',
                'weight_from' => 35.0000,
                'weight_to' => 99.9999,
                'website_id' => $this->websiteId
            ],
            [
                'dimension_label' => 'Grande',
                'weight_from' => 100.0000,
                'weight_to' => 249.9999,
                'website_id' => $this->websiteId
            ],
            [
                'dimension_label' => 'Kilo Adicional',
                'weight_from' => 250.0000,
                'weight_to' => 99999999.0000,
                'website_id' => $this->websiteId
            ]
        ];

        /**
         * Insert validations
         */
        foreach ($weight as $w) {
            $connection->insertForce(
                $this->moduleDataSetup->getTable('dccp_weight'),
                [
                    'dimension_label' => $w['dimension_label'],
                    'weight_from' => $w['weight_from'],
                    'weight_to' => $w['weight_to'],
                    'website_id' => $w['website_id']
                ]
            );
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '1.0.1';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
