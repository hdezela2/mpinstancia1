<?php

namespace Formax\RegionalCondition\Helper;

use Formax\RegionalCondition\Helper\Config as HelperConfig;
use Formax\RegionalCondition\Logger\Logger as RegionalConditionLogger;
use Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory;
use Formax\RegionalCondition\Model\ResourceModel\ValidationRegionalCondition\Collection as ValidationCollection;
use Formax\RegionalCondition\Model\ResourceModel\Weight\CollectionFactory as WeightCollection;
use Formax\RegionComuna\Model\ResourceModel\Comuna\CollectionFactory as ComunaCollectionFactory;
use Formax\RegionComuna\Model\ResourceModel\ComunaDensity\CollectionFactory as ComunaDensityCollectionFactory;
use Formax\RegionComuna\Model\ResourceModel\Density\CollectionFactory as DensityCollection;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Mpshipping\Model\ResourceModel\Mpshipping\CollectionFactory as MpshippingCollectionFactory;
use Webkul\SellerSubAccount\Helper\Data as SubAccountHelper;
use Chilecompra\CombustiblesRenewal\Model\Constants;

class Data extends AbstractHelper
{
    /**
     * Max number saved in DB representing unlimited value
     *
     * @var int
     */
    const UNLIMITED_VALUE = 999999999;

    /**
     * Allowed words to define unlimited value
     *
     * @var array
     */
    const UNLIMITED_TEXT = [
        'ilimitado',
        'ilimitados',
        'ilimitada',
        'ilimitadas',
    ];

    /**
     * @var ComunaDensityCollectionFactory
     */
    protected $comunaDensityCollectionFactory;

    /**
     * @var CollectionFactory
     */
    protected $regionalConditionCollectionFactory;

    /**
     * @var ComunaCollectionFactory
     */
    protected $comunaCollectionFactory;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var ResourceConnection
     */
    protected $dbConnection;

    /**
     * @var int
     */
    protected $websiteId;

    /**
     * @var string
     */
    protected $websiteCode;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var MpshippingCollectionFactory
     */
    protected $mpShipping;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var PricingHelper
     */
    protected $pricingHelper;

    /**
     * @var ValidationCollection
     */
    protected $validationCollection;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var SubAccountHelper
     */
    protected $subAccountHelper;

    /**
     * @var WeightCollection
     */
    protected $weightCollection;

    /**
     * @var DensityCollection
     */
    protected $densityCollection;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var array
     */
    private $postData = null;

    /**
     * @var array
     */
    protected $validateFields = [];

    /** @var JsonHelper */
    protected $jsonHelper;

    /** @var RegionalConditionLogger */
    protected $logger;

    /** @var ZendClientFactory */
    protected $httpClientFactory;

    /** @var SerializerInterface */
    protected $serializer;

    /**
     * @param Context $context
     * @param CollectionFactory $regionalConditionCollectionFactory
     * @param ResourceConnection $resource
     * @param StoreManagerInterface $storeManager
     * @param CustomerSession $customerSession
     * @param MpshippingCollectionFactory $mpShipping
     * @param Currency $currency
     * @param PricingHelper $pricingHelper
     * @param ValidationCollection $validationCollection
     * @param ComunaCollectionFactory comunaCollectionFactory
     * @param Registry $registry
     * @param DensityCollection $densityCollection
     * @param WeightCollection $weightCollection
     * @param SubAccountHelper $subAccountHelper
     * @param ComunaDensityCollectionFactory $comunaDensityCollectionFactory
     */
    public function __construct(
        Context $context,
        CollectionFactory $regionalConditionCollectionFactory,
        ResourceConnection $resource,
        StoreManagerInterface $storeManager,
        CustomerSession $customerSession,
        MpshippingCollectionFactory $mpShipping,
        Currency $currency,
        PricingHelper $pricingHelper,
        ValidationCollection $validationCollection,
        ComunaCollectionFactory $comunaCollectionFactory,
        Registry $registry,
        DensityCollection $densityCollection,
        WeightCollection $weightCollection,
        SubAccountHelper $subAccountHelper,
        ComunaDensityCollectionFactory $comunaDensityCollectionFactory,
        JsonHelper $jsonHelper,
        RegionalConditionLogger $logger,
        ZendClientFactory $httpClientFactory,
        SerializerInterface $serializer
    ) {
        $this->comunaDensityCollectionFactory = $comunaDensityCollectionFactory;
        $this->comunaCollectionFactory = $comunaCollectionFactory;
        $this->subAccountHelper = $subAccountHelper;
        $this->registry = $registry;
        $this->weightCollection = $weightCollection;
        $this->densityCollection = $densityCollection;
        $this->validationCollection = $validationCollection;
        $this->currency = $currency;
        $this->pricingHelper = $pricingHelper;
        $this->mpShipping = $mpShipping;
        $this->customerSession = $customerSession;
        $this->regionalConditionCollectionFactory = $regionalConditionCollectionFactory;
        $this->resource = $resource;
        $this->storeManager = $storeManager;
        $this->websiteId = (int)$this->storeManager->getStore()->getWebsiteId();
        $this->websiteCode = $this->storeManager->getWebsite()->getCode();
        $this->dbConnection = $this->resource->getConnection();
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->httpClientFactory = $httpClientFactory;
        $this->serializer = $serializer;

        parent::__construct($context);
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }

    /**
     * Get value from POST by key
     *
     * @param string $key
     * @param int $index
     * @param int $subIndex
     * @return string
     */
    public function getPostValue($key, $index = -1, $subIndex = -1)
    {
        if (null === $this->postData) {
            $this->postData = (array) $this->getDataPersistor()->get('dccp_regional_conditions');
            $this->getDataPersistor()->clear('dccp_regional_conditions');
        }

        if (isset($this->postData[$key])) {
            $dataVal = $this->getValidationModel();
            if (is_array($this->postData[$key]) && $index >= 0) {
                if ($subIndex >= 0) {
                    if (isset($this->postData[$key][$index][$subIndex])) {
                        return (string) $this->currencyFormat($this->postData[$key][$index][$subIndex]);
                    }
                } else {
                    if (isset($this->postData[$key][$index])) {
                        if ($key == 'maximum_delivery' || $key == 'maximum_emergency_delivery') {
                            return (string) $this->postData[$key][$index];
                        }

                        return (string) ((isset($dataVal[$key]) && $dataVal[$key]->getFormatValue() == 'price')
                            ?  $this->currencyFormat($this->postData[$key][$index]) : (isset($dataVal[$key]) && $dataVal[$key]->getFormatValue() == 'number'
                            ? $this->numberFormat($this->postData[$key][$index]) : $this->postData[$key][$index]));
                    }
                }
            } else {
                if ($key == 'maximum_delivery' || $key == 'maximum_emergency_delivery') {
                    return (string) $this->postData[$key];
                }

                return (string) ((isset($dataVal[$key]) && $dataVal[$key]->getFormatValue() == 'price')
                    ?  $this->currencyFormat($this->postData[$key]) : (isset($dataVal[$key]) && $dataVal[$key]->getFormatValue() == 'number'
                    ? $this->numberFormat($this->postData[$key]) : $this->postData[$key]));
            }
        }

        return '';
    }

    /**
     * Retrive field saved by column key
     *
     * @param array $data
     * @param string $key
     * @param int $index
     * @param int $subIndex
     * @return string
     */
    public function getField($data, $key, $index = -1, $subIndex = -1)
    {
        if (!empty($key) && is_array($data)) {
            if (isset($data[$key])) {
                $dataVal = $this->getValidationModel();
                if (is_array($data[$key]) && $index >= 0) {
                    if ($subIndex >= 0) {
                        if (isset($data[$key][$index][$subIndex])) {
                            return (string) $this->currencyFormat($data[$key][$index][$subIndex]);
                        }
                    } else {
                        if (isset($data[$key][$index])) {
                            if ($data[$key][$index] === -1) {
                                $data[$key][$index] = '';
                            }
                            if ($key == 'maximum_delivery' || $key == 'maximum_emergency_delivery') return (string) $data[$key][$index];
                            return (string) ((isset($dataVal[$key]) && $dataVal[$key]->getFormatValue() == 'price')
                                ?  $this->currencyFormat($data[$key][$index]) : (isset($dataVal[$key]) && $dataVal[$key]->getFormatValue() == 'number'
                                ? $this->numberFormat($data[$key][$index]) : $data[$key][$index]));
                        }
                    }
                } else {
                    if ($key == 'maximum_delivery' || $key == 'maximum_emergency_delivery') return (string) $data[$key];
                    return (string) ((isset($dataVal[$key]) && $dataVal[$key]->getFormatValue() == 'price')
                        ?  $this->currencyFormat($data[$key]) : (isset($dataVal[$key]) && $dataVal[$key]->getFormatValue() == 'number'
                        ? $this->numberFormat($data[$key]) : $data[$key]));
                }
            }
        }

        return '';
    }

    /**
     * Retrive Customer Id logged In
     *
     * @return int
     */
    public function getSellerId()
    {
        return $this->subAccountHelper->getSubAccountSellerId();
    }

    /**
     * Retrive regional condition by current seller and website
     *
     * @param mixed int/null $wkSellerId
     * @return array
     */
    public function getRegionalConditionbySeller($wkSellerId = null)
    {
        $result = [];
        $sellerId = $wkSellerId === null ? $this->getSellerId() : $wkSellerId;
        $collection = $this->regionalConditionCollectionFactory->create();
        $collection->getSelect()->joinLeft(
            ['r' => $this->dbConnection->getTableName('directory_country_region')],
            'main_table.region_id = r.region_id',
            ['default_name', 'code']
        )->where('seller_id = ?', $sellerId)
        ->where('website_id = ?', $this->websiteId)
        ->order('code ASC');
        if ($collection && is_array($collection->getData())) {
            foreach ($collection->getData() as $key => $row) {
                foreach ($row as $key2 => $value) {
                    $result[$key2][$key] = (int)$value === 999999999 ? 'Ilimitado' : $value;
                }
            }
        }

        return $result;
    }

    /**
     * @param int $sellerId
     * @param int $regionId
     * @return \Magento\Framework\DataObject
     */
    public function getRegionalConditionsBySellerAndRegionId(int $sellerId, int $regionId) {
        $collection = $this->regionalConditionCollectionFactory->create();
        $collection->getSelect()->joinLeft(
            ['r' => $this->dbConnection->getTableName('directory_country_region')],
            'main_table.region_id = r.region_id',
            ['default_name', 'code']
        )->where('seller_id = ?', $sellerId)
            ->where('website_id = ?', $this->websiteId)
            ->where('r.region_id = ?', $regionId)
            ->order('code ASC');


        return $collection->getFirstItem();
    }

    public function getRegionalIdsbySeller($wkSellerId = null)
    {
        $collection = $this->mpShipping->create()
            ->addFieldToFilter('partner_id', ['eq' => $wkSellerId])
            ->addFieldToSelect('dest_region_id')
            ->getColumnValues('dest_region_id')
        ;

        return $collection;
    }

    /**
     * Retrive regional condition by current seller and website
     *
     * @param mixed int/null $wkSellerId
     * @return array
     */
    public function getTableRatesbySeller($wkSellerId = null)
    {
        $concatSeller = $wkSellerId !== null ? '_' . $wkSellerId : '';
        $result = $this->registry->registry('dccp_tablerates_seller'. $concatSeller);
        if ($result === null) {
            $result = [];
            $sellerId = $wkSellerId === null ? $this->getSellerId() : $wkSellerId;

            $collection = $this->mpShipping->create();
            $collection->getSelect()
                ->reset(\Magento\Framework\DB\Select::ORDER)
                ->reset(\Magento\Framework\DB\Select::LIMIT_COUNT)
                ->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET)
                ->reset(\Magento\Framework\DB\Select::COLUMNS)
                ->distinct()
                ->columns(['price'])
                ->join(
                    ['w' => $this->dbConnection->getTableName('dccp_weight')],
                    'main_table.weight_from = w.weight_from and main_table.weight_to = w.weight_to and w.website_id = ' . $this->websiteId,
                    ['weight_id' => 'id']
                )->where('partner_id = ?', $sellerId)
                ->where('main_table.website_id = ?', $this->websiteId);

            if ($this->websiteId === HelperConfig::WEBSITE_ID_FERRETERIA) {
                $collection->getSelect()->join(
                    ['cd' => $this->dbConnection->getTableName('dccp_comuna_density')],
                    'main_table.dest_zip = cd.comuna_id',
                    ['density_id']
                );
                $collection->setOrder('density_id','ASC');
            }

            if ($this->websiteCode === HelperConfig::WEBSITE_CODE_ASEO) {
                $collection->getSelect()->join(
                    ['cm' => $this->dbConnection->getTableName('dccp_comuna')],
                    'main_table.dest_zip = cm.id',
                    ['tipification']
                );
            }
            $collection->setOrder('weight_id','ASC');
            if ($collection && is_array($collection->getData())) {
                foreach ($collection->getData() as $row) {
                    if ($this->websiteCode === HelperConfig::WEBSITE_CODE_ASEO) {
                        $result['shippingcost'][strtolower($row['tipification'])][$row['weight_id']] = $row['price'];
                    } else if ($this->websiteId === HelperConfig::WEBSITE_ID_FERRETERIA) {
                        $result['shippingcost'][$row['density_id']][$row['weight_id']] = $row['price'];
                    } elseif($this->websiteCode == \Linets\VehiculosSetUp\Model\Constants::WEBSITE_CODE) {
                        $result = $collection->getData();
                    }
                }
            }
            $this->registry->register('dccp_tablerates_seller' . $concatSeller, $result);
        }

        return $result;
    }

    /**
     * Retrive matrix rates by seller
     *
     * @param mixed int/null $wkSellerId
     * @return array
     */
    public function getMatrixbySeller($wkSellerId = null)
    {
        $concatSeller = $wkSellerId !== null ? '_'.$wkSellerId : '';
        $result = $this->registry->registry('dccp_matrix_rates_seller'.$concatSeller);
        if ($result === null) {
            $result = [];
            $sellerId = $wkSellerId === null ? $this->getSellerId() : $wkSellerId;

            $collection = $this->mpShipping->create();
            $collection->getSelect()
                ->reset(\Magento\Framework\DB\Select::ORDER)
                ->reset(\Magento\Framework\DB\Select::LIMIT_COUNT)
                ->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET)
                ->reset(\Magento\Framework\DB\Select::COLUMNS)
                ->distinct()
                ->columns(['price'])
                ->join(
                    ['w' => $this->dbConnection->getTableName('dccp_weight')],
                    'main_table.weight_from = w.weight_from and main_table.weight_to = w.weight_to',
                    ['weight_id' => 'id', 'dimension_label']
                )->where('partner_id = ?', $sellerId)
                ->where('main_table.website_id = ?', $this->websiteId);

            if ($this->websiteId === HelperConfig::WEBSITE_ID_FERRETERIA) {
                $collection->getSelect()->join(
                    ['cd' => $this->dbConnection->getTableName('dccp_comuna_density')],
                    'main_table.dest_zip = cd.comuna_id',
                    ['density_id']
                )->join(
                    ['d' => $this->dbConnection->getTableName('dccp_density')],
                    'cd.density_id = d.id',
                    ['density_name' => 'name']
                );

                $collection->setOrder('density_id','ASC');
            }
            if ($this->websiteCode === HelperConfig::WEBSITE_CODE_ASEO) {
                $collection->getSelect()->join(
                    ['cm' => $this->dbConnection->getTableName('dccp_comuna')],
                    'main_table.dest_zip = cm.id',
                    ['tipification']
                );
            }
            $collection->setOrder('weight_id','ASC');

            if ($collection && is_array($collection->getData())) {
                foreach ($collection->getData() as $row) {
                    if ($this->websiteId === HelperConfig::WEBSITE_ID_FERRETERIA) {
                        $result[$row['density_name']][$row['dimension_label']] = $this->currencyFormat($row['price']);
                    } else if ($this->websiteCode === HelperConfig::WEBSITE_CODE_ASEO) {
                        $result[$row['tipification']][$row['dimension_label']] = $this->currencyFormat($row['price']);
                    }
                }
            }

            $this->registry->register('dccp_matrix_rates_seller'.$concatSeller, $result);
        }

        return $result;
    }

    /**
     * Retrieve regional condition by seller
     *
     * @param int $sellerId
     * @param array
     */
    public function getRegionalConditionFormated($sellerId)
    {
        $result = [];

        if ((int)$sellerId > 0) {
            $collection = $this->regionalConditionCollectionFactory->create();
            $collection->getSelect()
                ->reset(\Magento\Framework\DB\Select::ORDER)
                ->reset(\Magento\Framework\DB\Select::LIMIT_COUNT)
                ->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET)
                ->reset(\Magento\Framework\DB\Select::COLUMNS)
                ->columns([
                    'region_id',
                    'contact_name',
                    'contact_phone',
                    'contact_email',
                    'maximum_amount' => new \Zend_Db_Expr('IF(maximum_amount = ' . self::UNLIMITED_VALUE . ', "Ilimitado", maximum_amount)'),
                    'maximum_qty' => new \Zend_Db_Expr('IF(maximum_qty = ' . self::UNLIMITED_VALUE . ', "Ilimitado", maximum_qty)'),
                    'enable_storepickup' => new \Zend_Db_Expr('IF(enable_storepickup = 1, "Sí", "No")'),
                    'days_storepickup',
                    'maximum_delivery',
                    'enable_emergency_storepickup' => new \Zend_Db_Expr('IF(enable_emergency_storepickup = 1, "Sí", "No")'),
                    'days_emergency_storepickup',
                    'maximum_emergency_delivery',
                    'time_maximum_emergency_delivery',
                    'time_maximum_contingency_delivery',
                    'time_maximum_prevention_delivery',
                    'max_days_packing_dispatch',
                    'max_days_bulk_dispatch',
                    'urban_factor',
                    'rural_factor',
                    'shop_address_takeout',
                    'food_benefit_administration' => new \Zend_Db_Expr('IF(food_benefit_administration = 1, "Sí", "No")'),
                    'benefit_card' => new \Zend_Db_Expr('IF(benefit_card = 1, "Sí", "No")'),
                    'products_availabilty_term_emergency_hours',
                    'products_replacement_term_emergency_days',
                    'products_replacement_term_prevention_days'
                ])
                ->join(
                    ['r' => $this->dbConnection->getTableName('directory_country_region')],
                    'main_table.region_id = r.region_id',
                    ['region_name' => 'default_name']
                )->join(
                    ['s' => $this->dbConnection->getTableName('dccp_regional_condition_seller')],
                    'main_table.id = s.regional_condition_id',
                    [$this->websiteCode == Constants::WEBSITE_CODE ? 'regional_percentage' : '']
                )->joinLeft(
                    ['c' => $this->dbConnection->getTableName('dccp_comuna')],
                    'main_table.comuna_storepickup = c.id',
                    ['comuna_storepickup' => 'name']
                )->where('s.seller_id = ' . (int)$sellerId);
            if ($collection) {
                foreach ($collection as $item) {
                    $result[$item->getRegionId()]['region_name'] = $item->getRegionName();
                    $result[$item->getRegionId()]['contact_name'] = $item->getContactName();
                    $result[$item->getRegionId()]['contact_phone'] = $item->getContactPhone();
                    $result[$item->getRegionId()]['contact_email'] = $item->getContactEmail();
                    $result[$item->getRegionId()]['maximum_amount'] = is_numeric($item->getMaximumAmount()) ? $this->currencyFormat($item->getMaximumAmount()) : $item->getMaximumAmount();
                    $result[$item->getRegionId()]['maximum_qty'] = is_numeric($item->getMaximumQty()) ? $this->numberFormat($item->getMaximumQty()) : $item->getmaximumQty();
                    $result[$item->getRegionId()]['enable_storepickup'] = $item->getEnableStorepickup();
                    $result[$item->getRegionId()]['days_storepickup'] = $item->getDaysStorepickup();
                    $result[$item->getRegionId()]['maximum_delivery'] = $this->numberFormat($item->getMaximumDelivery());
                    $result[$item->getRegionId()]['enable_emergency_storepickup'] = $item->getEnableEmergencyStorepickup();
                    $result[$item->getRegionId()]['days_emergency_storepickup'] = $this->numberFormat($item->getDaysEmergencyStorepickup());
                    $result[$item->getRegionId()]['maximum_emergency_delivery'] = $this->numberFormat($item->getMaximumEmergencyDelivery());
                    $result[$item->getRegionId()]['max_days_packing_dispatch'] = $item->getMaxDaysPackingDispatch();
                    $result[$item->getRegionId()]['max_days_bulk_dispatch'] = $item->getMaxDaysBulkDispatch();

                    $result[$item->getRegionId()]['time_maximum_emergency_delivery'] = $this->numberFormat($item->getData('time_maximum_emergency_delivery'));
                    $result[$item->getRegionId()]['time_maximum_contingency_delivery'] = $this->numberFormat($item->getData('time_maximum_contingency_delivery'));

                    $result[$item->getRegionId()]['excluded_comunes'] = $item->getExcludedComunes();

                    $result[$item->getRegionId()]['comuna_storepickup'] = $item->getComunaStorepickup();
                    $result[$item->getRegionId()]['urban_factor'] = $item->getUrbanFactor();
                    $result[$item->getRegionId()]['rural_factor'] = $item->getRuralFactor();
                    $result[$item->getRegionId()]['food_benefit_administration'] = $item->getFoodBenefitAdministration();
                    $result[$item->getRegionId()]['benefit_card'] = $item->getBenefitCard();
                    $result[$item->getRegionId()]['shop_address_takeout'] = $item->getShopAddressTakeout();

                    $result[$item->getRegionId()]['products_availabilty_term_emergency_hours'] = $item->getProductsAvailabiltyTermEmergencyHours();
                    $result[$item->getRegionId()]['time_maximum_prevention_delivery'] = $item->getTimeMaximumPreventionDelivery();
                    $result[$item->getRegionId()]['products_replacement_term_emergency_days'] = $item->getProductsReplacementTermEmergencyDays();
                    $result[$item->getRegionId()]['products_replacement_term_prevention_days'] = $item->getProductsReplacementTermPreventionDays();

                    if ($this->websiteCode == Constants::WEBSITE_CODE) {
                        $result[$item->getRegionId()]['regional_percentage'] = $item->getData('regional_percentage');
                    }
                }
            }
        }

        // Clean data by website
        foreach ($result as $key => $value) {
            foreach ($value as $k => $v) {
                if (!in_array($k, HelperConfig::FIELDS[$this->websiteCode])) {
                    unset($result[$key][$k]);
                }
            }
        }

        return $result;
    }

    /**
     * Get validation fields by current website
     *
     * @return array
     */
    public function getValidationModel()
    {
        if (is_array($this->validateFields) && count($this->validateFields) > 0) {
            return $this->validateFields;
        }

        $collection = $this->validationCollection->addWebsiteFilter($this->websiteId);
        foreach($collection as $item) {
            $this->validateFields[trim($item->getPostname())] = $item;
        }

        return $this->validateFields;
    }

    /**
     * Get Currenct Currency Symbol
     *
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->storeManager->getStore()->getCurrentCurrencyCode();
    }

    /**
     * Retrieve an instance of Density Collection
     *
     * @return \Formax\RegionComuna\Model\ResourceModel\Density\CollectionFactory
     */
    public function getDensities()
    {
        $collection = $this->densityCollection->create();
        return $collection;
    }

    /**
     * Retrieve an instance of Weight Collection
     *
     * @return \Formax\RegionalCondition\Model\ResourceModel\Weight\CollectionFactory
     */
    public function getWeight()
    {
        $collection = $this->weightCollection->create();
        $collection ->addFieldToFilter('website_id', $this->websiteId);
        return $collection;
    }

    /**
     * get formatted price
     *
     * @param float $price
     * @return string
     */
    public function currencyFormat($price)
    {
        if (is_numeric($price)) {
            return $this->pricingHelper->currency($price, true, false);
        }

        return $price;
    }

    /**
     * get formatted number
     *
     * @param float $number
     * @return string
     */
    public function numberFormat($number)
    {
        if (is_numeric($number)) {
            return $this->currency->format($number, ['display' => \Zend_Currency::NO_SYMBOL], false);
        }

        return $number;
    }

    /**
     * Get website Id by Website code
     *
     * @param string $code
     * @return int
     */
    public function getWebsiteIdByCode($code)
    {
        $websiteId = 0;

        try {
            $websiteId = $this->storeManager->getWebsite($code)->getWebsiteId();
        } catch (LocalizedException $localizedException) {
            $websiteCode = 0;
        }

        return $websiteId;
    }

    /**
     * Get current website
     *
     * @return int
     */
    public function getCurrentWebsite()
    {
        return (int)$this->storeManager->getStore()->getWebsiteId();
    }

    /**
     * Retrive comunas grouped by density
     *
     * @return array
     */
    public function getComunasGroupedByDensity()
    {
        $data = [];
        $collection = $this->comunaDensityCollectionFactory->create()
            ->addWebsiteFilter($this->websiteId);
        foreach ($collection as $item) {
            $obj = new \Magento\Framework\DataObject();
            $obj->setComunaId((int)$item->getComunaId());
            $obj->setRegionId((int)$item->getRegionId());
            $data[$item->getDensityId()][] = $obj;
        }

        return $data;
    }

    /**
     * Retrive comunas with tipification
     *
     * @return array
     */
    public function getComunasGroupedByTipification()
    {
        $data = [];
        $collection = $this->comunaCollectionFactory->create();
        foreach ($collection as $item) {
            $obj = new \Magento\Framework\DataObject();
            $obj->setComunaId((int)$item->getId());
            $obj->setRegionId((int)$item->getRegionId());
            $data[strtolower($item->getTipification())][] = $obj;
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getWebsiteCode(): string
    {
        return $this->websiteCode;
    }

    /**
     * Add New Regional Condition from REST service
     *
     * @param string $token
     * @param array $bodyParams
     * @return Zend_Http_Response
     */
    public function addRegionalConditionService($token, $bodyParams)
    {
        $response = null;

        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_regional_condition_add/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');

            if (!empty($endpoint) && !empty($token) &&
                is_array($bodyParams) && count($bodyParams)) {
                $httpHeaders = [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . trim($token)
                ];

                $data = $this->jsonHelper->jsonEncode($bodyParams);

                $this->logger->info('addRegionalConditionService REQUEST: ' . $data);

                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                        'timeout' => 30]
                );
                $client->setHeaders($httpHeaders);
                $client->setRawData($data);
                $response = $client->request(\Zend_Http_Client::POST)->getBody();

                $this->logger->info('addRegionalConditionService RESPONSE: ' . $response);
                $response = $this->jsonHelper->jsonDecode($response);
            } else {
                throw new LocalizedException(__('Define endpoint and token add regional condition webservice'));
            }
        } catch(LocalizedException $e) {
            $this->logger->critical('addRegionalConditionService Error message: ' . $e->getMessage());
            $response['error'] = "addRegionalConditionService Error";
            $response['message'] =  'There was some error while processing your request.';
        } catch(\Exception $e) {
            $this->logger->critical('addRegionalConditionService Error message: ' . $e->getMessage());
            $response['error'] = "addRegionalConditionService Error";
            $response['message'] =  'There was some error while processing your request.';
        }

        return $response;
    }
}
