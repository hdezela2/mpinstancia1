<?php

namespace Formax\RegionalCondition\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\LocalizedException;

class Config extends AbstractHelper
{
	/**
     * @var StoreManagerInterface
     */
	protected $storeManagerInterface;

	/**
	* @var int
	*/
	public $WEBSITE_ID_ASEO;

	/**
	* @var int
	*/
	public $WEBSITE_ID_VOUCHER;

	/**
	* @var int
	*/
	const WEBSITE_ID_FERRETERIA = 1;

	/**
	* @var string
	*/
	const WEBSITE_CODE_FERRETERIA = 'base';

	/**
	* @var string
	*/
	const WEBSITE_CODE_ASEO = 'convenio_aseo';

	/**
	* @var string
	*/
	const WEBSITE_CODE_VOUCHER = 'convenio_voucher';

	/**
	* @var array
	*/
	const FIELDS = [
		'base' => [
			'shippingcost',
			'seller_id',
			'region_name',
			'regional_condition_id',
			'contact_name',
			'contact_phone',
			'contact_email',
			'maximum_amount',
			'maximum_qty',
			'enable_storepickup',
			'days_storepickup',
			'maximum_delivery',
			'enable_emergency_storepickup',
			'days_emergency_storepickup',
			'maximum_emergency_delivery'
		],
		'convenio_aseo' => [
			'shippingcost',
			'seller_id',
			'region_name',
			'regional_condition_id',
			'contact_name',
			'contact_phone',
			'contact_email',
			'enable_storepickup',
			'comuna_storepickup',
			'maximum_delivery',
			'urban_factor',
			'rural_factor'
		],
        'aseo2022' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'enable_storepickup',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email',
            'time_maximum_emergency_delivery',
            'maximum_amount',
            'maximum_delivery',
            'days_storepickup',
            'excluded_comunes',
            'shop_address_takeout'
        ],
		'convenio_voucher' => [
			'shippingcost',
			'seller_id',
			'region_name',
			'regional_condition_id',
			'contact_name',
			'contact_phone',
			'contact_email',
			'maximum_delivery',
			'food_benefit_administration',
			'benefit_card'
		],
        'emergencias' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email',
            'enable_storepickup',
            'time_maximum_emergency_delivery',
            'time_maximum_contingency_delivery',
            'time_maximum_prevention_delivery'
        ],
        'computadores' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email'
        ],
        'computadores202101' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email'
        ],
        'computadores202201' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email'
        ],
        'combustibles' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email'
        ],
        'combustibles202110' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email',
            'regional_percentage'
        ],
        'mobiliario' => [
            'region_name',
            'contact_name',
            'contact_phone',
            'contact_email',
            'maximum_amount',
            'maximum_qty',
            'enable_storepickup',
            'days_storepickup',
            'maximum_delivery',
            'enable_emergency_storepickup',
            'days_emergency_storepickup',
            'maximum_emergency_delivery',
            'shop_address_takeout'
        ],
        'escritorio' => [
            'seller_id',
            'regional_condition_id',
            'region_name',
            'contact_name',
            'contact_phone',
            'contact_email',
            'maximum_delivery'
        ],
        'alimentos' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email',
            'time_maximum_emergency_delivery',
            'maximum_amount',
            'maximum_delivery',
            'excluded_comunes'
        ],
        'voucher202106' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email',
            'time_maximum_emergency_delivery',
            'maximum_amount',
            'maximum_delivery',
            'excluded_comunes',
            'maximum_qty',
            'enable_storepickup',
            'days_storepickup',
            'enable_emergency_storepickup',
            'days_emergency_storepickup',
            'maximum_emergency_delivery',
            'minimum_emergency_delivery',
            'food_benefit_administration',
            'benefit_card',
        ],
        'gas' => [
          'shippingcost',
          'seller_id',
          'region_name',
          'regional_condition_id',
          'contact_name',
          'contact_phone',
          'contact_email',
          'maximum_amount',
          'maximum_qty',
          'enable_storepickup',
          'days_storepickup',
          'maximum_delivery',
          'enable_emergency_storepickup',
          'days_emergency_storepickup',
          'maximum_emergency_delivery',
                'max_days_packing_dispatch',
                'max_days_bulk_dispatch',
        ],
        'vehiculos202106' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email',
            'shop_address_takeout',
            'days_storepickup',
            'maximum_delivery',
        ],
        'insumos' => [
            'shippingcost',
            'seller_id',
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email',
            'shop_address_takeout',
            'days_storepickup',
            'maximum_delivery',
        ],
        'software2022' => [
            'region_name',
            'regional_condition_id',
            'contact_name',
            'contact_phone',
            'contact_email',
        ],
        'emergencias202109' => [
                'shippingcost',
                'seller_id',
                'region_name',
                'regional_condition_id',
                'contact_name',
                'contact_phone',
                'contact_email',
                'enable_storepickup',
                'products_availabilty_term_emergency_hours',
                'days_storepickup',
                'shop_address_takeout',
                'time_maximum_prevention_delivery',
                'products_replacement_term_emergency_days',
                'products_replacement_term_prevention_days'
            ]
	];

	/**
	* @var array
	*/
	const TIPIFICATION = [
		'urbano' => 'Urbano',
		'rural' => 'Rural'
	];

	/**
     * @param Context $context
     * @param StoreManagerInterface $storeManagerInterface
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManagerInterface
    ) {
        $this->storeManagerInterface = $storeManagerInterface;
		$this->WEBSITE_ID_ASEO = $this->getWebsiteIdByWebsiteCode(self::WEBSITE_CODE_ASEO);
		$this->WEBSITE_ID_VOUCHER = $this->getWebsiteIdByWebsiteCode(self::WEBSITE_CODE_VOUCHER);

        parent::__construct($context);
	}

	/**
     * Get website Id by Website code
     *
     * @param string $code
     * @return int
     */
    public function getWebsiteIdByWebsiteCode($code)
    {
		$websiteId = 0;
        try {
            $websiteId = $this->storeManagerInterface->getWebsite($code)->getWebsiteId();
        } catch (LocalizedException $localizedException) {
            $websiteId = 0;
        }

        return $websiteId;
    }
}
