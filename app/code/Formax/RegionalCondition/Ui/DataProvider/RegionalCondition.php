<?php

namespace Formax\RegionalCondition\Ui\DataProvider;

/**
 * Class RegionalCondition
 */
class RegionalCondition extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * RegionalCondition Collection
     *
     * @var \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory
     */
    public $collection;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\App\RequestInterface $request
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $collectionFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\App\RequestInterface $request,
        array $meta = [],
        array $data = []
    ) {

        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );        
        
        $collectionData = $collectionFactory->create()
                    ->setOrder('seller_id', 'ASC')
                    ->setOrder('region_id', 'ASC');
        $this->collection = $collectionData;
    }
}
