<?php

namespace Formax\RegionalCondition\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Formax\RegionalCondition\Helper\Data as Helper;

/**
 * @api
 * @since 100.0.2
 */
class Number extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Helper $helper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Helper $helper,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->helper = $helper;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item[$fieldName])) {
                    if ((int)$item[$fieldName] === Helper::UNLIMITED_VALUE) {
                        $item[$fieldName] = 'Ilimitado';
                    } else {
                        $item[$fieldName] = $this->helper->numberFormat($item[$fieldName]);
                    }
                }
            }
        }

        return $dataSource;
    }
}
