<?php

/**
 * Override Webkul Mpshipping Admin Shipping Save Controller.
 *
 * @category  Formax
 * @package   Formax_RegionalCondition
 * @author    Formax
 */

namespace Formax\RegionalCondition\Controller\Adminhtml\Shipping;

use Magento\Backend\App\Action;
use Webkul\Mpshipping\Model\MpshippingmethodFactory;
use Webkul\Mpshipping\Model\MpshippingFactory;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Formax\RegionalCondition\Helper\Config as HelperConfig;
use Formax\RegionalCondition\Helper\Data as HelperData;
use Webkul\Mpshipping\Controller\Adminhtml\Shipping\Save as WebkulMpShippingSave;

class Save extends WebkulMpShippingSave
{
    private const CUSTOMER_ENTITY_TABLE = 'customer_entity';
    /**
     * @var HelperConfig
     */
    protected $helperData;
    private \Magento\Framework\App\ResourceConnection $resource;
    private \Magento\Framework\DB\Adapter\AdapterInterface $connection;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     * @param MpshippingmethodFactory $shippingmethodFactory
     * @param MpshippingFactory $mpshipping
     * @param UploaderFactory $fileUploader
     * @param \Magento\Framework\File\Csv $csvReader
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param HelperData $helperData
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        MpshippingmethodFactory $shippingmethodFactory,
        MpshippingFactory $mpshipping,
        UploaderFactory $fileUploader,
        \Magento\Framework\File\Csv $csvReader,
        \Magento\Framework\App\ResourceConnection $resource,
        HelperData $helperData
    ) {
        $this->resource = $resource;
        $this->connection = $this->resource->getConnection();
        $this->_resultPageFactory = $resultPageFactory;
        $this->_mpshippingMethod = $shippingmethodFactory;
        $this->_mpshipping = $mpshipping;
        $this->_fileUploader = $fileUploader;
        $this->_csvReader = $csvReader;
        $this->helperData = $helperData;

        parent::__construct(
            $context,
            $resultPageFactory,
            $shippingmethodFactory,
            $mpshipping,
            $fileUploader,
            $csvReader
        );
    }

    /**
     * Check for is allowed.
     *
     * @return bool
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = [];
        $wholedata = [];
        if ($this->getRequest()->isPost()) {
            try {
                if (!$this->_formKeyValidator->validate($this->getRequest())) {
                    return $this->resultRedirectFactory->create()->setPath('*/*/index');
                }

                $uploader = $this->_fileUploader->create(['fileId' => 'import_file']);
                $result = $uploader->validateFile();
                $rows = [];
                $file = $result['tmp_name'];
                $fileNameArray = explode('.', $result['name']);
                $ext = end($fileNameArray);
                $status = true;
                $totalSaved = 0;
                $totalUpdated = 0;

                if ($file != '' && $ext == 'csv') {
                    $csvFileData = $this->_csvReader->getData($file);
                    $partnerid = 0;
                    $count = 0;

                    if (isset($csvFileData[0]) && array_search('website', $csvFileData[0]) !== false) {
                        $headerArray = ['country_code', 'region_id', 'zip', 'zip_to', 'price', 'weight_from', 'weight_to', 'shipping_method', 'seller_id', 'website', 'numeric_zipcode', 'alphanumeric_zipcode'];
                        $columnCount = 12;
                    } else {
                        $headerArray = ['country_code', 'region_id', 'zip', 'zip_to', 'price', 'weight_from', 'weight_to', 'shipping_method', 'seller_id', 'numeric_zipcode', 'alphanumeric_zipcode'];
                        $columnCount = 11;
                    }

                    foreach ($csvFileData as $key => $rowData) {
                        if ($count == 0) {
                            if (count($rowData) < $columnCount) {
                                $this->messageManager->addError(__('CSV file is not a valid file!'));
                                return $this->resultRedirectFactory->create()->setPath('*/*/index');
                            } else {
                                $data = $rowData;
                                $status = ($headerArray === $rowData);
                                if (!$status) {
                                    $this->messageManager->addError(__('Please write the correct header formation of CSV file!'));
                                    return $this->resultRedirectFactory->create()->setPath('*/*/index');
                                }
                                ++$count;
                            }
                        } else {
                            foreach ($rowData as $filekey => $filevalue) {
                                $wholedata[$data[$filekey]] = $filevalue;
                            }

                            list($updatedWholedata, $errors) = $this->validateCsvDataToSave($wholedata, []);
                            $updatedWholedata['shipping_method'] = htmlentities($updatedWholedata['shipping_method']);

                            if (empty($errors)) {
                                $shippingMethodId = $this->calculateShippingMethodId(
                                    $updatedWholedata['shipping_method']
                                );

                                $websiteId = isset($updatedWholedata['website']) ? $this->helperData->getWebsiteIdByCode(trim($updatedWholedata['website'])) : HelperConfig::WEBSITE_ID_FERRETERIA;
                                $partnerId = $updatedWholedata['seller_id'];
                                $partnerIdQuoted = $this->connection->quote($partnerId);
                                $_bkEmail = $partnerId;
                                $sql = 'SELECT entity_id FROM ' . $this->resource->getTableName(self::CUSTOMER_ENTITY_TABLE) . ' WHERE email = ' . $partnerIdQuoted;
                                $partnerId = (int)$this->connection->fetchOne($sql);

                                if (!$partnerId) {
                                    $this->messageManager->addError(
                                        __(
                                            'Invalid Email: %1',
                                            $_bkEmail
                                        )
                                    );
                                } else {
                                    $temp = [
                                        'dest_country_id' => $updatedWholedata['country_code'],
                                        'dest_region_id' => htmlentities($updatedWholedata['region_id']),
                                        'dest_zip' => htmlentities($updatedWholedata['zip']),
                                        'dest_zip_to' => htmlentities($updatedWholedata['zip_to']),
                                        'price' => $updatedWholedata['price'],
                                        'weight_from' => $updatedWholedata['weight_from'],
                                        'weight_to' => $updatedWholedata['weight_to'],
                                        'shipping_method_id' => $shippingMethodId,
                                        'partner_id' => $partnerId,
                                        'website_id' => $websiteId,
                                        'is_range' =>strtolower($updatedWholedata['numeric_zipcode']),
                                        'zipcode' => htmlentities($updatedWholedata['alphanumeric_zipcode']),
                                    ];

                                    list($saved, $updated) = $this->addDataToCollection($temp, $updatedWholedata, $shippingMethodId, $partnerId);
                                    $totalSaved += $saved;
                                    $totalUpdated += $updated;
                                }

                            } else {
                                $rows[] = $key . ':' . $errors[0];
                            }
                        }
                    }
                    if (count($rows)) {
                        $this->messageManager->addError(
                            __(
                                'Following rows are not valid rows : %1',
                                implode(',', $rows)
                            )
                        );
                    }
                    if (($count - 1) <= 1) {
                        if ($totalSaved) {
                            $this->messageManager
                            ->addSuccess(
                                __('%1 Row(s) shipping detail has been successfully saved', $totalSaved)
                            );
                        }
                        if ($totalUpdated) {
                            $this->messageManager
                            ->addNotice(
                                __('%1 Row(s) shipping rule already exist for the given range.', $totalUpdated)
                            );
                        }
                    }

                    return $this->resultRedirectFactory->create()->setPath('*/*/index');
                } else {
                    $this->messageManager->addError(__('Please upload CSV file'));
                }
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/index');
    }

    /**
     * add data to DB
     *
     * @param array $temp
     * @param array $updatedWholedata
     * @param int $shippingMethodId
     * @param int $partnerId
     * @return array
     */
    public function addDataToCollection($temp, $updatedWholedata, $shippingMethodId, $partnerId)
    {
        $updated = 0;
        $saved = 0;
        $collection = $this->_mpshipping->create()
            ->getCollection()
            ->addFieldToFilter('partner_id', $partnerId)
            ->addFieldToFilter('dest_country_id', $updatedWholedata['country_code'])
            ->addFieldToFilter('dest_region_id', $updatedWholedata['region_id'])
            ->addFieldToFilter('dest_zip', ['gteq'=> $updatedWholedata['zip']])
            ->addFieldToFilter('dest_zip_to', ['lteq' => $updatedWholedata['zip_to']])
            ->addFieldToFilter('weight_from', ['lteq' => $updatedWholedata['weight_from']])
            ->addFieldToFilter('weight_to', ['gteq' => $updatedWholedata['weight_to']])
            ->addFieldToFilter('shipping_method_id', $shippingMethodId);

        if (isset($temp['website_id']) && (int)$temp['website_id'] > 0) {
            $collection->addFieldToFilter('website_id', $temp['website_id']);
        }

        if ($temp['is_range'] == 'no') {
            $collection->addFieldToFilter('is_range', ['eq' => $temp['is_range']]);
            $collection->addFieldToFilter('zipcode', ['eq' => $updatedWholedata['alphanumeric_zipcode']]);
        }

        if ($collection->getSize() > 0) {
          ++$updated;
        } else {
            $shippingModel = $this->_mpshipping->create();
            $shippingModel->setData($temp)->save();
            ++$saved;
        }

        return [$saved, $updated];
    }

    /**
     * Validate columns in CSV file
     *
     * @param array $wholedata
     * @return array
     */
    public function validateCsvDataToSave($wholedata, $headerArray)
    {
        $data = [];
        $errors = [];
        foreach ($wholedata as $key => $value) {
            switch ($key) {
                case 'shipping_method':
                    if (!is_string(trim($value))) {
                        $errors[] = __('Shipping Method should be a string %1', $value);
                    } elseif ($value == '') {
                        $errors[] = __('Shipping Method can not be empty');
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                case 'country_code':
                    if (!is_string(trim($value))) {
                        $errors[] = __('Country code should be a string %1', $value);
                    } elseif ($value == '') {
                        $errors[] = __('Country code can not be empty');
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                case 'region_id':
                    if ($value == '') {
                        $errors[] = __('Region Id can not be empty');
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                case 'zip':
                    if ($value == '') {
                        $errors[] = __('Zip field can not be empty');
                    } elseif (!preg_match('/^([0-9*])+?[0-9.]*$/', $value)) {
                        $errors[] = __('Zip field from should be a numeric or * value %1', $value);
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                case 'zip_to':
                    if ($value == '') {
                        $errors[] = __('Zip to field can not be empty');
                    } elseif (!preg_match('/^([0-9*])+?[0-9.]*$/', $value)) {
                        $errors[] = __('Zip to field from should be a numeric or * value %1', $value);
                        if (isset($data['zip'])) {
                            if ($data['zip'] >= $value) {
                                $errors[] = __('Zip To field should be greater then Zip From field');
                            }
                        } else {
                            $errors[] = __('Zip field can not be empty');
                        }
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                case 'price':
                    if ($value == '') {
                        $errors[] = __('Price can not be empty');
                    } elseif (!preg_match('/^([0-9])+?[0-9.]*$/', $value)) {
                        $errors[] = __('Not a valid value for price %1', $value);
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                case 'weight_from':
                    if ($value == '') {
                        $errors[] = __('Weight From can not be empty');
                    } elseif (!preg_match('/^([0-9])+?[0-9.]*$/', $value)) {
                        $errors[] = __('Not a valid value for weight from field %1', $value);
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                case 'weight_to':
                    if ($value == '') {
                        $errors[] = __('Weight To can not be empty');
                    } elseif (!preg_match('/^([0-9])+?[0-9.]*$/', $value)) {
                        $errors[] = __('Not a valid value for weight to field %1', $value);
                        if (isset($data['weight_from'])) {
                            if ($data['weight_from'] >= $value) {
                                $errors[] = __('Weight to should be greater then weight from field');
                            }
                        } else {
                            $errors[] = __('Weight From can not be empty');
                        }
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                case 'seller_id':
                    if ($value == '') {
                        $errors[] = __('Seller Id can not be empty');
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                case 'website':
                        $data[$key] = $value;
                    break;
                case 'numeric_zipcode':
                    if ($value == '') {
                        $errors[] = __('Numeric Zipcode value can not be empty');
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                case 'alphanumeric_zipcode':
                    if ($value == '' && (strtolower($wholedata['numeric_zipcode']) == 'no')) {
                         $errors[] = __('Alphanumeric Zipcode can not be empty');
                    } else {
                        $data[$key] = $value;
                    }
                    break;
            }
        }

        return [$data, $errors];
    }
}
