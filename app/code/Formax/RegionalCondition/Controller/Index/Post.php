<?php

namespace Formax\RegionalCondition\Controller\Index;

use Formax\Storepickup\Model\ResourceModel\Storepickup as storePickupResourceModel;
use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Formax\RegionalCondition\Helper\Config as HelperConfig;
use Intellicore\EmergenciasRenewal\Constants;
use Webkul\MpAssignProduct\Helper\Data as MpAssignProductHelper;

class Post extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * Marketplace Helper
     *
     * @var \Webkul\Marketplace\Helper\Data
     */
    public $marketplaceHelper;

    /**
     * @var int
     */
    const CUSTOM_ERROR_CODE = 555;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Webkul\Mpshipping\Model\MpshippingFactory
     */
    protected $mpShipping;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Formax\RegionalCondition\Model\RegionalConditionFactory
     */
    protected $regionalConditionFactory;

    /**
     * @var \Formax\RegionalCondition\Model\RegionalConditionSellerFactory
     */
    protected $regionalConditionSellerFactory;

    /**
     * @var \Formax\Storepickup\Model\StorepickupFactory
     */
    protected $storepickupFactory;

    /**
     * @var \Formax\RegionalCondition\Model\ValidationRegionalConditionFactory
     */
    protected $validationRegionalCondition;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    protected $regionCollectionFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $dbConnection;

    /**
     * @var \Formax\RegionalCondition\Helper\Data
     */
    protected $helper;

    /**
     * @var int
     */
    protected $websiteId;

    /**
     *
     */
    protected $isWebsiteEmergencias;

    /**
     * @var array
     */
    protected $unlimitedText;

    /**
     * @var array
     */
    protected $densityArray = [];

    /**
     * @var array
     */
    protected $weightArray = [];

    /**
     * @var array
     */
    protected $regionArray = [];

    protected $websiteCode;

    /**
     * @var \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory
     */
    protected $regionalConditionCollectionFactory;

    /** @var \Magento\Customer\Model\Session */
    protected $_customerSession;

    /** @var \Magento\Customer\Api\CustomerRepositoryInterface */
    protected $customerRepositoryInterface;

    /** @var MpAssignProductHelper */
    protected $_mpAssignProductHelper;

    /** @var storePickupResourceModel  */
    protected storePickupResourceModel $storePickupResourceModel;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $store
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Webkul\Marketplace\Helper\Data $marketplaceHelper
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Formax\RegionalCondition\Model\RegionalConditionFactory $regionalConditionFactory
     * @param \Formax\RegionalCondition\Model\RegionalConditionSellerFactory $regionalConditionSellerFactory
     * @param \Formax\Storepickup\Model\StorepickupFactory $storepickupFactory
     * @param \Webkul\Mpshipping\Model\MpshippingFactory $mpShipping
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Formax\RegionalCondition\Model\ValidationRegionalConditionFactory $validationRegionalCondition
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory
     * @param \Formax\RegionalCondition\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger,
        \Webkul\Marketplace\Helper\Data $marketplaceHelper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Formax\RegionalCondition\Model\RegionalConditionFactory $regionalConditionFactory,
        \Formax\RegionalCondition\Model\RegionalConditionSellerFactory $regionalConditionSellerFactory,
        \Formax\Storepickup\Model\StorepickupFactory $storepickupFactory,
        \Webkul\Mpshipping\Model\MpshippingFactory $mpShipping,
        \Magento\Framework\App\ResourceConnection $resource,
        \Formax\RegionalCondition\Model\ValidationRegionalConditionFactory $validationRegionalCondition,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Formax\RegionalCondition\Helper\Data $helper,
        MpAssignProductHelper $mpAssignProductHelper,
        \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $regionalConditionCollectionFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Session\SessionManagerInterface $session,
        storePickupResourceModel $storePickupResourceModel
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->validationRegionalCondition = $validationRegionalCondition;
        $this->regionalConditionFactory = $regionalConditionFactory;
        $this->regionalConditionSellerFactory = $regionalConditionSellerFactory;
        $this->storepickupFactory = $storepickupFactory;
        $this->regionCollectionFactory = $regionCollectionFactory;
        $this->mpShipping = $mpShipping;
        $this->resource = $resource;
        $this->storeManager = $storeManager;
        $this->resultPageFactory = $resultPageFactory;
        $this->marketplaceHelper = $marketplaceHelper;

        $this->websiteId = (int)$this->storeManager->getStore()->getWebsiteId();
        $this->websiteCode = $this->storeManager->getStore()->getCode();
        $this->isWebsiteEmergencias = $this->storeManager->getStore()->getCode() ==  \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE;
        $weightCollection = $this->helper->getWeight()->addFieldToFilter('website_id', $this->websiteId);
        $densityCollection = $this->helper->getDensities();
        $regionCollection = $this->regionCollectionFactory->create()->addCountryFilter('CL');
        $this->dbConnection = $this->resource->getConnection();
        $this->unlimitedText = \Formax\RegionalCondition\Helper\Data::UNLIMITED_TEXT;
        $this->regionalConditionCollectionFactory = $regionalConditionCollectionFactory;
        $this->_mpAssignProductHelper = $mpAssignProductHelper;
        $this->_customerSession = $customerSession;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->session = $session;
        $this->storePickupResourceModel = $storePickupResourceModel;

        foreach ($weightCollection as $w) {
            $this->weightArray[$w->getId()] = $w;
        }
        foreach ($densityCollection as $d) {
            $this->densityArray[$d->getId()] = $d;
        }
        foreach ($regionCollection as $r) {
            $this->regionArray[$r->getRegionId()] = $r;
        }

        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        if ($this->marketplaceHelper->isSeller()) {
            $this->getPostData();
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

    public function getPostData()
    {
        $post = $this->getRequest()->getPostValue();
        $message = '';
        $error = false;
        $regionIds = [];
        $regionIdsAdded = [];
        $model = null;
        $regionalConditionIds = [];
        $regionalConditionSellerRows = [];
        $storepickupRows = [];
        $maxAmount = 0;
        $maxQty = 0;
        $maximumDelivery = 0;
        $maximumEmergencyDelivery = 0;
        $shopAddressTakeout = '';
        $maxDaysPackingDispatch = 0;
        $maxDaysBulkDispatch = 0;

        $customerId = $this->getCustomerLoggedId();
        $sellerId = $this->_mpAssignProductHelper->getSellerIdBySubAccount();
        $customer = $this->customerRepositoryInterface->getById($sellerId);
        $customerLoguedIn = $this->customerRepositoryInterface->getById($customerId);
        $token = $customerLoguedIn->getCustomAttribute('user_rest_atk') ?
            $customerLoguedIn->getCustomAttribute('user_rest_atk')->getValue() : '';
        $agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
            $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : '';
        $organizationId = $customer->getCustomAttribute('user_rest_id_organization') ?
            $customer->getCustomAttribute('user_rest_id_organization')->getValue() : '';

        if (!$post) {
            $this->_redirect('*/*/');
            return;
        }

        $this->dbConnection->beginTransaction();

        try {
            $this->session->start();
            $this->session->setNewRegionalConditionFormData($post);

            $totalRegionIds = isset($post['region_id']) && is_array($post['region_id']) ? count($post['region_id']) : 0;
            $sellerId = (int)$post['seller_id'];

            if ((int)$sellerId === 0)
            {
                $message = __("Seller not founded. Only a seller's subaccount is allowed.");
                throw new \Exception($message, self::CUSTOM_ERROR_CODE);
            }

            // Validate only shipping cost
            $modelShipping = $this->helper->getTableRatesbySeller($sellerId);
            $modelShipping = is_array($modelShipping) && count($modelShipping) == 0 ? null : $modelShipping;

            if (!$this->isWebsiteEmergencias && $modelShipping != null && isset($post['shippingcost']))
            {
                $validation = $this->validateShippingPostValues($post['shippingcost'], $modelShipping);
                if (isset($validation['error']))
                {
                    $message = $validation['msg'];
                    throw new \Exception($message, self::CUSTOM_ERROR_CODE);
                }
            }

            // Preparing array new regions to validate
                if ($totalRegionIds > 0 && $sellerId > 0)
            {
                for ($i=0; $i<$totalRegionIds; $i++)
                {
                    if ((int)$post['regional_condition_id'][$i] === 0)
                    {
                        $regionIds[] = (int)$post['region_id'][$i];
                    }
                    $regionIdsAdded[] = (int)$post['region_id'][$i];
                }
            }

            // Validate if regions already exists by seller
            if (count($regionIds) > 0)
            {
                $valRegions = $this->validationRegionalCondition->create()->validateRegionExist($sellerId, $this->websiteId, $regionIds);
                if ($valRegions !== false && is_array($valRegions) && count($valRegions) > 0)
                {
                    foreach ($valRegions as $valRegion)
                    {
                        $this->messageManager->addError($valRegion);
                    }
                    $this->dbConnection->rollBack();
                    $this->getDataPersistor()->set('dccp_regional_conditions', $post);
                    $this->_redirect('regionalcondition/index/index');
                    return;
                }
            }

            // ADD NEW region condition for emergency202109 store AND ASEO renewal
            $websiteCodes = [Constants::WEBSITE_CODE, AseoRenewalConstants::WEBSITE_CODE];
            if (in_array($this->websiteCode, $websiteCodes) && !array_key_exists('regional_condition_id', $post)) {
                $regionalCondition = $this->regionalConditionFactory->create();
                $regionalConditionCollection = $this->regionalConditionCollectionFactory->create();
                $regionalConditionData = $regionalConditionCollection
                    ->addRegionFilter($post['region_id'])
                    ->addWebsiteFilter($this->websiteId)->getData();

                if (isset($post['products_availabilty_term_emergency_hours'])) {
                    $productsAvailabiltyTermEmergencyHours = array_column($regionalConditionData, 'products_availabilty_term_emergency_hours');
                    if (($key = array_search(0, $productsAvailabiltyTermEmergencyHours)) !== false) {
                        unset($productsAvailabiltyTermEmergencyHours[$key]);
                    }
                    $productsAvailabiltyTermEmergencyHours = min($productsAvailabiltyTermEmergencyHours);

                    if ($post['products_availabilty_term_emergency_hours'] > $productsAvailabiltyTermEmergencyHours) {
                        $this->messageManager->addError(__('El valor del campo Plazo Disponibilidad de Productos en Situación de Emergencia (Horas Corridas) debe ser menor o igual que %1, ya que es el valor mínimo de los proveedores del CM.', $productsAvailabiltyTermEmergencyHours));

                        return $this->_redirect('regionalcondition/index/add');
                    } else {
                        $regionalCondition->setProductsAvailabiltyTermEmergencyHours($post['products_availabilty_term_emergency_hours']);
                    }
                }

                if (isset($post['time_maximum_prevention_delivery'])) {
                    $timeMaximumPreventionDelivery = array_column($regionalConditionData, 'time_maximum_prevention_delivery');
                    if (($key = array_search(0, $timeMaximumPreventionDelivery)) !== false) {
                        unset($timeMaximumPreventionDelivery[$key]);
                    }
                    $timeMaximumPreventionDelivery = min($timeMaximumPreventionDelivery);

                    if ($post['time_maximum_prevention_delivery'] > $timeMaximumPreventionDelivery) {
                        $this->messageManager->addError(__('El valor del campo Plazo Entrega Productos en Situación de Prevención (Días Hábiles) debe ser menor o igual que %1, ya que es el valor mínimo de los proveedores del CM.', $timeMaximumPreventionDelivery));

                        return $this->_redirect('regionalcondition/index/add');
                    } else {
                        $regionalCondition->setTimeMaximumPreventionDelivery($post['time_maximum_prevention_delivery']);
                    }
                }
                $daysStorePickup = 10;
                if ($this->websiteCode == AseoRenewalConstants::WEBSITE_CODE) {
                    $daysStorePickup = 30;
                    if (isset($post['maximum_delivery'])) {
                        if ($post['maximum_delivery'] > $daysStorePickup) {
                            $this->messageManager->addError(__("Maximum Delivery Time equal to or less than 30 days."));
                            return $this->_redirect('regionalcondition/index/add');
                        } else if ($post['maximum_delivery'] <= 0) {
                            $this->messageManager->addError(__("Maximum Delivery Time must be equal to or greater than 1 day."));
                            return $this->_redirect('regionalcondition/index/add');
                        }else{
                            $regionalCondition->setMaximumDelivery($post['maximum_delivery']);
                        }
                    }

                }
                if (isset($post['days_storepickup'])) {
                    if ($post['days_storepickup'] > $daysStorePickup) {
                        $this->messageManager->addError(__('El valor del campo PLAZO RETIRO EN TIENDA (DÍAS HÁBILES) debe ser menor o igual que '.$daysStorePickup));
                        return $this->_redirect('regionalcondition/index/add');
                    } else {
                        $regionalCondition->setDaysStorepickup($post['days_storepickup']);
                    }
                }

                if (isset($post['products_replacement_term_emergency_days'])) {
                    if ($post['products_replacement_term_emergency_days'] > 2) {
                        $this->messageManager->addError(__('El valor del campo Plazo Reposición Productos Situación De Emergencia (Días Corridos) debe ser menor o igual que 2'));
                        return $this->_redirect('regionalcondition/index/add');
                    } else {
                        $regionalCondition->setProductsReplacementTermEmergencyDays($post['products_replacement_term_emergency_days']);
                    }
                }

                if (isset($post['products_replacement_term_prevention_days'])) {
                    if ($post['products_replacement_term_prevention_days'] > 5) {
                        $this->messageManager->addError(__('El valor del campo Plazo Reposición Productos en Situación de Prevención (Días Hábiles) debe ser menor o igual que 5'));
                        return $this->_redirect('regionalcondition/index/add');
                    } else {
                        $regionalCondition->setProductsReplacementTermPreventionDays($post['products_replacement_term_prevention_days']);
                    }
                }

                if (isset($post['shop_address_takeout'])) {
                    $regionalCondition->setShopAddressTakeout($post['shop_address_takeout']);
                }

                $params = [
                    'idvendedor' => $organizationId,
                    'idregion' => $post['region_id'],
                    'idConvenioMarco' => $agreementId
                ];

                // after validation, we send the information to MP and check if it was correctly added
                $validateAddRegionalConditionService = $this->helper->addRegionalConditionService($token, $params);

                if (isset($validateAddRegionalConditionService['success']) && $validateAddRegionalConditionService['success'] == 'OK') {
                    $regionalCondition->setRegionId($post['region_id']);
                    $regionalCondition->setContactName($post['contact_name']);
                    $regionalCondition->setContactPhone($post['contact_phone']);
                    $regionalCondition->setContactEmail($post['contact_email']);
                    if ($this->websiteCode == AseoRenewalConstants::WEBSITE_CODE) {
                        $regionalCondition->setEnableStorepickup($post['enable_storepickup']);
                        $regionalCondition->setComunaStorepickup(0);
                        $regionalCondition->setUrbanFactor(null);
                        $regionalCondition->setRuralFactor(null);
                        if ($post['enable_storepickup'] == "1") {
                            $regionalCondition->setEnableStorePickup($post['enable_storepickup']);
                            $regionalCondition->setDaysStorepickup($post['days_storepickup']);
                            $regionalCondition->setShopAddressTakeout($post['shop_address_takeout']);
                        } else {
                            $regionalCondition->setEnableStorePickup(0);
                            $regionalCondition->setDaysStorePickup(0);
                            $regionalCondition->setShopAddressTakeout('');
                        }
                    }
                    $regionalCondition->save();

                    $this->logger->info("Saving regional condition data");
                    $this->logger->info(json_encode($regionalCondition->getData()));

                    $regionalConditionId = (int)$regionalCondition->getId();

                    if ($regionalConditionId <= 0) {
                        $message = __('An error occured saving the information.');
                        throw new \Exception($message, self::CUSTOM_ERROR_CODE);
                    } else {
                        $regionalConditionSeller = $this->regionalConditionSellerFactory->create()->load($regionalConditionId, 'regional_condition_id');
                        $regionalConditionSeller->addData([
                            'regional_condition_id' => $regionalConditionId,
                            'seller_id' => $sellerId,
                            'dccp_seller_id' => $post['dccp_seller_id'],
                            'website_id' => $this->websiteId
                        ])->save();

                        $this->logger->info("Saving regional condition seller data");
                        $this->logger->info(json_encode($regionalConditionSeller->getData()));

                        if (AseoRenewalConstants::WEBSITE_CODE == $this->websiteCode) {
                            $mpShipping = $this->mpShipping->create();
                            $mpShipping->setWebsiteId($this->websiteId);
                            $mpShipping->setDestCountryId('CL');
                            $mpShipping->setDestRegionId($post['region_id']);
                            $mpShipping->setDestZip('*');
                            $mpShipping->setDestZipTo('*');
                            $mpShipping->setPrice(0);
                            $mpShipping->setWeightFrom(0);
                            $mpShipping->setWeightTo(99999999);
                            $mpShipping->setPartnerId($sellerId);
                            $mpShipping->setShippingMethodId(1);
                            $mpShipping->setIsRange('yes');
                            $mpShipping->setZipcode('*');
                            $mpShipping->save();

                            // Save in table dccp_storePickup
                            $storepickup = $this->storepickupFactory->create()->loadUniqueRow(
                                (int)$post['region_id'],
                                $sellerId,
                                $this->websiteId
                            );
                            if (!$storepickup) {
                                $storepickup = $this->storepickupFactory->create();
                                $storepickup->setDestRegionId((int)$post['region_id']);
                                $storepickup->setSellerId($sellerId);
                                $storepickup->setWebsiteId($this->websiteId);
                            }

                            $storepickup->setActive((int)$post['enable_storepickup']);
                            $this->storePickupResourceModel->save($storepickup);
                        }

                        $this->dbConnection->commit();
                        $this->messageManager->addSuccess(__('Region Condition saved succesfully.'));
                        $this->session->unsNewRegionalConditionFormData();
                        return $this->_redirect('regionalcondition/index/index');
                    }
                } else {
                    if (isset($validateAddRegionalConditionService['success']) && $validateAddRegionalConditionService['success'] == 'NOK' &&
                        isset($validateAddRegionalConditionService['errores']) && is_array($validateAddRegionalConditionService['errores'])) {
                        foreach ($validateAddRegionalConditionService['errores'] as $error) {
                            $this->messageManager->addError(__($error['descripcion']));
                        }
                    } elseif (!isset($validateAddRegionalConditionService['success']) && isset($validateAddRegionalConditionService['error']) &&
                        isset($validateAddRegionalConditionService['message'])) {
                        $message = $validateAddRegionalConditionService['message'];
                        $this->messageManager->addError(__($message));
                    } else {
                        $this->messageManager->addError(__('There was some error while processing your request.'));
                    }
                    return $this->_redirect('regionalcondition/index/add');
                }
            }

            if ($totalRegionIds > 0 && $sellerId > 0)
            {
                for ($i=0; $i<$totalRegionIds; $i++)
                {
                    $regionalConditionId = (int)$post['regional_condition_id'][$i];

                    if ($regionalConditionId > 0)
                    {
                        // I'm making loading into loop because I need model to validate data saved and
                        // I need last inserted Id for passing table dccp_regional_condition_seller
                        // A worthwhile expense, I am also in the seller's account not frontend
                        $regionalCondition = $this->regionalConditionFactory->create()->load($regionalConditionId);
                        $model = $regionalCondition;
                    } else {
                        $regionalCondition = $this->regionalConditionFactory->create();
                        $model = null;
                    }

                    // Exclude shippingcost to validate only regional conditions fields
                    $postValues = $post;
                    unset($postValues['shippingcost']);

                    $storeName = $this->storeManager->getStore()->getName();
                    $validation = ['error' => ''];

                    if ($storeName !== "Alimentos") {
                        $validation = $this->validatePostValues($postValues, $i, $model);
                    }

                    if (!empty($validation['error'])) {
                        $message = $validation['msg'];
                        throw new \Exception($message, self::CUSTOM_ERROR_CODE);
                    } else {
                        if (isset($post['maximum_amount'])) {
                            $maxAmount = !in_array(strtolower(trim($post['maximum_amount'][$i])), $this->unlimitedText)
                                ? (int)$post['maximum_amount'][$i] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;
                        }
                        if (isset($post['maximum_qty'])) {
                            $maxQty = !in_array(strtolower(trim($post['maximum_qty'][$i])), $this->unlimitedText)
                                ? (int)$post['maximum_qty'][$i] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;
                        }
                        if (isset($post['maximum_delivery'])) {
                            $maximumDelivery = !in_array(strtolower(trim($post['maximum_delivery'][$i])), $this->unlimitedText)
                                ? (int)$post['maximum_delivery'][$i] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;
                        }
                        if (isset($post['maximum_emergency_delivery'])) {
                            $maximumEmergencyDelivery = !in_array(strtolower(trim($post['maximum_emergency_delivery'][$i])), $this->unlimitedText)
                                ? (int)$post['maximum_emergency_delivery'][$i] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;
                        }
                        if (isset($post['shop_address_takeout'][$i])) {
                            $shopAddressTakeout = $post['shop_address_takeout'][$i];
                        }
                        if (isset($post['max_days_packing_dispatch'])) {
                            $maxDaysPackingDispatch = !in_array(strtolower(trim($post['max_days_packing_dispatch'][$i])), $this->unlimitedText)
                                ? (int)$post['max_days_packing_dispatch'][$i] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;
                        }
                        if (isset($post['max_days_bulk_dispatch'])) {
                            $maxDaysBulkDispatch = !in_array(strtolower(trim($post['max_days_bulk_dispatch'][$i])), $this->unlimitedText)
                                ? (int)$post['max_days_bulk_dispatch'][$i] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;
                        }

                        // Saving table dccp_regional_condition
                        $regionalCondition->setRegionId($post['region_id'][$i]);
                        $regionalCondition->setContactName($post['contact_name'][$i]);
                        $regionalCondition->setContactPhone($post['contact_phone'][$i]);
                        $regionalCondition->setContactEmail($post['contact_email'][$i]);
                        $regionalCondition->setMaximumAmount($maxAmount);
                        $regionalCondition->setMaximumQty($maxQty);
                        $regionalCondition->setMaximumDelivery($maximumDelivery);
                        $regionalCondition->setMaximumEmergencyDelivery($maximumEmergencyDelivery);
                        $regionalCondition->setShopAddressTakeout($shopAddressTakeout);
                        $regionalCondition->setMaxDaysPackingDispatch($maxDaysPackingDispatch);
                        $regionalCondition->setMaxDaysBulkDispatch($maxDaysBulkDispatch);

                        if (isset($post['enable_storepickup'][$i])) {
                            $regionalCondition->setEnableStorepickup((int)$post['enable_storepickup'][$i]);
                        }else{
                            $regionalCondition->setEnableStorepickup(0);
                        }
                        if (isset($post['comuna_storepickup'][$i])) {
                            $regionalCondition->setComunaStorepickup((int)$post['comuna_storepickup'][$i]);
                        }
                        if (isset($post['days_storepickup'][$i])) {
                            $regionalCondition->setDaysStorepickup((int)$post['days_storepickup'][$i]);
                        }
                        if (isset($post['enable_emergency_storepickup'][$i])) {
                            $regionalCondition->setEnableEmergencyStorepickup((int)$post['enable_emergency_storepickup'][$i]);
                        }
                        if (isset($post['days_emergency_storepickup'][$i])) {
                            $regionalCondition->setDaysEmergencyStorepickup((int)$post['days_emergency_storepickup'][$i]);
                        }
                        /** MPMT-23: Commented out below lines given that modifying urban_factor is not allowed from this form
                        if (isset($post['urban_factor'][$i])) {
                        $regionalCondition->setUrbanFactor((int)$post['urban_factor'][$i]);
                        }
                         */
                        /** MPMT-23: Commented out below lines given that modifying rural_factor is not allowed from this form
                        if (isset($post['rural_factor'][$i])) {
                        $regionalCondition->setRuralFactor((int)$post['rural_factor'][$i]);
                        }
                         */
                        if (isset($post['food_benefit_administration'][$i])) {
                            $regionalCondition->setRuralFactor((int)$post['food_benefit_administration'][$i]);
                        }
                        if (isset($post['benefit_card'][$i])) {
                            $regionalCondition->setRuralFactor((int)$post['benefit_card'][$i]);
                        }
                        if (isset($post['time_maximum_emergency_delivery'][$i])) {
                            $regionalCondition->setTimeMaximumEmergencyDelivery((int)$post['time_maximum_emergency_delivery'][$i]);
                        }
                        if (isset($post['time_maximum_contingency_delivery'][$i])) {
                            $regionalCondition->setTimeMaximumContingencyDelivery((int)$post['time_maximum_contingency_delivery'][$i]);
                        }
                        if (isset($post['time_maximum_prevention_delivery'][$i])) {
                            $regionalCondition->setTimeMaximumPreventionDelivery((int)$post['time_maximum_prevention_delivery'][$i]);
                        }

                        if(isset($post['excluded_comunes'][$i]))
                        {
                            $regionalCondition->setExcludedComunes($post['excluded_comunes'][$i]);
                        }

                        if (isset($post['products_availabilty_term_emergency_hours'][$i])) {
                            $regionalCondition->setProductsAvailabiltyTermEmergencyHours($post['products_availabilty_term_emergency_hours'][$i]);
                        }
                        if (isset($post['products_replacement_term_emergency_days'][$i])) {
                            $regionalCondition->setProductsReplacementTermEmergencyDays($post['products_replacement_term_emergency_days'][$i]);
                        }
                        if (isset($post['products_replacement_term_prevention_days'][$i])) {
                            $regionalCondition->setProductsReplacementTermPreventionDays($post['products_replacement_term_prevention_days'][$i]);
                        }

                        $regionalCondition->save();

                        $this->logger->info("Saving regional condition data");
                        $this->logger->info(json_encode($regionalCondition->getData()));

                        $regionalConditionId = (int)$regionalCondition->getId();

                        if ($regionalConditionId <= 0) {
                            $message = __('An error occured saving the information.');
                            throw new \Exception($message, self::CUSTOM_ERROR_CODE);
                        }
                    }

                    $regionalConditionSeller = $this->regionalConditionSellerFactory->create()->load($regionalConditionId, 'regional_condition_id');
                    $regionalConditionSeller->addData([
                        'regional_condition_id' => $regionalConditionId,
                        'seller_id' => $sellerId,
                        'dccp_seller_id' => $post['dccp_seller_id'],
                        'website_id' => $this->websiteId
                    ])->save();

                    $this->logger->info("Saving regional condition seller data");
                    $this->logger->info(json_encode($regionalConditionSeller->getData()));

                    $storepickup = $this->storepickupFactory->create()->loadUniqueRow(
                        (int)$post['region_id'][$i],
                        $sellerId,
                        $this->websiteId
                    );
                    if (!$storepickup) {
                        $storepickup = $this->storepickupFactory->create();
                    }
                    $valueEnableStorepickup = 0;
                    if (isset($post['enable_storepickup'][$i])) {
                        $valueEnableStorepickup = (int)$post['enable_storepickup'][$i];
                    }
                    $storepickup->addData([
                        'dest_region_id' => (int)$post['region_id'][$i],
                        'seller_id' => $sellerId,
                        'website_id' => $this->websiteId,
                        'active' => $valueEnableStorepickup
                    ])->save();

                    $this->logger->info("Store pickup add data");
                    $this->logger->info(json_encode($storepickup->getData()));
                }


                if (!$this->isWebsiteEmergencias && $modelShipping != null && isset($post['shippingcost'])) {
                    // Delete table rates by seller to insert again
                    $result = $this->dbConnection->delete(
                        $this->resource->getTableName('marketplace_shippinglist'),
                        [
                            'partner_id = ' . $sellerId,
                            'website_id = ' . $this->websiteId
                        ]
                    );
                    $this->logger->info("Delete table rates by seller to insert again");

                    // Saving table rates by seller table marketplace_shippinglist
                    $axiX = $this->websiteId === HelperConfig::WEBSITE_ID_FERRETERIA
                        ? $this->helper->getComunasGroupedByDensity()
                        : $this->helper->getComunasGroupedByTipification();

                    foreach ($post['shippingcost'] as $shipKey => $shipValue) {
                        foreach ($axiX[$shipKey] as $comuna) {
                            foreach ($shipValue as $wKey => $wValue) {
                                $range = $this->getWeightRange($wKey);
                                if ($range !== null && $wValue != '' &&
                                    in_array($comuna->getRegionId(), $regionIdsAdded)) {
                                    $mpShipping = $this->mpShipping->create();
                                    $mpShipping->setWebsiteId($this->websiteId);
                                    $mpShipping->setDestCountryId('CL');
                                    $mpShipping->setDestRegionId($comuna->getRegionId());
                                    $mpShipping->setDestZip($comuna->getComunaId());
                                    $mpShipping->setDestZipTo($comuna->getComunaId());
                                    $mpShipping->setPrice((int)$wValue);
                                    $mpShipping->setWeightFrom($range->getFrom());
                                    $mpShipping->setWeightTo($range->getTo());
                                    $mpShipping->setPartnerId($sellerId);
                                    $mpShipping->setShippingMethodId(1);
                                    $mpShipping->setIsRange('yes');
                                    $mpShipping->setZipcode('*');
                                    $mpShipping->save();

                                    $this->logger->info("Saving table rates by seller table marketplace_shippinglist");
                                    $this->logger->info(json_encode($mpShipping->getData()));
                                }
                            }
                        }
                    }
                }

                $this->dbConnection->commit();
                $this->messageManager->addSuccess(__('Data was saved succesfully.'));
                $this->session->unsNewRegionalConditionFormData();
                if ($this->getRequest()->getParam('back')) {
                    //todo: $resultRedirect don't exist in this class
                    return $resultRedirect->setPath('regionalcondition/index/index');
                }

                $this->getDataPersistor()->clear('dccp_regional_conditions');
                $this->_redirect('regionalcondition/index/index');
                return;
            }
        } catch (\Exception $e) {
            if ($e->getCode() == self::CUSTOM_ERROR_CODE) {
                $this->messageManager->addError($e->getMessage());
            } else {
                $this->logger->critical($e->getMessage());
                $this->messageManager->addError(__('An error occured saving the information.'));
            }
            $this->dbConnection->rollBack();
            $this->getDataPersistor()->set('dccp_regional_conditions', $post);
            $this->_redirect('regionalcondition/index/index');
            return;
        }
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }

    /**
     * Retrive weight range by ID
     *
     * @param int $weightId
     * @return \Magento\Framework\DataObject
     */
    public function getWeightRange($weightId)
    {
        foreach ($this->weightArray as $w) {
            if ((int)$w['id'] === (int)$weightId) {
                $range = new \Magento\Framework\DataObject();
                $range->setFrom((float)$w['weight_from']);
                $range->setTo((float)$w['weight_to']);
                return $range;
            }
        }
        return null;
    }

    /**
     * @param array $post
     * @param int $i
     * @param mixed $model null Formax\RegionalCondition\Model\ValidationRegionalCondition
     * @return mixed bool/array
     */
    public function validatePostValues($post, $i, $model = null)
    {
        $result = true;
        $validation = ['error' => false, 'msg' => ''];

        foreach ($post as $key => $value)
        {
            $location = '';

            if (isset($post[$key][$i]))
            {
                $trimValue = trim($post[$key][$i]);
                switch ($key) {
                    case 'products_availabilty_term_emergency_hours':
                    case 'time_maximum_prevention_delivery':
                    case 'days_storepickup':
                    case 'products_replacement_term_emergency_days':
                    case 'products_replacement_term_prevention_days':
                        $postValue = in_array(strtolower($trimValue), $this->unlimitedText) ? (int)\Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE : (int)$trimValue;
                        break;
                    default:
                        $postValue = $trimValue;
                    break;
                }

                $originData = $model !== null ? (int)$model->getData($key) : null;

                if (isset($this->regionArray[$post['region_id'][$i]]))
                {
                    $location = __('(Location - Row: %1).', $this->regionArray[$post['region_id'][$i]]->getDefaultName());
                }

                if ($this->websiteCode == Constants::WEBSITE_CODE)
                {
//                    maximum_delivery
                    switch ($key) {
                        case 'products_availabilty_term_emergency_hours':
                        case 'time_maximum_prevention_delivery':
                        case 'days_storepickup':
                        case 'products_replacement_term_emergency_days':
                        case 'products_replacement_term_prevention_days':
                            if ($postValue > $originData) {
                                $validation['error'] = true;
                                $validation['msg'] = __('You can not modify the values of the regional conditions to make it a worse value');
                            }
                            break;
                    }
                }

                if ($this->websiteCode == AseoRenewalConstants::WEBSITE_CODE) {
                    $nameReg = "";
                    if (isset($post['region_id'][$i]) && $this->regionArray[$post['region_id'][$i]]) {
                        $nameReg = $this->regionArray[$post['region_id'][$i]]->getName();
                    }
                    switch ($key) {
                        case 'days_storepickup':
                            if ($post['enable_storepickup'][$i] == 1) {
                                if ($postValue <= 0){
                                    $validation['msg'] = __("Withdrawal period in store to be equal to or greater than 1 day. %1", $nameReg);
                                    $validation['error'] = true;
                                }else if ($postValue > 30) {
                                    $validation['msg'] = __("%1 period must not exceed 30 days.", $nameReg);
                                    $validation['error'] = true;
                                }elseif($postValue > $originData && !($originData == 0 || $originData == "")) {
                                    // $validation['error'] = true;
                                    // $validation['msg'] = __("You can not modify the values of the regional %1 conditions to make it a worse value", $nameReg);
                                }
                            }
                            break;
                        case 'maximum_delivery':
                            if ($postValue <= 0){
                                $validation['msg'] = __("Maximum Delivery Time must be equal to or greater than 1 day.");
                                $validation['error'] = true;
                            }else if ($postValue > 30){
                                $validation['msg'] = __("Maximum Delivery Time equal to or less than 30 days.");
                                $validation['error'] = true;
                            }elseif($postValue > $originData && !($originData == "0" || $originData == "")) {
                                $validation['error'] = true;
                                $validation['msg'] = __('You can not modify the values of the regional conditions to make it a worse value. %1',$nameReg);
                            }
                            break;
                    }
                }
                if ($validation['error'])
                {
                    return $validation;
                }

                $validation = $this->validationRegionalCondition->create()->validateData($key, $postValue, $originData, $location);
                if ($validation['error'])
                {
                    return $validation;
                }
            }
        }
        return true;
    }

    /**
     * @param array $post
     * @param mixed null Formax\RegionalCondition\Model\ValidationRegionalCondition
     * @return mixed bool/array
     */
    public function validateShippingPostValues($post, $model = null)
    {
        $result = true;

        foreach ($post as $key => $value) {
            foreach ($value as $keyW => $item) {
                if (isset($model['shippingcost'][$key][$keyW])) {
                    $location = '';
                    $trimValue = trim($item);
                    $postValue = in_array(strtolower($trimValue), $this->unlimitedText) ? \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE : $item;
                    $originData = $model !== null ? $model['shippingcost'][$key][$keyW] : null;

                    if (isset($this->densityArray[$key]) && isset($this->weightArray[$keyW])) {
                        $location = __('(Location - Cell: %1, %2).', $this->densityArray[$key]->getName(), $this->weightArray[$keyW]->getDimensionLabel());
                    }

                    $validation = $this->validationRegionalCondition->create()
                        ->validateData('shippingcost', $postValue, $originData, $location);

                    if ($validation['error']) {
                        return $validation;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Get Current Customer Id
     *
     * @return int
     */
    protected function getCustomerLoggedId()
    {
        $customerId = 0;
        if ($this->_customerSession->isLoggedIn()) {
            $customerId = (int) $this->_customerSession->getCustomerId();
        }
        return $customerId;
    }
}
