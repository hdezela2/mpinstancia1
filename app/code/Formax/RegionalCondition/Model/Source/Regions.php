<?php

namespace Formax\RegionalCondition\Model\Source;

class Regions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    protected $collection;

    /**
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $collection
     */
    public function __construct(        
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $collection
    ) {        
        $this->collection = $collection;
    }
    
    /**
     * Convert collection items to select options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->collection->create()->addCountryFilter('CL')->toOptionArray();
    }
}
