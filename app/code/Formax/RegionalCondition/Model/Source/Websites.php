<?php

namespace Formax\RegionalCondition\Model\Source;

class Websites implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory
     */
    protected $collection;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $collection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(        
        \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $collection,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {        
        $this->collection = $collection;
        $this->storeManager = $storeManager;
    }
    
    /**
     * Convert collection items to select options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $collection = $this->storeManager->getWebsites();
        
        foreach ($collection as $item) {
            $option['value'] = $item->getId();
            $option['label'] = $item->getName();
            $options[] = $option;
        }

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['value' => '', 'label' => __('Please select a website')]
            );
        }
        
        return $options;
    }
}
