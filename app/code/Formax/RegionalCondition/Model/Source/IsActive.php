<?php

namespace Formax\RegionalCondition\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface
{
    /**
     * @var int
     */
    const ENABLE = 1;

    /**
     * @var int
     */
    const DISABLE = 0;

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'label' => __('No'),
                'value' => self::DISABLE
            ],
            [
                'label' => __('Yes'),
                'value' => self::ENABLE
            ]
        ];
        
        return $options;
    }
}
