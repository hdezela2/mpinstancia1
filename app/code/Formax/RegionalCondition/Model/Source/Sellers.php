<?php

namespace Formax\RegionalCondition\Model\Source;

class Sellers implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory
     */
    protected $collection;

    /**
     * @param \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $collection
     */
    public function __construct(        
        \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $collection
    ) {        
        $this->collection = $collection;
    }
    
    /**
     * Convert collection items to select options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $collection = $this->collection->create();

        foreach ($collection as $item) {
            $option['value'] = $item->getSellerId();
            $option['label'] = $item->getShopTitle();
            $options[] = $option;
        }

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['value' => '', 'label' => __('Please select a seller')]
            );
        }
        
        return $options;
    }
}
