<?php

namespace Formax\RegionalCondition\Model;

use Exception;
use Formax\RegionalCondition\Helper\Data;
use Magento\Framework\Exception\LocalizedException;

class ValidationRegionalCondition extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory
     */
    protected $regionalCollection;

    /**
     * @var int
     */
    protected $websiteId;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param Data $data
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $regionalCollection
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $regionalCollection,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->regionalCollection = $regionalCollection;
        $this->storeManager = $storeManager;
        $this->websiteId = (int)$this->storeManager->getStore()->getWebsiteId();

        parent::__construct(
            $context,
            $registry,
            $resource = null,
            $resourceCollection = null,
            $data
        );
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\RegionalCondition\Model\ResourceModel\ValidationRegionalCondition::class);
    }

    /**
     * Validate post value with configured in table
     *
     * @param string $key
     * @param string $postValue
     * @param mixed string/null $originData
     * @param string $location
     *
     * @return array
     */
    public function validateData($key, $postValue, $originData = null, $location = '')
    {
        try {
            $validate = [];
            $result = ['error' => false, 'msg' => ''];
            $collection = $this->_registry->registry('validation_regionalcondition');
            if ($collection === null) {
                $collection = $this->getCollection()->addWebsiteFilter($this->websiteId);
                $this->_registry->register('validation_regionalcondition', $collection);
            }
            foreach($collection as $item) {
                $validate[trim($item->getPostname())] = $item;
            }
            if (array_key_exists(trim($key), $validate) && !is_array($postValue)) {
                $data = $validate[$key];
                $value = (string)trim($postValue);
                // Validate required
                if ((bool)$data->getRequired()) {
                    $validator = new \Zend\Validator\NotEmpty();
                    if (!$validator->isValid($value)) {
                        $msg = __('Field %1 is required. %2', $data->getLabel(), $location);
                        throw new LocalizedException($msg, new Exception());
                    }
                }
                // Validate max length
                if ((int)$data->getMaxLength() > 0) {
                    $validator = new \Zend\Validator\StringLength(['min' => 0, 'max' => $data->getMaxLength()]);
                    if (!$validator->isValid($value)) {
                        $msg = __('Maxlength of field %1 is %2. %3', $data->getLabel(), $data->getMaxLength(), $location);
                        throw new LocalizedException($msg, new Exception());
                    }
                }
                switch($data->getValidation()) {
                    case 'alpha':
                    case 'none':
                    break;
                    case 'letter':
                        $validator = new \Zend\Validator\Regex(['pattern' => "/^[a-z áéíóúñüÁÉÍÓÚÑÜ]+$/i"]);
                        if (!$validator->isValid($value)) {
                            $msg = __('Field %1 must be only letters. %2', $data->getLabel(), $location);
                            throw new LocalizedException($msg, new Exception());
                        }
                    break;
                    case 'numeric':
                        $validator = new \Zend\Validator\Regex(['pattern' => "/[^1-9]/"]);
                        if (!$validator->isValid($value)) {
                            $msg = __('Field %1 must be only numbers greather than zero. %2', $data->getLabel(), $location);
                            throw new LocalizedException($msg, new Exception());
                        }
                    break;
                    case 'email':
                        $validator = new \Zend\Validator\EmailAddress();
                        if (!$validator->isValid($value)) {
                            $msg = __('Field %1 must be an email address. %2', $data->getLabel(), $location);
                            throw new LocalizedException($msg, new Exception());
                        }
                    break;
                    case 'numeric_empty':
                        $trimValue = trim($value);
                        if (!empty($trimValue)) {
                            $validator = new \Zend\Validator\Digits();
                            if (!$validator->isValid($value)) {
                                $msg = __('Field %1 must be only numbers or empty. %2', $data->getLabel(), $location);
                                throw new LocalizedException($msg, new Exception());
                            }
                        }
                    break;
                    case 'numeric_zero':
                        $validator = new \Zend\Validator\Digits();
                        if (!$validator->isValid($value)) {
                            $msg = __('Field %1 must be only numbers between 0 - 9. %2', $data->getLabel(), $location);
                            throw new LocalizedException($msg, new Exception());
                        }
                    break;
                    case 'numeric_ilimited':
                        if (!in_array(strtolower($value), Data::UNLIMITED_TEXT)) {
                            $validator = new \Zend\Validator\Digits();
                            if (!$validator->isValid($value)) {
                                $msg = __('Field %1 must be only numbers between 0 - 9 or Unlimited. %2', $data->getLabel(), $location);
                                throw new LocalizedException($msg, new Exception());
                            }
                        }
                    break;
                    case 'regex':
                        if ($data->getRegexValue()) {
                            $validator = new \Zend\Validator\Regex(['pattern' => $data->getRegexValue()]);
                            if (!$validator->isValid($value)) {
                                $msg = __('Field %1 not allow these characters.', $data->getLabel(), $location);
                                throw new LocalizedException($msg, new Exception());
                            }
                        }
                    break;
                    default:
                }

                if ($originData !== null && $data->getValueMustBe() != 'none')
                {
                    if($data->getValueMustBe() == 'lower_equal_list')
                    {
                        $values = explode(',', $value);
                        $originValues = explode(',', $originData);
                        $message = "No es posible excluir nuevas comunas";
                        $msg = __($message, $data->getLabel(), $location);
                        if(count($values) > count($originValues))
                        {
                            throw new LocalizedException($msg, new Exception());
                        }
                        if(count($values) == count($originValues))
                        {
                            if(count(array_diff($values, $originValues)) > 0)
                                throw new LocalizedException($msg, new Exception());

                        }
                        if(count($values) <= count($originValues))
                        {
                            foreach($values as $value)
                            {
                                if(!in_array($value, $originValues))
                                    throw new LocalizedException($msg, new Exception());
                            }
                        }
                    }
                    else
                    {
                        $originData = (int)$originData;
                        $unlimited = $originData === Data::UNLIMITED_VALUE ? true : false;
                        $unlimitedMessage = "Field %1 can't be edited because is unlimited. %2";

                        switch($data->getValueMustBe()) {
                            case 'greather':
                                if ((int)$value <= $originData || ($value === '' && $originData >= 0)) {
                                    $formatedData = $data->getFormatValue() == 'price' ?
                                        $this->helper->currencyFormat($originData) : ($data->getFormatValue() == 'number'
                                            ? $this->helper->numberFormat($originData) : $originData);
                                    $message = $unlimited ? $unlimitedMessage : 'Value of field %1 must be greather than %2. %3';
                                    $msg = __($message, $data->getLabel(), $formatedData, $location);
                                    throw new LocalizedException($msg, new Exception());
                                }
                                break;
                            case 'lower':
                                if ((int)$value >= $originData || ($value === '' && $originData >= 0)) {
                                    $formatedData = $data->getFormatValue() == 'price' ?
                                        $this->helper->currencyFormat($originData) : ($data->getFormatValue() == 'number'
                                            ? $this->helper->numberFormat($originData) : $originData);
                                    $message = $unlimited ? $unlimitedMessage :
                                        (($value === '' && $originData === 0) ? "Value of field %1 is %2 You can't set a value lower. %3" : 'Value of field %1 must be lower than %2. %3');
                                    $msg = __($message, $data->getLabel(), $formatedData, $location);
                                    throw new LocalizedException($msg, new Exception());
                                }
                                break;
                            case 'greather_equal':
                                if ((int)$value < $originData || ($value === '' && $originData >= 0)) {
                                    $formatedData = $data->getFormatValue() == 'price' ?
                                        $this->helper->currencyFormat($originData) : ($data->getFormatValue() == 'number'
                                            ? $this->helper->numberFormat($originData) : $originData);
                                    $message = $unlimited ? $unlimitedMessage : 'Value of field %1 must be greather or equal than %2. %3';
                                    $msg = __($message, $data->getLabel(), $formatedData, $location);
                                    throw new LocalizedException($msg, new Exception());
                                }
                                break;
                            case 'lower_equal':
                                if ((int)$value > $originData || ($value === '' && $originData >= 0)) {
                                    $formatedData = $data->getFormatValue() == 'price' ?
                                        $this->helper->currencyFormat($originData) : ($key != 'maximum_delivery' && $data->getFormatValue() == 'number'
                                            ? $this->helper->numberFormat($originData) : $originData);
                                    $message = $unlimited ? $unlimitedMessage :
                                        (($value === '' && $originData === 0) ? "Value of field %1 is %2 You can't set a value lower. %3" : 'Value of field %1 must be lower or equal than %2. %3');
                                    $msg = __($message, $data->getLabel(), $formatedData, $location);
                                    throw new LocalizedException($msg, new Exception());
                                }
                                break;
                            default:
                        }
                    }
                }
            }
        } catch (LocalizedException $e) {
            $result['error'] = true;
            $result['msg'] = $e->getMessage();
        } catch (Exception $e) {
            $result['error'] = true;
            $result['msg'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Validate if region exist by seller and website
     *
     * @param mixed int/array $sellerId
     * @param mixed int/array $websiteId
     * @param mixed array $regionIds
     * @return array
     */
    public function validateRegionExist($sellerId, $websiteId, $regionIds)
    {
        $result = false;

        if ((int)$sellerId > 0 && (int)$websiteId > 0 && is_array($regionIds) && count($regionIds) > 0) {
            $collection = $this->regionalCollection->create();
            $collection->addSellerFilter($sellerId)
                ->addWebsiteFilter($websiteId)
                ->addRegionFilter($regionIds);
            $collection->getSelect()->join(
                ['reg' => $this->_getResource()->getTable('directory_country_region')],
                'main_table.region_id = reg.region_id',
                ['region_name' => 'default_name']
            );

            if ($collection) {
                foreach($collection as $item) {
                    $result[] = __('%1 is already assigned to this seller.', $item->getRegionName());
                }
            }
        }

        return $result;
    }
}
