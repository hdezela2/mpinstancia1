<?php

namespace Formax\RegionalCondition\Model\ResourceModel\ValidationRegionalCondition;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Formax\RegionalCondition\Model\ValidationRegionalCondition::class,
            \Formax\RegionalCondition\Model\ResourceModel\ValidationRegionalCondition::class
        );

        $this->addOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
    }

    /**
     * Filter by website_id
     *
     * @param int|array $website_id
     * @return $this
     */
    public function addWebsiteFilter($website_id)
    {
        if (!empty($website_id)) {
            if (is_array($website_id)) {
                $this->addFieldToFilter('main_table.website_id', ['in' => $website_id]);
            } else {
                $this->addFieldToFilter('main_table.website_id', $website_id);
            }
        }

        return $this;
    }
}
