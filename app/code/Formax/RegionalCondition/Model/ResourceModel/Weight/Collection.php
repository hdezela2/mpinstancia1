<?php

namespace Formax\RegionalCondition\Model\ResourceModel\Weight;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Formax\RegionalCondition\Model\Weight::class,
            \Formax\RegionalCondition\Model\ResourceModel\Weight::class
        );

        $this->addOrder('weight_from', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
    }
}
