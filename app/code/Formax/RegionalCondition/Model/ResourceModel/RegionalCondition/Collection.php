<?php

namespace Formax\RegionalCondition\Model\ResourceModel\RegionalCondition;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Formax\RegionalCondition\Model\RegionalCondition::class,
            \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition::class
        );
    }

    /**
     * Initialize select object
     *
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        
        $this->getSelect()->join(
            ['rel' => $this->getTable('dccp_regional_condition_seller')],
            'main_table.id = rel.regional_condition_id',
            ['seller_id', 'website_id', 'regional_percentage']
        );

        return $this;
    }

    /**
     * Filter by seller_id
     *
     * @param mixed int|array $sellerId
     * @return $this
     */
    public function addSellerFilter($sellerId)
    {
        if (!empty($sellerId)) {
            if (is_array($sellerId)) {
                $this->addFieldToFilter('rel.seller_id', ['in' => $sellerId]);
            } else {
                $this->addFieldToFilter('rel.seller_id', $sellerId);
            }
        }

        return $this;
    }

    /**
     * Filter by website_id
     *
     * @param int|array $websiteId
     * @return $this
     */
    public function addWebsiteFilter($websiteId)
    {
        if (!empty($websiteId)) {
            if (is_array($websiteId)) {
                $this->addFieldToFilter('rel.website_id', ['in' => $websiteId]);
            } else {
                $this->addFieldToFilter('rel.website_id', $websiteId);
            }
        }

        return $this;
    }

    /**
     * Filter by region_id
     *
     * @param int|array $regionId
     * @return $this
     */
    public function addRegionFilter($regionId)
    {
        if (!empty($regionId)) {
            if (is_array($regionId)) {
                $this->addFieldToFilter('main_table.region_id', ['in' => $regionId]);
            } else {
                $this->addFieldToFilter('main_table.region_id', $regionId);
            }
        }

        return $this;
    }
}
