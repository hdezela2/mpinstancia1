<?php

namespace Formax\RegionalCondition\Model\ResourceModel;

class RegionalConditionSeller extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main and comuna name tables
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('dccp_regional_condition_seller', 'id');
    }
}