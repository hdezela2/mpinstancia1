<?php

namespace Formax\RegionalCondition\Model\ResourceModel\RegionalConditionSeller;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Formax\RegionalCondition\Model\RegionalConditionSeller::class,
            \Formax\RegionalCondition\Model\ResourceModel\RegionalConditionSeller::class
        );
    }
}
