<?php

namespace Formax\RegionalCondition\Model;

class Weight extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\RegionalCondition\Model\ResourceModel\Weight::class);
    }
}