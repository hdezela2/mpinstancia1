<?php

namespace Formax\RegionalCondition\Block\RegionalCondition;

use Magento\Framework\View\Element\Template;
use Formax\RegionalCondition\Helper\Config as HelperConfig;

class Matriz extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Formax\RegionalCondition\Helper\Data
     */
    protected $helper;

    /**
     * @param Template\Context $context
     * @param \Formax\RegionalCondition\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Formax\RegionalCondition\Helper\Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;

        parent::__construct($context, $data);
    }

    /**
     * Retrieve an instance of Density Collection
     * 
     * @return \Formax\RegionComuna\Model\ResourceModel\Density\CollectionFactory
     */
    public function getDensities()
    {
        return $this->helper->getDensities();
    }

    /**
     * Retrieve an instance of Weight Collection
     * 
     * @return \Formax\RegionalCondition\Model\ResourceModel\Weight\CollectionFactory
     */
    public function getWeight()
    {
        return $this->helper->getWeight();
    }

    /**
     * Retrieve array tipification
     * 
     * @return array
     */
    public function getTipification()
    {
        return HelperConfig::TIPIFICATION;
    }

    /**
     * Retieve an instance of Helper Data
     * 
     * @return \Formax\RegionalCondition\Helper\Data
     */
    public function getHelper()
    {
        return $this->helper;
    }
}