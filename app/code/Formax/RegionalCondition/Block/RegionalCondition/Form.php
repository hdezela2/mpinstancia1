<?php

namespace Formax\RegionalCondition\Block\RegionalCondition;

use Magento\Framework\View\Element\Template;

class Form extends Template
{
    /**
     * @var \Formax\RegionComuna\Model\ResourceModel\Comuna\CollectionFactory
     */
    protected $comunaCollectionFactory;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    protected $regionCollectionFactory;

    /**
     * @var \Formax\RegionalCondition\Helper\Data
     */
    protected $helper;

    /**
     * @param Template\Context $context
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory
     * @param \Formax\RegionComuna\Model\ResourceModel\Comuna\CollectionFactory $comunaCollectionFactory
     * @param \Formax\RegionalCondition\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Formax\RegionComuna\Model\ResourceModel\Comuna\CollectionFactory $comunaCollectionFactory,
        \Formax\RegionalCondition\Helper\Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->regionCollectionFactory = $regionCollectionFactory;
        $this->comunaCollectionFactory = $comunaCollectionFactory;

        parent::__construct($context, $data);
    }

    /**
     * Retieve regions
     * 
     * @return array
     */
    public function getRegions()
    {
        $region = $this->regionCollectionFactory->create();
        return $region->addCountryFilter('CL')->toOptionArray();
    }

    /**
     * Retieve comunas by regionId
     * 
     * @param int $regionId
     * @return array
     */
    public function getComunas($regionId = 0)
    {
        $comunas = $this->comunaCollectionFactory->create();
        if ((int)$regionId > 0) {
            $comunas->addRegionFilter($regionId)->toOptionArray();       
        }

        return $comunas->toOptionArray();
    }

    /**
     * Retieve an instance of Helper Data
     * 
     * @return \Formax\RegionalCondition\Helper\Data
     */
    public function getHelper()
    {
        return $this->helper;
    }

    /**
     * Retrieve options yes/No
     * 
     * @return array
     */
    public function getYesNo()
    {
        return [
            0 => __('No'),
            1 => __('Yes')
        ];
    }

    /**
     * Retrieve options emergency storepickup days
     * 
     * @return array
     */
    public function getEmergencyStorepickupDays()
    {
        return [
            0 => __('No'),
            1 => __('%1 day', '1'),
            2 => __('%1 days', '2'),
            3 => __('%1 days', '3'),
            4 => __('%1 days', '4'),
        ];
    }
}