<?php

namespace Formax\RegionalCondition\Block\RegionalCondition;

use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Magento\Framework\View\Element\Template;
use Intellicore\EmergenciasRenewal\Constants;

class Container extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory
     */
    protected $regionalConditionCollectionFactory;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    protected $regionCollectionFactory;

    /**
     * @var \Webkul\SellerSubAccount\Helper\Data
     */
    protected $helper;

    /**
     * @param Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession,
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
     * @param \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $regionalConditionCollectionFactory
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory
     * @param \Webkul\SellerSubAccount\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $regionalConditionCollectionFactory,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Webkul\SellerSubAccount\Helper\Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->storeManager = $storeManager;
        $this->customerSession = $customerSession;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->regionalConditionCollectionFactory = $regionalConditionCollectionFactory;
        $this->regionCollectionFactory = $regionCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * Retrive URL post action
     * @return string
     */
    public function getPostUrl()
    {
        return $this->getUrl('regionalcondition/index/post');
    }

    /**
     * Retrive URL Add action
     * @return string
     */
    public function getAddOfferUrl()
    {
        return $this->getUrl('regionalcondition/index/add');
    }

    /**
     * Retrive Customer Id logged In
     * @return int
     */
    public function getSellerId()
    {
        return $this->helper->getSubAccountSellerId();
    }

    /**
     * Retrive DCCP Seller Id logged In
     * @return string
     */
    public function getDccpSellerId()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customer = $this->customerSession->getCustomer();
            $customerRepository = $this->customerRepositoryInterface->getById($customer->getId());
            return $customerRepository->getCustomAttribute('user_rest_id') ?
                $customerRepository->getCustomAttribute('user_rest_id')->getValue() : false;
        }

        return false;
    }

    /**
     * If sho add new region button
     * @return bool
     */
    public function showAddNewRegion()
    {
        $show = false;
        $sellerId = (int)$this->getSellerId();
        $websiteId = (int)$this->storeManager->getStore()->getWebsiteId();
        $storeCode = $this->storeManager->getStore()->getCode();

        $storeCodes = [Constants::STORE_CODE, AseoRenewalConstants::STORE_CODE ];
        if ($show && $websiteId && $sellerId || in_array($storeCode, $storeCodes)) {
            return $this->getTotalRowsBySellerAndAgreement($sellerId, $websiteId);
        }

        return false;
    }

    /**
     * Retrive total records by seller ID and agreement ID
     * @param int $sellerId
     * @param int $websiteId
     */
    private function getTotalRowsBySellerAndAgreement($sellerId, $websiteId)
    {
        if ((int)$sellerId > 0 && (int)$websiteId > 0) {
            $regionalConditionCollection = $this->regionalConditionCollectionFactory->create();
            $totalRegionalCondition = $regionalConditionCollection
                ->addSellerFilter($sellerId)
                ->addWebsiteFilter($websiteId)->getSize();

            $regionCollection = $this->regionCollectionFactory->create();
            $totalRegion = $regionCollection->addCountryFilter('CL')->getSize();

            return !((int)$totalRegion === (int)$totalRegionalCondition);
        }

        return false;
    }
}
