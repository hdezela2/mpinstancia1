<?php

namespace Formax\RegionalCondition\Block\RegionalCondition;

use Magento\Framework\View\Element\Template;
use Intellicore\EmergenciasRenewal\Constants;
use Formax\RegionalCondition\Block\RegionalCondition\Container;

class Add extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory
     */
    protected $regionalConditionCollectionFactory;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    protected $regionCollectionFactory;

    /**
     * @param Template\Context $context
     * @param \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $regionalConditionCollectionFactory
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $regionalConditionCollectionFactory,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Session\SessionManagerInterface $session,
        Container $container,
        array $data = []
    ) {
        $this->regionalConditionCollectionFactory = $regionalConditionCollectionFactory;
        $this->regionCollectionFactory = $regionCollectionFactory;
        $this->container = $container;
        $this->storeManager = $storeManager;
        $this->session = $session;
        parent::__construct($context, $data);
    }

    /**
     * Retrive URL post action
     * @return string
     */
    public function getPostUrl()
    {
        return $this->container->getPostUrl();
    }

    /**
     * Retrive Customer Id logged In
     * @return int
     */
    public function getSellerId()
    {
        return $this->container->getSellerId();
    }

    /**
     * Retrive DCCP Seller Id logged In
     * @return string
     */
    public function getDccpSellerId()
    {
        return $this->container->getDccpSellerId();
    }

    /**
     * Retrive total records by seller ID and agreement ID
     * @param int $sellerId
     * @param int $websiteId
     */
    public function getRegions($sellerId)
    {
        $websiteId = (int)$this->storeManager->getStore()->getWebsiteId();
        
        if ((int)$sellerId > 0 && (int)$websiteId > 0) {
            $regionalConditionCollection = $this->regionalConditionCollectionFactory->create();
            $regionalConditionId = $regionalConditionCollection
                ->addSellerFilter($sellerId)
                ->addWebsiteFilter($websiteId)->getData();

            $regionalConditionId = array_column($regionalConditionId, 'region_id');

            $regionCollection = $this->regionCollectionFactory->create();
            $totalRegion = $regionCollection->addCountryFilter('CL')->getData();
            $remainRegions = [];
            foreach ($totalRegion as $value) {
                if (!in_array($value['region_id'], $regionalConditionId)) {
                    $remainRegions[] = [
                        'region_id' => $value['region_id'],
                        'name' => $value['name']
                    ];
                }
            }
            return $remainRegions;
        }
    }

    public function getNewRegionalConditionFormData(){

        return $this->session->getNewRegionalConditionFormData();
    }

    public function unsetNewRegionalConditionFormData(){
        
        return $this->session->unsNewRegionalConditionFormData();
    }
}