require([
    "jquery",
    "Magento_Catalog/js/price-utils",
    "mage/validation",
    "Magento_Ui/js/modal/modal"
  ], function($, priceUtils) {
    $(document).ready(function() {
        var fieldCount = parseInt($("#field_count").val());
        var fieldCountOriginal = fieldCount;

        var $all = $('.content-to-clone:not(:first)');
        $all.removeClass('active');
        $all.find('.regional-rows-box').slideUp(200);

        // prepera fields depend on selection
        $(".container-regional-condition-form").each(function(index) {
            var storepickup = $("#enable_storepickup_" + index);
            var storepickupDays = $("#days_storepickup_" + index);
            var storepickupEmergency = $("#enable_emergency_storepickup_" + index);
            var storepickupDaysEmergency = $("#days_emergency_storepickup_" + index);

            if (storepickup.val() == "0") {
                storepickupDays.val("0");
                storepickupDays.attr("readonly", true);
            } else {
                storepickupDays.remove("readonly");
            }

            if (storepickupEmergency.val() == "0") {
                storepickupDaysEmergency.val("0");
                storepickupDaysEmergency.attr("readonly", true);
            } else {
                storepickupDaysEmergency.remove("readonly");
            }
        });

        $(document).on('click', '.accordeon-box .overlay-avoidclick', function() {
            var $box = $(this).closest('.accordeon-box');
            var $all = $('.content-to-clone');
            $all.removeClass('active');
            $all.find('.regional-rows-box').slideUp(200);
            $box.addClass('active');
            $box.find('.regional-rows-box').slideDown(200);
        });

        $(".add-new-region").click(function() {
            var elem = $(".content-to-clone:first").clone().appendTo(".container-form-matriz");
            elem.find(".container-btn:first .delete-region").removeClass("hide");
            $("#field_count").val(++fieldCount).change();
            
            elem.find(".container-regional-condition-fields select").val("");
            elem.find(".container-regional-condition-fields input").val("");
            elem.find(".container-regional-condition-fields select.region_id").removeAttr("style");
            elem.find(".container-regional-condition-fields select.enable-storepickup:first").val("0");
            elem.find(".container-regional-condition-fields input.days-storepickup:first").val("0");
            elem.find(".container-regional-condition-fields input.maximum-delivery:first").val("0");
            elem.find(".container-regional-condition-fields select.enable-emergency-storepickup:first").val("0");
            elem.find(".container-regional-condition-fields input.days-emergency-storepickup:first").val("0");
            elem.find(".container-regional-condition-fields input.maximum-emergency-delivery:first").val("0");
            elem.find(".container-regional-condition-fields input.days-storepickup").attr("readonly", true);
            elem.find(".container-regional-condition-fields input.days-emergency-storepickup").attr("readonly", true);

            var $all = $('.content-to-clone');
            $all.removeClass('active');
            $all.find('.regional-rows-box').slideUp(200);
            elem.addClass('active');
            elem.find('.regional-rows-box').slideDown(200);
        });

        $(document).on("click", ".delete-region", function () {
            var $el = $(this).closest(".content-to-clone");
            $el.prev().find('.overlay-avoidclick').trigger('click');
            $el.remove();
            $("#field_count").val(--fieldCount).change();
        });

        $(document).on("click", ".enable-storepickup", function () {
            var $el = $(this).closest(".content-to-clone");
            if ($(this).val() == "0") {
                $el.find(".days-storepickup:first").val("0");
                $el.find(".days-storepickup:first").attr("readonly", true);
            } else {
                $el.find(".days-storepickup:first").removeAttr("readonly");
            }
        });

        $(document).on("click", ".enable-emergency-storepickup", function () {
            var $el = $(this).closest(".content-to-clone");
            if ($(this).val() == "0") {
                $el.find(".days-emergency-storepickup:first").val("0");
                $el.find(".days-emergency-storepickup:first").attr("readonly", true);
            } else {
                $el.find(".days-emergency-storepickup:first").removeAttr("readonly");
            }
        });

        $(document).on("focus", "#form-regional-condition input", function() {
            if ($(this).hasClass("format-number-unlimited") || $(this).hasClass("format-price-unlimited")
                || $(this).hasClass("format-number") || $(this).hasClass("format-price")) {
                unFormatedInput($(this));
            }
        });

        $("#form-regional-condition").submit(function(event) {
            // Remove values format before submit
            $("input.format-number-unlimited, input.format-price-unlimited, input.format-number, input.format-price").each(function() {
                unFormatedInput($(this));
            });

            if ($(this).valid()) {
                if (fieldCount > fieldCountOriginal) {
                    modalConfirmSave($(this));
                } else {
                    $(this).unbind().submit();
                }
            }

            return false;
        });

        function unFormatedInput(element) {
            var val = $.trim(element.val()).toLowerCase();
            if (val === 'ilimitado' || val === 'ilimitada' || val === 'ilimitados' || val === 'ilimitadas') {
                element.val('Ilimitado');
            } else {
                var numb = val.replace(/[^0-9]/g,'');
                element.val(numb === '' ? element.val() : numb);
            }

            return element;
        }

        function modalConfirmSave (form) {
            $(".wk-mp-page-wrapper").append("<div id='confirm-regional-condition-popup'></div>");
            var $target = $("#confirm-regional-condition-popup"),
                confirmText = $.mage.__("Are you sure to save the form?");
            $target.text(confirmText);
            var options = {
                responsive: true,
                innerScroll: true,
                modalClass: "confirm-regional-condition-modal confirm",
                title: null,
                closeText: "Cerrar",
                buttons: [
                    {
                        text: $.mage.__("No"),
                        class: "",
                        attr: {},
                        click: function (event) {
                            this.closeModal(event);
                        }
                    },
                    {
                        text: $.mage.__("Yes"),
                        class: "action-primary",
                        attr: {},
                        click: function (event) {
                            this.closeModal(event);
                            form.unbind().submit();
                        }
                    }
                ]
            };
            $target.modal(options);
            $target.modal("openModal");
        }
    });
});
