<?php

namespace Formax\Storepickup\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Framework\Session\SessionManager;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Model\Quote\Item\OptionFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\AddressFactory;
use Webkul\MarketplaceBaseShipping\Model\ShippingSettingRepository;
use Formax\Storepickup\Model\ResourceModel\Storepickup\CollectionFactory as StorepickupCollectionFactory;

/**
 * Custom shipping model
 */
class Storepickup extends \Webkul\MarketplaceBaseShipping\Model\Carrier\AbstractCarrier
    implements CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'dccp_storepickup';

    /**
     * @var bool
     */
    protected $_isFixed = true;

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    private $rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    private $rateMethodFactory;

    /**
     * @var StorepickupCollectionFactory
     */
    protected $storepickupCollectionFactory;

    /**
     * @var \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory
     */
    protected $assignProductCollectionFactory;

    /**
     * @var \Chilecompra\WebkulMpShipping\Helper\Data
     */
    protected $shippingHelper;

    /**
     * Raw rate request data
     *
     * @var \Magento\Framework\DataObject|null
     */
    protected $_rawRequest = null;

    /**
     * Raw rate request data
     *
     * @var \Magento\Framework\DataObject|null
     */
    protected $baseRequest = null;

    /**
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param SessionManager $coreSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\App\RequestInterface $requestInterface
     * @param PriceCurrencyInterface $priceCurrency
     * @param OptionFactory $quoteOptionFactory
     * @param CustomerFactory $customerFactory
     * @param AddressFactory $addressFactory
     * @param \Webkul\Marketplace\Model\ProductFactory $marketplaceProductFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory
     * @param ShippingSettingRepository $shippingSettingRepository
     * @param StorepickupCollectionFactory $storepickupCollectionFactory
     * @param \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory $assignProductCollectionFactory
     * @param \Chilecompra\WebkulMpShipping\Helper\Data $shippingHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        SessionManager $coreSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\RequestInterface $requestInterface,
        PriceCurrencyInterface $priceCurrency,
        OptionFactory $quoteOptionFactory,
        CustomerFactory $customerFactory,
        AddressFactory $addressFactory,
        \Webkul\Marketplace\Model\ProductFactory $marketplaceProductFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory,
        ShippingSettingRepository $shippingSettingRepository,
        StorepickupCollectionFactory $storepickupCollectionFactory,
        \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory $assignProductCollectionFactory,
        \Chilecompra\WebkulMpShipping\Helper\Data $shippingHelper,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->storepickupCollectionFactory = $storepickupCollectionFactory;
        $this->assignProductCollectionFactory = $assignProductCollectionFactory;
        $this->shippingHelper = $shippingHelper;
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $rateResultFactory,
            $rateMethodFactory,
            $regionFactory,
            $coreSession,
            $checkoutSession,
            $customerSession,
            $currencyFactory,
            $storeManager,
            $localeFormat,
            $jsonHelper,
            $requestInterface,
            $priceCurrency,
            $quoteOptionFactory,
            $customerFactory,
            $addressFactory,
            $marketplaceProductFactory,
            $productFactory,
            $saleslistFactory,
            $shippingSettingRepository,
            $data
        );
    }

    /**
     * Prepare and set request to this instance.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function setRequest(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        /**
         * @todo: Refactor $this->shippingHelper->getSellerIdFromQuote();
         *
         * Calls to $this->shippingHelper->getSellerIdFromQuote()
         * causes infinite loops, so we are initializing it as an empty array
         *
         * $sellerMapping = []; instead of
         * $sellerMapping = $this->shippingHelper->getSellerIdFromQuote()
         */

        $mpassignproductId = 0;
        $shippingdetail = [];
        $sellerProductDetails = [];
        $sellerMapping = $this->shippingHelper->getSellerIdFromQuote();

        foreach ($request->getAllItems() as $item) {
            if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                continue;
            }
            $sellerId = array_key_exists($item->getId(), $sellerMapping) ? $sellerMapping[$item->getId()] : 0;
            $weight = $this->_getItemWeight($item);
            $itemPrice = $item->getRowTotal();
            list($originPostcode, $originCountryId, $origRegionCode, $originCity) = $this->_getSellerOrigin($sellerId);

            if (empty($shippingdetail)) {
                array_push(
                    $shippingdetail,
                    [
                        'seller_id' => $sellerId,
                        'origin_postcode' => $originPostcode,
                        'origin_country_id' => $originCountryId,
                        'origin_region' => $origRegionCode,
                        'origin_city' => $originCity,
                        'items_weight' => $weight,
                        'total_amount'=> $itemPrice,
                        'product_name' => $item->getName(),
                        'qty' => $item->getQty(),
                        'item_id' => $item->getId(),
                        'price' => $item->getPrice()*$item->getQty(),
                    ]
                );
                $sellerProductDetails[$sellerId][] = $item->getName().'x'.$item->getQty();
            } else {
                $shipinfoflag = true;
                $index = 0;
                foreach ($shippingdetail as $itemship) {
                    if ($itemship['seller_id'] == $sellerId) {
                        $itemship['items_weight'] = $itemship['items_weight'] + $weight;
                        $itemship['total_amount']= $itemship['total_amount']+$itemPrice;
                        $itemship['product_name'] = $itemship['product_name'].','.$item->getName();
                        $itemship['item_id'] = $itemship['item_id'].','.$item->getId();
                        $itemship['qty'] = $itemship['qty'] + $item->getQty();
                        $itemship['price'] = $itemship['price'] + $item->getPrice()*$item->getQty();
                        $shippingdetail[$index] = $itemship;
                        $shipinfoflag = false;
                        $sellerProductDetails[$sellerId][] = $item->getName().' X '.$item->getQty();
                    }
                    ++$index;
                }
                if ($shipinfoflag == true) {
                    array_push(
                        $shippingdetail,
                        [
                            'seller_id' => $sellerId,
                            'origin_postcode' => $originPostcode,
                            'origin_country_id' => $originCountryId,
                            'origin_region' => $origRegionCode,
                            'origin_city' => $originCity,
                            'items_weight' => $weight,
                            'total_amount'=> $itemPrice,
                            'product_name' => $item->getName(),
                            'qty' => $item->getQty(),
                            'item_id' => $item->getId(),
                            'price' => $item->getPrice()*$item->getQty(),
                        ]
                    );
                    $sellerProductDetails[$sellerId][] = $item->getName().' X '.$item->getQty();
                }
            }
        }

        $request->setSellerProductInfo($sellerProductDetails);

        if ($request->getShippingDetails()) {
            $shippingdetail = $request->getShippingDetails();
        }
        $request->setShippingDetails($shippingdetail);

        if ($request->getDestCountryId()) {
            $destCountry = $request->getDestCountryId();
        } else {
            $destCountry = self::USA_COUNTRY_ID;
        }

        $request->setDestCountryId($destCountry);

        if ($request->getDestPostcode()) {
            $request->setDestPostal($request->getDestPostcode());
        }
        $this->setRawRequest($request);

        if ($request->getDestRegionId()) {
            $regionId = $request->getDestRegionId();
        } else {
            $regionId = 0;
        }

        $request->setDestRegionId($regionId);
    }

    /**
     * Collect and get rates.
     *
     * @param RateRequest $request
     *
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Error|bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        $this->baseRequest = $request;
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $this->setRequest($request);
        if ($request->getDestRegionId() === 0) {
            return false;
        }
        $shippingpricedetail = $this->getShippingPricedetail($this->_rawRequest);

        return $shippingpricedetail;
    }

    /**
     * Calculate the rate according to Tabel Rate shipping defined by the sellers.
     *
     * @return Result
     */
    public function getShippingPricedetail(RateRequest $request)
    {
        $price = 0;
        $sellerId = 0;
        $flag = false;
        $requestData = $request;
        $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();

        /**
         * @todo: Refactor $this->getSellerId();
         *
         * Calls to $this->getSellerId()
         * causes infinite loops, so we are initializing it as 0
         *
         * $sellerMapping = []; instead of
         * $sellerMapping = $this->getSellerId();
         */
        $sellerMapping = $this->getSellerId();
        foreach ($requestData->getShippingDetails() as $shipdetail) {
            $sellerId = isset($shipdetail) ? (int)$shipdetail['seller_id'] : 0;
            if ($sellerId === 0 && isset($shipdetail['item_id'])) {
                $sellerId = array_key_exists($shipdetail['item_id'], $sellerMapping) ? $sellerMapping[$shipdetail['item_id']] : 0;
                $shipdetail['seller_id'] = $sellerId;
            }
        }

        if ($sellerId > 0) {
            $price = (int)$this->getConfigData('shipping_cost');
            $collection = $this->storepickupCollectionFactory->create()
                ->addSellerFilter($sellerId)
                ->addWebsiteFilter($websiteId)
                ->addRegionFilter($requestData->getDestRegionId())
                ->addFieldToFilter('active', 1);

            if ($collection) {
                foreach ($collection as $item) {
                    $flag = true;
                }
            }
        }

        if ($flag) {
            /** @var \Magento\Shipping\Model\Rate\Result $result */
            $result = $this->rateResultFactory->create();

            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
            $method = $this->rateMethodFactory->create();

            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod($this->_code);
            $method->setMethodTitle($this->getConfigData('name'));

            $shippingCost = (float)$this->getConfigData('shipping_cost');

            $method->setPrice($price);
            $method->setCost($price);

            $result->append($method);

            return $result;
        }

        return $this->returnErrorFromConfig();
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    /**
     * Get Seller ID current quote
     *
     * @return bool|int
     */
    public function getSellerId()
    {
        return $this->shippingHelper->getSellerIdFromQuote();
    }

    /**
     * get product weight.
     *
     * @param object $item
     *
     * @return int
     */
    protected function _getItemWeight($item)
    {
        $weight = 0;
        if ($item->getHasChildren()) {
            $childWeight = 0;
            foreach ($item->getChildren() as $child) {
                if ($child->getProduct()->isVirtual()) {
                    continue;
                }
                $productWeight = $child->getProduct()->getWeight();
                $childWeight += $productWeight * $child->getQty();
            }
            $weight = $childWeight * $item->getQty();
        } else {
            $productWeight = $item->getProduct()->getWeight();
            $weight = $productWeight * $item->getQty();
            if ($item->getQtyOrdered()) {
                $weight = $productWeight * $item->getQtyOrdered();
            }
        }
        return $weight;
    }
}
