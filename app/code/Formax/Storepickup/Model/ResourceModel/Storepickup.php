<?php

namespace Formax\Storepickup\Model\ResourceModel;

class Storepickup extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main  table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('dccp_storepickup', 'id');
    }

    /**
     * Retrive one row by unique keys (dest_region_id, seller_id, website_id)
     * 
     * @param int $regionId
     * @param int $sellerId
     * @param int $websiteId
     * @return array
     */
    public function loadUniqueRow($regionId, $sellerId, $websiteId)
    {
        $table = $this->getMainTable();
        $regionQuote = $this->getConnection()->quoteInto('dest_region_id = ?', $regionId);
        $sellerQuote = $this->getConnection()->quoteInto('seller_id = ?', $sellerId);
        $websiteQuote = $this->getConnection()->quoteInto('website_id = ?', $websiteId);
        $sql = $this->getConnection()->select()->from($table, ['id'])
            ->where($regionQuote)->where($sellerQuote)->where($websiteQuote);
        $id = $this->getConnection()->fetchRow($sql);
        
        return $id;
    }
}
