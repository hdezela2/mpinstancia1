<?php

namespace Formax\Storepickup\Model\ResourceModel\Storepickup;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define main  tables
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Formax\Storepickup\Model\Storepickup::class,
            \Formax\Storepickup\Model\ResourceModel\Storepickup::class
        );
    }

    /**
     * Filter by seller_id
     *
     * @param int|array $sellerId
     * @return $this
     */
    public function addSellerFilter($sellerId)
    {
        if (!empty($sellerId)) {
            if (is_array($sellerId)) {
                $this->addFieldToFilter('main_table.seller_id', ['in' => $sellerId]);
            } else {
                $this->addFieldToFilter('main_table.seller_id', $sellerId);
            }
        }

        return $this;
    }

    /**
     * Filter by website_id
     *
     * @param int|array $websiteId
     * @return $this
     */
    public function addWebsiteFilter($websiteId)
    {
        if (!empty($websiteId)) {
            if (is_array($websiteId)) {
                $this->addFieldToFilter('main_table.website_id', ['in' => $websiteId]);
            } else {
                $this->addFieldToFilter('main_table.website_id', $websiteId);
            }
        }

        return $this;
    }

    /**
     * Filter by dest_region_id
     *
     * @param int|array $regionId
     * @return $this
     */
    public function addRegionFilter($regionId)
    {
        if (!empty($regionId)) {
            if (is_array($regionId)) {
                $this->addFieldToFilter('main_table.dest_region_id', ['in' => $regionId]);
            } else {
                $this->addFieldToFilter('main_table.dest_region_id', $regionId);
            }
        }

        return $this;
    }
}
