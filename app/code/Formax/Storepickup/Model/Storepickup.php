<?php

namespace Formax\Storepickup\Model;

class Storepickup extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\Storepickup\Model\ResourceModel\Storepickup::class);
    }

    /**
     * Retrive one row by unique keys (dest_region_id, seller_id, website_id)
     * 
     * @param int $regionId
     * @param int $sellerId
     * @param int $websiteId
     * @return array
     */
    public function loadUniqueRow($regionId, $sellerId, $websiteId)
    {
        if (!$regionId) {
            $regionId = $this->getDestRegionId();
        }
        if (!$sellerId) {
            $sellerId = $this->getSellerId();
        }
        if (!$websiteId) {
            $websiteId = $this->getWebsiteId();
        }
        $id = $this->getResource()->loadUniqueRow($regionId, $sellerId, $websiteId);

        if (isset($id['id']) && (int)$id['id'] > 0) {
            return $this->load($id['id']);
        } else {
            return false;
        }
        // return isset($id['id']) && (int)$id['id'] > 0 ? $this->load($id['id']) : $this->load();
    }
}