/**
 * Copyright © Formax, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-rates-validation-rules',
    '../model/shipping-rates-validator',
    '../model/shipping-rates-validation-rules'
], function (
    Component,
    defaultShippingRatesValidator,
    defaultShippingRatesValidationRules,
    storepickupShippingRatesValidator,
    storepickupShippingRatesValidationRules
) {
    'use strict';

    defaultShippingRatesValidator.registerValidator('storepickup', storepickupShippingRatesValidator);
    defaultShippingRatesValidationRules.registerRules('storepickup', storepickupShippingRatesValidationRules);

    return Component;
});
