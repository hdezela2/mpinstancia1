<?php

namespace Formax\SyncPrice\Cron;

use Formax\SyncPrice\Helper\Data as Helper;

class SyncPrice
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param Helper $helper
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        Helper $helper
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
    }

    public function execute()
    {
        $result = $this->helper->syncPrice();
        $msg = isset($result['msg']) ? $result['msg'] : '';
        
        if (!$result['error']) {
            $this->logger->info('Cron Task Success Sync Price Webkul to Magento');
        } else {
            $msg = isset($result['msg']) ?: null;
            $this->logger->critical('Cron Task Error Sync Price Webkul to Magento => ' . $msg);
        }
    }
}
