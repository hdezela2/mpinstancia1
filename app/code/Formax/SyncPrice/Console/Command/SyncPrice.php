<?php

namespace Formax\SyncPrice\Console\Command;

use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Formax\SyncPrice\Helper\Data as Helper;

class SyncPrice extends Command
{
    /**
     * @var string
     */
    const OPTION_SKU_CODE = 'skus';

    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @param \Magento\Framework\App\State $appState
     * @param Helper $helper
     * @param string $name
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        Helper $helper,
        $name = null
    ) {
        $this->helper = $helper;
        $this->appState = $appState;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('product:syncpricewktomage');
        $this->setDescription('Sync price between Webkul and Magento. You can pass as parameter SKUs comma-separated');
        $this->addOption(
            self::OPTION_SKU_CODE,
            null,
            InputOption::VALUE_OPTIONAL,
            'SKUs must be string comma-separated'
        );

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $param = $input->getOption(self::OPTION_SKU_CODE);
        $skus = $param !== null ? explode(',', $param) : null;
        $result = $this->helper->syncPrice($skus);
        $output->writeln($result['msg']);

        return Cli::RETURN_SUCCESS;
    }
}
