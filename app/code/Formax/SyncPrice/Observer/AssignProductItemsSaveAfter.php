<?php

namespace Formax\SyncPrice\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Formax\SyncPrice\Logger\Logger;
use Formax\SyncPrice\Helper\Data as Helper;
use Magento\Framework\Event\Observer as EventObserver;

class AssignProductItemsSaveAfter implements ObserverInterface
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @param Helper $helper
     * @param Logger $logger
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Helper $helper,
        Logger $logger,
        ProductRepositoryInterface $productRepository
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
    }

    /**
     * Update price of product
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        $model = $observer->getEvent()->getObject();        
        $productId = $model->getProductId();
        
        try {
            $product = $this->productRepository->getById($productId);
            $pricesData = $this->helper->getProductPrices(null, $productId, false);
            
            if (isset($pricesData[$productId])) {
                $prices = $pricesData[$productId];
                if ($product->getTypeId() == 'configurable') {
                    $children = $product->getTypeInstance()->getUsedProducts($product);
                    foreach ($children as $child) {
                        $chilProduct = $this->productRepository->getById($child->getId());
                        if ($chilProduct->getPrice() == $prices['price']
                            && $chilProduct->getSpecialPrice() == $prices['special_price']
                            && $chilProduct->getSpecialFromDate() == $prices['special_from_date']
                            && $chilProduct->getSpecialToDate() == $prices['special_to_date']
                        ) {
                            break;
                        }
                        $chilProduct->setPrice($prices['price']);
                        $chilProduct->setSpecialPrice($prices['special_price']);
                        $chilProduct->setSpecialFromDate($prices['special_from_date']);
                        $chilProduct->setSpecialToDate($prices['special_to_date']);
                        $this->productRepository->save($chilProduct);
                    }
                } else {
                    if ($product->getPrice() != $prices['price']
                        || $product->getSpecialPrice() != $prices['special_price']
                        || $product->getSpecialFromDate() != $prices['special_from_date']
                        || $product->getSpecialToDate() != $prices['special_to_date']
                    ) {
                        $product->setPrice($prices['price']);
                        $product->setSpecialPrice($prices['special_price']);
                        $product->setSpecialFromDate($prices['special_from_date']);
                        $product->setSpecialToDate($prices['special_to_date']);
                        $this->productRepository->save($product);
                    }
                }
            }
        } catch (NoSuchEntityException $e) {
            $this->logger->warning('AssignProductItemsSaveAfter - Product ID ' . $productId . ' not exists.');
        } catch (\Exception $e) {
            $this->logger->error('AssignProductItemsSaveAfter - ' . $e->getMessage());
        }
    }
}
