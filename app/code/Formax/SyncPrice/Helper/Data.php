<?php
/**
 * Formax Software.
 *
 * @category Formax
 * @package  Formax_SyncPrice
 * @author   Formax
 */
namespace Formax\SyncPrice\Helper;

use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollection;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var AssociatesCollection
     */
    protected $associatesCollection;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Formax\SyncPrice\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var EavAttribute
     */
    protected $eavAttribute;

    /**
     * @var \Summa\EmergenciasSetUp\Helper\Data
     */
    protected $_emergencyHelper;

    /**
     * @var \Summa\MobiliarioSetUp\Helper\Data
     */
    protected $furnitureHelper;


    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Formax\SyncPrice\Logger\Logger $logger
     * @param AssociatesCollection $associatesCollectionFactory
     * @param EavAttribute $eavAttribute
     * @param \Summa\EmergenciasSetUp\Helper\Data $emergencyHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\Module\Manager $moduleManager,
        \Formax\SyncPrice\Logger\Logger $logger,
        AssociatesCollection $associatesCollectionFactory,
        EavAttribute $eavAttribute,
        \Summa\EmergenciasSetUp\Helper\Data $emergencyHelper,
        \Summa\MobiliarioSetUp\Helper\Data $furnitureHelper
    ) {
        $this->timezone = $timezone;
        $this->resource = $resource;
        $this->moduleManager = $moduleManager;
        $this->logger = $logger;
        $this->eavAttribute = $eavAttribute;
        $this->associatesCollection = $associatesCollectionFactory;
        $this->_emergencyHelper = $emergencyHelper;
        $this->furnitureHelper = $furnitureHelper;

        parent::__construct($context);
    }

    /**
     * Get assign product prices collection
     *
     * @param mixed array/string $sku [optional]
     * @param mixed array/int $productId [optional]
     * @param mixed bool $searchBySku [optional]
     * @return mixed array
     */
    public function getAssignProductPrices($sku = null, $productId = null, $searchBySku = true)
    {
        $connection = $this->resource->getConnection();
        $isModuleOfferEnable = $this->moduleManager->isOutputEnabled('Formax_Offer');
        $currentDate = $connection->quote($this->timezone->date()->format('Y-m-d'));
        $collection = $this->associatesCollection->create();
        $tableItems = $this->resource->getTableName('marketplace_assignproduct_items');
        $tableUserData = $this->resource->getTableName('marketplace_userdata');
        $tableCatalogProduct = $this->resource->getTableName('catalog_product_entity');

        $collection->getSelect()->reset(\Magento\Framework\DB\Select::COLUMNS)
                            ->reset(\Magento\Framework\DB\Select::GROUP)
                            ->reset(\Magento\Framework\DB\Select::ORDER);
        $collection->getSelect()->distinct()->joinRight(
            ['i' => $tableItems],
            'i.id = main_table.parent_id',
            [
                'price' => new \Zend_Db_Expr("IF(i.type = 'configurable', main_table.price, i.price)"),
                'product_row_id',
                'shipping_price',
                'assembly_price'
            ]
        )->joinLeft(
            ['u' => $tableUserData],
            'i.seller_id = u.seller_id',
            []
        )->joinLeft(
            ['e' => $tableCatalogProduct],
            'i.product_id = e.entity_id',
            ['entity_id', 'sku']
        )->where(new \Zend_Db_Expr("IF(i.type = 'configurable', main_table.qty, i.qty)") . '> 0')
        ->where(new \Zend_Db_Expr("IF(i.type = 'configurable', main_table.status, i.status)") . '= 1')
        ->where(new \Zend_Db_Expr("IF(i.type = 'configurable', main_table.dis_dispersion, i.dis_dispersion)") . '= 0');

        if ($isModuleOfferEnable) {
            $tableOffer = $this->resource->getTableName('dccp_offer_product');
            $collection->getSelect()->joinLeft(
                ['o' => $tableOffer],
                'i.id = o.assign_id AND o.status = 3 AND o.end_date >= ' . $currentDate,
                [
                    'start_date' => new \Zend_Db_Expr('DATE(start_date)'),
                    'end_date' => new \Zend_Db_Expr('DATE(end_date)'),
                    'base_price',
                    'special_price',
                    'seller_id',
                    'website_id'
                ]
            );
        }
        
        $collection->addFieldToFilter('u.is_seller', 1);

        if (!$searchBySku) {
            if (is_array($productId) && count($productId) > 0) {
                $collection->addFieldToFilter('i.product_id', ['in' => $productId]);
            } else {
                if ((int)$productId > 0) {
                    $collection->addFieldToFilter('i.product_id', $productId);
                }
            }
        } else {
            if (is_array($sku) && count($sku) > 0) {
                $collection->addFieldToFilter('e.sku', ['in' => $sku]);
            } else {
                if (!empty($sku)) {
                    $collection->addFieldToFilter('e.sku', $sku);
                }
            }
        }

        $collection->getSelect()->order([
            'e.entity_id',
            new \Zend_Db_Expr("IF(i.type = 'configurable', main_table.price, i.price) DESC")]
        );
        
        return $collection;
    }

    /**
     * Get product prices
     *
     * @param mixed array/string $sku [optional]
     * @param mixed array/int $productId [optional]     
     * @param mixed bool $searchBySku [optional]
     * @return mixed array
     */
    public function getProductPrices($sku = null, $productId = null, $searchBySku = true)
    {
        $isModuleOfferEnable = $this->moduleManager->isOutputEnabled('Formax_Offer');
        $collection = $this->getAssignProductPrices($sku, $productId, $searchBySku);
        $result = [];

        if (count($collection) > 0) {
            foreach ($collection as $item) {
                if ($isModuleOfferEnable) {
                    if ($this->_emergencyHelper->isEmergencySeller($item->getSellerId())) {
                        $price = (int)$item->getSpecialPrice() > 0 ? ((float)$item->getBasePrice() + (float)$item->getShippingPrice()) : (float)$item->getPrice();
                        $specialPrice = (int)$item->getSpecialPrice() > 0 ? ((float)$item->getSpecialPrice() + (float)$item->getShippingPrice()) : null;
                    }else if ($this->furnitureHelper->isFurnitureSeller($item->getSellerId())) {
                        $price = (int)$item->getSpecialPrice() > 0 ? ((float)$item->getBasePrice() + (float)$item->getAssemblyPrice()) : (float)$item->getPrice();
                        $specialPrice = (int)$item->getSpecialPrice() > 0 ? ((float)$item->getSpecialPrice() + (float)$item->getAssemblyPrice()) : null;
                    } else {
                        $price = (int)$item->getSpecialPrice() > 0 ? (float)$item->getBasePrice() : (float)$item->getPrice();
                        $specialPrice = (int)$item->getSpecialPrice() > 0 ? (float)$item->getSpecialPrice() : null;
                    }
                    $specialFromDate = $item->getStartDate() ? $item->getStartDate() : null;
                    $specialToDate = $item->getEndDate() ? $item->getEndDate() : null;
                } else {
                    $price = (float)$item->getPrice();
                    $specialPrice = null;
                    $specialFromDate = null;
                    $specialToDate = null;
                }

                $result[$item->getEntityId()] = [
                    'row_id' => (int)$item->getProductRowId(),
                    'price'  => $price,
                    'special_price' => $specialPrice,
                    'special_from_date' => $specialFromDate,
                    'special_to_date' => $specialToDate
                ];
            }
        }
        
        return $result;
    }

    /**
     * Sync proccess price between Webkul & Magento
     *
     * @param mixed array/string $sku [optional]
     * @param mixed array/int $productId [optional]
     * @param mixed bool $searchBySku [optional]
     * @return mixed array
     */
    public function syncPrice($sku = null, $productId = null, $searchBySku = true)
    {
        $result = ['error' => false, 'msg' => ''];
        $startTime = microtime(true);
        $connection = $this->resource->getConnection();
        $connection->beginTransaction();
        $i = 0;

        try {
            $collection = $this->getProductPrices($sku , $productId, $searchBySku);
            if (count($collection) > 0) {
                $tableCatalogProductDecimal = $this->resource->getTableName('catalog_product_entity_decimal');
                $tableCatalogProductDateTime = $this->resource->getTableName('catalog_product_entity_datetime');
                $attrPrice = $this->eavAttribute->getIdByCode('catalog_product', 'price');
                $attrSpecialPrice =$this->eavAttribute->getIdByCode('catalog_product', 'special_price');
                $attrSpecialFromDate =$this->eavAttribute->getIdByCode('catalog_product', 'special_from_date');
                $attrSpecialToDate =$this->eavAttribute->getIdByCode('catalog_product', 'special_to_date');

                foreach ($collection as $item) {
                    if (isset($item['row_id'])) {
                        if (isset($item['price'])) {
                            $sql = 'UPDATE ' . $tableCatalogProductDecimal . ' SET value = ' . $item['price'] .
                            ' WHERE row_id = ' . $item['row_id'] . ' AND attribute_id = ' . $attrPrice . ' AND store_id = 0';
                            $connection->query($sql);
                        }
                        if (isset($item['special_price'])) {
                            $value = $item['special_price'] !== null ? $item['special_price'] : 'NULL';
                            $sql = 'INSERT INTO ' . $tableCatalogProductDecimal .
                            ' (attribute_id, store_id, value, row_id) VALUES (' . $attrSpecialPrice . ', 0, ' . $value . ', ' . $item['row_id'] .
                            ') ON DUPLICATE KEY UPDATE value = ' . $value;
                            $connection->query($sql);
                        }
                        if (isset($item['special_from_date'])) {
                            $value = $item['special_from_date'] !== null ? $connection->quote($item['special_from_date']) : 'NULL';
                            $sql = 'INSERT INTO ' . $tableCatalogProductDateTime .
                            ' (attribute_id, store_id, value, row_id) VALUES (' . $attrSpecialFromDate . ', 0, ' . $value . ', ' . $item['row_id'] .
                            ') ON DUPLICATE KEY UPDATE value = ' . $value;
                            $connection->query($sql);
                        }
                        if (isset($item['special_to_date'])) {
                            $value = $item['special_to_date'] !== null ? $connection->quote($item['special_to_date']) : 'NULL';
                            $sql = 'INSERT INTO ' . $tableCatalogProductDateTime .
                            ' (attribute_id, store_id, value, row_id) VALUES (' . $attrSpecialToDate . ', 0, ' . $value . ', ' . $item['row_id'] .
                            ') ON DUPLICATE KEY UPDATE value = ' . $value;
                            $connection->query($sql);
                        }
                    }

                    $i++;
                }
            }

            $connection->commit();
            $resultTime = microtime(true) - $startTime;
            $result = ['error' => false, 'msg' => 'Process was executed successfully at: ' . gmdate('H:i:s', $resultTime) . ' Total records: ' . $i];
        } catch (\Exception $e) {
            $connection->rollBack();
            $this->logger->critical($e->getMessage());
            $result = ['error' => true, 'msg' => $e->getMessage()];
        }

        return $result;
    }
}
