<?php

namespace Formax\AgreementInfo\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Store extends AbstractHelper{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * constructor
     * 
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ){
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Return the current website name.
     * 
     * @return string
     */
    public function getWebsiteName(){
        return $this->_storeManager->getWebsite()->getName();
    }

    /**
     * Return the current website code.
     * 
     * @return string
     */
    public function getWebsiteCode(){
        return $this->_storeManager->getWebsite()->getCode();
    }

    /**
     * Return the value of the system configuration for catalog/search/min_query_length based on the store scope
     *
     * @return mixed
     */
    public function getSearchLength()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return $this->_scopeConfig->getValue('catalog/search/min_suggestion_length', $storeScope);
    }
}
