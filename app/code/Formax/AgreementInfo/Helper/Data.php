<?php

namespace Formax\AgreementInfo\Helper;

use Magento\Backend\Model\Auth\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;

class Data extends AbstractHelper
{
    /**
     *
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     *
     * @var \Formax\AgreementInfo\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;
    
    /** @var Session */
    private $_authSession;
    private Context $context;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Formax\AgreementInfo\Logger\Logger $logger
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        Context $context,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Formax\AgreementInfo\Logger\Logger $logger,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        Session $authSession
    ) {
        $this->httpClientFactory = $httpClientFactory;
        $this->logger = $logger;
        $this->context = $context;
        $this->jsonHelper = $jsonHelper;
        $this->_authSession = $authSession;

        parent::__construct($context);
    }

    /**
     * Get agreement data from REST service
     * 
     * @param string $token
     * @param string $agreementId
     * @return Zend_Http_Response
     */
    public function getAgreementInfo($token, $agreementId)
    {
        $response = null;

        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_agreement_data/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');

            if (!empty($endpoint) && !empty($token) && !empty($agreementId)) {
                $httpHeaders = [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . trim($token)
                ];
                
                $endpoint .= '/conveniomarco/' . $agreementId;
                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                    'timeout' => 30]
                );
                $client->setHeaders($httpHeaders);
                $response = $client->request(\Zend_Http_Client::GET)->getBody();

                $this->logger->info('Agreement ID: ' . $agreementId . ' Service Response: ' . $response);
                $response = $this->jsonHelper->jsonDecode($response);
            } else {
                throw new LocalizedException(__('Define endpoint and token agreement webservice'));
            }
        } catch(LocalizedException $e) {
            $this->logger->warning('Agreement ID: ' . $agreementId . ' Error message: ' . $e->getMessage());
        } catch(\Exception $e) {
            $this->logger->critical('Agreement ID: ' . $agreementId . ' Error message: ' . $e->getMessage());
        }

        return $response;
    }

    /**
     * Get Agreement ID
     *
     * @return mixed
     */
    public function getAgreementId()
    {
        $authSession = $this->_authSession->getUser();
        return $authSession->getUserRestIdActiveAgreement();
    }
}
