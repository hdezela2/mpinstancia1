<?php

namespace Formax\AgreementInfo\Block\Account;

class Dashboard extends \Webkul\Marketplace\Block\Account\Dashboard
{
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var \Formax\AgreementInfo\Model\ServiceData
     */
    protected $serviceData;

    /**
     * @var \Webkul\SellerSubAccount\Helper\Data
     */
    protected $helperWk;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Sales\Model\Order\ItemRepository $orderItemRepository
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Catalog\Model\CategoryRepository $categoryRepository
     * @param \Webkul\Marketplace\Helper\Orders $orderHelper
     * @param \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory $mpSaleslistCollectionFactory
     * @param \Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory $mpOrderCollectionFactory
     * @param \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory $mpProductCollectionFactory
     * @param \Webkul\Marketplace\Model\ResourceModel\Saleperpartner\CollectionFactory $mpSalePerPartnerCollectionFactory
     * @param \Webkul\Marketplace\Model\ResourceModel\Feedback\CollectionFactory $mpFeedbackCollectionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Formax\AgreementInfo\Model\ServiceData $serviceData
     * @param \Webkul\SellerSubAccount\Helper\Data $helperWk
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Sales\Model\Order\ItemRepository $orderItemRepository,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Webkul\Marketplace\Helper\Orders $orderHelper,
        \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory $mpSaleslistCollectionFactory,
        \Webkul\Marketplace\Model\ResourceModel\Orders\CollectionFactory $mpOrderCollectionFactory,
        \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory $mpProductCollectionFactory,
        \Webkul\Marketplace\Model\ResourceModel\Saleperpartner\CollectionFactory $mpSalePerPartnerCollectionFactory,
        \Webkul\Marketplace\Model\ResourceModel\Feedback\CollectionFactory $mpFeedbackCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Formax\AgreementInfo\Model\ServiceData $serviceData,
        \Webkul\SellerSubAccount\Helper\Data $helperWk,
        array $data = []
    ) {
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->serviceData = $serviceData;
        $this->helperWk = $helperWk;

        parent::__construct(
            $context,
            $customer,
            $customerSession,
            $orderRepository,
            $orderItemRepository,
            $productRepository,
            $categoryRepository,
            $orderHelper,
            $mpSaleslistCollectionFactory,
            $mpOrderCollectionFactory,
            $mpProductCollectionFactory,
            $mpSalePerPartnerCollectionFactory,
            $mpFeedbackCollectionFactory,
            $orderCollectionFactory,
            $data
        );
    }

    /**
     * Retrieve seller ID
     * 
     * @return int
     */
    public function getSellerIdBySubAccount()
    {
        return (int)$this->helperWk->getSubAccountSellerId();
    }

    /**
     * Retrieve Agreement data
     * 
     * @return object
     */
    public function getAgreementData()
    {
        $result = null;
        $sellerId = $this->getSellerIdBySubAccount();
        $seller = $this->customerRepositoryInterface->getById($sellerId);
        
        if ($seller && $this->customerSession->isLoggedIn()) {
            $agreementId = $seller->getCustomAttribute('user_rest_id_active_agreement') ?
                $seller->getCustomAttribute('user_rest_id_active_agreement')->getValue() : '';
            $customerId = $this->customerSession->getCustomer()->getId();
            $customer = $this->customerRepositoryInterface->getById($customerId);

            if ($customer) {
                $token = $customer->getCustomAttribute('user_rest_atk') ?
                    $customer->getCustomAttribute('user_rest_atk')->getValue() : '';
            }
            if ($agreementId !== '' && $token !== '') {
                $result = $this->serviceData->getData(trim($token), trim($agreementId));
            }
        }
        
        return $result;
    }
}
