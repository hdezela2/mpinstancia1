<?php

namespace Formax\AgreementInfo\Block\Adminhtml\Backend;

/**
 * @api
 * @since 100.0.2
 */
class Dashboard extends \Magento\Backend\Block\Dashboard
{
    /**
     * @var string
     */
    protected $_template = 'Formax_AgreementInfo::dashboard/index.phtml';

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var \Formax\AgreementInfo\Model\ServiceData
     */
    protected $serviceData;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Formax\AgreementInfo\Model\ServiceData $serviceData
     * @param array $data = []
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Formax\AgreementInfo\Model\ServiceData $serviceData,
        array $data = []
    ) {
        $this->authSession = $authSession;
        $this->serviceData = $serviceData;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getAgreementData()
    {
        $result = null;
        $authSession = $this->authSession->getUser();
        
        if ($authSession) {
            $agreementId = $authSession->getUserRestIdActiveAgreement();
            $token = $authSession->getUserRestAtk();
            if ($agreementId !== '' && $token !== '') {
                $result = $this->serviceData->getData(trim($token), trim($agreementId));
            }
        }
        
        return $result;
    }
}
