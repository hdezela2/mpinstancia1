<?php

namespace Formax\AgreementInfo\Model;

use Magento\Framework\Model\AbstractModel;

class Agreement extends AbstractModel
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\AgreementInfo\Model\ResourceModel\Agreement::class);
    }
}