<?php

namespace Formax\AgreementInfo\Model;

class ServiceData
{
    /**
     * @var \Formax\AgreementInfo\Helper\Data
     */
    protected $helper;

    /**
     * @var \Formax\AgreementInfo\Model\AgreementFactory
     */
    protected $agreementFactory;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @param \Formax\AgreementInfo\Model\AgreementFactory $agreementFactory
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     * @param \Formax\AgreementInfo\Helper\Data $helper
     */
    public function __construct(
        \Formax\AgreementInfo\Model\AgreementFactory $agreementFactory,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Formax\AgreementInfo\Helper\Data $helper
    ) {
        $this->helper = $helper;
        $this->agreementFactory = $agreementFactory;
        $this->dataObjectFactory = $dataObjectFactory;
    }

    /**
     * @param string $token
     * @param string $agreementId
     * @return mixed \Magento\Framework\DataObject | \Formax\AgreementInfo\Model\AgreementFactory
     */
    public function getData($token, $agreementId)
    {
        $result = null;

        if ($agreementId !== '') {
            $agreement = $this->agreementFactory->create();
            $agreementData = $agreement->load($agreementId, 'agreement_id');
            
            if (is_array($agreementData->getData()) && count($agreementData->getData()) > 0) {
                $agreementDate = new \DateTime($agreementData->getAggrementValidity());
                $agreementData->setAggrementValidity($agreementDate->format('Y-m-d'));
                $result = $agreementData;
            } else {
                $agreementData = $this->helper->getAgreementInfo($token, $agreementId);
                if ($agreementData !== null) {
                    if (isset($agreementData['success']) && $agreementData['success'] === 'OK'
                        && isset($agreementData['payload'])) {
                        $dataObject = $this->dataObjectFactory->create();
                        $dataObject->setData('agreement_id', $agreementId);
                        $dataObject->setData('agreement_name', trim($agreementData['payload']['nombreconveniomarco']));
                        $dataObject->setData('agreement_validity', trim($agreementData['payload']['fechatermino']));
                        $dataObject->setData('aggrement_number_bidding', trim($agreementData['payload']['nrolicitacionpublica']));
                        $dataObject->setData('aggrement_bid_sheet', trim($agreementData['payload']['linkficha']));
                        $dataObject->setData('entity_state', 1);

                        $agreement = $this->agreementFactory->create();
                        $agreementDate = new \DateTime($dataObject->getAgreementValidity());
                        $agreement->setAgreementId($dataObject->getAgreementId());
                        $agreement->setAgreementName($dataObject->getAgreementName());
                        $agreement->setAggrementValidity($agreementDate->format('Y-m-d H:i:s'));
                        $agreement->setAggrementBidSheet($dataObject->getAggrementBidSheet());
                        $agreement->setAggrementNumberBidding($dataObject->getAggrementNumberBidding());
                        $agreement->setEntityState($dataObject->getEntityState());
                        $agreement->save();
                        
                        $dataObject->setAggrementValidity($agreementDate->format('Y-m-d'));
                        $result = $dataObject;
                    }
                }
            }
        }

        return $result;
    }
}