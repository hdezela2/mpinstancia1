<?php

namespace Formax\AgreementInfo\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Agreement extends AbstractDb
{
    public function _construct()
    {
        $this->_init('dccp_agreement', 'entity_id');
    }
}