<?php

namespace Formax\AgreementInfo\Model\ResourceModel\Agreement;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Formax\AgreementInfo\Model\Agreement',
            'Formax\AgreementInfo\Model\ResourceModel\Agreement'
        );
    }
}
