<?php
namespace Formax\AgreementInfo\Plugin\Block\Cart;

class Sidebar{

    /**
     * @var \Formax\AgreementInfo\Helper\Store
     */
    protected $helper;

    /**
     * constructor
     * 
     * @param \Formax\AgreementInfo\Helper\Store
     */
    public function __construct(
        \Formax\AgreementInfo\Helper\Store $helper
    ){
        $this->helper = $helper;
    }

    /**
     * Modify config array to send website name.
     */
    public function afterGetConfig($subject, $config){
        $opts = [
            'agreementName' => $this->helper->getWebsiteName()
        ];
        return array_merge($config, $opts);
    }

}