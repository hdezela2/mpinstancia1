<?php
namespace Formax\AgreementInfo\Plugin\Checkout\Controller\Cart;

class Index{

    /**
     * @var \Formax\AgreementInfo\Helper\Store
     */
    protected $helper;

    /**
     * constructor
     * 
     * @param \Formax\AgreementInfo\Helper\Store
     */
    public function __construct(
        \Formax\AgreementInfo\Helper\Store $helper
    ){
        $this->helper = $helper;
    }

    /**
     * Modify shopping cart title based on the website name.
     */
    public function afterExecute($subject, $resultPage){
        $websiteName = $this->helper->getWebsiteName();
        $resultPage->getConfig()->getTitle()->set(__('Shopping cart - %1', $websiteName));
        return $resultPage;
    }

}