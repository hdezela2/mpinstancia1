<?php

namespace Formax\Coupon\Helper;

use Magento\Framework\Serialize\SerializerInterface;
use Magento\Customer\Model\Session as customerSession;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\Context;
use Formax\GreatBuy\Controller\Validate\Index as Utm;
use Magento\Checkout\Model\Session as checkoutSession;
use Magento\Framework\App\ResourceConnection;
use Webkul\MpAssignProduct\Helper\Data as helperMpAssign;
use Magento\Framework\Pricing\Helper\Data as helperPrice;
use Magento\Checkout\Model\Cart;
use Magento\SalesRule\Api\Data\RuleInterface;
use Magento\SalesRule\Api\RuleRepositoryInterface;
use Magento\SalesRule\Model\CouponFactory;
use Magento\SalesRule\Api\CouponRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Formax\ConfigCmSoftware\Helper\Data as helperConfigSoftware;
use Magento\SalesRule\Model\Coupon as couponModel;
use Psr\Log\LoggerInterface;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    private $_actionPermitidosProvedores;
    private $_actionLogsFactory;
    private $_customerSession;
    private $_storeManager;
    private $_serializer;
    private $_utm;
    private $_checkoutSession;
    private $_resource;
    private $_helperMpAssign;
    private $_priceHelper;
    private $_cart;
    private $rule;
    private $RuleRepository;
    private $_coupon;
    private $couponRepository;
    private $_product;
    private $configSoftware;
    private $_couponModel;
    protected $logger;
    private SerializerInterface $_serialize;

    public function __construct(
        Context $context,
        customerSession $customerSession,
        StoreManagerInterface $storeManager,
        checkoutSession $checkoutSession,
        ResourceConnection $resource,
        SerializerInterface $serialize,
        helperMpAssign $helperMpAssign,
        Utm $utm,
        helperPrice $priceHelper,
        Cart $cart,
        RuleRepositoryInterface $RuleRepository,
        CouponFactory $coupon,
        CouponRepositoryInterface $couponRepository,
        RuleInterface $rule,
        ProductRepositoryInterface $product,
        helperConfigSoftware $helperConfigSoftware,
        couponModel $couponModel,
        LoggerInterface $logger
    ){
        $this->_checkoutSession = $checkoutSession;
        $this->_utm = $utm;
        $this->_resource = $resource;
        $this->_serialize = $serialize;
        $this->_helperMpAssign = $helperMpAssign;
        $this->_priceHelper = $priceHelper;
        $this->_cart = $cart;
        $this->_storeManager = $storeManager;
        $this->rule = $rule;
        $this->RuleRepository = $RuleRepository;
        $this->_coupon = $coupon;
        $this->couponRepository = $couponRepository;
        $this->_product = $product;
        $this->configSoftware = $helperConfigSoftware;
        $this->_couponModel = $couponModel;
        $this->logger = $logger;
        parent::__construct($context);
    }

    private function getStoreName()
    {
        return trim($this->_storeManager->getStore()->getName());
    }

    /**
     * Return the current website code.
     *
     * @return string
     */
    public function getWebsiteCode(){
        return $this->_storeManager->getWebsite()->getCode();
    }

    public function obtenerDescuento($utm, $subtotal, $seller, $quiteInfoId)
    {
        $utmCart = round($subtotal/$utm);
        $tramo = $this->obtenerTramo($utmCart);
        $desc = 0;
        $urlCategory = $this->getTypeProductCategory($quiteInfoId);

        switch ($urlCategory) {
            case "servicios-profesionales-ti":
            case "proyectos-de-servicios-profesionales-ti":
                switch ($tramo) {
                    case 1:
                        $desc = $seller->getData('wkv_sw_profesional_tramo1');
                        break;
                    case 2:
                        $desc = $seller->getData('wkv_sw_profesional_tramo2');
                        break;
                    case 3:
                        $desc = $seller->getData('wkv_sw_profesional_tramo3');
                        break;
                    default:
                        $desc = 0;
                        break;
                }
                break;
            case "desarrollo-y-mantencion-de-software":
            case "servicios-de-desarrollo-y-mantencion-de-software":
                switch ($tramo) {
                    case 1:
                        $desc = $seller->getData('wkv_sw_desarrollo_tramo1');
                        break;
                    case 2:
                        $desc = $seller->getData('wkv_sw_desarrollo_tramo2');
                        break;
                    case 3:
                        $desc = $seller->getData('wkv_sw_desarrollo_tramo3');
                        break;
                    default:
                        $desc = 0;
                    break;
                }
                break;

        }


        return $desc;
    }

    private function obtenerTramo($utmCart)
    {
        $tramo = 0;

        $rangos = $this->getWebsiteCode() == "software2022" ?
            //tramos para software renovacion
            [
                [
                    'tramo' => 1,
                    'min' => 30,
                    'max' => 100
                ],
                [
                    'tramo' => 2,
                    'min' => 101,
                    'max' => 300
                ],
                [
                    'tramo' => 3,
                    'min' => 301,
                    'max' => 600
                ]
            ]
            :
            //tramos para software original
            [
                [
                    'tramo' => 1,
                    'min' => 10,
                    'max' => 99
                ],
                [
                    'tramo' => 2,
                    'min' => 100,
                    'max' => 299
                ],
                [
                    'tramo' => 3,
                    'min' => 300,
                    'max' => 600
                ]
            ];

        foreach ($rangos as $item) {
            if ($utmCart >= $item['min'] && $utmCart <= $item['max']) {
                $tramo = $item['tramo'];
            }
        }

        return $tramo;
    }

    public function calcularDescuentoTramo()
    {
        $utm = round($this->_utm->getCLPValue());
        $quoteId = $this->_checkoutSession->getQuote()->getId();
        $sellerId = $this->getSellerIdByQuote($quoteId);
        $seller = $this->_helperMpAssign->getCustomer($sellerId); // se carga el proveedor
        $subTotal = (double)str_replace('.', '', str_replace('$', '', $this->_priceHelper->currency($this->_cart->getQuote()->getSubtotal(), true, false)));
        $quoteInfoArray = $this->getQuoteInfoByCartId($quoteId);
        $quoteInfoId = $quoteInfoArray['quoteInfoId'];
        $pdesc = $this->obtenerDescuento($utm, $subTotal, $seller, $quoteInfoId); // se obtiene el porcentaje de descuento
        return $pdesc;
    }

    public function getSellerIdByQuote($quoteId)
    {
        if (isset($quoteId) && is_numeric($quoteId) && $quoteId > 0) {
            $con = $this->_resource->getConnection();
            $sql = 'SELECT  qio.value as serialize FROM  quote q
                    INNER JOIN quote_item qi ON(qi.quote_id = q.entity_id)
                    INNER JOIN quote_item_option qio ON(qio.item_id = qi.item_id)
                    WHERE  q.entity_id = ' . $quoteId;
            $result = $con->fetchOne($sql);

            if (isset($result) && !empty($result)) {
                $data = $this->_serialize->unserialize($result);
                if (isset($data['seller_id'])) {
                    return $data['seller_id'];
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function getSubtotal()
    {
        $tstotal = $this->_cart->getQuote()->getSubtotal();
        $subTotal = (double)str_replace('.', '', str_replace('$', '', $this->_priceHelper->currency($tstotal, true, false)));
        return $subTotal;
    }

    public function getValorDescuentoHtml()
    {
        $subtotal = $this->getSubtotal();
        $desc = ($subtotal*$this->calcularDescuentoTramo())/100;
        return $this->_priceHelper->currency($desc, true, false);
    }

    public function getValorDescuento()
    {
        $subtotal = $this->getSubtotal();
        $desc = ($subtotal*$this->calcularDescuentoTramo())/100;
        $val = (double)str_replace('.', '', str_replace('$', '', $this->_priceHelper->currency($desc, true, false)));
        return $val;
    }

    public function createAndApplyCoupon($sellerId = 0,$quoteInfoId = 0)
    {
        if (!in_array($this->getStoreName(), ['convenio_storeview_software', 'Software', SoftwareRenewalConstants::STORE_CODE, SoftwareRenewalConstants::STORE_NAME])) {
            return false;
        }

        $webSiteId = [$this->getWebsiteId()];
        $descRule = [];
        $rule = $this->rule;
        $groupId = [1, 2, 3, 7, 10];
        $utm = round($this->_utm->getCLPValue());
        $quoteId = $this->_checkoutSession->getQuote()->getId();
        $sellerId = $this->getSellerIdByQuote($quoteId);
        $quoteInfoArray = $this->getQuoteInfoByCartId($quoteId);
        if (!empty($quoteInfoArray)) {
            $quoteInfoId = $quoteInfoArray['quoteInfoId'];
        }
        if ($sellerId > 0 && $quoteInfoId > 0) {
            $subTotal = $this->getSubtotal();
            $seller = $this->_helperMpAssign->getCustomer($sellerId); // se carga el proveedor
            $descRule['seller_id'] = $sellerId;
            $descRule['quote_id'] = $quoteId;
            $couponCode = $sellerId . '_' . $quoteInfoArray['product'] . '_' . $quoteId;
            $loadruleId = $this->_couponModel->loadByCode($couponCode)->getRuleId();
            if (isset($loadruleId) && is_numeric($loadruleId) && $loadruleId > 0) {
                $this->_checkoutSession
                    ->getQuote()
                    ->setCouponCode($couponCode)
                    ->save();
            } else {
                $descRule['cretaded_at'] = date("Y-m-d H:i:s");
                $pdesc = $this->obtenerDescuento($utm, $subTotal, $seller, $quoteInfoId); // se obtiene el porcentaje de descuento
                if ($pdesc > 0) {
                    $rule->setName('Descuento de Proveedor') // Rules name
                            ->setDescription($this->_serialize->serialize($descRule))
                            ->setIsAdvanced(true)
                            ->setStopRulesProcessing(false)
                            ->setDiscountQty(0)
                            ->setCustomerGroupIds($groupId)
                            ->setWebsiteIds($webSiteId)
                            ->setUseAutoGeneration(0) // If want to auto generate
                            ->setCouponType(RuleInterface::COUPON_TYPE_SPECIFIC_COUPON)
                            ->setSimpleAction(RuleInterface::DISCOUNT_ACTION_BY_PERCENT)
                            ->setUsesPerCoupon(0)
                            ->setUsesPerCustomer(0)
                            ->setDiscountAmount($pdesc)
                            ->setIsActive(true);
                    $resultRules = $this->RuleRepository->save($rule);
                    $coupon = $this->_coupon->create();

                    $coupon->setCode($couponCode)
                        ->setIsPrimary(true)
                        ->setRuleId( $resultRules->getRuleId());
                    $this->couponRepository->save($coupon);

                    $this->_checkoutSession
                        ->getQuote()
                        ->setCouponCode($couponCode)
                        ->save();
                }
            }
        }
    }

    private function getWebsiteId()
    {
        return $this->_storeManager->getStore()->getWebsiteId();
    }

    public function getTotal()
    {
        $d = 0;
        $subTotal = $this->getSubtotal();
        $tmpTotal = $this->_priceHelper->currency($this->_checkoutSession->getQuote()->getGrandTotal(), true, false);
        $total = (double)str_replace('.', '', str_replace('$', '', $tmpTotal));
        $pdesc = $this->calcularDescuentoTramo();

        if ($subTotal == $total && $pdesc != null && !empty($pdesc) && $pdesc > 0) {
            $total = $subTotal - $this->getValorDescuento();
            $total = $this->_priceHelper->currency($total, true, false);
            $total = $tmpTotal;
        } else {
            $total = $tmpTotal;
        }

        return $total;
    }

    public function getTypeProductCategory($quiteInfoId)
    {
        $con = $this->_resource->getConnection();
        $sql = 'SELECT p.sku  FROM requestforquote_quote_info qi
                INNER JOIN requestforquote_quote q ON(qi.quote_id = q.entity_id)
                INNER JOIN catalog_product_entity p ON(q.product_id = p.entity_id)
                WHERE qi.entity_id = '. $quiteInfoId;
        $skutmp = $con->fetchOne($sql);

        $tmp = explode('-', $skutmp);
        $sku = '';

        if (count($tmp) > 1) {
            $sku = (isset($tmp[1])) ? $tmp[1] : $tmp[0];
        } else {
            $sku = (isset($tmp[1])) ? $tmp[1] : $tmp[0];
        }

        $producto = $this->_product->get($sku);
        $urlCategory = $this->configSoftware->getCategoryUrl($producto);

        return $urlCategory;
    }

    public function getQuoteInfoByCartId($quoteId)
    {
        if (isset($quoteId) && is_numeric($quoteId) && $quoteId > 0) {
            $con = $this->_resource->getConnection();
            $sql = 'SELECT qio.value as serialize FROM  quote q
                        INNER JOIN quote_item qi ON(qi.quote_id = q.entity_id)
                        INNER JOIN quote_item_option qio ON(qio.item_id = qi.item_id)
                        WHERE q.entity_id = ' . $quoteId;
            $result = $con->fetchOne($sql);

            if (isset($result) && !empty($result)) {
                $data = $this->_serialize->unserialize($result);
                if (isset($data['quoteInfoId']) && isset($data['product'])) {
                    return $data;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
}
