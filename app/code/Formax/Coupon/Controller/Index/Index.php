<?php
declare(strict_types=1);

namespace Formax\Coupon\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Controller\Index\Index as CheckoutIndex;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Checkout\Helper\Data as HelperCheckout;
use Formax\Coupon\Helper\Data as HelperCoupon;

class Index extends CheckoutIndex
{
    /**
     * Checkout page
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $helperCoupon = $this->_objectManager->get(HelperCoupon::class);
        
        /** @var \Magento\Checkout\Helper\Data $checkoutHelper */
        $checkoutHelper = $this->_objectManager->get(HelperCheckout::class);
        if (!$checkoutHelper->canOnepageCheckout()) {
            $this->messageManager->addErrorMessage(__('One-page checkout is turned off.'));
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

        $quote = $this->getOnepage()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError() || !$quote->validateMinimumAmount()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

        if (!$this->_customerSession->isLoggedIn() && !$checkoutHelper->isAllowedGuestCheckout($quote)) {
            $this->messageManager->addErrorMessage(__('Guest checkout is disabled.'));
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

        if (!$this->isSecureRequest()) {
            $this->_customerSession->regenerateId();
        }

        $this->_objectManager->get(CheckoutSession::class)->setCartWasUpdated(false);
        $this->getOnepage()->initCheckout();
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Checkout'));
        return $resultPage;
    }

    /**
     * Checks if current request uses SSL and referer also is secure.
     *
     * @return bool
     */
    private function isSecureRequest(): bool
    {
        $storeManager = $this->_objectManager->get(StoreManagerInterface::class);
        $isFrontUrlSecure = $storeManager->getStore()->isFrontUrlSecure();
        $isCurrentlySecure = $storeManager->getStore()->isCurrentlySecure();
        
        return $isFrontUrlSecure && $isCurrentlySecure;
    }
}
