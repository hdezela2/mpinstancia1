<?php
namespace Formax\Coupon\Controller\Cartover;

use Magento\Framework\App\Action\Action;
use Magento\Customer\Model\Session;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\SalesRule\Api\Data\RuleInterface;
use Magento\SalesRule\Api\RuleRepositoryInterface;
use Magento\SalesRule\Model\CouponFactory;
use Magento\SalesRule\Model\RuleFactory;
use Magento\SalesRule\Api\CouponRepositoryInterface;
use Magento\Framework\Serialize\SerializerInterface;

class Proceedtocheckout extends \Webkul\Mpsplitcart\Controller\Cartover\Proceedtocheckout{
    protected $_session;
    protected $_formKeyValidator;
    private $helper;
    protected $_error = false;
    private $_logger;
    private $_coupon;
    private $couponRepository;
    private $_checkoutSession;
    private $_cart;
    private $_utm;
    private $_priceHelper;
    private $_helperSeller;
    private $_rsConnection;
    private $_serializer;
    private $_storeManager;

    /**
     * @var RuleRepositoryInterface
     */
    private $RuleRepository;

    /**
     * @var RuleFactory
     */
    private $ruleFactory;

    /**
     * @var RuleInterface
     */
    private $rule;

    public function __construct(
        Context $context,
        Session $customerSession,
        FormKeyValidator $formKeyValidator,
        \Webkul\Mpsplitcart\Helper\Data $helper,
        \Formax\OrderManagement\Logger\Logger $logger,
        RuleRepositoryInterface $RuleRepository,
        RuleFactory $ruleFactory,
        RuleInterface $rule,
        CouponFactory $coupon,
        CouponRepositoryInterface $couponRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Model\Cart $cart,
        \Formax\GreatBuy\Controller\Validate\Index $utm,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Webkul\MpAssignProduct\Helper\Data $helperSeller,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        SerializerInterface $serializer,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_session = $customerSession;
        $this->_formKeyValidator = $formKeyValidator;
        parent::__construct($context,$customerSession,$formKeyValidator,$helper);
        $this->helper           = $helper;
        $this->_logger          = $logger;
        $this->RuleRepository   = $RuleRepository;
        $this->ruleFactory      = $ruleFactory;
        $this->rule             = $rule;
        $this->_coupon          = $coupon;
        $this->couponRepository = $couponRepository;
        $this->_checkoutSession = $checkoutSession;
        $this->_cart            = $cart;
        $this->_utm             = $utm;
        $this->_priceHelper     = $priceHelper;
        $this->_helperSeller    = $helperSeller;
        $this->_rsConnection    = $resourceConnection;
        $this->_serializer      = $serializer;
        $this->_storeManager    = $storeManager;
    }

    public function execute(){
        try{
            switch($this->getStoreName()){
                case 'convenio_storeview_software':
                case SoftwareRenewalConstants::STORE_CODE:
                    //$this->createAndApplyCoupon();
                break;
            }
        }catch(\Exception $err){
            $this->_logger->info("error: ".$err->getMessage());
        }
        if ($this->getRequest()->isPost()) {
            try {
                if (!$this->_formKeyValidator->validate($this->getRequest())) {
                    $this->messageManager->addError(__("Something Went Wrong !!!"));
                    return $this->resultRedirectFactory->create()->setPath(
                        'checkout/cart',
                        [
                            '_secure' => $this->getRequest()->isSecure()
                        ]
                    );
                }
                $fields = $this->getRequest()->getParams();
                $sellerId = $this->helper->getMpCustomerId();

                if (isset($fields['mpslitcart-checkout'])
                    && $fields['mpslitcart-checkout']!==""
                ) {
                    $this->helper->createCustomQuote();
                    $this->helper->getUpdatedQuote(
                        $fields['mpslitcart-checkout']
                    );
                    return $this->resultRedirectFactory->create()->setPath(
                        'checkout',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                } else {
                    if (isset($fields['mpslitcart-disbale'])
                        && $fields['mpslitcart-disbale']!==""
                    ) {
                        return $this->resultRedirectFactory->create()->setPath(
                            'checkout',
                            ['_secure' => $this->getRequest()->isSecure()]
                        );
                    } else {
                        //foreach ($errors as $message) {
                            //$this->messageManager->addError($message);
                        //}

                        return $this->resultRedirectFactory->create()->setPath(
                            'checkout/cart',
                            [
                                '_secure' => $this->getRequest()->isSecure()
                            ]
                        );
                    }
                }
            } catch (\Exception $e) {
                $this->helper->logDataInLogger("Controller_Proceedtocheckout execute : ".$e->getMessage());
                $this->messageManager->addError($e->getMessage());
                return $this->resultRedirectFactory->create()->setPath(
                    'checkout/cart',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }
        } else {
            $this->messageManager->addError(__("Something Went Wrong !!!"));
            return $this->resultRedirectFactory->create()->setPath(
                'checkout/cart',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
    private function createAndApplyCoupon(){
        try {
            $webSiteId  = array( $this->getWebsiteId() );
            $descRule   = array();
            $rule       = $this->rule;
            $groupId    = array(1,2,3,7,10);
            $couponCode = date("YmdHis");
            $utm        = round($this->_utm->getCLPValue());
            $quoteId    = $this->_checkoutSession->getQuote()->getId();
            $sellerId   = $this->getSellerId($quoteId);

            $subTotal                   = (double)str_replace('.','',str_replace('$','',$this->_priceHelper->currency($this->_cart->getQuote()->getSubtotal(),true,false)));
            $seller                     = $this->_helperSeller->getCustomer($sellerId);// se carga el proveedor
            $descRule['seller_id']      = $sellerId;
            $descRule['quote_id']       = $quoteId;
            $descRule['cretaded_at']    = date("Y-m-d H:i:s");
            $pdesc                      = $this->obtenerDescuento($utm,$subTotal,$seller);// se obtiene el porcentaje de descuento
            if($pdesc>0){
                $rule->setName('Descuento de Proveedor') // Rules name
                        ->setDescription($this->_serializer->serialize($descRule))
                        ->setIsAdvanced(true)
                        ->setStopRulesProcessing(false)
                        ->setDiscountQty(0)
                        ->setCustomerGroupIds($groupId)
                        ->setWebsiteIds($webSiteId)
                        ->setUseAutoGeneration(0)    // If want to auto generate
                        ->setCouponType(RuleInterface::COUPON_TYPE_SPECIFIC_COUPON)
                        ->setSimpleAction(RuleInterface::DISCOUNT_ACTION_BY_PERCENT)
                        ->setUsesPerCoupon(0)
                        ->setUsesPerCustomer(0)
                        ->setDiscountAmount($pdesc)
                        ->setIsActive(true);
                $resultRules    = $this->RuleRepository->save($rule);
                $coupon         = $this->_coupon->create();
                $coupon->setCode($couponCode)
                        ->setIsPrimary(true)
                        ->setRuleId( $resultRules->getRuleId() );
                $this->couponRepository->save($coupon);

                $this->_checkoutSession->getQuote()->setCouponCode($couponCode)
                                    ->collectTotals()
                                    ->save();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
    public function getSellerId($quoteId){
        if(isset($quoteId) && is_numeric($quoteId) && $quoteId>0){
            $con = $this->_rsConnection->getConnection();
            $sql = 'SELECT
                            qio.value as serialize
                        FROM
                            quote q
                        INNER JOIN quote_item qi ON(qi.quote_id=q.entity_id)
                        INNER JOIN quote_item_option qio ON(qio.item_id=qi.item_id)
                        WHERE
                            q.entity_id='.$quoteId;
            $result = $con->fetchOne($sql);
            $data = $this->_serializer->unserialize($result);
            if(isset($data['seller_id'])){
                return $data['seller_id'];
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    private function getStoreName(){
        return trim($this->_storeManager->getStore()->getName());
    }
    private function obtenerDescuento($utm,$subtotal,$seller){
        $utmCart = round($subtotal/$utm);
        $tramo = $this->obtenerTramo($utmCart);
        $desc = 0;
        switch($tramo){
            case 1:
                $desc = $seller->getData('wkv_sw_desarrollo_tramo1');
            break;
            case 2:
                $desc = $seller->getData('wkv_sw_desarrollo_tramo2');
            break;
            case 3:
                $desc = $seller->getData('wkv_sw_desarrollo_tramo3');
            break;
            default:
                $desc=0;
            break;
        }
        return $desc;
    }
    private function obtenerTramo($utmCart){
        $tramo = 0;
        $rangos = array(
            array(
                'tramo'=>1,
                'min'=>10,
                'max'=>99
            ),
            array(
                'tramo'=>2,
                'min'=>100,
                'max'=>299
            ),
            array(
                'tramo'=>3,
                'min'=>300,
                'max'=>600
            )
        );
        $this->_logger->info("subTotalQuoteToUtm: ".$utmCart);
        foreach($rangos as $item){
            if($utmCart>=$item['min'] && $utmCart<=$item['max']){
                $tramo=$item['tramo'];
            }
        }
        return $tramo;
    }
    private function getWebsiteId(){
        return $this->_storeManager->getStore()->getWebsiteId();
    }
}
