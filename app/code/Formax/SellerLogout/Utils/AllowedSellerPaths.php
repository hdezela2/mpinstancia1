<?php

namespace Formax\SellerLogout\Utils;

class AllowedSellerPaths{

    public const ACTIONS = [
        'marketplace',
        'mpassignproduct',
        'penalty',
        'regionalcondition',
        'requestforquote_seller',
        'offerassignproduct'
    ];

}
