<?php

namespace Formax\SellerLogout\Plugin\Adminhtml\Auth;

use \Magento\Backend\Controller\Adminhtml\Auth\Logout as Mg_Logout;
use Formax\SellerLogout\Helper\Data;

class Logout{

    /**
     * @var \Magento\Backend\Model\View\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Formax\SellerLogout\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $_helper;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Data $data
    ) {
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->_helper = $context->getHelper();
        $this->helper = $data;
    }

    /**
     * Administrator logout action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function afterExecute(Mg_Logout $subject, $result){

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $loginUrl = $this->helper->getLogoutUrl();

        if(!empty($loginUrl)){
            $resultRedirect->setUrl($loginUrl);
        }else{
            $resultRedirect->setPath($this->_helper->getHomePageUrl());
        }
        return $resultRedirect;
    }
}
