<?php

namespace Formax\SellerLogout\Plugin\Adminhtml\Auth;

use \Magento\Backend\Controller\Adminhtml\Auth\Login as Mg_Login;
use Formax\SellerLogout\Helper\Data;
use Formax\SellerLogout\Helper\Cookie;

class Login{

    /**
     * @var Cookie
     */
    protected $cookieManager;

    /**
     * @var \Magento\Backend\Model\View\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Backend\Model\Auth
     */
    protected $_auth;

    /**
     * @var \Formax\SellerLogout\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Cookie $cookie,
        Data $data
    ) {
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->helper = $data;
        $this->_url = $context->getUrl();
        $this->_auth = $context->getAuth();
        $this->cookieManager = $cookie;
    }

    /**
     * Administrator login action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function aroundExecute(Mg_Login $subject, $proceed){

        if (!$this->_auth->isLoggedIn()) {
            $logoutUrl = $this->helper->getLogoutUrl();

            if(!empty($logoutUrl)) {

                $keyName = $this->helper->getMageLoginParamKey();
                $cookie = (boolean)$this->cookieManager->get();
                $isMageAdmin = strpos($this->_url->getCurrentUrl(), $keyName) !== false;

                if(!$isMageAdmin && !$cookie) {
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setUrl($logoutUrl);
                    return $resultRedirect;
                }else{
                    $this->cookieManager->set(true, 120);
                }
            }
        }

        return $proceed();

    }

}
