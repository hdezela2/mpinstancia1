<?php

namespace Formax\SellerLogout\Plugin\Customer\Account;

use Magento\Customer\Controller\Account\Login as Mg_Login;
use Formax\SellerLogout\Helper\Data;

class Login{

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Formax\SellerLogout\Helper\Data
     */
    protected $helper;

    /**
     * Login plugin constructor
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Formax\SellerLogout\Helper\Data $data
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Data $data
    ) {
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->helper = $data;
    }

    /**
     * Execute plugin
     */
    public function aroundExecute(Mg_Login $subject, $proceed){
        $resultRedirect = $this->resultRedirectFactory->create();
        $returnValue = $proceed();
        $logoutUrl = $this->helper->getLogoutUrl();

        $isLoggedIn = $this->helper->isLoggedIn();

        if($isLoggedIn){
          if($this->helper->isSeller()){
            $resultRedirect->setPath($this->helper->getMarketplaceIndex());
          }else{
            $resultRedirect->setPath($this->helper->getCustomerAccountIndex());
          }
        }else{
          $resultRedirect->setUrl($logoutUrl);
        }

        if($isLoggedIn || !empty($logoutUrl)){
          return $resultRedirect;
        }

        return $returnValue;
    }

}