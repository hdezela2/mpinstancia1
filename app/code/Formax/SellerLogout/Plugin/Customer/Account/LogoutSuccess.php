<?php

namespace Formax\SellerLogout\Plugin\Customer\Account;

use Magento\Customer\Controller\Account\LogoutSuccess as Mg_LogoutSuccess;
use Formax\SellerLogout\Helper\Data;

class LogoutSuccess{

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Formax\SellerLogout\Helper\Data
     */
    protected $helper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Data $data
    ) {
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->helper = $data;
    }

    public function aroundExecute(Mg_LogoutSuccess $subject, $proceed){

        $resultRedirect = $this->resultRedirectFactory->create();
        $returnValue = $proceed();
        $loginUrl = $this->helper->getLogoutUrl();

        $resultRedirect->setUrl($loginUrl);

        if(!empty($loginUrl)){
          return $resultRedirect;
        }

        return $returnValue;
    }

}