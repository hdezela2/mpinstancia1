<?php

namespace Formax\SellerLogout\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Webkul\Marketplace\Helper\Data as MpData;

class Data extends \Magento\Framework\App\Helper\AbstractHelper{

    const MARKETPLACE_INDEX_PATH = 'marketplace/account/dashboard';

    const CUSTOMER_INDEX_PATH = 'customer/account';

    const MARKETPLACE_LOGOUT_PATH = 'marketplace/profile_settings/url_logout_seller';

    const PARAM_FOR_MG_ADMIN_LOGIN = 'isMageAdmin';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeInterface;

    /**
     * @var MpData
     */
    protected $mpHelper;

    /**
     * Helper constructor
     * 
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Webkul\Marketplace\Helper\Data $data
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        ScopeConfigInterface $scopeConfig,
        MpData $data
    ) {
        $this->scopeInterface = $scopeConfig;
        $this->mpHelper = $data;
        parent::__construct($context);
    }

    /**
     * Get if customer is logged in
     * 
     * @return boolean
     */
    public function isLoggedIn(){
      return $this->mpHelper->isCustomerLoggedIn();
    }

    /**
     * Get marketplace logout URL
     * 
     * @return string
     */
    public function getLogoutUrl(){
      return $this->scopeInterface->getValue(
        self::MARKETPLACE_LOGOUT_PATH,
        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
      );
    }

    /**
     * Get param name for validate if is admin login or provider login (DCCP)
     *
     * @return string
     */
    public function getMageLoginParamKey(){
      return self::PARAM_FOR_MG_ADMIN_LOGIN;
    }

    /**
     * Get marketplace base path
     * 
     * @return string
     */
    public function getMarketplaceIndex(){
      return self::MARKETPLACE_INDEX_PATH;
    }

    /**
     * Get customer account base path
     * 
     * @return string
     */
    public function getCustomerAccountIndex(){
      return self::CUSTOMER_INDEX_PATH;
    }

    /**
     * Get if customer is seller
     * 
     * @return int
     */
    public function isSeller(){
      return $this->mpHelper->isSeller();
    }
}
