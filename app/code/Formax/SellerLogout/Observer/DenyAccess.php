<?php

namespace Formax\SellerLogout\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;
use \Formax\SellerLogout\Utils\AllowedSellerPaths;
use \Magento\Framework\App\ResponseFactory;
use \Magento\Framework\UrlInterface;

class DenyAccess implements ObserverInterface{

    /**
     * @var \Formax\SellerLogout\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @var array
     */
    protected $allowedActions = AllowedSellerPaths::ACTIONS;

    /**
     * DenyAccess constructor
     * 
     * @param \Formax\SellerLogout\Helper\Data $data
     * @param \Magento\Framework\UrlInterface $url
     */
    public function __construct(
        \Formax\SellerLogout\Helper\Data $data,
        UrlInterface $url
    ){
        $this->helper = $data;
        $this->url = $url;
    }

    /**
     * Controller action predispatch observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer){
        $actionName = $observer->getRequest()->getFullActionName();
        $responseRedirect = $observer->getControllerAction()->getResponse();

        /*if($this->helper->isSeller() && !$this->isAllowed($actionName)){
            $redirectionUrl = $this->url->getUrl($this->helper->getMarketplaceIndex());
            $responseRedirect->setRedirect($redirectionUrl);
        }*/

    }

    /**
     * Check if a seller is allowed to enter into the action name sent
     * 
     * @return boolean
     */
    private function isAllowed($currentAction){
        $found = false;
        foreach($this->allowedActions as $action){
            if(strpos($currentAction, $action) !== false){
                $found = true;
                break;
            }
        }
        return $found;
    }

}