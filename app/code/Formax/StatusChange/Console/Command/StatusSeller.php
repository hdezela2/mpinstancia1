<?php

namespace Formax\StatusChange\Console\Command;

use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Formax\StatusChange\Helper\Data as Helper;

class StatusSeller extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @param \Magento\Framework\App\State $appState
     * @param Helper $helper
     * @param string $name
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        Helper $helper,
        $name = null
    ) {
        $this->helper = $helper;
        $this->appState = $appState;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('seller:changestatus');
        $this->setDescription("Change status of seller");

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $result = $this->helper->changeStatusSeller();
        $output->writeln($result['msg']);

        return Cli::RETURN_SUCCESS;
    }
}
