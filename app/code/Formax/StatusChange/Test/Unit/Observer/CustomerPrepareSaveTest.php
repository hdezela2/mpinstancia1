<?php

namespace Formax\StatusChange\Test\Unit\Observer;

use Formax\StatusChange\Helper\Data as Helper;
use Formax\StatusChange\Logger\Logger;
use Magento\Backend\Model\Auth\Session as AuthSession;
use PHPUnit\Framework\TestCase;
use Formax\StatusChange\Observer\CustomerPrepareSave;
use Magento\Framework\Api\AttributeValue;
use Magento\User\Model\User;

class CustomerPrepareSaveTest extends TestCase
{
    /**
     * @var Formax\StatusChange\Observer\CustomerPrepare|\PHPUnit_Framework_MockObject_MockObject
     */
    private $observer;

    /**
     * @var \Magento\Framework\Event\Observer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $eventObserver;

    /**
     * @var \Formax\StatusChange\Helper\Data|\PHPUnit_Framework_MockObject_MockObject
     */
    private $helperMock;

    /**
     * @var \Formax\StatusChange\Logger\Logger|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var \Magento\Backend\Model\Auth\Session|\PHPUnit_Framework_MockObject_MockObject
     */
    private $authSessionMock;

    /**
     * @var Magento\Framework\Api\AttributeValue|\PHPUnit_Framework_MockObject_MockObject
     */
    private $attributeValue;
    /**
     * @var \Magento\Framework\App\RequestInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private $request;
    /**
     * @var \Magento\Customer\Api\Data\CustomerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private $customer;

    /**
     * Test setup
     */
    protected function setUp()
    {
        $this->helperMock = $this->getMockBuilder(Helper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->loggerMock = $this->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->authSessionMock = $this->getMockBuilder(AuthSession::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->observer = new CustomerPrepareSave(
            $this->helperMock,
            $this->loggerMock,
            $this->authSessionMock
        );
    }

    /**
     * @return void
     */
    public function testSellerStatusChange()
    {
        $postData = [
            'customer' => ['wkv_dccp_state_details' => 73]
        ];
        $attributeValue = '5800254';

        $this->eventObserver = $this->createPartialMock(
            \Magento\Framework\Event\Observer::class,
            ['getCustomer', 'getRequest']
        );
        $this->request = $this->getMockBuilder(\Magento\Framework\App\RequestInterface::class)
            ->setMethods(['getPostValue'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->customer = $this->getMockForAbstractClass(
            \Magento\Customer\Api\Data\CustomerInterface::class,
            [],
            '',
            false
        );
        $this->attributeValue = $this->createPartialMock(
            \Magento\Framework\Event\Observer::class,
            ['getValue']
        );
        $this->attributeValue->expects($this->any())
            ->method('getValue')
            ->willReturn($attributeValue);
        $this->request->expects($this->any())
            ->method('getPostValue')
            ->willReturn($postData);
        $this->eventObserver->expects($this->once())
            ->method('getRequest')
            ->willReturn($this->request);
        $this->customer->expects($this->any())
            ->method('getCustomAttribute')
            ->willReturn($this->attributeValue);
        $this->eventObserver->expects($this->once())
            ->method('getCustomer')
            ->willReturn($this->customer);

        $this->observer->execute($this->eventObserver);
    }
}
