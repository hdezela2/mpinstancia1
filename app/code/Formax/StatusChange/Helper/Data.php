<?php

namespace Formax\StatusChange\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;

class Data extends AbstractHelper
{
    /**
     * @var string
     */
    const CUSTOMER_ENTITY_TABLE = 'customer_entity';

    /**
     * @var string
     */
    const CUSTOMER_ENTITY_VARCHAR_TABLE = 'customer_entity_varchar';

    /**
     * @var string
     */
    const CUSTOMER_ENTITY_INT_TABLE = 'customer_entity_int';

    /**
     * @var string
     */
    const MK_USER_DATA_TABLE = 'marketplace_userdata';

    /**
     * @var int
     */
    const ERROR = 0;

    /**
     * @var int
     */
    const OK = 1;

    /**
     * @var int
     */
    const EMAIL_NOT_FOUND = 2;

    /**
     * @var int
     */
    const MISS_PARAMS = 3;

    /**
     * @var int
     */
    const STATUS_CODE_NOT_FOUND = 4;

    /**
     * @var int
     */
    const EMAIL_COLUMN = 0;

    /**
     * @var int;
     */
    const STATUS_COLUMN = 4;

    /**
     * @var int
     */
    const ACTIVE_COLUMN = 10;

    /**
     * @var int
     */
    const CUSTOM_ERROR_CODE = 555;

    /**
     * @var array
     */
    const EXCLUDE_COLS_UPDATE = [0, 1, 2, 10];

    /**
     *
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     *
     * @var \Formax\StatusChange\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Formax\StatusChange\Model\ResourceModel\StatusMatch\CollectionFactory
     */
    protected $statusCollectionFactory;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $eavAttribute;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * @var int
     */
    protected $dccpStateDetailAtrribute;

    /**
     * @var int
     */
    protected $isVendorGroupAtrribute;
    private Context $context;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Formax\StatusChange\Logger\Logger $logger
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Formax\StatusChange\Model\ResourceModel\StatusMatch\CollectionFactory $statusCollectionFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Formax\StatusChange\Logger\Logger $logger,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
        \Magento\Framework\App\ResourceConnection $resource,
        \Formax\StatusChange\Model\ResourceModel\StatusMatch\CollectionFactory $statusCollectionFactory
    ) {
        $this->resource = $resource;
        $this->connection = $this->resource->getConnection();
        $this->eavAttribute = $eavAttribute;
        $this->statusCollectionFactory = $statusCollectionFactory;
        $this->httpClientFactory = $httpClientFactory;
        $this->logger = $logger;
        $this->context = $context;
        $this->jsonHelper = $jsonHelper;

        $this->dccpStateDetailAtrribute = $this->getAttributeCodeById('wkv_dccp_state_details');
        $this->isVendorGroupAtrribute = $this->getAttributeCodeById('is_vendor_group');
        parent::__construct($context);
    }

    /**
     * Set status to seller from REST service
     *
     * @param string $token
     * @param array $payLoad
     * @return Zend_Http_Response
     */
    public function setSellerStatus($token, $payLoad)
    {
        $response = null;
        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_status_change_data/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');

            if (!empty($endpoint) && !empty($token) && is_array($payLoad)
                && isset($payLoad['idProveedor']) && isset($payLoad['idEstadoProveedorConvenio']) && $payLoad['idConvenioMarco']) {
                $httpHeaders = [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . trim($token)
                ];

                $data = $this->jsonHelper->jsonEncode($payLoad);
                $endpoint .= '/proveedor/conveniomarco/';
                $this->logger->info('setSellerStatus REQUEST: ' . $data);

                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                    'timeout' => 30]
                );
                $client->setHeaders($httpHeaders);
                $client->setRawData($data);
                $response = $client->request(\Zend_Http_Client::POST)->getBody();

                $this->logger->info('setSellerStatus RESPONSE: Seller ID: ' . $payLoad['idProveedor'] . ' Agreement ID: ' . $payLoad['idConvenioMarco'] . ' Service Response: ' . $response);
                $response = $this->jsonHelper->jsonDecode($response);
            } else {
                throw new LocalizedException(__('Define endpoint and token change status seller webservice'));
            }
        } catch(LocalizedException $e) {
            $this->logger->critical('setSellerStatus Seller ID: ' . $payLoad['idProveedor'] . ' Agreement ID: ' . $payLoad['idConvenioMarco'] . ' Error message: ' . $e->getMessage());
        } catch(\Exception $e) {
            $this->logger->critical('setSellerStatus Seller ID: ' . $payLoad['idProveedor'] . ' Agreement ID: ' . $payLoad['idConvenioMarco'] . ' Error message: ' . $e->getMessage());
        }

        return $response;
    }

    /**
     * Set status to seller from REST service
     *
     * @return Zend_Http_Response
     */
    public function setSellerStatusMass()
    {
        $response = null;

        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_status_change_data/endpoint_mass',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');
            $token = trim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_status_change_data/token_mass_statuschange',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ));

            if (!empty($endpoint) && !empty($token)) {
                $httpHeaders = [
                    'Authorization' => 'Bearer ' . trim($token)
                ];

                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                    'timeout' => 30]
                );
                $client->setHeaders($httpHeaders);
                $response = $client->request(\Zend_Http_Client::GET)->getBody();

                $this->logger->info('setSellerStatusMass RESPONSE: Service Response: ' . $response);
            } else {
                throw new LocalizedException(__('Define endpoint and token change status seller mass webservice'));
            }
        } catch(LocalizedException $e) {
            $this->logger->critical('setSellerStatusMass: ' . ' Error message: ' . $e->getMessage());
        } catch(\Exception $e) {
            $this->logger->critical('setSellerStatusMass: ' . ' Error message: ' . $e->getMessage());
        }

        return $response;
    }

    /**
     * Retrive status DCCP
     *
     * @return array
     */
    public function getDccpStatusSeller()
    {
        return $this->statusCollectionFactory->create()->getDccpStatusSeller();
    }

    /**
     * Retrive status Magento
     *
     * @return array
     */
    public function getMagentoStatusSeller()
    {
        return $this->statusCollectionFactory->create()->getMagentoStatusSeller();
    }

    /**
     * Set status description and active seller by email
     *
     * @param string $email
     * @param int $statusId
     * @param int $active
     * @param array $colToUpdate
     * @return int
     */
    public function changeStatusSellerInDb($email, $statusId, $active, $colToUpdate)
    {
        $statusCode = self::ERROR;
        $dataTypeTable = '';

        if (!empty($email) && is_array($colToUpdate)) {
            try {
                $email = $this->connection->quote(trim($email));
                $sql = 'SELECT entity_id FROM ' . $this->resource->getTableName(self::CUSTOMER_ENTITY_TABLE) . ' WHERE email = ' . $email;
                $entityId = (int)$this->connection->fetchOne($sql);

                if ($entityId > 0) {
                    foreach ($colToUpdate as $key => $col) {
                        if (isset($col['value']) && trim($col['value']) !== ''
                        && isset($col['data_type']) && !empty($col['data_type']) && $key > 0) {
                            $valueToUpdate = $this->connection->quote(trim($col['value']));
                            $dataTypeTable = $col['data_type'] == 'varchar'
                            ? $this->resource->getTableName(self::CUSTOMER_ENTITY_VARCHAR_TABLE)
                            : $this->resource->getTableName(self::CUSTOMER_ENTITY_INT_TABLE);

                            $sql = 'INSERT INTO ' . $dataTypeTable .
                            ' (attribute_id, entity_id, value) VALUES (' . (int)$key . ', ' . $entityId . ', ' . $valueToUpdate .
                            ') ON DUPLICATE KEY UPDATE value = ' . $valueToUpdate;
                            $this->connection->query($sql);
                        }
                    }

                    if ($dataTypeTable != '') {
                        $sql = 'INSERT INTO ' . $dataTypeTable .
                        ' (attribute_id, entity_id, value) VALUES (' . $this->dccpStateDetailAtrribute . ', ' . $entityId . ', ' . $statusId .
                        ') ON DUPLICATE KEY UPDATE value = ' . $statusId;
                        $this->connection->query($sql);

                        $sql = 'INSERT INTO ' . $this->resource->getTableName(self::CUSTOMER_ENTITY_INT_TABLE) .
                        ' (attribute_id, entity_id, value) VALUES (' . $this->isVendorGroupAtrribute . ', ' . $entityId . ', ' . (int)$active .
                        ') ON DUPLICATE KEY UPDATE value = ' . (int)$active;
                        $this->connection->query($sql);

                        $sql = 'UPDATE ' .  $this->resource->getTableName(self::MK_USER_DATA_TABLE) . ' SET is_seller = ' . (int)$active .
                        ' WHERE seller_id = ' . $entityId;
                        $this->connection->query($sql);

                        $statusCode = self::OK;
                    } else {
                        self::MISS_PARAMS;
                        $this->logger->warning('changeStatusSeller: ' . $email . ' Value not found');
                    }
                } else {
                    $this->logger->warning('changeStatusSeller: ' . ' No se encontró el email: ' . $email);
                    $statusCode = self::EMAIL_NOT_FOUND;
                }
            } catch (\Exception $e) {
                $this->logger->critical('changeStatusSeller: Email: ' . $email . ' Error: ' . $e->getMessage());
            }
        } else {
            $statusCode = self::MISS_PARAMS;
        }

        return $statusCode;
    }

    /**
     * Retrive attribute code by ID
     *
     * @param string $code
     * @return mixed int/bool
     */
    public function getAttributeCodeById($code)
    {
        if (!empty($code)) {
            return (int)$this->eavAttribute->getIdByCode('customer', $code);
        }

        return false;
    }

    /**
     * Set status seller
     *
     * @return array
     */
    public function changeStatusSeller()
    {
        $this->connection->beginTransaction();

        try {
            $msg = '';
            $msgWarnig = '';
            $startTime = microtime(true);
            $statusMagento = $this->getMagentoStatusSeller();
            $response = $this->setSellerStatusMass();
            $response = utf8_encode($response);
            //$lines = preg_split('/[\r\n]{1,2}(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/', $response);
            $array = [];
            $header = [];
            $dataRows = [];
            $result = ['error' => false, 'msg' => ''];
            $i = 0;

            $lines2 = explode(PHP_EOL, $response);
            $array2 = array();
            foreach ($lines2 as $line2) {
                $array2[] = str_getcsv($line2);
            }

            foreach ($array2 as $line) {
                if (!empty(($line)) && count($line) > 1) {
                    $array = $line;
                    $countArray = count($array);

                    if ($countArray == 11) {
                        foreach ($array as $key => $row) {
                            if (!in_array($key, self::EXCLUDE_COLS_UPDATE)) {
                                if ($i === 0) {
                                    $header[$key] = $this->getAttributeCodeById($row);
                                } else {
                                    if (isset($statusMagento[$array[self::STATUS_COLUMN]])) {
                                        $dataRows[$header[$key]] = [
                                            'data_type' => 'varchar',
                                            'value' => $row
                                        ];
                                    } else {
                                        $msgWarnig .= 'Status code ' . $array[self::STATUS_COLUMN] . ' --> ' . $array[self::STATUS_COLUMN+1] . ' not found in match with Magento in row with email: ' . $array[self::EMAIL_COLUMN] . "\n";
                                        throw new \Exception('Miss params in row with email: ' . $array[self::EMAIL_COLUMN] . ' ' . $msgWarnig, self::CUSTOM_ERROR_CODE);
                                    }
                                }
                            }
                        }

                        if ($i > 0 && count($dataRows) > 0) {
                            if (isset($array[self::EMAIL_COLUMN]) && isset($array[self::ACTIVE_COLUMN])) {
                                $result = $this->changeStatusSellerInDb(
                                    $array[self::EMAIL_COLUMN],
                                    $statusMagento[$array[self::STATUS_COLUMN]],
                                    $array[self::ACTIVE_COLUMN],
                                    $dataRows
                                );

                                if ((int)$result === self::MISS_PARAMS) {
                                    throw new \Exception('Miss params in row with email: ' . $array[self::EMAIL_COLUMN] . ' Fix the error an run the process again', self::CUSTOM_ERROR_CODE);
                                } else if ((int)$result === self::ERROR) {
                                    throw new \Exception('An error occured in with row email: ' . $array[self::EMAIL_COLUMN] . ' Fix the error an run the process again', self::CUSTOM_ERROR_CODE);
                                } else if ((int)$result === self::EMAIL_NOT_FOUND) {
                                    $msgWarnig .= 'Email: ' . $array[self::EMAIL_COLUMN] . ' not found' . "\n";
                                }
                            } else {
                                $emailError = isset($array[self::EMAIL_COLUMN]) ? $array[self::EMAIL_COLUMN] : ($i+1);
                                throw new \Exception('Columns missing in row: ' . $emailError . ' Fix the error an run the process again', self::CUSTOM_ERROR_CODE);
                            }
                        }
                    } else {
                        $emailError = isset($array[self::EMAIL_COLUMN]) ? $array[self::EMAIL_COLUMN] : ($i+1);
                        throw new \Exception('Columns number must be 11 in row: ' . $emailError, self::CUSTOM_ERROR_CODE);
                    }
                }

                $i++;
            }

            $resultTime = microtime(true) - $startTime;
            if (empty($msgWarnig)) {
                $msg = 'seller:changestatus has been executed successfully in ' . gmdate('H:i:s', $resultTime);
            } else {
                $msg = 'seller:changestatus has been executed successfully but review these warnings: ' . "\n" . $msgWarnig . "It's end at " . gmdate('H:i:s', $resultTime);
            }
            $result = ['error' => false, 'msg' => $msg];
            $this->connection->commit();
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            $this->connection->rollBack();

            if ($e->getCode() == self::CUSTOM_ERROR_CODE) {
                $result = ['error' => true, 'msg' => $e->getMessage()];
            } else {
                $result = ['error' => true, 'msg' => 'An error occured while executing the process. ' . $e->getMessage()];
            }
        }

        return $result;
    }

    /**
     * Set value to column is_seller into marketplace_userdata table
     *
     * @param int $sellerId
     * @param int $value
     * @return bool
     */
    public function setStatusSeller($sellerId, $value)
    {
        $where = ['seller_id = ?' => (int)$sellerId];
        return $this->connection->update(
            $this->resource->getTableName('marketplace_userdata'),
            ['is_seller' => (int)$value],
            $where
        );
    }
}
