<?php

namespace Formax\StatusChange\Model;

class StatusMatch extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\StatusChange\Model\ResourceModel\StatusMatch::class);
    }
}