<?php

namespace Formax\StatusChange\Model\ResourceModel\StatusMatch;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define main  tables
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Formax\StatusChange\Model\StatusMatch::class,
            \Formax\StatusChange\Model\ResourceModel\StatusMatch::class
        );
    }

    /**
     * Filter by DCCP Status
     *
     * @param int|array $dccpStatus
     * @return $this
     */
    public function addDccpStatusFilter($dccpStatus)
    {
        if (!empty($dccpStatus)) {
            if (is_array($dccpStatus)) {
                $this->addFieldToFilter('main_table.dccp_status', ['in' => $dccpStatus]);
            } else {
                $this->addFieldToFilter('main_table.dccp_status', $dccpStatus);
            }
        }

        return $this;
    }

    /**
     * Filter by Magento Status
     *
     * @param int|array $mageStatus
     * @return $this
     */
    public function addMageStatusFilter($mageStatus)
    {
        if (!empty($mageStatus)) {
            if (is_array($mageStatus)) {
                $this->addFieldToFilter('main_table.mage_status', ['in' => $mageStatus]);
            } else {
                $this->addFieldToFilter('main_table.mage_status', $mageStatus);
            }
        }

        return $this;
    }

    /**
     * Retrive status DCCP
     * 
     * @return array
     */
    public function getDccpStatusSeller()
    {
        $option = [];
        foreach ($this as $item) {
            $option[$item->getMageStatus()] = $item->getDccpStatus();
        }

        return $option;
    }

    /**
     * Retrive status Magento
     * 
     * @return array
     */
    public function getMagentoStatusSeller()
    {
        $option = [];
        foreach ($this as $item) {
            $option[$item->getDccpStatus()] = $item->getMageStatus();
        }

        return $option;
    }
}
