<?php

namespace Formax\StatusChange\Model\ResourceModel;

class StatusMatch extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main  table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('dccp_seller_status', 'id');
    }
}