<?php

namespace Formax\StatusChange\Cron;

use Formax\StatusChange\Helper\Data as Helper;

class StatusSeller
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param Helper $helper
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        Helper $helper
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
    }

    public function execute()
    {
        $result = $this->helper->changeStatusSeller();
        $msg = isset($result['msg']) ? $result['msg'] : '';
        
        if (!$result['error']) {
            $this->logger->info('Cron Task Success seller_changestatus => ' . $msg);
        } else {
            $this->logger->critical('Cron Task Error seller_changestatus => ' . $msg);
        }
    }
}
