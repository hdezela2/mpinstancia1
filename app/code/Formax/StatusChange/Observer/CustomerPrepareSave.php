<?php

namespace Formax\StatusChange\Observer;

use Magento\Framework\Event\ObserverInterface;
use Formax\StatusChange\Helper\Data;

class CustomerPrepareSave implements ObserverInterface
{
    /**
     * @var \Formax\StatusChange\Helper\Data
     */
    protected $helper;

    /**
     * @var \Formax\StatusChange\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @param \Formax\StatusChange\Helper\Data $helper
     * @param \Formax\StatusChange\Logger\Logger $logger
     * @param \Magento\Backend\Model\Auth\Session $authSession
     */
    public function __construct(
        \Formax\StatusChange\Helper\Data $helper,
        \Formax\StatusChange\Logger\Logger $logger,
        \Magento\Backend\Model\Auth\Session $authSession
    ) {
        $this->authSession = $authSession;
        $this->logger = $logger;
        $this->helper = $helper;
    }

    /**
     * admin customer save after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer = $observer->getCustomer();
        $status = $this->helper->getDccpStatusSeller();
        $postData = $observer->getRequest()->getPostValue();
        $authSession = $this->authSession->getUser();
        $sellerId = $customer->getCustomAttribute('user_rest_id') ?
                    $customer->getCustomAttribute('user_rest_id')->getValue() : '';
        $agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
                    $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : '';
        $statusMage = isset($postData['customer']['wkv_dccp_state_details']) ?
            $postData['customer']['wkv_dccp_state_details'] : false;
        
        if ($authSession && $sellerId && $agreementId) {
            if ($statusMage && !empty($statusMage) && isset($status[$statusMage])) {
                $token = $authSession->getUserRestAtk();
                if (empty($token) || $token === NULL) {
                    $token = $customer->getCustomAttribute('user_rest_atk') ? $customer->getCustomAttribute('user_rest_atk')->getValue() : '';
                }
                
                $payLoad = [
                    'idProveedor' => $sellerId,
                    'idConvenioMarco' => $agreementId,
                    'idEstadoProveedorConvenio' => $status[$statusMage]
                ];
                
                // Call DCCP Webservice
                $response = $this->helper->setSellerStatus(
                    $token,
                    $payLoad
                );

                if (!isset($response['success']) || (isset($response['success']) && $response['success'] != 'OK')) {
                    $this->logger->critical('Webservice response error.');
                    $errorMsg = __('An error occurred when you trying to change seller status.');
                    if(isset($response['errores'])){
                        if(is_array($response['errores'])){
                            $errorMsg = '';
                            foreach($response['errores'] as $error){
                                $errorMsg .= $error['codigo'].' '.$error['descripcion']."\n";
                            }
                        }
                    }
                    throw new \Exception($errorMsg);
                } else {
                    if ($status[$statusMage] == '1') {
                        $customer->setCustomAttribute('is_vendor_group', 1);
                        $this->helper->setStatusSeller($customer->getId(), 1);
                    }
                    
                    if ($status[$statusMage] == '6') {
                        $customer->setCustomAttribute('is_vendor_group', 0);
                        $this->helper->setStatusSeller($customer->getId(), 0);
                    }

                    return $this;
                }
            } else {
                $this->logger->critical('Status ID not found in table dccp_seller_status or is empty.');
                throw new \Exception('Status ID not found in table dccp_seller_status or is empty.');
            }
        }

        return $this;
    }
}
