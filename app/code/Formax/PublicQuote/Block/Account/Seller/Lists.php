<?php

namespace Formax\PublicQuote\Block\Account\Seller;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Response\Http as HttpResponse;
use Formax\ConfigCmSoftware\Helper\Data as ConfigCmSoftwareHelper;

/**
 * Lists class
 */
class Lists extends Template
{
    /**
     * @var \Webkul\Requestforquote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Formax\ConfigCmSoftware\Helper\Data
     */
    protected $cmSoftwareHelper;

    /**
     * @var int
     */
    protected $quoteId;

    /**
     * @var \Magento\Framework\App\Response\Http
     */
    protected $response;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Formax\ConfigCmSoftware\Helper\Data $cmSoftwareHelper
     * @param \Webkul\Requestforquote\Model\QuoteFactory
     * @param \Magento\Framework\App\Response\Http $response
     * @param array $data
     */
    public function __construct(
        Context $context,
        ConfigCmSoftwareHelper $cmSoftwareHelper,
        QuoteFactory $quoteFactory,
        HttpResponse $response,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $data
        );

        $this->quoteFactory = $quoteFactory;
        $this->cmSoftwareHelper = $cmSoftwareHelper;
        $this->response = $response;
        $this->quoteId = (int)$this->getRequest()->getParam('id', 0);
    }

    /**
     * Get quote ID
     * 
     * @return int
     */
    public function getQuotedId()
    {
        return $this->quoteId;
    }

    /**
     * Get Quote by current quote_id
     * 
     * @return Webkul\Requestforquote\Model\Quote
     */
    public function getQuote()
    {
        $quote = $this->quoteFactory->create()->load($this->quoteId);

        return $quote;
    }

    /**
     * Get product object by ID
     * 
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProduct()
    {
        $product = null;

        try {
            $quote = $this->getQuoteById($this->quoteId);
            if ($quote) {
                $product = $this->cmSoftwareHelper->getProduct($quote->getProductId());
            }
        } catch (NoSuchEntityException $e) {}

        return $product;
    }

    /**
     * _prepareLayout
     *
     * @return void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $collection = $this->getSellersFromQuote();

        if ($collection) {
            $blockName = 'publicquotes.sellers.list.pager';
            if ($this->getLayout()->getBlock($blockName)) {
                $pager = $this->getLayout()->getBlock($blockName)
                    ->setAvailableLimit([10 => 10])
                    ->setShowPerPage(false)
                    ->setCollection($collection);
            } else {
                $pager = $this->getLayout()->createBlock(
                    'Magento\Theme\Block\Html\Pager',
                    $blockName
                )
                ->setAvailableLimit([10 => 10])
                ->setShowPerPage(false)
                ->setCollection($collection);
            }

            $this->setChild('pager', $pager);
            $collection->load();
        }
        return $this;
    }

    /**
     * Get paginator
     * 
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get download URL for files
     * 
     * @param object $quote
     * @return string
     */
    public function getDownloadUrl($quote)
    {
        return $this->getUrl(
            'publicquotes/seller/download',
            [
                'seller_id' => $quote->getSellerId(),
                'quote_id' => $quote->getQuoteId()
            ]
        );
    }

    /**
     * Get download URL for all files
     * 
     * @return string
     */
    public function getDownloadAllUrl()
    {
        return $this->getUrl('publicquotes/seller/download');
    }

    /**
     * Get URL seller profile
     * 
     * @param string $sellerProfileId
     * @return string
     */
    public function getUrlSellerProfile($sellerProfileId)
    {
        $url = '#';
        if ($sellerProfileId) {
            $url = $this->getUrl('marketplace/seller/profile', ['shop' => $sellerProfileId]);
        }

        return $url;
    }

    /**
     * Get seller from quote
     * 
     * @param array
     */
    public function getSellersFromQuote()
    {
        if ($this->quoteId > 0) {
            $page = $this->getRequest()->getParam('p') ? $this->getRequest()->getParam('p') : 1;
            $pageSize = $this->getRequest()->getParam('limit') ? $this->getRequest()->getParam('limit') : 10;
            $quotes = $this->cmSoftwareHelper->getSellersFromQuote($this->quoteId);
            $quotes->setPageSize($pageSize)
                ->setCurPage($page);
            return $quotes;
        }

        return null;
    }
}
 