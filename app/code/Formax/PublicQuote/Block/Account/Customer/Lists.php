<?php

namespace Formax\PublicQuote\Block\Account\Customer;

use Formax\QuotesSupplier\Helper\Data as QuoteSupplierHelper;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Catalog\Model\ProductRepository;
use Magento\Eav\Model\Attribute;
use Magento\Eav\Model\Entity;
use Magento\Customer\Model\CustomerFactory;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\Requestforquote\Helper\Data as RequestforquoteHelper;
use Formax\ConfigCmSoftware\Helper\Data as ConfigCmSoftwareHelper;
use Formax\QuotesSupplier\Block\Account\Customer\Lists as FormaxCustomerLists;
use Webkul\Requestforquote\Model\QuoteFactory;
use Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory;

/**
 * Lists class
 */
class Lists extends FormaxCustomerLists
{
    private ResourceConnection $resourceConnection;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory
     * @param QuoteSupplierHelper $quoteSupplierHelper
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Webkul\Requestforquote\Helper\Data $helper
     * @param \Formax\ConfigCmSoftware\Helper\Data $cmSoftwareHelper
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Eav\Model\Attribute $eavAttribute
     * @param \Magento\Eav\Model\Entity $entity
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Webkul\Marketplace\Helper\Data $helperMp
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $quoteCollectionFactory,
        QuoteSupplierHelper $quoteSupplierHelper,
        MarketplaceHelper $mpHelper,
        RequestforquoteHelper $helper,
        ConfigCmSoftwareHelper $cmSoftwareHelper,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Eav\Model\Attribute $eavAttribute,
        \Magento\Eav\Model\Entity $entity,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Webkul\Marketplace\Helper\Data $helperMp,
        array $data = []
    ) {
        $this->resourceConnection = $resourceConnection;
        parent::__construct(
            $context,
            $quoteCollectionFactory,
            $quoteSupplierHelper,
            $mpHelper,
            $helper,
            $cmSoftwareHelper,
            $resourceConnection,
            $productRepository,
            $eavAttribute,
            $entity,
            $customerFactory,
            $helperMp
        );

        $this->setCollection($this->setCollectionForList());
    }

    protected function setCollectionForList()
    {
        $websiteId = $this->cmSoftwareHelper->getStoreManager()->getWebsite()->getId();
        $quotes = $this->quoteCollectionFactory->create();
        $quotes->setOrder('entity_id', 'DESC')
            ->getSelect()
            ->join(
                ['w' => $this->resourceConnection->getTableName('catalog_product_website')],
                'main_table.product_id = w.product_id AND w.website_id = ' . $websiteId,
                []
            )
            ->joinLeft(
                ['cv' => $this->resourceConnection->getTableName('requestforquote_quote_conversation')],
                'main_table.approved_seller_quote_id = cv.entity_id AND cv.edited = 1',
                []
            )
            ->joinLeft(
                ['info' => $this->resourceConnection->getTableName('requestforquote_quote_info')],
                'cv.seller_quote_id = info.entity_id',
                []
            )
            ->joinLeft(
                ['seller' => $this->resourceConnection->getTableName('customer_entity')],
                'info.seller_id = seller.entity_id',
                ['seller_name' => new \Zend_Db_Expr('CONCAT(firstname, " ", lastname)')]
            )
            ->joinLeft(
                ['u' => $this->resourceConnection->getTableName('marketplace_userdata')],
                'seller.entity_id = u.seller_id',
                ['shop_url']
            )
            ->joinLeft(
                ['oc' => $this->resourceConnection->getTableName('sales_order')],
                'main_table.entity_id = oc.requestforquote_info',
                ['url_orden_de_compra', 'orden_de_compra']
            )->group('main_table.entity_id');

        if ($this->getRequest()->getParam('quote_id') != '') {
            $filterQuoteId = trim($this->getRequest()->getParam('quote_id'));
            $quotes->addFieldToFilter('entity_id', $filterQuoteId);
        }
        if ($this->getRequest()->getParam('quote_name') != '') {
            $filterQuoteName = trim($this->getRequest()->getParam('quote_name'));
            $quotes->addFieldToFilter('description', ['like' => '%' . $filterQuoteName . '%']);
        }
        if ($this->getRequest()->getParam('oc') != '') {
            $filterOc = trim($this->getRequest()->getParam('oc'));
            $quotes->addFieldToFilter('orden_de_compra', $filterOc);
        }
        if ($this->getRequest()->getParam('organization_name') != '') {
            $filterOrganization = trim($this->getRequest()->getParam('organization_name'));
            $quotes->addFieldToFilter('organization', ['like' => '%' . $filterOrganization . '%']);
        }
        if ($this->getRequest()->getParam('status') != '') {
            $filterStatus = trim($this->getRequest()->getParam('status'));
            $quotes->addFieldToFilter('main_table.status', $filterStatus);
        } else {
            $quotes->addFieldToFilter('main_table.status', ['nin' => array(6, 10)]);
        }
        if ($this->getRequest()->getParam('service_type') != '') {
            $filterServiceType = trim($this->getRequest()->getParam('service_type'));
            $quotes->addFieldToFilter('main_table.subject', $filterServiceType);
        }
        if ($this->getRequest()->getParam('awarded_date') != '') {
            $filterAwardedDateIni = trim($this->getRequest()->getParam('awarded_date')) . ' 00:00:00';
            $filterAwardedDateEnd = trim($this->getRequest()->getParam('awarded_date')) . ' 23:59:59';
            $quotes->addFieldToFilter('main_table.award_date', ['gteq' => $filterAwardedDateIni])
                ->addFieldToFilter('main_table.award_date', ['lteq' => $filterAwardedDateEnd]);
        }
        if ($this->getRequest()->getParam('publication_start') != '') {
            $filterPublicationStartDateIni = trim($this->getRequest()->getParam('publication_start')) . ' 00:00:00';
            $filterPublicationStartDateEnd = trim($this->getRequest()->getParam('publication_start')) . ' 23:59:59';
            $quotes->addFieldToFilter('main_table.created_at', ['gteq' => $filterPublicationStartDateIni])
                ->addFieldToFilter('main_table.created_at', ['lteq' => $filterPublicationStartDateEnd]);
        }
        if ($this->getRequest()->getParam('publication_end') != '') {
            $filterPublicationEndDateIni = trim($this->getRequest()->getParam('publication_end')) . ' 00:00:00';
            $filterPublicationEndDateEnd = trim($this->getRequest()->getParam('publication_end')) . ' 23:59:59';
            $quotes->addFieldToFilter('main_table.publication_end', ['gteq' => $filterPublicationEndDateIni])
                ->addFieldToFilter('main_table.publication_end', ['lteq' => $filterPublicationEndDateEnd]);
        }
        if ($this->getRequest()->getParam('evaluation_start') != '') {
            $filterEvaluationStartDateIni = trim($this->getRequest()->getParam('evaluation_start')) . ' 00:00:00';
            $filterEvaluationStartDateEnd = trim($this->getRequest()->getParam('evaluation_start')) . ' 23:59:59';
            $quotes->addFieldToFilter('main_table.evaluation_start', ['gteq' => $filterEvaluationStartDateIni])
                ->addFieldToFilter('main_table.evaluation_start', ['lteq' => $filterEvaluationStartDateEnd]);
        }
        if ($this->getRequest()->getParam('evaluation_end') != '') {
            $filterEvaluationEndDateIni = trim($this->getRequest()->getParam('evaluation_end')) . ' 00:00:00';
            $filterEvaluationEndDateEnd = trim($this->getRequest()->getParam('evaluation_end')) . ' 23:59:59';
            $quotes->addFieldToFilter('main_table.evaluation_end', ['gteq' => $filterEvaluationEndDateIni])
                ->addFieldToFilter('main_table.evaluation_end', ['lteq' => $filterEvaluationEndDateEnd]);
        }

        return $quotes;
    }

    /**
     * Get URL seller profile
     *
     * @param string $sellerProfileId
     * @return string
     */
    public function getUrlSellerProfile($sellerProfileId)
    {
        $url = '#';
        if ($sellerProfileId) {
            $url = $this->getUrl('marketplace/seller/profile', ['shop' => $sellerProfileId]);
        }

        return $url;
    }


    /**
     * Get URL requestforquote list
     *
     * @return string
     */
    public function getUrlList()
    {
        return $this->getUrl('publicquotes/requestforquote/lists');
    }

    /**
     * Get URL for requestforquote detail
     *
     * @param int $requestforquoteId
     * @return string
     */
    public function getUrlDetail($requestforquoteId)
    {
        $url = '#';
        if ($requestforquoteId) {
            $url = $this->getUrl('publicquotes/requestforquote/view', ['id' => $requestforquoteId]);
        }

        return $url;
    }

    /**
     * Get seller URL
     *
     * @param int $requestforquoteId
     * @return string
     */
    public function getSellerUrl($requestforquoteId)
    {
        return $this->getUrl('publicquotes/seller/lists', ['id' => $requestforquoteId]);
    }

    /**
     * Get download all URL
     *
     * @param array $params
     * @return string
     */
    public function getDownloadUrl($params)
    {
        return $this->getUrl('publicquotes/requestforquote/downloadall', $params);
    }

    /**Get categories level 3 from CM Software
     *
     * @param int $requestforquoteId
     * @return string
     */
    public function getCategoriesCmSoftware()
    {
        return $this->cmSoftwareHelper->getCategoriesCmSoftware();
    }
}
