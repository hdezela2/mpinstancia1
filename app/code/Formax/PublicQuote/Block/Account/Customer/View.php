<?php

namespace Formax\PublicQuote\Block\Account\Customer;

use Formax\PublicQuote\Helper\Data as PublicQuoteHelper;
use Formax\QuotesSupplier\Helper\Data as QuoteSupplierHelper;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\ResourceConnection;
use Magento\Customer\Model\CustomerFactory;
use Magento\Catalog\Model\ProductFactory;
use Webkul\Requestforquote\Model\ConversationFactory;
use Webkul\Requestforquote\Model\InfoFactory;
use Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory as InfoCollectionFactory;
use Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory as ConversationCollectionFactory;
use Webkul\Marketplace\Model\ProductFactory as WkProductFactory;
use Webkul\Requestforquote\Helper\Data as HelperRequestforquote;
use Magento\Catalog\Helper\Image as HelperImage;
use Magento\Catalog\Helper\Output as HelperOutput;
use Magento\Framework\Pricing\Helper\Data as HelperPricing;
use Webkul\Marketplace\Helper\Data as HelperMarketplace;
use Formax\ConfigCmSoftware\Helper\Data as CmSoftwareHelper;
use Formax\QuotesSupplier\Block\Customer\View as FormaxCustomerView;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as MpSellerCollectionFactory;
use Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory as quoteCollectionFactory;
use Magento\Framework\Filesystem\Io\File;

class View extends FormaxCustomerView
{
    /** @var \Formax\PublicQuote\Helper\Data */
    protected $_publicQuoteHelper;
    private QuoteFactory $quoteFactory;
    private int $quoteId;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Catalog\Model\ProductFactory $catalogProductFactory
     * @param \Webkul\Requestforquote\Model\QuoteFactory $quote
     * @param \Webkul\Requestforquote\Model\ConversationFactory $conversation
     * @param \Webkul\Requestforquote\Model\InfoFactory $info
     * @param \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoFactory
     * @param \Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory $conversationFactory
     * @param \Webkul\Marketplace\Model\ProductFactory $product
     * @param \Webkul\Requestforquote\Helper\Data $helper
     * @param \Magento\Catalog\Helper\Image $catalogImgHelper
     * @param \Magento\Catalog\Helper\Output $catalogOpHelper
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $mpSellerColl
     * @param \Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory $quoteCollection
     * @param \Formax\ConfigCmSoftware\Helper\Data $cmSoftwareHelper
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param array $data
     */
    public function __construct(
        Context $context,
        CustomerFactory $customerFactory,
        ProductFactory $catalogProductFactory,
        QuoteFactory $quote,
        ConversationFactory $conversation,
        InfoFactory $info,
        InfoCollectionFactory $infoFactory,
        ConversationCollectionFactory $conversationFactory,
        WkProductFactory $product,
        HelperRequestforquote $helper,
        HelperImage $catalogImgHelper,
        HelperOutput $catalogOpHelper,
        HelperPricing $priceHelper,
        HelperMarketplace $mpHelper,
        MpSellerCollectionFactory $mpSellerColl,
        File $file,
        QuoteCollectionFactory $quoteCollection,
        CmSoftwareHelper $cmSoftwareHelper,
        ResourceConnection $resourceConnection,
        QuoteSupplierHelper $quoteSupplierHelper,
        PublicQuoteHelper $publicQuoteHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $customerFactory,
            $catalogProductFactory,
            $quote,
            $conversation,
            $info,
            $infoFactory,
            $conversationFactory,
            $product,
            $helper,
            $catalogImgHelper,
            $catalogOpHelper,
            $priceHelper,
            $mpHelper,
            $mpSellerColl,
            $file,
            $quoteCollection,
            $cmSoftwareHelper,
            $resourceConnection,
            $quoteSupplierHelper,
            $data
        );

        $this->quoteFactory = $quote;
        $this->quoteId = (int)$this->getRequest()->getParam('id', 0);
        $this->_publicQuoteHelper = $publicQuoteHelper;
    }

    /**
     * Get seller URL
     *
     * @return string
     */
    public function getSellerUrl()
    {
        return $this->getUrl('publicquotes/seller/lists', ['id' => $this->quoteId]);
    }

    /**
     * Get download URL for files
     *
     * @param object $quote
     * @return string
     */
    public function getDownloadSellerAwardedUrl($quote)
    {
        return $this->getUrl(
            'publicquotes/seller/download',
            [
                'seller_id' => $quote->getSellerId(),
                'quote_id' => $quote->getEntityId()
            ]
        );
    }

    /**
     * Get download URL for requestforquote and files
     *
     * @param object $quote
     * @return string
     */
    public function getDownloadQuoteUrl($quote)
    {
        return $this->getUrl(
            'publicquotes/requestforquote/download',
            [
                'quote_id' => $quote->getEntityId()
            ]
        );
    }

    /**
     * Get seller from quote
     *
     * @param array
     */
    public function getSellersCountFromQuote()
    {
        $quoteId = $this->quoteId;
        $collection = $this->infoCollectionFactory->create();
        $collection->addFieldToFilter('quote_id', $quoteId)
            ->addFieldToFilter('status', ['gteq' => 2])
            ->getSelect()->join(
                    ['cv' => $this->resourceConnection->getTableName('requestforquote_quote_conversation')],
                    'main_table.entity_id = cv.seller_quote_id AND cv.edited = 1',
                    []
            );
        return $collection->getSize();
    }

    public function getQuoteId()
    {
        return $this->quoteId;
    }

    /**
     * Get Quote by current quote_id
     *
     * @return Webkul\Requestforquote\Model\Quote
     */
    public function getQuoteDetail()
    {
        return $this->_publicQuoteHelper->getQuoteDetail($this->quoteId);
    }
}
