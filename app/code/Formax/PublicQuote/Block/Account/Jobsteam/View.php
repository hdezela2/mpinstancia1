<?php

namespace Formax\PublicQuote\Block\Account\Jobsteam;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * JobsTeam class
 */
class View extends Template
{

    private $_info;

    private $_idQuote;

    private $_quote;

    private $_comprador;

    private $_teamJobFactory;

    private $_servicesFactory;

    private $_dir;

    protected $_serializer;

    protected $_storeManager;

    private $_orderFactory;

    private $_resource;

    public $priceHelper;

    protected $helperMp;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\RequestInterface $request,
        \Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory $quote,
        \Formax\QuotesSupplier\Model\TeamJobFactory $teamJobFactory,
        \Formax\QuotesSupplier\Model\ServicesFactory $servicesFactory,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Sales\Api\OrderRepositoryInterface $orderFactory,
        SerializerInterface $serializer,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Webkul\Marketplace\Helper\Data $helperMp,
        array $data = []
    ) {
        $this->_idQuote = $request->getParam('id', 0);
        $this->_quote = $quote;
        $this->_teamJobFactory = $teamJobFactory;
        $this->_servicesFactory = $servicesFactory;
        $this->_dir = $dir;
        $this->_serializer = $serializer;
        $this->_storeManager = $storeManager;
        $this->_orderFactory = $orderFactory;
        $this->_resource = $resource;
        $this->priceHelper = $priceHelper;
        $this->helperMp = $helperMp;

        parent::__construct($context, $data);

    }

    public function getQuoteInfo()
    {
        $quote = $this->_quote->create();
        $quote->addFieldToFilter('entity_id', $this->_idQuote)
            ->getSelect()->join(
                ['c' => $this->_resource->getTableName('requestforquote_quote_conversation')],
                'main_table.approved_seller_quote_id = c.entity_id',
                []
            )->join(
                ['i' => $this->_resource->getTableName('requestforquote_quote_info')],
                'c.seller_quote_id = i.entity_id',
                []
            )->join(
                ['e' => $this->_resource->getTableName('customer_entity')],
                'i.seller_id = e.entity_id',
                ['seller_name' => new \Zend_Db_Expr('CONCAT(e.firstname, " ", e.lastname)') ]
            );
        if ($quote) {
            foreach ($quote as $value) {
                return $value;
            }
        }

        return $quote;
    }

    public function getCollectionTeamJob()
    {
        $collection = $this->_teamJobFactory->create()->getCollection();
        $collection->addFieldToFilter('quote_id', ['eq' => $this->_idQuote])
            ->addFieldToFilter('active', 1);
        return $collection;
    }

    public function getCollectionServices()
    {
        $collection = $this->_servicesFactory->create()->getCollection();
        $collection->addFieldToFilter('quote_id', ['eq' => $this->_idQuote])
            ->addFieldToFilter('active', 1);
        return $collection;
    }

    public function getPathMedia()
    {
        return $this->_dir->getPath('media');
    }

    public function unSerializeData($extraData)
    {
        $additionalData = $this->_serializer->unserialize($extraData);
        return $additionalData;
    }

    public function getUrlBaseInforme()
    {
        $tmp = str_replace(array("software/","software2022/"), "" , $this->_storeManager->getStore()->getBaseUrl());
        return $tmp . 'media/informeteam/files';
    }

    public function getValueNumber($amount)
    {
        return $this->priceHelper->currency($amount, true, false);
    }
}
