<?php

namespace Formax\PublicQuote\Block\Account\Rejected;

use Formax\ConfigCmSoftware\Helper\Data as ConfigCmSoftwareHelper;
use Formax\EvaluateRequestforquote\Model\RejectedQuoteInfoFactory;
use Formax\EvaluateRequestforquote\Model\Repository\RejectedQuoteInfoRepository;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Exception\NoSuchEntityException;

class Lists extends Template
{
    /** @var int */
    protected $_quoteId;

    /** @var RejectedQuoteInfoFactory */
    protected $_rejectedQuoteInfo;

    /** @var RejectedQuoteInfoRepository */
    protected $_rejectedQuoteInfoRepository;

    /** @var ConfigCmSoftwareHelper */
    private $_cmSoftwareHelper;

    public function __construct(
        Context $context,
        RejectedQuoteInfoFactory $rejectedQuoteInfo,
        RejectedQuoteInfoRepository $rejectedQuoteInfoRepository,
        ConfigCmSoftwareHelper $cmSoftwareHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $data
        );
        $this->_quoteId = (int)$this->getRequest()->getParam('id', 0);
        $this->_rejectedQuoteInfo = $rejectedQuoteInfo;
        $this->_rejectedQuoteInfoRepository = $rejectedQuoteInfoRepository;
        $this->_cmSoftwareHelper = $cmSoftwareHelper;
    }

    public function getQuotedId()
    {
        return $this->_quoteId;
    }

    public function getRejectedQuoteInfo()
    {
        return $this->_rejectedQuoteInfoRepository->getByQuoteId($this->getQuotedId());
    }

    public function getRejectedJustification()
    {
        $rejectedQuoteInfo = $this->getRejectedQuoteInfo();
        return $rejectedQuoteInfo->getJustification();
    }

    public function getRejectedReportDocuments()
    {
        return $this->getRejectedQuoteInfo()->getRejectedReportDocuments();
    }

    public function getAllRejectedFiles($rejectedReportDocuments)
    {
        return $this->_cmSoftwareHelper->getFilesPath($rejectedReportDocuments);
    }
}
