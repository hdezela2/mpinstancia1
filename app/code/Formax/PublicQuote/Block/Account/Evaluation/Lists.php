<?php

namespace Formax\PublicQuote\Block\Account\Evaluation;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\Requestforquote\Model\QuoteFactory;
use Magento\Framework\App\Response\Http as HttpResponse;
use Formax\ConfigCmSoftware\Helper\Data as ConfigCmSoftwareHelper;
use Formax\EvaluateRequestforquote\Model\ResourceModel\TechnicalEvaluation\CollectionFactory as CollectionTechnical;
use Formax\EvaluateRequestforquote\Model\ResourceModel\EconomicEvaluation\CollectionFactory as CollectionEconomic;

/**
 * Lists class
 */
class Lists extends Template
{
    /**
     * @var \Formax\EvaluateRequestforquote\Model\ResourceModel\TechnicalEvaluation\CollectionFactory
     */
    protected $collectionTechnical;

    /**
     * @var \Formax\EvaluateRequestforquote\Model\ResourceModel\EconomicEvaluation\CollectionFactory
     */
    protected $collectionEconomic;

    /**
     * @var \Formax\ConfigCmSoftware\Helper\Data
     */
    protected $cmSoftwareHelper;

    /**
     * @var int
     */
    protected $quoteId;

    /**
     * @var \Magento\Framework\App\Response\Http
     */
    protected $response;

    /**
     * @var int
     */
    protected $websiteId;
    private QuoteFactory $quoteFactory;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Formax\ConfigCmSoftware\Helper\Data $cmSoftwareHelper
     * @param \Webkul\Requestforquote\Model\QuoteFactory
     * @param \Magento\Framework\App\Response\Http $response
     * @param \Formax\EvaluateRequestforquote\Model\ResourceModel\TechnicalEvaluation\CollectionFactory $collectionTechnical
     * @param \Formax\EvaluateRequestforquote\Model\ResourceModel\EconomicEvaluation\CollectionFactory $collectionEconomic
     * @param array $data
     */
    public function __construct(
        Context $context,
        ConfigCmSoftwareHelper $cmSoftwareHelper,
        QuoteFactory $quoteFactory,
        HttpResponse $response,
        CollectionTechnical $collectionTechnical,
        CollectionEconomic $collectionEconomic,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $data
        );

        $this->quoteFactory = $quoteFactory;
        $this->cmSoftwareHelper = $cmSoftwareHelper;
        $this->collectionTechnical = $collectionTechnical;
        $this->collectionEconomic = $collectionEconomic;
        $this->response = $response;
        $this->quoteId = (int)$this->getRequest()->getParam('id', 0);
        $this->websiteId = $this->cmSoftwareHelper->getStoreManager()->getWebsite()->getId();
    }

    /**
     * Get quote ID
     *
     * @return int
     */
    public function getQuotedId()
    {
        return $this->quoteId;
    }

    /**
     * Get download URL for files
     *
     * @param object $quote
     * @return string
     */
    public function getDownloadAllEconomicUrl()
    {
        return $this->getUrl('publicquotes/evaluation/downloadeconomic');
    }

    /**
     * Get download URL for all files
     *
     * @return string
     */
    public function getDownloadAllTechnicalUrl()
    {
        return $this->getUrl('publicquotes/evaluation/downloadtechnical');
    }

    /**
     * Get technical evaluation awarded seller
     *
     * @param int $sellerId
     * @param array
     */
    public function getEvaluationTechnicalCollection()
    {
        $collection = false;
        if ($this->quoteId > 0) {
            $collection = $this->collectionTechnical->create();
            $collection->addFieldToFilter('quote_id', $this->quoteId)
                ->addFieldToFilter('website_id', $this->websiteId)
                ->setPageSize(1);
        }

        return $collection;
    }

    /**
     * Get technical evaluation awarded seller
     *
     * @param int $sellerId
     * @param array
     */
    public function getEvaluationEconomicCollection()
    {
        $collection = false;
        if ($this->quoteId > 0) {
            $collection = $this->collectionEconomic->create();
            $collection->addFieldToFilter('info.quote_id', $this->quoteId)
                ->addFieldToFilter('main_table.website_id', $this->websiteId)
                ->setPageSize(1);
            $collection->getSelect()->join(
                ['info' => $collection->getTable('requestforquote_quote_info')],
                'main_table.seller_quote_id = info.entity_id',
                ['info.seller_id', 'info.quote_id']
            );
        }

        return $collection;
    }

    /**
     * Join all files in array
     *
     * @param string $document1
     * @param string $document1
     * @param string $document1
     * @return array|false
     */
    public function getAllEvaluationFiles($document1, $document2, $document3)
    {
        $docs1 = [];
        $docs2 = [];
        $docs3 = [];

        if ($document1) {
            $docs1 = ['Acta de evaluación' => $this->cmSoftwareHelper->getFilesPath($document1)];
        }
        if ($document2) {
            $docs2 = ['Acto administrativo que aprueba' => $this->cmSoftwareHelper->getFilesPath($document2)];
        }
        if ($document3) {
            $docs3 = ['Anexos complementarios' => $this->cmSoftwareHelper->getFilesPath($document3)];
        }

        return array_merge($docs1, $docs2, $docs3);
    }
}
