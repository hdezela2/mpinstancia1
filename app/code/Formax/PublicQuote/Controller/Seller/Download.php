<?php

namespace Formax\PublicQuote\Controller\Seller;

use Formax\PublicQuote\Controller\DownloadController;
use Formax\PublicQuote\Extend\DCCPPDF;
use Magento\Framework\App\Filesystem\DirectoryList;

class Download extends DownloadController
{
    /**
     * @var array
     */
    protected $includeColumns = [
        'requestquote_id' => ['label' => 'Número Cotización'],
        'description' => ['label' => 'Nombre cotización cliente'],
        'subject' => ['label' => 'Categoría producto'],
        'status' => ['label' => 'Estado'],
        'beneficiary' => ['label' => 'Beneficiario'],
        'compliance_guarantee' => ['label' => 'Garantía de cumplimiento', 'format' => 'percent'],
        'amount' => ['label' => 'Presupuesto máximo', 'format' => 'currency'],
        'award_date' => ['label' => 'Fecha en que se adjudicó', 'format' => 'date'],
        'email' => ['label' => 'Correo Electrónico'],
        'deliverable_services' => ['label' => 'Servicios y/o productos entregables', 'format' => 'serialize'],
        'execution_services_term_number' => ['label' => 'Plazo máximo disponible para la ejecución de los servicios (numero)'],
        'execution_services_term_text' => ['label' => 'Plazo máximo disponible para la ejecución de los servicios (texto)'],
        'evaluation_start' => ['label' => 'Inicio Evaluación'],
        'evaluation_term' => ['label' => 'Plazo de evaluación (Días)'],
        'evaluation_end' => ['label' => 'Fecha límite de evaluación', 'format' => 'date'],
        'file' => ['label' => 'Documentos Cotizacion', 'format' => 'serialize'],
        'match_criterias' => ['label' => 'Criterios de desempate'],
        'minimum_offer_required' => ['label' => 'Cumplimiento de requerimientos técnicos mínimos', 'format' => 'serialize'],
        'organization' => ['label' => 'Organismo'],
        'organization_unit' => ['label' => 'Unidad de compra'],
        'publication_start' => ['label' => 'Inicio Publicación'],
        'publication_term' => ['label' => 'Plazo de publicación (Días)'],
        'publication_end' => ['label' => 'Fecha límite publicación', 'format' => 'date'],
        'question_end' => ['label' => 'Fecha límite para hacer preguntas', 'format' => 'date'],
        'recruitment_target' => ['label' => 'Objetivo de contratación'],
        'especific_requirement' => ['label' => 'Requerimientos específicos'],
        'engagement_scope' => ['label' => 'Alcance de la contratación'],
        'make_question_term' => ['label' => 'Plazo para realizar preguntas (Días)'],
        'tic' => ['label' => 'Código comité TIC (Si tiene)'],
        'sku' => ['label' => 'SKU producto'],
        'product_name' => ['label' => 'Nombre producto'],
        'rut' => ['label' => 'Rut proveedor'],
        'seller_name' => ['label' => 'Nombre proveedor'],
        'quote_name' => ['label' => 'Nombre cotización proveedor'],
        'quote_detalis' => ['label' => 'Detalle de la oferta'],
        'evaluation_modality' => ['label' => 'Modalidad de evaluación'],
        'table_requirement' => ['label' => 'Cumplimiento de requerimientos técnicos', 'format' => 'serialize'],
        'duracion_number' => ['label' => 'Duración estimada para servicios', 'format' => 'concat', 'concatWith' => 'duracion_text'],
        'teamwork' => ['label' => 'Equipo de trabajo', 'format' => 'serialize'],
        'offer_price' => ['label' => 'Presupuesto ofertado', 'format' => 'currency'],
        'offer_document' => ['label' => 'Documentos', 'format' => 'serialize'],
        'created_at' => ['label' => 'Fecha de creación oferta técnica', 'format' => 'date'],
        'quote_price' => ['label' => 'Valor cotización', 'format' => 'currency'],
        'approved_seller_quote_id' => ['label' => 'ID Cotización Aprovada'],
        'entity_id' => ['label' => 'ID Cotización Proveedor']
    ];

    /**
     * Create bulk CSV file
     *
     * @param int $quoteId
     * @param array $sellerIds
     * @return \Magento\Framework\App\Response\Http\FileFactory
     */
    public function createFileCsv($quoteId, $sellerIds)
    {
        $collection = $this->getCollection($quoteId, $sellerIds);

        if ($collection->getSize() > 0) {
            /** Create zip file */
            $zip = new \ZipArchive;
            $fileDriver = $this->helperSoftware->getFileDriver();
            $zipName = 'ofertas_tecnicas_' . $quoteId . '.zip';
            $path = 'chilecompra/files/formattributes';
            $pathZip = 'export/' . $zipName;
            $destinationZip = $this->helperSoftware->getPathVar() . $pathZip;
            if (!$zip->open($destinationZip, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)) {
                $this->messageManager->addError(__('Zip Archive Could not create'));
                $this->_redirect('publicquotes/seller/lists', ['id' => $quoteId]);
            }

            /* Open file */
            //$csvName = date('Ymd_Hi');
            $pdfName = date('Ymd_Hi');
            //$csvFilePath = 'export/custom' . $csvName . '.csv';
            $pdfFilePath = 'export/custom' . $pdfName . '.pdf';
            $this->directory->create('export');
            $data = [];
            //$csvStream = $this->directory->openFile($csvFilePath, 'w+');
            $pdfStream = $this->directory->openFile($pdfFilePath, 'w+');
            //$csvStream->lock();
            $pdfStream->lock();

            //foreach ($this->includeColumns as $keyCol => $column) {
            //    if (isset($column['label'])) {
            //        if ($keyCol == 'table_requirement') {
            //            $header[] = $collection->getFirstItem() ? $collection->getFirstItem()->getData('evaluation_modality') : $column['label'];
            //        } else {
            //            $header[] = $column['label'];
            //        }
            //    }
            //}

            /* Write Header */
            //$csvStream->writeCsv($header);

            /* Create PDF File */
            /** @var DCCPPDF $pdf */
            $pdf = $this->_publicQuoteHelper->createPdfFile();
            $firstPageCreated = false;
            foreach ($collection as $item) {
                foreach ($this->includeColumns as $key => $value) {
                    $concatValue = isset($value['concatWith']) && $item->getData($value['concatWith']) ? $item->getData($value['concatWith']) : '';
                    $data[$key] = $this->formatValue($key, $item->getData($key), $concatValue);
                }
                //$csvStream->writeCsv($data);
                if (!$firstPageCreated) {
                    $this->_publicQuoteHelper->addQuoteInformationToPdf($pdf, $data);
                    $firstPageCreated = true;
                }
                $this->_publicQuoteHelper->addPdfSellerInformation($pdf, $data);

                /** Attach documents uploaded by seller to zip */
                $createNewFolder = true;
                $newFolder = 'Documentos Adjuntos - ' . str_replace('.', '', $item->getSellerName());
                if (isset($data['offer_document']) && $item->getData('offer_document')) {
                    $this->addFileToZip($item->getData('offer_document'), $newFolder, $zip);
                    $createNewFolder = false;
                }

                /** Files evaluation modality = 2 */
                if ($this->filterText($item->getData('evaluation_modality')) == 'evaluacion_de_criterios_tecnicos_adicionales') {
                    $newSerialized = $this->helperSoftware->formatSerializeData($item->getData('table_requirement'));
                    $filesSerialized = $this->helperSoftware->getJsonSerialized()->unserialize($newSerialized);
                    if (is_array($filesSerialized)) {
                        $k = 0;
                        foreach ($filesSerialized as $keyLastFile => $itemFile) {
                            if (isset($itemFile['criterio_file']) && $itemFile['criterio_file']) {
                                $fileSellerName = $this->helperSoftware->getPathMedia() . $path . $itemFile['criterio_file'];
                                if ($fileDriver->isExists($fileSellerName)) {
                                    $onlyFileName = explode('/', $itemFile['criterio_file']);
                                    $fileNameWithExtension = explode('.', $onlyFileName[count($onlyFileName)-1]);
                                    $fileSellerNewName = isset($itemFile['criterio_label']) ? $this->filterText($itemFile['criterio_label']) : 'criterio_' . ($k+1);
                                    $fileSellerNewName .= '.'  . $fileNameWithExtension[count($fileNameWithExtension)-1];

                                    /** Create folder with seller RUT if doesn't exist */
                                    if ($createNewFolder) {
                                        $zip->addEmptyDir($newFolder);
                                    }
                                    $zip->addFile($fileSellerName, $newFolder . '/' . $fileSellerNewName);
                                }
                                $k++;
                            }
                        }
                    }
                }
            }
            $pdfStream->write($pdf->getPDFData());

            /** Attach csv and pdf created to zip */
            //$csvFileSellerName = $this->helperSoftware->getPathVar() . $csvFilePath;
            $pdfFileSellerName = $this->helperSoftware->getPathVar() . $pdfFilePath;
            //$csvFileSellerNewName = 'ofertas.csv';
            $pdfFileSellerNewName = 'ofertas.pdf';
            //$zip->addFile($csvFileSellerName, $csvFileSellerNewName);
            $zip->addFile($pdfFileSellerName, $pdfFileSellerNewName);
            $zip->close();

            /** Download zip file */
            $content['type'] = 'filename';
            $content['value'] = $pathZip;
            $content['rm'] = '1';
            return $this->fileFactory->create($zipName, $content, DirectoryList::VAR_DIR, 'application/zip');
        } else {
            $this->messageManager->addNotice(__('No Record found.'));
            return $this->_redirect('publicquotes/seller/lists', ['id' => $quoteId]);
        }
    }

    /**
     * Get quotes collection
     *
     * @param int $quoteId
     * @param array $sellerIds
     * @return array
     */
    public function getCollection($quoteId, $sellerIds)
    {
        $collection = [];
        if ((int)$quoteId > 0 && is_array($sellerIds)) {
            $collection = $this->collectionQuoteFactory->create();
            $resource = $collection->getResource();
            $quoteInfoTable = $resource->getTable('requestforquote_quote_info');
            $quoteTable = $resource->getTable('requestforquote_quote');
            $customerTable = $resource->getTable('customer_entity');
            $productTable = $resource->getTable('catalog_product_entity');
            $productVarcharTable = $resource->getTable('catalog_product_entity_varchar');
            $customerVarcharTable = $resource->getTable('customer_entity_varchar');
            $attrProductName = $this->eavAttribute->getIdByCode('catalog_product', 'name');
            $attrRut = $this->eavAttribute->getIdByCode('customer', 'wkv_dccp_rut');

            $collection->addFieldToFilter('q.entity_id', $quoteId)
                ->addFieldToFilter('edited', 1)
                ->addFieldToFilter('info.seller_id', ['in' => $sellerIds]);
            $collection->getSelect()->join(
                    ['info' => $quoteInfoTable],
                    'main_table.seller_quote_id = info.entity_id',
                    []
                )->joinLeft(
                    ['q' => $quoteTable],
                    'info.quote_id = q.entity_id',
                    [
                        'requestquote_id' => 'q.entity_id',
                        'description',
                        'subject',
                        'evaluation_modality' => new \Zend_Db_Expr('IF(evaluation_modality = 1, "Cumplimiento de requerimientos técnicos mínimos", "Evaluación de criterios técnicos adicionales")'),
                        'status',
                        'beneficiary',
                        'compliance_guarantee',
                        'created_at',
                        'award_date',
                        'deliverable_services',
                        'email',
                        'execution_services_term_number',
                        'execution_services_term_text',
                        'evaluation_start',
                        'evaluation_end',
                        'evaluation_term',
                        'file',
                        'match_criterias',
                        'minimum_offer_required',
                        'organization',
                        'publication_start',
                        'publication_end',
                        'publication_term',
                        'question_end',
                        'make_question_term',
                        'organization',
                        'organization_unit',
                        'recruitment_target',
                        'especific_requirement',
                        'engagement_scope',
                        'tic',
                        'amount',
                        'approved_seller_quote_id'
                    ]
                )->joinLeft(
                    ['c' => $customerTable],
                    'info.seller_id = c.entity_id',
                    ['seller_name' => 'c.firstname']
                )->joinLeft(
                    ['r' => $customerVarcharTable],
                    'c.entity_id = r.entity_id AND r.attribute_id = ' . $attrRut,
                    ['rut' => 'r.value']
                )->joinLeft(
                    ['e' => $productTable],
                    'q.product_id = e.entity_id',
                    ['sku']
                )->joinLeft(
                    ['n' => $productVarcharTable],
                    'e.row_id = n.row_id AND n.store_id = 0 AND n.attribute_id = ' . $attrProductName,
                    ['product_name' => 'n.value']
                );
        }
        
        return $collection;
    }
}