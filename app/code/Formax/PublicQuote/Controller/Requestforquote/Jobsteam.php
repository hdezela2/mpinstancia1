<?php

namespace Formax\PublicQuote\Controller\Requestforquote;

use Formax\PublicQuote\Controller\ListController;

/**
 * Jobsteam class
 */
class Jobsteam extends ListController
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
