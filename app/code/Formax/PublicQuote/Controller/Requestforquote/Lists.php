<?php

namespace Formax\PublicQuote\Controller\Requestforquote;

use Formax\PublicQuote\Controller\ListController;

/**
 * Lists class
 */
class Lists extends ListController
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
