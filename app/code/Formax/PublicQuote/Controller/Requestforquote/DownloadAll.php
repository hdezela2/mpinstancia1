<?php

namespace Formax\PublicQuote\Controller\Requestforquote;

use Formax\PublicQuote\Controller\DownloadController;
use Formax\PublicQuote\Extend\DCCPPDFFactory as DccpPdfFactory;
use Formax\PublicQuote\Helper\Data as PublicQuoteHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory as CollectionQuoteFactory;
use Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory as CollectionMainQuote;
use Magento\Framework\Filesystem;

class DownloadAll extends DownloadController
{
    /**
     * @var array
     */
    protected $includeColumns = [
        'entity_id' => ['label' => 'Número Cotización'],
        'description' => ['label' => 'Nombre cotización'],
        'sku' => ['label' => 'SKU producto'],
        'product_name' => ['label' => 'Nombre producto'],
        'organization' => ['label' => 'Organismo público'],
        'seller_name' => ['label' => 'Proveedor adjudicado'],
        'award_date' => ['label' => 'Fecha en que se adjudicó', 'format' => 'date'],
        'status_name' => ['label' => 'Status'],
        'orden_de_compra' => ['label' => 'Orden de compra']
    ];

    /**
     * @var Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory
     */
    protected $collectionMainQuote;

    /**
     * Initialize dependencies
     *
     * @param Magento\Framework\App\Action\Context $context
     * @param Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param Magento\Framework\Filesystem $filesystem
     * @param Formax\ConfigCmSoftware\Helper\Data $helperSoftware
     * @param Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionQuoteFactory $collectionQuoteFactory
     * @param Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory $collectionMainQuote
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        EavAttribute $eavAttribute,
        Filesystem $filesystem,
        HelperSoftware $helperSoftware,
        CollectionQuoteFactory $collectionQuoteFactory,
        PublicQuoteHelper $publicQuoteHelper,
        DccpPdfFactory $dccpPdf,
        CollectionMainQuote $collectionMainQuote
    ) {
        $this->collectionMainQuote = $collectionMainQuote;

        parent::__construct(
            $context,
            $fileFactory,
            $eavAttribute,
            $filesystem,
            $helperSoftware,
            $collectionQuoteFactory,
            $publicQuoteHelper,
            $dccpPdf
        );
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        return $this->createFileCsv(0, $params);
    }

    /**
     * Create bulk CSV file
     *
     * @param int $quoteId
     * @param array $params
     * @return \Magento\Framework\App\Response\Http\FileFactory
     */
    public function createFileCsv($quoteId, $params)
    {
        $collection = $this->getCollection($quoteId, $params);
        $header = [];

        if ($collection->getSize() > 0) {
            $csvName = 'cotizaciones_' . date('Ymd_Hi') . '.csv';
            $filepath = 'export/custom' . $csvName;
            $this->directory->create('export');
            $data = [];
            $stream = $this->directory->openFile($filepath, 'w+');
            $stream->lock();

            foreach ($this->includeColumns as $keyCol => $column) {
                if (isset($column['label'])) {
                    $header[] = $column['label'];
                }
            }

            /* Write Header */
            $stream->writeCsv($header);

            foreach ($collection as $item) {
                foreach ($this->includeColumns as $key => $value) {
                    $concatValue = isset($value['concatWith']) && $item->getData($value['concatWith']) ? $item->getData($value['concatWith']) : '';
                    $data[$key] = $this->formatValue($key, $item->getData($key), $concatValue);
                }
                $stream->writeCsv($data);
            }

            $content['type'] = 'filename';
            $content['value'] = $filepath;
            $content['rm'] = '1';
            return $this->fileFactory->create($csvName, $content, DirectoryList::VAR_DIR);
        } else {
            $this->messageManager->addNotice(__('No Record found.'));
            return $this->_redirect('publicquotes/requestforquote/lists');
        }
    }

    /**
     * Get quotes collection
     *
     * @param int $quoteId
     * @param array $params
     * @return array
     */
    public function getCollection($quoteId, $params)
    {
        $collection = $this->collectionMainQuote->create();
        $resource = $collection->getResource();
        $customerTable = $resource->getTable('customer_entity');
        $productTable = $resource->getTable('catalog_product_entity');
        $quoteInfoTable = $resource->getTable('requestforquote_quote_info');
        $quoteConversationTable = $resource->getTable('requestforquote_quote_conversation');
        $productVarcharTable = $resource->getTable('catalog_product_entity_varchar');
        $salesOrderTable = $resource->getTable('sales_order');
        $productWebsiteTable = $resource->getTable('catalog_product_website');
        $attrProductName = $this->eavAttribute->getIdByCode('catalog_product', 'name');
        $websiteId = $this->helperSoftware->getStoreManager()->getWebsite()->getId();

        if (isset($params['quote_id']) && $params['quote_id'] != '') {
            $filterQuoteId = trim($params['quote_id']);
            $collection->addFieldToFilter('main_table.entity_id', $filterQuoteId);
        }
        if (isset($params['quote_name']) && $params['quote_name'] != '') {
            $filterQuoteName = trim($params['quote_name']);
            $collection->addFieldToFilter('main_table.description', ['like' => '%' . $filterQuoteName . '%']);
        }
        if (isset($params['oc']) && $params['oc'] != '') {
            $filterOc = trim($params['oc']);
            $collection->addFieldToFilter('orden_de_compra', $filterOc);
        }
        if (isset($params['organization_name']) && $params['organization_name'] != '') {
            $filterOrganization = trim($params['organization_name']);
            $collection->addFieldToFilter('organization', ['like' => '%' . $filterOrganization . '%']);
        }
        if (isset($params['status']) && $params['status'] != '') {
            $filterStatus = trim($params['status']);
            $collection->addFieldToFilter('main_table.status', $filterStatus);
        }
        if (isset($params['service_type']) && $params['service_type'] != '') {
            $filterServiceType = trim($params['service_type']);
            $collection->addFieldToFilter('main_table.subject', $filterServiceType);
        }
        if (isset($params['awarded_date']) && $params['awarded_date'] != '') {
            $filterAwardedDateIni = trim($params['awarded_date']) . ' 00:00:00';
            $filterAwardedDateEnd = trim($params['awarded_date']) . ' 23:59:59';
            $collection->addFieldToFilter('main_table.award_date', ['gteq' => $filterAwardedDateIni])
                ->addFieldToFilter('main_table.award_date', ['lteq' => $filterAwardedDateEnd]);
        }

        $collection->getSelect()->columns(
                ['status_name' => new \Zend_Db_Expr('
                CASE
                    WHEN main_table.status = 1 THEN "Publicada"
                    WHEN main_table.status = 2 THEN "Finalizada"
                    WHEN main_table.status = 3 THEN "Evaluación"
                    WHEN main_table.status = 4 THEN "Proveedor"
                    WHEN main_table.status = 5 THEN "Desierta"
                    WHEN main_table.status = 6 THEN "Guardada"
                    WHEN main_table.status = 7 THEN "Publicación Cerrada"
                    WHEN main_table.status = 8 THEN "Evaluación Cerrada"
                    WHEN main_table.status = 9 THEN "Desestimada"
                    ELSE "Pendiente"
                END
                ')]
            )->join(
                ['pw' => $productWebsiteTable],
                'main_table.product_id = pw.product_id AND pw.website_id = ' . $websiteId,
                []
            )->joinLeft(
                ['cv' => $quoteConversationTable],
                'main_table.approved_seller_quote_id = cv.entity_id AND cv.edited = 1',
                []
            )->joinLeft(
                ['info' => $quoteInfoTable],
                'cv.seller_quote_id = info.entity_id',
                []
            )->joinLeft(
                ['c' => $customerTable],
                'info.seller_id = c.entity_id',
                ['seller_name' => new \Zend_Db_Expr('CONCAT(c.firstname, " ", c.lastname)')]
            )->joinLeft(
                ['e' => $productTable],
                'main_table.product_id = e.entity_id',
                ['sku']
            )->joinLeft(
                ['n' => $productVarcharTable],
                'e.row_id = n.row_id AND n.store_id = 0 AND n.attribute_id = ' . $attrProductName,
                ['product_name' => 'n.value']
            )->joinLeft(
                ['oc' => $salesOrderTable],
                'main_table.entity_id = oc.requestforquote_info',
                ['orden_de_compra']
            )->group('main_table.entity_id');

        return $collection;
    }
}
