<?php

namespace Formax\PublicQuote\Controller\Requestforquote;

use Formax\PublicQuote\Controller\DownloadController;
use Formax\PublicQuote\Extend\DCCPPDF;
use Formax\PublicQuote\Extend\DCCPPDFFactory as DccpPdfFactory;
use Formax\PublicQuote\Helper\Data as PublicQuoteHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory as CollectionQuoteFactory;
use Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory as CollectionMainQuote;
use Magento\Framework\Filesystem;

class Download extends DownloadController
{
    /**
     * @var array
     */
    protected $includeColumns = [
        'entity_id' => ['label' => 'Número Cotización'],
        'description' => ['label' => 'Nombre cotización'],
        'subject' => ['label' => 'Categoría producto'],
        'status' => ['label' => 'Estado'],
        'execution_services_term_number' => ['label' => 'Plazo máximo disponible para la ejecución de los servicios (numero)'],
        'execution_services_term_text' => ['label' => 'Plazo máximo disponible para la ejecución de los servicios (texto)'],
        'evaluation_start' => ['label' => 'Inicio Evaluación'],
        'file' => ['label' => 'Documentos Cotizacion', 'format' => 'serialize'],
        'organization' => ['label' => 'Organismo'],
        'organization_unit' => ['label' => 'Unidad de compra'],
        'publication_start' => ['label' => 'Inicio Publicación'],
        'sku' => ['label' => 'SKU producto'],
        'product_name' => ['label' => 'Nombre producto'],
        'customer_name' => ['label' => 'Comprador'],
        'tic' => ['label' => 'TIC'],
        'publication_term' => ['label' => 'Plazo de publicación (Días)'],
        'evaluation_term' => ['label' => 'Plazo de evaluación (Días)'],
        'make_question_term' => ['label' => 'Plazo para realizar preguntas (Días)'],
        'email' => ['label' => 'Email'],
        'compliance_guarantee' => ['label' => 'Garantía de cumplimiento', 'format' => 'percent'],
        'beneficiary' => ['label' => 'Beneficiario'],
        'recruitment_target' => ['label' => 'Objetivo de contratación'],
        'engagement_scope' => ['label' => 'Alcance de la contratación'],
        'especific_requirement' => ['label' => 'Requerimientos específicos'],
        'deliverable_services' => ['label' => 'Servicios y/o productos entregables', 'format' => 'serialize'],
        'amount' => ['label' => 'Presupuesto máximo', 'format' => 'currency'],
        'evaluation_modality' => ['label' => 'Modalidad de evaluación'],
        'minimum_offer_required' => ['label' => 'Cumplimiento de requerimientos técnicos mínimos', 'format' => 'serialize'],
        'table_evaluation_criteria' => ['label' => 'Evaluación de criterios técnicos adicionales', 'format' => 'serialize'],
        'score' => ['label' => 'Puntaje mínimo oferta', 'format' => 'percent'],
        'match_criterias' => ['label' => 'Criterios de desempate'],
        'created_at' => ['label' => 'Fecha de creación cotización', 'format' => 'date'],
        'publication_end' => ['label' => 'Fecha límite publicación', 'format' => 'date'],
        'evaluation_end' => ['label' => 'Fecha límite de evaluación', 'format' => 'date'],
        'question_end' => ['label' => 'Fecha límite para hacer preguntas', 'format' => 'date'],
        'award_date' => ['label' => 'Fecha en que se adjudicó', 'format' => 'date']
    ];

    /**
     * @var Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory
     */
    protected $collectionMainQuote;

    /**
     * Initialize dependencies
     *
     * @param Magento\Framework\App\Action\Context $context
     * @param Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param Magento\Framework\Filesystem $filesystem
     * @param Formax\ConfigCmSoftware\Helper\Data $helperSoftware
     * @param Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionQuoteFactory $collectionQuoteFactory
     * @param Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory $collectionMainQuote
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        EavAttribute $eavAttribute,
        Filesystem $filesystem,
        HelperSoftware $helperSoftware,
        CollectionQuoteFactory $collectionQuoteFactory,
        PublicQuoteHelper $publicQuoteHelper,
        DccpPdfFactory $dccpPdf,
        CollectionMainQuote $collectionMainQuote
    ) {
        $this->collectionMainQuote = $collectionMainQuote;

        parent::__construct(
            $context,
            $fileFactory,
            $eavAttribute,
            $filesystem,
            $helperSoftware,
            $collectionQuoteFactory,
            $publicQuoteHelper,
            $dccpPdf
        );
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $quoteId = (int)$this->getRequest()->getParam('quote_id', 0);
        if ($quoteId > 0) {
            return $this->createFileCsv($quoteId, 0);
        } else {
            $this->messageManager->addNotice(__('And error occured trying download all files.'));
            return $this->_redirect('publicquotes/requestforquote/view', ['id' => $quoteId]);
        }
    }

    /**
     * Create bulk CSV file
     *
     * @param int $quoteId
     * @param array $sellerIds
     * @return \Magento\Framework\App\Response\Http\FileFactory
     */
    public function createFileCsv($quoteId, $sellerIds)
    {
        $collection = $this->getCollection($quoteId, $sellerIds);

        if ($collection->getSize() > 0) {
            /** Create zip file */
            $zip = new \ZipArchive;
            $fileDriver = $this->helperSoftware->getFileDriver();
            $zipName = 'ofertas_tecnicas_' . $quoteId . '.zip';
            $path = 'chilecompra/files/formattributes';
            $pathZip = 'export/' . $zipName;
            $destinationZip = $this->helperSoftware->getPathVar() . $pathZip;
            if (!$zip->open($destinationZip, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)) {
                $this->messageManager->addError(__('Zip Archive Could not create'));
                $this->_redirect('publicquotes/requestforquote/view', ['id' => $quoteId]);
            }

            /* Open file */
            //$csvName = date('Ymd_Hi');
            $pdfName = date('Ymd_Hi');
            //$csvFilePath = 'export/custom' . $csvName . '.csv';
            $pdfFilePath = 'export/custom' . $pdfName . '.pdf';
            $this->directory->create('export');
            $data = [];
            //$csvStream = $this->directory->openFile($csvFilePath, 'w+');
            $pdfStream = $this->directory->openFile($pdfFilePath, 'w+');
            //$csvStream->lock();
            $pdfStream->lock();

            //foreach ($this->includeColumns as $keyCol => $column) {
            //    if (isset($column['label'])) {
            //        $header[] = $column['label'];
            //    }
            //}

            /* Write Header */
            //$csvStream->writeCsv($header);

            /* Create PDF File */
            /** @var DCCPPDF $pdf */
            $pdf = $this->_publicQuoteHelper->createPdfFile();
            $firstPageCreated = false;

            foreach ($collection as $item) {
                foreach ($this->includeColumns as $key => $value) {
                    $concatValue = isset($value['concatWith']) && $item->getData($value['concatWith']) ? $item->getData($value['concatWith']) : '';
                    $data[$key] = $this->formatValue($key, $item->getData($key), $concatValue);
                }
                //$csvStream->writeCsv($data);
                if (!$firstPageCreated) {
                    $this->_publicQuoteHelper->addQuoteInformationToPdf($pdf, $data);
                    $firstPageCreated = true;
                }

                /** Attach documents uploaded by customer to zip */
                if ($item->getData('file')) {
                    $this->addFileToZip($item->getData('file'), 'documentos_adjuntos', $zip);
                }
            }
            $pdfStream->write($pdf->getPDFData());

            /** Attach csv and pdf created to zip */
            //$csvFileSellerName = $this->helperSoftware->getPathVar() . $csvFilePath;
            $pdfFileSellerName = $this->helperSoftware->getPathVar() . $pdfFilePath;
            $csvFileSellerNewName = 'ofertas.csv';
            $pdfFileSellerNewName = 'ofertas.pdf';
            //$zip->addFile($csvFileSellerName, $csvFileSellerNewName);
            $zip->addFile($pdfFileSellerName, $pdfFileSellerNewName);
            $zip->close();

            /** Download zip file */
            $content['type'] = 'filename';
            $content['value'] = $pathZip;
            $content['rm'] = '1';
            return $this->fileFactory->create($zipName, $content, DirectoryList::VAR_DIR);
        } else {
            $this->messageManager->addNotice(__('No Record found.'));
            return $this->_redirect('publicquotes/requestforquote/view', ['id' => $quoteId]);
        }
    }

    /**
     * Get quotes collection
     *
     * @param int $quoteId
     * @param array $sellerIds
     * @return array
     */
    public function getCollection($quoteId, $sellerIds)
    {
        $collection = [];
        if ((int)$quoteId > 0) {
            $collection = $this->collectionMainQuote->create();
            $resource = $collection->getResource();
            $customerTable = $resource->getTable('customer_entity');
            $productTable = $resource->getTable('catalog_product_entity');
            $productVarcharTable = $resource->getTable('catalog_product_entity_varchar');
            $attrProductName = $this->eavAttribute->getIdByCode('catalog_product', 'name');

            $collection->addFieldToFilter('main_table.entity_id', $quoteId);
            $collection->getSelect()->columns(
                    ['evaluation_modality' => new \Zend_Db_Expr('IF(evaluation_modality = 1, "Cumplimiento de requerimientos técnicos mínimos", "Evaluación de criterios técnicos adicionales")')]
                )->joinLeft(
                    ['c' => $customerTable],
                    'main_table.customer_id = c.entity_id',
                    ['customer_name' => new \Zend_Db_Expr('CONCAT(c.firstname, " ", c.lastname)')]
                )->joinLeft(
                    ['e' => $productTable],
                    'main_table.product_id = e.entity_id',
                    ['sku']
                )->joinLeft(
                    ['n' => $productVarcharTable],
                    'e.row_id = n.row_id AND n.store_id = 0 AND n.attribute_id = ' . $attrProductName,
                    ['product_name' => 'n.value']
                );
        }

        return $collection;
    }
}
