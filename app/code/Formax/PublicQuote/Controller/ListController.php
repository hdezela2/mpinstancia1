<?php

namespace Formax\PublicQuote\Controller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * ListController class
 */
abstract class ListController extends Action
{
    /**
     * @var Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Initialize constructor
     * 
     * @param Magento\Framework\App\Action\Context $context
     * @param Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $quoteId = (int)$this->getRequest()->getParam('id', 0);
        if ($quoteId == 0) {
            $this->_forward('noRoute');
            return false;
        }

        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
