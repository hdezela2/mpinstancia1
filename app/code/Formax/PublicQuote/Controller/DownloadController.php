<?php

namespace Formax\PublicQuote\Controller;

use Formax\PublicQuote\Helper\Data as PublicQuoteHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Formax\PublicQuote\Extend\DCCPPDFFactory as DccpPdfFactory;
use Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory as CollectionQuoteFactory;
use Magento\Framework\Filesystem;

class DownloadController extends Action
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    /**
     * @var Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directory;

    /**
     * @var Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $eavAttribute;

    /**
     * @var Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionQuoteFactory
     */
    protected $collectionQuoteFactory;

    /**
     * @var Formax\ConfigCmSoftware\Helper\Data
     */
    protected $helperSoftware;

    /**
     * @var array
     */
    protected $includeColumns = [];

    /** @var DccpPdfFactory */
    protected $dccpPdf;

    /** @var \Formax\PublicQuote\Helper\Data */
    protected $_publicQuoteHelper;

    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        EavAttribute $eavAttribute,
        Filesystem $filesystem,
        HelperSoftware $helperSoftware,
        CollectionQuoteFactory $collectionQuoteFactory,
        PublicQuoteHelper $publicQuoteHelper,
        DccpPdfFactory $dccpPdf
    ) {
        $this->fileFactory = $fileFactory;
        $this->eavAttribute = $eavAttribute;
        $this->helperSoftware = $helperSoftware;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->collectionQuoteFactory = $collectionQuoteFactory;
        $this->dccpPdf = $dccpPdf;
        $this->_publicQuoteHelper = $publicQuoteHelper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $post = $this->getRequest()->getPost();
        if (isset($post) && (count($post) > 0) && isset($post['bulkaction'])) {
            $quoteId = isset($post['quote_id'][0]) ? $post['quote_id'][0] : 0;
            $sellerIds = [];

            if (isset($post['seller_id']) && is_array($post['seller_id'])) {
                foreach ($post['seller_id'] as $sellerId) {
                    $sellerIds[] = $sellerId;
                }
            }
        } else {
            $sellerIds[] = (int)$this->getRequest()->getParam('seller_id', 0);
            $quoteId = (int)$this->getRequest()->getParam('quote_id', 0);
        }

        if (count($sellerIds) == 0) {
            $this->messageManager->addNotice(__('You must select a seller.'));
            return $this->_redirect('publicquotes/seller/lists', ['id' => $quoteId]);
        } else {
            return $this->createFileCsv($quoteId, $sellerIds);
        }
    }

    /**
     * Create bulk CSV file
     *
     * @param int $quoteId
     * @param array $sellerIds
     * @return \Magento\Framework\App\Response\Http\FileFactory
     */
    public function createFileCsv($quoteId, $sellerIds)
    {
        return false;
    }

    /**
     * Get quotes collection
     *
     * @param int $quoteId
     * @param array $sellerIds
     * @return array
     */
    public function getCollection($quoteId, $sellerIds)
    {
        return false;
    }

    /**
     * Get value formated
     *
     * @param string $columnKey
     * @param string $value
     * @param string $concatWith [optional]
     * @return string
     */
    public function formatValue($columnKey, $value, $concatWith = '')
    {
        $formatedValue = $value;
        if (isset($this->includeColumns[$columnKey]['format'])) {
            switch ($this->includeColumns[$columnKey]['format']) {
                case 'date':
                    $formatedValue = $this->helperSoftware->formatDateTime($value);
                    break;
                case 'datetime':
                    $formatedValue = $this->helperSoftware->formatDateToDateTime($value);
                    break;
                case 'currency':
                    $formatedValue = $this->helperSoftware->currencyFormat($value);
                    break;
                case 'number':
                    $formatedValue = $this->helperSoftware->numberFormat($value);
                    break;
                case 'percent':
                    $formatedValue = number_format($value, 1, '.', ',');
                    break;
                case 'concat':
                    if (isset($this->includeColumns[$columnKey]['concatWith'])
                        && $this->includeColumns[$columnKey]['concatWith'] == 'duracion_text') {
                        $formatedValue = $value . ' ' . $this->helperSoftware->getTextServiceTerm($concatWith);
                    } else {
                        $formatedValue = $value . ' ' . $concatWith;
                    }
                    break;
                case 'link':
                    $path = 'chilecompra/files/formattributes';
                    $formatedValue = '';
                    if ($value) {
                        $filename = $this->helperSoftware->getPathMedia() . $path . $value;
                        if ($this->helperSoftware->getFileDriver()->isExists($filename)) {
                            $formatedValue = $this->helperSoftware->getUrlMedia() . $path .  $value;
                        }
                    }
                    break;
                case 'serialize':
                    if ($value) {
                        $columnKeySend = $columnKey == 'table_evaluation_criteria' ? $columnKey : ($columnKey == 'table_requirement'? $columnKey: null);
                        $formatedValue = $this->helperSoftware->formatSerializeData($value,$columnKeySend);
                    }
                    break;
                default:
                    break;
            }
        }

        return $formatedValue;
    }

    /**
     * Get value of text filter only by letter
     *
     * @return string
     */
    protected function filterText($text)
    {
        if ($text) {
            return str_replace(['á', 'é', 'í', 'ó', 'ú', ' '], ['a', 'e', 'i', 'o', 'u', '_'], strtolower(trim($text)));
        }

        return '';
    }

    /**
     *
     * @param string $columnData
     * @param string $fileType
     * @param \ZipArchive $zip
     * @return void
     */
    protected function addFileToZip($columnData, $filteType, $zip)
    {
        if ($columnData !== null && !empty($columnData) && !empty($filteType) && $zip) {
            $newSerialized = $this->helperSoftware->formatSerializeData($columnData);
            $filesSerialized = $this->helperSoftware->getJsonSerialized()->unserialize($newSerialized);
            $fileDriver = $this->helperSoftware->getFileDriver();
            $path = 'chilecompra/files/formattributes';
            if (is_array($filesSerialized)) {
                foreach ($filesSerialized as $itemFile) {
                    $fileName = $this->helperSoftware->getPathMedia() . $path . $itemFile;
                    if ($fileDriver->isExists($fileName)) {
                        /** Create folder with file type */
                        $onlyFileName = explode('/', $itemFile);
                        $fileNewName = $onlyFileName[count($onlyFileName)-1];
                        $zip->addEmptyDir($filteType);
                        $zip->addFile($fileName, $filteType . '/' . $fileNewName);
                    }
                }
            }
        }
    }
}
