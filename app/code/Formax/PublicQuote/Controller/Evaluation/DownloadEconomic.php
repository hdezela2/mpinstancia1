<?php

namespace Formax\PublicQuote\Controller\Evaluation;

use Formax\PublicQuote\Extend\DCCPPDFFactory as DccpPdfFactory;
use Formax\PublicQuote\Helper\Data as PublicQuoteHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Webkul\Requestforquote\Model\ResourceModel\Conversation\CollectionFactory as CollectionQuoteFactory;
use Formax\EvaluateRequestforquote\Model\ResourceModel\EconomicEvaluation\CollectionFactory as CollectionEconomicFactory;
use Formax\PublicQuote\Controller\DownloadController;
use Magento\Framework\Filesystem;

class DownloadEconomic extends DownloadController
{
    /**
     * @var Formax\EvaluateRequestforquote\Model\ResourceModel\EconomicEvaluation\CollectionFactory
     */
    protected $collectionEconomicFactory;

    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        EavAttribute $eavAttribute,
        Filesystem $filesystem,
        HelperSoftware $helperSoftware,
        CollectionQuoteFactory $collectionQuoteFactory,
        PublicQuoteHelper $publicQuoteHelper,
        DccpPdfFactory $dccpPdf,
        CollectionEconomicFactory $collectionEconomicFactory
    ) {
        $this->collectionEconomicFactory = $collectionEconomicFactory;

        parent::__construct(
            $context,
            $fileFactory,
            $eavAttribute,
            $filesystem,
            $helperSoftware,
            $collectionQuoteFactory,
            $publicQuoteHelper,
            $dccpPdf
        );
    }

    /**
     * @return false|FileFactory|\Magento\Framework\App\ResponseInterface|\Magento\Framework\View\Result\Page|void
     */
    public function execute()
    {
        $post = $this->getRequest()->getPost();
        if (isset($post['bulkaction'])) {
            $quoteId = isset($post['seller_quote_id']) ? $post['seller_quote_id'] : 0;

            if ($quoteId > 0) {
                return $this->createFileCsv($quoteId, 0);
            } else {
                $this->messageManager->addNotice(__('And error occured trying download all files.'));
                return $this->_redirect('publicquotes/seller/lists', ['id' => $quoteId]);
            }
        }
    }

    /**
     * Create bulk CSV file
     *
     * @param int $quoteId
     * @param int $sellerId
     * @return \Magento\Framework\App\Response\Http\FileFactory
     */
    public function createFileCsv($quoteId, $sellerId)
    {
        $collection = $this->getCollection($quoteId, $sellerId);

        if ($collection->getSize() > 0) {
            /** Create zip file */
            $zip = new \ZipArchive;
            $fileDriver = $this->helperSoftware->getFileDriver();
            $zipName = 'evaluacion_economica_' . date('Ymd_Hi') . '.zip';
            $pathZip = 'export/' . $zipName;
            $destinationZip = $this->helperSoftware->getPathVar() . $pathZip;
            if (!$zip->open($destinationZip, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)) {
                $this->messageManager->addError(_('Zip Archive Could not create'));
                $this->_redirect('publicquotes/requestforquote/view', ['id' => $quoteId]);
            }

            foreach ($collection as $item) {
                /** Attach documents uploaded by customer to zip */
                $this->addFileToZip($item->getData('evaluation_report_documents'), 'acta_evaluacion', $zip);
                $this->addFileToZip($item->getData('admon_act_documents'), 'acto_admon_que_aprueba', $zip);
                $this->addFileToZip($item->getData('complementary_annexes_document'), 'anexos_complementarios', $zip);
            }

            $zip->close();

            /** Download zip file */
            $content['type'] = 'filename';
            $content['value'] = $pathZip;
            $content['rm'] = '1';
            return $this->fileFactory->create($zipName, $content, DirectoryList::VAR_DIR);
        } else {
            $this->messageManager->addNotice(__('No Record found.'));
            return $this->_redirect('publicquotes/requestforquote/view', ['id' => $quoteId]);
        }
    }

    /**
     * Get quotes collection
     *
     * @param int $quoteId
     * @param int $sellerId
     * @return array
     */
    public function getCollection($quoteId, $sellerId)
    {
        $collection = [];
        if ((int)$quoteId > 0) {
            $collection = $this->collectionEconomicFactory->create();
            $collection->addFieldToFilter('seller_quote_id', $quoteId)
                ->setPageSize(1);
        }

        return $collection;
    }
}
