<?php
namespace Formax\PublicQuote\Extend;

use Magento\Theme\Block\Html\Header\Logo;

class DCCPPDF extends \TCPDF
{
    /** @var Logo */
    protected $logo;

    public function __construct(
        Logo $logo,
        $orientation = 'P',
        $unit = 'mm',
        $format = 'A4',
        $unicode = true,
        $encoding = 'UTF-8',
        $diskcache = false,
        $pdfa = false
    ) {
        $this->logo = $logo;
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }

    //Page header
    public function Header() {
        //$logoImage = $this->logo->getLogoSrc();
        //$logoImage = str_replace('svg', 'png', $logoImage);

        if ($this->header_xobjid === false) {
            // start a new XObject Template
            $this->header_xobjid = $this->startTemplate($this->w, $this->tMargin);
            $headerfont = ['helvetica', '', 10];
            $headerdata = [
                //'logo' => $logoImage,
                'logo_width' => '30',
                'title' => 'CONVENIO MARCO DESARROLLO DE SOFTWARE',
                'string' => 'Cotizaciones ofertadas',
                'text_color' => [135,152,173],
                'line_color' => [0,0,0]
            ] ;
            $y = 25;

            $this->x = $this->original_lMargin;

            //$this->Image($headerdata['logo'], '', '7.5', '35');
            $imgy = $this->getImageRBY();

            // set starting margin for text data cell
            $header_x = $this->original_lMargin + ($headerdata['logo_width'] * 1.1);
            $cw = $this->w - $this->original_lMargin - $this->original_rMargin - ($headerdata['logo_width'] * 1.1);
            $this->SetTextColorArray([135,152,173]);

            // header title
            $this->SetFont($headerfont[0], 'B', $headerfont[2] + 2);
            $this->SetX($header_x);
            //$this->Cell($cw, 14, $headerdata['title'], 0, 1, 'R', 0, '', 0);
            $this->Cell($cw,12, $headerdata['title'],0,1,'R', 0,'',0,false,'T', 'B');

            // header string
            $this->SetFont($headerfont[0], 'B', $headerfont[2] + 3);
            $this->SetX($header_x);
            $this->SetTextColorArray([0,0,0]);
            $this->MultiCell($cw, 14, $headerdata['string'], 0, 'R'); // 0, 1, '', '', true, 0, false, true, 0, 'T', false);

            // print an ending header line
            $this->SetLineStyle(array('width' => 0.85 / $this->k, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => [135,152,173]));
            $this->SetY((2.835 / $this->k) + max($imgy, $y));


            $this->SetX($this->original_lMargin);

            $this->Cell(($this->w - $this->original_lMargin - $this->original_rMargin), 0, '', 'T', 0, 'C');
            $this->endTemplate();
        }
        // print header template
        $x = 0;
        $dx = 0;
        if (!$this->header_xobj_autoreset AND $this->booklet AND (($this->page % 2) == 0)) {
            // adjust margins for booklet mode
            $dx = ($this->original_lMargin - $this->original_rMargin);
        }
        if ($this->rtl) {
            $x = $this->w + $dx;
        } else {
            $x = 0 + $dx;
        }
        $this->printTemplate($this->header_xobjid, $x, 0, 0, 0, '', '', false);
        if ($this->header_xobj_autoreset) {
            // reset header xobject template at each page
            $this->header_xobjid = false;
        }
    }
}