<?php

namespace Formax\PublicQuote\Helper;

use Formax\ConfigCmSoftware\Helper\Data as ConfigCmSoftwareHelper;
use Formax\PublicQuote\Extend\DCCPPDF;
use Formax\QuotesSupplier\Helper\Data as QuoteSupplierHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Theme\Block\Html\Header\Logo;
use Webkul\Requestforquote\Model\QuoteFactory;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /** @var QuoteFactory */
    protected $quoteFactory;

    /** @var ResourceConnection */
    protected $resourceConnection;

    /** @var Logo */
    protected $logo;

    /** @var ConfigCmSoftwareHelper */
    protected $cmSoftwareHelper;

    /** @var QuoteSupplierHelper */
    protected $quoteSupplierHelper;

    public function __construct(
        Context $context,
        QuoteFactory $quote,
        ResourceConnection $resourceConnection,
        Logo $logo,
        ConfigCmSoftwareHelper $configCmSoftwareHelper,
        QuoteSupplierHelper $quoteSupplierHelper
    )
    {
        parent::__construct($context);
        $this->quoteFactory = $quote;
        $this->resourceConnection = $resourceConnection;
        $this->logo = $logo;
        $this->cmSoftwareHelper = $configCmSoftwareHelper;
        $this->quoteSupplierHelper = $quoteSupplierHelper;
    }

    /**
     * @return DCCPPDF
     */
    public function createPdfFile()
    {
        // create new PDF document
        $pdf = new DCCPPDF($this->logo, 'P', 'mm', 'A4', true, 'UTF-8', false, false);

        // set document information
        $pdf->SetCreator('TCPDF');
        $pdf->SetAuthor('DCCP');
        $pdf->SetTitle('Cotizaciones Ofertadas');
        $pdf->SetSubject('CONVENIO MARCO DESARROLLO DE SOFTWARE');
        $pdf->SetKeywords('Cotizaciones, Ofertadas, DCCP, Convenio, Marco, Software');

        // set header and footer fonts
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins('15', '27', '15');
        $pdf->SetHeaderMargin('5');
        $pdf->SetFooterMargin('10');

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        //$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        $pdf->SetFont('helvetica', '', 12);

        return $pdf;
    }

    public function addQuoteInformationToPdf($pdf, $data)
    {
        $quoteId = $data["requestquote_id"] ?? $data['entity_id'];
        $status = $this->quoteSupplierHelper->getStatusLabel($data['status']);
        // add a page
        $pdf->AddPage();
        $html = '<div>';

        $html .= '<h2>Cotización ID ' . $quoteId . '</h2>';
        $html .= '<b>Organismo: </b> ' . $data['organization'] . '<br>';
        $organizationUnit = $this->cmSoftwareHelper->getPurchaseUnitByCode($data['organization_unit']);
        if ($organizationUnit) {
            $html .= '<b>Unidad de compra: </b> ' . $organizationUnit . '<br>';
        }
        //else {
        //    $html .= '<b>Unidad de compra: </b> ' . $data['organization'] . '<br>';
        //}
        $html .= '<b>Ficha servicio: </b>' . $data["subject"] . ' ID ' . $data["sku"] . '<br>';
        $html .= '<b>Estado: </b>' . $status . '<br>';
        $html .= '<b>Inicio de publicación: </b>' . $data["created_at"] . '<br>';
        $html .= '<b>Fin Publicación: </b>' . $data["publication_end"] . '<br>';
        $html .= '<b>Inicio Evaluación: </b>' . $this->formatStartDateTime($data["evaluation_start"]) . '<br>';
        $html .= '<b>Fin Evaluación: </b>' . $data["evaluation_end"];
        $data['award_date'] ? '<b>Fecha Adjudicación: </b>' . $this->formatDateTime($data["award_date"]) . '<br>' : '';

        $html .= '<h3>Antecedentes Generales</h3>';
        $html .= '<b>Nombre de la Cotización: </b>' . $data["description"] . '<br>';
        $data['tic'] ? $html .= '<b>Código comité TIC: </b>' . $data['tic'] . '<br>' : '';
        $html .= '<b>Plazo de Publicación: </b>' . $data["publication_term"] . ' días hábiles' . '<br>';
        $html .= '<b>Plazo de Evaluación: </b>' . $data["evaluation_term"] . ' días hábiles' . '<br>';
        $html .= '<b>Plazo para realizar preguntas: </b>' . $data["make_question_term"] . ' días hábiles' . '<br>';
        $html .= '<b>Correo electrónico: </b>' . $data['email'] . '<br>';
        $html .= '<b>Garantía de fiel cumplimento de acuerdo complementario: </b>' . number_format($data['compliance_guarantee'], 1, ",", ".") . '%<br>';
        $html .= '<b>Beneficiario: </b>' . $data['beneficiary'];

        $html .= '<h3>Descripción de los requerimientos</h3>';
        $html .= '<h4>Objeto contratación: </h4>' . $this->cleanSymbolsOnPDF($data['recruitment_target']);
        $html .= '<h4>Requerimientos específicos: </h4>' . $this->cleanSymbolsOnPDF($data['especific_requirement']);
        $html .= '<h4>Alcance de la contratación: </h4>' . $this->cleanSymbolsOnPDF($data['engagement_scope']);

        /***************************************************
         * //Sección de SERVICIOS Y/O PRODUCTOS ENTREGABLES
         ****************************************************/

        $deliverableServicesMultiArray = json_decode($data['deliverable_services'], true);
        if (is_array($deliverableServicesMultiArray)) {
            $html .= '<h3>Servicios y/o productos entregrables</h3>';
            $html .= '
                <style type="text/css">
                    .tg {border-collapse:collapse;border-color:#9ABAD9;border-spacing:0;border-style:solid;border-width:1px;margin:0px auto;}
                    .tg td{background-color:#EBF5FF;border-color:#9ABAD9;border-style:solid;border-width:0px;color:#444;font-family:Arial, sans-serif;font-size:10px;overflow:hidden;padding:10px 5px;word-break:normal;}
                    .tg th{background-color:#409cff;border-color:#9ABAD9;border-style:solid;border-width:0px;color:#fff;font-family:Arial, sans-serif;font-size:10px;font-weight:bold;overflow:hidden;padding:10px 5px;word-break:normal;}
                    .tg .tg-0lax{text-align:left;vertical-align:top}
                </style>
                <table class="tg"><thead>
                    <tr>
                        <th class="tg-0lax">Nombre del Servicio</th>
                        <th class="tg-0lax">Detalle</th>
                    </tr>
                </thead>
                <tbody>';

            foreach ($deliverableServicesMultiArray as $deliveryService) {
                $html .= '<tr>';
                key_exists('service_name', $deliveryService) ? $html .= '<td class="tg-0lax">' . $deliveryService['service_name'] . '</td>' : '';
                key_exists('detail', $deliveryService) ? $html .= '<td class="tg-0lax">' . $deliveryService['detail'] . '</td>' : '';
                $html .= '</tr>';
            }
            $html .= '</tbody></table><br><br>';

        }


        //PLAZO MÁXIMO DE EJECUCIÓN

        $html .= '<b>Plazo máximo disponible para la ejecución de los servicios: </b>'
            . $data['execution_services_term_number'] . ' '
            . $this->cmSoftwareHelper->getTextServiceTerm($data['execution_services_term_text']) . '<br>';

        //PRESUPUESTO MÁXIMO

        $html .= '<b>Presupuesto máximo: </b>' . $data['amount'];

        //MODALIDAD

        $html .= '<h3>Modalidad: ' . $data['evaluation_modality'] . '</h3>';

        /*****************************************
         * REQUERIMIENTO MINIMOS DE OFERTA
         *****************************************/
        if ($data['evaluation_modality'] == 'Cumplimiento de requerimientos técnicos mínimos') {
            if (isset($data['minimum_offer_required']) && $data['minimum_offer_required'] !== null && $data['minimum_offer_required'] !== '') {
                $minimumOfferRequiredMultiArray = json_decode($data['minimum_offer_required'], true);
                // $minimumOfferRequired = array_shift($minimumOfferRequiredMultiArray);
                $html .= '<h4>Requerimiento mínimo de la oferta: </h4>';
                $html .= '<ul>';
                if (is_array($minimumOfferRequiredMultiArray)) {
                    foreach ($minimumOfferRequiredMultiArray as $item) {
                        $html .= '<li>' . $this->cleanSymbolsOnPDF($item) . '</li>';
                    }
                }
//            else {
//                $html .= '<li>' . $this->cleanSymbolsOnPDF($minimumOfferRequired) . '</li>';
//            }
                $html .= '</ul>';
            }
        } else {
            /**
             * IMPORTANT
             * para los casos de criterios de evaluación, para la cotización la información se guardó con un formato
             * y para el quote se guardo con otro formato, por lo que se trabajo la data para que se adaptara a
             * la renderización de esta sección.
             */
            if ((isset($data['table_requirement']) && $data['table_requirement'] !== null && $data['table_requirement'] !== '')
                ||(isset($data['table_evaluation_criteria']) && $data['table_evaluation_criteria'] !== null && $data['table_evaluation_criteria'] !== '')) {

                $tableEvaluationCriteria = isset($data['table_requirement'])? json_decode($data['table_requirement'], true): json_decode($data['table_evaluation_criteria'], true);

                $html .= '<br><br>';
                $html .= '
                <style type="text/css">
                    .tg {border-collapse:collapse;border-color:#9ABAD9;border-spacing:0;border-style:solid;border-width:1px;margin:0px auto;}
                    .tg td{background-color:#EBF5FF;border-color:#9ABAD9;border-style:solid;border-width:0px;color:#444;font-family:Arial, sans-serif;font-size:10px;overflow:hidden;padding:10px 5px;word-break:normal;}
                    .tg th{background-color:#409cff;border-color:#9ABAD9;border-style:solid;border-width:0px;color:#fff;font-family:Arial, sans-serif;font-size:10px;font-weight:bold;overflow:hidden;padding:10px 5px;word-break:normal;}
                    .tg .tg-0lax{text-align:left;vertical-align:top}
                </style>
                <table class="tg"><thead>
                    <tr>
                        <th class="tg-0lax">Criterios de Evaluación</th>
                        <th class="tg-0lax">Ponderación (%)</th>
                        <th class="tg-0lax">Observaciones y Consideraciones del Criterio</th>
                    </tr>
                </thead>
                <tbody>';
                foreach ($tableEvaluationCriteria as $key => $item) {
                    if (key_exists('ponderacion', $item)) {
                        if ($item['ponderacion'] != "%" && !empty($item['ponderacion'])){
                            $html .= '<tr>';
                            key_exists('criterio_label', $item) ? $html .= '<td class="tg-0lax">' . $item['criterio_label'] . '</td>' : '';
                            key_exists('ponderacion', $item) ? $html .= '<td class="tg-0lax">' . $item['ponderacion'] . '</td>' : '';
                            key_exists('observaciones', $item) ? $html .= '<td class="tg-0lax">' . $item['observaciones'] . '</td>' : '';
                            $html .= '</tr>';
                        }

                    }
                }
                $html .= '</tbody></table><br><br>';
            }
        }


        if (isset($data['score'])) {
            $html .= '<h3>Umbral de corte evaluación técnica</h3>';
            $html .= '<h4>Puntaje: </h4>' . $data['score'] . '<br>';
        }
        $html .= '<h4>Criterios de desempate: </h4>' . $this->cleanSymbolsOnPDF($data['match_criterias']) . '<br>';
        $files = json_decode($data['file'], true);
        if ($files) {
            if (sizeof($files)) {
                $html .= '<h3>Documentos adjuntos: </h3><ul>';
                foreach ($files as $document) {
                    $fileSellerURL = $this->cmSoftwareHelper->getUrlMedia() . 'chilecompra/files/formattributes' . $document;
                    $onlyFileName = explode('/', $document);
                    $fileNameWithExtension = explode('.', $onlyFileName[count($onlyFileName) - 1]);
                    $html .= '<li><b>Archivo adjunto: </b><a href="' . $fileSellerURL . '">' . $fileNameWithExtension[0] . '.' . $fileNameWithExtension[1] . '</a></li>';

                }
                $html .= '</ul>';
            }
        }
        $html .= '</div>';

        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        // reset pointer to the last page
        $pdf->lastPage();
        return $pdf;
    }

    public function cleanSymbolsOnPDF($string)
    {
        return trim(preg_replace('/[^\p{L}\p{N}\s]\t/u', '', $string));
    }

    public function addPdfSellerInformation($pdf, $data)
    {
        // add a page
        $pdf->AddPage();

        // content
        $html = '<div>';
        $isAwarded = false;
        if ($data['approved_seller_quote_id'] && $data['entity_id'] == $data['approved_seller_quote_id']) {
            $isAwarded = true;
        }
        $html .= '<h1>Proveedor';
        $html .= $isAwarded ? ' (adjudicado): ' : ': ';
        $html .= $data["seller_name"] . '</h1>';

        $html .= '<b>RUT Proveedor: </b>' . $data["rut"] . '<br>';
        //$html .= '<b>Nombre Proveedor: </b>' . $data["seller_name"] . '<br>';
        $html .= '<b>Nombre Cotización Proveedor: </b>' . $data["quote_name"] . '<br>';
        $html .= '<b>Modalidad de Evaluación: </b>' . $data["evaluation_modality"];
        $tableRequirement = json_decode($data['table_requirement'], true);
        if ($tableRequirement) {
            if (sizeof($tableRequirement)) {
                $html .= '<ul>';
                foreach ($tableRequirement as $requirement) {
                    $count = 0;

                    if (isset($requirement['ponderacion'])) {

                            $value = str_replace('%', '', $requirement['ponderacion'], $count);
                        if ($value !="" ){
                            if ($count > 1) {
                                $requirement['ponderacion'] = preg_replace('/%/', '', $requirement['ponderacion'], $count - 1);
                            }
                            if ($value) {
                                $fileSellerURL = '';
                                $fileSellerName = '';
                                if (isset($requirement['criterio_file'])) {
                                    $fileSellerURL = $requirement['criterio_file'] ? $this->cmSoftwareHelper->getUrlMedia() . 'chilecompra/files/formattributes' . $requirement['criterio_file'] : '';

                                    $onlyFileName = explode('/', $requirement['criterio_file']);
                                    $fileNameWithExtension = explode('.', $onlyFileName[count($onlyFileName) - 1]);
                                    $fileSellerName = $this->filterText($requirement['criterio_label']);
                                    $fileSellerName .= '.' . $fileNameWithExtension[count($fileNameWithExtension) - 1];


                                }
                                $html .= '<li><b>' . $requirement['criterio_label'] . ': </b>' . $requirement['ponderacion'] . ' . <ul>';
                                if (isset($requirement['observaciones'])) {
                                    $html .= '<li>Observaciones: ' . $requirement['observaciones'] . '</li>';
                                }
                                if ($fileSellerName && $fileSellerURL) {
                                    $html .= '<li>Archivo adjunto: <a href="' . $fileSellerURL . '">' . $fileSellerName . '</a></li>';
                                }
                                $html .= '</ul></li>';
                            }
                        }

                    } else {

                            $html .= '<li><b>' . 'Requerimiento: </b>' . $requirement . '</li>';


                    }
                }
                $html .= '</ul>';
            }
        } else {
            $html .= '<br>';
        }
        $html .= '<b>Duración estimada para servicios: </b>' . $data["duracion_number"] . '<br>';
        $documents = json_decode($data['offer_document'], true);
        if ($documents) {
            if (sizeof($documents)) {
                $html .= '<b>Documentos: </b><ul>';
                foreach ($documents as $document) {
                    $fileSellerURL = $this->cmSoftwareHelper->getUrlMedia() . 'chilecompra/files/formattributes' . $document;
                    $onlyFileName = explode('/', $document);
                    $fileNameWithExtension = explode('.', $onlyFileName[count($onlyFileName) - 1]);
                    $html .= '<li><b>Archivo adjunto: </b><a href="' . $fileSellerURL . '">' . $fileNameWithExtension[0] . '.' . $fileNameWithExtension[1] . '</a></li>';

                }
                $html .= '</ul>';
            }
        }
        $teamwork = json_decode($data['teamwork'], true);
        if ($teamwork) {
            if (sizeof($teamwork)) {
                $html .= '<b>Equipo de Trabajo: </b><br><br>';
                $html .= '
                <style type="text/css">
                    .tg {border-collapse:collapse;border-color:#9ABAD9;border-spacing:0;border-style:solid;border-width:1px;margin:0px auto;}
                    .tg td{background-color:#EBF5FF;border-color:#9ABAD9;border-style:solid;border-width:0px;color:#444;font-family:Arial, sans-serif;font-size:10px;overflow:hidden;padding:10px 5px;word-break:normal;}
                    .tg th{background-color:#409cff;border-color:#9ABAD9;border-style:solid;border-width:0px;color:#fff;font-family:Arial, sans-serif;font-size:10px;font-weight:bold;overflow:hidden;padding:10px 5px;word-break:normal;}
                    .tg .tg-0lax{text-align:left;vertical-align:top}
                </style>
                <table class="tg"><thead>
                    <tr>
                        <th class="tg-0lax">Rol</th>
                        <th class="tg-0lax">Duración (hs)</th>
                    </tr>
                </thead>
                <tbody>';
                foreach ($teamwork as $key => $item) {
                    $html .= '<tr>';
                    key_exists('rol_teamwork', $item) ? $html .= '<td class="tg-0lax">' . $item['rol_teamwork'] . '</td>' : '';
//                    key_exists('name_teamwork',$item) ? $html .= '<td class="tg-0lax">' . $item['name_teamwork'] . '</td>' : '';
//                    key_exists('rut_teamwork',$item) ? $html .= '<td class="tg-0lax">' . $item['rut_teamwork'] . '</td>' : '';
                    key_exists('duracion_teamwork', $item) ? $html .= '<td class="tg-0lax">' . $item['duracion_teamwork'] . '</td>' : '';
                    $html .= '</tr>';
                }
                $html .= '</tbody></table><br><br>';
            }
        }
        $html .= '<b>Detalle de la oferta: </b><br>' . $data["quote_detalis"] . '<br>';
//        if ($data['quote_price']) {
//            if ($data['quote_price'] != "$0") {
//                $html .= '<b>Precio oferta: </b>' . $data["quote_price"];
//            } else {
//                if ($documents) {
//                    $html .= '<b>Precio oferta: </b> Ver archivo adjunto de oferta técnica económica.';
//                }
//            }
//        }
        if ($data['offer_price']) {
            $html .= '<b>Precio oferta: </b>' . $data["offer_price"];
            $html .= '<p>El % de descuento se debe revisar en la evaluación económica (paso 3) o revisar en el detalle del proveedor.</p>';

        }
        $html .= '</div>';

        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        // reset pointer to the last page
        $pdf->lastPage();
        return $pdf;
    }

    /**
     * Get Quote by current quote_id
     *
     * @return Webkul\Requestforquote\Model\Quote
     */
    public function getQuoteDetail($quoteId)
    {
        $resultCollection = null;
        $collection = $this->quoteFactory->create()->getCollection()
            ->addFieldToFilter('entity_id', $quoteId);
        $collection->getSelect()
            ->joinLeft(
                ['cv' => $this->resourceConnection->getTableName('requestforquote_quote_conversation')],
                'main_table.approved_seller_quote_id = cv.entity_id AND cv.edited = 1',
                []
            )
            ->joinLeft(
                ['info' => $this->resourceConnection->getTableName('requestforquote_quote_info')],
                'cv.seller_quote_id = info.entity_id',
                []
            )
            ->joinLeft(
                ['seller' => $this->resourceConnection->getTableName('customer_entity')],
                'info.seller_id = seller.entity_id',
                ['seller_id' => 'seller.entity_id', 'seller_name' => new \Zend_Db_Expr('CONCAT(firstname, " ", lastname)')]
            )
            ->joinLeft(
                ['oc' => $this->resourceConnection->getTableName('sales_order')],
                'main_table.entity_id = oc.requestforquote_info',
                ['url_orden_de_compra', 'orden_de_compra']
            )
            ->joinLeft(
                ['ee' => $this->resourceConnection->getTableName('dccp_economic_evaluation')],
                'cv.seller_quote_id = ee.seller_quote_id',
                ['base_price', 'final_price']
            );
        $collection->setPageSize(1);
        foreach ($collection as $item) {
            $resultCollection = $item;
        }

        return $resultCollection;
    }

    public function formatDateTime($date)
    {
        return $this->cmSoftwareHelper->formatDateTime($date);
    }

    public function formatStartDateTime($date)
    {
        return $this->cmSoftwareHelper->formatStartDateTime($date);
    }

    /**
     * Get value of text filter only by letter
     *
     * @return string
     */
    protected function filterText($text)
    {
        if ($text) {
            return str_replace(['á', 'é', 'í', 'ó', 'ú', ' '], ['a', 'e', 'i', 'o', 'u', '_'], strtolower(trim($text)));
        }
        return '';
    }
}
