<?php

namespace Formax\RatingSeller\Helper;

use Exception;
use Magento\Customer\Model\Customer;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection as EavCollection;

class Data extends AbstractHelper
{
    /**
     * @var string
     */
    const RATING_PRODUCT_ATTR_LABEL = [
        'swqa_performance1' => 'wkv_question_rate_avg_1',
        'swqa_performance2' => 'wkv_question_rate_avg_2',
        'swqa_performance3' => 'wkv_question_rate_avg_3',
        'swqa_performance4' => 'wkv_question_rate_avg_4',
        'swqa_performance5' => 'wkv_question_rate_avg_5'
    ];

    /**
     * @var Attribute
     */
    protected $eavAttribute;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var EavCollection
     */
    protected $eavCollection;

    /**
     * Initialize dependcies
     * 
     * @param Context $context
     * @param Attribute $eavAttribute
     * @param EavCollection $eavCollection
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManager
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        Context $context,
        Attribute $eavAttribute,
        EavCollection $eavCollection,
        CollectionFactory $collectionFactory,
        StoreManagerInterface $storeManager,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->eavAttribute = $eavAttribute;
        $this->storeManager = $storeManager;
        $this->eavCollection = $eavCollection;
        $this->collectionFactory = $collectionFactory;
        $this->customerRepository = $customerRepository;

        parent::__construct($context);
    }

    /**
     * Calculate avg rating for seller by question
     * 
     * @param string $attributeCode
     * @param int $sellerId
     * @return float
     */
    public function avgRatingByQuestion($attributeCode, $sellerId)
    {
        $avgRating = 0;
        $acum = 0;
        $count = 0;
        if ($attributeCode && $sellerId) {
            $storeId = $this->storeManager->getStore()->getId();
            $attributeId = $this->eavAttribute->getIdByCode('catalog_product', $attributeCode);
            $collection = $this->collectionFactory->create();
            $resource = $collection->getResource();

            $collection->addFieldToFilter('seller_id', $sellerId)
                ->getSelect()->reset(\Zend_Db_Select::COLUMNS)
                ->join(
                    ['q' => $resource->getTable('requestforquote_quote')],
                    'main_table.quote_id = q.entity_id',
                    []
                )->join(
                    ['e' => $resource->getTable('catalog_product_entity')],
                    'q.product_id = e.entity_id',
                    []
                )->join(
                    ['d' => $resource->getTable('catalog_product_entity_varchar')],
                    'e.row_id = d.row_id AND d.attribute_id = ' . $attributeId . ' AND d.store_id = ' . $storeId,
                    ['rating' => new \Zend_Db_Expr('AVG(d.value)')]
                );
            
            if ($collection) {
                foreach ($collection as $item) {
                    $avgRating = (float)$item->getRating();
                }
            }
        }

        return $avgRating;
    }

    /**
     * Calculate total avg rating for seller
     * 
     * @param array $arrayValues
     * @return float
     */
    public function totalAvgRating($arrayValues)
    {
        $totalAvg = 0;
        $acum = 0;
        if (is_array($arrayValues) && count($arrayValues) > 0) {
            foreach ($arrayValues as $item) {
                $acum += (float)$item;
            }

            $totalAvg = $acum/count($arrayValues);
        }

        return $totalAvg;
    }

    /**
     * get avg rating by seller
     * 
     * @param int $sellerId
     * @return array
     */
    public function getRatingBySeller($sellerId)
    {
        $rating = [];
        $attributeArray = [];
        $attributeCodes = [
            'wkv_question_rate_avg_1',
            'wkv_question_rate_avg_2',
            'wkv_question_rate_avg_3',
            'wkv_question_rate_avg_4',
            'wkv_question_rate_avg_5'
        ];

        try {
            $sellerId = (int)$sellerId;
            $seller = $this->customerRepository->getById($sellerId);
            $storeId = $this->storeManager->getStore()->getId();

            if ($seller && is_array($attributeCodes) && count($attributeCodes) > 0) {
                foreach ($attributeCodes as $attributeCode) {
                    $attributeArray[$attributeCode] = [
                        'value' => $seller->getCustomAttribute($attributeCode) ?
                            (float)$seller->getCustomAttribute($attributeCode)->getValue() : 0
                    ];
                }
            }

            $eavCollection = $this->eavCollection;
            $eavCollection->addStoreLabel($storeId)
                ->setCodeFilter($attributeCodes)
                ->setEntityTypeFilter(1);
            
            foreach ($eavCollection as $eav) {
                $attrCode = $eav->getAttributeCode();
                $rating[$attrCode] = [
                    'label' => $eav->getStoreLabel(),
                    'value' => isset($attributeArray[$attrCode]['value'])
                        ? $attributeArray[$attrCode]['value'] : 0
                ];
            }

        } catch (Exception $e) {

        }

        return $rating;
    }

    /**
     * Get seller rating attribute code by product rating attribute code
     * 
     * @param string $productAttributeCode
     * @return string
     */
    public function getSellerAttribute($productAttributeCode)
    {
        return isset(self::RATING_PRODUCT_ATTR_LABEL[$productAttributeCode])
            ? self::RATING_PRODUCT_ATTR_LABEL[$productAttributeCode] : false;
    }
}