<?php

namespace Formax\RatingSeller\Block;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Webkul\Marketplace\Helper\Data as MpHelper;
use Webkul\Marketplace\Model\FeedbackFactory;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;
use Webkul\Marketplace\Model\ProductFactory as MpProductModel;
use Magento\Catalog\Model\ProductFactory;
use Webkul\Marketplace\Block\Profile;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Catalog\Block\Product\Context;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Url\Helper\Data as UrlHelper;
use Formax\RatingSeller\Helper\Data as RatingHelper;

/**
 * Class Summary
 */
class Summary extends Profile
{
    private RatingHelper $ratingHelper;

    /**
     * @param Context $context
     * @param PostHelper $postDataHelper
     * @param UrlHelper $urlHelper
     * @param Customer $customer
     * @param Session $session
     * @param StringUtils $stringUtils
     * @param MpHelper $mpHelper
     * @param FeedbackFactory $feedbackModel
     * @param CollectionFactory $mpProductCollection
     * @param MpProductModel $mpProductModel
     * @param ProductFactory $productFactory
     * @param RatingHelper $ratingHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        PostHelper $postDataHelper,
        UrlHelper $urlHelper,
        Customer $customer,
        Session $session,
        StringUtils $stringUtils,
        MpHelper $mpHelper,
        FeedbackFactory $feedbackModel,
        CollectionFactory $mpProductCollection,
        MpProductModel $mpProductModel,
        ProductFactory $productFactory,
        RatingHelper $ratingHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $postDataHelper,
            $urlHelper,
            $customer,
            $session,
            $stringUtils,
            $mpHelper,
            $feedbackModel,
            $mpProductCollection,
            $mpProductModel,
            $productFactory,
            $data
        );

        $this->ratingHelper = $ratingHelper;
    }

    public function getRatingSummary($sellerId)
    {
        return $this->ratingHelper->getRatingBySeller($sellerId);
    }
}
