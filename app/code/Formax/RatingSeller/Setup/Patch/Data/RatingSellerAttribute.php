<?php

namespace Formax\RatingSeller\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Eav\Model\Config as Eavconfig;
use Webkul\MpVendorAttributeManager\Model\VendorAttributeFactory;

class RatingSellerAttribute implements DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Customer\Setup\CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\SetFactory
     */
    private $attributeSetFactory;

    /**
     * @var \Webkul\MpVendorAttributeManager\Model\VendorAttributeFactory
     */
    private $vendorAttributeFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory $customerSetupFactory
     * @param VendorAttributeFactory $vendorAttributeFactory
     * @param EavConfig $eavConfig
     * @param SetFactory $attributeSetFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        VendorAttributeFactory $vendorAttributeFactory,
        EavConfig $eavConfig,
        SetFactory $attributeSetFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavConfig = $eavConfig;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->vendorAttributeFactory = $vendorAttributeFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        $entityTypeId = Customer::ENTITY;
        $newAttributes = [
            'wkv_question_rate_avg_1' => 'Promedio valoración pregunta 1',
            'wkv_question_rate_avg_2' => 'Promedio valoración pregunta 2',
            'wkv_question_rate_avg_3' => 'Promedio valoración pregunta 3',
            'wkv_question_rate_avg_4' => 'Promedio valoración pregunta 4',
            'wkv_question_rate_avg_5' => 'Promedio valoración pregunta 5',
            'wkv_total_avg' => 'Promedio valoración total',
        ];

        foreach ($newAttributes as $newAttributeCode => $newAttributeLabel) {
            $attributeExists = $this->isEavAttributeExists($entityTypeId, $newAttributeCode);
            if (!$attributeExists) {
                $i = 0;
                $sort = 500 + ($i*10);
                $customerSetup->addAttribute($entityTypeId, $newAttributeCode, [
                    'type' => 'decimal',
                    'label' => $newAttributeLabel,
                    'input' => 'text',
                    'frontend_class' => '',
                    'required' => false,
                    'default_value' => 0,
                    'visible' => false,
                    'user_defined' => true,
                    'sort_order' => $sort,
                    'position' => $sort,
                    'system' => 0
                ]);
                $attribute = $customerSetup->getAttribute($entityTypeId, $newAttributeCode);
                $customerSetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $attributeGroupId,
                    $attribute['attribute_id']
                );

                /** save record for webkul attribute manager table **/
                $vendorAttribute = $this->vendorAttributeFactory->create();
                $vendorAttribute->setAttributeId($attribute['attribute_id']);
                $vendorAttribute->setRequiredField(0);
                $vendorAttribute->setAttributeUsedFor(2);
                $vendorAttribute->setShowInFront(0);
                $vendorAttribute->save();
            }
        }

        return $this;
    }

    /**
     * Get if EAV attribute exists
     *
     * @param int $entityTypeId
     * @param string $field
     * @return bool
     */
    public function isEavAttributeExists($entityTypeId, $field)
    {
        $attr = $this->eavConfig->getAttribute($entityTypeId, $field);
        return ($attr && $attr->getId()) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }
}
