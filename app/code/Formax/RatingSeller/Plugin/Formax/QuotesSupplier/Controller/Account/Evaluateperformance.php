<?php

namespace Formax\RatingSeller\Plugin\Formax\QuotesSupplier\Controller\Account;

use Formax\RatingSeller\Helper\Data;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Customer;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Formax\QuotesSupplier\Controller\Account\Evaluateperformance as QuotesSupplierRatingSeller;

class Evaluateperformance
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Initialize dependencies
     * 
     * @param Data $helper
     * @param CustomerFactory $customerFactory
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        Data $helper,
        CustomerFactory $customerFactory,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->helper = $helper;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Plugin after execute method
     * 
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function afterExecute(QuotesSupplierRatingSeller $subject, $result)
    {
        $post = $subject->getRequest()->getPostValue();
        $sellerId = isset($post['sellerId']) ? (int)$post['sellerId'] : 0;
        $totalAvgArray = [];

        if ($sellerId > 0) {
            $seller = $this->customerRepository->getById($sellerId);
            foreach ($post as $attrcode => $attrvalue) {
                if (strpos($attrcode, 'swqa_performance') !== false) {
                    $matchAttrCode = $this->helper->getSellerAttribute($attrcode);
                    $avgRatingQuestion = $this->helper->avgRatingByQuestion($attrcode, $sellerId);
                    $totalAvgArray[] = $avgRatingQuestion;
                    $seller->setCustomAttribute($matchAttrCode, $avgRatingQuestion);
                    $this->customerRepository->save($seller);
                }
            }
            
            $totalAvg = $this->helper->totalAvgRating($totalAvgArray);
            $seller->setCustomAttribute('wkv_total_avg', $totalAvg);
            $this->customerRepository->save($seller);
        }

        return $result;
    }
}