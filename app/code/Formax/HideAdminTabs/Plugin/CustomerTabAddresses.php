<?php
namespace Formax\HideAdminTabs\Plugin;

use \Formax\HideAdminTabs\Helper\Data;


class CustomerTabAddresses
{
    private $helper;

    public function __construct(
      Data $helper
    ){
      $this->helper = $helper;
    }
    public function afterIsComponentVisible($subject, $result)
    {
      if(!$this->helper->isAdmin()){
        $result = false;
      }
      return $result;
    }

}
