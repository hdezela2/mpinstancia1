<?php
namespace Formax\HideAdminTabs\Plugin;

use \Formax\HideAdminTabs\Helper\Data;


class CustomerTab
{
    private $helper;

    public function __construct(
      Data $helper
    ){
      $this->helper = $helper;
    }
    public function afterCanShowTab($subject, $result)
    {
      if(!$this->helper->isAdmin()){
        $result = false;
      }
      return $result;
    }
}
