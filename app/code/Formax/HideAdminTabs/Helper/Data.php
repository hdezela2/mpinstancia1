<?php
namespace Formax\HideAdminTabs\Helper;

use Magento\Backend\Model\Auth\Session;

class Data extends \Magento\Framework\App\Helper\AbstractHelper{

   protected $authSession;
   private const ROLE_ADMIN = "ADMINISTRATORS";

   public function __construct(
     Session $session
   ){
     $this->authSession = $session;
  }

  public function isAdmin(){
    if($this->authSession->isLoggedIn()){
      $roleName = $this->authSession->getUser()->getRole()->getRoleName();
      if (strtoupper($roleName) == self::ROLE_ADMIN) {
        return true;
      }
    }
    return false;
  }
}
