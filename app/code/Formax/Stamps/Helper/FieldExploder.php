<?php

namespace Formax\Stamps\Helper;

/**
 * Class FieldExploder
 * Helper class for explode CSV fields into a key value object.
 *
 */
class FieldExploder extends \Magento\Framework\App\Helper\AbstractHelper{

    /**
     * @var string
     */
    const PAIR_NAME_VALUE_SEPARATOR = '=';
    /**
     * @var string
     */
    const DEFAULT_GLOBAL_MULTI_VALUE_SEPARATOR = ',';
    /**
     * @var string
     */
    const DEFAULT_TEXT_QUOTES_SYMBOL = "'";

    /**
     * Explode text, separating by comma and equals, sent by CSV
     *
     * @param string $stamps
     * @return array
     */
    public function getObject($stamps){
      $attributeNameValuePairs = explode(self::DEFAULT_GLOBAL_MULTI_VALUE_SEPARATOR, $stamps);
      $preparedAttributes = [];
      $code = '';
      foreach ($attributeNameValuePairs as $attributeData) {
          if (strpos($attributeData, self::PAIR_NAME_VALUE_SEPARATOR) === false) {
              if (!$code) {
                  continue;
              }
              $preparedAttributes[$code] .= self::DEFAULT_GLOBAL_MULTI_VALUE_SEPARATOR . $attributeData;
              continue;
          }
          list($code, $value) = explode(self::PAIR_NAME_VALUE_SEPARATOR, $attributeData, 2);
          $preparedAttributes[$code] = str_replace(self::DEFAULT_TEXT_QUOTES_SYMBOL,'',$value);
      }
      return $preparedAttributes;
    }
}
