define([
    "jquery"
], function($){
    var StorageStamps = {
        init: function(config){
            StorageStamps.saveStamps(config);
        },
        saveStamps: function(obj){
            var seller = obj.seller,
                stamps = obj.stamps;
            
            if(!window.supplierStamps){
                window.supplierStamps = {};
            }
            var winStamps = window.supplierStamps;
            winStamps[seller] = stamps;
            $(document).trigger('WK_AssignProduct_PricesChanged');
        }
    };
    return {
        'storage-stamps': StorageStamps.init
    }
});