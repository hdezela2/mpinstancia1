require(
  [ 'jquery', 'mage/url', 'mage/translate' ],
  function ($, url, $t) {

    const PAIR_NAME_VALUE_SEPARATOR = '=';
    const DEFAULT_GLOBAL_MULTI_VALUE_SEPARATOR = ',';
    const IMG_FOLDER = 'SELLOS';
    const DEFAULT_TEXT_QUOTES_SYMBOL = "'";

    $(function(){
      var search = setInterval(function(){
        var divs = $("div[class*='field-wkv_dccp_stamp_']");
        if(divs.length){
          divs.first().before('<div class="admin__field field field-wkv_dccp_stamps">'+
                                '<label class="label admin__field-label"><span>'+$t('Stamps')+'</span></label>'+
                                '<div class="admin__field-control control seller-profile-stamps">'+
                                  '<div class="cc-seller-stamps"></div>'+
                                '</div>'+
                              '</div>');
          var control = $('.field-wkv_dccp_stamps .control .cc-seller-stamps');
          var stamps = [];
          $.each(divs, function(){
            var _stamp = getObject($(this).find('input').val());
            if(_stamp['id']){
              var id = _stamp['id'],
                  name = _stamp['name'] ? _stamp['name'] : '',
                  desc = _stamp['description'] ? _stamp['description'] : '';
              var s = [];
              s['id'] = id;
              s['name'] = name;
              s['desc'] = desc;
              stamps.push(s);
            }
          });
          divs.remove();
          if(stamps.length){
            stamps.forEach(function(stamp){
              var colon = (stamp['name'] != '') ? ': ' : '';
              var imgUrl = url.build('/pub/media/wysiwyg/'+IMG_FOLDER+'/'+stamp['id']+'.png');
              control.append('<div class="cc-seller-stamp" data-title="'+stamp['name']+colon+stamp['desc']+'">'+
                              '<img src="'+imgUrl+'" alt="'+stamp['name']+colon+stamp['desc']+'" onerror="this.style.display = `none`;">'+
                             '</div>');
            });
          }else{
            control.append('<p>'+$t("This seller doesn't have stamps.")+'</p>');
          }
          clearInterval(search);
        }
      }, 300);

    });

    function getObject(stamps){
      var attributeNameValuePairs = stamps.split(DEFAULT_GLOBAL_MULTI_VALUE_SEPARATOR);
      var preparedAttributes = [];
      var code = '';
      var regex = new RegExp(DEFAULT_TEXT_QUOTES_SYMBOL, 'g');
      attributeNameValuePairs.forEach(function(attributeData){
        if (!attributeData.includes(PAIR_NAME_VALUE_SEPARATOR)) {
            if (!code) {
                return;
            }
            preparedAttributes[code] += DEFAULT_GLOBAL_MULTI_VALUE_SEPARATOR + attributeData.replace(regex,'');
            return;
        }
        [code, value] = attributeData.split(PAIR_NAME_VALUE_SEPARATOR, 2);
        preparedAttributes[code] = value.replace(regex,'');
      });
      return preparedAttributes;
    }

  }
);
