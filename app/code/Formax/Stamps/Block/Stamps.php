<?php
namespace Formax\Stamps\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use \Formax\Stamps\Helper\FieldExploder;

class Stamps extends Template{

  /**
   * @var int
   */
  private $seller;
  /**
   * @var \Magento\Customer\Model\CustomerFactory
   */
  protected $_customer;
  /**
   * @var \Formax\Stamps\Helper\FieldExploder
   */
  protected $_helper;
  /**
   * @var string
   */
  const FOLDER_NAME = 'SELLOS';
  /**
   * @var string
   */
  const ATTR_NAME = 'wkv_dccp_stamp_';

  /**
   * @param Magento\Framework\View\Element\Template\Context $context
   * @param \Magento\Customer\Model\CustomerFactory $customer
   * @param \Formax\Stamps\Helper\FieldExploder $fieldExploder
   */
  public function __construct(
    Context $context,
    \Magento\Customer\Model\CustomerFactory $customer,
    FieldExploder $fieldExploder,
    array $data = []
  ){
      parent::__construct($context, $data);
      $this->_helper = $fieldExploder;
      $this->_customer = $customer;
  }

  /**
   * Set seller id
   *
   * @param int $id
   */
  public function setSellerId($id){
    $this->seller = $id;
  }

  /**
   * Get seller id
   *
   * @return int
   */
  public function getSellerId(){
    return $this->seller;
  }

  /**
   * Get seller
   *
   * @return \Magento\Customer\Model\CustomerFactory|null
   */
  public function getSeller(){
    if($this->getSellerId()!==null){
      return $this->_customer->create()->load($this->getSellerId());
    }
    return null;
  }

  /**
   * Get stamps array
   *
   * @param \Magento\Customer\Model\CustomerFactory $seller
   * @return array
   */
  public function getStamps($seller){
    $i = 1;
    $stamps = [];
    while( $seller->getAttribute(self::ATTR_NAME.$i) ){
      $data = $seller->getData(self::ATTR_NAME.$i);
      $_stamp = (object)$this->_helper->getObject($data);
      if(isset($_stamp->id)){
        $id = $_stamp->id;
        $name = isset($_stamp->name) ? $_stamp->name : '';
        $desc = isset($_stamp->description) ? $_stamp->description : '';
        $s = new \stdClass();
        $s->id = $id;
        $s->name = $name;
        $s->desc = $desc;
        $stamps[] = $s;
      }
      $i++;
    }
    return $stamps;
  }

  /**
   * Get stamp image
   *
   * @param string $stampId
   * @return string
   */
  public function getStampsImg($stampId){
    return $this->getMediaUrl().'wysiwyg/'.self::FOLDER_NAME.'/'.$stampId.'.png';
  }

  /**
   * Get magento media URL
   *
   * @return string
   */
  protected function getMediaUrl(){
    return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
  }
}
