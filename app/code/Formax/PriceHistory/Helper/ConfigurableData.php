<?php

namespace Formax\PriceHistory\Helper;

/**
 * Class ConfigurableData
 * Helper class for getting options
 *
 */
class ConfigurableData extends \Magento\ConfigurableProduct\Helper\Data{

    /**
     * Get swatches options added the DCCP custom product ID
     *
     * @param array $allowedProducts
     * @return array
     */
    public function getOptionsDinamic($allowedProducts)
    {
        $options = [];
        foreach ($allowedProducts as $product) {
            $productId = $product->getId();
            $options['dccpProductId'][$productId] = $product->getSku();
        }

        return $options;
    }
}
