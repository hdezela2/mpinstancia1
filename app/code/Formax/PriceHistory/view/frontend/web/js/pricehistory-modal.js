define([
	'jquery',
	'underscore',
	'Magento_Ui/js/modal/modal',
	'chart'
], function($, _) {
	const PriceHistoryModal = {
		initHistoryModal: function(config, element)
		{
			$target = $('#pricehistory-popup');
          	const $element = $('#viewPriceHistory');
          	const options = {
				responsive: true,
				innerScroll: true,
				modalClass: 'pricehistory-modal',
				title: 'Historial de precios',
				closeText: 'Cerrar',
				buttons: []
          	};
          	$target.modal(options);
          	$element.click(function()
			      {
            	let prodId = null,
                endpoint = config.endpoint,
                swatchRenderer = $('[data-role=swatch-options]').data('mageSwatchRenderer');
				        prodId = swatchRenderer ? window.dccpProductIdFound : (config.type == "configurable") ? getChildProduct() : config.dccp_productid;
            	const table = $target.find('table#prices-list'),
				chartAreaSection = $target.find('div.chartArea'),
                tbody = table.find('tbody'),
                errText = 'Lo sentimos, hubo un problema al intentar obtener el historial de precios.',
                errNFound = 'Este producto aún no cuenta con esta información.';

            	if (prodId && endpoint != '') {
              		$.ajax({
						        showLoader: true,
						        url: endpoint.replace('{{id}}', prodId),
						        method: 'GET',
						        dataType: 'json',
                		error: function(res) {
                  			let body = res.responseJSON;
                  			let errMsg = errText;
                  			if (Object.keys(res).length && body && body['errores'].length) {
                    			body.errores.forEach(function(error) {
									if (error.codigo == 404 || error.codigo == 400) {
										errMsg = errNFound;
									}
                    			});
							} else {
								switch(res.status) {
									case 500:
										errMsg = 'Lo sentimos, hubo un problema al intentar obtener el historial.';
									break;
									default:
										errMsg = 'Lo sentimos, hubo un problema al intentar obtener el historial.';
									break;
								}
							}
							table.addClass('no-records');
                  			chartAreaSection.hide();
							tbody.find('tr#no-records>td').text(errMsg);
							$(document).ready(function() {
								$target.modal('openModal');
							});
                		},
						success: function(res) {
							table.removeClass('no-records');
							tbody.find('tr:not(#no-records)').remove();

							var getDate = function(fecha) {
								const date = new Date(fecha);

								return {
									day: date.getUTCDate(),
									month: date.getUTCMonth() + 1,
									year: date.getUTCFullYear()
								}
							};
							var datasets = {};
							var foundRows = 0;
							res.payload.forEach(function(item) {

								if (item.urlordendecompra) {
									foundRows++;
									const date = getDate(new Date(item.fecha));
									const newdate = date.year + '/' + date.month + '/' + date.day;

									if (!datasets[date.year]) {
										datasets[date.year] = {};
									}

									if (datasets[date.year][date.month - 1]) {
										datasets[date.year][date.month - 1].sum += item.precio;
										datasets[date.year][date.month - 1].no += 1;
									} else {
										datasets[date.year][date.month - 1] = {
											sum: item.precio,
											no: 1
										}
									}

									tbody.append('' +
										'<tr>' +
										'<td><a href="' + item.urlordendecompra + '" target="_blank">' + item.ordeDeCompra + '</a></td>' +
										'<td>' + newdate + '</td>' +
										'<td>' + item.region + '</td>' +
										'<td>' + item.comuna + '</td>' +
										'<td>' + item.moneda + '</td>' +
										'<td>$' + item.precio + '</td>' +
										'</tr>'
									);
								}
							});

							var chartData = [];
							var chartColors = [
								'rgb(48, 104, 255)',
								'rgb(244, 73, 149)',
								'rgb(244, 149, 73)',
								'rgb(104, 48, 244)'
							];
							var chartBgColors = [
								'rgba(48, 104, 255, 0.1)',
								'rgba(244, 73, 149, 0.1)',
								'rgba(244, 149, 73, 0.1)',
								'rgba(104, 48, 244, 0.1)'
							];

							Object.keys(datasets).forEach(function(item, index) {
								chartData[index] = {};
								chartData[index].label = item;
								chartData[index].data = [];
								chartData[index].fill = true;
								chartData[index].borderColor = chartColors[index];
								chartData[index].backgroundColor = chartBgColors[index];
								chartData[index].lineTension = 0.1;

								for (var i = 0; i < 12; i++) {
									if (datasets[item][i]) {
										chartData[index].data[i] = parseInt((parseFloat(datasets[item][i].sum) / parseFloat(datasets[item][i].no)).toFixed(0));
									} else {
										chartData[index].data[i] = 0;
									}
								}
							});

							var ctx = document.getElementById('chart').getContext('2d');
							new Chart(ctx, {
								"type": "line",
								"data": {
									"labels": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
									"datasets": chartData
								},
								"options": {}
							});
							Chart.platform.disableCSSInjection = true;
							if (foundRows == 0) {
								table.addClass('no-records');
								chartAreaSection.hide();
								tbody.find('tr#no-records>td').text('Lo sentimos, hubo un problema al intentar obtener el historial.');
							}
							$(document).ready(function() {
								$target.modal('openModal');
							});
						}
              		});
            	} else {
					table.addClass('no-records');
					tbody.find('tr#no-records>td').text(errText);
					$(document).ready(function() {
						$target.modal('openModal');
					});
            	}
          	});
      	}
  	};

	function getChildProduct()
	{
		return $('#form_quotesupplier .col_2quote .input_disable').text();
	}
	  
	return {
		'pricehistory-modal': PriceHistoryModal.initHistoryModal
  	};
});
