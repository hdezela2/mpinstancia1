<?php

namespace Formax\PriceHistory\Block\Product\View\Type;

class Configurable{

    /**
     * @var \Magento\Framework\Json\EncoderInterface $jsonEncoder
     */
    protected $jsonEncoder;
    /**
     * @var \Magento\Framework\Json\DecoderInterface $jsonDecoder
     */
    protected $jsonDecoder;
    /**
     * @var \Formax\PriceHistory\Helper\ConfigurableData $swatchHelper
     */
    protected $swatchHelper;

    /**
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Json\DecoderInterface $jsonDecoder
     * @param \Formax\PriceHistory\Helper\ConfigurableData $swatchHelper
     */
    public function __construct(
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Json\DecoderInterface $jsonDecoder,
        \Formax\PriceHistory\Helper\ConfigurableData $swatchHelper
    ) {
        $this->jsonDecoder = $jsonDecoder;
        $this->jsonEncoder = $jsonEncoder;
        $this->swatchHelper = $swatchHelper;
    }

    /**
     * Composes configuration for js
     *
     * @return string
     */
    public function aroundGetJsonConfig(
        \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject,
        \Closure $proceed
    )
    {
        $productOptions = $this->swatchHelper->getOptionsDinamic($subject->getAllowProducts());
        $config = $proceed();
        $config = $this->jsonDecoder->decode($config);
        $config['dccpProductId'] = isset($productOptions['dccpProductId']) ? $productOptions['dccpProductId'] : [];
        return $this->jsonEncoder->encode($config);
    }
}
