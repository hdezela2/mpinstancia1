<?php
namespace Formax\PriceHistory\Block;

use Magento\Framework\View\Element\Template\Context;

class Modal extends \Magento\Catalog\Block\Product\View\Description{

  /**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
  protected $scope;
  /**
   * @var string
   */
  protected $systemPath = 'dccp_endpoint/pricehistory/';
  /**
   * @var \Magento\Store\Model\ScopeInterface
   */
  protected $interface = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

  /**
   * @param \Magento\Framework\View\Element\Template\Context $context
   * @param \Magento\Framework\Registry $registry
   */
  public function __construct(
    Context $context,
    \Magento\Framework\Registry $registry
  ){
      $this->scope = $context->getScopeConfig();
      parent::__construct($context, $registry);
  }

  /**
   * Get the endpoint for the price history service
   *
   * @return string
   */
  public function getEndpoint(){
    return $this->getConfig('endpoint');
  }

  /**
   * Get the values from the Magento 2 admin config
   *
   * @param string $config
   * @return string
   */
  private function getConfig($config){
    return $this->scope->getValue($this->systemPath.$config, $this->interface);
  }
}
