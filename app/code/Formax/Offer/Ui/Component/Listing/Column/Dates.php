<?php

namespace Formax\Offer\Ui\Component\Listing\Column;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\UrlInterface;
use Formax\Offer\Helper\Data as Helper;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;

class Dates extends Column
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var HelperSoftware
     */
    protected $helperSoftware;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Helper $helper
     * @param HelperSoftware $helperSoftware
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Helper $helper,
        HelperSoftware $helperSoftware,
        array $components = array(),
        array $data = array()
    ) {
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );

        $this->helper = $helper;
        $this->helperSoftware = $helperSoftware;
    }

    /**
     * Prepare component configuration
     *
     * @return void
     */
    public function prepare()
    {
        parent::prepare();

        if ($this->helperSoftware->getCurrentStoreCode() == HelperSoftware::SOFTWARE_STOREVIEW_CODE  || $this->helperSoftware->getCurrentStoreCode() == SoftwareRenewalConstants::STORE_CODE) {
            $this->_data['config']['componentDisabled'] = true;
        }
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');

                if (isset($item[$name])) {
                    if (isset($item['start_date']) && isset($item['end_date'])) {
                        $offerActive = $this->helper->datesAreCurrent($item['start_date'], $item['end_date']);
                        $item[$name] = $offerActive !== 0 ? $item[$name] : '';;
                    }
                }
            }
        }

        return $dataSource;
    }
}
