<?php

namespace Formax\Offer\Ui\Component\Listing\Column;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Framework\UrlInterface;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Summa\AlimentosSetUp\Helper\Data as AlimentosHelper;
use Summa\ProductPackages\Helper\Data as PackagesHelper;
use Formax\Offer\Helper\Data;

/**
 * Class Actions
 */
class Actions
{
    /**
     * Url path
     */
    const URL_PATH_EDIT = 'mpassignproduct/product/edit';
    const URL_PATH_DELETE = 'mpassignproduct/product/delete';
    const URL_PATH_OFFER = 'offerassignproduct/offer/index';

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    protected $helper;

    /**
     * @var HelperSoftware
     */
    protected $helperSoftware;

    /**
     * @var AlimentosHelper
     */
    protected $alimentosHelper;

    /**
     * @var PackagesHelper
     */
    protected $packagesHelper;

    /**
     * @var HelperTemporaryUpdate|\Summa\TemporaryPriceAdjustment\Helper\Data
     */
    protected $helperTemporaryUpdate;

    /**
     * Actions constructor.
     * @param UrlInterface $urlBuilder
     * @param HelperSoftware $helperSoftware
     * @param HelperTemporaryUpdate $helperTemporaryUpdate
     * @param Data $data
     */
    public function __construct(
        UrlInterface $urlBuilder,
        HelperSoftware $helperSoftware,
        AlimentosHelper $helper,
        PackagesHelper $packagesHelper,
        \Summa\TemporaryPriceAdjustment\Helper\Data $helperTemporaryUpdate,
        Data $data
    ) {
        $this->helperSoftware = $helperSoftware;
        $this->helperTemporaryUpdate = $helperTemporaryUpdate;
        $this->urlBuilder = $urlBuilder;
        $this->helper = $data;
        $this->alimentosHelper = $helper;
        $this->packagesHelper = $packagesHelper;
    }

    /**
     * @param \Formax\AssignProduct\Ui\Component\Listing\Column\Actions $subject
     * @param callable $proceed
     * @param array $dataSource
     * @return array
     */
    public function aroundPrepareDataSource (
        \Formax\AssignProduct\Ui\Component\Listing\Column\Actions $subject,
        callable $proceed,
        array $dataSource
    )
    {
        if (isset($dataSource['data']['items']))
        {
            foreach ($dataSource['data']['items'] as & $item)
            {
                if (isset($item['id']))
                {
                    $title = $subject->getEscaper()->escapeHtml($item['name']);
                    if (
                        $this->helperSoftware->getCurrentStoreCode() === \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE
                        && $this->packagesHelper->isProductAPackage($item['sku'])
                    ) {
                        $item[$subject->getData('name')] = [];
                    } elseif (
                        $item['type_id'] == 'configurable' ||
                        (
                            $item['status'] == '1' &&
                            (
                                !in_array(
                                    $this->helperSoftware->getCurrentStoreCode(),
                                    [
                                        HelperSoftware::SOFTWARE_STOREVIEW_CODE,
                                        SoftwareRenewalConstants::STORE_VIEW_CODE
                                    ]
                                ) && $this->helper->shouldRenderAddProductLink()
                            )
                        )
                    ) {
                        $item[$subject->getData('name')] = [
                            'edit' => [
                                'href' => $this->urlBuilder->getUrl(
                                    static::URL_PATH_EDIT,
                                    [
                                        'id' => $item['id']
                                    ]
                                ),
                                'label' => __('Edit'),
                                'confirm' => [
                                    'title' => __('Edit %1', $title),
                                    'message' => __('Are you sure you want to edit a %1 record?', $title)
                                ]
                            ],
                            'offer' => [
                                'href' => $this->urlBuilder->getUrl(
                                    static::URL_PATH_OFFER,
                                    [
                                        'id' => $item['id']
                                    ]
                                ),
                                'label' => __('Offer special price'),
                                'confirm' => [
                                    'title' => __('Offer %1', $title),
                                    'message' => __('Are you sure you want to offer a %1 record?', $title)
                                ]
                            ]
                        ];
                    } else {
                        $item[$subject->getData('name')] = [
                            'edit' => [
                                'href' => $this->urlBuilder->getUrl(
                                    static::URL_PATH_EDIT,
                                    [
                                        'id' => $item['id']
                                    ]
                                ),
                                'label' => __('Edit'),
                                'confirm' => [
                                    'title' => __('Edit %1', $title),
                                    'message' => __('Are you sure you want to edit a %1 record?', $title)
                                ]
                            ]
                        ];
                    }
                }
            }
        }
        return $dataSource;
    }
}
