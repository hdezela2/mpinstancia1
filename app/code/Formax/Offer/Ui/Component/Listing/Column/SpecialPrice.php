<?php

namespace Formax\Offer\Ui\Component\Listing\Column;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Linets\VoucherSetup\Model\Constants as Voucher2021Constants;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\UrlInterface;
use Formax\Offer\Helper\Data as Helper;
use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;
use Summa\CombustiblesSetUp\Helper\Data as CombustiblesHelper;
use Chilecompra\CombustiblesRenewal\Model\Constants as CombustiblesRenewal202110;
use Summa\EmergenciasSetUp\Helper\Data;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;
use Summa\MobiliarioSetUp\Helper\Data as MobiliarioHelper;
use Linets\VoucherSetup\Model\Constants as VoucherSetupConstants;

class SpecialPrice extends Column
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var VoucherHelper
     */
    protected $voucherHelper;

    /**
     * @var HelperSoftware
     */
    protected $helperSoftware;

    /**
     * @var Data
     */
    protected $_emergencyHelper;

    /**
     * @var MobiliarioHelper
     */
    protected $mobiliarioHelper;

    /**
     * SpecialPrice constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Helper $helper
     * @param VoucherHelper $voucherHelper
     * @param HelperSoftware $helperSoftware
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Helper $helper,
        VoucherHelper $voucherHelper,
        HelperSoftware $helperSoftware,
        Data $emergencyHelper,
        MobiliarioHelper $mobiliarioHelper,
        array $components = array(),
        array $data = array()
    ) {
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );

        $this->voucherHelper = $voucherHelper;
        $this->helperSoftware = $helperSoftware;
        $this->_emergencyHelper = $emergencyHelper;
        $this->mobiliarioHelper = $mobiliarioHelper;
        $this->helper = $helper;
    }

    /**
     * Prepare component configuration
     *
     * @return void
     */
    public function prepare()
    {
        parent::prepare();

        if ($this->helperSoftware->getCurrentStoreCode() == HelperSoftware::SOFTWARE_STOREVIEW_CODE  || $this->helperSoftware->getCurrentStoreCode() == SoftwareRenewalConstants::STORE_CODE) {
            $this->_data['config']['componentDisabled'] = true;
        }
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            if (in_array($this->voucherHelper->getCurrentStoreCode(),[
                VoucherHelper::VOUCHER_STOREVIEW_CODE,
                Voucher2021Constants::STORE_VIEW_CODE
            ])) {
                foreach ($dataSource['data']['items'] as & $item) {
                    $name = $this->getData('name');

                    if (isset($item[$name])) {
                        if ($item['type_id'] == 'configurable') {
                            $item[$name] = '-';
                        } else {
                            if (isset($item['start_date']) && isset($item['end_date'])) {
                                $offerActive = $this->helper->datesAreCurrent($item['start_date'], $item['end_date']);
                                $basePrice = $offerActive !== 0 ? $item['special_price'] : '';
                                $item[$name] = $basePrice ? number_format($basePrice, 1, ',', '.') . '%' : '';
                            } else {
                                $item[$name] = number_format($item[$name], 1, ',', '.') . '%';
                            }
                        }
                    }
                }
            }
            elseif (
                $this->voucherHelper->getCurrentStoreCode() == CombustiblesHelper::CODE_STORE_AGREEMENT_FUELS ||
                $this->voucherHelper->getCurrentStoreCode() == CombustiblesRenewal202110::STORE_CODE
            ){
                foreach ($dataSource['data']['items'] as & $item) {
                    $name = $this->getData('name');

                    if (isset($item[$name])) {
                        if ($item['type_id'] == 'configurable') {
                            $item[$name] = '-';
                        } else {
                            if (isset($item['start_date']) && isset($item['end_date'])) {
                                //Combustibles no posee ofertas especiales.
                                $offerActive = $this->helper->datesAreCurrent($item['start_date'], $item['end_date']);
                                $basePrice = $offerActive !== 0 ? $item['special_price'] : '';
                                $item[$name] = $basePrice ? '$'. number_format($basePrice, 0, ',', '.'): '';
                            } else {
                                $item[$name] = '$'. number_format($item[$name], 0, ',', '.');
                            }
                        }
                    }
                }
            } elseif ($this->voucherHelper->getCurrentStoreCode() == VoucherSetupConstants::WEBSITE_CODE){
                foreach ($dataSource['data']['items'] as & $item) {
                    $name = $this->getData('name');

                    if (isset($item[$name])) {
                        if ($item['type_id'] == 'configurable') {
                            $item[$name] = '-';
                        } else {
                            if (isset($item['start_date']) && isset($item['end_date'])) {
                                $offerActive = $this->helper->datesAreCurrent($item['start_date'], $item['end_date']);
                                if ($this->_emergencyHelper->isEmergencySeller($item['seller_id'])) {
                                    $basePrice = $offerActive !== 0 ? ($item['special_price'] + $item['shipping_price']) : '';
                                } if ($this->mobiliarioHelper->isFurnitureSeller($item['seller_id'])) {
                                    $basePrice = $offerActive !== 0 ? ($item['special_price'] + $item['assembly_price']) : '';
                                }else {
                                    $basePrice = $offerActive !== 0 ? $item['special_price'] : '';
                                }
                                $item[$name] = $basePrice ? number_format($basePrice, 1, ',', '.') : '';
                            } else {
                                $item[$name] = number_format($item[$name], 1, ',', '.');
                            }
                        }
                    }
                }
            } else {
                foreach ($dataSource['data']['items'] as & $item) {
                    $name = $this->getData('name');

                    if (isset($item[$name])) {
                        if ($item['type_id'] == 'configurable') {
                            $item[$name] = '-';
                        } else {
                            if (isset($item['start_date']) && isset($item['end_date'])) {
                                $offerActive = $this->helper->datesAreCurrent($item['start_date'], $item['end_date']);
                                if ($this->_emergencyHelper->isEmergencySeller($item['seller_id'])) {
                                    $basePrice = $offerActive !== 0 ? ($item['special_price'] + $item['shipping_price']) : '';
                                } if ($this->mobiliarioHelper->isFurnitureSeller($item['seller_id'])) {
                                    $basePrice = $offerActive !== 0 ? ($item['special_price'] + $item['assembly_price']) : '';
                                }else {
                                    $basePrice = $offerActive !== 0 ? $item['special_price'] : '';
                                }
                                $item[$name] = $basePrice ? strip_tags($this->helper->currencyFormat($basePrice)) : '';
                            } else {
                                $item[$name] = strip_tags($this->helper->currencyFormat($item[$name]));
                            }
                        }
                    }
                }
            }
        }

        return $dataSource;
    }
}
