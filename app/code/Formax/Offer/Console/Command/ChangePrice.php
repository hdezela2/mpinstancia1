<?php

namespace Formax\Offer\Console\Command;

use Formax\Offer\Model\Offer;
use Linets\InsumosSetup\Model\InsumosConstants;
use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Formax\Offer\Helper\Data as Helper;

class ChangePrice extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @param \Magento\Framework\App\State $appState
     * @param Helper $helper
     * @param string $name
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        Helper $helper,
        $name = null
    ) {
        $this->helper = $helper;
        $this->appState = $appState;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('product:changefinalprice');
        $this->setDescription("Change final price of product by offer");

        parent::configure();
    }

    /**
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $startTime = microtime(true);

        $offerCollection = $this->helper->getOfferCollection();
        $offerCollectionSize = $offerCollection->getSize();

        $progressBar = new ProgressBar($output, $offerCollectionSize);
        $progressBar->start();

        foreach ($offerCollection->getItems() as $item) {
            $itemType = $item['item_type'];
            $websiteId = $item['website_id'];
            $itemWebsiteCode = $this->helper->getWebsiteCodeById($websiteId);
            if ($itemType == 'configurable') {
                $associatedId = $item['asso_id'];
                $specialPrice = $item['special_price'];
                if($itemWebsiteCode == InsumosConstants::WEBSITE_CODE && (int)$associatedId > 0 && (float)$specialPrice > 0){
                    $this->helper->changePriceOnConfigurableOfferInsumos($item);
                }elseif ((int)$associatedId > 0 && (float)$specialPrice > 0) {
                    $this->helper->changePriceOnConfigurableOffer($associatedId, $specialPrice);
                }
            } else {
                $itemId = $item['item_id'];
                $specialPrice = $item['special_price'];
                if ((int)$itemId > 0 && (float)$specialPrice > 0) {
                    if ($itemWebsiteCode == \Formax\Offer\Helper\Data::CONVENIO_VOUCHER_WEBSITE) {
                        if ((float)$specialPrice > (float)$item['base_price']) {
                            $this->helper->changePriceOnSimpleOffer($itemId, $item['special_price']);
                        }
                    } else if ( in_array($itemWebsiteCode, [\Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE, ConstantsEmergency202109::WEBSITE_CODE]) ) {
                        $price = $specialPrice + $item['item_shipping_price'];
                        $this->helper->changePriceOnSimpleOffer($itemId, $price);
                    } else if ($itemWebsiteCode == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) {
                        $price = $specialPrice + $item['item_assembly_price'];
                        $this->helper->changePriceOnSimpleOffer($itemId, $price);
                    } else {
                        $this->helper->changePriceOnSimpleOffer($itemId, $specialPrice);
                    }
                }
            }
            // update offer status to 3 (special price applied)
            $this->helper->updateStatusOffer($item, Offer::OFFER_APPLIED);
            $progressBar->advance();
        }

        $progressBar->finish();
        $resultTime = microtime(true) - $startTime;
        $output->writeln('');
        $output->writeln('Process was executed successfully in ' . gmdate('H:i:s', $resultTime));

        return Cli::RETURN_SUCCESS;
    }
}
