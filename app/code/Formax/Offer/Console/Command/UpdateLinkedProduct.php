<?php
namespace Formax\Offer\Console\Command;

use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Formax\Offer\Helper\Data as Helper;

class UpdateLinkedProduct extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * @var \Formax\AssignProduct\Model\LinkedProductFactory
     */
    protected $linkedProductCollectionFactory;
    private \Formax\AssignProduct\Model\ResourceModel\LinkedProduct\CollectionFactory $linkedProductOfferCollectionFactory;

    /**
     * UpdateLinkedProductOffer constructor.
     *
     * @param \Magento\Framework\App\State $appState
     * @param Helper $helper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Formax\AssignProduct\Model\ResourceModel\LinkedProduct\CollectionFactory $linkedProductOfferCollectionFactory
     * @param null $name
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        Helper $helper,
        \Psr\Log\LoggerInterface $logger,
        \Formax\Offer\Model\OfferFactory $offerFactory,
        \Formax\AssignProduct\Model\ResourceModel\LinkedProduct\CollectionFactory $linkedProductOfferCollectionFactory,
        $name = null
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->appState = $appState;
        $this->linkedProductOfferCollectionFactory = $linkedProductOfferCollectionFactory;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('product:updatelinkedproduct');
        $this->setDescription("Update Linked Products - price");

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);

        $collection = $this->linkedProductOfferCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('status', 0);


        foreach ($collection as $item)
        {
            try {
                $data = (array) json_decode($item->getJsonData());
                $serviceData = (array) json_decode($item->getJsonServiceData());

                $result = $this->helper->setLinkedProducts(
                    $item->getProductId(),
                    $item->getTriggerProductId(),
                    $data,
                    $item->getAssignId(),
                    $item->getSellerId(),
                    $serviceData,
                    $item->getWebsiteId(),
                    $item->getFlag()
                );
                $msg = isset($result['msg']) ? $result['msg'] : '';

                if (!$result['error']) {
                    $this->logger->info('Cron Task Success update linked product => ' . $msg);
                    $item->setStatus(1);
                    $item->save();
                } else {
                    $this->logger->critical('Cron Task Error update linked product => ' . $msg);
                }
                $output->writeln($msg);
            } catch (\Exception $e) {
                $info = 'Cron Task Error updating linked product => itemID:' .
                    $item->getId(). ' ProductId:'. $item->getProductId(). ' SellerID:'. $item->getSellerId();
                $this->logger->info($info);
                $this->logger->critical($info);
                $this->logger->critical('Exception message => ' . $e->getMessage());
                $item->setStatus(2);
                $item->save();
            }
        }

        return Cli::RETURN_SUCCESS;
    }
}


