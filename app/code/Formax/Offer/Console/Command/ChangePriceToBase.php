<?php

namespace Formax\Offer\Console\Command;

use Formax\Offer\Model\Offer;
use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Formax\Offer\Helper\Data as Helper;

class ChangePriceToBase extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @param \Magento\Framework\App\State $appState
     * @param Helper $helper
     * @param string $name
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        Helper $helper,
        $name = null
    ) {
        $this->helper = $helper;
        $this->appState = $appState;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('product:changepricetobase');
        $this->setDescription("Change final price of product by offer to base price");

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $startTime = microtime(true);

        $collection = $this->helper->getOfferCollectionForPriceToBase();

        if ($collection) {
            $collectionSize = sizeof($collection);
            $progressBar = new ProgressBar($output, $collectionSize);
            $progressBar->start();

            foreach ($collection as $item) {
                $websiteId = $item['website_id'];
                $itemWebsiteCode = $this->helper->getWebsiteCodeById($websiteId);
                if ($item['type'] == 'configurable') {
                    $assignId = $item['assign_id'];
                    $parentProductId = $item['parent_product_id'];
                    $productId = $item['product_id'];
                    $basePrice = $item['base_price'];
                    if($itemWebsiteCode == InsumosConstants::WEBSITE_CODE && (int)$assignId > 0 && (int)$parentProductId && (int)$productId && (float) $basePrice > 0){
                        $this->helper->updateConfigurableOfferPriceToBaseInsumos($item);
                    }elseif ((int)$assignId > 0 && (int)$parentProductId && (int)$productId && (float)$basePrice > 0) {
                        $this->helper->updateConfigurableOfferPriceToBase($basePrice, $assignId, $parentProductId, $productId);
                    }
                } else {
                    $assignId = $item['assign_id'];
                    $basePrice = $item['base_price'];
                    $specialPrice = $item['special_price'];
                    if ((int)$assignId > 0 && (float)$basePrice > 0) {
                        $this->helper->updateItemOfferPriceToBase($assignId, $basePrice, $specialPrice, $websiteId);
                    }
                }
                // update offer status to 4 (base price applied)
                $this->helper->updateStatusOffer($item, Offer::OFFER_EXPIRED);
                $progressBar->advance();
            }
            $progressBar->finish();
            $resultTime = microtime(true) - $startTime;
            $output->writeln('');
            $output->writeln('Process was executed successfully in ' . gmdate('H:i:s', $resultTime));
        } else {
            $output->writeln('No record was updated.');
        }

        return Cli::RETURN_SUCCESS;
    }
}
