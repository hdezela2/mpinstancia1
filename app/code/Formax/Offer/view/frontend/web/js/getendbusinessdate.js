define([
    'jquery',
    'underscore',
    "mage/calendar"
], function ($, _)
{
    function main(config) {

        var URL = config.ajaxUrl;
        var token = config.token;
        var currentYear = config.currentYear;
        var buttonImage = config.buttonImage;
        var minPublicationDays = config.minPublicationDays;

        let date1 = new Date();
        date1.setDate(date1.getDate() + minPublicationDays);

        $('#datepicker').datepicker({
            minDate: date1,
            defaultDate: "+" + minPublicationDays + "d",
            dateFormat:"yy-mm-dd",
            yearRange: currentYear + ":+1",
            buttonText: "",
            changeMonth: true,
            changeYear: true,
            showOn: "both",
            monthNames: ["Enero","Febrero","Marzo","Abril","May","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
            monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
            dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
            dayNamesShort: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"],
            dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"],
            beforeShowDay: $.datepicker.noWeekends,
            buttonImage: buttonImage
        });
        $('#publication_term').change(function(){
            var date = new Date();

            $.ajax({
                type: 'POST',
                url: URL,
                async: true,
                dataType: 'json',
                data : { 'token': token, 'startDate': function(){ return date.toISOString();}, 'days':  function(){ return parseInt($('#publication_term').val(), 10)+1;} },
                success:function (data) {
                    $('#datepicker').datepicker('option', {minDate: data.endDate});
                },
                error:function (e){
                    console.log(e);
                }
            });
        });


        // $(document).on('click','yourID_Or_Class',function() {
        //     var param = 'ajax=1';
        //     $.ajax({
        //         showLoader: true,
        //         url: URL,
        //         data: param,
        //         type: "POST",
        //         dataType: 'json'
        //     }).done(function (data) {
        //         $('#test').removeClass('hideme');
        //         var html = template('#test', {posts:data});
        //         $('#test').html(html);
        //     });
        // });
    }
    return main;
});
