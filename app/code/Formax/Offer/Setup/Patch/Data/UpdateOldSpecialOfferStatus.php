<?php
namespace Formax\Offer\Setup\Patch\Data;

use Formax\Offer\Model\Offer;
use Formax\Offer\Model\ResourceModel\Offer\CollectionFactory as OfferCollection;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class UpdateOldSpecialOfferStatus implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    protected $_moduleDataSetup;

    /** @var OfferCollection */
    protected $_offerCollection;

    /** @var TimezoneInterface */
    protected $_timezone;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        OfferCollection $_offerCollection,
        TimezoneInterface $timezone
    ) {
        $this->_moduleDataSetup = $moduleDataSetup;
        $this->_offerCollection = $_offerCollection;
        $this->_timezone = $timezone;
    }

    /**
     * We are going to UPDATE the status of all dccp_offer_product records where current date > end_date of the offer to 4
     */
    public function apply()
    {
        $this->_moduleDataSetup->getConnection()->startSetup();

        $connection = $this->_moduleDataSetup->getConnection();
        $currentDate = $connection->quote($this->_timezone->date()->format('Y-m-d'));

        // Filter offer collection
        $offerCollection = $this->_offerCollection->create();

        // check for all status 1
        $offerCollection->addFieldToFilter('status', Offer::OFFER_CREATED);

        // check for all offers that already expired
        $dateDiff = 'DATEDIFF('. $currentDate .', DATE(end_date)) > 0';
        $offerCollection->getSelect()->where($dateDiff);

        if ($offerCollection->getSize() > 0) {
            foreach ($offerCollection as $item) {
                $item->setStatus(Offer::OFFER_EXPIRED);
            }
            $offerCollection->save();
        }

        $this->_moduleDataSetup->getConnection()->endSetup();

        return $this;
    }

    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [];
    }
}
