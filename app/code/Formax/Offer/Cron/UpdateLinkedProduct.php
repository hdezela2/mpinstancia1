<?php

namespace Formax\Offer\Cron;

use Formax\Offer\Helper\Data as Helper;

class UpdateLinkedProduct
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * @var \Formax\AssignProduct\Model\LinkedProductFactory
     */
    protected $linkedProductCollectionFactory;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        Helper $helper,
        \Formax\AssignProduct\Model\ResourceModel\LinkedProduct\CollectionFactory $linkedProductCollectionFactory
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->linkedProductCollectionFactory = $linkedProductCollectionFactory;
    }

    public function execute()
    {
        $collection = $this->linkedProductCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('status', 0);

        foreach ($collection as $item)
        {
            try{

                $data = (array) json_decode($item->getJsonData());
                $serviceData = (array) json_decode($item->getJsonServiceData());

                $result = $this->helper->setLinkedProducts(
                    $item->getProductId(),
                    $item->getTriggerProductId(),
                    $data,
                    $item->getAssignId(),
                    $item->getSellerId(),
                    $serviceData,
                    $item->getWebsiteId(),
                    $item->getFlag()
                );
                $msg = isset($result['msg']) ? $result['msg'] : '';

                if (!$result['error']) {
                    $this->logger->info('Cron Task Success update linked product offer => ' . $msg);
                    $item->setStatus(1);
                    $item->save();
                } else {
                    $this->logger->critical('Cron Task Error update linked product offer => ' . $msg);
                }
            } catch (\Exception $e) {
                $info = 'Cron Task Error updating linked product offer => itemID:' .
                    $item->getId(). ' ProductId:'. $item->getProductId(). ' SellerID:'. $item->getSellerId();
                $this->logger->info($info);
                $this->logger->critical($info);
                $this->logger->critical('Exception message => ' . $e->getMessage());
                $item->setStatus(2);
                $item->save();
            }
        }
    }
}