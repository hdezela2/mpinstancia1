<?php

namespace Formax\Offer\Cron;

use Formax\Offer\Helper\Data as Helper;

class SetOffer
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param Helper $helper
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        Helper $helper
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
    }

    public function execute()
    {
        $result = $this->helper->changePrices();
        $msg = isset($result['msg']) ? $result['msg'] : '';

        if (!$result['error']) {
            $this->logger->info('Cron Task Success set special price => ' . $msg);
        } else {
            $this->logger->critical('Cron Task Error set special price => ' . $msg);
        }
    }
}
