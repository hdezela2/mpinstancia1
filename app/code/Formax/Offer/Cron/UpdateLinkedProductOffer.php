<?php
/**
 * @namespace   Formax
 * @module      Offer
 * @author      Dario Grau
 * @email       dgrau@cleveritgroup.com
 * @date        07/07/2020
 */

namespace Formax\Offer\Cron;

use Formax\Offer\Helper\Data as Helper;

class UpdateLinkedProductOffer
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * @var \Formax\Offer\Model\OfferFactory
     */
    protected $_offerFactory;

    /**
     * @var \Formax\Offer\Model\LinkedProductOfferFactory
     */
    protected $_linkedProductOfferCollectionFactory;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param Helper $helper
     * @param \Formax\Offer\Model\OfferFactory $offerFactory
     * @param \Formax\Offer\Model\LinkedProductOfferFactory $linkedProductOfferCollectionFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        Helper $helper,
        \Formax\Offer\Model\OfferFactory $offerFactory,
        \Formax\Offer\Model\ResourceModel\LinkedProductOffer\CollectionFactory $linkedProductOfferCollectionFactory
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->_offerFactory = $offerFactory;
        $this->_linkedProductOfferCollectionFactory = $linkedProductOfferCollectionFactory;
    }

    public function execute()
    {
        $collection = $this->_linkedProductOfferCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('status', 0);

        foreach ($collection as $item) {
            try{

                $productId = $item->getProductId();
                $offerId = $item->getOfferId();
                $sellerId = $item->getSellerId();
                $parentProductId = $item->getParentProductId();
                $assignId = $item->getAssignId();
                $websiteId = $item->getWebsiteId();
                $startDate = $item->getStartDate();
                $endDate = $item->getEndDate();
                $days = $item->getDays();
                $specialPrice = $item->getSpecialPrice();
                $basePrice = $item->getBasePrice();

                $result = $this->helper->setLinkedProductsOffer($offerId, $parentProductId, $productId, $assignId, $sellerId, $websiteId, $startDate, $endDate, $days, $specialPrice, $basePrice);
                $msg = isset($result['msg']) ? $result['msg'] : '';

                if (!$result['error']) {
                    $this->logger->info('Cron Task Success update linked product offer => ' . $msg);
                    $item->setStatus(1);
                    $item->save();
                } else {
                    $this->logger->critical('Cron Task Error update linked product offer => ' . $msg);
                }
            } catch (\Exception $e) {
                $info = 'Cron Task Error updating linked product offer => itemID:' .
                    $item->getId(). ' ProductId:'. $item->getProductId(). ' SellerID:'. $item->getSellerId();
                $this->logger->info($info);
                $this->logger->critical($info);
                $this->logger->critical('Exception message => ' . $e->getMessage());
                $item->setStatus(2);
                $item->save();
            }

        }
    }
}
