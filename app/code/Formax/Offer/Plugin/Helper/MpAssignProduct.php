<?php

namespace Formax\Offer\Plugin\Helper;

use Magento\Framework\Session\SessionManager;
use Formax\Offer\Helper\Data as Helper;
use Formax\Offer\Model\ResourceModel\Offer\CollectionFactory as OfferCollection;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\App\ResourceConnection;

class MpAssignProduct
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var OfferCollection
     */
    protected $offerCollection;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * @var ResourceConnection
     */
    protected $resource;
    /**
     * @var Magento\Framework\Session\SessionManager|SessionManager
     */
    private $_coreSession;

    /**
     * Initialize dependencies.
     *
     * @param Magento\Framework\Session\SessionManager $coreSession
     * @param Formax\Offer\Helper\Data $helper
     * @param Formax\Offer\Model\ResourceModel\Offer\CollectionFactory $collection
     * @param Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        SessionManager $coreSession,
        Helper $helper,
        OfferCollection $offerCollection,
        TimezoneInterface $timezone,
        ResourceConnection $resource
    ) {
        $this->resource = $resource;
        $this->timezone = $timezone;
        $this->helper = $helper;
        $this->offerCollection = $offerCollection;
        $this->_coreSession = $coreSession;
    }

    /**
     * @param \Webkul\MpAssignProduct\Helper\Data $subject
     * @param \Closure $proceed
     * @param Object $quote
     */
    public function aroundCollectTotals(
        \Webkul\MpAssignProduct\Helper\Data $subject,
        \Closure $proceed,
        $quote
    ) {
        $childAssignId = 0;
        foreach ($quote->getAllVisibleItems() as $item) {
            $product = $item->getProduct();
            $productType = $product->getTypeId();
            $itemId = $item->getId();
            $assignData = $subject->getAssignDataByItemId($itemId);
            $mpassignItemId = (int)$this->_coreSession->getMpAssignItemId();

            if ($mpassignItemId) {
                $assignData = $subject->getAssignDataByItemId($mpassignItemId);
            }

            if ($assignData['assign_id'] > 0) {
                $assignId = $assignData['assign_id'];
                if ($assignData['child_assign_id'] > 0) {
                    $childAssignId = $assignData['child_assign_id'];
                    $price = $subject->getAssocitePrice($assignId, $childAssignId);
                } else {
                    $price = $subject->getAssignProductPrice($assignId);
                }

                $productId = isset($assignData['product_id']) ? $assignData['product_id'] : 0;
                $offer = $this->getOffer($assignData['assign_id'], $productType, $childAssignId);
                $basePrice = $offer === false ? $price : $offer->getBasePrice();
                
                $price = $subject->getFinalPrice($price);
                $item->setOriginalPrice($basePrice);
                $item->setBaseOriginalPrice($basePrice);
                $item->setCustomPrice($price);
                $item->setOriginalCustomPrice($basePrice);
                $item->setRowTotal($item->getQty()*$price);
                $product->setIsSuperMode(true);
            }
        }

        return $proceed($quote);
    }

    /**
     * @param int $assignId
     * @param string $productType
     * @param int $assignAssociatedId
     * @return mixed bool/Object
     */
    private function getOffer($assignId, $productType, $assignAssociatedId = 0)
    {
        if ($assignId) {
            $connection = $this->resource->getConnection();
            $currentDate = $connection->quote($this->timezone->date()->format('Y-m-d'));
            $collection = $this->offerCollection->create();

            if ($productType == 'configurable') {
                $tableAssociated = $this->resource->getTableName('marketplace_assignproduct_associated_products');
                $collection->getSelect()->join(
                    ['asso' => $tableAssociated],
                    'main_table.product_id = asso.product_id AND main_table.assign_id = asso.parent_id',
                    []
                );
                $collection->addFieldToFilter('asso.id', $assignAssociatedId);
            }

            $collection->addFieldToFilter('assign_id', $assignId)
                        ->addFieldToFilter('main_table.status', 1)
                        ->addFieldToFilter('special_price', ['lt' => new \Zend_Db_Expr('main_table.base_price')])
                        ->addFieldToFilter(new \Zend_Db_Expr($currentDate), ['gteq' => new \Zend_Db_Expr('DATE(start_date)')])
                        ->addFieldToFilter(new \Zend_Db_Expr($currentDate), ['lteq' => new \Zend_Db_Expr('DATE(end_date)')])
                        ->setPageSize(1);

            if ($collection) {
                foreach ($collection as $item) {
                    return $item;
                }
            }
        }

        return false;
    }
}
