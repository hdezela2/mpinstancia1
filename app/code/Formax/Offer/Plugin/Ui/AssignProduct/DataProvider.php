<?php

namespace Formax\Offer\Plugin\Ui\AssignProduct;

use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemCollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Formax\AssignProduct\Ui\DataProvider\AssignProduct
{
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string  $requestFieldName
     * @param MarketplaceHelper $marketplaceHelper
     * @param ItemCollectionFactory $itemCollectionFactory
     * @param EavAttribute $eavAttribute
     * @param StoreManagerInterface $storeManager
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        MarketplaceHelper $marketplaceHelper,
        ItemCollectionFactory $itemCollectionFactory,
        EavAttribute $eavAttribute,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->_storeManager = $storeManager;

        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $marketplaceHelper,
            $itemCollectionFactory,
            $eavAttribute,
            $meta,
            $data
        );

        $customerId = $this->_marketplaceHelper->getCustomerId();
        $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
        $storeId = (int)$this->_storeManager->getStore()->getId();
        $attrProductName = $this->_eavAttribute->getIdByCode('catalog_product', 'name');
        $attrProductImage = $this->_eavAttribute->getIdByCode('catalog_product', 'image');
        $sellercollection = $this->_itemCollectionFactory->create();
        $sellercollection->addFieldToSelect('id')
            ->addFieldToSelect('product_row_id')
            ->addFieldToSelect('price')
            ->addFieldToSelect('description')
            ->addFieldToSelect('qty')
            ->addFieldToSelect('comment')
            ->addFieldToSelect('has_offer')
            ->addFieldToSelect('dis_dispersion')
            ->addFieldToSelect('shipping_price')
            ->addFieldToSelect('assembly_price')
            ->addFieldToSelect('seller_id')
            ->addFieldToSelect('status');

        $sellercollection->addFieldToFilter('main_table.seller_id', $customerId)
            ->getSelect()->join(
                ['e' => $sellercollection->getResource()->getTable('catalog_product_entity')],
                'main_table.product_id = e.entity_id',
                ['sku', 'type_id']
            )->join(
                ['cpev' => $sellercollection->getResource()->getTable('catalog_product_entity_varchar')],
                'e.row_id = cpev.row_id AND cpev.store_id = 0 AND cpev.attribute_id = ' . $attrProductName,
                ['name' => 'cpev.value']
            )->joinLeft(
                ['cpevImg' => $sellercollection->getResource()->getTable('catalog_product_entity_varchar')],
                'e.row_id = cpevImg.row_id AND cpevImg.store_id = ' .  $storeId . ' AND cpevImg.attribute_id = ' . $attrProductImage,
                ['image' => 'cpevImg.value']
            )->joinLeft(
                ['doc' => $sellercollection->getResource()->getTable('dccp_assign_product')],
                'main_table.product_id = doc.product_id AND main_table.seller_id = doc.seller_id',
                ['file', 'file_aseo', 'file_anexo']
            )->joinLeft(
                ['of' => $sellercollection->getResource()->getTable('dccp_offer_product')],
                'main_table.product_id = of.product_id AND of.parent_product_id = of.product_id AND main_table.seller_id = of.seller_id AND of.website_id = '. $websiteId,
                [
                    'start_date' => new \Zend_Db_Expr('DATE(start_date)'),
                    'end_date' => new \Zend_Db_Expr('DATE(end_date)'),
                    'special_price',
                    'base_price'
                ]
            );
        $websiteCodesAllowed = [
            \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE,
            \Intellicore\EmergenciasRenewal\Constants::WEBSITE_CODE,
            AseoRenewalConstants::WEBSITE_CODE];
        if (in_array($this->_storeManager->getWebsite()->getCode(),$websiteCodesAllowed)) {
            $sellercollection->getSelect()->joinLeft(
                ['asso' => 'marketplace_assignproduct_associated_products'],
                'main_table.id = asso.parent_id',
                [
                    'pending_children_count' => new \Zend_Db_Expr('SUM(CASE WHEN asso.status = 2 THEN 1 ELSE 0 END)'),
                    'approved_children_count' => new \Zend_Db_Expr('SUM(CASE WHEN asso.status = 1 THEN 1 ELSE 0 END)'),
                    'disapproved_children_count' => new \Zend_Db_Expr('SUM(CASE WHEN asso.status = 0 THEN 1 ELSE 0 END)'),
                    'disabled_children_count' => new \Zend_Db_Expr('SUM(CASE WHEN NOT asso.status IN (0,1,2) THEN 1 ELSE 0 END)')
                ]
            );
        }

        $sellercollection->getSelect()->group('main_table.id');

        $sellercollection->addFilterToMap('id', 'main_table.id');
        $sellercollection->addFilterToMap('name', 'cpev.value');
        $sellercollection->addFilterToMap('has_offer', 'main_table.has_offer');
        $sellercollection->addFilterToMap('seller_id', 'main_table.seller_id');
        $sellercollection->addFilterToMap('status', 'main_table.status');
        $sellercollection->setOrder('main_table.created_at', 'desc');
        $this->collection = $sellercollection;
    }

}
