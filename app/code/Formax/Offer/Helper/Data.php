<?php
/**
 * Formax Software.
 *
 * @category Formax
 * @package  Formax_Offer
 * @author   Formax
 */
namespace Formax\Offer\Helper;

use Formax\Offer\Model\Offer;
use Formax\Offer\Model\ResourceModel\Offer\CollectionFactory as OfferCollection;
use Linets\InsumosSetup\Model\InsumosConstants;
use Linets\VehiculosSetUp\Model\Constants as VehiculosConstants;
use Linets\VoucherSetup\Model\Constants as LinetsVoucherConstants;
use Linets\VoucherSetup\Model\Constants as VoucherSetupConstants;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product as productResourceModel;
use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreRepository;
use Summa\CombustiblesSetUp\Helper\Data as CombustiblesHelper;
use Chilecompra\CombustiblesRenewal\Model\Constants as CombustiblesRenewal202110;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollection;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates as AssociatesResourceModel;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemsCollection;
use Linets\GasSetup\Constants as GasConstants;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var int
     */
    const CUSTOM_ERROR_CODE = 555;

    /**
     * @var int
     */
    const MINIMUM_DAY_OFFER = 2;

    /**
     * @var int
     */
    const MAXIMUM_DAY_OFFER = 7;

    /**
     * @var int
     */
    const MINIMUM_PERCENT_OFFER = 5;

    /**
     * @var string
     */
    const CONVENIO_VOUCHER_WEBSITE = 'convenio_voucher';

    /**
     * @var string
     */
    const CONVENIO_VOUCHER = 'voucher';

    const XML_PATH_BUSINESS_DAYS = 'catalog/dccp_offer/business_days';

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var array
     */
    private $postData = null;

    /**
     * @var ItemsCollection
     */
    protected $_itemsCollection;

    /**
     * @var AssociatesCollection
     */
    protected $_associatesCollection;

    /**
     * @var OfferCollection
     */
    protected $_offerCollection;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var \ Magento\Framework\View\Asset\Repository
     */
    protected $_viewAsset;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Webkul\SellerSubAccount\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timezone;

    /**
     * @var int
     */
    protected $_websiteId;

    /**
     * @var Magento\Store\Model\StoreRepository
     */
    protected $_storeRepository;

    /**
     * @var PricingHelper
     */
    protected $_pricingHelper;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_httpClientFactory;

    /**
     * @var \Formax\Offer\Logger\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var EavAttribute
     */
    protected $_eavAttribute;

    /**
     * @var \Webkul\MpAssignProduct\Model\Associates
     */
    public $_associates;

    /**
     * @var Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    protected $_emergencyHelper;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $_assignHelper;

    /**
     * @var \Formax\Offer\Model\OfferFactory
     */
    protected $_offerFactory;

    /**
     * @var \Webkul\MpAssignProduct\Model\ItemsFactory
     */
    protected $_itemsFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepositoryInterface;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var \Summa\MobiliarioSetUp\Helper\Data
     */
    protected $helperMobiliario;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollectionFactory;
    /**
     * @var ProductFactory
     */
    private $productFactory;
    /**
     * @var productResourceModel
     */
    private $productResourceModel;
    /**
     * @var DispatchPriceManagementInterface
     */
    private $dispatchPriceManagement;
    /**
     * @var AssociatesResourceModel
     */
    private $associatesResourceModel;


    /**
     * __construct
     *
     * @param \Magento\Framework\App\Helper\Context                     $context
     * @param \Magento\Store\Model\StoreManagerInterface                $storeManager
     * @param \Magento\Framework\App\ResourceConnection                 $resource
     * @param \Magento\Framework\View\Asset\Repository                  $viewAsset
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface      $timezone
     * @param \Webkul\SellerSubAccount\Helper\Data                      $helper
     * @param \Magento\Framework\HTTP\ZendClientFactory                 $httpClientFactory
     * @param \Magento\Framework\Json\Helper\Data                       $jsonHelper
     * @param \Magento\Customer\Model\Session                           $customerSession
     * @param \Formax\Offer\Logger\Logger                               $logger
     * @param \Magento\Framework\Registry                               $registry
     * @param ItemsCollection                                           $itemsCollectionFactory
     * @param AssociatesCollection                                      $associatesCollectionFactory
     * @param OfferCollection                                           $offerCollectionFactory
     * @param \Webkul\MpAssignProduct\Model\AssociatesFactory           $associates
     * @param PricingHelper                                             $pricingHelper
     * @param EavAttribute                                              $eavAttribute
     * @param \Magento\Customer\Model\CustomerFactory                   $customerFactory
     * @param \Summa\EmergenciasSetUp\Helper\Data                       $emergencyHelper
     * @param \Webkul\MpAssignProduct\Helper\Data                       $assignHelper
     * @param \Formax\Offer\Model\OfferFactory                          $offerFactory
     * @param \Webkul\MpAssignProduct\Model\ItemsFactory                $itemsFactory
     * @param \Formax\Offer\Model\ResourceModel\Offer\CollectionFactory $offerCollection
     * @param \Magento\Catalog\Api\ProductRepositoryInterface           $productRepositoryInterface
     * @param \Magento\Framework\Api\SearchCriteriaBuilder              $searchCriteriaBuilder
     * @param \Magento\Customer\Api\CustomerRepositoryInterface         $customerRepositoryInterface
     * @param \Summa\MobiliarioSetUp\Helper\Data                        $helperMobiliario
     * @param ProductCollectionFactory                                  $productCollectionFactory
     * @param ProductFactory                                            $productFactory
     * @param productResourceModel                                      $productResourceModel
     * @param DispatchPriceManagementInterface                          $dispatchPriceManagement
     * @param AssociatesResourceModel                                   $associatesResourceModel
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\View\Asset\Repository $viewAsset,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Webkul\SellerSubAccount\Helper\Data $helper,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Formax\Offer\Logger\Logger $logger,
        \Magento\Framework\Registry $registry,
        ItemsCollection $itemsCollectionFactory,
        AssociatesCollection $associatesCollectionFactory,
        OfferCollection $offerCollectionFactory,
        \Webkul\MpAssignProduct\Model\AssociatesFactory $associates,
        PricingHelper $pricingHelper,
        EavAttribute $eavAttribute,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Summa\EmergenciasSetUp\Helper\Data $emergencyHelper,
        \Webkul\MpAssignProduct\Helper\Data $assignHelper,
        \Formax\Offer\Model\OfferFactory $offerFactory,
        \Webkul\MpAssignProduct\Model\ItemsFactory $itemsFactory,
        \Formax\Offer\Model\ResourceModel\Offer\CollectionFactory $offerCollection,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Summa\MobiliarioSetUp\Helper\Data $helperMobiliario,
        ProductCollectionFactory $productCollectionFactory,
        ProductFactory $productFactory,
        productResourceModel $productResourceModel,
        DispatchPriceManagementInterface $dispatchPriceManagement,
        AssociatesResourceModel $associatesResourceModel
    ) {
        $this->_customerSession = $customerSession;
        $this->_registry = $registry;
        $this->_timezone = $timezone;
        $this->_helper = $helper;
        $this->_resource = $resource;
        $this->_storeManager = $storeManager;
        $this->_pricingHelper = $pricingHelper;
        $this->_viewAsset = $viewAsset;
        $this->_httpClientFactory = $httpClientFactory;
        $this->_logger = $logger;
        $this->_jsonHelper = $jsonHelper;
        $this->_eavAttribute = $eavAttribute;
        $this->_itemsCollection = $itemsCollectionFactory;
        $this->_associatesCollection = $associatesCollectionFactory;
        $this->_offerCollection = $offerCollectionFactory;
        $this->_associates = $associates;
        $this->_websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
        $this->_request = $context->getRequest();
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_emergencyHelper = $emergencyHelper;
        $this->_customerFactory = $customerFactory;
        $this->_assignHelper = $assignHelper;
        $this->_offerFactory = $offerFactory;
        $this->_itemsFactory = $itemsFactory;
        $this->_offerCollection = $offerCollection;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->helperMobiliario = $helperMobiliario;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productFactory = $productFactory;
        $this->productResourceModel = $productResourceModel;
        $this->dispatchPriceManagement = $dispatchPriceManagement;
        $this->associatesResourceModel = $associatesResourceModel;
        parent::__construct($context);
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function shouldRenderAddProductLink()
    {
        $forbiddenSites =
            [
                \Summa\CombustiblesSetUp\Helper\Data::WEBSITE_CODE,
                \Chilecompra\CombustiblesRenewal\Model\Constants::WEBSITE_CODE
            ];
        return !in_array($this->_storeManager->getStore()->getCode(), $forbiddenSites);
    }

    /**
     * Get Assign Products
     *
     * @param int $assignId
     *
     * @return collection object
     */
    public function getAssignOfferProducts($assignId)
    {
        $sellerId = $this->getSellerId();
        $collection = $this->_itemsCollection->create();
        $joinTable = $this->_resource
            ->getTableName('marketplace_userdata');
        $sql = 'mp.seller_id = main_table.seller_id';
        $sql .= ' and main_table.id = ' . $assignId;
        $sql .= ' and mp.is_seller = 1';
        // $sql .= ' and mp.store_id = 0';
        // $sql .= ' and main_table.status = 1';
        $fields = ['seller_id', 'is_seller', 'shop_url', 'shop_title'];

        $leftTableOffer = $this->_resource->getTableName('dccp_offer_product');
        $sqlOffer = 'main_table.seller_id = of.seller_id';
        $sqlOffer .= ' and main_table.product_id = of.parent_product_id';
        $sqlOffer .= ' and of.website_id = '. $this->_websiteId;
        $sqlOffer .= ' and (main_table.status = 1 OR main_table.type = \'configurable\')';
        $fieldsOffer = [
            'offer_id' => 'id',
            'start_date' => new \Zend_Db_Expr('DATE(start_date)'),
            'end_date' => new \Zend_Db_Expr('DATE(end_date)'),
            'days',
            'base_price',
            'special_price',
            'new_base_price' => 'base_price'
        ];

        $collection->getSelect()->join($joinTable.' as mp', $sql, $fields);
        $collection->getSelect()->joinLeft($leftTableOffer.' as of', $sqlOffer, $fieldsOffer);
        $collection->addFieldToFilter('main_table.seller_id', $sellerId);
        $collection->addFilterToMap('seller_id', 'main_table.seller_id');
        $collection->addFilterToMap('product_id', 'main_table.product_id');
        $collection->addFilterToMap('status', 'main_table.status');
        $collection->addFilterToMap('base_price', 'main_table.base_price');
        $collection->setPageSize(1);
        $collection->distinct(true);

        if ($collection) {
            foreach ($collection as $item) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Get Associates Data
     *
     * @param int $assignId
     * @return array
     */
    public function getAssociatesData($assignId)
    {
        $result = [];
        $sellerId = $this->getSellerId();
        $attrProductName = $this->_eavAttribute->getIdByCode('catalog_product', 'name');

        $associatesCollection = $this->_associatesCollection->create()
            ->addFieldToFilter('main_table.parent_id', $assignId)
            ->addFieldToFilter('main_table.qty', ['gt' => 0])
            ->addFieldToFilter('main_table.status', 1);

        $leftTableOffer = $this->_resource->getTableName('dccp_offer_product');
        $sqlOffer = 'main_table.product_id = of.product_id';
        $sqlOffer .= ' and of.product_id != of.parent_product_id';
        $sqlOffer .= ' and of.website_id = '. $this->_websiteId;
        if ($sellerId) {
            $sqlOffer .= ' and of.seller_id = '. $sellerId;
        }
        $offerStatusFilter = [Offer::OFFER_APPLIED];
        if (in_array($this->getStoreCode(),[GasConstants::GAS_WEBSITE_CODE,InsumosConstants::WEBSITE_CODE])) {
            $offerStatusFilter[] = Offer::OFFER_CREATED;
        }
        $sqlOffer .= ' and of.status IN (' . implode(',', $offerStatusFilter) . ')';

        $fieldsOffer = [
            'offer_id' => 'id',
            'start_date' => new \Zend_Db_Expr('DATE(start_date)'),
            'end_date' => new \Zend_Db_Expr('DATE(end_date)'),
            'days',
            'base_price',
            'special_price',
            'offer_status' => 'status'
        ];

        $joinTableCatalog = $this->_resource->getTableName('catalog_product_entity');
        $sqlCatalog = 'main_table.product_id = e.entity_id';
        $fieldsCatalog = ['row_id', 'sku'];

        $joinTableCatalogVarchar = $this->_resource->getTableName('catalog_product_entity_varchar');
        $sqlCatalogVarchar = 'e.row_id = cpev.row_id';
        $sqlCatalogVarchar .= ' and cpev.attribute_id = ' . $attrProductName;
        $sqlCatalogVarchar .= ' and cpev.store_id = 0';
        $fieldsCatalogVarchar = ['name' => 'value'];

        $associatesCollection->getSelect()
            ->joinLeft($leftTableOffer . ' as of', $sqlOffer, $fieldsOffer)
            ->join($joinTableCatalog . ' as e', $sqlCatalog, $fieldsCatalog)
            ->join($joinTableCatalogVarchar . ' as cpev', $sqlCatalogVarchar, $fieldsCatalogVarchar);
        foreach ($associatesCollection as $item) {
            $info = [
                'offer_id' => $item->getOfferId(),
                'qty' => $item->getQty(),
                'price' => $item->getPrice(),
                'start_date' => $item->getStartDate(),
                'end_date' => $item->getEndDate(),
                'days' => $item->getDays(),
                'base_price' => $item->getBasePrice(),
                'special_price' => $item->getSpecialPrice(),
                'name' => $item->getName(),
                'offer_status' => $item->getOfferStatus()
            ];
            $result[$item->getProductId()] = $info;
        }

        return $result;
    }

    /**
     * get formatted price
     *
     * @param float $price
     * @return string
     */
    public function currencyFormat($price)
    {
        if (is_numeric($price)) {
            return $this->_pricingHelper->currency($price, true, false);
        }

        return $price;
    }

    /**
     * Retrive url calendar image
     *
     * @return string
     */
    public function getImageCalendar()
    {
        $params = array('_secure' => $this->_request->isSecure());
        return $this->_viewAsset->getUrlWithParams('Magento_Theme::calendar.png', $params);
    }

    /**
     * Get Show minimum days to offer setting config
     *
     * @return int
     */
    public function showMinimumDayOffer($storeId = null)
    {
        $config = 'catalog/dccp_offer/ini_range_offer';
        $value = $this->_scopeConfig->getValue($config, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        if ($value == '') {
            return self::MINIMUM_DAY_OFFER;
        }

        return (int)$value;
    }

    /**
     * Get Show maximum days to offer setting config
     *
     * @return int
     */
    public function showMaximumDayOffer($storeId = null)
    {
        $config = 'catalog/dccp_offer/end_range_offer';
        $value = $this->_scopeConfig->getValue($config, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        if ($value == '') {
            return self::MAXIMUM_DAY_OFFER;
        }

        return (int)$value;
    }

    /**
     * Get Show minimum percent to offer setting config
     *
     * @return int
     */
    public function showMinimumPercentOffer($storeId = null)
    {
        $config = 'catalog/dccp_offer/min_percent';
        $value = $this->_scopeConfig->getValue($config, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        if ($value == '') {
            return self::MINIMUM_PERCENT_OFFER;
        }

        return (float)$value;
    }

    /**
     * Validate if dates are active
     *
     * @param string $startDate
     * @param string $endDate
     * @return int
     */
    public function datesAreCurrent($startDate, $endDate)
    {
        $ini = new \DateTime($startDate);
        $end = new \DateTime($endDate);
        $current = $this->_timezone->date();
        $result = 0;

        if (!empty($startDate) && !empty($endDate)) {
            //Offer is active
            if ($current->format('Y-m-d') >= $ini->format('Y-m-d') &&
                $current->format('Y-m-d') <= $end->format('Y-m-d')) {
                $result = 1;
            }

            //Offer doesn't start yet
            if ($current->format('Y-m-d') < $ini->format('Y-m-d')) {
                $result = 2;
            }
        }

        return $result;
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }

    /**
     * Get value from POST by key
     *
     * @param string $key
     * @param int $index
     * @return string
     */
    public function getPostValue($key, $index = -1)
    {
        if (null === $this->postData) {
            $this->postData = (array) $this->getDataPersistor()->get('dccp_offer');
            $this->getDataPersistor()->clear('dccp_offer');
        }

        if ($index < 0) {
            if (isset($this->postData[$key])) {
                return (string) $this->postData[$key];
            }
        } else {
            if (isset($this->postData['products'][$index][$key])) {
                return (string) $this->postData['products'][$index][$key];
            }
        }

        return '';
    }

    /**
     * Retrive next day
     *
     * @return \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    public function getNextDay()
    {
        return $this->_timezone->date()->add(new \DateInterval('P1D'));
    }

    /**
     * Retrive minimum special price
     *
     * @param int $basePrice
     * @param bool $positive [optional]
     * @return int
     */
    public function getMinSpecialPrice($basePrice, $positive = false, $storeId = null)
    {
        $currencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrency()->getCode();

        $minSpecialPrice = 0.0;

        if ($basePrice > 0) {
            $percent = $this->showMinimumPercentOffer($storeId);

            if ($positive) {
                $minSpecialPrice = $basePrice + (($basePrice * $percent) / 100.0);
            } else {
                $minSpecialPrice = $basePrice - (($basePrice * $percent) / 100.0);
            }

            if($currencyCode == "CLP") {
                $minSpecialPrice = round($minSpecialPrice, 0);
            } else if ($currencyCode == "USD") {
                $minSpecialPrice = round($minSpecialPrice, 2);
            }
        }

        return $minSpecialPrice;
    }

    /**
     * Validate if data fields ara correct
     *
     * @param float $basePrice
     * @param float $specialPrice
     * @param string $startDate
     * @param string $originStartDate
     * @param int $days
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param string $location
     * @param \Magento\Catalog\Api\Data\ProductInterface|null $simpleProduct
     * @return array
     */
    public function validateOffer($basePrice, $specialPrice, $originStartDate, $startDate, $days, $product, $location = '', $simpleProduct = null)
    {
        $result = ['error' => false, 'msg' => ''];

        try {

            $validator = new \Zend\Validator\GreaterThan(['min' => 0]);
            if (!$validator->isValid($basePrice)) {
                $msg = __('Field %1 must be only numbers greather than zero. %2', __('Base Price'), $location);
                throw new LocalizedException($msg, new \Exception());
            }

            $validator = new \Zend\Validator\GreaterThan(['min' => 0]);
            if (!$validator->isValid($specialPrice)) {
                $msg = __('Field %1 must be only numbers greather than zero. %2', __('Special Price'), $location);
                throw new LocalizedException($msg, new \Exception());
            }

            $minPercent = $this->showMinimumPercentOffer();
            $isBonification = $product->getData('voucher_product_type');
            $storeCode = $this->getStoreCode();
            if ($storeCode == self::CONVENIO_VOUCHER && ((int)$isBonification === 1 || (int)$isBonification === 2)) {
                $minSpecialPrice = $this->getMinSpecialPrice($basePrice, true);
                if ((float) $specialPrice < $minSpecialPrice) {
                    $msg = (int) $isBonification === 1
                        ? __('Field %1 must be equal or greather than %2%% bonification. %3', __('Special Percent'), $minPercent, $location)
                        : __('Field %1 must be equal or greather than %2%% off. %3', __('Special Percent'), $minPercent, $location);
                    throw new LocalizedException($msg, new \Exception());
                }
            } elseif ($storeCode == LinetsVoucherConstants::STORE_VIEW_CODE && ((int)$isBonification === 1 || (int)$isBonification === 2)) {
                if ((float) $specialPrice < $basePrice) {
                    $msg = __('Field %1 must be equal or greather than %2%% off. %3', __('Special Percent'), $basePrice, $location);
                    throw new LocalizedException($msg, new \Exception());
                }
            } elseif (
                $this->getStoreCode() == CombustiblesHelper::CODE_STORE_AGREEMENT_FUELS ||
                $this->getStoreCode() == CombustiblesRenewal202110::STORE_CODE
            ){
                //Combustibles no posee ofertas especiales. Componente desactivado
                $msg = __('The agreement does not have this functionality defined. %1', $location);
                throw new LocalizedException($msg, new \Exception());
            } elseif ($this->getStoreCode() == VoucherSetupConstants::WEBSITE_CODE){
                $__specialPrice = $this->getMinSpecialPrice($basePrice, true);
                if ($specialPrice <= $__specialPrice) {
                    $msg = __('Field %1 must be equal or greather than %2%% off. %3', __('Special Price'), $__specialPrice, $location);
                    throw new LocalizedException($msg, new \Exception());
                }
            } elseif ($product->getTypeId()=='configurable' && $this->getStoreCode() == VehiculosConstants::STORE_VIEW_CODE && $simpleProduct){
                $basePrice = $this->getVehiculosBasePrice($simpleProduct, $this->getSellerId());
                $maxSpecialPrice = $this->getMinSpecialPrice($basePrice, false);
                if ((float) $specialPrice >= $maxSpecialPrice) {
                    $msg = __('Field %1 must be equal or greather than %2%% off. %3', __('Special Price'), '$' . number_format($maxSpecialPrice, 0,',','.'), $minPercent, $location);
                    throw new LocalizedException($msg, new \Exception());
                }
            } else {
                $maxSpecialPrice = $this->getMinSpecialPrice($basePrice, false);
                if ((float) $specialPrice > $maxSpecialPrice) {
                    $msg = __('Field %1 must be equal or greather than %2%% off. %3', __('Special Price'), $minPercent, $location);
                    if ($this->getStoreCode() == InsumosConstants::STORE_VIEW_CODE) {
                        $msg = __('Field %1 must be %2 lower than %3. %4', __('Special Price'),  $minPercent.'%',
                            '$' . number_format($basePrice, 0,',','.'), $location);
                    }
                    throw new LocalizedException($msg, new \Exception());
                }
            }

            $validator = new \Zend\Validator\Date();
            if (!$validator->isValid($startDate)) {
                $msg = __('Field %1 must be a valid date. %2', __('Start Date Offer'), $location);
                throw new LocalizedException($msg, new \Exception());
            }

            $__currentDate = $this->_timezone->date();
            $__startDate = new \DateTime($startDate);
            $__originStartDate = !empty($originStartDate) ? new \DateTime($startDate) : '';
            if ($__originStartDate != $__startDate) {
                if ($__startDate->format('Y-m-d') <= $__currentDate->format('Y-m-d')) {
                    $msg = __('Field %1 must be greather than current date. %2', __('Start Date Offer'), $location);
                    throw new LocalizedException($msg, new \Exception());
                }
            }

            $__minDays = $this->showMinimumDayOffer();
            $__maxDays = $this->showMaximumDayOffer();

            $validator = new \Zend\Validator\GreaterThan(['min' => 0]);
            if (!$validator->isValid($days)) {
                $msg = __('Field %1 must be only numbers greather than zero. %2', __('Days End Offer'), $location);
                throw new LocalizedException($msg, new \Exception());
            }

            $validator = new \Zend\Validator\Between(['min' => $__minDays, 'max' => $__maxDays]);
            if (!$validator->isValid($days)) {
                $msg = __('Field %1 must be between %2 and %3 %4', __('Days End Offer'), $__minDays, $__maxDays, $location);
                throw new LocalizedException($msg, new \Exception());
            }
        } catch (LocalizedException $e) {
            $result['error'] = true;
            $result['msg'] = $e->getMessage();
        } catch (\Exception $e) {
            $result['error'] = true;
            $result['msg'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $simpleProduct
     * @param $sellerId
     * @return int
     */
    public function getVehiculosBasePrice($simpleProduct, $sellerId)
    {
        $collection = $this->productCollectionFactory->create();
        $activeStatus = 70;
        $statusAttributeCode = 'wkv_dccp_state_details';
        $associatesTable = 'marketplace_assignproduct_associated_products as maap';
        $customerEntityVarchar = 'customer_entity_varchar as cev';
        $eavAttribute = 'eav_attribute as ea';
        $itemsTable = 'marketplace_assignproduct_items as mai';
        $collection->addAttributeToFilter('gama', ['eq' => $simpleProduct->getGama()])
            ->addAttributeToFilter('macrozona', ['eq' => $simpleProduct->getMacrozona()])
            ->addAttributeToFilter('status', ['eq' => 1])
            ->addWebsiteFilter(VehiculosConstants::WEBSITE_CODE)
            ->addAttributeToFilter('attribute_set_id', ['eq' => $simpleProduct->getAttributeSetId()])
            ->getSelect()
            ->join($associatesTable, 'e.entity_id = maap.product_id', ['offer_price' => 'price'])
            ->join($itemsTable, 'maap.parent_id = mai.id AND mai.status = 1')
            ->join($customerEntityVarchar, 'cev.entity_id = mai.seller_id')
            ->join($eavAttribute,
                "ea.attribute_id = cev.attribute_id AND attribute_code = '$statusAttributeCode' AND cev.value = '$activeStatus'"
            )
            ->where('maap.status = ?', 1)
            ->order('offer_price ASC')
            ->limit(1);

        return (int)$collection->getFirstItem()['offer_price'];
    }

    /**
     * Get agreement data from REST service
     *
     * @param string $token
     * @param string $startDate
     * @param string $days
     * @return Zend_Http_Response
     */
    public function getEndBusinessDate($token, $startDate, $days)
    {
        $response = null;

        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_holidays/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');

            if ($endpoint && $token && $startDate && (int)$days > 0) {
                $endpoint = sprintf($endpoint, $startDate, $days);
                $httpHeaders = [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . trim($token)
                ];

                $client = $this->_httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                        'timeout' => 30]
                );
                $client->setHeaders($httpHeaders);
                $response = $client->request(\Zend_Http_Client::GET)->getBody();

                $this->_logger->info('Holidays Start Date: ' . $startDate . ' Days: ' . $days . ' Service Response: ' . $response);
                $response = $this->_jsonHelper->jsonDecode($response);
            } else {
                throw new LocalizedException(__('Define endpoint and params of holidays webservice'));
            }
        } catch(LocalizedException $e) {
            $this->_logger->warning('Holidays Error message: ' . $e->getMessage());
        } catch(\Exception $e) {
            $this->_logger->critical('Holidays Error message: ' . $e->getMessage());
        }

        return $response;
    }

    /**
     * Get Current Customer Id
     *
     * @return int
     */
    public function getCustomerId()
    {
        $customerId = 0;
        if ($this->_customerSession->isLoggedIn()) {
            $customerId = (int)$this->_customerSession->getCustomerId();
        }

        return $customerId;
    }

    /**
     * Get Current Customer Website Id
     *
     * @return int|null
     */
    public function getCustomerWebsiteId()
    {
        $websiteId = null;
        if ($this->_customerSession->isLoggedIn()) {
            $websiteId = (int)$this->_customerSession->getCustomerData()->getWebsiteId();
        }

        return $websiteId;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCustomerById($id) {
        return $this->_customerFactory->create()->load($id);
    }

    /**
     * Retrieve seller ID
     *
     * @return int
     */
    public function getSellerId()
    {
        return (int)$this->_helper->getSubAccountSellerId();
    }

    /**
     * Retrieve website ID
     *
     * @return int
     */
    public function getWebsiteId()
    {
        return $this->_websiteId;
    }

    /**
     * Retrieve website ID
     *
     * @return int
     */
    public function getWebsiteCode()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }

    /**
     * Website id de del convenio de emergencias
     * @return mixed
     */
    public function getEmergencyWebsiteId(){
        return $this->_emergencyHelper->getWebsite()->getId();
    }

    /**
     * Retrieve product with special price. IMPORTANT: Only use this function for Voucher and virtual products
     *
     * @param mixed bool/array $productIds
     * @return array
     */
    public function getAllProductWithSpecialPrice($productIds = false, $isVoucher = false)
    {

        $attrProductName = $this->_eavAttribute->getIdByCode('catalog_product', 'name');
        $products = $this->_registry->registry('dccp_products_with_offer');
        $activeStatus = 70;
        //configurable-fix: Fix configurable product not displaying price in PLP
        if (!$products) {
            $whereInAssoProductIds = (is_array($productIds) && count($productIds) > 0) ? ' WHERE ud.is_seller = 1 AND asso.parent_product_id IN (' . implode(',', $productIds) . ') AND asso.qty > 0 AND asso.status = 1' : '';

            $websiteId = $this->getWebsiteId();
            $connection = $this->_resource->getConnection();
            $currentDate = $connection->quote($this->_timezone->date()->format('Y-m-d'));

            $tableUserData = $this->_resource->getTableName('marketplace_userdata');
            $tableItems = $this->_resource->getTableName('marketplace_assignproduct_items');
            $tableAssociatedItems = $this->_resource->getTableName('marketplace_assignproduct_associated_products');
            $tableOffer = $this->_resource->getTableName('dccp_offer_product');
            $customerEntityVarchar = $this->_resource->getTableName('customer_entity_varchar');
            $eavAttribute = $this->_resource->getTableName('eav_attribute');
            //configurable-fix: Fix configurable product not displaying price in PLP

            $sql = 'SELECT DISTINCT asso.parent_product_id, asso.product_id AS asso_product_id,
            asso.price AS asso_price, p.special_price, p.base_price
            FROM ' . $tableAssociatedItems . ' asso
            LEFT JOIN ' . $tableItems . ' i ON i.id = asso.parent_id
            LEFT JOIN ' . $tableUserData . ' ud ON ud.seller_id = i.seller_id
            LEFT JOIN ' . $tableOffer . ' p
            ON asso.product_id = p.product_id AND asso.parent_id = p.assign_id
            AND p.status = 3 AND p.website_id = ' . $websiteId . ' AND p.special_price < p.base_price'
                .' LEFT JOIN ' . $customerEntityVarchar . ' AS cv ON ud.seller_id = cv.entity_id'
                .' LEFT JOIN ' . $eavAttribute . ' AS ea ON ea.attribute_id = cv.attribute_id AND ea.attribute_code = "wkv_dccp_state_details"'
                . $whereInAssoProductIds . ' AND cv.value = '. $activeStatus .
                ' ORDER BY asso.price DESC';

            // NOTE: There is a difference in above filter (p.special_price < p.base_price) with the following (p.special_price > p.base_price), probably only one is right
            $result = $connection->query($sql);

            foreach ($result as $item) {
                if ((float)$item['asso_price'] > 0) {
                    if ((float)$item['base_price'] > 0 && (float)$item['special_price'] > 0) {
                        $basePrice = (float)$item['base_price'];
                        $specialPrice = (float)$item['asso_price'];
                    } else {
                        $basePrice = (float)$item['asso_price'];
                        $specialPrice = '';
                    }

                    $products[$item['parent_product_id']] = [
                        'product_id' => $item['asso_product_id'],
                        'base_price' => $basePrice,
                        'special_price' => $specialPrice
                    ];
                }
            }

            //cristian merge-alimentos
            //$whereInItProductIds = (is_array($productIds) && count($productIds) > 0) ? " WHERE ud.is_seller = 1 AND it.product_id IN (" . implode(',', $productIds) . ')' : "";
            $whereInItProductIds = (is_array($productIds) && count($productIds) > 0) ? " WHERE ud.is_seller = 1 AND main_table.product_id IN (" . implode(',', $productIds) . ') AND main_table.qty > 0 AND main_table.status = 1' : "";
            $sortMode=($isVoucher?'ASC':'DESC');

            $collection = $this->_itemsCollection->create();
            $collection->getSelect()
                ->reset(\Zend_Db_Select::COLUMNS)
                ->distinct(true)
                ->columns([
                    'main_table.product_id',
                    'main_table.price as it_price',
                    'p.special_price',
                    'p.base_price'
                ]);

            $fields = [];
            $cond = 'main_table.product_id = p.product_id
                    AND main_table.id = p.assign_id
                    AND p.status = 3
                    AND p.website_id = ' . $websiteId . '
                    AND p.special_price > p.base_price ' . $whereInItProductIds;
            $collection->getSelect()->joinLeft(
                ['ud' => $tableUserData],
                'main_table.seller_id = ud.seller_id',
                $fields
            );
            $collection->getSelect()->joinLeft(
                ['p' => $tableOffer],
                $cond,
                $fields
            );
            $collection->setOrder('main_table.price', $sortMode);

            foreach ($collection as $item) {
                if ((float)$item['it_price'] > 0) {
                    if ((float)$item['base_price'] > 0 && (float)$item['special_price'] > 0) {
                        $basePrice = (float)$item['base_price'];
                        $specialPrice = (float)$item['it_price'];
                    } else {
                        $basePrice = (float)$item['it_price'];
                        $specialPrice = '';
                    }

                    $products[$item['product_id']] = [
                        'product_id' => $item['product_id'],
                        'base_price' => $basePrice,
                        'special_price' => $specialPrice
                    ];
                }
            }
            $this->_registry->register('dccp_products_with_offer', $products);
        }
        return $products;
    }

    /**
     * Retrieve product with special price grouped by seller
     *
     * @param string $productType
     * @param mixed int $productId
     * @param mixed false/int $sellerId
     * @return array
     */
    public function getProductWithSpecialPriceBySeller($productType, $productId, $sellerId = false)
    {
        //$this->_registry->unregister('dccp_products_with_offer_grouped_seller');
        //$products = $this->_registry->registry('dccp_products_with_offer_grouped_seller');
        ////if (!$products) {
            $websiteId = $this->getWebsiteId();
            $connection = $this->_resource->getConnection();
            $currentDate = $connection->quote($this->_timezone->date()->format('Y-m-d'));
            $tableOffer = $this->_resource->getTableName('dccp_offer_product');

            if ($productType == 'configurable') {
                $tableItems = $this->_resource->getTableName('marketplace_assignproduct_items');

                $collection = $this->_associatesCollection->create();
                $collection->getSelect()
                    ->reset(\Zend_Db_Select::COLUMNS)
                    ->columns([
                        'it.seller_id',
                        'main_table.parent_product_id',
                        'main_table.product_id as conf_id',
                        'it.product_id',
                        'main_table.price as asso_price',
                        'it.price as it_price',
                        'of.base_price',
                        'of.special_price'
                    ]);
                //configurable-fix: column name with error
                $collection->addFieldToFilter('main_table.parent_product_id', $productId);

                if ( (int)$sellerId > 0 ) {
                    //configurable-fix: column name with error
                    $collection->addFieldToFilter('it.seller_id', (int)$sellerId);
                }

                $sql = 'main_table.parent_id = it.id';
                $fields = [];
                $sql2 = 'main_table.parent_id = of.assign_id';
                $sql2 .= ' and main_table.product_id = of.product_id';
                $sql2 .= ' and of.status = 3';
                $sql2 .= ' and of.website_id = '. $websiteId;
                $sql2 .= ' and of.special_price < of.base_price';
                $fields2 = [];

                $collection->getSelect()->joinLeft($tableItems.' as it', $sql, $fields);
                $collection->getSelect()->joinLeft($tableOffer.' as of', $sql2, $fields2);
                $collection->setOrder('main_table.price', 'desc');
            } else {
                $collection = $this->_itemsCollection->create();
                $collection->getSelect()
                    ->reset(\Zend_Db_Select::COLUMNS)
                    ->columns([
                        'main_table.seller_id',
                        'main_table.product_id',
                        'main_table.price as it_price',
                        'of.base_price',
                        'of.special_price'
                    ]);
                $collection->addFieldToFilter('main_table.product_id', $productId);

                $sql = 'main_table.id = of.assign_id';
                $sql .= ' and of.website_id = '. $websiteId;
                $sql .= ' and of.status = 3';
                $fields = [];

                $collection->getSelect()->joinLeft($tableOffer.' as of', $sql, $fields);
                $collection->setOrder('main_table.price', 'desc');
            }

            if ($collection) {
                foreach ($collection as $item) {
                    if ($productType == 'configurable') {
                        if ((float)$item['asso_price'] > 0) {
                            $productId = $item['conf_id'];
                            if ((float)$item['base_price'] > 0 && (float)$item['special_price'] > 0) {
                                $basePrice = (float)$item['base_price'];
                                $specialPrice = (float)$item['special_price'];
                            } else {
                                $basePrice = (float)$item['asso_price'];
                                $specialPrice = '';
                            }
                        }
                    } else {
                        if ((float)$item['it_price'] > 0) {
                            $productId = $item['product_id'];
                            if ((float)$item['base_price'] > 0 && (float)$item['special_price'] > 0) {
                                $basePrice = (float)$item['base_price'];
                                $specialPrice = (float)$item['special_price'];
                            } else {
                                $basePrice = (float)$item['it_price'];
                                $specialPrice = '';
                            }
                        }
                    }

                    $products[$item['seller_id']][$item['product_id']][$productId] = [
                        'base_price' => $basePrice ?? 0,
                        'special_price' => $specialPrice ?? 0
                    ];
                }
            }

            //$this->_registry->register('dccp_products_with_offer_grouped_seller', $products);
        ////}

        return $products ?? [];
    }

    /**
     * Get Offer Collection to change base_price from special offer
     * @return \Formax\Offer\Model\ResourceModel\Offer\Collection
     */
    public function getOfferCollection()
    {
        $connection = $this->_resource->getConnection();
        $currentDate = $connection->quote($this->_timezone->date()->format('Y-m-d'));
        $tableItems = $this->_resource->getTableName('marketplace_assignproduct_items');
        $tableAssociated = $this->_resource->getTableName('marketplace_assignproduct_associated_products');

        $conditionsItems = [
            "main_table.parent_product_id = it.product_id",
            "main_table.seller_id = it.seller_id",
            "(it.type=\"configurable\" OR it.status = 1)",
            "main_table.status = 1",
            $currentDate . " >= DATE(main_table.start_date)",
            $currentDate . " <= DATE(main_table.end_date)",

        ];
        $conditionsAssociated = [
            "main_table.product_id = asso.product_id",
            "it.id = asso.parent_id",
            "asso.status = 1",
        ];

        $offerCollection = $this->_offerCollection->create();
        $offerCollection->getSelect()
            ->joinInner(
                ['it' => $tableItems],
                implode(' AND ', $conditionsItems),
                [
                    'item_id' => 'it.id',
                    'item_price' => 'it.price',
                    'item_assembly_price' => 'it.assembly_price',
                    'item_base_price' => 'it.base_price',
                    'item_shipping_price' => 'it.shipping_price',
                    'item_type' => 'it.type'
                ])->joinLeft(
                ['asso' => $tableAssociated],
                implode(' AND ', $conditionsAssociated),
                [
                    'asso_id' => 'asso.id',
                    'asso_product_id' => 'asso.product_id',
                    'asso_price' => 'asso.price'
                ]);

        $offerCollection->distinct(true);
        return $offerCollection;
    }

    public function changePriceOnConfigurableOffer($associatedId, $price)
    {
        $associatesCollection = $this->_associatesCollection->create();
        $associatesCollection->addFieldToFilter('id', $associatedId)->getFirstItem();
        foreach ($associatesCollection as $item) {
            $item->setPrice((float)$price);
        }
        $associatesCollection->save();
    }

    public function changePriceOnSimpleOffer($itemId, $price) {
        $itemsCollection = $this->_itemsCollection->create();
        $itemsCollection->addFieldToFilter('id', $itemId)->getFirstItem();
        foreach ($itemsCollection as $item) {
            $item->setPrice((float)$price);
        }
        $itemsCollection->save();
    }

    /**
     * @param $item
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function changePriceOnConfigurableOfferInsumos($item)
    {
        $product = $this->productFactory->create();
        $this->productResourceModel->load($product, $item['product_id']);
        $attributeSetId = $product->getAttributeSetId();
        $macrozona = $product->getAttributeText('macrozona');
        $resultDispatch = $this->dispatchPriceManagement->getDispatchPrice($item['seller_id'], $macrozona, $attributeSetId, $item['website_id']);
        $shippingPrice = (int)($resultDispatch ? $resultDispatch->getPrice() : 0);
        $associatedId = $item['asso_id'];
        $specialPrice = $item['special_price'];
        $basePrice = $specialPrice - $shippingPrice;

        $associate = $this->_associates->create();
        $this->associatesResourceModel->load($associate, $associatedId);
        $associate->setPrice($specialPrice);
        $associate->setBasePrice($basePrice);
        $this->associatesResourceModel->save($associate);
    }

    public function updateStatusOffer($item, $status)
    {
        $offerCollection = $this->_offerCollection->create();
        $offerCollection->addFieldToFilter('id', $item['id']);
        foreach ($offerCollection as $item) {
            $item->setStatus($status);
        }
        $offerCollection->save();
    }

    /**
     * Execute change price to special price
     *
     * @return array
     */
    public function changePrices()
    {
        $result = ['error' => false, 'msg' => ''];
        $startTime = microtime(true);
        $logger = ObjectManager::getInstance()
            ->get(\Psr\Log\LoggerInterface::class);
        try {
            $offerCollections = $this->getOfferCollection();

            $i = 0;
            if ($offerCollections->getSize() > 0) {
                foreach ($offerCollections->getItems() as $item) {
                    try
                    {
                        $itemType = $item['item_type'];
                        $itemWebsiteCode = $this->getWebsiteCodeById($item['website_id']);
                        if ($itemType == 'configurable') {
                            $associatedId = $item['asso_id'];
                            $specialPrice = $item['special_price'];
                            if ($itemWebsiteCode == InsumosConstants::STORE_VIEW_CODE && (int)$associatedId > 0 && (float)$specialPrice > 0) {
                                $this->changePriceOnConfigurableOfferInsumos($item);
                                $i++;
                            } elseif ((int)$associatedId > 0 && (float)$specialPrice > 0) {
                                $this->changePriceOnConfigurableOffer($associatedId, $specialPrice);
                                $i++;
                            }
                        } else {
                            $itemId = $item['item_id'];
                            $specialPrice = $item['special_price'];
                            $websiteId = $item['website_id'];
                            if ((int)$itemId > 0 && (float)$specialPrice > 0) {
                                if (in_array($itemWebsiteCode,[self::CONVENIO_VOUCHER_WEBSITE, LinetsVoucherConstants::WEBSITE_CODE])) {
                                    if ((float)$specialPrice > (float)$item['base_price']) {
                                        $this->changePriceOnSimpleOffer($itemId, $item['special_price']);
                                    }
                                } else if ($itemWebsiteCode == \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE) {
                                    $price = $specialPrice + $item['item_shipping_price'];
                                    $this->changePriceOnSimpleOffer($itemId, $price);
                                } else if ($itemWebsiteCode == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) {
                                    $price = $specialPrice + $item['item_assembly_price'];
                                    $this->changePriceOnSimpleOffer($itemId, $price);
                                } else {
                                    $this->changePriceOnSimpleOffer($itemId, $specialPrice);
                                }
                                $i++;
                            }
                        }
                        // update offer status to 3 (special price applied)
                        $this->updateStatusOffer($item, Offer::OFFER_APPLIED);
                    }
                    catch (\Exception $e) {
                        $logger->critical($e->getMessage());
                    }
                }
                $resultTime = microtime(true) - $startTime;
                $result = ['error' => false, 'msg' => 'Process was executed successfully in ' . gmdate('H:i:s', $resultTime) . ' Total records: ' . $i];
            } else {
                $result = ['error' => false, 'msg' => 'No record was updated.'];
            }
        } catch (\Exception $e) {
            $logger->critical($e->getMessage());

            if ($e->getCode() == self::CUSTOM_ERROR_CODE) {
                $result = ['error' => true, 'msg' => $e->getMessage()];
            } else {
                $result = ['error' => true, 'msg' => 'An error occured while executing the process. ' . $e->getMessage()];
            }
        }

        return $result;
    }

    public function getOfferCollectionForPriceToBase()
    {
        $connection = $this->_resource->getConnection();
        $currentDate = $connection->quote($this->_timezone->date()->format('Y-m-d'));
        $tableItems = $this->_resource->getTableName('marketplace_assignproduct_items');

        // Filter Offer Collection
        $offerCollection = $this->_offerCollection->create();
        $offerCollection->addFieldToFilter('main_table.status', Offer::OFFER_APPLIED);

        $dateDiff = 'DATEDIFF('. $currentDate .', DATE(end_date)) > 0';
        $offerCollection->getSelect()->where($dateDiff);

        $offerCollection->getSelect()->joinInner(['it' => $tableItems],"main_table.assign_id = it.id", 'type');
        $offerCollection->distinct(true);

        return $offerCollection;
    }

    public function updateConfigurableOfferPriceToBase($basePrice, $asignId, $parentProductId, $productId)
    {
        $connection = $this->_resource->getConnection();
        $tableAssociated = $this->_resource->getTableName('marketplace_assignproduct_associated_products');
        $sql = 'UPDATE ' . $tableAssociated . ' SET price = ' . (float)$basePrice .
            ' WHERE parent_id = ' . $asignId . ' AND parent_product_id = ' . $parentProductId .
            ' AND product_id = ' . $productId;
        $connection->query($sql);
    }

    public function updateConfigurableOfferPriceToBaseInsumos($item)
    {
        $product = $this->productFactory->create();
        $this->productResourceModel->load($product, $item['product_id']);
        $attributeSetId = $product->getAttributeSetId();
        $macrozona = $product->getAttributeText('macrozona');
        $resultDispatch = $this->dispatchPriceManagement->getDispatchPrice($item['seller_id'], $macrozona, $attributeSetId, $item['website_id']);
        $shippingPrice = (int)($resultDispatch ? $resultDispatch->getPrice() : 0);
        //base_price es de la tabla dccp_offer_product que es el price (finalPrice) de la tabla marketplace_assignproduct_associated_products
        $finalPrice = $item['base_price'];
        $basePrice = $finalPrice - $shippingPrice;

        $connection = $this->_resource->getConnection();
        $tableAssociated = $this->_resource->getTableName('marketplace_assignproduct_associated_products');
        $sql = 'UPDATE ' . $tableAssociated . ' SET price = ' . (float)$finalPrice .
            ', base_price = ' . (float)$basePrice . ' WHERE parent_id = ' . $item['assign_id'] . ' AND parent_product_id = ' .
            $item['parent_product_id'] .
            ' AND product_id = ' . $item['product_id'];
        $connection->query($sql);
    }

    public function updateItemOfferPriceToBase($assignId, $basePrice, $specialPrice, $websiteId)
    {
        $connection = $this->_resource->getConnection();
        $tableItems = $this->_resource->getTableName('marketplace_assignproduct_items');
        $itemWebsiteCode = $this->getWebsiteCodeById($websiteId);

        if (in_array($itemWebsiteCode,[self::CONVENIO_VOUCHER_WEBSITE, LinetsVoucherConstants::WEBSITE_CODE])) {
            if ((float)$specialPrice > (float)$basePrice) {
                $sql = 'UPDATE ' . $tableItems . ' SET price = ' . (float)$basePrice .
                    ' WHERE id = ' . $assignId;
                $connection->query($sql);
            }
        } else if ($itemWebsiteCode == \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE) {
            // MPMT-38: Error Oferta Especial CM Emergencia - se remueve actualizacion de base_price -> base_price = (float)$item['base_price']
            $sql = 'UPDATE ' . $tableItems . ' SET price = ' . (float)$basePrice . " + shipping_price" . ' WHERE id = ' . $assignId;
            // MPMT-38: Error Oferta Especial CM Emergencia - Faltaba ejectuar la query para actualizar nuevamente el precio del producto al precio base + shipping_price
            $connection->query($sql);
        } else if ($itemWebsiteCode == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) {
            $sql = 'UPDATE ' . $tableItems . ' SET price = ' . (float)$basePrice ." + assembly_price" . ' WHERE id = ' . $assignId;
            $connection->query($sql);
        } else {
            if ( (float)$specialPrice < (float)$basePrice ) {
                $sql = 'UPDATE ' . $tableItems . ' SET price = ' . (float)$basePrice .
                    ' WHERE id = ' . $assignId;
                $connection->query($sql);
            }
        }


    }

    /**
     * Execute change price to base price
     *
     * @return array
     */
    public function changePricesToBase()
    {
        $result = ['error' => false, 'msg' => ''];
        $startTime = microtime(true);
        $connection = $this->_resource->getConnection();
        $logger = ObjectManager::getInstance()
            ->get(\Psr\Log\LoggerInterface::class);
        $connection->beginTransaction();

        try {
            $collection = $this->getOfferCollectionForPriceToBase();
            $i = 0;

            if ($collection) {
                foreach ($collection as $item) {
                    try
                    {
                        $itemWebsiteCode = $this->getWebsiteCodeById($item['website_id']);
                        if ($item['type'] == 'configurable') {
                            $assignId = $item['assign_id'];
                            $parentProductId = $item['parent_product_id'];
                            $productId = $item['product_id'];
                            $basePrice = $item['base_price'];
                            if($itemWebsiteCode == InsumosConstants::WEBSITE_CODE && (int)$assignId > 0 && (int)$parentProductId && (int)$productId && (float) $basePrice > 0){
                                $this->updateConfigurableOfferPriceToBaseInsumos($item);
                                $i++;
                            }elseif ((int)$assignId > 0 && (int)$parentProductId && (int)$productId && (float) $basePrice > 0) {
                                $this->updateConfigurableOfferPriceToBase($basePrice, $assignId, $parentProductId, $productId);
                                $i++;
                            }
                        } else {
                            $assignId = $item['assign_id'];
                            $basePrice = $item['base_price'];
                            $websiteId = $item['website_id'];
                            $specialPrice = $item['special_price'];
                            if ((int)$assignId > 0 && (float)$basePrice > 0) {
                                $this->updateItemOfferPriceToBase($assignId, $basePrice, $specialPrice, $websiteId);
                                $i++;
                            }
                        }
                        // update offer status to 4 (special price applied)
                        $this->updateStatusOffer($item, Offer::OFFER_EXPIRED);
                    }
                    catch (\Exception $e) {
                        $logger->critical($e->getMessage());
                    }
                }
                $resultTime = microtime(true) - $startTime;
                $result = ['error' => false, 'msg' => 'Process was executed successfully in ' . gmdate('H:i:s', $resultTime) . ' Total records: ' . $i];
            } else {
                $result = ['error' => false, 'msg' => 'No record was updated.'];
            }
            $connection->commit();
        } catch (\Exception $e) {
            $logger->critical($e->getMessage());
            $connection->rollBack();

            if ($e->getCode() == self::CUSTOM_ERROR_CODE) {
                $result = ['error' => true, 'msg' => $e->getMessage()];
            } else {
                $result = ['error' => true, 'msg' => 'An error occured while executing the process. ' . $e->getMessage()];
            }
        }

        return $result;
    }

    /**
     * Get Base price by AssignId
     *
     * @param $assignId
     * @param $productType
     * @param bool $isEmergenciasBasePrice
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBasePrice($assignId, $productType, $isEmergenciasBasePrice = false)
    {
        $price = [];

        if ($assignId) {
            $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
            $connection = $this->_resource->getConnection();
            $tableOffer = $this->_resource->getTableName('dccp_offer_product');
            $tableItems = $this->_resource->getTableName('marketplace_assignproduct_items');

            if ($productType == 'configurable') {
                $model = $this->_associates->create();
                $collection = $model->getCollection()->addFieldToFilter('parent_id', $assignId);
                $collection->getSelect()->reset(\Magento\Framework\DB\Select::COLUMNS)
                    ->reset(\Magento\Framework\DB\Select::GROUP)
                    ->reset(\Magento\Framework\DB\Select::ORDER);

                $sql = 'main_table.parent_id = it.id';
                $fields = [];
                $sql2 = 'main_table.parent_id = of.assign_id';
                $sql2 .= ' and main_table.product_id = of.product_id';
                $sql2 .= ' and of.website_id = '. $websiteId;
                $sql2 .= ' and of.status = 3';
                $fields2 = [];
                $collection->getSelect()->joinLeft($tableItems . ' as it', $sql, $fields);
                $collection->getSelect()->joinLeft($tableOffer . ' as of', $sql2, $fields2);
                $collection->addFieldToSelect(['id', 'qty', 'product_id', 'base_price']);
                if ($isEmergenciasBasePrice) {
                    $collection->getSelect()->columns(['price' =>new \Zend_Db_Expr('IFNULL(of.base_price, main_table.base_price)')]);
                } else {
                    $collection->getSelect()->columns(['price' =>new \Zend_Db_Expr('IFNULL(of.base_price, main_table.price)')]);
                }

                foreach ($collection as $item) {
                    $info = [
                        'id' => $item->getId(),
                        'qty' => $item->getQty(),
                            'price' => (float)$item->getPrice() > 0 ? (float)$item->getPrice() : '',
                            'base_price' => (float)$item->getBasePrice() > 0 ? (float)$item->getBasePrice() : ''
                    ];
                    $price[$item->getProductId()] = $info;
                }
            } else {
                $collection = $this->_itemsCollection->create();
                $collection->getSelect()->reset(\Magento\Framework\DB\Select::COLUMNS)
                    ->reset(\Magento\Framework\DB\Select::GROUP)
                    ->reset(\Magento\Framework\DB\Select::ORDER);
                $collection->addFieldToFilter('main_table.id', $assignId);

                $sql = 'main_table.id = of.assign_id';
                $sql .= ' and of.website_id = '. $websiteId;
                $sql .= ' and of.status = 1';
                $sql .= ' and date(of.start_date) <= ' . $connection->quote($this->_timezone->date()->format('Y-m-d'));
                $sql .= ' and date(of.end_date) >= ' . $connection->quote($this->_timezone->date()->format('Y-m-d'));
                $fields = [];
                $collection->getSelect()->joinLeft($tableOffer.' as of', $sql, $fields);
                if ($isEmergenciasBasePrice) {
                    $collection->getSelect()->columns(['price' => new \Zend_Db_Expr('IFNULL(of.base_price, main_table.base_price)')]);
                } else {
                    $collection->getSelect()->columns(['price' => new \Zend_Db_Expr('IFNULL(of.base_price, main_table.price)')]);
                }
                $collection->setPageSize(1);

                if ($collection) {
                    foreach ($collection as $item) {
                        $price[] = (float)$item->getPrice();
                    }
                }
            }
        }

        return $price;
    }

    /**
     * Get Store code
     *
     * @return string
     */
    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }

    /**
     * Get Store code by ID
     *
     * @param int $id
     * @return string|null
     */
    public function getWebsiteCodeById($id)
    {
        try {
            $websiteCode = (string)$this->_storeManager->getWebsite($id)->getCode();
        } catch (LocalizedException $localizedException) {
            $websiteCode = null;
            $this->_logger->error($localizedException->getMessage());
        }

        return $websiteCode;
    }

    /**
     * Set/Update Linked Products Offer
     *
     * @param $offerId
     * @param $parentProductId
     * @param $productId
     * @param $assignId
     * @param $sellerId
     * @param $websiteId
     * @param $startDate
     * @param $__endDate
     * @param $days
     * @param $specialPrice
     * @param $basePrice
     * @return array
     */
    public function setLinkedProductsOffer($offerId, $parentProductId, $productId, $assignId, $sellerId, $websiteId, $startDate, $__endDate, $days, $specialPrice, $basePrice)
    {
        $result = ['error' => false, 'msg' => ''];
        $model = $this->_offerFactory->create();
        if ($offerId > 0) {
            $model->load($offerId);
        }

        $model->setParentProductId($parentProductId);
        $model->setProductId($productId);
        $model->setAssignId($assignId);
        $model->setSellerId($sellerId);
        $model->setWebsiteId($websiteId);
        $model->setStartDate($startDate);
        $model->setEndDate($__endDate);
        $model->setDays($days);
        $model->setSpecialPrice($specialPrice);
        $model->setBasePrice($basePrice);
        $model->setStatus(1);
        $model->save();

        // save in table marketplace_assignproduct_items
        $itemsFactory = $this->_itemsFactory->create()->load($assignId);
        if ($itemsFactory->getId() > 0) {
            $itemsFactory->setHasOffer(1);
            $itemsFactory->save();
        }

        $product = $this->_assignHelper->getProduct($productId);
        $linkedProductsSkus = $product->getLinkedProducts();
        $originalSku = $product->getSku();
        $linkedProductsSkus = $linkedProductsSkus ? explode(',', $linkedProductsSkus) : [];

        //Toda la info de los items en la lista de linkeados
        $this->_searchCriteriaBuilder->addFilter('sku', $linkedProductsSkus, 'in');
        $searchCriteria = $this->_searchCriteriaBuilder->create();

        $linkedProductsObjects = $this->_productRepositoryInterface->getList($searchCriteria)->getItems();

        //Todas las ofertas especiales que ya están cargadas
        foreach ($linkedProductsObjects as $linkedProductEntity)
        {
            //Consultamos la info de item a actualizar
            $assignId = $this->_assignHelper->getAssignId($linkedProductEntity->getId(), $sellerId);

            //Actualizamos solo si no es el producto sobre el que generamos la oferta especial
            if($linkedProductEntity->getSku() != $originalSku && $assignId)
            {
                $assignData = $this->_assignHelper->getAssignDataByAssignId($assignId);

                //consultamos si existe una oferta especial previamente definida para actualizarla.
                $itemLinked = $this->_offerCollection->create()
                    ->addFilter('assign_id', $assignId, 'eq')
                    ->addFilter('seller_id', $sellerId, 'eq')
                    ->addFieldToSelect('id')
                    ->getFirstItem();
                $idLinked = $itemLinked && (int)$itemLinked['id'] > 0 ? (int)$itemLinked['id'] : 0;

                $model = $this->_offerFactory->create();
                if ($idLinked > 0) {
                    $model->load($idLinked);
                }

                $model->setParentProductId($assignData['product_id']);
                $model->setProductId($assignData['product_id']);
                $model->setAssignId($assignId);
                $model->setSellerId($sellerId);
                $model->setWebsiteId($websiteId);
                $model->setStartDate($startDate);
                $model->setEndDate($__endDate);
                $model->setDays($days);
                $model->setSpecialPrice($specialPrice);
                $model->setBasePrice($basePrice);
                $model->setStatus(1);
                $model->save();

                $result['msg'] .= __('Data was saved succesfully in Product SKU: %1. ' ,$linkedProductEntity->getSku(). ' ,'. $originalSku);
                // save in table marketplace_assignproduct_items
                $itemsFactory = $this->_itemsFactory->create()->load($assignId);
                if ($itemsFactory->getId() > 0) {
                    $itemsFactory->setHasOffer(1);
                    $itemsFactory->save();
                }
            }
        }
        return $result;
    }

    public function setLinkedProducts($productId, $TriggerProductId, $triggerProductData, $assignId, $sellerId, $serviceData, $websiteId, $flag )
    {
        $result = ['error' => false, 'msg' => ''];
        $isMobiliario = $this->helperMobiliario->getWebsite()->getId() == $websiteId;
        $product = $this->_assignHelper->getProduct($productId);

        if ($assignId)//si tiene ofertas existentes
        {
            $assignData = $this->_assignHelper->getAssignDataByAssignId($assignId);
            $newBasePrice = $triggerProductData['base_price'];

            //------------------ en caso de respuesta del cliente ------
            //ojo con esto, ver de validar que el precio de armado sea el ultimo configurado para ese producto.
            //$seller = $this->customerRepositoryInterface->getById($sellerId);
            //$assemblyPrice = $this->helperMobiliario->getAssemblyPriceforProduct($product, $seller) ;
            //------------------ assebly_price real -------------- ------
            $newPrice = ($assignData->getBasePrice() && $assignData->getAssemblyPrice()) ? $newBasePrice + $assignData->getAssemblyPrice() : $newBasePrice;

            $isOfferActive = false;
            if($assignData->getHasOffer()) {
                $specialOffer = $this->_offerFactory->create()->load($assignId, 'assign_id');
                if ($specialOffer->getId() > 0) {
                    $isOfferActive = $this->datesAreCurrent($specialOffer->getStartDate(), $specialOffer->getEndDate()) == 1 ? true :false;
                }
            }

            if ($assignData->getHasOffer())
            {
                $offerId = $this->_offerFactory->create()
                    ->getCollection()
                    ->addFieldToFilter('assign_id', $assignId)
                    ->addFieldToSelect('id')
                    ->getFirstItem()
                    ->getId();
                $actualOffer = $this->_offerFactory->create();
                if ($offerId){
                    $actualOffer->load($offerId);
                    $actualOffer->setBasePrice((float)$newBasePrice);
                    $actualOffer->save();
                }else{//esto es un error porque la oferta tiene que existir.
                    $result['error'] = true; $result['msg'] .= __('problem wit an special offer');
                }
            }
            if ( ($isMobiliario) || !$assignData->getHasOffer() || ($assignData->getHasOffer() && !$isOfferActive))
                //el update se efectua siempre en Mobiliario (porque modifica el 'tiene Stock'
                //en Emergencia no lo modifica - entiendo que solo edita cundo cumple condicion original
            {
                $tieneStock = (float)$assignData->getQty() > 0 ? true : false;
                if ($isMobiliario){
                    $tieneStock = (float)$triggerProductData['qty'] > 0 ? true : false;
                }
                $params = [
                    'idOrganismo' => $serviceData['idOrganismo'],
                    'idProducto' => $product->getSku(),
                    'idConvenioMarco' => $serviceData['idConvenioMarco'],
                    'idEstadoProductoProveedorConvenio' =>  $serviceData['idEstadoProductoProveedorConvenio'],
                    'tieneStock' => $tieneStock, //depende del website
                    'precio' => (float)$newPrice
                ];

                $itemsFactory = $this->_itemsFactory->create()->load($assignId);
                if ($itemsFactory->getId() > 0) {
                    $itemsFactory->setPrice((float)$newPrice);
                    $itemsFactory->setBasePrice((float)$newBasePrice);
                    //assembly_Price - Esperando respuesta del cliente (si se hace update)
                    //update_At - Validad que esta variable cambia con pruebas (para que sincronize el cron diario)
                    $itemsFactory->save();
                }

                if ($isMobiliario){
                    //en mobiliario se tiene que ejecutar siempre porque no tenemos forma de diferenciar cunado
                    //modifica stock y cuando No, En Emergencias no modifican Stock.
                    $productType = $product->getTypeId();

                    $updateData = $triggerProductData;
                    $updateData['description'] = $assignData->getDescription();
                    $updateData['image'] = $assignData->getImage();
                    $updateData['price'] = $newPrice;
                    $updateData['base_price'] = $newBasePrice;
                    $updateData['assign_id'] = $assignId;
                    $updateData['assembly_price'] = $assignData->getAssemblyPrice();
                    $updateData['product_id'] = $product->getEntityId();
                    $updateData['seller_id'] = $sellerId;

                    $result = $this->_assignHelper->processAssignProduct($updateData, $productType, $flag);
                    $this->_assignHelper->processProductStatus($result);
                }

                $validateServiceAssign = $this->_assignHelper->setAssignProductsUpdate($serviceData['token'], $params);

                if (isset($validateServiceAssign['success']) && $validateServiceAssign['success'] == 'NOK' &&
                    isset($validateServiceAssign['errores']) && is_array($validateServiceAssign['errores']))
                {
                    foreach ($validateServiceAssign['errores'] as $error)
                    {
                        $result['msg'] .= __($error['descripcion']);
                        $result['msg'] .= __(' - This information will be synchronized with Mercado Publico later');
                    }
                } elseif (!isset($validateServiceAssign['success']) && isset($validateServiceAssign['error']) &&
                    isset($validateServiceAssign['message']))
                {
                    $message = $validateServiceAssign['error'] . '. ' . $validateServiceAssign['message'];
                    $result['msg'] .= __($message);
                    $result['msg'] .= __(' - This information will be synchronized with Mercado Publico later');
                }
            }

        } else
        {
            $productType = $product->getTypeId();
            $newBasePrice = $triggerProductData['base_price'];

            $seller = $this->customerRepositoryInterface->getById($sellerId);
            $assemblyPrice = $this->helperMobiliario->getAssemblyPriceforProduct($product, $seller) ;

            $newPrice = ($assemblyPrice > 0 ) ? $newBasePrice + $assemblyPrice : $newBasePrice;
            $newData = $triggerProductData;
            $newData['price'] = $newPrice;
            $newData['base_price'] = $newBasePrice;
            $newData['assembly_price'] = $assemblyPrice;
            $newData['product_id'] = $product->getEntityId();
            $newData['seller_id'] = $sellerId;

            $params = [
                'idOrganismo' => $serviceData['idOrganismo'],
                'idProducto' => $product->getSku(),
                'idConvenioMarco' => $serviceData['idConvenioMarco'],
                'idEstadoProductoProveedorConvenio' =>  $serviceData['idEstadoProductoProveedorConvenio'],
                'tieneStock' => (float)$triggerProductData['qty'] > 0 ? true : false, //no existe copia la info del trigger
                'precio' => (float)$newPrice
            ];

            $validateServiceAssign = $this->_assignHelper->setAssignProduct($serviceData['token'], $params);

            if (isset($validateServiceAssign['success']) && $validateServiceAssign['success'] == 'OK') {
//                no se realizan validciones, se realizaron con el producto original
//                $result = $this->_assignHelper->validateData($data, $productType, true, (bool)$flag);
            }

            $result = $this->_assignHelper->processAssignProduct($newData, $productType, $flag);
            $this->_assignHelper->processProductStatus($result);
            $this->_assignHelper->manageImages($newData, $result);
            (__('Product is saved successfully.'));

        }
        if (!$result['error']){
            $result['msg'] .= __('Data was saved succesfully in Product SKU: %1. ' ,$product->getSku());
        }else{
            $result['msg'] .= __('Data with error in Product SKU: %1 - Seller: %2. ' ,$product->getSku(), $sellerId);
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function isBusinessDays()
    {
        return $this->getConfig(self::XML_PATH_BUSINESS_DAYS);
    }

    public function checkForSpecificSpecialOfferOnAssociate($productId, $assignId, $status = 3) {
        $collection = $this->_offerCollection->create()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('assign_id', $assignId)
            ->addFieldToFilter('status', $status);

        if ($collection->getSize()) {
            $startDate = new \DateTime($collection->getFirstItem()->getStartDate());
            $endDate = new \DateTime($collection->getFirstItem()->getEndDate());
            return [
                'has_offer' => true,
                'start_date' => $startDate->format('Y-m-d'),
                'end_date' => $endDate->format('Y-m-d'),
                'days' => $collection->getFirstItem()->getDays(),
                'price' => $collection->getFirstItem()->getBasePrice(),
                'special_price' => $collection->getFirstItem()->getSpecialPrice(),
                'id' => $collection->getFirstItem()->getId()
            ];
        } else {
            return ['has_offer' => false];
        }
    }

    public function checkIfAssociateHasSpecialOffer($kitem, $status = 3)
    {
        $collection = $this->_offerCollection->create()
            ->addFieldToFilter('product_id', $kitem)
            ->addFieldToFilter('status', $status);

        $sellerId = $this->getSellerId();
        if ($sellerId) {
            $collection->addFieldToFilter('seller_id', $sellerId);
        }

        if ($collection->getSize()) {
            $startDate = new \DateTime($collection->getFirstItem()->getStartDate());
            $endDate = new \DateTime($collection->getFirstItem()->getEndDate());
            return [
                'has_offer' => true,
                'start_date' => $startDate->format('Y-m-d'),
                'end_date' => $endDate->format('Y-m-d'),
                'days' => $collection->getFirstItem()->getDays(),
                'price' => $collection->getFirstItem()->getBasePrice(),
                'special_price' => $collection->getFirstItem()->getSpecialPrice(),
                'id' => $collection->getFirstItem()->getId()
            ];
        } else {
            return ['has_offer' => false];
        }
    }

    /**
     * @param $configPath
     * @return mixed
     */
    protected function getConfig($configPath)
    {
        return $this->scopeConfig->getValue($configPath, ScopeInterface::SCOPE_STORE);
    }
}
