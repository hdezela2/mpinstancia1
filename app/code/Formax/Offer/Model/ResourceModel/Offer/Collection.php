<?php

namespace Formax\Offer\Model\ResourceModel\Offer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Formax\Offer\Model\Offer',
            'Formax\Offer\Model\ResourceModel\Offer'
        );
    }
}
