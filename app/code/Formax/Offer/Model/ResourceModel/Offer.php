<?php

namespace Formax\Offer\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Offer extends AbstractDb
{
    public function _construct()
    {
        $this->_init('dccp_offer_product', 'id');
    }
}