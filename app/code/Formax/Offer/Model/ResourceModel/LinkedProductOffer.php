<?php
/**
 * @namespace   Formax
 * @module      Offer
 * @author      Dario Grau
 * @email       dgrau@cleveritgroup.com
 * @date        07/07/2020
 */

namespace Formax\Offer\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class LinkedProductOffer extends AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    public function _construct()
    {
        $this->_init('dccp_linked_product_offer', 'id');
    }
}