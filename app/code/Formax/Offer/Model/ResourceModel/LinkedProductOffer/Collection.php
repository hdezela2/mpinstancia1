<?php
/**
 * @namespace   Formax
 * @module      Offer
 * @author      Dario Grau
 * @email       dgrau@cleveritgroup.com
 * @date        07/07/2020
 */

namespace Formax\Offer\Model\ResourceModel\LinkedProductOffer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'formax_offer_dccp_linked_product_offer_collection';
    protected $_eventObject = 'dccp_linked_product_offer_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Formax\Offer\Model\LinkedProductOffer',
            'Formax\Offer\Model\ResourceModel\LinkedProductOffer'
        );
    }
}
