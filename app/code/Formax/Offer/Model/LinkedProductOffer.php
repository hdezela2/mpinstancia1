<?php
/**
 * @namespace   Formax
 * @module      Offer
 * @author      Dario Grau
 * @email       dgrau@cleveritgroup.com
 * @date        07/07/2020
 */

namespace Formax\Offer\Model;

use Magento\Framework\Model\AbstractModel;

class LinkedProductOffer extends AbstractModel
{
    /**
     * Linked Product Offer cache tag.
     */
    const CACHE_TAG = 'dccp_linked_product_offer';

    /**
     * @var string
     */
    protected $_cacheTag = 'dccp_linked_product_offer';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'dccp_linked_product_offer';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\Offer\Model\ResourceModel\LinkedProductOffer::class);
    }
}