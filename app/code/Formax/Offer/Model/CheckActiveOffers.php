<?php
namespace Formax\Offer\Model;

use Exception;
use Formax\Offer\Model\ResourceModel\Offer\CollectionFactory as OfferCollectionFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;

class CheckActiveOffers
{
    const PENDIGN = 1;
    const ACTIVE = 3;

    /**
     * @var OfferCollectionFactory
     */
    protected OfferCollectionFactory $offerCollectionFactory;

    /**
     * @var ProductRepository
     */
    protected ProductRepository $productRepository;

    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManager;

    /**
     * @var CustomerCollectionFactory
     */
    protected CustomerCollectionFactory $customerCollectionFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    protected CustomerRepositoryInterface $customerRepository;

    public function __construct
    (
        OfferCollectionFactory $offerCollectionFactory,
        ProductRepository $productRepository,
        StoreManagerInterface $storeManager,
        CustomerCollectionFactory $customerCollectionFactory,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->offerCollectionFactory = $offerCollectionFactory;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->customerRepository = $customerRepository;
    }

    /**
     * GET for Post api
     * @param string $sku
     * @param string $agreement
     * @param string $orgCode
     * @return string
     */
    public function getPost(string $sku, string $agreement, string $orgCode): string
    {
//        'orgCode' es un custom attribute de customer entity, el nombre del campo es 'wkv_dccp_id'
//        Devuelve 1 cuando tiene oferta especial vigente (pendiente o activa).
//        Devuelve 0 cuando NO tiene oferta especial vigente (pendiente o activa).
//        Devuelve error (HTTP 204) cuando la consulta CM-oferta-Proveedor no encuentra datos de ofertas (no especiales)
//        Para el resto de los errores se debe contestar un error genérico.
        try {
            $result = "";
            $product = $this->productRepository->get($sku);

            $storesCodes = $this->storeManager->getStores();
            foreach ($storesCodes as $storeCode){
                if($storeCode->getAgreementCode() == $agreement){
                    $websiteId = $storeCode->getWebsiteId();
                }
            }

            //customer->entityId = sellerId
            if(isset($websiteId)){
                $customer = $this->customerCollectionFactory->create()
                                ->addAttributeToSelect('*')
                                ->addAttributeToFilter('wkv_dccp_id', $orgCode)
                                ->addAttributeToFilter('website_id', $websiteId)
                                ->getFirstItem();
                $sellerId = $customer->getId();
            }

            if(isset($websiteId) && isset($sellerId)){
                $offerCollectionFactory = $this->offerCollectionFactory->create()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('product_id',
                        ['eq' => $product->getId()])
                    ->addFieldToFilter('website_id',
                        ['eq' => $websiteId])
                    ->addFieldToFilter('seller_id',
                        ['eq' => $sellerId]);

                $offers = $offerCollectionFactory->getData();
            }

            if(!empty($offers)){
                foreach ($offers as $offer){
                    $offerStatus = $offer["status"];
                    if($offerStatus == self::PENDIGN || $offerStatus == self::ACTIVE){
                        $result = 1;
                    } else {
                        $result = 0;
                    }
                }
            } else {
//                $result = "No pending or active offers associated with the data entered were found: ".
//                    " SKU: ".$sku.
//                    " Convenio: ".$agreement.
//                    " Proveedor: ".$sellerId;
                $result = -1;
            }

        }
        catch (Exception $e) {
            echo "Oferta: ",  $e->getMessage(), "\n";
            $result = $e;
        }

        return $result;
    }
}
