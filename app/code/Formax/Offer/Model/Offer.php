<?php

namespace Formax\Offer\Model;

use Magento\Framework\Model\AbstractModel;

class Offer extends AbstractModel
{
    const OFFER_CREATED = 1;
    const OFFER_ERROR = 2;
    const OFFER_APPLIED = 3;
    const OFFER_EXPIRED = 4;

    /**
     * Offer Product cache tag.
     */
    const CACHE_TAG = 'dccp_offer';

    /**
     * @var string
     */
    protected $_cacheTag = 'dccp_offer';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'dccp_offer';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\Offer\Model\ResourceModel\Offer::class);
    }
}