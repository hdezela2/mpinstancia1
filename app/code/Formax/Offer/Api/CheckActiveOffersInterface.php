<?php

namespace Formax\Offer\Api;

interface CheckActiveOffersInterface
{
    /**
     * GET for Post api
     * @param string $sku
     * @param string $agreement
     * @param string $orgCode
     * @return string
     */

    public function getPost(string $sku, string $agreement, string $orgCode): string;
}
