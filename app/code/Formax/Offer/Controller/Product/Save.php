<?php

namespace Formax\Offer\Controller\Product;

use Cleverit\ComputadoresRenewal\Constants as ComputadoresRenewalConstants;
use Intellicore\ComputadoresRenewal2\Constants as ComputadoresRenewal2Constants;
use Formax\Offer\Model\Offer;
use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Linets\GasSetup\Constants;
use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Formax\Offer\Helper\Data as HelperOffer;
use Formax\Offer\Model\ResourceModel\Offer\CollectionFactory as OfferCollection;
use Summa\EmergenciasSetUp\Helper\Data;
use Summa\ProductOfferLimit\Helper\Data as ProductOfferLimit;
use Summa\StockManagementConfiguration\Helper\Data as StockHelper;
use Summa\CombustiblesSetUp\Helper\Data as Combustibles;
use Chilecompra\CombustiblesRenewal\Model\Constants as CombustiblesRenewal202110;
use Webkul\MpAssignProduct\Helper\Data as MpAssignProductHelper;
use Webkul\MpAssignProduct\Model\ItemsFactory;
use Linets\Offer\Helper\Data as GasDataHelper;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Linets\PriceManagement\Constants as PriceManagementConstants;
use Linets\PriceManagement\Helper\Config as GasPriceHelper;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollection;
use Linets\VehiculosSetUp\Model\Constants as VehiculosConstants;
use Formax\AssignProduct\Block\Product\Add;
use Formax\AssignProduct\Helper\MpAssignProduct as AssignProductHelper;
use Formax\RegionalCondition\Block\RegionalCondition\Add as RegionalCondition;
use Chilecompra\LoginMp\Helper\Helper as ChilecompraLoginHelper;
use Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface;
use Linets\InsumosTempPriceAdjustment\Helper\Data as InsumosTempPriceAdjustmentHelper;
use Linets\SellerCentralizedDispatchCost\Helper\Data as SellerCentralizedDispatchHelper;

class Save extends \Formax\AssignProduct\Controller\Product\Save
{
    /** @var MpAssignProductHelper */
    protected $_mpAssignProductHelper;

    /**
     * @var HelperOffer
     */
    protected $helperOffer;

    /**
     * @var \Formax\Offer\Model\OfferFactory
     */
    protected $offer;

    /** @var OfferCollection */
    protected $_offerCollection;

    /** @var ItemsFactory */
    protected $_itemsFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepositoryInterface;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var ProductOfferLimit
     */
    protected $_productOfferLimit;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Formax\AssignProduct\Model\LinkedProductFactory
     */
    protected $linkedProductFactory;

    /**
     * @var \Summa\StockManagementConfiguration\Helper\Data
     */
    protected $stockManagementHelper;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $helperAssign;

    /**
     * @var AssociatesCollection
     */
    protected $asociatesCollection;

    /**
     * @var GasPriceHelper
     */
    protected $gasPriceHelper;

    /**
     * @var AssignProductHelper
     */
    protected $assignProductHelper;

    /**
     * @var InsumosTempPriceAdjustmentHelper
     */
    protected $insumosTempPriceHelper;

    /**
     * @var SellerCentralizedDispatchHelper
     */
    protected $sellerCentDispatchHelper;


    /**
     * @param Context $context
     * @param \Magento\Customer\Model\Url $url
     * @param \Magento\Customer\Model\Session $session
     * @param MpAssignProductHelper $helper
     * @param \Magento\Catalog\Model\Product\Copier $productCopier
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param StockConfigurationInterface $stockConfiguration
     * @param StockRegistryInterface $stockRegistry
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Model\Product\Gallery\Processor $imageProcessor
     * @param \Magento\Catalog\Model\ResourceModel\Product\Gallery $productGallery
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Formax\AssignProduct\Model\AssignProductFactory $assignProduct
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Formax\StatusChange\Helper\Data $helperSellerStatus
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Formax\Offer\Model\OfferFactory $offer
     * @param OfferCollection $offerCollection
     * @param ItemsFactory $itemsFactory
     * @param Filesystem $filesystem
     * @param HelperOffer $helperOffer
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ProductOfferLimit $productOfferLimit
     * @param StockHelper $stockManagementHelper
     * @param \Formax\AssignProduct\Model\LinkedProductFactory $linkedProductFactory
     * @param AssociatesCollection $asociatesCollection
     * @param GasPriceHelper $gasPriceHelper
     * @param AssignProductHelper $assignProductHelper
     * @param Add $assignProductAdd
     * @param RegionalCondition $regionalCondition
     * @param InsumosTempPriceAdjustmentHelper $insumosTempPriceHelper
     * @param SellerCentralizedDispatchHelper $sellerCentDispatchHelper
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Url $url,
        \Magento\Customer\Model\Session $session,
        MpAssignProductHelper $helper,
        \Magento\Catalog\Model\Product\Copier $productCopier,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        StockConfigurationInterface $stockConfiguration,
        StockRegistryInterface $stockRegistry,
        ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\Product\Gallery\Processor $imageProcessor,
        \Magento\Catalog\Model\ResourceModel\Product\Gallery $productGallery,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Formax\AssignProduct\Model\AssignProductFactory $assignProduct,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Formax\StatusChange\Helper\Data $helperSellerStatus,
        \Magento\Framework\App\ResourceConnection $resource,
        \Psr\Log\LoggerInterface $logger,
        \Formax\Offer\Model\OfferFactory $offer,
        OfferCollection $offerCollection,
        ItemsFactory $itemsFactory,
        Filesystem $filesystem,
        HelperOffer $helperOffer,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductOfferLimit $productOfferLimit,
        StockHelper $stockManagementHelper,
        \Formax\AssignProduct\Model\LinkedProductFactory $linkedProductFactory,
        \Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory $asociatesCollection,
        GasPriceHelper $gasPriceHelper,
        AssignProductHelper $assignProductHelper,
        Add $assignProductAdd,
        RegionalCondition $regionalCondition,
        InsumosTempPriceAdjustmentHelper $insumosTempPriceHelper,
        SellerCentralizedDispatchHelper $sellerCentDispatchHelper
    )
    {
        $this->_mpAssignProductHelper = $helper;
        $this->helperOffer = $helperOffer;
        $this->offer = $offer;
        $this->_offerCollection = $offerCollection;
        $this->_itemsFactory = $itemsFactory;
        $this->productRepositoryInterface = $productRepositoryInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_storeManager = $storeManager;
        $this->_productOfferLimit = $productOfferLimit;
        $this->stockManagementHelper = $stockManagementHelper;
        $this->linkedProductFactory = $linkedProductFactory;
        $this->helperAssign = $helper;
        $this->asociatesCollection = $asociatesCollection;
        $this->gasPriceHelper = $gasPriceHelper;
        $this->assignProductHelper = $assignProductHelper;
        $this->assignProductAdd = $assignProductAdd;
        $this->regionalCondition = $regionalCondition;
        $this->insumosTempPriceHelper = $insumosTempPriceHelper;
        $this->sellerCentDispatchHelper = $sellerCentDispatchHelper;

        parent::__construct(
            $context,
            $url,
            $session,
            $helper,
            $productCopier,
            $mpHelper,
            $stockConfiguration,
            $stockRegistry,
            $productRepository,
            $productFactory,
            $imageProcessor,
            $productGallery,
            $storeManager,
            $assignProduct,
            $fileUploaderFactory,
            $customerRepositoryInterface,
            $helperSellerStatus,
            $resource,
            $logger,
            $filesystem
        );
    }

    public function execute()
    {
        $isInsumos = $this->_storeManager->getWebsite()->getCode() == InsumosConstants::WEBSITE_CODE;
        $isAseoRenewal = $this->_storeManager->getWebsite()->getCode() == AseoRenewalConstants::WEBSITE_CODE;
        $websiteCode = $this->_storeManager->getWebsite()->getCode();
        $approvedRequiredSites = [
            InsumosConstants::WEBSITE_CODE,
            \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE,
            ConstantsEmergency202109::WEBSITE_CODE,
            VehiculosConstants::WEBSITE_CODE,
            AseoRenewalConstants::WEBSITE_CODE
        ];
        $this->connection->beginTransaction();

        try {
            $helper = $this->_assignHelper;
            $data = $this->getRequest()->getParams();
            if (!in_array($websiteCode, $approvedRequiredSites)) {
                $files = $this->getRequest()->getFiles('products');
            }
            $data['image'] = '';
            if (isset($data['base_price']) && isset($data['shipping_price'])) {
                $data['price'] = $data['base_price'] + $data['shipping_price'];
            }

            if (isset($data['base_price']) && isset($data['assembly_price'])) {
                $data['price'] = $data['base_price'] + $data['assembly_price'];
            }
            $isVoucher = false;
            if ($this->storeManager->getStore()->getCode() == 'voucher') {
                $isVoucher = true;
            }

            $isCombustibles = false;
            if (
                $this->storeManager->getStore()->getCode() == 'combustibles' ||
                $this->storeManager->getStore()->getCode() == CombustiblesRenewal202110::STORE_CODE
            ) {
                $isCombustibles = true;
            }

            $dccpStatus = $this->helperSellerStatus->getDccpStatusSeller();
            $productId = $data['product_id'];
            $product = $helper->getProduct($productId);
            $productType = $product->getTypeId();
            $customerId = $this->getCustomerLoggedId();
            $sellerId = $helper->getSellerIdBySubAccount();
            if ($isInsumos) {
                $dispatchPricesProducts = $this->sellerCentDispatchHelper->getChildDistpatchPrices($productId, $sellerId, $this->storeManager->getWebsite()->getId());
            }
            if ($productType == 'configurable') {//hot fix change in validation form Edit and Add for Alimentos.
                $stockValidation['error'] = false;
                $itemsError = [];
                foreach ($data['products'] as $kItem => $item) {
                    if ($isInsumos) {
                        if (!isset($item['qty']) && (empty($item['price']) && (!isset($item[DispatchPriceInterface::BASE_PRICE]) || empty($item[DispatchPriceInterface::BASE_PRICE])))) {
                            unset($data['products'][$kItem]);
                            continue;
                        }

                        $priceMaxByProductSimple = $helper->getProduct($kItem)->getPrecioMaximo() ?? null;

                    } else {
                        if (!isset($item['qty']) && empty($item['price'])) {
                            unset($data['products'][$kItem]);
                            continue;
                        }

                        if (isset($item['associate_id']) && !isset($item['qty']) && $item['old_status'] == 0) {
                            unset($data['products'][$kItem]);
                            continue;
                        }
                    }

                    if ($this->storeManager->getWebsite()->getCode() == ChilecompraLoginHelper::CODE_AGREEMENT_MED_SUPPLIES &&
                        isset($item[DispatchPriceInterface::BASE_PRICE]) &&
                        isset($item[DispatchPriceInterface::DISPATCHPRICE]) &&
                        !empty($item[DispatchPriceInterface::DISPATCHPRICE])) {
                        if (is_numeric($item[DispatchPriceInterface::BASE_PRICE])) {
                            $finalPriceInsumos = $item[DispatchPriceInterface::BASE_PRICE] + $item[DispatchPriceInterface::DISPATCHPRICE];
                            $data['products'][$kItem]['price'] = $finalPriceInsumos;
                            $item['price'] = $finalPriceInsumos;
                        }
                    }
                    //$checkHasSpecialOffer = $this->helperOffer->checkIfAssociateHasSpecialOffer($kItem);

                    if (!in_array($websiteCode, $approvedRequiredSites)) {
                        //$checkHasSpecialOffer = $this->helperOffer->checkIfAssociateHasSpecialOffer($kItem);
                        $productIds = "document_" . $kItem;
                        $isChildProductAssignedSellerVisible = $this->assignProductHelper->isChildProductAssignedSellerVisible($kItem, $productId);

                        //if ($checkHasSpecialOffer['has_offer']) {
                        //                        $data['products'][$kItem]['price'] = $checkHasSpecialOffer['price'];
                        //}
                        if (!$isChildProductAssignedSellerVisible &&
                            !empty($item['price']) &&
                            ($files[$productIds]['name'] == '' && (isset($item['has_file_attached']) && $item['has_file_attached'] != 1))
                        ) {
                            $stockValidation['error'] = true;
                            $childProduct = $helper->getProduct($kItem);
                            $itemsError[] = __('The Price Document is mandatory to offer product %1', $childProduct->getName());
                        } elseif (
                            ($files[$productIds]['name'] != '' && empty($item['price'])) ||
                            (isset($item['qty']) && $item['qty'] > 0 && empty($item['price']))
                        ) {
                            $stockValidation['error'] = true;
                            $childProduct = $helper->getProduct($kItem);
                            $itemsError[] = __('The price is mandatory to offer product %1', $childProduct->getName());
                        } elseif (
                            !empty($item['price']) && !isset($item['qty'])
                        ) {
                            $stockValidation['error'] = true;
                            $childProduct = $helper->getProduct($kItem);
                            $itemsError[] = __('The stock is mandatory to offer product %1', $childProduct->getName());
                        }
                    } else {
                        $checkHasSpecialOffer = $this->helperOffer->checkIfAssociateHasSpecialOffer($kItem);

                        if ($checkHasSpecialOffer['has_offer']) {
                            //                        $data['products'][$kItem]['price'] = $checkHasSpecialOffer['price'];
                        } elseif (!isset($item['associate_id']) && !isset($item['qty']) && !empty($item['price'])) {
                            $stockValidation['error'] = true;
                            $childProduct = $helper->getProduct($kItem);
                            $itemsError[] = __('The input “Stock available” is mandatory - offer %1', $childProduct->getName());
                        } elseif (!isset($item['associate_id']) && isset($item['qty']) && empty($item['price'])) {
                            $stockValidation['error'] = true;
                            $childProduct = $helper->getProduct($kItem);
                            $itemsError[] = __('New offers need to be priced - offer %1', $childProduct->getName());
                        }
                        if ($isInsumos && !isset($item['associate_id']) && isset($item['price']) && is_null
                            ($dispatchPricesProducts[$kItem])) {
                            $stockValidation['error'] = true;
                            $childProduct = $helper->getProduct($kItem);
                            $itemsError[] = __('In order to incorporate the %1 offer you must have the dispatch price charged.', $childProduct->getName());
                        }
                        if ((isset($priceMaxByProductSimple) && !empty($priceMaxByProductSimple)) &&
                            ((int)$item['base_price'] > (int)$priceMaxByProductSimple)) {
                            $stockValidation['error'] = true;
                            $itemsError[] = __('The maximum price for a new offer is %1: Offer %2', $priceMaxByProductSimple, $helper->getProduct($kItem)->getName());
                        }
                    }
                }

                if ($stockValidation['error'] == true) {
                    foreach ($itemsError as $kItem => $item) {
                        $this->messageManager->addError($item);
                    }
                    $this->connection->rollBack();
                    return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                }
            }

            $customer = $this->customerRepositoryInterface->getById($sellerId);
            $customerLoguedIn = $this->customerRepositoryInterface->getById($customerId);

            $token = $customerLoguedIn->getCustomAttribute('user_rest_atk') ?
                $customerLoguedIn->getCustomAttribute('user_rest_atk')->getValue() : '';
            $userId = $customer->getCustomAttribute('user_rest_id') ?
                $customer->getCustomAttribute('user_rest_id')->getValue() : '';
            $agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
                $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : '';
            $sellerStatus = $customer->getCustomAttribute('wkv_dccp_state_details') ?
                $customer->getCustomAttribute('wkv_dccp_state_details')->getValue() : '';
            $dccpSellerStatus = isset($dccpStatus[$sellerStatus]) ? $dccpStatus[$sellerStatus] : 0;

            //if you don't have the condition you can't add products with_stock=NO then need to be qty > 0
            if (!$this->stockConditionsMatched()) {
                if ($productType == 'configurable') {
                    $stockValidation['error'] = false;
                    $itemsError = [];
                    foreach ($data['products'] as $kItem => $item) {
                        if (!isset($item['associate_id']) && $item['qty'] == 0 && !empty($item['price'])) {
                            //item nuevo - pero sin stock
                            $stockValidation['error'] = true;
                            $itemsError[] = __('It is not possible to add new offers without stock, the offer with price %1 must have stock', $item['price']);
                        } elseif (isset($item['associate_id'])) {
                            $oldStatus = $this->helperAssign->getOldStatusConfigurable($kItem);
                            if ($oldStatus > 0 && $item['qty'] == 0) {
                                $stockValidation['error'] = true;
                                $arrayWebsiteCodes = [
                                    VehiculosConstants::WEBSITE_CODE,
                                    InsumosConstants::WEBSITE_CODE,
                                    Constants::GAS_WEBSITE_CODE,
                                    AseoRenewalConstants::WEBSITE_CODE
                                ];
                                if (in_array($this->storeManager->getWebsite()->getCode(), $arrayWebsiteCodes)) {
                                    $itemsError[] = $this->stockManagementHelper->getStockDisableText($this->_storeManager->getStore()->getWebsiteId());
                                } else {
                                    $itemsError[] = __('The stock option is not available yet, because the necessary time has not yet passed !!');
                                }
                            }
                        }
                    }
                    if ($stockValidation['error'] == true) {
                        foreach ($itemsError as $kItem => $item) {
                            $this->messageManager->addError($item);
                        }
                        $this->connection->rollBack();
                        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                    }
                }

                if ($productType != 'configurable') {
                    if ((float)$data['qty'] <= 0) {
                        $this->messageManager->addError($this->stockManagementHelper->getStockDisableText($this->_storeManager->getStore()->getWebsiteId()));
                        $this->messageManager->addError('There was a problem with the Stock configuration of this product');
                        $this->connection->rollBack();
                        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                    }
                }
            }

            /** Prices validation */
            if ($productType != 'configurable') {
                if (!isset($data['assign_id'])) {
                    /** Validate first price */
                    $result = $helper->validateFirstPrice($data);
                    if ($result['error']) {
                        $this->messageManager->addErrorMessage(__($result['msg']));
                        $this->connection->rollBack();
                        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                    }
                } else {
                    /** validate assign product price in computadores */
                    if ($this->storeManager->getWebsite()->getCode() == \Summa\ComputadoresSetUp\Helper\Data::WEBSITE_CODE ||
                        $this->storeManager->getWebsite()->getCode() == ComputadoresRenewalConstants::WEBSITE_CODE ||
                        $this->storeManager->getWebsite()->getCode() == ComputadoresRenewal2Constants::WEBSITE_CODE ||
                        $this->storeManager->getWebsite()->getCode() == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) {
                        $assignData = $helper->getAssignDataByAssignId($data['assign_id']);
                        if (!empty($assignData->getPrice()) && $assignData->getStatus() == '1' && $data['price'] > $assignData->getPrice()) {
                            $this->messageManager->addErrorMessage(__('The price cannot be higher than %1', $helper->currencyFormat($assignData->getPrice())));
                            $this->connection->rollBack();
                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                        }
                    }
                }
            } else {
                /** Price validation on configurable products */
                foreach ($data['products'] as $childId => $item) {
                    if (!isset($item['associate_id']) && !empty($item['price'])) {
                        if (!$isInsumos || !$isAseoRenewal) {
                            $childProduct = $helper->getProduct($childId);

                            // Validate Prices based on Product Situation on CM Emergency 202109
                            if ($this->storeManager->getWebsite()->getCode() == ConstantsEmergency202109::WEBSITE_CODE) {
                                // get the Region that we are going to check the Situation prices
                                $regionToCompare = '';
                                foreach ($data['products'] as $value) {
                                    if (isset($value['id']) && $value['id'] = '1') {
                                        $regionToCompare = $value['region'];
                                    }
                                }
                                // get the situation prices
                                $prevencionPrice = 0;
                                $retiroPrice = 0;
                                $emergencyPrice = 0;
                                foreach ($data['products'] as $value) {
                                    if ($value['situation'] == 'Prevención' &&
                                        $value['region'] == $regionToCompare &&
                                        $value['price'] != 0) {
                                        $prevencionPrice = $value['price'];
                                    } else if ($value['situation'] == 'Retiro en Tienda' &&
                                        $value['region'] == $regionToCompare &&
                                        $value['price'] != 0) {
                                        $retiroPrice = $value['price'];
                                    } else if ($value['situation'] == 'Emergencia' &&
                                        $value['region'] == $regionToCompare &&
                                        $value['price'] != 0) {
                                        $emergencyPrice = $value['price'];
                                    }
                                }
                                // validate prices based on situation
                                switch ($item['situation']) {
                                    case 'Retiro en Tienda':
                                        if ($prevencionPrice != 0 && $item['price'] >= $prevencionPrice) {
                                            $this->messageManager->addError(__('Store Pick Up Price cannot be higher or equal than %1 (Prevention Price) for product %2', $helper->currencyFormat($prevencionPrice), $helper->getProduct($childId)->getName()));
                                            $this->connection->rollBack();
                                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                        }
                                        if ($emergencyPrice != 0 && $item['price'] >= $emergencyPrice) {
                                            $this->messageManager->addError(__('Store Pick Up Price cannot be higher or equal than %1 (Emergency Price) for product %2', $helper->currencyFormat($emergencyPrice), $helper->getProduct($childId)->getName()));
                                            $this->connection->rollBack();
                                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                        }
                                        break;
                                    case 'Prevención':
                                        if ($retiroPrice != 0 && $item['price'] <= $retiroPrice) {
                                            $this->messageManager->addError(__('Prevention Price cannot be lower or equal than %1 (Store Pick Up Price) for product %2', $helper->currencyFormat($retiroPrice), $helper->getProduct($childId)->getName()));
                                            $this->connection->rollBack();
                                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                        }
                                        if ($emergencyPrice != 0 && $item['price'] >= $emergencyPrice) {
                                            $this->messageManager->addError(__('Prevention Price cannot be higher or equal than %1 (Emergency Price) for product %2', $helper->currencyFormat($emergencyPrice), $helper->getProduct($childId)->getName()));
                                            $this->connection->rollBack();
                                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                        }
                                        break;
                                    case 'Emergencia':
                                        if ($prevencionPrice != 0 && $item['price'] <= $prevencionPrice) {
                                            $this->messageManager->addError(__('Emergency Price cannot be lower or equal than %1 (Prevention Price) for product %2', $helper->currencyFormat($prevencionPrice), $helper->getProduct($childId)->getName()));
                                            $this->connection->rollBack();
                                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                        }
                                        if ($retiroPrice != 0 && $item['price'] <= $retiroPrice) {
                                            $this->messageManager->addError(__('Emergency Price cannot be lower or equal than %1 (Store Pick Up Price) for product %2', $helper->currencyFormat($retiroPrice), $helper->getProduct($childId)->getName()));
                                            $this->connection->rollBack();
                                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                        }
                                        break;
                                }
                            }
                        }


                        //finding min price for status = 1 offers
                        $collectionAsociates = $this->asociatesCollection->create()
                            ->addFieldToFilter('product_id', $childId)
                            ->addFieldToFilter('status', 1)
                            ->addFieldToFilter('qty', ['gt' => 0]);

                        $validAssociateId = [];
                        // Search for disapproved sellers in offers
                        foreach ($collectionAsociates as $associated) {
                            $parentItemId = $associated->getParentId();
                            $assignItem = $helper->getAssignProduct($parentItemId);
                            $sellerId = $assignItem->getSellerId();
                            if (empty($sellerId)) {
                                continue;
                            }
                            $sellerStatus = $helper->getSellerStatus($sellerId);
                            if ($sellerStatus == '70') {
                                $validAssociateId[] = $associated->getId();
                            }
                        }
                        // Reload and remove from collection offers with disapproved seller
                        $collectionAsociates = $this->asociatesCollection->create()
                            ->addFieldToFilter('product_id', $childId)
                            ->addFieldToFilter('status', 1)
                            ->addFieldToFilter('qty', ['gt' => 0])
                            ->addFieldToFilter('id', ['in' => $validAssociateId]);

                        // Validate prices when there exists offers visible in front store
                        if (!empty($collectionAsociates) && $collectionAsociates->getSize() > 0) {
                            // Average Price Validation / Price Percentage Validation / Minimum Price Validation
                            if ($this->storeManager->getWebsite()->getCode() ==
                                ConstantsEmergency202109::WEBSITE_CODE || $isInsumos) {
                                // In Emergency Store, we validate the price with the average price of current offers on product

                                // get base price of offers in case they have a special offer active
                                $offerBasePrices = [];
                                foreach ($collectionAsociates as $assoc) {
                                    // check if associate has an active special offer
                                    $hasOffer = $this->helperOffer->checkForSpecificSpecialOfferOnAssociate($assoc['product_id'], $assoc['parent_id']);
                                    if ($hasOffer['has_offer']) {
                                        array_push($offerBasePrices, $hasOffer['price']);
                                    } else {
                                        array_push($offerBasePrices, $assoc['price']);
                                    }
                                }
                                $averagePrice = array_sum($offerBasePrices) / count($offerBasePrices);
                                $averagePrice = round($averagePrice);

                                if ($item['price'] > $averagePrice) {
                                    $childProduct = $helper->getProduct($childId);
                                    $this->messageManager->addError(__('Price cannot be higher than %1 (Average Price) for product %2', $helper->currencyFormat($averagePrice), $childProduct->getName()));
                                    $this->connection->rollBack();
                                    return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                }
                            } else {
                                $minPriceOffer = null;
                                $totalOffersPrices = 0;
                                $offersCount = 0;

                                if ($this->_storeManager->getStore()->getCode() == AseoRenewalConstants::STORE_CODE) {
                                    // get base price of offers in case they have a special offer active
                                    $offerBasePrices = [];
                                    foreach ($collectionAsociates as $assoc) {
                                        // check if associate has an active special offer
                                        $hasOffer = $this->helperOffer->checkForSpecificSpecialOfferOnAssociate($assoc['product_id'], $assoc['parent_id']);
                                        if ($hasOffer['has_offer']) {
                                            array_push($offerBasePrices, $hasOffer['price']);
                                        } else {
                                            array_push($offerBasePrices, $assoc['price']);
                                        }
                                    }

                                    foreach ($offerBasePrices as $offerPrice) {
                                        $totalOffersPrices += $offerPrice;
                                        $offersCount++;
                                        if ($minPriceOffer == null || $offerPrice < $minPriceOffer) {
                                            $minPriceOffer = $offerPrice;
                                        }
                                    }
                                } else {
                                    foreach ($collectionAsociates as $offer) {
                                        $totalOffersPrices += $offer->getPrice();
                                        $offersCount++;
                                        if ($minPriceOffer == null || $offer->getPrice() < $minPriceOffer) {
                                            $minPriceOffer = $offer->getPrice();
                                        }
                                    }
                                }

                                if ($this->storeManager->getWebsite()->getCode() == Constants::GAS_WEBSITE_CODE) {
                                    // On CM Gas we calculate the percentage price difference
                                    if ($helper->getPricePercentage($item['price'], (int)$minPriceOffer) > 3) {
                                        $this->messageManager->addError(__('The price cannot be higher than 3% of %1', $helper->currencyFormat($minPriceOffer)));
                                        $this->connection->rollBack();
                                        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                    }
                                } else {
                                    // In other CM we validate with the minimum offer price
                                    if ($item['price'] >= $minPriceOffer) {
                                        $childProduct = $helper->getProduct($childId);
                                        $this->messageManager->addError(__('Price cannot be higher or equal than %1 for product %2', $helper->currencyFormat($minPriceOffer), $childProduct->getName()));
                                        $this->connection->rollBack();
                                        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                    }
                                }
                            }
                        } else {
                            // if product does not have offers, then check maximum base price on CM Emergency Store
                            if ($this->storeManager->getWebsite()->getCode() == ConstantsEmergency202109::WEBSITE_CODE) {
                                $maxBasePrice = $childProduct->getMaxBasePrice();
                                if ($maxBasePrice) {
                                    if ($item['price'] >= $maxBasePrice && $maxBasePrice > 0) {
                                        $this->messageManager->addError(__('Price cannot be higher or equal than %1 (Maximum Price) for product %2', $helper->currencyFormat($maxBasePrice), $childProduct->getName()));
                                        $this->connection->rollBack();
                                        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                    }
                                }
                            }
                        }
                    } elseif (isset($item['associate_id']) && !empty($item['price'])) {
                        if ($this->storeManager->getWebsite()->getCode() ==
                            ChilecompraLoginHelper::CODE_AGREEMENT_MED_SUPPLIES || $isAseoRenewal) {
                            $associatedItem = $this->helperAssign->getAssociatedItem($item['associate_id']);
                            $associateStatus = $associatedItem->getStatus();
                            $associatePrice = (int)$associatedItem->getPrice();
                            $itemEditedPrice = (int)$item['price'];
                            if ($isInsumos) {
                                $associateBasePrice = (int)$associatedItem->getBasePrice();
                                $itemEditedBasePrice = (int)$item['base_price'];
                            }
                            $hasActivePriceAdjustment = $this->insumosTempPriceHelper->isActiveSuppliesAdjustment($sellerId, $childId, 'active');
                            if ($associateStatus == 2 && $itemEditedPrice < $associatePrice) {      // Pending status case
                                $this->messageManager->addError(__('Cannot edit price for offers with Pending status'));
                                $this->connection->rollBack();
                                return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                            } elseif ($isInsumos && $associateBasePrice < $itemEditedBasePrice) {
                                $this->messageManager->addError(__('Base price cannot be higher than %1', $associateBasePrice));
                                $this->connection->rollBack();
                                return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                            } elseif ($associateStatus == 3 && $itemEditedPrice != $associatePrice) {      // Disabled by dispersion case
                                $minPriceOffer = $this->getMinOffer($childId);
                                if ($minPriceOffer !== null) {
                                    $disPercentage = $this->getDispersionPercentage($childId, $associatePrice, $sellerId);
                                    $minDispersionPrice = round($minPriceOffer + ($minPriceOffer * $disPercentage));
                                    if ($minDispersionPrice > $itemEditedPrice) {
                                        $data['products'][$childId]['status'] = 1;
                                    } else {
                                        $childProduct = $helper->getProduct($childId);
                                        $this->messageManager->addError(__('Price cannot be higher or equal than dispersion price = %1 for product %2', $helper->currencyFormat($minDispersionPrice), $childProduct->getName()));
                                        $this->connection->rollBack();
                                        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                                    }
                                }
                            } elseif ($itemEditedPrice < $associatePrice && $hasActivePriceAdjustment) {
                                $this->messageManager->addError(__('Cannot edit price for offers active price temporary adjustments'));
                                $this->connection->rollBack();
                                return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                            }
                        }
                    }
                }
            }

            if ($this->storeManager->getWebsite()->getCode() == Constants::GAS_WEBSITE_CODE) {
                if (!$helper->isAllowedGasCategory($data) && $this->hasBeenSetOutOfStock($data, $productType)) {
                    $this->messageManager->addError(__('The products that belong to the category %1 are not available for stock editing', GasDataHelper::CATEGORY_ENVASADO_NAME));
                    $this->connection->rollBack();
                    return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                }

                foreach ($data['products'] as $childId => $item) {
                    if (isset($item['associate_id']) && !empty($item['price'])) {
                        $oldPrice = $this->getCurrentPrice($childId, $item['associate_id']);
                        $isPriceEditableDay = $this->gasPriceHelper->verifyCurrentDayAndWebsite();

                        if (!$isPriceEditableDay && (float)$item['price'] !== $oldPrice) {
                            $selectedLabels = $this->gasPriceHelper->getSelectedLabels();
                            $this->messageManager->addError(__('You can only edit prices on days: %1', implode(', ', $selectedLabels)));
                            $this->connection->rollBack();
                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                        } elseif ((float)$item['price'] > $oldPrice) {
                            $this->messageManager->addError(__('The price cannot be higher than %1', $oldPrice));
                            $this->connection->rollBack();
                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                        }
                    }
                }
            }

            // Validate Region on CM Emergencias
            if (in_array($this->storeManager->getWebsite()->getCode(), [\Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE, ConstantsEmergency202109::WEBSITE_CODE])) {
                if ($this->storeManager->getWebsite()->getCode() == ConstantsEmergency202109::WEBSITE_CODE) {
                    foreach ($data['products'] as $childId => $item) {
                        if (!isset($item['associate_id']) && !empty($item['price'])) {
                            $childProduct = $helper->getProduct($childId);
                            $regionsIds = $this->regionalCondition->getRegions($sellerId);
                            $regionsIds = array_column($regionsIds, 'region_id');
                            $regionId = $childProduct->getRegion();
                            if (in_array($regionId, $regionsIds)) {
                                $this->messageManager->addError(__('Sorry, You can not add %1 product because the region is not assigned to this seller.', $childProduct->getName()));
                                $this->connection->rollBack();
                                return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                            }
                        }
                    }
                } else {
                    $result = $helper->validateRegion($data);
                    if ($result['error']) {
                        $this->messageManager->addError(__($result['msg']));
                        $this->connection->rollBack();
                        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                    }
                }
            }

            /** Product Offers limit validation */
            if (!isset($data['assign_id']) && $this->_productOfferLimit->isEnabled()) {
                $sellerOffers = $this->_productOfferLimit->getSellerOffers($sellerId);
                if ($this->_productOfferLimit->getConfigValue() <= $sellerOffers) {
                    $this->messageManager->addError(__('You have reached the maximum offers limit for this month'));
                    $this->connection->rollBack();
                    return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                }
            }

            if ($this->storeManager->getWebsite()->getCode() == \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE) {
                $limit = $customer->getCustomAttribute('wkv_dccp_limit_of_offers') ?
                    $customer->getCustomAttribute('wkv_dccp_limit_of_offers')->getValue() : '';

                if ($productType == 'configurable') {
                    $itemAdd = 0;
                    $itemRemove = 0;
                    foreach ($data['products'] as $kItem => $item) {
                        if (!isset($item['associate_id']) && $item['qty'] > 0) {
                            //item nuevo
                            $itemAdd = $itemAdd + 1;
                        } elseif (isset($item['associate_id'])) {
                            $oldStatus = $this->helperAssign->getOldStatusConfigurable($kItem);
                            if ($oldStatus > 0 && $item['qty'] == 0) {
                                $itemRemove = $itemRemove + 1;
                            } elseif ($oldStatus <= 0 && $item['qty'] > 0) {
                                //los pendientes de la oferta se consideran nuevos tambien.
                                $itemAdd = $itemAdd + 1;
                            }
                        }
                    }
                    //no estamos contando los pendientes de otras ofertas
                    $actualCount = $this->helperAssign->countAssignOffer($sellerId);

                    if ($actualCount + $itemAdd - $itemRemove > $limit) {
                        $this->messageManager->addError(__('You have reached the maximum offers limit in your store, You need to disabe some offers to enable new offers'));
                        $this->connection->rollBack();
                        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                    }
                }
            }

            if (empty($token) || empty($userId) || empty($agreementId) || $dccpSellerStatus == 0) {
                $this->logger->warning('TOKEN: ' . $token . ' USER ID: ' . $userId . ' AGREEMENT ID: ' . $agreementId . ' SELLER DETAIL STATE: ' . $dccpSellerStatus);
                $this->messageManager->addError(__('Params missing to execute assign product service.'));
                $this->connection->rollBack();
                return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
            }

            if (!array_key_exists('product_id', $data)) {
                $this->messageManager->addError(__('Something went wrong.'));
                return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
            }

            /** Check if the offer is being updated or created */
            if (array_key_exists('assign_id', $data)) {
                // flag = 1 -> updated
                $flag = 1;
            } else {
                // flag = 0 -> created
                $flag = 0;
                $data['del'] = 0;
            }

            $result = $helper->validateData($data, $productType, !in_array($websiteCode, $approvedRequiredSites), (bool)$flag);

            if ($result['error']) {
                $this->messageManager->addError(__($result['msg']));
                $this->connection->rollBack();
                return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
            }

            $status = $data['product_condition'];
            if ($flag == 1 && array_key_exists('assign_id', $data)) {
                $assignId = $data['assign_id'];
                $model = $this->_mpAssignProductHelper->getAssignDataByAssignId($assignId);
                $status = $model->getStatus(); //recuperamos el status original.
            }

            $isPending = false;

            if ($status !== "2" || $productType == 'configurable') {
                if ($productType == 'configurable') {
                    foreach ($data['products'] as $childId => $item) {
                        try {
                            $childProduct = $helper->getProduct($childId);
                        } catch (\Exception $ex) {
                            $childProduct = $helper->getProductCollection()
                                ->addFieldToFilter('entity_id', $childId)
                                ->getFirstItem();
                        }
                        $childStatus = $helper->preProcessAssignStatusChild($childId, $data, isset($item['associate_id']));
                        $stock = (isset($item['qty']) && (float)$item['qty'] > 0) ? true : false;

                        $params = [
                            'idOrganismo' => $userId,
                            'idProducto' => $childProduct->getSku(),
                            'idConvenioMarco' => $agreementId,
                            'idEstadoProductoProveedorConvenio' => $childStatus,
                            'tieneStock' => $stock,
                            'precio' => (float)$item['price']
                        ];
                        if ($flag == 1 && isset($item['associate_id'])) {
                            $validateServiceAssign = $helper->setAssignProductUpdate($token, $params);
                            if (isset($validateServiceAssign['success']) && $validateServiceAssign['success'] == 'NOK') {
                                $validateServiceAssign = $helper->setAssignProduct($token, $params);
                            }
                        } else {
                            $validateServiceAssign = $helper->setAssignProduct($token, $params);
                            $checkData = [
                                'seller_id' => $sellerId,
                                'product_row_id' => $product->getRowId(),
                                'product_id' => $productId
                            ];

                            //se agrega validación para que se agregue el producto padre a la sincronización de MP
                            if (!$helper->checkIfItemWithSameDataExists($checkData) || ($isInsumos || $isAseoRenewal)) {
                                $configurableParams = $params;
                                $configurableParams['precio'] = 0;
                                $configurableParams['idProducto'] = $product->getSku();
                                $helper->setAssignProduct($token, $configurableParams);
                            }
                        }

                        if (!isset($validateServiceAssign['success']) || (isset($validateServiceAssign['success']) && isset($validateServiceAssign['success']) != 'OK')) {
                            $this->messageManager->addNotice(__('Error in the response when trying to load the offer in Mercadopublico - Item: %1', $childProduct->getSku()));

                            $this->connection->rollBack();

                            $this->logger->critical(__('Error in the response when trying to load the offer in Mercadopublico - Item: %1', $childProduct->getSku()));
                            $this->logger->critical('params: ' . json_encode($params));

                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                        }
                    }
                } else {
                    $tmpS = $helper->preProcessAssignStatus($data, $flag);
                    if (isset($tmpS) && $tmpS!=null) {
                        $status = $tmpS;
                    }
                    $stock = (float)$data['qty'] > 0 ? true : false;

                    $price = 0;
                    if (!$isVoucher && !$isCombustibles) {
                        $price = (float)$data['price'];
                    }
                    $params = [
                        'idOrganismo' => $userId,
                        'idProducto' => $product->getSku(),
                        'idConvenioMarco' => $agreementId,
                        'idEstadoProductoProveedorConvenio' => $status,
                        'tieneStock' => $stock,
                        'precio' => $price
                    ];

                    if ($isCombustibles) {
                        if (
                            ($flag == 1 && !array_key_exists('base_price_initial', $data)) ||
                            ($flag == 1 && $data['price'] < $data['base_price_initial'])
                        ) {
                            $this->messageManager->addErrorMessage(__('El descuento no puede ser menor a $%1', $data['base_price_initial']));
                            $this->connection->rollBack();
                            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                        }
                    }

                    if ($flag == 1) {
                        $validateServiceAssign = $helper->setAssignProductUpdate($token, $params);
                        if (isset($validateServiceAssign['success']) && $validateServiceAssign['success'] == 'NOK') {
                            $validateServiceAssign = $helper->setAssignProduct($token, $params);
                        }
                    } else {
                        $validateServiceAssign = $helper->setAssignProduct($token, $params);
                    }

                    if (!isset($validateServiceAssign['success']) || (isset($validateServiceAssign['success']) && isset($validateServiceAssign['success']) != 'OK')) {
                        $this->messageManager->addNotice(__('Error in the response when trying to load the offer in Mercadopublico - Item: %1', $product->getSku()));

                        $this->connection->rollBack();

                        $this->logger->critical(__('Error in the response when trying to load the offer in Mercadopublico - Item: %1', $product->getSku()));
                        $this->logger->critical('params: ' . json_encode($params));

                        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
                    }
                }
            } else {
                $isPending = true;
            }
            if ($isPending || (isset($validateServiceAssign['success']) && $validateServiceAssign['success'] == 'OK')) {
                $result = $helper->processAssignProduct($data, $productType, $flag);
                if (!isset($result['status']) || $result['status']==null) {
                    $result['status'] = $status;
                }

                if (isset($result['avoid_duplicate']) && $result['avoid_duplicate']) {
                    // since the offer has already been created, we show the success message when it detects that it is a duplicated offer
                    $this->messageManager->addSuccess(__('Product is saved successfully.'));
                    return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
                }
                if (($productType == 'virtual' && isset($result['error']) && $result['error']) || ($productType == 'simple' && isset($result['error']) && $result['error'])) {
                    $this->messageManager->addError(__($result['msg']));
                    $this->connection->rollBack();
                    return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
                } else if ($productType == 'configurable' && isset($result['associates']['error']) && $result['associates']['error']) {
                    $this->messageManager->addError(__($result['associates']['msg']));
                    $this->connection->rollBack();
                    return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
                } else {
                    if ($result['assign_id']) {

                        if ($this->_storeManager->getWebsite()->getCode() === \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE) {
                            // Guardar nuevo precio para los productos linkeados (Solo Emergencias)
                            $serviceData = [
                                'idOrganismo' => $userId,
                                'idConvenioMarco' => $agreementId,
                                'idEstadoProductoProveedorConvenio' => $status,
                                'token' => $token,
                            ];
                            $this->updateLinkedProductPrices($product, $data, $productType, $flag, $sellerId, $serviceData);
                        }
                        if ($this->_storeManager->getWebsite()->getCode() === \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) {
                            // Guardar nuevo precio para los productos linkeados - codigo nuevo (Solo Mobiliario)
                            $serviceData = [
                                'idOrganismo' => $userId,
                                'idConvenioMarco' => $agreementId,
                                'idEstadoProductoProveedorConvenio' => $status,
                                'token' => $token,
                            ];
                            $this->updateLinkedProductPricesNew($product, $data, $productType, $flag, $sellerId, $serviceData);

                        }
                        $helper->processProductStatus($result);
                        $helper->manageImages($data, $result);

                        // Save upload file
                        if (
                            $this->storeManager->getWebsite()->getCode() == Combustibles::WEBSITE_CODE ||
                            $this->storeManager->getWebsite()->getCode() == CombustiblesRenewal202110::WEBSITE_CODE
                        ) {
                            if ($data['qty'] <= 0) {
                                $files = $this->getRequest()->getFiles();
                                foreach ($files as $key => $file) {
                                    if ($key == 'doc_anexo_' . $productId) {
                                        if (isset($file['name']) && !empty($file['name'])) {
                                            $this->uploadFile($this->getRequest()->getFiles());
                                            break;
                                        } else {
                                            $this->messageManager->addError(__('You must attach a pdf justifying the absence of stock'));
                                            $this->connection->rollBack();
                                            return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
                                        }
                                    }
                                }
                            }
                        } elseif (in_array($websiteCode, $approvedRequiredSites) && $this->getRequest()->getFiles()->count() > 0) {
                            $allFiles = clone $this->getRequest()->getFiles();
                            $filesChild = clone $this->getRequest()->getFiles();
                            if (isset($allFiles['document_regulated_' . $productId]) && empty($allFiles['document_regulated_' . $productId]['name'])) {
                                throw new \Exception(__('You must need to upload your Regulated Product PDF File'));
                            }
                            if (isset($filesChild['document_regulated_' . $productId])) {
                                unset($filesChild['document_regulated_' . $productId]);
                            }
                            if ($isAseoRenewal) {
                                foreach ($data['products'] as $key => $asociado) {
                                    //Validation para carta de producto autorizado
                                    if (array_key_exists('associate_id', $data['products'][$key])) {
                                        $collection = $this->asociatesCollection->create()
                                            ->addFieldToFilter('id', $asociado['associate_id']);
                                        $associateProductId = $collection->getFirstItem()->getProductId();
                                        if ($result['associates'][$associateProductId]['old_status'] == 0) {
                                            if (isset($filesChild['authorized_letter_'.$productId]) &&
                                                empty($filesChild['authorized_letter_'.$productId]['name'])) {
                                                throw new \Exception(__('You must need to upload your Authorized Letter PDF File'));
                                            }
                                        }
                                    } elseif (isset($filesChild['authorized_letter_'.$productId]) &&
                                        empty($filesChild['authorized_letter_'.$productId]['name'])) {
                                        throw new \Exception(__('You must need to upload your Authorized Letter PDF File'));
                                    }
                                }
                                if (isset($filesChild['authorized_letter_' . $productId])) {
                                    unset($filesChild['authorized_letter_' . $productId]);
                                }
                            }
                            if ($filesChild->count() > 0) {
                                foreach ($data['products'] as $key => $asociado) {
                                    if (array_key_exists('associate_id', $data['products'][$key])) {
                                        continue;
                                    }
                                    $childIdDoc = 'document_child_' . $key;
                                    $nameProduct = $helper->getProduct($key)->getName();
                                    if (isset($filesChild[$childIdDoc]) && empty($filesChild[$childIdDoc]['name'])) {
                                        throw new \Exception(__('Upload the PDF file for the product %1', $nameProduct));
                                    }
                                }
                            }
                            $this->uploadFile($this->getRequest()->getFiles());
                        } else {
                            $this->uploadFile($this->getRequest()->getFiles());
                        }

                        $resp = $this->validateOffer($data, $productType);

                        if ($resp['error']) {
                            $this->messageManager->addError(__($resp['msg']));
                            $this->connection->rollBack();
                            return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
                        }

                        $this->connection->commit();
                        $this->messageManager->addSuccess(__('Product is saved successfully.'));
                        return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
                    } else {
                        $this->messageManager->addError(__('There was some error while processing your request.'));
                        $this->connection->rollBack();
                        return $this->resultRedirectFactory->create()->setPath('*/*/add', ['id' => $data['product_id']]);
                    }
                }
            } else {
                if (isset($validateServiceAssign['success']) && $validateServiceAssign['success'] == 'NOK' &&
                    isset($validateServiceAssign['errores']) && is_array($validateServiceAssign['errores'])) {
                    foreach ($validateServiceAssign['errores'] as $error) {
                        $this->messageManager->addError(__($error['descripcion']));
                    }
                } elseif (!isset($validateServiceAssign['success']) && isset($validateServiceAssign['error']) &&
                    isset($validateServiceAssign['message'])) {
                    $message = $validateServiceAssign['error'] . '. ' . $validateServiceAssign['message'];
                    $this->messageManager->addError(__($message));
                } else {
                    $this->messageManager->addError(__('There was some error while processing your request.'));
                }

                $this->connection->rollBack();
                return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            $this->connection->rollBack();
            $this->messageManager->addError($e->getMessage());
            return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
        }
    }

    protected function stockConditionsMatched()
    {
        $websiteId = $this->helperOffer->getWebsiteId();
        $enable = $this->stockManagementHelper->getGeneralConfig('enable', $websiteId) == 1;
        if (!$enable)
            return true;

        $sellerId = $this->helperOffer->getSellerId();
        $seller = $this->helperOffer->getCustomerById($sellerId);
        $difference = $this->stockManagementHelper->getDateDifference($seller->getData()["wkv_dccp_date_of_entry"], date("Y-m-d"));
        return $difference >= $this->stockManagementHelper->getGeneralConfig('number_of_days', $websiteId);
    }

    /**
     * Validate if offer is active and syncronize base price
     *
     * @param array $data
     * @param string $productType
     * @return array
     */
    public function validateOffer($data, $productType)
    {
        $result = ['error' => false, 'msg' => ''];
        $helper = $this->_assignHelper;

        if (isset($data['assign_id']) && (int)$data['assign_id'] > 0) {
            $id = (int)$data['assign_id'];

            if ($productType != 'configurable') {
                $save = $this->offer->create()->load($id, 'assign_id');
                if ($save->getId() > 0) {
                    $basePrice = (float)$data['price'];
                    $save->setBasePrice($basePrice);
                    $save->save();
                }
            } else {
                $productIds = [];
                $sql = '';
                $tableOffer = $this->resource->getTableName('dccp_offer_product');

                if (isset($data['products']) && is_array($data['products'])) {
                    foreach ($data['products'] as $key => $p) {
                        if (isset($p['id']) && $p['id'] == 1) {
                            $productIds[] = $key;
                        }
                    }

                    if (count($productIds) > 0) {
                        $obj = $this->offer->create();
                        $collection = $obj->getCollection();
                        $collection->addFieldToFilter('assign_id', $data['assign_id'])
                            ->addFieldToFilter('product_id', ['in' => $productIds]);
                        if ($collection) {
                            foreach ($collection as $item) {
                                if ($item->getId() > 0) {
                                    $basePrice = (float)$data['products'][$item->getProductId()]['price'];
                                    $sql = ' UPDATE ' . $tableOffer . ' SET base_price = ' . $basePrice .
                                        ' WHERE id = ' . $item->getId();
                                    $this->connection->query($sql);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    protected function updateLinkedProductPrices($product, $data, $productType, $flag, $sellerId, $serviceData = [])
    {
        $linkedProductsSkus = $product->getLinkedProducts();
        $linkedProductsSkus = $linkedProductsSkus ? explode(',', $linkedProductsSkus) : [];
        $linkedProducts = [];
        $linkedProductsWithOffer = [];
        $linkedProductsServiceData['idOrganismo'] = null;
        $linkedProductsServiceData['idConvenioMarco'] = null;
        $token = null;

        $this->searchCriteriaBuilder->addFilter('sku', $linkedProductsSkus, 'in');
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $linkedProductsObjects = $this->productRepositoryInterface->getList($searchCriteria)->getItems();

        if ($serviceData) {
            if (isset($serviceData['token'])) {
                $token = $serviceData['token'];
            } else {
                throw new \Exception('Invalid parameters service data.');
            }
            if (isset($serviceData['idOrganismo'])) {
                $linkedProductsServiceData['idOrganismo'] = $serviceData['idOrganismo'];
            } else {
                throw new \Exception('Invalid parameters service data.');
            }
            if (isset($serviceData['idConvenioMarco'])) {
                $linkedProductsServiceData['idConvenioMarco'] = $serviceData['idConvenioMarco'];
            } else {
                throw new \Exception('Invalid parameters service data.');
            }
        }
        $addActiveSpecialOfferToOffer = false;
        foreach ($linkedProductsObjects as $linkedProductEntity) {
            $assign_id = $this->_assignHelper->getAssignId($linkedProductEntity->getId(), $sellerId);
            if ($linkedProductEntity->getSku() != $product->getSku() && $assign_id) {
                $assign_data = $this->_assignHelper->getAssignDataByAssignId($assign_id);
                $new_base_price = $data['base_price'];
                $new_price = ($assign_data->getBasePrice() && $assign_data->getShippingPrice()) ? $new_base_price + $assign_data->getShippingPrice() : $new_base_price;

                $isOfferActive = false;
                if ($assign_data->getHasOffer()) {
                    $specialOffer = $this->offer->create()->load($assign_id, 'assign_id');
                    if ($specialOffer->getId() > 0) {
                        $isOfferActive = $this->helperOffer->datesAreCurrent($specialOffer->getStartDate(), $specialOffer->getEndDate()) == 1 ? true : false;
                    }
                }

                if (!$assign_data->getHasOffer() || ($assign_data->getHasOffer() && !$isOfferActive)) {

                    $linkedProductsServiceData['productos'][] = [
                        'idProducto' => $linkedProductEntity->getSku(),
                        'tieneStock' => (float)$assign_data->getQty() > 0 ? true : false,
                        'precio' => (float)$new_price
                    ];

                    $linkedProducts[$linkedProductEntity->getId()]['obj'] = $product;
                    $linkedProducts[$linkedProductEntity->getId()]['assign_id'] = $assign_id;
                    $linkedProducts[$linkedProductEntity->getId()]['data'] = [
                        'form_key' => $data['form_key'],
                        'product_id' => $assign_data->getProductId(),
                        'assign_id' => $assign_id,
                        'product_condition' => $assign_data->getCondition(),
                        'del' => "0",
                        'shipping_price' => $assign_data->getShippingPrice(),
                        'base_price' => $new_base_price,
                        'qty' => $assign_data->getQty(),
                        'description' => $assign_data->getDescription(),
                        'image' => "",
                        'price' => $new_price
                    ];
                }

                if ($assign_data->getHasOffer()) {
                    $linkedProductsWithOffer[$linkedProductEntity->getId()]['obj'] = $product;
                    $linkedProductsWithOffer[$linkedProductEntity->getId()]['assign_id'] = $assign_id;
                    $linkedProductsWithOffer[$linkedProductEntity->getId()]['data'] =
                        [
                            'new_base_price' => $new_base_price
                        ];
                }

                if ($isOfferActive && !$addActiveSpecialOfferToOffer) {
                    $addActiveSpecialOfferToOffer = $this->addActiveSpecialOfferToOffer($product, $linkedProductEntity, $sellerId, $new_base_price);
                }
            }
        }

        foreach ($linkedProductsWithOffer as $linkedProduct) {
            $sql = ' UPDATE dccp_offer_product SET base_price = ' . (float)$linkedProduct['data']['new_base_price'] . ' WHERE assign_id = ' . $linkedProduct['assign_id'];
            $this->connection->query($sql);
            $sql = 'UPDATE marketplace_assignproduct_items SET base_price = ' . (float)$linkedProduct['data']['new_base_price'] . ' WHERE id = ' . $linkedProduct['assign_id'];
            $this->connection->query($sql);
        }

        foreach ($linkedProducts as $linkedProduct) {
            $sql = 'UPDATE marketplace_assignproduct_items SET price = ' . (float)$linkedProduct['data']['price'] . ', base_price = ' . (float)$linkedProduct['data']['base_price'] . ' WHERE id = ' . $linkedProduct['assign_id'];
            $this->connection->query($sql);
        }

        if (isset($linkedProductsServiceData['productos'])) {
            $validateServiceAssign = $this->_assignHelper->setAssignProductsUpdate($token, $linkedProductsServiceData);

            if (isset($validateServiceAssign['success']) && $validateServiceAssign['success'] == 'NOK' &&
                isset($validateServiceAssign['errores']) && is_array($validateServiceAssign['errores'])) {
                foreach ($validateServiceAssign['errores'] as $error) {
                    $this->messageManager->addError(__($error['descripcion']));
                }
                throw new \Exception('No fue posibe sincronizar los precios de los productos asociados');
            } elseif (!isset($validateServiceAssign['success']) && isset($validateServiceAssign['error']) &&
                isset($validateServiceAssign['message'])) {
                $message = $validateServiceAssign['error'] . '. ' . $validateServiceAssign['message'];
                $this->messageManager->addError(__($message));
                throw new \Exception('No fue posibe sincronizar los precios de los productos asociados');
            }
        }

        return true;
    }

    protected function updateLinkedProductPricesNew($product, $data, $productType, $flag, $sellerId, $serviceData = [])
    {
        $linkedProductsSkus = $product->getLinkedProducts();
        $linkedProductsSkus = $linkedProductsSkus ? explode(',', $linkedProductsSkus) : [];
        $linkedProductsServiceData['idOrganismo'] = null;
        $linkedProductsServiceData['idConvenioMarco'] = null;
        $token = null;

        $this->searchCriteriaBuilder->addFilter('sku', $linkedProductsSkus, 'in');
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $linkedProductsObjects = $this->productRepositoryInterface->getList($searchCriteria)->getItems();

        if ($serviceData) {
            if (isset($serviceData['token'])) {
                $token = $serviceData['token'];
            } else {
                throw new \Exception('Invalid parameters service data.');
            }
            if (isset($serviceData['idOrganismo'])) {
                $linkedProductsServiceData['idOrganismo'] = $serviceData['idOrganismo'];
            } else {
                throw new \Exception('Invalid parameters service data.');
            }
            if (isset($serviceData['idConvenioMarco'])) {
                $linkedProductsServiceData['idConvenioMarco'] = $serviceData['idConvenioMarco'];
            } else {
                throw new \Exception('Invalid parameters service data.');
            }
        }

        foreach ($linkedProductsObjects as $linkedProductEntity) {
            $assignId = $this->_assignHelper->getAssignId($linkedProductEntity->getId(), $sellerId);
            if ($linkedProductEntity->getSku() != $product->getSku()) {
                //agregamos productos a la tabla de links.
                // add row in dccp_linked_product table
                $linkedProduct = $this->linkedProductFactory->create();
                $data = [
                    'trigger_product_id' => $product->getId(),
                    'trigger_product_sku' => $product->getSku(),
                    'product_id' => $linkedProductEntity->getId(),
                    'product_sku' => $linkedProductEntity->getSku(),
                    'seller_id' => $sellerId,
                    'assign_id' => $assignId, //si es nuevo, esto es null.
                    'flag' => $flag,
                    'website_id' => $this->_storeManager->getWebsite()->getId(),
                    'json_data' => json_encode($data),
                    'json_service_data' => json_encode($serviceData),
                    'status' => 0
                ];

                // check if offer already exists
                $collection = $linkedProduct->getCollection();
                $collection->addFieldToFilter('product_id', $linkedProductEntity->getId())
                    ->addFieldToFilter('seller_id', $sellerId)
                    ->addFieldToFilter('status', ['eq' => 0]);

                if ($collection->getSize()) {
                    // update offer
                    $linkedProductFirstItem = $collection->getFirstItem();
                    $linkedProduct->load($linkedProductFirstItem->getId());
                    foreach ($data as $key => $value) {
                        $linkedProduct->setData($key, $value);
                    }
                    $linkedProduct->save();
                } else {
                    $linkedProduct->setData($data);
                    $linkedProduct->save();
                }

            }
            if ($linkedProductEntity->getSku() == $product->getSku()) {
                //buscar en la tabla y anular el update si es que no fue procesado aun?
                //o nunca va a pasar este caso?
            }
        }

        $linkedProductsSkusToShow = str_replace(',', ', ', $product->getLinkedProducts());
        $this->messageManager->addSuccess(__('Changes on products %1 will be reflected in the next 5 minutes.', $linkedProductsSkusToShow));


        return true;
    }

    /**
     * Add active special offer to the new offer
     *
     * @param $product
     * @param $linkedProductEntity
     * @param $sellerId
     * @param $newBasePrice
     * @return bool
     * @throws \Exception
     */
    public function addActiveSpecialOfferToOffer($product, $linkedProductEntity, $sellerId, $newBasePrice)
    {
        $productAssignId = $this->_assignHelper->getAssignId($product->getId(), $sellerId);
        $linkedProductAssignId = $this->_assignHelper->getAssignId($linkedProductEntity->getId(), $sellerId);

        $offerCollection = $this->_offerCollection->create()->addFieldToFilter('assign_id', $productAssignId)->setPageSize(1);
        if (!$offerCollection->getSize()) {
            $linkedOfferCollection = $this->_offerCollection->create()->addFieldToFilter('assign_id', $linkedProductAssignId)->setPageSize(1);
            if (!$linkedOfferCollection->getSize()) {
                return false;
            } else {
                $linkedOfferData = $linkedOfferCollection->getFirstItem()->getData();

                $newOffer = $this->offer->create();

                $newOffer->setParentProductId($product->getId());
                $newOffer->setProductId($product->getId());
                $newOffer->setAssignId($productAssignId);
                $newOffer->setSellerId($sellerId);
                $newOffer->setWebsiteId($linkedOfferData['website_id']);
                $newOffer->setStartDate($linkedOfferData['start_date']);
                $newOffer->setEndDate($linkedOfferData['end_date']);
                $newOffer->setDays($linkedOfferData['days']);
                $newOffer->setSpecialPrice($linkedOfferData['special_price']);
                $newOffer->setBasePrice($newBasePrice);
                $newOffer->setStatus(1);
                $newOffer->save();

                $itemsFactory = $this->_itemsFactory->create()->load($productAssignId);
                if ($itemsFactory->getId() > 0) {
                    $itemsFactory->setHasOffer(1);
                    $itemsFactory->save();
                }
                return true;
            }
        }
        return false;
    }

    /**
     * @param array $data
     * @param string $productType
     * @return boolean
     */
    protected function hasBeenSetOutOfStock($data, $productType)
    {
        $hasBeenSet = false;
        if ($productType != Configurable::TYPE_CODE) {
            $qty = (int)$data['qty'];
            if ($qty === 0) {
                $hasBeenSet = true;
            }
        } elseif ($productType == Configurable::TYPE_CODE) {
            foreach ($data['products'] as $childId => $item) {
                if (isset($item['associate_id']) && isset($item['qty'])) {
                    $qty = (int)$item['qty'];
                    if ($qty === 0) {
                        $hasBeenSet = true;
                        break;
                    }
                }
            }
        }

        return $hasBeenSet;
    }

    /**
     * @param int $productId
     * @param int $associateId
     * @return float
     */
    protected function getCurrentPrice($productId, $associateId)
    {
        $checkHasSpecialOffer = $this->helperOffer->checkIfAssociateHasSpecialOffer($productId);
        if ($checkHasSpecialOffer['has_offer'] === true) {
            $oldPrice = (float)$checkHasSpecialOffer['price'];
        } else {
            $assignProductItem = $this->_assignHelper->getAssociatedItem($associateId);
            $oldPrice = (float)$assignProductItem->getPrice();
        }

        return $oldPrice;
    }

    /**
     * @param int $childId
     * @return float
     */
    protected function getMinOffer($childId)
    {
        //finding min price for status = 1 offers
        $collectionAsociates = $this->asociatesCollection->create()
            ->addFieldToFilter('product_id', $childId)
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('qty', ['gt' => 0]);

        $validAssociateId = [];
        // Search for disapproved sellers in offers
        foreach ($collectionAsociates as $associated) {
            $parentItemId = $associated->getParentId();
            $assignItem = $this->_assignHelper->getAssignProduct($parentItemId);
            $sellerId = $assignItem->getSellerId();
            $sellerStatus = $this->_assignHelper->getSellerStatus($sellerId);
            if ($sellerStatus == '70') {
                $validAssociateId[] = $associated->getId();
            }
        }
        // Reload and remove from collection offers with disapproved seller
        $collectionAsociates = $this->asociatesCollection->create()
            ->addFieldToFilter('product_id', $childId)
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('qty', ['gt' => 0])
            ->addFieldToFilter('id', ['in' => $validAssociateId]);

        if (!empty($collectionAsociates) && $collectionAsociates->getSize() > 0) {
            $minPriceOffer = null;

            foreach ($collectionAsociates as $offer) {
                if ($minPriceOffer == null || $offer->getPrice() < $minPriceOffer) {
                    $minPriceOffer = $offer->getPrice();
                }
            }
        }

        return (float)$minPriceOffer;
    }

    /**
     * @param int $productId
     * @param float $price
     * @param int $sellerId
     * @return float
     */
    protected function getDispersionPercentage($productId, $price, $sellerId)
    {
        $table = $this->resource->getTableName('price_dispersion');
        $select = $this->connection->select();
        $select->from($table, ['update_percentage'])
            ->where('mageproduct_id = ?', $productId)
            ->where('mageproduct_price = ?', $price)
            ->where('seller_id = ?', $sellerId);
        $disPercentage = $this->connection->fetchOne($select);

        return (float)($disPercentage / 100);
    }
}
