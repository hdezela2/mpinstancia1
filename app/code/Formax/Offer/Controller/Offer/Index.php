<?php

namespace Formax\Offer\Controller\Offer;

use Formax\Offer\Helper\Data as OfferHelper;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Magento\Customer\Model\Url
     */
    protected $_url;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_session;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $_assignHelper;

    /**
     * @var HelperSoftware
     */
    protected $helperSoftware;

    /** @var OfferHelper */
    protected $offerHelper;
    private \Webkul\Marketplace\Helper\Data $_mpHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Customer\Model\Url $url
     * @param \Magento\Customer\Model\Session $session
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     * @param HelperSoftware $helperSoftware
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Url $url,
        \Magento\Customer\Model\Session $session,
        \Webkul\MpAssignProduct\Helper\Data $helper,
        OfferHelper $offerHelper,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        HelperSoftware $helperSoftware
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_url = $url;
        $this->_session = $session;
        $this->_assignHelper = $helper;
        $this->offerHelper = $offerHelper;
        $this->_mpHelper = $mpHelper;
        $this->helperSoftware = $helperSoftware;
        parent::__construct($context);
    }

    /**
     * Check customer authentication.
     *
     * @param RequestInterface $request
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->_url->getLoginUrl();
        if (!$this->_session->authenticate($loginUrl)
            || $this->helperSoftware->getCurrentStoreCode() == HelperSoftware::SOFTWARE_STOREVIEW_CODE || $this->helperSoftware->getCurrentStoreCode() == SoftwareRenewalConstants::STORE_CODE) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $checkProduct = $this->_assignHelper->checkProduct(0, true);

        if ($checkProduct['error'] == 1 || $this->helperSoftware->getCurrentStoreCode() == SoftwareRenewalConstants::STORE_VIEW_CODE) {
            if ($checkProduct['error']) {
                $this->messageManager->addError($checkProduct['msg']);
            } else {
                $this->messageManager->addError(__('You are not allowed to view this section.'));
            }
            return $this->resultRedirectFactory->create()->setPath('mpassignproduct/product/productlist');
        } else {
            $resultPage = $this->_resultPageFactory->create();
            if ($this->_mpHelper->getIsSeparatePanel()) {
                $resultPage->addHandle('offerassignproduct_offer_index_layout2');
            }
            $resultPage->getConfig()->getTitle()->set(__('Assign Offer'));
            return $resultPage;
        }
    }
}
