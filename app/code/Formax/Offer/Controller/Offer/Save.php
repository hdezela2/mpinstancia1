<?php

namespace Formax\Offer\Controller\Offer;

use Cleverit\ComputadoresRenewal\Constants as ComputadoresRenewalConstants;
use Intellicore\ComputadoresRenewal2\Constants as ComputadoresRenewal2Constants;
use DateTime;
use Exception;
use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Linets\InsumosSetup\Model\InsumosConstants;
use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Linets\VoucherSetup\Model\Constants as VoucherSetupConstants;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Linets\GasSetup\Constants as GasConstants;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * Marketplace Helper
     *
     * @var \Webkul\Marketplace\Helper\Data
     */
    public $marketplaceHelper;

    /**
     * @var int
     */
    const CUSTOM_ERROR_CODE = 555;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $dbConnection;

    /**
     * @var \Formax\Offer\Helper\Data
     */
    protected $helper;

    /**
     * @var \Formax\Offer\Model\OfferFactory
     */
    protected $offerFactory;

    /**
     * @var \Webkul\MpAssignProduct\Model\ItemsFactory
     */
    protected $itemsFactory;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var int
     */
    protected $websiteId;

    /**
     * @var \Formax\Offer\Model\ResourceModel\Offer\CollectionFactory
     */
    protected $offerCollection;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepositoryInterface;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $assignHelper;

    /**
     * @var \Formax\Offer\Model\LinkedProductOfferFactory
     */
    protected $_linkedProductOfferFactory;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * Save constructor.
     * @param Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Webkul\Marketplace\Helper\Data $marketplaceHelper
     * @param PageFactory $resultPageFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Formax\Offer\Model\OfferFactory $offerFactory
     * @param \Formax\Offer\Helper\Data $helper
     * @param \Webkul\MpAssignProduct\Model\ItemsFactory $itemsFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Formax\Offer\Model\ResourceModel\Offer\CollectionFactory $offerCollection
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Webkul\MpAssignProduct\Helper\Data $assignHelper
     * @param \Formax\Offer\Model\LinkedProductOfferFactory $linkedProductOfferFactory
     * @param TimezoneInterface $timezone
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger,
        \Webkul\Marketplace\Helper\Data $marketplaceHelper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Formax\Offer\Model\OfferFactory $offerFactory,
        \Formax\Offer\Helper\Data $helper,
        \Webkul\MpAssignProduct\Model\ItemsFactory $itemsFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Formax\Offer\Model\ResourceModel\Offer\CollectionFactory $offerCollection,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Webkul\MpAssignProduct\Helper\Data $assignHelper,
        \Formax\Offer\Model\LinkedProductOfferFactory $linkedProductOfferFactory,
        TimezoneInterface $timezone
    ) {
        $this->itemsFactory = $itemsFactory;
        $this->offerFactory = $offerFactory;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->resource = $resource;
        $this->storeManager = $storeManager;
        $this->resultPageFactory = $resultPageFactory;
        $this->marketplaceHelper = $marketplaceHelper;
        $this->customerRepositoryInterface = $customerRepositoryInterface;

        $this->websiteId = (int)$this->storeManager->getStore()->getWebsiteId();
        $this->dbConnection = $this->resource->getConnection();

        $this->offerCollection = $offerCollection;
        $this->productRepositoryInterface = $productRepositoryInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->assignHelper = $assignHelper;
        $this->_linkedProductOfferFactory = $linkedProductOfferFactory;
        $this->timezone = $timezone;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        if ($this->marketplaceHelper->isSeller()) {
            $this->getPostData();
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function getPostData()
    {
        $websiteCode = $this->storeManager->getWebsite()->getCode();
        $post = $this->getRequest()->getPostValue();
        $customerId = $this->helper->getCustomerId();
        $sellerId = $this->helper->getSellerId();
        $websiteId = $this->helper->getWebsiteId();
        $customer = $this->customerRepositoryInterface->getById($customerId);
        $product = isset($post['parent_product_id']) ? $this->productRepositoryInterface->getById($post['parent_product_id']) : null;
        $assignId = isset($post['assign_id']) ? (int)$post['assign_id'] : 0;
        $token = '';
        $flagOffer = false;

        if (!$post && $assignId > 0) {
            $this->_redirect('mpassignproduct/product/productlist');
            return;
        }

        $this->dbConnection->beginTransaction();

        try {

            $validation = ['error' => false, 'msg' => ''];

            if ($customer && $product) {
                $token = $customer->getCustomAttribute('user_rest_atk') ?
                    $customer->getCustomAttribute('user_rest_atk')->getValue() : '';

                if ($post['product_type'] == 'configurable') {
                    if (isset($post['products']) && is_array($post['products'])) {
                        $tableName = $this->resource->getTableName('dccp_offer_product');

                        foreach ($post['products'] as $pId => $pItem) {
                            //Only Insumos
                            if ($websiteCode == InsumosConstants::WEBSITE_CODE) {
                                $LowerBasePrice = $this->getLowerBasePrice($product->getId(), $pId);
                            }
                            $simpleProduct = $this->productRepositoryInterface->getById($pId);
                            // validation that the post product has no offer active, so we don't update it
                            $checkHasSpecialOfferActive = $this->helper->checkIfAssociateHasSpecialOffer($pId, 3);
                            $pName = $simpleProduct->getName();
                            if ($checkHasSpecialOfferActive['has_offer']) {
                                unset($post['products'][$pId]);
                                continue;
                            }
                            $checkHasSpecialOffer = $this->helper->checkIfAssociateHasSpecialOffer($pId, 1);

                            if ($websiteCode == InsumosConstants::WEBSITE_CODE) {
                                $location = __('(Product Name: %1).', $pItem['name']);
                            } else {
                                $location = __('(Product Name: %1).', $pName);
                            }

                            $id = (isset($pItem['offer_id']) && (int)$pItem['offer_id'] > 0 ? (int)$pItem['offer_id'] : $checkHasSpecialOffer['has_offer']) ? $checkHasSpecialOffer['id'] : 'NULL';


                            if (!empty($pItem['special_price'])) {
                                $flagOffer = true;
                                $validation = $this->helper->validateOffer(
                                    isset($LowerBasePrice) && !empty($LowerBasePrice) ? $LowerBasePrice :  $pItem['base_price'],
                                    $pItem['special_price'],
                                    $pItem['origin_start_date'],
                                    $pItem['start_date'],
                                    $pItem['days'],
                                    $product,
                                    $location,
                                    $simpleProduct
                                );

                                if ($validation['error']) {
                                    $message = $validation['msg'];
                                    throw new \Exception($message, self::CUSTOM_ERROR_CODE);
                                }

                                if (!empty($pItem['start_date']) && !empty($pItem['end_date'])) {
                                    if ((int)$id > 0 && ($pItem['start_date'] != $pItem['origin_start_date'] ||
                                            $pItem['days'] != $pItem['origin_days'] || (float)$pItem['special_price'] != (float)$pItem['origin_special_price'])) {
                                        $isOfferActive = $this->helper->datesAreCurrent($pItem['origin_start_date'], $pItem['end_date']);

                                        if ($isOfferActive === 1) {
                                            throw new LocalizedException(__("You can't edit this offer because is active. %1", $location), new \Exception());
                                        }
                                    }
                                }

                                $__startDate = new \DateTime($pItem['start_date']);
                                $__date = $__startDate->format('d-m-Y');

				                $totalDays = $pItem['days'];
                                if ($this->storeManager->getWebsite()->getCode() === \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE ){
                                    $totalDays = $totalDays - 1; // This is used to end date count from start date.
                                }

                                if ($this->storeManager->getWebsite()->getCode() ==
                                    AseoRenewalConstants::WEBSITE_CODE) {
                                    $totalDays = $totalDays - 1;
                                    $__endDate = new \DateTime($__date.' +'.$totalDays.' day');
                                    $this->addNewOfferSpecial($id, $post['parent_product_id'], $pId, $assignId,
                                        $sellerId,
                                        $websiteId,
                                        $pItem,
                                        $__endDate
                                    );
                                }else{
                                    $response = $this->helper->getEndBusinessDate($token, $__date, $totalDays);
                                    if (isset($response['success']) && $response['success'] == 'OK' && isset($response['payload'])) {
                                        $endDate = $response['payload']['fecha_termino'];
                                        $__endDate = new \DateTime($endDate);
                                        $this->addNewOfferSpecial($id, $post['parent_product_id'], $pId, $assignId,
                                            $sellerId,
                                            $websiteId,
                                            $pItem,
                                            $__endDate
                                        );
                                    } else {
                                        throw new LocalizedException(__("An error occured trying get the offer's end date."), new \Exception());
                                    }
                                }
                            }
                        }

                        if (!$flagOffer) {
                            throw new LocalizedException(__("You must set one offer at least."), new \Exception());
                        }
                    }
                } else {
                    if ($this->storeManager->getWebsite()->getCode() === VoucherSetupConstants::WEBSITE_CODE &&
                        (!is_numeric($post['base_price']) || !is_numeric($post['special_price']))) {
                        try {
                            $post['base_price'] = str_replace(',', '.', $post['base_price']);
                            $post['special_price'] = str_replace(',', '.', $post['special_price']);
                        } catch (\Exception $ex) {}
                    }
                    $validation = $this->helper->validateOffer(
                        isset($LowerBasePrice) && !empty($LowerBasePrice) ? $LowerBasePrice :  $post['base_price'],
                        $post['special_price'],
                        '',
                        $post['start_date'],
                        $post['days'],
                        $product
                    );
                    //los productos linkeados tienen el mismo base - o es necesario validar para todos los linkeados Emergencias.

                    if ($validation['error']) {
                        $message = $validation['msg'];
                        throw new \Exception($message, self::CUSTOM_ERROR_CODE);
                    }

                    if (!empty($post['start_date']) && !empty($post['end_date'])) {
                        if ((int)$post['offer_id'] > 0 && ($post['start_date'] != $post['origin_start_date'] ||
                                $post['days'] != $post['origin_days'] || (float)$post['special_price'] != (float)$post['origin_special_price'])) {
                            $isOfferActive = $this->helper->datesAreCurrent($post['origin_start_date'], $post['end_date']);

                            if ($isOfferActive === 1) {
                                throw new LocalizedException(__("You can't edit this offer because is active. %1", ''), new \Exception());
                            }
                        }
                    }
                    $businessDays = $this->helper->isBusinessDays();
                    $__startDate = new \DateTime($post['start_date']);
                    $__date = $__startDate->format('d-m-Y');
                    $endDate = null;
                    if ($this->storeManager->getWebsite()->getCode() === \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE ){
                        $endDate = $this->getEndDate($token, $__date, $post['days']);
                    }elseif (($this->storeManager->getWebsite()->getCode() === \Summa\EscritorioSetUp\Helper\Data::WEBSITE_CODE )
                        && $businessDays == 0 ) {
                        $endDate = $this->getEndDate($token, $__date, $post['days']);
                    }else{
                        $totalDays = $post['days'];
                        if ($this->storeManager->getWebsite()->getCode() === \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE ){
                            $totalDays = $totalDays - 1; // This is used to end date count from start date.
                        }
			            $response = $this->helper->getEndBusinessDate($token, $__date, $totalDays);
			            if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])){
                            $endDate = $response['payload']['fecha_termino'];
                            $endDate = new \DateTime($endDate);
                            $endDate->format('Y-m-d');
                        }
                    }

                    if ($endDate != null) {
                        $__endDate = $endDate;
                        $id = isset($post['offer_id']) && (int)$post['offer_id'] > 0 ? (int)$post['offer_id'] : 0;
                        $linkedProductsSkus = $product->getLinkedProducts();

                        //if emergencias && product has linked products - the offer will be created via cronjob
                        if ( ($this->storeManager->getWebsite()->getCode() === \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE
                            || $this->storeManager->getWebsite()->getCode() === ConstantsEmergency202109::WEBSITE_CODE
                            || $this->storeManager->getWebsite()->getCode() === \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE
                            || $this->storeManager->getWebsite()->getCode() === \Summa\EscritorioSetUp\Helper\Data::WEBSITE_CODE ) && strlen($linkedProductsSkus) ) {

                            $parentProductId = $post['parent_product_id'];
                            $productId = $post['product_id'];
                            $startDate = $post['start_date'];
                            $days = $post['days'];
                            $specialPrice = $post['special_price'];
                            $basePrice = $post['base_price'];

                            // add row in dccp_linked_product_offer table
                            $linkedProductOffer = $this->_linkedProductOfferFactory->create();
                            $data = [
                                'parent_product_id' => $parentProductId,
                                'product_id' => $productId,
                                'seller_id' => $sellerId,
                                'assign_id' => $assignId,
                                'offer_id' => $id,
                                'website_id' => $websiteId,
                                'start_date' => $startDate,
                                'end_date' => $__endDate,
                                'days' => $days,
                                'special_price' => $specialPrice,
                                'base_price' => $basePrice,
                                'status' => 0
                            ];

                            if ($data['offer_id'] != 0){
                                // check if offer already exists
                                $collection = $linkedProductOffer->getCollection();
                                $collection->addFieldToFilter('offer_id', $data['offer_id']);
                                // codigo de chilecompra - no filtra por estado.

                                if ($collection->getSize()) {
                                    // update offer
                                    $linkedProductOfferFirstItem = $collection->getFirstItem();
                                    $linkedProductOffer->load($linkedProductOfferFirstItem->getId());
                                    $linkedProductOffer->setStatus(0);
                                    $linkedProductOffer->setParentProductId($parentProductId);
                                    $linkedProductOffer->setProductId($productId);
                                    $linkedProductOffer->setSellerId($sellerId);
                                    $linkedProductOffer->setOfferId($id);
                                    $linkedProductOffer->setAssignId($assignId);
                                    $linkedProductOffer->setWebsiteId($websiteId);
                                    $linkedProductOffer->setStartDate($startDate);
                                    $linkedProductOffer->setEndDate($__endDate);
                                    $linkedProductOffer->setDays($days);
                                    $linkedProductOffer->setSpecialPrice($specialPrice);
                                    $linkedProductOffer->setBasePrice($basePrice);
                                    $linkedProductOffer->save();
                                } else {
                                    $linkedProductOffer->setData($data);
                                    $linkedProductOffer->save();
                                }
                            } else{
                                $linkedProductOffer->setData($data);
                                $linkedProductOffer->save();
                            }

                            $product = $this->assignHelper->getProduct($post['product_id']);
                            $linkedProductsSkus = $product->getLinkedProducts();
                            $currentProductSku = $product->getSku();
                            $linkedProductsSkus = str_replace(',', ', ', $linkedProductsSkus);
                            $this->messageManager->addSuccess(__('The special offer will be applied to the following products: %1 in the next 5 minutes.', $linkedProductsSkus));
                        } else {

                            $model = $this->offerFactory->create();
                            if ($id > 0) {
                                $model->load($id);
                            }

                            $model->setParentProductId($post['parent_product_id']);
                            $model->setProductId($post['product_id']);
                            $model->setAssignId($assignId);
                            $model->setSellerId($sellerId);
                            $model->setWebsiteId($websiteId);
                            $model->setStartDate($post['start_date']);
                            $model->setEndDate($__endDate);
                            $model->setDays($post['days']);
                            $model->setSpecialPrice($post['special_price']);
                            $model->setBasePrice($post['base_price']);
                            $model->setStatus(1);
                            $model->save();

                            // save in table marketplace_assignproduct_items
                            $itemsFactory = $this->itemsFactory->create()->load($assignId);
                            if ($itemsFactory->getId() > 0) {
                                $itemsFactory->setHasOffer(1);
                                $itemsFactory->save();
                            }
                        }

                    } else {
                        throw new LocalizedException(__("An error occured trying get the offer's end date."), new \Exception());
                    }
                }

                $this->dbConnection->commit();

                $this->messageManager->addSuccess(__('Data was saved succesfully.'));

                $this->getDataPersistor()->clear('dccp_offer');
                $this->_redirect('mpassignproduct/product/productlist');
                return;
            } else {
                throw new LocalizedException(__('Seller token is empty and/or product is null.'), new \Exception());
            }
        } catch(LocalizedException $e) {
            $this->logger->critical('Offer Error message: ' . $e->getMessage());
            $this->messageManager->addError($e->getMessage());
            $this->dbConnection->rollBack();
            $this->getDataPersistor()->set('dccp_offer', $post);
            $this->_redirect('offerassignproduct/offer/index/id/' . $assignId);
            return;
        } catch (\Exception $e) {
            if ($e->getCode() == self::CUSTOM_ERROR_CODE) {
                $this->messageManager->addError($e->getMessage());
            } else {
                $this->logger->critical($e->getMessage());
                $this->messageManager->addError(__('An error occured saving the information.'));
            }
            $this->dbConnection->rollBack();
            $this->getDataPersistor()->set('dccp_offer', $post);
            $this->_redirect('offerassignproduct/offer/index/id/' . $assignId);
            return;
        }
    }

    /**
     * @param $id
     * @param $parent_product_id
     * @param $pId
     * @param $assignId
     * @param $sellerId
     * @param $websiteId
     * @param $pItem
     * @param $__endDate
     * @return \Formax\Offer\Model\Offer|void
     * @throws Exception
     */
    public function addNewOfferSpecial($id, $parent_product_id, $pId, $assignId, $sellerId, $websiteId, $pItem,
                                       $__endDate)
    {
        try {
            $model = $this->offerFactory->create();
            if ((int)$id > 0) {
                $model->load($id);
            }

            $model->setParentProductId($parent_product_id);
            $model->setProductId($pId);
            $model->setAssignId($assignId);
            $model->setSellerId($sellerId);
            $model->setWebsiteId($websiteId);
            $model->setStartDate($pItem['start_date']);
            $model->setEndDate($__endDate->format('Y-m-d'));
            $model->setDays($pItem['days']);
            $model->setSpecialPrice($pItem['special_price']);
            $model->setBasePrice($pItem['base_price']);
            $model->setStatus(1);
            return $model->save();
        } catch (\Exception $e) {
            $this->messageManager->addError(__('An error occured saving the information.'));
            $this->logger->critical('An error occured saving the information. > '.$e->getMessage());
            $this->_redirect('offerassignproduct/offer/index/id/'.$assignId);
            return;
        }
    }

    /**
     * @param $parentProductId
     * @param $childProductId
     * @return array
     */
    public function getLowerBasePrice($parentProductId, $childProductId)
    {
        $activeStatusSeller = 70;
        $activeAssoc = 1;
        //Only for Insumos
        $connection = $this->resource->getConnection();
        $tableItems = $this->resource->getTableName('marketplace_assignproduct_items');
        $tableAssociatedItems = $this->resource->getTableName('marketplace_assignproduct_associated_products');
        $tableUserData = $this->resource->getTableName('marketplace_userdata');
        $tableOffer = $this->resource->getTableName('dccp_offer_product');
        $tableCustomerEntityVarchar = $this->resource->getTableName('customer_entity_varchar');
        $sql = 'SELECT asso.price
                FROM '.$tableAssociatedItems.' asso
                         LEFT JOIN '.$tableItems.' i ON i.id = asso.parent_id
                         LEFT JOIN '.$tableUserData.' ud ON ud.seller_id = i.seller_id
                         LEFT JOIN '.$tableOffer.' p ON asso.product_id = p.product_id AND asso.parent_id = p.assign_id
                         LEFT JOIN '.$tableCustomerEntityVarchar.' AS cv ON ud.seller_id = cv.entity_id AND cv.attribute_id = 604
                WHERE asso.parent_product_id = '.$parentProductId.'
                  AND asso.product_id = '.$childProductId.'
                  AND cv.value = '.$activeStatusSeller.'
                  AND asso.status = '.$activeAssoc.'
                  AND asso.qty > 0
                ORDER BY asso.price ASC';
        return (int)$connection->fetchOne($sql);
    }

    /**
     * Get End Date
     * @param $token
     * @param $startDate
     * @param $days
     * @return DateTime|false|string
     * @throws LocalizedException
     * @throws Exception
     */
    public function getEndDate($token, $startDate, $days)
    {
        $startDate = new DateTime($startDate);
        $startDate = $startDate->format('Y-m-d');
        $endDate = null;

        if ($this->storeManager->getWebsite()->getCode() == \Summa\ComputadoresSetUp\Helper\Data::WEBSITE_CODE ||
            $this->storeManager->getWebsite()->getCode() == ComputadoresRenewalConstants::WEBSITE_CODE ||
            $this->storeManager->getWebsite()->getCode() == ComputadoresRenewal2Constants::WEBSITE_CODE ||
            $this->storeManager->getWebsite()->getCode() === \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE  ||
            $this->storeManager->getWebsite()->getCode() === \Summa\EscritorioSetUp\Helper\Data::WEBSITE_CODE) {
                $endDate = date_add(date_create($startDate), date_interval_create_from_date_string($days.' days'));
            $endDate = date_format($endDate, 'Y-m-d');
        } else {
            $response = $this->helper->getEndBusinessDate($token, $startDate, $days);
            if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                $endDate = $response['payload']['fecha_termino'];
                $endDate = new DateTime($endDate);
                $endDate->format('Y-m-d');
            } else {
                throw new LocalizedException(__("An error occured trying get the offer's end date."), new Exception());
            }
        }
        return $endDate;
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }

    protected function updateLinkedProductOffer($productId, $dataModelOriginalProduct, $productType, $sellerId){

        $product = $this->assignHelper->getProduct($productId);
        $linkedProductsSkus = $product->getLinkedProducts();
        $originalSku = $product->getSku();
        $linkedProductsSkus = $linkedProductsSkus ? explode(',', $linkedProductsSkus) : [];

        //Toda la info de los items en la lista de linkeados
        $this->searchCriteriaBuilder->addFilter('sku', $linkedProductsSkus, 'in');
        $searchCriteria = $this->searchCriteriaBuilder->create();

        $linkedProductsObjects = $this->productRepositoryInterface->getList($searchCriteria)->getItems();

        //Todas las ofertas especiales que ya están carfadas
        foreach ($linkedProductsObjects as $linkedProductEntity)
        {
            //Consultamos la info de item a actualizar
            $assignId = $this->assignHelper->getAssignId($linkedProductEntity->getId(), $sellerId);

            //Actualizamos solo si no es el producto sobre el que generamos la oferta especial
            if($linkedProductEntity->getSku() != $originalSku && $assignId)
            {

                $assignData = $this->assignHelper->getAssignDataByAssignId($assignId);

                //consultamos si existe una oferta especial previamente definida para actualizarla.
                $itemLinked = $this->offerCollection->create()
                    ->addFilter('assign_id', $assignId, 'eq')
                    ->addFilter('seller_id', $sellerId, 'eq')
                    ->addFieldToSelect('id')
                    ->getFirstItem();
                $idLinked = $itemLinked && (int)$itemLinked['id'] > 0 ? (int)$itemLinked['id'] : 0;

                $model = $this->offerFactory->create();
                if ($idLinked > 0) {
                    $model->load($idLinked);
                }

                $model->setParentProductId($assignData['product_id']);
                $model->setProductId($assignData['product_id']);
                $model->setAssignId($assignId);
                $model->setSellerId($sellerId);
                $model->setWebsiteId($dataModelOriginalProduct->getWebsiteId());
                $model->setStartDate($dataModelOriginalProduct->getStartDate());
                $model->setEndDate($dataModelOriginalProduct->getEndDate());
                $model->setDays($dataModelOriginalProduct->getDays());
                $model->setSpecialPrice($dataModelOriginalProduct->getSpecialPrice());
                $model->setBasePrice($dataModelOriginalProduct->getBasePrice());
                $model->setStatus(1);
                $model->save();

                $this->messageManager->addSuccess(__('Data was saved succesfully in %1' ,$linkedProductEntity->getSku()));

                // save in table marketplace_assignproduct_items
                $itemsFactory = $this->itemsFactory->create()->load($assignId);
                if ($itemsFactory->getId() > 0) {
                    $itemsFactory->setHasOffer(1);
                    $itemsFactory->save();
                }
            }
        }
    }
}
