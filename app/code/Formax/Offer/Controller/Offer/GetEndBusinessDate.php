<?php

namespace Formax\Offer\Controller\Offer;

use Formax\Offer\Helper\Data as OfferHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Json\Helper\Data as JsonHelper;

class GetEndBusinessDate extends \Magento\Framework\App\Action\Action
{
    /** @var PageFactory */
    protected $_resultPageFactory;

    /** @var JsonFactory */
    protected $_resultJsonFactory;

    /** @var OfferHelper */
    protected $_offerHelper;

    /** @var JsonHelper */
    protected $_jsonHelper;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        OfferHelper $offerHelper,
        JsonHelper $jsonHelper
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_offerHelper = $offerHelper;
        $this->_jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Exception
     */
    public function execute()
    {
        $token = $this->getRequest()->getParam('token');
        $startDate = new \DateTime($this->getRequest()->getParam('startDate'));

        $days = (int)$this->getRequest()->getParam('days');

        try {
            $response = $this->_offerHelper->getEndBusinessDate($token, $startDate->format('d-m-Y'), $days);

            if (isset($response['success']) && $response['success'] == 'OK' && isset($response['payload'])) {
                $apiResult = new \DateTime($response['payload']['fecha_termino']);
                $result['endDate'] = $apiResult->format('Y-m-d');
            } else {
                $result['error'] = 1;
                $result['msg'] = "Not able to get end business date.";
            }
        } catch (\Exception $e) {
            $result['error'] = 1;
            $result['msg'] = $e->getMessage();
        }

        $result = $this->_jsonHelper->jsonEncode($result);
        $this->getResponse()->representJson($result);
    }
}
