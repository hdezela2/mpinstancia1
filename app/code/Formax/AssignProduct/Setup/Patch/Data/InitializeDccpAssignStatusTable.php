<?php
namespace Formax\AssignProduct\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class InitializeDccpAssignStatusTable implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Formax\AssignProduct\Model\AssignStatusFactory
     */
    protected $_assignStatusFactory;

    /**
     * InitializeDccpAssignStatusTable constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param \Formax\AssignProduct\Model\AssignStatusFactory $assignStatusFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        \Formax\AssignProduct\Model\AssignStatusFactory $assignStatusFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->_assignStatusFactory = $assignStatusFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function apply()
    {
        $values = [
            [ 'mage_status' => 0, 'dccp_status' => 3 ],
            [ 'mage_status' => 1, 'dccp_status' => 1 ],
            [ 'mage_status' => 3, 'dccp_status' => 8 ],
            [ 'mage_status' => 4, 'dccp_status' => 6 ],
            [ 'mage_status' => 5, 'dccp_status' => 4 ],
            [ 'mage_status' => 6, 'dccp_status' => 2 ],
            [ 'mage_status' => 7, 'dccp_status' => 3 ]
        ];
        foreach ($values as $value){
            $this->_assignStatusFactory->create()->setData($value)->save();
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
