<?php
namespace Formax\AssignProduct\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddPendingStatusToDccpAssignStatusTable implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Formax\AssignProduct\Model\AssignStatusFactory
     */
    protected $_assignStatusFactory;

    /**
     * InitializeDccpAssignStatusTable constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param \Formax\AssignProduct\Model\AssignStatusFactory $assignStatusFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        \Formax\AssignProduct\Model\AssignStatusFactory $assignStatusFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->_assignStatusFactory = $assignStatusFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function apply()
    {
        $value = [ 'mage_status' => 2, 'dccp_status' => 3 ];
        $this->_assignStatusFactory->create()->setData($value)->save();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            \Formax\AssignProduct\Setup\Patch\Data\InitializeDccpAssignStatusTable::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
