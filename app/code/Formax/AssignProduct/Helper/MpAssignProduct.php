<?php

namespace Formax\AssignProduct\Helper;

use DateTime;
use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Linets\GasSetup\Constants;
use Linets\RegulatedProduct\ViewModel\RegulatedData;
use Linets\InsumosSetup\Model\InsumosConstants;
use Linets\PriceManagement\Constants as PriceManagementConstants;
use Linets\PriceManagement\Helper\Config;
use Cleverit\ComputadoresRenewal\Constants as ComputadoresRenewalConstants;
use Intellicore\ComputadoresRenewal2\Constants as ComputadoresRenewal2Constants;
use Formax\AssignProduct\Model\ResourceModel\AssignStatus\Collection;
use Formax\AssignProduct\Model\ResourceModel\AssignStatus\CollectionFactory as AssignProductCollectionFactory;
use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Linets\VoucherSetup\Model\Constants as LinetsVoucherConstants;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ProductConfigurable;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Serialize\SerializerInterface;
use Summa\EmergenciasSetUp\Helper\Data;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as SellerCollection;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemsCollection;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollection;
use Webkul\MpAssignProduct\Model\ResourceModel\Quote\CollectionFactory as QuoteCollection;
use Webkul\MpAssignProduct\Model\ResourceModel\Data\CollectionFactory as DataCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Formax\AssignProduct\Model\ResourceModel\AssignProduct\CollectionFactory as AssignProductFileCollection;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableCollection;
use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;
use Summa\CombustiblesSetUp\Helper\Data as CombustiblesHelper;
use Chilecompra\CombustiblesRenewal\Model\Constants as CombustiblesRenewal202110;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Linets\VehiculosSetUp\Model\Constants as VehiculosConstants;
use Webkul\MpAssignProduct\Model\Zip;
use Linets\Offer\Helper\Data as GasDataHelper;
use Magento\Framework\App\Area;

class MpAssignProduct extends \Webkul\MpAssignProduct\Helper\Data
{
    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * @var AssignProductFileCollection
     */
    protected $assignProductFileCollection;

    /**
     * @var \Webkul\SellerSubAccount\Helper\Data
     */
    protected $helperWk;

    /**
     * @var \Formax\AssignProduct\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $_eavAttribute;

    /**
     * @var \Formax\StatusChange\Helper\Data
     */
    protected $_helperSellerStatus;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;

    /**
     * @var \Chilecompra\LoginMp\Helper\HelperAdmin
     */
    protected $_helperAdmin;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $_authSession;

    /** @var AssignProductCollectionFactory */
    protected $_assignStatusCollectionFactory;

    /**
     * @var AssociatesCollection $associatesCollectionFactory
     */
    protected $associatesCollectionFactory;

    /**
     * @var string
     */
    const CONVENIO_ASEO = 'aseo';

    /**
     * @var string
     */
    const CONVENIO_VOUCHER = 'voucher';

    const CONVENIO_VOUCHER_202106 = 'voucher202106';

    const CONVENIO_MOBILIARIO = 'mobiliario';

    const WHITE_LIST_STATUS = ["1"];

    /**
     * @var int
     */
    const DISAPPROVE_STATUS = 0;

	/**
	 * @var Config
	 */
	private $helperConfig;

    /**
     * @var array
     */
    private $sellerStatuses;

    /**
     * @var \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceAdjustment\CollectionFactory
     */
    protected $_temporaryPriceAdjustmentCollection;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_configurableProductType;

    /**
     * @var \Magento\Directory\Model\Country
     */
    protected $regionFactory;
    /**
     * @var RegulatedData
     */
    private $regulatedData;

    /** @var State */
    protected $_state;

    /**
     * @var \Formax\RegionalCondition\Helper\Data
     */
    private $_regionalData;

    /**
     * @var CategoryFactory
     */
    private $_categoryFactory;

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollectionFactory;

    private \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoFactory;

    private ResourceConnection $resourceConnection;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\CustomerFactory $customer
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Framework\Pricing\Helper\Data $currency
     * @param ResourceConnection $resource
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Webkul\Marketplace\Model\ProductFactory $mpProductFactory
     * @param \Webkul\MpAssignProduct\Model\ItemsFactory $itemsFactory
     * @param \Webkul\MpAssignProduct\Model\DataFactory $dataFactory
     * @param \Webkul\MpAssignProduct\Model\AssociatesFactory $associatesFactory
     * @param \Magento\Quote\Model\Quote\Item\OptionFactory $quoteOption
     * @param \Webkul\SellerSubAccount\Helper\Data $helperWk
     * @param CollectionFactory $mpProductCollectionFactory
     * @param SellerCollection $sellerCollectionFactory
     * @param ItemsCollection $itemsCollectionFactory
     * @param AssociatesCollection $associatesCollectionFactory
     * @param QuoteCollection $quoteCollectionFactory
     * @param DataCollection $dataCollectionFactory
     * @param ProductCollection $productCollectionFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param File $fileDriver
     * @param ConfigurableCollection $configurableCollection
     * @param \Magento\Catalog\Model\Product\Option $customOptions
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param \Magento\Catalog\Model\Product\Action $productAction
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\File\Csv $csvReader
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Webkul\MpAssignProduct\Model\ProfileFactory $profile
     * @param SerializerInterface $serializerInterface
     * @param File $file
     * @param Zip $zip
     * @param \Formax\RegionalCondition\Helper\Data $regionalData
     * @param AssignProductFileCollection $assignProductFileCollection
     * @param \Formax\AssignProduct\Logger\Logger $logger
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param CategoryFactory $categoryFactory
     * @param \Formax\StatusChange\Helper\Data $helperSellerStatus
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Chilecompra\LoginMp\Helper\HelperAdmin $helperAdmin
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param AssignProductCollectionFactory $assignStatusCollectionFactory
     * @param Config $helperConfig
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceAdjustment\CollectionFactory $temporaryPriceAdjustmentCollection
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProductType
     * @param \Magento\Directory\Model\Country $regionFactory
     * @param RegulatedData $regulatedData
     * @param \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoFactory
     * @param ResourceConnection $resourceConnection
     * @param State $state
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customer,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Framework\Pricing\Helper\Data $currency,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Webkul\Marketplace\Model\ProductFactory $mpProductFactory,
        \Webkul\MpAssignProduct\Model\ItemsFactory $itemsFactory,
        \Webkul\MpAssignProduct\Model\DataFactory $dataFactory,
        \Webkul\MpAssignProduct\Model\AssociatesFactory $associatesFactory,
        \Magento\Quote\Model\Quote\Item\OptionFactory $quoteOption,
        \Webkul\SellerSubAccount\Helper\Data $helperWk,
        CollectionFactory $mpProductCollectionFactory,
        SellerCollection $sellerCollectionFactory,
        ItemsCollection $itemsCollectionFactory,
        AssociatesCollection $associatesCollectionFactory,
        QuoteCollection $quoteCollectionFactory,
        DataCollection $dataCollectionFactory,
        ProductCollection $productCollectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Filesystem\Driver\File $fileDriver,
        ConfigurableCollection $configurableCollection,
        \Magento\Catalog\Model\Product\Option $customOptions,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface,
        \Magento\Framework\Escaper $escaper,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
        \Magento\Catalog\Model\Product\Action $productAction,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\File\Csv $csvReader,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Webkul\MpAssignProduct\Model\ProfileFactory $profile,
        SerializerInterface $serializerInterface,
        File $file,
        Zip $zip,
        \Formax\RegionalCondition\Helper\Data $regionalData,
        AssignProductFileCollection $assignProductFileCollection,
        \Formax\AssignProduct\Logger\Logger $logger,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        CategoryFactory $categoryFactory,
        \Formax\StatusChange\Helper\Data $helperSellerStatus,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Chilecompra\LoginMp\Helper\HelperAdmin $helperAdmin,
        \Magento\Backend\Model\Auth\Session $authSession,
        AssignProductCollectionFactory $assignStatusCollectionFactory,
        Config $helperConfig,
        CategoryCollectionFactory $categoryCollectionFactory,
        \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceAdjustment\CollectionFactory $temporaryPriceAdjustmentCollection,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProductType,
        \Magento\Directory\Model\Country $regionFactory,
        RegulatedData $regulatedData,
        \Webkul\Requestforquote\Model\ResourceModel\Info\CollectionFactory $infoFactory,
        ResourceConnection $resourceConnection,
        State $state
    ) {
        parent::__construct(
            $context,
            $storeManager,
            $messageManager,
            $customerSession,
            $customer,
            $filesystem,
            $formKey,
            $currency,
            $resource,
            $fileUploaderFactory,
            $productFactory,
            $cart,
            $mpProductFactory,
            $itemsFactory,
            $dataFactory,
            $associatesFactory,
            $quoteOption,
            $mpProductCollectionFactory,
            $sellerCollectionFactory,
            $itemsCollectionFactory,
            $quoteCollectionFactory,
            $dataCollectionFactory,
            $productCollectionFactory,
            $coreRegistry,
            $stockRegistry,
            $transportBuilder,
            $inlineTranslation,
            $priceCurrency,
            $fileDriver,
            $configurableCollection,
            $customOptions,
            $moduleManager,
            $stockStateInterface,
            $escaper,
            $eavAttribute,
            $productAction,
            $productRepository = null,
            $csvReader = null,
            $mpHelper = null,
            $profile = null,
            $serializerInterface = null,
            $file = null,
            $zip = null,
            $infoFactory,
            $resourceConnection
        );

        $this->request = $request;
        $this->moduleManager = $moduleManager;
        $this->httpClientFactory = $httpClientFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->helperWk = $helperWk;
        $this->timezone = $timezone;
        $this->_eavAttribute = $eavAttribute;
        $this->_helperSellerStatus = $helperSellerStatus;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_helperAdmin = $helperAdmin;
        $this->_authSession = $authSession;
        $this->assignProductFileCollection = $assignProductFileCollection;
        $this->_assignStatusCollectionFactory = $assignStatusCollectionFactory;
        $this->associatesCollectionFactory = $associatesCollectionFactory;
        $this->connection = $this->_resource->getConnection();

		$this->helperConfig = $helperConfig;
        $this->sellerStatuses = [];
        $this->_temporaryPriceAdjustmentCollection = $temporaryPriceAdjustmentCollection;
        $this->_configurableProductType = $configurableProductType;
        $this->regionFactory = $regionFactory;
        $this->regulatedData = $regulatedData;
        $this->_regionalData = $regionalData;
        $this->_categoryFactory = $categoryFactory->create();
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->infoFactory = $infoFactory;
        $this->resourceConnection = $resourceConnection;
        $this->_state = $state;
    }


    /**
     * Update Stock Data of Product
     *
     * @param int $productId
     * @param int $qty
     * @param int $flag [optional]
     * @param int $oldQty [optional]
     */
    public function updateStockData($productId, $qty, $flag = 0, $oldQty = 0)
    {
        try {
            $product = $this->getProduct($productId);
            $stockItem = $this->_stockRegistry->getStockItem($productId);
            if ($flag == 1) {
                $qty = $qty + $stockItem->getQty() - $oldQty;
            } elseif ($flag == 2) {
                $qty = $stockItem->getQty() - $qty;
            } elseif ($flag == 3) {
                $qty = $qty;
            } else {
                $qty = $qty + $stockItem->getQty();
            }
            //MU24-202 - prevent qty = 0,9999
            if ($qty < 99999999) $qty = 99999999;
            $stockItem->setData('qty', $qty);
            $stockItem->save();
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }

    /**
     * @param $data
     * @param $validateDocument
     * @param $edit
     * @return array
     * @throws LocalizedException
     */
    public function validateConfigData($data, $validateDocument = true, $edit = false)
    {
        $websiteCode = $this->_storeManager->getWebsite()->getCode();
        $approvedRequiredSites = [
            InsumosConstants::WEBSITE_CODE,
            \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE,
            AseoRenewalConstants::WEBSITE_CODE];
        $result = ['error' => false, 'msg' => ''];
        $msg = "";

        $files = in_array($websiteCode, $approvedRequiredSites)
        ? $this->request->getFiles()
        :         $this->request->getFiles('products');

        $valProductId = isset($data['product_id']) ? (int)$data['product_id'] : 0;
        $anyChildProductNotAssignedToSeller = false;
        foreach ($data['products'] as $childProductId => $info) {
            $hasSeller = $this->isChildProductAssignToSeller($childProductId, $data['product_id']);
            if (!$hasSeller) {
                $anyChildProductNotAssignedToSeller = true;
                break;
            }
        }
        $p = $this->getProduct($valProductId);

        try {
            if (trim($data['product_condition']) == "") {
                $msg .= "Product condition is required field.";
                $result['error'] = true;
            }
            if (trim($data['description']) == "") {
                $data['description'] = '.';
            }
            if(in_array($websiteCode, $approvedRequiredSites)) {
                if ($validateDocument &&
                    (!isset($data['assign_id']) || $this->_storeManager->getWebsite()->getCode() === VehiculosConstants::WEBSITE_CODE) &&
                    $anyChildProductNotAssignedToSeller
                ) {
                    if (!isset($files['document_' . $valProductId]['name']) ||
                        (isset($files['document_' . $valProductId]['name']) &&
                            $files['document_' . $valProductId]['name'] == '')) {
                        $msg .= 'Document is required field.';
                        $result['error'] = true;
                        $result['msg'] = $msg;
                    }
                }
            }else{
                foreach ($data['products'] as $childProductId => $info) {
                    if ($validateDocument &&
                        (!isset($data['assign_id']) || $this->_storeManager->getWebsite()->getCode() === VehiculosConstants::WEBSITE_CODE) &&
                        $anyChildProductNotAssignedToSeller && $info['qty'] > 0
                    ) {
                        if (!isset($files['document_' . $childProductId]['name']) ||
                            (isset($files['document_' . $childProductId]['name']) &&
                                $files['document_' . $childProductId]['name'] == '')) {
                            $msg .= 'Document is required field.';
                            $result['error'] = true;
                            $result['msg'] = $msg;
                        }
                    }
                }
            }

            $count = 0;
            foreach ($data['products'] as $productId => $info) {
                if (array_key_exists("id", $info)) {

                    $price = ($this->_storeManager->getWebsite()->getCode() === Data::WEBSITE_CODE) ? $info['base_price'] : $info['price'];
                    $validateQty = $this->validateQty($info['qty']);
                    $validatePrice = $this->validatePrice($price);
                    $validatePriceMin = (!$edit && (!isset($info['associate_id']) || (isset($info['associate_id']) && (int)$info['associate_id'] === 0))) ?
                        $this->validatePriceMin($p, $price) :
                        ['error' => false, 'msg' => ''];

                    if ($validateQty['error']) {
                        $msg .= $validateQty['msg'];
                        $result['error'] = true;
                        $result['msg'] = $msg;
                        return $result;
                    }
                    if ($validatePrice['error']) {
                        $msg .= $validatePrice['msg'];
                        $result['error'] = true;
                        $result['msg'] = $msg;
                        return $result;
                    }
                    if ($validatePriceMin['error']) {
                        $msg .= $validatePriceMin['msg'];
                        $result['error'] = true;
                        $result['msg'] = $msg;
                        return $result;
                    }
                    $count++;
                }
            }
            if ($count == 0) {
                $msg .= "Please select associated products.";
                $result['error'] = true;
            }
            $result['msg'] = $msg;
        } catch (\Exception $e) {
            $result['error'] = true;
            $result['msg'] = "Something went wrong.";
        }
        return $result;
    }

    /**
     * Validate Data
     *
     * @param array $data
     * @param string type
     * @param bool $validateDocument [optional]
     * @param bool $edit [optional]
     * @return array
     */
    public function validateData($data, $type, $validateDocument = false, $edit = false)
    {
        if ($type == "configurable") {
            return $this->validateConfigData($data, $validateDocument, $edit);
        }

        $result = ['error' => false, 'msg' => ''];
        $msg = "";
        $productId = (int)$data['product_id'];
        $p = $this->getProduct($productId);
        $price = ($this->_storeManager->getWebsite()->getCode() === Data::WEBSITE_CODE) ? $data['base_price'] : $data['price'];
        $validatePriceMin = !$edit ?
            $this->validatePriceMin($p, $price) :
            ['error' => false, 'msg' => ''];

        //todo mobiliario - pricechek min value?maxvalue?
        try {
            if (trim($data['product_condition']) == "") {
                $msg .= "Product condition is required field.";
                $result['error'] = true;
            }
            if (trim($data['price']) == "") {
                $msg .= "Price is required field.";
                $result['error'] = true;
            } else {
                if (!is_numeric(trim($data['price']))) {
                    $msg .= "Price should be numeric.";
                    $result['error'] = true;
                } else {
                    if ($validatePriceMin['error']) {
                        $msg .= $validatePriceMin['msg'];
                        $result['error'] = true;
                        $result['msg'] = $msg;
                    }
                }
            }

            if (trim($data['qty']) == "") {
                $msg .= "Quantity is required field.";
                $result['error'] = true;
            } else {
                if (!is_numeric(trim($data['qty']))) {
                    $msg .= "Quantity should be numeric.";
                    $result['error'] = true;
                }
            }

            if (trim($data['description']) == "") {
                $data['description'] = '.';
            }

            if ($validateDocument && !isset($data['assign_id']) && !$this->isProductAssignToSeller($productId)) {
                $files = $this->request->getFiles();
                if (!isset($files['document_' . $productId]['name']) ||
                    (isset($files['document_' . $productId]['name']) && $files['document_' . $productId]['name'] == '')) {
                    $msg = 'Document is required field.';
                    $result['error'] = true;
                    $result['msg'] = $msg;
                }
            }

            $result['msg'] = $msg;
        } catch (\Exception $e) {
            $result['error'] = true;
            $result['msg'] = "Something went wrong.";
        }
        return $result;
    }

    public function getOldStatusConfigurable($itemId){

        $collectionOffersConf = $this->associatesCollectionFactory->create();
        $collectionOffersConf->addFieldToFilter('main_table.status', 1)
            ->addFieldToFilter('main_table.dis_dispersion', 0)
            ->addFieldToFilter('main_table.product_id', $itemId);

        if ($collectionOffersConf->count() == 0){
            return 0; //el producto originalmente esta desactivado.
        }else{
            //si está habilitado sin Stock retorna 0
            //si está habilitado con Stock retorna su stock
            return $collectionOffersConf->getFirstItem()->getQty();
        }

    }

    public function countAssignOffer($sellerId){
        $collectionOffersConf = $this->associatesCollectionFactory->create();

        $collectonItemsSimples = $this->_itemsCollection
            ->create()
            ->addFieldToFilter("seller_id", $sellerId)
            ->addFieldToFilter("status", 1)
            ->addFieldToFilter("type", 'simple');

        $allConfIdsForSeller = $this->_itemsCollection
            ->create()
            ->addFieldToFilter("seller_id", $sellerId)
            ->addFieldToFilter("type", 'configurable')
            ->addFieldToSelect("id")
            ->getAllIds();


        $collectionOffersConf->addFieldToFilter('main_table.status', 1)
            ->addFieldToFilter('main_table.dis_dispersion', 0)
            ->addFieldToFilter('main_table.qty', ['gt' => 0])
            ->addFieldToFilter('main_table.parent_id', ['in' => $allConfIdsForSeller]);

        return $collectionOffersConf->count() + $collectonItemsSimples->count();
    }

    /**
     * @param $productId
     * @param $data
     * @param $editingFlag
     * @param $_status
     * @param $isFromApi
     * @return int
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function preProcessAssignStatusChild($productId, $data, $editingFlag = 0, $_status = null, $isFromApi=false)
    {
        $websiteCode = $this->_storeManager->getWebsite()->getCode();
        $approvedRequiredSites = [InsumosConstants::WEBSITE_CODE, AseoRenewalConstants::WEBSITE_CODE];

        if ($editingFlag == 1) {
            $associatedId =  $data['products'][$productId]['associate_id'];
            /* PHPSTAN - return type of getAssociatedItem() is wrong in 3rd party module*/
            $associate = $this->getAssociatedItem($associatedId); /** @phpstan-ignore-line */
            if ($associate->getId() > 0) {
                $status = $associate->getStatus();
                return $status;
            }
        }

        if(in_array($websiteCode, $approvedRequiredSites)){
            $isProductAssignToSeller = $this->isChildProductAssignToSeller($productId, $data['product_id']);
        }else{
            $isProductAssignToSeller = $this->isChildProductAssignedSellerVisible($productId, $data['product_id']);
        }

        $status = $_status !== null ? (int)$_status : ($isProductAssignToSeller ? 1 : 2);
        if ($isFromApi && isset($data['products'][$productId]['status'])){
            $status = $data['status']; //status de la api.
        }

        return $status;
    }


    /**
     * Pre-process assign product status
     *
     * @param      $data
     * @param int  $editingFlag
     * @param null $_status
     * @return int
     * @throws LocalizedException
     */
    public function preProcessAssignStatus($data, $editingFlag = 0, $_status = null)
    {
        $productId = (int) $data['product_id'];
        $productObj = $this->getProduct($productId);
        $isQuimico = (bool)$productObj->getData('is_quimico');
        $storeCode = $this->getStoreCode();
        $websiteCode = $this->getWebsiteCode();
        $status = $_status;
        if ($editingFlag == 0) {
            if ($storeCode == self::CONVENIO_ASEO && $isQuimico) {
                $isProductAssignedToAnySeller = false;
            } else {
                $isProductAssignedToAnySeller = $this->isProductAssignToSeller($productId);
            }
            $aprovedRequiredSites = [
                Data::WEBSITE_CODE,
                ConstantsEmergency202109::WEBSITE_CODE,
                \Summa\ComputadoresSetUp\Helper\Data::WEBSITE_CODE,
                \Cleverit\ComputadoresRenewal\Constants::WEBSITE_CODE,
                \Intellicore\ComputadoresRenewal2\Constants::WEBSITE_CODE
            ];

            if(in_array($websiteCode, $aprovedRequiredSites) && isset($data['add']) && $data['add'] == 1){
                $status = 2;
            }else{
                if ($_status !== null){
                    $status = (int)$_status;
                } else{
                    $status = ($isProductAssignedToAnySeller ? 1 : 2);
                }
            }
        } else {
            // Check Status if product is being updated
            if ($storeCode == 'computadores' || $storeCode == ComputadoresRenewalConstants::STORE_CODE || $storeCode == ComputadoresRenewal2Constants::STORE_CODE){ //computadores or computadores202101
                if ($editingFlag == 1) {//estoy editado producto
                    $assignId = $data['assign_id'];
                    $model = $this->getAssignDataByAssignId($assignId); //levanto el modelo existeinte
                    $status = $model->getStatus(); //recuperamos el starus original.
                    if ($status == 0) {//si está desaprobado
                        $status = 2;
                    }
                }
            } elseif (
                $this->getWebsite() ==  CombustiblesHelper::WEBSITE_CODE ||
                $this->getWebsite() == CombustiblesRenewal202110::WEBSITE_CODE
            ){
                if ($editingFlag == 1) {//estoy editado producto
                    $assignId = $data['assign_id'];
                    $model = $this->getAssignDataByAssignId($assignId); //levanto el modelo existeinte
                    if ($model->getOrigData('qty') > 0 && $data['qty'] == 0){
                        $status = 2;
                    }
                }
            }

            //Summa - agregar website si conserva estado
            $otrosConvenioMarco = [
                \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE,
                \Formax\Offer\Helper\Data::CONVENIO_VOUCHER_WEBSITE,
                \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE,
                \Formax\RegionalCondition\Helper\Config::WEBSITE_CODE_FERRETERIA,
                \Formax\RegionalCondition\Helper\Config::WEBSITE_CODE_ASEO,
                \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE,
                \Summa\EscritorioSetUp\Helper\Data::WEBSITE_CODE,
                LinetsVoucherConstants::WEBSITE_CODE,
                InsumosConstants::WEBSITE_CODE,
                AseoRenewalConstants::WEBSITE_CODE
            ];
            if (in_array($this->getWebsite(), $otrosConvenioMarco)){
                if ($editingFlag == 1){
                    $assignId = $data['assign_id'];
                    $model = $this->getAssignDataByAssignId($assignId);
                    $status = $model->getStatus(); //recuperamos el status original.
                }
            }
        }

        return $status;
    }

    /**
     * @param \Magento\Checkout\Model\Cart $cartModel
     * @param mixed $id
     * @throws \Exception
     */
    public function removeCartItem($cartModel, $id)
    {
        $cartModel->removeItem($id)->save();
        return $cartModel;
    }

    /**
     * Check Product Quantities are Available from Seller on Cart
     */
    public function checkStatus()
    {
        $result = [];
        $updateRequired = false;
        $cartModel = $this->getCart();
        $quote = $cartModel->getQuote();
        foreach ($quote->getAllItems() as $item) {
            $deleteRequire = false;
            $product = $item->getProduct();
            $productId = $item->getProductId();
            $productType = $product->getTypeId();
            if ($productType == "configurable") {
                continue;
            }
            $allowedProductTypes = $this->getAllowedProductTypes();
            if (in_array($productType, $allowedProductTypes)) {
                $item = $item->getParentItem() ? $item->getParentItem() : $item;
                $product = $item->getProduct();
                $parentProductId = $item->getProductId();
                $productType = $product->getTypeId();
                $itemId = $item->getId();
                $requestedQty = $item->getQty();
                $assignData = $this->getAssignDataByItemId($itemId);
                if ($assignData['assign_id'] > 0) {
                    $assignId = $assignData['assign_id'];
                    if ($this->isEnabled($assignId)) {
                        if ($assignData['child_assign_id'] > 0) {
                            $childAssignId = $assignData['child_assign_id'];
                            $qty = $this->getAssociteQty($assignId, $childAssignId);
                        } else {
                            $assignData = $this->getAssignDataByAssignId($assignId);
                            $qty = $assignData->getQty();
                        }
                    } else {
                        $qty = 0;
                        $deleteRequire = true;
                        $this->_messageManager
                            ->addError(__('Product is currently not available from seller.'));
                    }
                } else {
                    /*$qty = $this->getOriginalQty($productId, $productType, $parentProductId);*/
                    $stockItem = $this->_stockRegistry->getStockItem($productId);
                    $qty = $stockItem->getQty();
                }
                if ($requestedQty > $qty) {
                    $updateRequired = true;
                    $item->setQty($qty);
                    $this->_messageManager
                        ->addError(__('Stock is not available.'));
                }
                $result[] = $qty;
                if ($qty <= 0 || $deleteRequire) {
                    $cartModel = $this->removeCartItem($cartModel, $itemId);
                }
            }
        }
        if ($updateRequired) {
            $cartModel->save();
        }
    }

    /**
     * Assign Product to Seller
     *
     * @param array $data
     * @param int $flag
     * @param null $_sellerId
     * @param null $_status
     * @param null $websiteCode
     * @return array
     * @throws \Exception
     */
    public function assignProduct($data, $flag = 0, $_sellerId = null, $_status = null, $isFromApi= false)
    {
        $productId = (int) $data['product_id'];
        $productObj = $this->getProduct($productId);
        $productRowId = $productObj->getRowId();
        $storeCode = $this->getStoreCode();

        if ($isFromApi && isset($data['status'])){
            $status = $data['status']; //status del producto simple
        }else{
            $status = $this->preProcessAssignStatus($data, $flag, $_status);
        }

        $result = [
            'assign_id' => 0,
            'product_id' => 0,
            'product_row_id' => 0,
            'error' => 0,
            'msg' => '',
            'qty' => 0,
            'flag' => 0,
            'status' => $status,
            'type' => 0
        ];

        $condition = (int) $data['product_condition'];
        $qty = (int) $data['qty'];
        $price = (float) $data['price'];
        $description = trim($data['description']) ? trim($data['description']) : '.';
        $image = $data['image'];
        $base_price = isset($data['base_price']) ? $data['base_price'] : null;
        $shipping_price = isset($data['shipping_price']) ? $data['shipping_price'] : null;
        $assembly_price = isset($data['assembly_price']) ? $data['assembly_price'] : null;
        $ownerId = $this->getSellerIdByProductId($productId);
        $sellerId = $_sellerId !== null  ? $_sellerId : $this->getCustomerId();
        $type = $productObj->getTypeId();
        $date = date('Y-m-d');
        $result['condition'] = $condition;

        if ($qty < 0) {
            $qty = 0;
        }

        if (!$sellerId){
            $sellerId = isset($data['seller_id']) ? $data['seller_id'] : $this->getCustomerId();
        }

        $assignProductData = [
            'product_id' => $productId,
            'product_row_id' => $productRowId,
            'owner_id' => $ownerId,
            'seller_id' => $sellerId,
            'qty' => $qty,
            'price' => $price,
            'description' => $description,
            'condition' => $condition,
            'type' => $type,
            'created_at' => $date,
            'image' => $image,
            'base_price' => $base_price,
            'shipping_price' => $shipping_price,
            'assembly_price' => $assembly_price,
            'status' => $status
        ];

        // MPMT-16: set dis_dispersion to 0 if the product status is automatically approved
        if ($status == 1) {
            $assignProductData['dis_dispersion'] = 0;
        }

        if ($image == '') {
            unset($assignProductData['image']);
        }
        if ($data['del'] == 1) {
            $assignProductData['image'] = "";
        }
        if ((int)$assignProductData['status'] !== 0) {
            $assignProductData['comment'] = '';
        }

        $model = $this->_items->create();
        if ($flag == 1) {
            // Offer is being updated
            $assignId = $data['assign_id'];
            $model = $this->getAssignDataByAssignId($assignId);


            if ($this->moduleManager->isOutputEnabled('Formax_Offer')) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $helperOffer = $objectManager->create('Formax\Offer\Helper\Data');
                $priceArray = $helperOffer->getBasePrice($assignId, $type);
                $oldPrice = $priceArray[0];
                $priceArrayBase = $helperOffer->getBasePrice($assignId, $type, true);
                $oldBasePriceItem = $priceArrayBase[0];

            } else {
                if ($this->_storeManager->getWebsite()->getCode() === Data::WEBSITE_CODE) {
                    $oldPrice = $model->getBasePrice();
                } else {
                    $oldPrice = $model->getPrice();
                }
                $oldBasePriceItem = 0;
            }

            if ($model->getId() > 0) {
                $isBonification = $productObj->getData('voucher_product_type');

                // Validate price must be less or equal than old price
                if (
                    ($this->_storeManager->getWebsite()->getCode() !== CombustiblesHelper::WEBSITE_CODE) &&
                    ($this->_storeManager->getWebsite()->getCode() !== CombustiblesRenewal202110::WEBSITE_CODE)
                ) {
                    if ($this->_storeManager->getWebsite()->getCode() === Data::WEBSITE_CODE) { //convenio emergencias
                        $oldShippingPrice = $model->getShippingPrice();
                        if ($base_price > (float)$oldBasePriceItem) {
                            $result['error'] = true;
                            $result['msg'] = __('Base price must be less or equal than previous ones. Either the related products. Product id: %1 - Old base price: %2', $productObj->getSku(), $this->currencyFormat($oldBasePriceItem));
                            return $result;
                        }
                        if ($shipping_price > (float)$oldShippingPrice) {
                            $result['error'] = true;
                            $result['msg'] = __('Shipping price must be less or equal than previous ones. Product id: %1 - Old shipping price: %2', $productObj->getSku(), $this->currencyFormat($oldShippingPrice));
                            return $result;
                        }
                    }elseif ($this->_storeManager->getWebsite()->getCode() === \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) { //convenio moviliario
                        $oldAssemblyPrice = $model->getAssemblyPrice();
                        if ($base_price > (float)$oldBasePriceItem) {
                            $result['error'] = true;
                            $result['msg'] = __('Base price must be less or equal than previous ones. Either the related products. Product id: %1 - Old base price: %2', $productObj->getSku(), $this->currencyFormat($oldBasePriceItem));
                            return $result;
                        }
                    } elseif (in_array($this->getStoreCode(),[self::CONVENIO_VOUCHER, LinetsVoucherConstants::STORE_VIEW_CODE]) && ((int)$isBonification === 1 || (int)$isBonification === 2)) {
                        // Validate price must be greather or equal than old price
                        if ($price < (float)$oldPrice) {
                            $result['error'] = true;
                            $result['msg'] = __('Percent must be greather or equal than %1', number_format($oldPrice, 1) . '%');
                            return $result;
                        }
                    } else {
                        // Validate price must be less or equal than old price
                        if ((float)$price > (float)$oldPrice) {
                            $msg = $this->getStoreCode() == self::CONVENIO_VOUCHER
                                ? __('Percent must be less or equal than %1', number_format($oldPrice, 1) . '%')
                                : __('Price must be less or equal than %1', $this->currencyFormat($oldPrice));
                            $result['error'] = true;
                            $result['msg'] = $msg;
                            return $result;
                        }
                    }
                }


                $oldImage = $model->getImage();
                if ($oldImage != $image && $image != "") {
                    $assignProductData['image'] = $image;
                }
                $oldQty = $model->getQty();
                $status = $model->getStatus();
                $result['old_qty'] = $oldQty;
                $result['prev_status'] = $status;
                $result['flag'] = 1;
                unset($assignProductData['created_at']);

                if (
                    $storeCode != 'computadores' &&
                    $storeCode != ComputadoresRenewalConstants::STORE_CODE &&
                    $storeCode != ComputadoresRenewal2Constants::STORE_CODE &&
                    $storeCode != CombustiblesHelper::WEBSITE_CODE &&
                    $storeCode != CombustiblesRenewal202110::WEBSITE_CODE
                ) {
                    if ($this->isEditApprovalRequired() || $this->isAddApprovalRequired()) {
                        $result['status'] = 0;
                        $assignProductData['status'] = 0;
                    }

                    if ($isFromApi && isset($data['status'])) {
                        $result['status'] = $data['status']; //status del producto simple
                    }
                }
            } else {
                return $result;
            }
            $model->addData($assignProductData)->save();
            $this->manageDescription($data, $assignId, $isFromApi);
        } else {
            // Creating new offer
            // Before creating a new item, we need to verify that there doesn't exist an item with the same data (avoid duplicate)
            $itemWithSameDataExists = $this->checkIfItemWithSameDataExists($assignProductData);
            // if there is not an item with same data, create it
            if (!$itemWithSameDataExists) {
                if ($storeCode != 'computadores' && $storeCode != ComputadoresRenewalConstants::STORE_CODE && $storeCode != ComputadoresRenewal2Constants::STORE_CODE){
                    if ($this->isAddApprovalRequired()) {
                        $result['status'] = 0;
                        $assignProductData['status'] = 0;
                    }
                    if ($isFromApi && isset($data['status'])){
                        $result['status'] = $data['status']; //status del producto simple
                    }
                }
                $model->setData($assignProductData)->save();
                $this->manageDescription($data, $model->getId());
            } else {
                // there exist an item with same data, return error and avoid duplicate
                $result['error'] = true;
                $result['avoid_duplicate'] = true;
            }
        }
        if ($model->getId() > 0) {
            $result['product_id'] = $productId;
            $result['qty'] = $qty;
            $result['assign_id'] = $model->getId();
        }

        return $result;
    }

	/**
	 * @param $assignId
	 * @param $product
	 * @param $model
	 * @param bool $return
	 */
	public function setProductAssociates($assignId, $product, $model, bool $return = false)
	{
		$usedProducts = $product->getTypeInstance()->getUsedProducts($product);
		$associatesData = $this->getAssociatesData($assignId);

		$count = 0;
		$countArray = [];
		foreach ($usedProducts as $childProduct) {
			$childId = $childProduct->getId();
			if (array_key_exists($childId, $associatesData)) {
				$itemStatus = $associatesData[$childId]['status_label'];
                                $key = $associatesData[$childId]['status'];
				$countArray[$key]['count'] = (array_key_exists($key, $countArray))
					? $countArray[$key]['count'] + 1
					: 1;
                                if (!isset($countArray[$key]['label'])) {
                                    $countArray[$key]['label'] = strtolower(str_replace(' ','_', $itemStatus->getText()));
                                }

				$count++;
			}
		}

		$assignProductData[$assignId] = [
			'products_count' => $count,
			'statuses' => $countArray
		];

		if ($return) {
			return json_encode($assignProductData, JSON_UNESCAPED_SLASHES);
		}

		$data = ['associates' => json_encode($assignProductData, JSON_UNESCAPED_SLASHES)];

		$model->addData($data)->save();
    }

	/**
	 * Plugin Assign Product configurable to Seller
	 *
	 * @param array $data
	 * @param int $flag [optional]
	 * @param mixed int/null $_sellerId [optional]
	 * @param mixed int/null $_status [optional]
	 * @return array
	 * @throws \Exception
	 */
    public function assignConfigProduct($data, $flag = 0, $_sellerId = null, $_status = null, $isFromApi=false)
    {
        $websitesCodeAllowed = [InsumosConstants::WEBSITE_CODE, AseoRenewalConstants::WEBSITE_CODE];
        $websitesCode = $this->_storeManager->getWebsite()->getCode();
        $productId = (int) $data['product_id'];
        $productObj = $this->getProduct($productId);
        $productRowId = $productObj->getRowId();
        $isQuimico = (bool)$productObj->getData('is_quimico');

        if ($this->getStoreCode() == self::CONVENIO_ASEO && $isQuimico) {
            $isProductAssignedToAnySeller = false;
        } else {
            $isProductAssignedToAnySeller = $this->isProductAssignToSeller($productId);
        }

        $status = $_status !== null ? (int)$_status : ($isProductAssignedToAnySeller ? 1 : 2);
        if ($isFromApi && isset($data['status'])){
            $status = $data['status']; //status del producto padre
        }
        $result = [
            'assign_id' => 0,
            'product_id' => 0,
            'product_row_id' => 0,
            'error' => 0,
            'msg' => '',
            'qty' => 0,
            'flag' => 0,
            'status' => $status,
            'type' => 1
        ];

        $condition = (int) $data['product_condition'];
        $description = trim($data['description']) ? trim($data['description']) : '.';
        $image = $data['image'];
        $ownerId = $this->getSellerIdByProductId($productId);
        $sellerId = $_sellerId !== null ? $_sellerId : $this->getCustomerId($_sellerId);
        $type = "configurable";
        $date = date('Y-m-d');
        $qty = 0;
        $price = 0;
        $result['condition'] = $condition;

        $assignProductData = [
            'product_id' => $productId,
            'product_row_id' => $productRowId,
            'owner_id' => $ownerId,
            'seller_id' => $sellerId,
            'qty' => 0,
            'price' => 0,
            'description' => $description,
            'condition' => $condition,
            'type' => $type,
            'created_at' => $date,
            'image' => $image,
            'status' => $status
        ];

        if ($image == '') {
            unset($assignProductData['image']);
        }
        if ($data['del'] == 1) {
            $assignProductData['image'] = "";
        }
        if ((int)$assignProductData['status'] !== 0) {
            $assignProductData['comment'] = '';
        }

        $model = $this->_items->create();
        // Both regulated product and authorized product are validated with the getAssingProductFile method
        $fileCollection = $this->regulatedData->getAssingProductFile($productObj->getEntityId(), $sellerId);
        if ($flag == 1) {
            // Offer is being updated
            $assignId = $data['assign_id'];
            $model = $this->getAssignDataByAssignId($assignId);
            if ($model->getId() > 0) {
                $oldImage = $model->getImage();
                if (in_array($websitesCode, $websitesCodeAllowed) &&
                    ($productObj->getProductoRegulado() || $productObj->getProductoAutorizado()) &&
                    (empty($fileCollection) ||
                        ((!empty($fileCollection->getRegulatedProductFilePath() ||
                            !empty($fileCollection->getAuthorizedLetter()) &&
                            $fileCollection->getStatus() == 0))))) {
                    $status = 2;
                } else {
                    $status = $model->getStatus();
                }

                $result['prev_status'] = $status;
                if ($oldImage != $image && $image != "") {
                    $assignProductData['image'] = $image;
                }
                $result['flag'] = 1;
                unset($assignProductData['created_at']);
                if ($this->isEditApprovalRequired() || $this->isAddApprovalRequired()) {
                    $result['status'] = 0;
                    $assignProductData['status'] = 0;
                }
                if ($isFromApi && isset($data['status'])){
                    $result['status'] = $data['status'];
                }
            } else {
                return $result;
            }
            $model->addData($assignProductData)->save();
			$this->setProductAssociates($assignId, $productObj, $model);
            $this->manageDescription($data, $assignId);
        } else {
            // Creating new offer
            // Before creating a new item, we need to verify that there doesn't exist an item with the same data (avoid duplicate)
            $itemWithSameDataExists = $this->checkIfItemWithSameDataExists($assignProductData);
            // if there is not an item with same data, create it
            if (!$itemWithSameDataExists) {
                if ($this->isAddApprovalRequired()) {
                    $result['status'] = 0;
                    $assignProductData['status'] = 0;
                }
                if ($isFromApi && isset($data['status'])){
                    $result['status'] = $data['status'];
                }
                $model->setData($assignProductData)->save();
				$this->setProductAssociates($model->getId(), $productObj, $model);
                $this->manageDescription($data, $model->getId());
            } else {
                // there exist an item with same data, return error and avoid duplicate
                $result['error'] = true;
                $result['avoid_duplicate'] = true;
            }
        }
        if ($model->getId() > 0) {
            $result['product_id'] = $productId;
            $result['qty'] = $qty;
            $result['assign_id'] = $model->getId();
            // set associated options
            $result['associates'] = $this->setAssociatedOptions($data, $model->getId(), $productId, $isFromApi);
			      $this->setProductAssociates($model->getId(), $productObj, $model);
            $this->setConfigStatus($model);
        }

        return $result;
    }

    /**
     * @param $assignId
     * @param $status
     * @return int|mixed
     */
    public function getParentStatusInsumos($assignId, $status){
        $associatesCollection = $this->associatesCollectionFactory->create();
        $associatesCollection->addFieldToFilter('parent_id', ['eq'=> $assignId])
            ->addFieldToFilter('status', ['eq' => 1]);
        if ($associatesCollection->count() > 0) {
            $status = 1;
        }
        return $status;
    }

    /**
     * Check if there exists an item with same data, if exists it will return >0, else it will return 0
     *
     * @param $data
     * @return bool
     */
    public function checkIfItemWithSameDataExists($data)
    {
        $collection = $this->_itemsCollection->create();

        // add filters with the data to check if we don't have an item created with same data
        $collection->addFieldToFilter('seller_id', $data['seller_id']);
        $collection->addFieldToFilter('product_id', $data['product_id']);
        $collection->addFieldToFilter('product_row_id', $data['product_row_id']);

        return $collection->getSize() > 0;
    }

    public function checkIfAssociatedWithSameDataExists($data)
    {
        $collection = $this->associatesCollectionFactory->create();

        // add filters with the data to check if we don't have an item created with same data
        $collection->addFieldToFilter('parent_id', $data['parent_id']);
        $collection->addFieldToFilter('product_id', $data['product_id']);
        $collection->addFieldToFilter('parent_product_id', $data['parent_product_id']);

        if ($collection->getSize() > 0) {
            return $collection;
        } else {
            return false;
        }
    }

    /**
     * @param $data
     * @param $parentId
     * @param $parentProductId
     * @param $isFromApi
     * @return array
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function setAssociatedOptions($data, $parentId, $parentProductId, $isFromApi = false)
    {
        $approvedRequiredSites = [
            InsumosConstants::WEBSITE_CODE,
            \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE,
            AseoRenewalConstants::WEBSITE_CODE
        ];

        $priceArray = [];
        $hasFile = false;
        $isRegulated = 0;
        $isAuth = 0;
        if ($this->moduleManager->isOutputEnabled('Formax_Offer')) {
            foreach ($data['products'] as $productId => $info) {
                if (array_key_exists('id', $info)) {
                    $assignId = $parentId;
                    break;
                }
            }

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $helperOffer = $objectManager->create('Formax\Offer\Helper\Data');
            if(!empty($assignId)) {
                $priceArray = $helperOffer->getBasePrice($assignId, 'configurable');
            }
        }

        $countPriceArray = count($priceArray);
        $result = [];
        foreach ($data['products'] as $productId => $info) {
            $options = "";
            $qty = $info['qty'];
            $price = $info['price'];
            $item = [];
            $item['product_id'] = $productId;
            $item['parent_id'] = $parentId;
            $item['parent_product_id'] = $parentProductId;
            $item['options'] = $options;
            $item['qty'] = $qty;
            $item['price'] = $price;
            $item['base_price'] = isset($info['base_price']) ? $info['base_price'] : null;


            //esta funcion pone los hijos del configurable en pendiente si es la primer asignacion.
            //incluso si es por el importador de CSV.
            if (in_array($this->getWebsite(), $approvedRequiredSites)) {
                $isProductAssignToSeller = $this->isChildProductAssignToSeller($productId, $parentProductId);
            } else {
                $isProductAssignToSeller = $this->isChildProductAssignedSellerVisible($productId, $parentProductId);
            }

            $status = $isProductAssignToSeller ? 1 : 2;
            //Only Insumos
            if (in_array($this->getWebsite(), $approvedRequiredSites) && $this->moduleManager->isOutputEnabled('Linets_RegulatedProduct')
            ) {
                $objectManagerProductRegulated = \Magento\Framework\App\ObjectManager::getInstance();
                $regulatedProductVM = $objectManagerProductRegulated->create('Linets\RegulatedProduct\ViewModel\RegulatedData');
                $assignIdForRequest = $assignId ? : $data['assign_id'];
                $isRegulated = $this->getProductByAssignId($assignIdForRequest)->getProductoRegulado();
                $isAuth = $this->getProductByAssignId($assignIdForRequest)->getProductoAutorizado();
                $fileCollection = $regulatedProductVM->getAssingProductFile($parentProductId, $helperOffer->getSellerId());
                if (!empty($fileCollection) &&
                    (!empty($fileCollection->getRegulatedProductFilePath()) ||
                        !empty($fileCollection->getAuthorizedLetter()))
                ) {
                    $hasFile = true;
                }

                if (in_array($this->getWebsite(), $approvedRequiredSites) && ($isRegulated || $isAuth) &&
                    (!$hasFile || (!empty($fileCollection) && (int)$fileCollection->getStatus() == 0))
                ) {
                    $status = 2;
                }
            }

            if ($isFromApi && isset($info['status'])) {
                $status = $info['status'];
            }

            $item['status'] = $status;
            if ($status == 1) {
                $item['dis_dispersion'] = 0;
            }

            if (array_key_exists("associate_id", $info)) { //edit product case
                /* PHPSTAN - return type of getAssociatedItem() is wrong in 3rd party module*/
                $associate = $this->getAssociatedItem($info['associate_id']); /** @phpstan-ignore-line */
                $oldPrice = $countPriceArray > 0 ? (isset($priceArray[$productId]['price']) ? $priceArray[$productId]['price'] : $associate->getPrice()) : $associate->getPrice();
                $oldQty = $associate->getQty();
                $oldStatus = $associate->getStatus();

                if (in_array($this->getWebsite(), $approvedRequiredSites)) {
                    $isEdit = !((float)$price === (float)$oldPrice) || $this->isEditOfferByQty((int)$qty, $info['associate_id'], $oldQty) || ($isFromApi && isset($info['status']));
                } else {
                    $isEdit = !((float)$price === (float)$oldPrice && (float)$qty === (float)$oldQty) || ($isFromApi && isset($info['status']));
                }

                if ($price > (float)$oldPrice) {
                    $simpleProduct = $this->getProduct($productId);
                    $result['error'] = true;
                    $result['msg'] = __('Product %1 must have a price less or equal than %2', $simpleProduct->getName(), $this->currencyFormat($oldPrice));
                    return $result;
                }

                if ($isFromApi && isset($info['status'])) {
                    $item['status'] = $info['status'];
                }

                if (!$isFromApi && $isEdit && ((float)$price <= (float)$oldPrice)) {
                    $item['status'] = $associate->getStatus();
                }
                if (in_array($this->getWebsite(), $approvedRequiredSites) && ($isRegulated || $isAuth) &&
                    $hasFile && (int)$associate->getStatus() == 0) {
                    $item['status'] = 2;
                    $oldStatus = 0;
                    $isEdit = true;
                }
                if (in_array($this->getWebsite(), $approvedRequiredSites) && $isAuth &&
                    (int)$associate->getStatus() == 0 && (isset($info['id']) && isset($info['id']) == 1)) {
                    $item['status'] = 2;
                    $isEdit = true;
                }

                if (in_array($this->getWebsite(), $approvedRequiredSites) && $isEdit && $associate->getStatus() == 4) {
                    $item['status'] = 2;
                }

                // CE2-110 - Do not change status when offer is disabled by dispersion
                //if (in_array($this->getWebsite(), $approvedRequiredSites) && $isEdit && $associate->getStatus() == 3) {
                //    $item['status'] = 1;
                //}

                $result[$productId] = [
                    'qty' => $qty,
                    'old_qty' => $oldQty,
                    'manage_stock' => true,
                    'status' => $isEdit ? $item['status'] : $associate->getStatus(),
                    'old_status' => $oldStatus ?? '',
                    'flag' => 1,
                    'is_edit' => $isEdit,
                    'product_id' => $item['product_id'],
                    'associate_id' => $info['associate_id'],
                    'base_price' => $item['base_price']
                ];
                if ($isEdit) {
                    $this->updateRecord($associate, $item, $info['associate_id']);
                }
            } else if (array_key_exists("id", $info)) {
                if ($this->isAddApprovalRequired() && !$isFromApi) {
                    $item['status'] = 0;
                }
                if (in_array($this->getWebsite(), $approvedRequiredSites) && $hasFile && $isRegulated &&
                    $this->getAssignProduct($productId)->getStatus() == 2) {
                    $item['status'] = 2;
                }

                $associateAlreadyExists = $this->checkIfAssociatedWithSameDataExists($item);

                if (!$associateAlreadyExists) {
                    /* PHPSTAN - return type of newRecord() is wrong in 3rd party module*/
                    $model = $this->newRecord($this->_associates->create(), $item); /** @phpstan-ignore-line */
                    $result[$productId] = [
                        'qty' => $qty,
                        'manage_stock' => false,
                        'status' => $item['status'],
                        'old_status' => $oldStatus ?? '',
                        'flag' => 0,
                        'product_id' => $item['product_id'],
                        'is_edit' => true,
                        'associate_id' => $model->getId(),
                    ];
                } else {
                    $associate = $associateAlreadyExists->getFirstItem();
                    $associateId = $associate->getId();
                    $this->updateRecord($associate, $item, $associateId);
                }
            }
        }

        return $result;
    }

    /**
     * @param          $qty
     * @param          $productId
     * @param int|null $oldQty
     * @return bool|void
     */
    public function isEditOfferByQty($qty, $productId, int $oldQty = null)
    {
        if ((empty((int)$oldQty) && !(int)$oldQty == 0) || ($oldQty == 0 && $qty == 0)) {
            return false;
        }

        if (($oldQty == 0 && $qty == 99999999) || ($oldQty >= 1 && $qty == 0)) {
            return true;
        } elseif ($oldQty >= 1 && $qty == 99999999) {
            //Only Update stock
            $con = $this->_resource->getConnection();
            $sql = 'UPDATE marketplace_assignproduct_associated_products SET qty = '.$qty.' WHERE id = '.$productId;
            $con->query($sql);
            return false;
        }
    }

    public function getChildProducts($parentId)
    {
        $product = $this->getProduct($parentId);
        $productTypeInstance = $product->getTypeInstance();
        return $productTypeInstance->getUsedProducts($product);
    }

    /**
     * Retrieve It's first time product is assign to seller
     *
     * @param int $productId
     * @param bool $isAseoDoc
     * @return bool
     */
    public function isProductAssignToSeller($productId, $isAseoDoc = false)
    {
        $websiteCode = $this->_storeManager->getWebsite()->getCode();
        $approvedRequiredSites = [
            InsumosConstants::WEBSITE_CODE,
            \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE,
            ConstantsEmergency202109::WEBSITE_CODE,
            VehiculosConstants::WEBSITE_CODE,
            AseoRenewalConstants::WEBSITE_CODE
        ];

        $product = $this->getProduct($productId);
        if ($isAseoDoc) {
            $isQuimico = (bool)$product->getData('is_quimico');
            if ($this->getStoreCode() == self::CONVENIO_ASEO) {
                if ($isQuimico) {
                    return false;
                }
            }

            return true;
        }

        // for configurable products, we need to show the upload file field if there is any child product that has no offers
        if ($product->getTypeId() == 'configurable') {
            // Filter associated collection by parent_product_id and status 1
            $assoCollection = $this->associatesCollectionFactory->create();
            $assoCollection->addFieldToFilter('parent_product_id', $productId)
                ->addFieldToFilter('status', 1);

            // if we have child products that have offers, check all child products if any of them has no offer
            if ($assoCollection->getSize()) {
                // get child products of current parent
                $productTypeInstance = $product->getTypeInstance();
                $usedProducts = $productTypeInstance->getUsedProducts($product);

                // check for each child product if there is any with no offers
                foreach ($usedProducts as $childProduct) {
                    $childId = $childProduct->getId();
                    $collection = $this->associatesCollectionFactory->create();
                    $collection->addFieldToFilter('parent_product_id', $productId)
                        ->addFieldToFilter('status', 1);
                    $collection->addFieldToFilter('product_id', $childId);
                    // if no offer is on this child product, return false, that means that there is one
                    // child product that is not assigned to any seller, so we need to show the Upload File Field
                    if (!$collection->getSize()) {
                        return false;
                    } else {
                        $associatedCount = 0;
                        $associatedWithDisapprovedSeller = 0;
                        foreach ($collection as $associated) {
                            $associatedCount++;
                            $parentItemId = $associated->getParentId();
                            $assignItem = $this->getAssignProduct($parentItemId);
                            $sellerId = $assignItem->getSellerId();
                            if (!$sellerId) {
                                continue;
                            }
                            $sellerStatus = $this->getSellerStatus($sellerId);
                            if ($sellerStatus != '70') {
                                $associatedWithDisapprovedSeller++;
                            }
                        }
                        if ($associatedWithDisapprovedSeller == $associatedCount) {
                            // All the offers for certain product have no approved sellers
                            return false;
                        }
                    }
                }
                // If we are on this point, means that all the child products have offers, so we don't need
                // to show the Upload File Field
                return true;
            }
        }

        //Only Insumos, Alimentos, Emergencia202109, Vehiculos, Aseo2022
        if ((int)$productId > 0 && in_array($websiteCode, $approvedRequiredSites)) {
            $activeStatus = 70;
            $connection = $this->_resource->getConnection();
            $tableItems = $this->_resource->getTableName('marketplace_assignproduct_items');
            $tableAssociatedItems = $this->_resource->getTableName('marketplace_assignproduct_associated_products');
            $tableUserData = $this->_resource->getTableName('marketplace_userdata');
            $tableOffer = $this->_resource->getTableName('dccp_offer_product');
            $tableCustomerEntityVarchar = $this->_resource->getTableName('customer_entity_varchar');
            $sql = 'SELECT asso.parent_id
                FROM '. $tableAssociatedItems .' asso
                    LEFT JOIN '. $tableItems .' i ON i.id = asso.parent_id
                    LEFT JOIN '. $tableUserData .' ud ON ud.seller_id = i.seller_id
                    LEFT JOIN '. $tableOffer .' p ON asso.product_id = p.product_id AND asso.parent_id = p.assign_id
                    LEFT JOIN '. $tableCustomerEntityVarchar .' AS cv ON ud.seller_id = cv.entity_id AND cv.attribute_id = 604
                        WHERE asso.product_id = '. $productId .'
                        AND cv.value = '. $activeStatus .'
                        AND asso.status = 1
                        AND asso.qty > 0
                        ORDER BY i.seller_id DESC';
            if (count($connection->query($sql)->fetchAll()) > 0) {
                return true;
            }
            return false;
        }

        if ((int)$productId > 0) {
            $collection = $this->_itemsCollection->create();
            $collection->addFieldToFilter('main_table.product_id', $productId)
                ->addFieldToFilter('main_table.status', 1)
                ->addFieldToFilter('main_table.dis_dispersion', 0)
                ->join(
                    ['u' => $this->_resource->getTableName('marketplace_userdata')],
                    'main_table.seller_id = u.seller_id AND u.is_seller = 1',
                    []
                );
            if (count($collection->getData()) > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $productId
     * @param $parentProductId
     * @return bool
     * @throws LocalizedException
     */
    public function isChildProductAssignToSeller($productId, $parentProductId)
    {
        $approvedRequiredSites = [
            InsumosConstants::WEBSITE_CODE,
            \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE,
            AseoRenewalConstants::WEBSITE_CODE
        ];

        if ((int)$productId > 0 && (int)$parentProductId > 0) {
            $collection = $this->associatesCollectionFactory->create();
            $collection->addFieldToFilter('main_table.product_id', $productId)
                ->addFieldToFilter('main_table.parent_product_id', $parentProductId)
                ->addFieldToFilter('main_table.status', 1)
                ->addFieldToFilter('main_table.qty', ['gt' => 0])
                ->addFieldToFilter('main_table.dis_dispersion', 0)
                ->join(
                    ['i' => $this->_resource->getTableName('marketplace_assignproduct_items')],
                    'main_table.parent_id = i.id',
                    []
                )
                ->join(
                    ['u' => $this->_resource->getTableName('marketplace_userdata')],
                    'i.seller_id = u.seller_id AND u.is_seller = 1',
                    []
                );
            if (in_array($this->getWebsiteCode(), $approvedRequiredSites)) {
                $collection->join(
                    ['z' => $this->_resource->getTableName('customer_entity_varchar')],
                    "i.seller_id = z.entity_id AND z.attribute_id = 604 AND z.value = '70'",
                    []
                );
            }
            if (count($collection->getData()) > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $productId
     * @param $parentProductId
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function isChildProductAssignedSellerVisible($productId, $parentProductId)
    {
        if ((int)$productId > 0 && (int)$parentProductId > 0) {
            $collection = $this->associatesCollectionFactory->create();
            $collection->addFieldToFilter('main_table.product_id', $productId)
                ->addFieldToFilter('main_table.parent_product_id', $parentProductId)
                ->addFieldToFilter('main_table.status', 1)
                ->addFieldToFilter('main_table.qty', ['gt' => 0])
                ->addFieldToFilter('main_table.dis_dispersion', 0)
                ->join(
                    ['i' => $this->_resource->getTableName('marketplace_assignproduct_items')],
                    'main_table.parent_id = i.id',
                    ['seller_id']
                )
                ->join(
                    ['u' => $this->_resource->getTableName('marketplace_userdata')],
                    'i.seller_id = u.seller_id AND u.is_seller = 1',
                    []
                );

            if (count($collection->getData()) > 0) {
                $cnt = 0;
                foreach ($collection->getData() as $value) {
                    $sellerData = $this->_customerRepositoryInterface->getById($value['seller_id']);
                    $sellerStatus = $sellerData->getCustomAttribute('wkv_dccp_state_details')->getValue();
                    if ($sellerStatus == 70) {
                        $cnt++;
                    }
                }

                if ($cnt > 0) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get Associates
     *
     * @param int $assignId
     * @return array
     */
    public function getAssociates($assignId)
    {
        $model = $this->_associates->create();
        return $model->getCollection()->addFieldToFilter("parent_id", $assignId);
    }

    /**
     * Get Associates Data
     *
     * @param int $assignId
     * @return array
     */
    public function getAssociatesData($assignId)
    {
        $availableOptions = [__('Disapproved'), __('Approved'), __('Pending'), __('Disabled by dispersion'), __('Disabled by price'), __('Disabled by No traded Product'), __('Disabled by Empty Stock'), __('Disabled by penalty')];

        $result = [];

        $sellerId = $this->getSellerIdBySubAccount();
        $attrProductName = $this->_eavAttribute->getIdByCode('catalog_product', 'name');

        $associatesCollection = $this->associatesCollectionFactory->create();
        $associatesCollection->addFieldToFilter('main_table.parent_id', $assignId);

        $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
        $leftTableOffer = $this->_resource->getTableName('dccp_offer_product');
        $sqlOffer = 'main_table.product_id = of.product_id';
        $sqlOffer .= ' and of.product_id != of.parent_product_id';
        $sqlOffer .= ' and of.website_id = '. $websiteId;
        if ($sellerId) {
            $sqlOffer .= ' and of.seller_id = '. $sellerId;
        }
        $sqlOffer .= ' and of.status = 3';

        $fieldsOffer = [
            'offer_id' => 'id',
            'start_date' => new \Zend_Db_Expr('DATE(start_date)'),
            'end_date' => new \Zend_Db_Expr('DATE(end_date)'),
            'days',
            'base_price',
            'special_price',
            'offer_status' => 'status'
        ];

        $joinTableCatalog = $this->_resource->getTableName('catalog_product_entity');
        $sqlCatalog = 'main_table.product_id = e.entity_id';
        $fieldsCatalog = ['row_id', 'sku'];

        $joinTableCatalogVarchar = $this->_resource->getTableName('catalog_product_entity_varchar');
        $sqlCatalogVarchar = 'e.row_id = cpev.row_id';
        $sqlCatalogVarchar .= ' and cpev.attribute_id = ' . $attrProductName;
        $sqlCatalogVarchar .= ' and cpev.store_id = 0';
        $fieldsCatalogVarchar = ['name' => 'value'];

        $associatesCollection->getSelect()
            ->joinLeft($leftTableOffer . ' as of', $sqlOffer, $fieldsOffer)
            ->join($joinTableCatalog . ' as e', $sqlCatalog, $fieldsCatalog)
            ->join($joinTableCatalogVarchar . ' as cpev', $sqlCatalogVarchar, $fieldsCatalogVarchar);

        foreach ($associatesCollection as $item) {
            $info = [
                    'id' => $item->getId(),
                    'offer_id' => $item->getOfferId(),
                    'qty' => $item->getQty(),
                    'price' => number_format($item->getPrice(), 1),
                    'start_date' => $item->getStartDate(),
                    'end_date' => $item->getEndDate(),
                    'days' => $item->getDays(),
                    'base_price' => $item->getBasePrice(),
                    'special_price' => $item->getSpecialPrice(),
                    'name' => $item->getName(),
                    'dis_dispersion' => $item->getDisDispersion(),
                    'status_label' => $availableOptions[$item->getStatus()],
                    'status' => $item->getStatus(),
                    'comment' => $item->getComment(),
                    'offer_status' => $item->getOfferStatus()
                ];

            $result[$item->getProductId()] = $info;
        }
        return $result;
    }

    public function getAssociatesStatus($associateId)
    {
        $statusArray = [];
        $associatesCollection = $this->associatesCollectionFactory->create()
            ->addFieldToFilter('parent_id', $associateId);
        foreach ($associatesCollection as $associate) {
            array_push($statusArray, $associate->getStatus());
        }
        return $statusArray;
    }

    /**
     * Validate Price Min
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param int $price
     * @return array
     */
    public function validatePriceMin($product, $price)
    {
        $isVoucher = false;
        $result = ['error' => false, 'msg' => ''];
        $productId = $product->getId();
        $productType = $product->getTypeId();
        $sellerId = $this->getSellerIdBySubAccount();
        $isQuimico = (bool)$product->getData('is_quimico');
        $isMayorista = (bool)$product->getData('is_mayorista');
        $isBonification = $product->getData('voucher_product_type');
        $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
        $currentDate = $this->connection->quote($this->timezone->date()->format('Y-m-d'));

        if ((int)$productId > 0 && !empty($productType) && (int)$price > 0) {
            $currencySymbol =  $this->getCurrencySymbol();
            $tableConfigurable = $this->_resource->getTableName('marketplace_assignproduct_associated_products');
            $tableSimple = $this->_resource->getTableName('marketplace_assignproduct_items');
            $tableUserData = $this->_resource->getTableName('marketplace_userdata');
            $tableOffer = $this->_resource->getTableName('dccp_offer_product');
            $tableCatalogProductInt = $this->_resource->getTableName('catalog_product_entity_int');
            $attrVoucherProductTypeId = $this->_eavAttribute->getIdByCode('catalog_product', 'voucher_product_type');

            $sqlOpts = [
                'price' => 'MIN'
            ];

            $storeCode = $this->getStoreCode();

            if ($storeCode == self::CONVENIO_ASEO) {
                if (!$isMayorista) {
                    $sqlOpts['price'] = 'AVG';
                }
            } elseif (in_array($storeCode,[self::CONVENIO_VOUCHER, LinetsVoucherConstants::STORE_VIEW_CODE])) {
                if ((int)$isBonification === 1 || (int)$isBonification === 2) {
                    $isVoucher = true;
                    $sqlOpts['price'] = 'AVG';
                }
            }

            /* Emergency prices average */
            if ($storeCode == \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE || $storeCode == ConstantsEmergency202109::WEBSITE_CODE) {
                $sqlOpts['price'] = 'AVG';
            }

            if ($productType == 'configurable') {
                $sql = 'SELECT '.$sqlOpts['price'].'(IFNULL(o.base_price, c.price)) AS smallestPrice FROM ' . $tableConfigurable . ' c
                INNER JOIN ' . $tableSimple . ' s ON c.parent_product_id = s.product_id
                INNER JOIN ' . $tableUserData . ' u ON s.seller_id = u.seller_id AND u.is_seller = 1
                LEFT JOIN ' . $tableOffer . ' o ON s.id = o.assign_id AND o.status = 3 AND o.website_id = ' . $websiteId .
                ' AND o.special_price < o.base_price WHERE c.product_id = ' . (int)$productId .
                ' AND c.status = 1 AND c.qty > 0 AND s.dis_dispersion = 0';
            } else if (in_array($storeCode, [\Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE, ConstantsEmergency202109::WEBSITE_CODE])) {
                $sql = 'SELECT '.$sqlOpts['price'].'(s.base_price) AS smallestPrice FROM ' . $tableSimple . ' s
                INNER JOIN ' . $tableUserData . ' u ON s.seller_id = u.seller_id AND u.is_seller = 1
                LEFT JOIN ' . $tableOffer . ' o ON s.id = o.assign_id AND o.status = 3 AND o.website_id = ' . $websiteId .
                ' AND o.special_price < o.base_price WHERE s.product_id = ' . (int)$productId .
                ' AND s.status = 1 AND s.qty > 0 AND s.dis_dispersion = 0';
            } else if (in_array($storeCode,[self::CONVENIO_VOUCHER, LinetsVoucherConstants::STORE_VIEW_CODE])) {
                $sql = 'SELECT AVG(IFNULL(o.base_price, s.price)) AS smallestPrice FROM ' . $tableSimple . ' s
                LEFT JOIN ' . $tableCatalogProductInt . ' AS vpt
                ON s.product_row_id = vpt.row_id AND vpt.store_id = 0 AND vpt.attribute_id = ' . $attrVoucherProductTypeId .
                ' LEFT JOIN ' . $tableOffer . ' o ON s.id = o.assign_id AND o.status = 1 AND o.website_id = ' . $websiteId .
                ' AND ' . $currentDate . ' >= DATE(o.start_date) AND ' . $currentDate . ' <= DATE(o.end_date) AND o.special_price > o.base_price
                WHERE s.seller_id = ' . $sellerId . ' AND vpt.value = ' . VoucherHelper::BONIFICATION . ' AND s.status = 1 AND s.qty > 0 AND s.dis_dispersion = 0';
            } else {
                $sql = 'SELECT '.$sqlOpts['price'].'(IFNULL(o.base_price, s.price)) AS smallestPrice FROM ' . $tableSimple . ' s
                INNER JOIN ' . $tableUserData . ' u ON s.seller_id = u.seller_id AND u.is_seller = 1
                LEFT JOIN ' . $tableOffer . ' o ON s.id = o.assign_id AND o.status = 3 AND o.website_id = ' . $websiteId .
                ' AND o.special_price < o.base_price WHERE s.product_id = ' . (int)$productId .
                ' AND s.status = 1 AND s.qty > 0 AND s.dis_dispersion = 0';
            }

            $minPrice = (double)$this->connection->fetchOne($sql);

            if (!$isVoucher) {
                if ($minPrice > 0 && $price >= $minPrice) {
                    $product = $this->getProduct($productId);
                    $msg = __('Current min price offered for product %1 is %2 and you set greater. Please this price must be lower.',
                        $product->getName(), $this->currencyFormat($minPrice));
                    $result['error'] = true;
                    $result['msg'] = $msg;
                }
            } else {
                if ($minPrice > 0 && $price < $minPrice) {
                    $product = $this->getProduct($productId);
                    $msg = __('Current max percent offered for product %1 is %2 and you set lower. Please this percent must be greather or equal.',
                            $product->getName(), number_format($minPrice, 1, ',', '.') . '%');
                    $result['error'] = true;
                    $result['msg'] = $msg;
                }
            }
        }

        return $result;
    }

    /**
     * Get Current Customer Id
     *
     * @return int
     */
    public function getCustomerId($sellerId = null)
    {
        if ($sellerId) {
            return $sellerId;
        }

        $customerId = 0;
        if ($this->_customerSession->isLoggedIn()) {
            $customerId = (int) $this->_customerSession->getCustomerId();
        }
        return $customerId;
    }

    /**
     * Process Assigned Product
     *
     * @param array $data
     * @param int $type
     * @param int $flag
     * @param int/null $sellerId [optional]
     * @param int/null $status [optional]
     * @return array
     */
    public function processAssignProduct($data, $type, $flag, $sellerId = null, $status = null, $isFromApi= false)
    {
        if ($type == "configurable") {
            return $this->assignConfigProduct($data, $flag, $sellerId, $status, $isFromApi);
        } else {
            return $this->assignProduct($data, $flag, $sellerId, $status, $isFromApi);
        }
    }

        /**
     * Approve Associate Assign Product
     *
     * @param int $assignId
     * @param mixed string/null $comment [optional]
     */
    public function approveChildProduct($associate, $comment = null)
    {

        $status = $associate->getStatus();


            $assignProduct = $this->getAssignProduct($associate->getParentId());
            $productId = $associate->getProductId();
            $product = $this->getProduct($productId);
            $token = '';
            $adminUser = $this->getCurrentAdminUser();
            if ($adminUser) {
                $adminUserName = $adminUser->getUserName();
                $adminUserData = $this->_helperAdmin->getUserData($adminUserName);
                $token = $adminUserData->getData('user_rest_atk');
            } else {
                $token = $this->getToken();
            }
            $sellerId = $assignProduct->getSellerId();
            $seller = $this->_customerRepositoryInterface->getById($sellerId);
            $organizationId = $seller->getCustomAttribute('user_rest_id') ? $seller->getCustomAttribute('user_rest_id')->getValue() : '';
            $agreementId = '';
            if ($adminUser) {
                $agreementId = $adminUserData->getData('user_rest_id_active_agreement') ?
                    $adminUserData->getData('user_rest_id_active_agreement') : '';
            } else {
                $agreementId = $this->getAgreementId();
            }
            $qty = $associate->getQty();
            $hasStock = (float)$qty > 0 ? true : false;
            $data = [];
            $data['status'] = 1;
            $data['comment'] = $comment;
            $data['product_id'] = $associate->getProductId();
            // MPMT-16: Added line below to update the dis_dispersion value to 0 when the product is approved by the JP
            $data['dis_dispersion'] = 0;
            // CMIM-164 - Fix on CM Alimentos -> Error when approving ofers as JP, it was adding stock to offers without stock
            $data['qty'] = $hasStock ? 999999999 : 0;

            $associate->addData($data)->save();
            $this->updateStockData($productId, $qty);

            $params = [
                'idOrganismo' => $organizationId,
                'idProducto' => $product->getSku(),
                'idConvenioMarco' => $agreementId,
                'idEstadoProductoProveedorConvenio' => 1,
                'tieneStock' => $hasStock,
                'precio' => (float)$associate->getPrice()
            ];

            //falta mostrar mensaje si ocurre error con la api.
            $this->setAssignProductUpdate($token, $params);

        return $associate;
    }

    /**
     * Approve Assign Product
     *
     * @param int $assignId
     * @param string $comment
     */
    public function approveProduct($assignId, $comment = '')
    {
        $assignProduct = $this->getAssignProduct($assignId);
        if ($assignProduct->getId() > 0) {
            $status = $assignProduct->getStatus();
            if ($status != 1) {
                $productId = $assignProduct->getProductId();
                $product = $this->getProduct($productId);
                $token = '';
                $adminUser = $this->getCurrentAdminUser();
                if ($adminUser) {
                    $adminUserName = $adminUser->getUserName();
                    $adminUserData = $this->_helperAdmin->getUserData($adminUserName);
                    $token = $adminUserData->getData('user_rest_atk');
                } else {
                    $token = $this->getToken();
                }
                $sellerId = $assignProduct->getSellerId();
                try {
                    $seller = $this->_customerRepositoryInterface->getById($sellerId);
                    $organizationId = $seller->getCustomAttribute('user_rest_id') ? $seller->getCustomAttribute('user_rest_id')->getValue() : '';
                } catch(\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $organizationId = '';
                }

                $agreementId = '';
                if ($adminUser) {
                    $agreementId = $adminUserData->getData('user_rest_id_active_agreement') ?
                        $adminUserData->getData('user_rest_id_active_agreement') : '';
                } else {
                    $agreementId = $this->getAgreementId();
                }
                $qty = $assignProduct->getQty();
                $hasStock = (float)$qty > 0 ? true : false;
                $data = [];
                $data['status'] = 1;
                $data['comment'] = $comment;
                $data['seller_id'] = $assignProduct->getSellerId();
                $data['product_id'] = $assignProduct->getProductId();
                // MPMT-16: Added line below to update the dis_dispersion value to 0 when the product is approved by the JP
                $data['dis_dispersion'] = 0;
                $assignProduct->addData($data)->save();
                $this->updateStockData($productId, $qty);

                $params = [
                    'idOrganismo' => $organizationId,
                    'idProducto' => $product->getSku(),
                    'idConvenioMarco' => $agreementId,
                    'idEstadoProductoProveedorConvenio' => 1,
                    'tieneStock' => $hasStock,
                    'precio' => (float)$assignProduct->getPrice()
                ];

                $this->setAssignProductUpdate($token, $params);
            }
        }
        return $assignProduct;
    }

    /**
     * Approve Assign Product
     *
     * @param int $assignId
     */
    public function approveConfigProduct($assignId)
    {
        $assignProduct = $this->getAssignProduct($assignId);
        if ($assignProduct->getId() > 0) {
            $status = $assignProduct->getStatus();
            if ($status != 1) {
                $productId = $assignProduct->getProductId();
                $product = $this->getProduct($productId);
                $token = '';
                $adminUser = $this->getCurrentAdminUser();
                if ($adminUser) {
                    $adminUserName = $adminUser->getUserName();
                    $adminUserData = $this->_helperAdmin->getUserData($adminUserName);
                    $token = $adminUserData->getData('user_rest_atk');
                } else {
                    $token = $this->getToken();
                }
                $sellerId = $assignProduct->getSellerId();
                $seller = $this->_customerRepositoryInterface->getById($sellerId);
                $organizationId = $seller->getCustomAttribute('user_rest_id') ? $seller->getCustomAttribute('user_rest_id')->getValue() : '';
                $agreementId = '';
                if ($adminUser) {
                    $agreementId = $adminUserData->getData('user_rest_id_active_agreement') ?
                        $adminUserData->getData('user_rest_id_active_agreement') : '';
                } else {
                    $agreementId = $this->getAgreementId();
                }
                $qty = $assignProduct->getQty();
                $hasStock = (float)$qty > 0 ? true : false;

                $data = [];
                $data['status'] = 1;
                $data['comment'] = '';
                $data['seller_id'] = $assignProduct->getSellerId();
                $data['product_id'] = $assignProduct->getProductId();
                // MPMT-16: Added line below to update the dis_dispersion value to 0 when the product is approved by the JP
                $data['dis_dispersion'] = 0;
                $assignProduct->addData($data)->save();
                $model = $this->_associates->create();
                $collection = $model->getCollection()->addFieldToFilter("parent_id", $assignId);
                foreach ($collection as $key => $item) {
                    $this->updateStockData($item->getProductId(), $item->getQty());
                }

                $params = [
                    'idOrganismo' => $organizationId,
                    'idProducto' => $product->getSku(),
                    'idConvenioMarco' => $agreementId,
                    'idEstadoProductoProveedorConvenio' => 1,
                    'tieneStock' => $hasStock,
                    'precio' => (float)$assignProduct->getPrice()
                ];

                $this->setAssignProductUpdate($token, $params);
            }
        }
        return $assignProduct;
    }

    /**
     * Get Assign Products
     *
     * @param int $productId
     * @param string $sort [optional]
     * @param string $order [optional]
     *
     * @return collection object
     */
    public function getAssignProducts($productId, $sort = '', $order = 'ASC', $exclude = false)
    {
        $collection = $this->getCollection();
        $joinTable = $this->_resource
            ->getTableName('marketplace_userdata');
        $sql = 'mp.seller_id = main_table.seller_id';
        $sql .= ' and main_table.product_id = '.$productId;
        $sql .= ' and mp.is_seller = 1';
        $sql .= ' and mp.store_id = 0';
        if ($exclude) {
            $sql .= ' and main_table.qty > 0';
        }
        $sql .= ' and main_table.status = 1';
        $fields = ['seller_id', 'is_seller', 'shop_url', 'shop_title'];
        $collection->getSelect()->join($joinTable.' as mp', $sql, $fields);
        $collection->getSelect()->group('main_table.seller_id');
        $collection->addFilterToMap('seller_id', 'main_table.seller_id');
        $collection->addFilterToMap('status', 'main_table.status');
        if ($sort != '') {
            $collection->setOrder($sort, $order);
        }
        return $collection;
    }
    /**
     * Get Assign Products
     *
     * @param int $productId
     * @param string $sort [optional]
     * @param string $order [optional]
     *
     * @return collection object
     */
    public function getAssignConfiurableProducts($productId, $sort = '', $order = 'ASC', $exclude = false)
    {
        $collection = $this->getCollection();
        $websiteId = $this->_storeManager->getWebsite()->getId();
        /* PHPSTAN - eavAttribute property dynamically declared in 3rd party module*/
        $proPriceAttrId = $this->eavAttribute->getIdByCode("catalog_product", "price"); /** @phpstan-ignore-line */
        $associatedProductId = $this->_resource->getTableName('marketplace_assignproduct_associated_products');
        $catalogProductEntityDecimal = $this->_resource->getTableName('catalog_product_entity_decimal');
        $catalogInventoryStockItem = $this->_resource->getTableName('cataloginventory_stock_item');
        $collection->getSelect()->joinInner(
            $associatedProductId.' as assign',
            'main_table.product_id = assign.parent_product_id AND
             main_table.id = assign.parent_id'
        );
        $collection->getSelect()->joinLeft(
            $catalogProductEntityDecimal.' as cped',
            'assign.assign_product_id = cped.row_id and cped.store_id = 0
            AND cped.attribute_id = '.$proPriceAttrId,
            ["product_price" => "value"]
        );
        $collection->getSelect()->join(
            $catalogInventoryStockItem.' as csi',
            'assign.assign_product_id = csi.product_id',
            ["assign_qty" => "csi.qty"]
        )->where("csi.website_id = 0 OR csi.website_id = ".$websiteId);
        $joinTable = $this->_resource
            ->getTableName('marketplace_userdata');
        $sql = 'mp.seller_id = main_table.seller_id';
        $sql .= ' and main_table.product_id = '.$productId;
        $sql .= ' and mp.is_seller = 1';
        $sql .= ' and mp.store_id = 0';
        if ($exclude) {
            $sql .= ' and csi.qty > 0';
        }
        $sql .= ' and main_table.status = 1';
        $fields = ['seller_id', 'is_seller', 'shop_url', 'shop_title'];
        $collection->getSelect()->join($joinTable.' as mp', $sql, $fields);
        $collection->getSelect()->group('main_table.seller_id');
        $collection->addFilterToMap('seller_id', 'main_table.seller_id');
        $collection->addFilterToMap('status', 'main_table.status');
        $collection->addFilterToMap('qty', 'assign_qty');
        $collection->addFilterToMap('price', 'product_price');

        if ($sort != '') {
            $collection->setOrder($sort, $order);
        }

        return $collection;
    }

    /**
     * Get Minimum Price with Currency
     *
     * @param int $productId
     * @param string $type
     * @param boolean $sellerId
     * @param boolean $listPage
     * @return string
     */
    public function getMinimumPriceHtml($productId, $type = '-', $sellerId = false, $listPage = false)
    {
        $prices = [];
        $sellerIds = [];
        if ($type == "configurable") {
            $model = $this->_associates->create();
            $collection = $model->getCollection()->addFieldToFilter("parent_product_id", $productId);
            if ($sellerId) {
                return $collection;
            } else {
                foreach ($collection as $key => $item) {
                    $prices[$key] = $item->getPrice();
                }
            }
        } else {
            $totalProducts = $this->getTotalProducts($productId);
            if ($sellerId) {
                return $totalProducts;
            } else {
                foreach ($totalProducts as $key => $product) {
                    $prices[$key] = $product['price'];
                }
            }
        }
        sort($prices);
        $price = $prices[0]?? 0;
        if ($listPage) {
            return $price;
        }
        return $this->_currency->currency($price, true, false);
    }

    /**
     * Dissapprove Associate Assign Product
     *
     * @param int $assignId
     * @param int $status [optional]
     * @param int $flag [optional]
     * @param int $qty [optional]
     * @param mixed string/null $comment [optional]
     */
    public function disApproveChildProduct($associate, $status = 0, $flag = 0, $qty = 0, $comment = null)
    {
            $approvedRequiredSites = [
                InsumosConstants::WEBSITE_CODE,
                \Summa\AlimentosSetUp\Helper\Data::WEBSITE_CODE,
                AseoRenewalConstants::WEBSITE_CODE
            ];

            $assignProduct = $this->getAssignProduct($associate->getParentId());
            $productId = $associate->getProductId();
            $oldStatus = (int)$associate->getStatus();

            $data = [];
            $data['status'] = in_array($this->getWebsiteCode(), $approvedRequiredSites) ? $status : 0;
            $data['comment'] = $comment;
            $data['product_id'] = $associate->getProductId();
            $data['qty'] = $associate->getQty();
            $associate->addData($data)->save();
            if ($flag == 1) {
                $qty = $associate->getQty();
            }

            if ($oldStatus === 1) {
                $this->updateStockData($productId, $qty, 2);
            }

            $token = '';
            $adminUser = $this->getCurrentAdminUser();
            if ($adminUser) {
                $adminUserName = $adminUser->getUserName();
                $adminUserData = $this->_helperAdmin->getUserData($adminUserName);
                $token = $adminUserData->getData('user_rest_atk');
            } else {
                $token = $this->getToken();
            }
            $product = $this->getProduct($productId);
            $sellerId = $assignProduct->getSellerId();
            $seller = $this->_customerRepositoryInterface->getById($sellerId);
            $organizationId = $seller->getCustomAttribute('user_rest_id') ? $seller->getCustomAttribute('user_rest_id')->getValue() : '';
            $agreementId = '';
            if ($adminUser) {
                $agreementId = $adminUserData->getData('user_rest_id_active_agreement') ?
                    $adminUserData->getData('user_rest_id_active_agreement') : '';
            } else {
                $agreementId = $this->getAgreementId();
            }
            $hasStock = (float)$data['qty'] > 0 ? true : false;
            $params = [
                'idOrganismo' => $organizationId,
                'idProducto' => $product->getSku(),
                'idConvenioMarco' => $agreementId,
                'idEstadoProductoProveedorConvenio' => $data['status'],
                'tieneStock' => $hasStock,
                'precio' => (float)$associate->getPrice()
            ];

            $response = $this->setAssignProductUpdate($token, $params);

            $this->validateServiceUpdate($response);

        return $associate;
    }

    /**
     * Dissapprove Assign Product
     *
     * @param int $assignId
     * @param int $status [optional]
     * @param int $flag [optional]
     * @param int $qty [optional]
     * @param mixed string/null $comment [optional]
     */
    public function disApproveProduct($assignId, $status = 0, $flag = 0, $qty = 0, $comment = null)
    {
        $assignProduct = $this->getAssignProduct($assignId);
        if ($assignProduct->getId() > 0) {
            if ($status == 1) {

                $productId = $assignProduct->getProductId();
                $oldStatus = (int)$assignProduct->getStatus();

                $data = [];
                $data['status'] = 0;
                $data['comment'] = $comment;
                $data['seller_id'] = $assignProduct->getSellerId();
                $data['product_id'] = $assignProduct->getProductId();
                $data['qty'] = $assignProduct->getQty();
                $assignProduct->addData($data)->save();
                if ($flag == 1) {
                    $qty = $assignProduct->getQty();
                }

                if ($oldStatus === 1) {
                    $this->updateStockData($productId, $qty, 2);
                }

                $token = '';
                $adminUser = $this->getCurrentAdminUser();
                if ($adminUser) {
                    $adminUserName = $adminUser->getUserName();
                    $adminUserData = $this->_helperAdmin->getUserData($adminUserName);
                    $token = $adminUserData->getData('user_rest_atk');
                } else {
                    $token = $this->getToken();
                }
                $product = $this->getProduct($productId);
                $sellerId = $assignProduct->getSellerId();
                try {
                    $seller = $this->_customerRepositoryInterface->getById($sellerId);
                    $organizationId = $seller->getCustomAttribute('user_rest_id') ? $seller->getCustomAttribute('user_rest_id')->getValue() : '';
                } catch(\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $organizationId = '';
                }

                $agreementId = '';
                if ($adminUser) {
                    $agreementId = $adminUserData->getData('user_rest_id_active_agreement') ?
                        $adminUserData->getData('user_rest_id_active_agreement') : '';
                } else {
                    $agreementId = $this->getAgreementId();
                }
                $hasStock = (float)$data['qty'] > 0 ? true : false;
                $params = [
                    'idOrganismo' => $organizationId,
                    'idProducto' => $product->getSku(),
                    'idConvenioMarco' => $agreementId,
                    'idEstadoProductoProveedorConvenio' => $data['status'],
                    'tieneStock' => $hasStock,
                    'precio' => (float)$assignProduct->getPrice()
                ];


                $response = $this->setAssignProductUpdate($token, $params);
                $this->validateServiceUpdate($response);

            }
        }
        return $assignProduct;
    }
    /**
     * Dissapprove Assign Product
     *
     * @param int $assignId
     * @param int $status [optional]
     */
    public function untradedProduct($assignId)
    {
        $item = $this->_itemsCollection->create();
        $item->addFieldToFilter('product_id', $assignId);
        foreach ($item as $assignProduct) {
            if ($assignProduct->getData() > 0) {
                    $data = [];
                    $data['status'] = 5;
                    $assignProduct->addData($data)->save();
            }
        }
        return $assignProduct ?? null;
    }

    public function validateServiceUpdate($response)
    {
        if (isset($response['success']) && $response['success'] == 'NOK' &&
            isset($response['errores'])) {
            foreach ($response['errores'] as $error) {
                $this->_messageManager->addError(__("error with Mercadopublico: %1", $error['descripcion']));
            }
        } elseif (!isset($response['success']) && isset($response['error']) &&
            isset($response['message'])) {
            $message = $response['error'] . '. ' . $response['message'];
            $this->_messageManager->addError(__($message));
        }
    }

    /**
     * Dissapprove Assign Product
     *
     * @param array $data
     * @param int $status [optional]
     * @param bool $byAdmin [optional]
     * @param mixed string/null $comment [optional]
     */
    public function disApproveConfigProduct($data, $status = 0, $byAdmin = false, $comment = null)
    {
        //esta funcion, segun veo, no se está utilizando mas en el flujo de las ofertas configurables
        if (!$byAdmin) {
            $assignId = $data['assign_id'];
        } else {
            $assignId = $data;
        }
        $assignProduct = $this->getAssignProduct($assignId);
        if ($assignProduct->getId() > 0) {
            if ($status == 1) {
                $parentProductId = $assignProduct->getProductId();
                $oldStatus = (int)$assignProduct->getStatus();
                $prodata = [];
                $prodata['status'] = 0;
                $prodata['comment'] = $comment;
                $prodata['seller_id'] = $assignProduct->getSellerId();
                $prodata['product_id'] = $assignProduct->getProductId();
                $assignProduct->addData($prodata)->save();

                //Parent Update MP;
                $token = '';
                $adminUser = $this->getCurrentAdminUser();
                if ($adminUser) {
                    $adminUserName = $adminUser->getUserName();
                    $adminUserData = $this->_helperAdmin->getUserData($adminUserName);
                    $token = $adminUserData->getData('user_rest_atk');
                } else {
                    $token = $this->getToken();
                }
                $product = $this->getProduct($parentProductId);
                $sellerId = $assignProduct->getSellerId();
                $seller = $this->_customerRepositoryInterface->getById($sellerId);
                $organizationId = $seller->getCustomAttribute('user_rest_id') ? $seller->getCustomAttribute('user_rest_id')->getValue() : '';
                $agreementId = '';
                if ($adminUser) {
                    $agreementId = $adminUserData->getData('user_rest_id_active_agreement') ?
                        $adminUserData->getData('user_rest_id_active_agreement') : '';
                } else {
                    $agreementId = $this->getAgreementId();
                }
                $hasStock = (float)$data['qty'] > 0 ? true : false;
                $params = [
                    'idOrganismo' => $organizationId,
                    'idProducto' => $product->getSku(),
                    'idConvenioMarco' => $agreementId,
                    'idEstadoProductoProveedorConvenio' => $data['status'],
                    'tieneStock' => $hasStock,
                    'precio' => (float)$assignProduct->getPrice()
                ];
                $response = $this->setAssignProductUpdate($token, $params);
                $this->validateServiceUpdate($response);
                // END Parent Update MP;

                if ($oldStatus === 1) {
                    if ($byAdmin) {
                        $model = $this->_associates->create();
                        $collection = $model->getCollection()->addFieldToFilter("parent_id", $assignId);
                        //capaz se puede hacer un Join para tener el SKU en el resultado de la collection,
                        // en lugar del load de cada Producto.
                        foreach ($collection as $key => $item) {
                            $this->updateStockData($item->getProductId(), $item->getQty(), 2);
                            //child Update
                            $product = $this->getProduct($item->getProductId()); //<- esto se puede sacar si viene desde la collection.
                            $params['idProducto']=$product->getSku();
                            $params['tieneStock']= (float)$item->getQty() > 0 ? true : false;;
                            $params['precio']=$item->getQty();

                            $response = $this->setAssignProductUpdate($token, $params);
                            $this->validateServiceUpdate($response);
                        }
                    } else {
                        foreach ($data['associates'] as $productId => $info) {
                            if ($info['manage_stock']) {
                                $this->updateStockData($productId, $info['qty'], 2);
                            }
                        }
                    }
                }
            }
        }
        return $assignProduct;
    }

    /**
     * Dissapprove Associate Assign Product
     *
     * @param int $assignId
     * @param int $status [optional]
     * @param int $flag [optional]
     * @param int $qty [optional]
     */
    public function pendingChildProduct($associate, $status = 0, $flag = 0, $qty = 0, $disabledStatus, $comment)
    {
        if ($status == 1) {
            $assignProduct = $this->getAssignProduct($associate->getParentId());
            $productId = $associate->getProductId();
            $product = $this->getProduct($productId);
            $oldStatus = (int)$associate->getStatus();
            $token = '';
            $adminUser = $this->getCurrentAdminUser();
            if ($adminUser) {
                $adminUserName = $adminUser->getUserName();
                $adminUserData = $this->_helperAdmin->getUserData($adminUserName);
                $token = $adminUserData->getData('user_rest_atk');
            } else {
                $token = $this->getToken();
            }
            $sellerId = $assignProduct->getSellerId();
            $seller = $this->_customerRepositoryInterface->getById($sellerId);
            $organizationId = $seller->getCustomAttribute('user_rest_id') ? $seller->getCustomAttribute('user_rest_id')->getValue() : '';
            $agreementId = '';
            if ($adminUser) {
                $agreementId = $adminUserData->getData('user_rest_id_active_agreement') ?
                    $adminUserData->getData('user_rest_id_active_agreement') : '';
            } else {
                $agreementId = $this->getAgreementId();
            }
            $data = [];
            $data['status'] = $disabledStatus;
            $data['comment'] = $comment;
            $data['product_id'] = $associate->getProductId();
            $data['qty'] = $associate->getQty();
            $associate->addData($data)->save();
            if ($flag == 1) {
                $qty = $associate->getQty();
            }

            if ($oldStatus === 1) {
                $this->updateStockData($productId, $qty, 2);
            }

            $hasStock = (float)$data['qty'] > 0 ? true : false;
            $params = [
                'idOrganismo' => $organizationId,
                'idProducto' => $product->getSku(),
                'idConvenioMarco' => $agreementId,
                'idEstadoProductoProveedorConvenio' => $disabledStatus,
                'tieneStock' => $hasStock,
                'precio' => (float)$associate->getPrice()
            ];
            //validar que se haga el update ok en MP
            // si se realiza el proceso desde Admin, no tenemos TOKEN ni Id de convenio OJO.
            $this->setAssignProductUpdate($token, $params);
        }
        return $associate;
    }

    /**
     * Dissapprove Assign Product
     *
     * @param int $assignId
     * @param int $status [optional]
     * @param int $flag [optional]
     * @param int $qty [optional]
     */
    public function pendingProduct($assignId, $status = 0, $flag = 0, $qty = 0, $disabledStatus, $comment)
    {
        $assignProduct = $this->getAssignProduct($assignId);
        if ($assignProduct->getId() > 0) {
            if ($status == 1) {

                $productId = $assignProduct->getProductId();
                $product = $this->getProduct($productId);
                $oldStatus = (int)$assignProduct->getStatus();
                $token = '';
                $adminUser = $this->getCurrentAdminUser();
                if ($adminUser) {
                    $adminUserName = $adminUser->getUserName();
                    $adminUserData = $this->_helperAdmin->getUserData($adminUserName);
                    $token = $adminUserData->getData('user_rest_atk');
                } else {
                    $token = $this->getToken();
                }
                $sellerId = $assignProduct->getSellerId();
                $seller = $this->_customerRepositoryInterface->getById($sellerId);
                $organizationId = $seller->getCustomAttribute('user_rest_id') ? $seller->getCustomAttribute('user_rest_id')->getValue() : '';
                $agreementId = '';
                if ($adminUser) {
                    $agreementId = $adminUserData->getData('user_rest_id_active_agreement') ?
                        $adminUserData->getData('user_rest_id_active_agreement') : '';
                } else {
                    $agreementId = $this->getAgreementId();
                }
                $data = [];
                $data['status'] = $disabledStatus;
                $data['comment'] = $comment;
                $data['seller_id'] = $assignProduct->getSellerId();
                $data['product_id'] = $assignProduct->getProductId();
                $data['qty'] = $assignProduct->getQty();
                $assignProduct->addData($data)->save();
                if ($flag == 1) {
                    $qty = $assignProduct->getQty();
                }

                if ($oldStatus === 1) {
                    $this->updateStockData($productId, $qty, 2);
                }

                $hasStock = (float)$data['qty'] > 0 ? true : false;
                $params = [
                    'idOrganismo' => $organizationId,
                    'idProducto' => $product->getSku(),
                    'idConvenioMarco' => $agreementId,
                    'idEstadoProductoProveedorConvenio' => $disabledStatus,
                    'tieneStock' => $hasStock,
                    'precio' => (float)$assignProduct->getPrice()
                ];
                $this->setAssignProductUpdate($token, $params);
            }
        }
        return $assignProduct;
    }

    /**
     * Dissapprove Assign Product
     *
     * @param array $data
     * @param int $status [optional]
     * @param bool $byAdmin [optional]
     */
    public function pendingConfigProduct($data, $status = 0, $byAdmin = false, $requestedStatus, $comment)
    {
        if (!$byAdmin) {
            $assignId = $data['assign_id'];
        } else {
            $assignId = $data;
        }
        $assignProduct = $this->getAssignProduct($assignId);
        if ($assignProduct->getId() > 0) {
            if ($status == 1) {
                $parentProductId = $assignProduct->getProductId();
                $product = $this->getProduct($parentProductId);
                $oldStatus = (int)$assignProduct->getStatus();
                $token = '';
                $adminUser = $this->getCurrentAdminUser();
                if ($adminUser) {
                    $adminUserName = $adminUser->getUserName();
                    $adminUserData = $this->_helperAdmin->getUserData($adminUserName);
                    $token = $adminUserData->getData('user_rest_atk');
                } else {
                    $token = $this->getToken();
                }
                $sellerId = $assignProduct->getSellerId();
                try {
                    $seller = $this->_customerRepositoryInterface->getById($sellerId);
                    $organizationId = $seller->getCustomAttribute('user_rest_id') ? $seller->getCustomAttribute('user_rest_id')->getValue() : '';
                } catch(\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $organizationId = '';
                }

                $agreementId = '';
                if ($adminUser) {
                    $agreementId = $adminUserData->getData('user_rest_id_active_agreement') ?
                        $adminUserData->getData('user_rest_id_active_agreement') : '';
                } else {
                    $agreementId = $this->getAgreementId();
                }
                $prodata = [];
                $prodata['status'] = $requestedStatus;
                $prodata['comment'] = $comment;
                $prodata['seller_id'] = $assignProduct->getSellerId();
                $prodata['product_id'] = $assignProduct->getProductId();
                $prodata['qty'] = $assignProduct->getQty();
                $assignProduct->addData($prodata)->save();

                if ($oldStatus === 1) {
                    if ($byAdmin) {
                        $model = $this->_associates->create();
                        $collection = $model->getCollection()->addFieldToFilter("parent_id", $assignId);
                        foreach ($collection as $key => $item) {
                            $this->updateStockData($item->getProductId(), $item->getQty(), 2);
                        }
                    } else {
                        foreach ($data['associates'] as $productId => $info) {
                            if ($info['manage_stock']) {
                                $this->updateStockData($productId, $info['qty'], 2);
                            }
                        }
                    }
                }
                $hasStock = (float)$prodata['qty'] > 0 ? true : false;
                $params = [
                    'idOrganismo' => $organizationId,
                    'idProducto' => $product->getSku(),
                    'idConvenioMarco' => $agreementId,
                    'idEstadoProductoProveedorConvenio' => $requestedStatus,
                    'tieneStock' => $hasStock,
                    'precio' => (float)$assignProduct->getPrice()
                ];
                $this->setAssignProductUpdate($token, $params);
            }
        }
        return $assignProduct;
    }

    /**
     * Get Assign Products
     *
     * @param int $productId
     * @param string $sort [optional]
     * @param string $order [optional]
     *
     * @return collection object
     */
    public function getAssingProductFile($productId, $sellerId)
    {
        $file = '';

        if ($productId > 0 && $sellerId > 0) {
            $areaCode = $this->_state->getAreaCode();
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $collection = $this->assignProductFileCollection->create();
            $collection->addFieldToFilter('product_id', $productId);
            $collection->addFieldToFilter('seller_id', $sellerId);
            if ($areaCode != 'adminhtml' && $websiteId != 1) {
                $collection->addFieldToFilter('website_id', (int)$websiteId);
            }
            $collection->addFieldToFilter('status', '1');

            foreach ($collection as $item) {
                $file = $item->getFile();
            }
        }

        return $file != '' ? $this->getMediaBaseUrl($file) : $file;
    }

    /**
     * Get Aseo Product File
     *
     * @param int $productId
     * @param int $sellerId
     *
     * @return string
     */
    public function getAseoProductFile($productId, $sellerId)
    {
        $file = '';

        if ($productId > 0 && $sellerId > 0) {
            $areaCode = $this->_state->getAreaCode();
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $collection = $this->assignProductFileCollection->create();
            $collection->addFieldToFilter('product_id', $productId);
            $collection->addFieldToFilter('seller_id', $sellerId);
            if ($areaCode != 'adminhtml') {
                $collection->addFieldToFilter('website_id', (int)$websiteId);
            }
            $collection->addFieldToFilter('status', '1');

            foreach ($collection as $item) {
                $file = $item->getFileAseo();
            }
        }

        return $file != '' ? $this->getMediaBaseUrl($file) : $file;
    }

    /**
     * Get Anexo Product File
     * @param $productId
     * @param $sellerId
     * @return string
     * @throws NoSuchEntityException
     */
    public function getAnexoProductFile($productId, $sellerId)
    {
        $file = '';

        if ($productId > 0 && $sellerId > 0) {
            $areaCode = $this->_state->getAreaCode();
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $collection = $this->assignProductFileCollection->create();
            $collection->addFieldToFilter('product_id', $productId);
            $collection->addFieldToFilter('seller_id', $sellerId);
            if ($areaCode != 'adminhtml') {
                $collection->addFieldToFilter('website_id', (int)$websiteId);
                $collection->addFieldToFilter('status', '1');
            }

            foreach ($collection as $item) {
                $file = $item->getFileAnexo();
            }
        }

        return $file != '' ? $this->getMediaBaseUrl($file) : $file;
    }

    public function getAuthorizedProductFile($productId, $sellerId)
    {
        $file = '';

        if ($productId > 0 && $sellerId > 0) {
            $areaCode = $this->_state->getAreaCode();
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $collection = $this->assignProductFileCollection->create();
            $collection->addFieldToFilter('product_id', $productId);
            $collection->addFieldToFilter('seller_id', $sellerId);
            if ($areaCode != 'adminhtml') {
                $collection->addFieldToFilter('website_id', (int)$websiteId);
                $collection->addFieldToFilter('status', '1');
            }

            foreach ($collection as $item) {
                $file = $item->getAuthorizedLetter();
            }
        }

        return $file != '' ? $this->getMediaBaseUrl($file) : $file;
    }

    /**
     * Get file url media
     *
     * @param string $file
     * @return string
     */
    public function getMediaBaseUrl($file)
    {
        $urlFile = $this->_storeManager->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $urlFile . 'chilecompra/files/assignproducts'. $file;
    }

    /**
     * Retrieve seller ID
     *
     * @return int
     */
    public function getSellerIdBySubAccount()
    {
        return $this->helperWk->getSubAccountSellerId();
    }

    /**
     * get formatted price
     *
     * @param float $price
     * @return string
     */
    public function currencyFormat($price)
    {
        return strip_tags($this->_priceCurrency->format($price));
    }

    /**
     * Set assign product from REST service
     *
     * @param string $token
     * @param array $bodyParams
     * @return Zend_Http_Response
     */
    public function setAssignProduct($token, $bodyParams)
    {
        $response = null;
        $mageStatus = $bodyParams['idEstadoProductoProveedorConvenio'];
        $dccpStatus = $this->_assignStatusCollectionFactory->create()
            ->addFieldToSelect('dccp_status')
            ->addFieldToFilter('mage_status', $mageStatus)
            ->getFirstItem()->getDccpStatus();
        $bodyParams['idEstadoProductoProveedorConvenio'] = $dccpStatus;

        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_assign_product/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');

            if (!empty($endpoint) && !empty($token) &&
                is_array($bodyParams) && count($bodyParams)) {
                $httpHeaders = [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . trim($token)
                ];

                $data = $this->jsonHelper->jsonEncode($bodyParams);
                $this->logger->info('setAssignProduct REQUEST: ' . $data);

                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                    'timeout' => 30]
                );
                $client->setHeaders($httpHeaders);
                $client->setRawData($data);
                $response = $client->request(\Zend_Http_Client::POST)->getBody();

                $this->logger->info('setAssignProduct RESPONSE: ' . $response);
                $response = $this->jsonHelper->jsonDecode($response);
            } else {
                throw new LocalizedException(__('Define endpoint and token assign product webservice'));
            }
        } catch(LocalizedException $e) {
            $this->logger->critical('setAssignProduct Error message: ' . $e->getMessage());
        } catch(\Exception $e) {
            $this->logger->critical('setAssignProduct Error message: ' . $e->getMessage());
        }

        return $response;
    }

    /**
     * Update assign product from REST service
     *
     * @param string $token
     * @param array $bodyParams
     * @return Zend_Http_Response
     */
    public function setAssignProductUpdate($token, $bodyParams)
    {
        $response = null;

        $mageStatus = $bodyParams['idEstadoProductoProveedorConvenio'];
        $dccpStatus = $this->_assignStatusCollectionFactory->create()
            ->addFieldToSelect('dccp_status')
            ->addFieldToFilter('mage_status', $mageStatus)
            ->getFirstItem()->getDccpStatus();
        $bodyParams['idEstadoProductoProveedorConvenio'] = $dccpStatus;

        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_assign_product_update/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');

            if (!empty($endpoint) && !empty($token) &&
                is_array($bodyParams) && count($bodyParams)) {
                $httpHeaders = [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . trim($token)
                ];

                $data = $this->jsonHelper->jsonEncode($bodyParams);
                $this->logger->info('setAssignProductUpdate REQUEST: ' . $data);

                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                    'timeout' => 30]
                );
                $client->setHeaders($httpHeaders);
                $client->setRawData($data);
                $client->setMethod(\Zend_Http_Client::PUT);
                $response = $client->request(\Zend_Http_Client::PUT)->getBody();

                $this->logger->info('setAssignProductUpdate RESPONSE: ' . $response);
                $response = $this->jsonHelper->jsonDecode($response);
            } else {
                throw new LocalizedException(__('Define endpoint and token assign product update webservice'));
            }
        } catch(LocalizedException $e) {
            $this->logger->critical('setAssignProductUpdate Error message: ' . $e->getMessage());
            $response['error'] = "setAssignProductUpdate Error";
            $response['message'] =  'message: ' . $e->getMessage();
        } catch(\Exception $e) {
            $this->logger->critical('setAssignProductUpdate Error message: ' . $e->getMessage());
            $response['error'] = "setAssignProductUpdate Error";
            $response['message'] =  'message: ' . $e->getMessage();
        }

        return $response;
    }

    /**
     * Update assign products from REST service
     *
     * @param string $token
     * @param array $bodyParams
     * @return Zend_Http_Response
     */
    public function setAssignProductsUpdate($token, $bodyParams)
    {
        $response = null;

        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_assign_products_update/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');

            if (!empty($endpoint) && !empty($token) &&
                is_array($bodyParams) && count($bodyParams)) {
                $httpHeaders = [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . trim($token)
                ];

                $data = $this->jsonHelper->jsonEncode($bodyParams);
                $this->logger->info('setAssignProductsUpdate REQUEST: ' . $data);

                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                    'timeout' => 30]
                );
                $client->setHeaders($httpHeaders);
                $client->setRawData($data);
                $client->setMethod(\Zend_Http_Client::POST);
                $response = $client->request(\Zend_Http_Client::POST)->getBody();

                $this->logger->info('setAssignProductsUpdate RESPONSE: ' . $response);
                $response = $this->jsonHelper->jsonDecode($response);
            } else {
                throw new LocalizedException(__('Define endpoint and token assign products update webservice'));
            }
        } catch(LocalizedException $e) {
            $this->logger->critical('setAssignProductsUpdate Error message: ' . $e->getMessage());
        } catch(\Exception $e) {
            $this->logger->critical('setAssignProductsUpdate Error message: ' . $e->getMessage());
        }

        return $response;
    }

    /**
     * Get Store code
     *
     * @return string
     */
    public function getStoreCode(){
        return $this->_storeManager->getStore()->getCode();
    }

    public function getWebsiteCode()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }

    /**
     * @param $productId
     * @return \Magento\Framework\DataObject
     */
    public function getFirsttAssingItemByProductId($productId)
    {
        $collection = $this->_itemsCollection
            ->create()
            ->addFieldToFilter('product_id', $productId);

        return $collection->getFirstItem();
    }

    public function getMaxBasePrice($productId){
        //this function doesn't have the code to work in ASEO 'is_mayorista' and doesn't have the is_configurable condition with the products options
        $someId = $this->getFirsttAssingItemByProductId($productId)->getId();

        $productCollection = $this->_productCollection->create()
            ->addAttributeToFilter('entity_id', ['eq' => $productId]);
        if ($this->getStoreCode() == \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE) {
            $productCollection->addAttributeToSelect(['max_base_price', 'max_shipping_price']);
        }else if ($this->getStoreCode() == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE){
            $productCollection->addAttributeToSelect(['max_base_price']);
        }

        $product = $productCollection->getFirstItem();
        /* Validation if it is the first offer */
        if (!$someId) {
            //this is the First Item - the Max base price is for the product
            return $this->currencyFormat($product->getMaxBasePrice());
        }

        $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
        $currentDate = $this->connection->quote($this->timezone->date()->format('Y-m-d'));

        if ($productId > 0) {
            $currencySymbol =  $this->getCurrencySymbol();
            $tableConfigurable = $this->_resource->getTableName('marketplace_assignproduct_associated_products');
            $tableSimple = $this->_resource->getTableName('marketplace_assignproduct_items');
            $tableUserData = $this->_resource->getTableName('marketplace_userdata');
            $tableOffer = $this->_resource->getTableName('dccp_offer_product');

            $sqlOpts = [
                'price' => 'MIN'
            ];
            $storeCode = $this->getStoreCode();

            /* Emergency prices average */
            if ($this->getStoreCode() == \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE) {
                $sqlOpts['price'] = 'AVG';
            }

            if ($this->getStoreCode() == \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE
                || $this->getStoreCode() == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) {
                $sql = 'SELECT '.$sqlOpts['price'].'(s.base_price) AS smallestPrice FROM ' . $tableSimple . ' s
                INNER JOIN ' . $tableUserData . ' u ON s.seller_id = u.seller_id AND u.is_seller = 1
                LEFT JOIN ' . $tableOffer . ' o ON s.id = o.assign_id AND o.status = 3 AND o.website_id = ' . $websiteId .
                    ' AND o.special_price < o.base_price WHERE s.product_id = ' . $productId .
                    ' AND s.status = 1 AND s.qty > 0 AND s.dis_dispersion = 0';
            } else {
                $sql = 'SELECT '.$sqlOpts['price'].'(IFNULL(o.base_price, s.price)) AS smallestPrice FROM ' . $tableSimple . ' s
                INNER JOIN ' . $tableUserData . ' u ON s.seller_id = u.seller_id AND u.is_seller = 1
                LEFT JOIN ' . $tableOffer . ' o ON s.id = o.assign_id AND o.status = 3 AND o.website_id = ' . $websiteId .
                    ' AND o.special_price < o.base_price WHERE s.product_id = ' . $productId .
                    ' AND s.status = 1 AND s.qty > 0 AND s.dis_dispersion = 0';
            }

            $minPrice = (double)$this->connection->fetchOne($sql);

            if ($this->getStoreCode() == \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE && !$minPrice) {
                return $this->currencyFormat($product->getMaxBasePrice());
            }

            if ($minPrice > 0) {
                return $this->currencyFormat($minPrice);
            }
        }

        return null;
    }
    public function getMaxShippingPrice($producId){

        $product = $this->_productCollection->create()
            ->addAttributeToFilter('entity_id', ['eq' => $producId])
            ->addAttributeToSelect(['max_base_price', 'max_shipping_price'])
            ->getFirstItem();

        return $this->currencyFormat($product->getMaxShippingPrice());
    }

    /**
     * Get Freight Price
     *
     * @param int $itemId
     * @return float
     */
    public function getFreightPrice($itemId)
    {
        $price = 0;
        $mpQuoteItem = $this->getQuoteCollection()->addFieldToFilter('item_id', ['eq' => $itemId])->getFirstItem();

        if($mpQuoteItem){
            $assignId = $mpQuoteItem->getAssignId();

            $item = $this->_itemsCollection
                ->create()
                ->addFieldToFilter("id", $assignId)
                ->getFirstItem();

            $price = $item ? $item->getShippingPrice() : 0;
        }

        return $price;
    }

    /**
     * Get Current Admin User
     *
     * @return \Magento\User\Model\User|null
     */
    public function getCurrentAdminUser()
    {
        return $this->_authSession->getUser();
    }

    /**
     * Return linked products base price for the current product
     * @param $product
     * @return int[]
     */
    public function getLinkedProductBasePrice($product)
    {
        $linkedProductBasePrice = [
            'offered' => 0,
            'base_price' => 0,
            'value' => 0
        ];
        $linkedProducts = $product->getLinkedProducts();
        if (!strlen($linkedProducts)){
            return $linkedProductBasePrice;
        }
        $linkedProductsArray = explode(',', $linkedProducts);
        $sellerId = $this->getCustomerId();

        foreach ($linkedProductsArray as $linkedProductSku){
            if ($linkedProductBasePrice['value'] > 0) {
                break;
            }
            $linkedProduct = $this->productRepository->get($linkedProductSku);
            $linkedProductId = $linkedProduct->getId();

            $itemsCollection = $this->_itemsCollection->create()
                ->addFieldToFilter('product_id', ['eq' => $linkedProductId])
                ->addFieldToFilter('qty', ['gt' => 0])
                ->addFieldToFilter('condition', ['eq' => 1])
                ->addFieldToFilter('seller_id', ['eq' => $sellerId])
                ->addFieldToFilter('status', ['eq' => 1])
                ->setPageSize(1);
            $itemCollectionSize = $itemsCollection->getSize();
            if ($itemCollectionSize) {
                $linkedProduct = $itemsCollection->getFirstItem();
                $linkedProductBasePrice['offered'] = 1;
                $basePrice = $linkedProduct->getBasePrice();
                $linkedProductBasePrice['base_price'] = $this->currencyFormat($basePrice);
                $linkedProductBasePrice['value'] = (int) $this->_priceCurrency->convert($basePrice);
            }
        }
        return $linkedProductBasePrice;
    }

    /**
     * Sets configurable assign product status based on childs status
     */
    public function setConfigStatus($assignProduct)
    {
        $tableConfigurable = $this->_resource->getTableName('marketplace_assignproduct_associated_products');
        $sql = "SELECT
            SUM(CASE WHEN a.status = 0 THEN 1 ELSE 0 END) AS disapproved,
            SUM(CASE WHEN a.status = 1 THEN 1 ELSE 0 END) AS approved,
            SUM(CASE WHEN a.status = 2 THEN 1 ELSE 0 END) AS pending,
            SUM(CASE WHEN a.status > 2 THEN 1 ELSE 0 END) AS others
            FROM {$tableConfigurable} a WHERE parent_id = {$assignProduct->getId()}";

        $data = $this->connection->fetchRow($sql);
        $status = $data['others'] || $data['pending'] || ($data['approved'] && $data['disapproved'])
            ? 2
            : ($data['disapproved']
                ? 0
                : ($data['approved']
                    ? 1
                    : 2));

        $assignProduct->setStatus($status)->save();
    }

    public function getToken()
    {
        $customerId = $this->_customerSession->getCustomer()->getId();
        $customer = $this->_customerRepositoryInterface->getById($customerId);
        $token = '';
        if ($customer) {
            $token = $customer->getCustomAttribute('user_rest_atk') ?
                $customer->getCustomAttribute('user_rest_atk')->getValue() : '';
        }
        return $token;
    }

    public function getAgreementId()
    {
        $customerId = $this->_customerSession->getCustomer()->getId();
        $customer = $this->_customerRepositoryInterface->getById($customerId);
        $agreementId = '';
        if ($customer) {
            $agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
                $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : '';
        }
        return $agreementId;
    }

    public function checkProduct($isAdd = 0, $validateChild = false) {
        $result = parent::checkProduct($isAdd);

        if($result['error'] == 1 || !$validateChild) {
            return $result;
        }

        $assignId = (int) $this->_request->getParam('id');

        if ($isAdd == 1) {
            $productId = $assignId;
        } else {
            $assignData = $this->getAssignDataByAssignId($assignId);
            $productId = $assignData->getProductId();
        }

        $product = $this->getProduct($productId);
        $productType = $product->getTypeId();

        if($productType == 'configurable') {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $helperOffer = $objectManager->create('Formax\Offer\Helper\Data');

            $associatedData = $helperOffer->getAssociatesData($assignId);

            if(!$associatedData || empty($associatedData)) {
                $result['error'] = 1;
                $result['msg'] = __("This product doesn't have approved offers or offers are out of stock.");
            }
        }

        return $result;
    }


    /**
     * Get seller from quote
     *
     * @param array
     */
    public function getSellersCountFromQuote($id)
    {
        $quoteId = (int) $id;
        $collection = $this->infoFactory->create();
        $collection->addFieldToFilter('quote_id', $quoteId)
            ->addFieldToFilter('status', ['gteq' => 2])
            ->getSelect()->join(
                ['cv' => $this->resourceConnection->getTableName('requestforquote_quote_conversation')],
                'main_table.entity_id = cv.seller_quote_id AND cv.edited = 1',
                []
            );
        return $collection->getSize();
    }

    /**
     * @param int $sellerId
     * @return int
     */
    public function getSellerStatus($sellerId)
    {
        if (!isset($this->sellerStatuses[$sellerId])) {
            $seller = $this->_customerRepositoryInterface->getById($sellerId);
            $sellerStatus = $seller->getCustomAttribute('wkv_dccp_state_details') ?
                $seller->getCustomAttribute('wkv_dccp_state_details')->getValue() : '';
            $this->sellerStatuses[$sellerId] = $sellerStatus;
        }

        return $this->sellerStatuses[$sellerId];
    }

    public function isTemporalPriceAdjustmentActive($product)
    {
        $productSku = $product->getSku();
        $productId = $product->getId();

        // \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceAdjustment\Collection
        $temporaryPriceAdjustmentCollection = $this->_temporaryPriceAdjustmentCollection->create();

        // Check if the product has a temporary price adjustment running
        $isChild = (bool) $this->_configurableProductType->getParentIdsByChild($productId);
        if ($isChild) {
            $temporaryPriceAdjustmentCollection->addFieldToFilter('sku_assign', ['eq' => $productSku]);
        } else {
            $temporaryPriceAdjustmentCollection->addFieldToFilter('sku', ['eq' => $productSku]);
        }
        $temporaryPriceAdjustmentCollection->addFieldToFilter('status', ['eq' => \Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_RUNNIG]);

        return $temporaryPriceAdjustmentCollection->getSize() > 0;
    }

    /**
     * @param $regionId
     * @return string
     */
    public function getRegionName($regionId)
    {
        $regionCollection = $this->regionFactory->loadByCode('CL')->getRegions();
        $regionName = '';
        foreach($regionCollection as $region){
            if ($region->getRegionId() == $regionId) {
                $regionName = $region->getName();
                break;
            }
        }

        return $regionName;
    }

    /**
     * @param $selectRegions
     * @return void
     */
    public function formatRegions($selectRegions)
    {
        $regionCollection = $this->regionFactory->loadByCode('CL')->getRegions();
        foreach($regionCollection as $region){
            foreach ($selectRegions as $key => $selectRegion) {
                if ($selectRegion['label'] == $region->getRegionId()) {
                    $selectRegions[$key]['label'] = $region->getName();
                }
            }
        }

        return $selectRegions;
    }

    /**
     * @param $productId
     * @param $sellerId
     * @return \Magento\Framework\DataObject
     */
    public function getAssignItem($productId, $sellerId)
    {
        $collection = $this->_itemsCollection
            ->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('product_id', $productId);

        $assignProduct = $collection->getFirstItem();
        return $assignProduct;
    }

    /**
     * @param $productId
     * @param $sellerId
     * @return int
     */
    public function getAssemblyPriceByInfo($productId, $sellerId){
        $product = $this->getProduct($productId);
        $assemblyPrice = 0;

        if ($product->getTypeId() == 'quote'){
            $assemblyPrice = $this->getAssemblyPriceByRequestedSku($product->getSku(),$sellerId);
        }else{
            $assignProduct = $this->getAssignItem($productId, $sellerId);
            $assemblyPrice = $assignProduct->getAssemblyPrice();
        }
        return $assemblyPrice;
    }

    /**
     * @param $sku
     * @param $sellerId
     * @return int
     */
    public function getAssemblyPriceByRequestedSku($sku, $sellerId)
    {
        $assemblyPrice = 0;
        $originalSku = explode("-",$sku)[1];
        $originalId = $this->productRepository->get($originalSku)->getId();
        $assignProduct =  $this->getAssignItem($originalId, $sellerId);
        $assemblyPrice = $assignProduct->getAssemblyPrice();
        return $assemblyPrice;
    }

    /**
     * @return \Magento\Checkout\Model\Cart
     */
    public function getCart()
    {
        $cartModel = $this->_cart;
        return $cartModel;
    }

    /**
     * Get Searched Attribute Set
     *
     * @return string
     */
    public function getAttributeSetId()
    {
        $attributeSetId = $this->_request->getParam('atribute_set_id');
        $attributeSetId = strip_tags(trim($attributeSetId));

        return $attributeSetId;
    }

    /**
     * Get Searched Marca
     *
     * @return string
     */
    public function getMarcaId()
    {
        $marca = $this->_request->getParam('marca');
        $marca = strip_tags(trim($marca));

        return $marca;
    }

    /**
     * Get Searched PROD ID
     *
     * @return string
     */
    public function getProdId()
    {
        $prodId = $this->_request->getParam('prod_id');
        $prodId = strip_tags(trim($prodId));

        return $prodId;
    }

    /**
     * Dissapprove Assign Product
     *
     * @param int $assignId
     * @param int $status [optional]
     */
    public function disApproveUntradedProduct($assignId)
    {
        $item = $this->_itemsCollection->create();
        $item->addFieldToFilter('product_id', $assignId);
        foreach ($item as $assignProduct) {
            if ($assignProduct->getData() > 0) {
                $data = [];
                $data['status'] = 5;
                $assignProduct->addData($data)->save();
            }
        }
        return $assignProduct ?? null;
    }

    /**
     * Dissapprove Assign Product
     *
     * @param int $assignId
     * @param int $status [optional]
     * @param int $flag [optional]
     */
    public function disApproveUntradedConfigProduct($data, $status = 0, $byAdmin = false)
    {
        if (!$byAdmin) {
            $assignId = $data['assign_id'];
        } else {
            $assignId = $data;
        }
        $assignProduct = $this->getAssignProduct($assignId);
        if ($assignProduct->getId() > 0) {
            if ($status == 1) {
                // $parentProductId = $assignProduct->getProductId();
                $prodata = [];
                $prodata['status'] = 5;
                $prodata['product_id'] = $assignProduct->getProductId();
                $assignProduct->setData($prodata)->setId($assignId)->save();
            }
        }
        return $assignProduct;
    }

    public function compareOptions($itemOptions, $assignId = 0) {
        $itemAssignId = 0;
        if(!empty($itemOptions['info_buyRequest'])) {
            if($itemOptions['info_buyRequest']->getValue()) {
                $value = $itemOptions['info_buyRequest']->getValue();
                $data = json_decode($value, true);
                $itemAssignId = isset($data['mpassignproduct_id']) ? $data['mpassignproduct_id'] : 0;
            }
        }
        // $infoBuyRequest = $itemOptions->getInfoBuyRequest();
        // $data = json_decode($infoBuyRequest, true);
        if ($assignId == 0) {
            $assignId = (int) $this->_request->getParam('mpassignproduct_id');
        }
        if ($itemAssignId == $assignId) {
            return false;
        }
        return true;
    }

    /**
     * Check Whether Added Product to Cart is New or Not
     *
     * @return bool
     */
    public function isNewProduct($productId = 0, $assignId = 0, $childAssignId = 0)
    {
        if ($productId == 0) {
            $productId = (int) $this->_request->getParam('product');
        }
        if ($assignId == 0) {
            $assignId = (int) $this->_request->getParam('mpassignproduct_id');
        }
        if ($childAssignId == 0) {
            $childAssignId = (int) $this->_request->getParam('associate_id');
        }
        $cartModel = $this->getCart();
        $quoteId = $cartModel->getQuote()->getId();
        $collection = $this->getQuoteCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('assign_id', $assignId)
            ->addFieldToFilter('child_assign_id', $childAssignId)
            ->addFieldToFilter('quote_id', $quoteId);
        if ($collection->count() > 0) {
            return false;
        }
        return true;
    }

    public function manageProductStock($result)
    {
        if ($result['type'] == 1) {
            foreach ($result['associates'] as $productId => $info) {
                if ($info['status'] == 1 && $info['is_edit']) {
                    $this->updateStockData($productId, $info['qty']);
                }
            }
        }

        if ($result['status'] == 1) {
            if ($result['type'] != 1) {
                $this->updateStockData($result['product_id'], $result['qty']);
            }
        }
    }

    public function manageProductStockAndStatus($result)
    {
        if ($result['type'] != 1) {
            if ($result['status'] == 1) {
                $this->updateStockData($result['product_id'], $result['qty'], 1, $result['old_qty']);
            } elseif (
                $result['status'] == 2
                && in_array
                (
                    $this->_storeManager->getWebsite()->getCode(),
                    [
                        \Summa\CombustiblesSetUp\Helper\Data::WEBSITE_CODE,
                        \Chilecompra\CombustiblesRenewal\Model\Constants::WEBSITE_CODE
                    ]
                )
            )
            {
                $this->updateStockData($result['product_id'], $result['qty'], 3, $result['old_qty']); //reemplaza el valor de Stock - por eso el 3
            } else {
                $this->disApproveProduct($result['assign_id'], $result['prev_status'], 0, $result['old_qty']);
            }
        }

        if ($result['type'] == 1) {
            foreach ($result['associates'] as $productId => $info) {
                if ($info['is_edit']) {
                    if ($info['manage_stock']) {
                        $this->updateStockData($productId, $info['qty'], 1, $info['old_qty']);
                    } else {
                        $this->updateStockData($productId, $info['qty']);
                    }

                    if ($info['status'] != 1 && $info['flag'] != 0) {
                        /* PHPSTAN - return type of getAssociatedItem() is wrong in 3rd party module*/
                        if ($this->_storeManager->getStore()->getCode() != AseoRenewalConstants::STORE_CODE) {
                            $this->disApproveChildProduct($this->getAssociatedItem($info['associate_id']), $result['prev_status']); /** @phpstan-ignore-line */
                        }
                    }
                }
            }
        }
    }

    /**
     * Manage Product Stock Data
     *
     * @param array $result
     */
    public function processProductStatus($result)
    {
        if ($result['flag'] == 0) {
            $this->manageProductStock($result);
        } else {
            $this->manageProductStockAndStatus($result);
        }
        if ($result['type'] == 1) {
            $this->processConfigProduct($result);
        }
        if ($result['type'] != 1) {
            if ($result['flag'] == 1) {
                if ($result['status'] == 0) {
                    $this->sendProductMail($result, true);
                }
            } else {
                if ($result['status'] == 0) {
                    $this->sendProductMail($result);
                }
            }
        } else {
            foreach ($result['associates'] as $productId => $info) {
                if ($info['status'] == 0 && $info['is_edit']) {
                    $this->sendProductMail(['condition' => $result['condition'], 'product_id' => $info['product_id']], !!$info['flag']);
                }
            }
        }
    }

    /**
     * Remove Unnecessasy Assocaited Assigned Products
     *
     * @param array $result
     */
    public function processConfigProduct($result)
    {
        $productIds = [];
        $assignId = $result['assign_id'];
        foreach ($result['associates'] as $productId => $info) {
            $productIds[] = $productId;
        }
        $model = $this->_associates->create();
        $collection = $model->getCollection()->addFieldToFilter("parent_id", $assignId);
        foreach ($collection as $key => $item) {
            if (!in_array($item->getProductId(), $productIds)) {
                $this->updateStockData($item->getProductId(), $item->getQty(), 2);

                /**
                 * We are not supposed to delete offers, so I'm commenting out the code below
                 */
                //$this->deleteItem($item);
            }
        }
    }

    /**
     * Get Associated Assigned Product By Product and Parent
     *
     * @param int $assignId
     * @param int $productId
     * @return object
     */
    public function getAssociateByProduct($assignId, $productId)
    {
        $model = $this->_associates->create();
        $collection = $model->getCollection()
            ->addFieldToFilter("parent_id", $assignId)
            ->addFieldToFilter("product_id", $productId);
        return $collection->getFirstItem();
    }

    public function getLowestPriceForProduct($productId){
        $product = $this->productRepository->getById($productId);

        $productLowestPrice = null;

        $collectionSellers = $this->_sellerCollection->create()
            ->addFieldToSelect('seller_id')
            ->addFieldToFilter('is_seller', ['eq' => 1]);

        $productPrice = $productLowestPrice;
        if (is_null($productLowestPrice)){
            /** Product Lowest Price in Category */
            $categoryId = $this->getProductCategory($product);
            $categoryProductsIds = !empty($this->_categoryFactory->load($categoryId)->getProductCollection()->getAllIds())
                ? implode(',', $this->_categoryFactory->load($categoryId)->getProductCollection()->getAllIds())
                : null;

            $productLowestPriceInCategory = null;
            if ($categoryProductsIds){
                $productcollection = $this->_productCollection->create()
                    ->addAttributeToSelect('macrozona')
                    ->addFieldToSelect('entity_id')
                    ->addFieldToFilter('type_id', ['eq' => $product->getTypeId()])
                    ->addFieldToFilter('entity_id', ['in' => $categoryProductsIds])
                    ->addAttributeToFilter(
                        [
                            ['attribute'=>'macrozona','eq'=> $product->getmacrozona()]
                        ]
                    )
                    ->addAttributeToFilter(
                        [
                            ['attribute'=>'gamma','eq'=> $product->getgamma()]
                        ]
                    );

                foreach ($productcollection as $value) {
                    $productIds[] = $value['entity_id'];
                }

                $collectionProductInCategori = $this->_itemsCollection->create()
                    ->addFieldToFilter('product_id', ['in' => $productIds])
                    ->addFieldToFilter('status', ['eq' => 1])
                    ->addFieldToFilter('seller_id', ['in' => $collectionSellers->getAllSellerIds() ])
                    ->addFieldToSelect('base_price')
                    ->setOrder('base_price','ASC');

                $productLowestPriceInCategory = $collectionProductInCategori->getFirstItem()->getBasePrice();

                $productPrice = !is_null($productLowestPrice) ? $productLowestPrice : $productLowestPriceInCategory;
            }else{
                $productPrice = $product->getMaxBasePrice();
            }
        }

        return $productPrice;
    }

    /**
     * Only Insumos
     *
     * @param $idProduct
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getOnlyEnabledSeller($idProduct)
    {
        $activeStatus = 70;
        //Only for Insumos
        $connection = $this->_resource->getConnection();
        $tableItems = $this->_resource->getTableName('marketplace_assignproduct_items');
        $tableAssociatedItems = $this->_resource->getTableName('marketplace_assignproduct_associated_products');
        $tableUserData = $this->_resource->getTableName('marketplace_userdata');
        $tableOffer = $this->_resource->getTableName('dccp_offer_product');
        $sql = 'SELECT DISTINCT i.seller_id
            FROM '. $tableAssociatedItems .' asso
                LEFT JOIN '. $tableItems .' i ON i.id = asso.parent_id
                LEFT JOIN '. $tableUserData .' ud ON ud.seller_id = i.seller_id
                LEFT JOIN '. $tableOffer .' p ON asso.product_id = p.product_id AND asso.parent_id = p.assign_id
                LEFT JOIN customer_entity_varchar AS cv ON ud.seller_id = cv.entity_id AND cv.attribute_id = 604
                    WHERE asso.parent_product_id = '. $idProduct .'
                    AND cv.value = '. $activeStatus .'
                    AND asso.status = 1
                    AND asso.qty > 0
                    ORDER BY i.seller_id DESC';
        return $connection->query($sql)->fetchAll();
    }

    /**
     * @param $data
     * @return array
     * @throws LocalizedException
     */
    public function validateFirstPrice($data)
    {
        $website = $this->_storeManager->getWebsite();
        $result = ['error' => false, 'msg' => ''];
        $msg = '';
        $tableSimple = $this->_resource->getTableName('marketplace_assignproduct_items');
        $tableUserData = $this->_resource->getTableName('marketplace_userdata');
        $tableOffer = $this->_resource->getTableName('dccp_offer_product');

        /** Validate price in mobiliario */
        if ($website->getCode() == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) {
            $productPrice = $this->getLowestPriceForProduct($data['product_id']);
            if ($productPrice) {
                //compara el precio total, tiene que comparar solo el base.
                if ($data['base_price'] > $productPrice) {
                    $msg = __("El precio base no puede ser mayor a %1 (menor precio base de la categoría)", $this->currencyFormat($productPrice));
                    $result['error'] = true;
                }
            }

        }

        /** Validate price in emergency */
        if (in_array($website->getCode(), [\Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE, ConstantsEmergency202109::WEBSITE_CODE])) {
            $currentDate = $this->connection->quote($this->timezone->date()->format('Y-m-d'));
            $sql = 'SELECT AVG(s.base_price) AS smallestPrice FROM ' . $tableSimple . ' s
            INNER JOIN ' . $tableUserData . ' u ON s.seller_id = u.seller_id AND u.is_seller = 1
            LEFT JOIN ' . $tableOffer . ' o ON s.id = o.assign_id AND o.status = 1 AND o.website_id = ' . $website->getId() .
                ' AND ' . $currentDate . ' >= DATE(o.start_date) AND ' . $currentDate . ' <= DATE(o.end_date) AND o.special_price < o.base_price
            WHERE s.product_id = ' . (int)$data['product_id'] . ' AND s.status = 1 AND s.qty > 0 AND s.dis_dispersion = 0';
            $size = (double)$this->connection->fetchOne($sql);

            $product = $this->_productCollection->create()
                ->addAttributeToFilter('entity_id', ['eq' => $data['product_id']])
                ->addAttributeToSelect(['max_base_price', 'max_shipping_price'])
                ->getFirstItem();

            /** Validation if it is the first offer */
            if (!$size) {
                if ($product->getMaxBasePrice() && $data['base_price'] > $product->getMaxBasePrice()) {
                    $msg = __("El precio base no puede ser mayor a %1", $this->currencyFormat($product->getMaxBasePrice()));
                    $result['error'] = true;
                }
            }

            /** Minimum shipping price validation */
            if ($product->getMaxShippingPrice() && isset($data['shipping_price']) && $data['shipping_price'] > $product->getMaxShippingPrice()) {
                $msg = __("El precio de envío no puede ser mayor a %1", $this->currencyFormat($product->getMaxShippingPrice()));
                $result['error'] = true;
            }
        }

        /** Validate price in computadores */
        if ($website->getCode() == \Summa\ComputadoresSetUp\Helper\Data::WEBSITE_CODE || $website->getCode() == ComputadoresRenewalConstants::WEBSITE_CODE || $website->getCode() == ComputadoresRenewal2Constants::WEBSITE_CODE) {
            $product = $this->productRepository->getById($data['product_id']);

            /** Product Lowest Price Assign */
            $collectionSellers = $this->_sellerCollection->create()
                ->addFieldToSelect('seller_id')
                ->addFieldToFilter('is_seller', ['eq' => 1]);
            $collectionProduct = $this->_itemsCollection->create()
                ->addFieldToFilter('product_id', ['eq' => $product->getId()])
                ->addFieldToFilter('status', ['eq' => 1])
                ->addFieldToFilter('seller_id', ['in' => $collectionSellers->getAllSellerIds() ])
                ->addFieldToSelect('price')
                ->setOrder('price','ASC');
            $productLowestPrice = $collectionProduct->getFirstItem()->getPrice();
            $productPrice = $productLowestPrice;
            if (is_null($productLowestPrice)){
                /** Product Lowest Price in Category */
                $categoryId = $this->getProductCategory($product);
                $categoryProductsIds = !empty($this->_categoryFactory->load($categoryId)->getProductCollection()->getAllIds())
                    ? implode(',', $this->_categoryFactory->load($categoryId)->getProductCollection()->getAllIds())
                    : null;
                $productLowestPriceInCategory = null;
                if ($categoryProductsIds){
                    $collectionProductInCategori = $this->_itemsCollection->create()
                        ->addFieldToFilter('product_id', ['in' => $categoryProductsIds])
                        ->addFieldToFilter('status', ['eq' => 1])
                        ->addFieldToFilter('seller_id', ['in' => $collectionSellers->getAllSellerIds() ])
                        ->addFieldToSelect('price')
                        ->setOrder('price','ASC');
                    $productLowestPriceInCategory = $collectionProductInCategori->getFirstItem()->getPrice();

                    $productPrice = !is_null($productLowestPrice) ? $productLowestPrice : $productLowestPriceInCategory;
                }
            }
            if ($productPrice) {
                //compara el precio total, tiene que comparar solo el base.
                if ($data['price'] > $productPrice) {
                    $msg = __("El precio no puede ser mayor a %1 (menor precio de la categoría)", $this->currencyFormat($productPrice));
                    $result['error'] = true;
                }
            }
        }

        /** Validate price in escritorio */
        if ($website->getCode() == \Summa\EscritorioSetUp\Helper\Data::WEBSITE_CODE) {
            $product = $this->productRepository->getById($data['product_id']);
            /** Product Lowest Price Assign */
            $collectionSellers = $this->_sellerCollection->create()
                ->addFieldToSelect('seller_id')
                ->addFieldToFilter('is_seller', ['eq' => 1]);
            $collectionProduct = $this->_itemsCollection->create()
                ->addFieldToFilter('product_id', ['eq' => $product->getId()])
                ->addFieldToFilter('status', ['eq' => 1])
                ->addFieldToFilter('seller_id', ['in' => $collectionSellers->getAllSellerIds() ])
                ->addFieldToSelect('price')
                ->setOrder('price','ASC');
            $productLowestPrice = $collectionProduct->getFirstItem()->getPrice();
            $productPrice = $productLowestPrice;
            if (is_null($productLowestPrice)){
                //If the product does not have offers, then it compares with the maximum defined in the product.
                if ($product->getMaxBasePrice()){
                    $productPrice = !is_null($productLowestPrice) ? $productLowestPrice : $product->getMaxBasePrice();
                }
            }
            if ($productPrice) {
                if ((float)$data['price'] > (float)$productPrice ){
                    $msg = __("The price cannot be higher than %1", $this->currencyFormat($productPrice));
                    $result['error'] = true;
                }
            }
        }

        /** Validate price in escritorio */
        if ($website->getCode() == Constants::GAS_WEBSITE_CODE) {

            /** Product Lowest Price Assign */
            $collectionSellers = $this->_sellerCollection->create()
                ->addFieldToSelect('seller_id')
                ->addFieldToFilter('is_seller', ['eq' => 1]);

            $collectionProduct = $this->_itemsCollection->create()
                ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                ->addFieldToFilter('status', ['eq' => 1])
                ->addFieldToFilter('seller_id', ['in' => $collectionSellers->getAllSellerIds() ])
                ->addFieldToSelect('price')
                ->setOrder('price','ASC');

            $productPrice = $collectionProduct->getFirstItem()->getPrice();
            if ($productPrice) {
                if ($this->getPricePercentage((float) $data['price'], $productPrice) > 3) {
                    $msg = __("The price cannot be higher than %3", $this->currencyFormat($productPrice));
                    $result['error'] = true;
                }
            }
        }


        $result['msg'] = $msg;
        return $result;
    }

    /**
     * @param $givenPrice
     * @param $totalPrice
     * @return float
     */
    public function getPricePercentage($givenPrice, $totalPrice): float
    {
        $percentage = ($givenPrice / $totalPrice) * 100;

        return round($percentage, 0);
    }

    /**
     * Get Product Category
     * @param $product
     * @return mixed
     */
    public function getProductCategory($product)
    {
        $productCategory = $product->getCategoryCollection()->getData();
        $category = end($productCategory);
        foreach ($productCategory as $item){
            if ($category['level'] < $item['level']){
                $category = $item;
            }
        }
        return $category['entity_id'];
    }

    public function validateRegion($data){
        $customerId = $this->getCustomerId();
        $product = $this->_productCollection->create()
            ->addFieldToFilter('entity_id', ['eq' => $data['product_id']])
            ->addFieldToSelect('*')
            ->getFirstItem();
        ;
        $result = ['error' => false, 'msg' => ''];
        $msg = '';

        $regionsIds = $this->_regionalData->getRegionalIdsbySeller($customerId);
        $regionId = $product->getRegion();

        /* Validation if product is teh same region */
        if ($regionId && !in_array($regionId, $regionsIds)) {
            $msg = __('Usted no está habilitado a realizar envíos a esta región');
            $result['error'] = true;
        }

        $result['msg'] = $msg;

        return $result;
    }

    /**
     * Get Associated Product Collection
     *
     * @param int $productId
     * @return object
     */
    public function getAssignProductCollection($productId)
    {
        $collection = $this->_itemsCollection->create();
        $joinTable = $this->_resource->getTableName('marketplace_datafeedback');
        $sql = 'mp.seller_id = main_table.seller_id';
        $sql .= ' and mp.status = 1';
        $fields = [];
        $fields[] = 'status';
        $fields[] = 'seller_id as mp_seller_id';
        $fields[] = "sum(mp.feed_price+mp.feed_value+mp.feed_quality) as total_rating";
        $fields[] = "count(mp.seller_id) as count";
        $collection->getSelect()->joinLeft($joinTable.' as mp', $sql, $fields);
        $field = 'sum(mp.feed_price+mp.feed_value+mp.feed_quality)/(count(mp.seller_id)*3)';
        $collection->getSelect()->columns(['rating' => new \Zend_Db_Expr($field)]);

        $joinTable = $this->_resource->getTableName('marketplace_userdata');
        $sql = 'mpud.seller_id = main_table.seller_id';
        $fields = [];
        $fields[] = 'shop_url';
        $fields[] = 'shop_title';
        $fields[] = 'logo_pic';
        $fields[] = 'is_seller';
        $collection->getSelect()->joinLeft($joinTable.' as mpud', $sql, $fields);

        $joinTable = $this->_resource->getTableName('marketplace_assignproduct_associated_products');
        $sql = 'asso.parent_id = main_table.id and asso.status = 1';
        $fields = [];
        $fields[] = "IF(main_table.type='configurable', COUNT(asso.id) , 1) as childs";
        $collection->getSelect()->joinLeft($joinTable.' as asso', $sql, $fields);

        $joinTable = $this->_resource->getTableName('customer_entity');
        $sql = 'cent.entity_id = main_table.seller_id';
        $fields = [];
        $collection->getSelect()->joinLeft($joinTable.' as cent', $sql, $fields);

        $collection->getSelect()->group('main_table.seller_id')
            ->where('mpud.is_seller = 1 and cent.website_id=' . $this->getWebsiteId())->having('childs > 0');
        $collection->addFieldToFilter("product_id", $productId);

        return $collection;
    }

    /**
     * Get Associated Options of Assign Product
     *
     * @param int $productId
     * @return array
     */
    public function getAssociatedOptions($productId, $viewProductId = 0)
    {
        $result = [];
        $model = $this->_associates->create();
        $collection = $model->getCollection()
            ->addFieldToFilter("parent_product_id", $productId)
            ->addFieldToFilter("status", 1);
        foreach ($collection as $item) {
            $info = [
                'id' => $item->getId(),
                'qty' => $item->getQty(),
                'price' => number_format($this->convertPriceFromBase($item->getPrice()), 2)
            ];
            $result[$item->getProductId()][$item->getParentId()] = $info;
        }
        return $result;
    }

    public function getAssociatedOptionsForOriginal($productId)
    {
        $productInfo = [];
        $websiteId = $this->_storeManager->getWebsite()->getId();
        $model = $this->_associates->create();
        $collection = $model->getCollection()->addFieldToFilter("parent_product_id", $productId);
        /* PHPSTAN - eavAttribute property dynamically declared in 3rd party module*/
        $proPriceAttrId = $this->eavAttribute->getIdByCode("catalog_product", "price"); /** @phpstan-ignore-line */
        $catalogProductEntityDecimal = $model->getCollection()->getTable('catalog_product_entity_decimal');
        $catalogInventoryStockItem = $model->getCollection()->getTable('cataloginventory_stock_item');
        $collection->getSelect()->joinLeft(
            $catalogProductEntityDecimal.' as cped',
            'main_table.product_id = cped.row_id and cped.store_id = 0 AND cped.attribute_id = '.$proPriceAttrId,
            ["product_price" => "value"]
        );
        $collection->getSelect()->join(
            $catalogInventoryStockItem.' as csi',
            'main_table.product_id = csi.product_id',
            ["assign_qty" => "qty"]
        )->where("csi.website_id = 0 OR csi.website_id = ".$websiteId);
        foreach ($collection as $item) {
            if (!isset($productInfo[$item->getProductId()])) {
                $productInfo[$item->getProductId()] = [
                    'id' => 0,
                    'qty' => $item->getAssignQty(),
                    'price' => number_format($this->convertPriceFromBase($item->getProductPrice()), 2)
                ];
            }
        }
        return $productInfo;
    }

    public function sendChildStatusMail($associate, $assignProduct, $flag = 0)
    {
        try {
            $adminEmail = $this->getAdminEmail();
            $adminName = $this->getAdminName();
            if ($adminEmail != '') {
                if ($assignProduct->getId() <= 0 || $associate->getId() <= 0) {
                    return;
                }
                $sellerId = $assignProduct->getSellerId();
                $product = $this->getProduct($associate->getProductId());
                if (!($seller = $this->getSellerDetails($sellerId))) {
                    return;
                }
                $shopTitle = $seller->getShopTitle();
                if (!$shopTitle) {
                    $shopTitle = $seller->getShopUrl();
                }
                $customer = $this->_customer->create()->load($sellerId);
                $sellerName = $customer->getFirstname();
                $area = Area::AREA_FRONTEND;
                $store = $this->_storeManager->getStore()->getId();
                $productName = $product->getName();
                if ($flag == 0) {
                    $templateId = "product_approve";
                    $msg = __("Your assigned product for '%1' is approved.", $productName);
                } else {
                    $templateId = "product_disapprove";
                    $msg = __("Your assigned product for '%1' is disapproved.", $productName);
                }
                $templateOptions = ['area' => $area, 'store' => $store];
                $templateVars = [
                    'store' => $this->_storeManager->getStore(),
                    'seller_name' => $sellerName,
                    'msg' => $msg,
                ];
                $from = ['email' => $adminEmail, 'name' => $adminName];
                $this->_inlineTranslation->suspend();
                $to = [$customer->getEmail()];
                static $countCheck = 0;
                $errorMsg = "";
                try {
                    $transport = $this->_transportBuilder
                        ->setTemplateIdentifier($templateId)
                        ->setTemplateOptions($templateOptions)
                        ->setTemplateVars($templateVars)
                        ->setFrom($from)
                        ->addTo($to)
                        ->getTransport();
                    $transport->sendMessage();
                } catch (\Exception $e) {
                    $countCheck++;
                    $errorMsg = $e->getMessage();
                }
                if($countCheck == 1) {
                    $this->_messageManager->addError($errorMsg);
                }
                $this->_inlineTranslation->resume();
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }
    }

    public function manageDescription($data, $assignId, $isFromApi = false)
    {
        /* PHPSTAN - return type of getStore() is wrong in 3rd party module*/
        $store_id = $this->getStore()->getId(); /** @phpstan-ignore-line */
        $collection = $this->_data->create()->getCollection()
            ->addFieldToFilter('assign_id', $assignId)
            ->addFieldToFilter('is_default', 1)
            ->addFieldToFilter('type', 2)
            ->addFieldToFilter('store_view', $store_id);
        if ($collection->getSize()) {
            foreach ($collection as $key) {
                $key->setValue($data['description'])->setId($key->getId())->save();
            }
        } else {
            if ($isFromApi && isset($data['status'])){
                $status = $data['status'];
            }else{
                $status = 1;
            }
            $descData = [
                'type' => 2,
                'assign_id' => $assignId,
                'value' => $data['description'],
                'is_default' => 1,
                'store_view' => $store_id,
                'status' => $status
            ];
            $model = $this->_data->create();
            $model->setData($descData)->save();
        }
    }

    public function getConfigProductChildrens($productId)
    {
        $mainProduct = $this->getProduct($productId);

        $stockState = $this->stockStateInterface;
        $productTypeInstance = $mainProduct->getTypeInstance();
        $associateProducts = $productTypeInstance->getUsedProducts($mainProduct);
        $products = [];
        foreach ($associateProducts as $child) {
            $stock = $stockState->getStockQty($child->getId(), $child->getStore()->getWebsiteId());
            $products[] = [
                'price' => sprintf('%01.2f', $child->getPrice()),
                'stock' => $stock,
                'id' => $child->getId(),
                'sku' => $child->getSku(),
            ];
        }
        return $products;
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getWebsite()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }

    /**
     * @return int
     * @throws LocalizedException
     */
    public function getWebsiteId()
    {
        try {
            $websiteId = $this->_storeManager->getWebsite()->getId();
        }catch (\Exception $e) {
            return 0;
        }
        return $websiteId;
    }

    public function getSellerByCartItem($itemId)
    {
        $item = $this->getQuoteCollection()->addFieldToFilter('item_id', ['eq' => $itemId])->getFirstItem();
        return $item ? $item->getSellerId() : 0;
    }

    /**
     * @param array $data
     * @return boolean
     */
    public function isAllowedGasCategory($data)
    {
        $allowed = false;
        try {
            $envasadoCategoryId = $this->getEnvasadoCategoryId();
            $product = $this->productRepository->getById($data['product_id']);
            $categories = $product->getCategoryIds();
            if (!in_array($envasadoCategoryId, $categories)) {
                $allowed = true;
            }
        } catch (\Exception $ex) { }

        return $allowed;
    }

    /**
     * @return int
     */
    public function getEnvasadoCategoryId()
    {
        $categoryCollection = $this->categoryCollectionFactory->create();
        $category = $categoryCollection->addAttributeToSelect('*')
            ->setPageSize(1)
            ->addAttributeToFilter('name', GasDataHelper::CATEGORY_ENVASADO_NAME)
            ->getFirstItem();

        return $category->getId();
    }
}
