<?php

namespace Formax\AssignProduct\Model;

use Magento\Framework\Model\AbstractModel;

class AssignProduct extends AbstractModel
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\AssignProduct\Model\ResourceModel\AssignProduct::class);
    }
}