<?php

namespace Formax\AssignProduct\Model;

use Magento\Framework\Model\AbstractModel;

class LinkedProduct extends AbstractModel
{
    /**
     * Linked Product
     * -   cache tag.
     */
    const CACHE_TAG = 'dccp_linked_product';

    /**
     * @var string
     */
    protected $_cacheTag = 'dccp_linked_product';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'dccp_linked_product';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\AssignProduct\Model\ResourceModel\LinkedProduct::class);
    }
}