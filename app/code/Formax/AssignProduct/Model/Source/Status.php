<?php

namespace Formax\AssignProduct\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Status implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Disapproved'),
                'value' => 0
            ],
            [
                'label' => __('Approved'),
                'value' => 1
            ],
            [
                'label' => __('Pending'),
                'value' => 2
            ],
            [
                'label' => __('Disabled by dispersion'),
                'value' => 3
            ],
            [
                'label' => __('Disabled by price'),
                'value' => 4
            ],
            [
                'label' => __('Disabled by No traded Product'),
                'value' => 5
            ],
            [
                'label' => __('Disabled by Empty Stock'),
                'value' => 6
            ],
            [
                'label' => __('Disabled by penalty'),
                'value' => 7
            ]
        ];
    }
}
