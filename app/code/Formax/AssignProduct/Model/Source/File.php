<?php

namespace Formax\AssignProduct\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class File
 */
class File implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {        
        return [
            [
                'label' => __('No files'),
                'value' => 0
            ],
            [
                'label' => __('Has files'),
                'value' => 1
            ]
        ];
    }
}
