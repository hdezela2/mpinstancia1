<?php


namespace Formax\AssignProduct\Model;

class AssignStatus extends \Magento\Framework\Model\AbstractModel
{
    const MAGE_STATUS = 'mage_status';
    const DCCP_STATUS = 'dccp_status';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Formax\AssignProduct\Model\ResourceModel\AssignStatus::class);
    }
}