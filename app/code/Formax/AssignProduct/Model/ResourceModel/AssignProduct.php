<?php

namespace Formax\AssignProduct\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class AssignProduct extends AbstractDb
{
    public function _construct()
    {
        $this->_init('dccp_assign_product', 'id');
    }
}