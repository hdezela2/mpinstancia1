<?php

namespace Formax\AssignProduct\Model\ResourceModel\AssignProduct;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Formax\AssignProduct\Model\AssignProduct',
            'Formax\AssignProduct\Model\ResourceModel\AssignProduct'
        );
    }
}
