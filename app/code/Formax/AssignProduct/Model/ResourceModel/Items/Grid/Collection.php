<?php

namespace Formax\AssignProduct\Model\ResourceModel\Items\Grid;

use Webkul\MpAssignProduct\Model\ResourceModel\Items\Grid\Collection as MpAssignProductGridCollection;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\Collection as ItemsCollection;

class Collection extends MpAssignProductGridCollection
{
    /**
     * @inheritdoc
     */
    protected $_map = ['fields' => ['id' => 'main_table.id', 'status' => 'main_table.status']];

    /**
     * Join store relation table if there is store filter.
     */
    protected function _renderFiltersBefore()
    {
        /* PHPSTAN - attributeFactory property dynamically declared in 3rd party module*/
        $eavAttribute = $this->attributeFactory->create(); /** @phpstan-ignore-line */
        $proAttId = $eavAttribute->getIdByCode('catalog_product', 'name');
        $proPriceAttId = $eavAttribute->getIdByCode('catalog_product', 'price');
        $proWeightAttId = $eavAttribute->getIdByCode('catalog_product', 'weight');

        $customerGridFlat = $this->getTable('customer_grid_flat');
        $catalogProductEntityVarchar = $this->getTable('catalog_product_entity_varchar');
        $catalogProductEntityDecimal = $this->getTable('catalog_product_entity_decimal');
        $cataloginventoryStockItem = $this->getTable('cataloginventory_stock_item');
        $dccpAssignProductFile = $this->getTable('dccp_assign_product');

        $sql = $customerGridFlat.' as cgf';
        $cond = 'main_table.seller_id = cgf.entity_id';
        $fields = ['name' => 'name'];
        $this->getSelect()
            ->joinLeft($sql, $cond, $fields);
        $this->addFilterToMap('name', 'cgf.name');

        $sql = $catalogProductEntityVarchar.' as cpev';
        $cond = 'main_table.product_row_id = cpev.row_id AND cpev.store_id = 0 AND cpev.attribute_id = ' . $proAttId;
        $fields = ['product_name' => 'value'];
        $this->getSelect()
            ->joinLeft($sql, $cond, $fields);
        $this->addFilterToMap('product_name', 'cpev.value');

        $sql = $catalogProductEntityDecimal.' as cped';
        $cond = 'main_table.product_id = cped.row_id AND cped.store_id = 0 AND (cped.attribute_id = ' . $proPriceAttId . ' OR cped.attribute_id = ' . $proWeightAttId . ')';
        $fields = ['product_price' => 'value'];
        $this->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $dccpAssignProductFile.' as fl';
        $cond = 'main_table.seller_id = fl.seller_id and main_table.product_id = fl.product_id';
        $fields = ['file' => 'file', 'file_aseo' => 'file_aseo', 'file_anexo' => 'file_anexo'];
        $this->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $this->getSelect()->group('main_table.id');
        $this->addFilterToMap('product_price', 'cped.value');
        $this->addFilterToMap('main_qty', 'csi.qty');

        ItemsCollection::_renderFiltersBefore();
    }
}
