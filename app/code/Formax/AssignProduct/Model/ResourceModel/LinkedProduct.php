<?php

namespace Formax\AssignProduct\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class LinkedProduct extends AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    public function _construct()
    {
        $this->_init('dccp_linked_product', 'id');
    }
}