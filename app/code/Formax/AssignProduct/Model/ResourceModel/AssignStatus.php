<?php
namespace Formax\AssignProduct\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class AssignStatus extends AbstractDb
{
    public function _construct()
    {
        $this->_init('dccp_assign_status', 'id');
    }
}