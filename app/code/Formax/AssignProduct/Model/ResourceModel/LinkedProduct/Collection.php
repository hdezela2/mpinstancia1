<?php

namespace Formax\AssignProduct\Model\ResourceModel\LinkedProduct;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'formax_assignproduct_dccp_linked_product_collection';
    protected $_eventObject = 'dccp_linked_product_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Formax\AssignProduct\Model\LinkedProduct',
            'Formax\AssignProduct\Model\ResourceModel\LinkedProduct'
        );
    }
}
