<?php

namespace Formax\AssignProduct\Controller\Adminhtml\Product;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class MassApprove.
 */
class MassApprove extends \Webkul\MpAssignProduct\Controller\Adminhtml\Product\MassApprove
{
    /**
     * Execute action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        foreach ($collection as $item) {
            $assignId = $item->getId();
            $qty = $item->getQty();
            $status = $item->getStatus();
            $type = $item->getType();
            $assignProduct = $this->_assignHelper->getAssignProduct($assignId);

            if ($status != 1 && $type != 'configurable') {
                $assignProduct = $this->_assignHelper->approveProduct($assignId);
                $this->_assignHelper->sendStatusMail($assignProduct);
            } else if ($type == 'configurable') {
                $associates = $this->_assignHelper->getAssociates($assignId);

                foreach($associates as $associate) {
                    $status = $associate->getStatus();
                    //Approve Product
                    if ($status != 1) {
                        $associate = $this->_assignHelper->approveChildProduct($associate);
                        $this->_assignHelper->sendChildStatusMail($associate, $assignProduct);
                    }
                }

                $this->_assignHelper->setConfigStatus($assignProduct);
            }
        }

        $msg = 'A total of %1 Product(s) have been approved.';
        $this->messageManager->addSuccess(__($msg, $collection->getSize()));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
