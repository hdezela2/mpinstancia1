<?php

namespace Formax\AssignProduct\Controller\Adminhtml\Product;

use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Store\Model\ResourceModel\Website\CollectionFactory;
use Magento\Store\Api\Data\WebsiteInterfaceFactory;
use Summa\MobiliarioSetUp\Helper\Data;
use Linets\RegulatedProduct\ViewModel\RegulatedData;
use Formax\AssignProduct\Model\Source\Status;
use Formax\AssignProduct\Helper\MpAssignProduct as helperDataProducts;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Save extends \Webkul\MpAssignProduct\Controller\Adminhtml\Product\Save
{
    protected $productRepository;
    protected $assignProduct;
    protected $requestedStatus;
    protected $websiteFactory;
    protected $resultRedirect;
    protected $assignId;
    protected $message;
    protected $product;
    protected $comment;
    protected $status;
    protected $type;
    protected $regulatedData;

    /**
     * Property status
     *
     * @var Status $_status
     */
    private $_status;

    /**
     * Property helper data products
     *
     * @var helperDataProducts $_helperDataProducts
     */
    private $_helperDataProducts;

    /**
     * Property product repository
     *
     * @var ProductRepositoryInterface $_productRepository
     */
    private $_productRepositoryInterface;

    /**
     * @var SchemaSetupInterface
     */
    private $setup;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $registry,
        \Webkul\MpAssignProduct\Model\ItemsFactory $items,
        \Webkul\MpAssignProduct\Helper\Data $mpAssignHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product\Action $productAction,
        ProductFactory $productFactory,
        WebsiteInterfaceFactory $websiteFactory,
        RegulatedData $regulatedData,
        SchemaSetupInterface $setup,
        Status $status,
        helperDataProducts $helperDataProducts,
        ProductRepositoryInterface $productRepository
    ) {
        $this->productRepository = $productFactory;
        $this->websiteFactory = $websiteFactory;
        $this->regulatedData = $regulatedData;
        $this->setup = $setup;
        $this->_status = $status;
        $this->_helperDataProducts = $helperDataProducts;
        $this->_productRepositoryInterface = $productRepository;

        parent::__construct($context, $registry, $items, $mpAssignHelper, $storeManager, $productAction);

    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $this->resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if ($this->getRequest()->isPost())
        {
            $this->initialize();
            $params = $this->getRequest()->getParams();
            if (!$this->validateObservationOnStatus($params)) {
                return $this->resultRedirect->setPath('*/*/edit', ['id' => $this->assignId]);
            }
            $result = $this->manageAssign();
            if (!$result) {
                $this->messageManager->addError(__('The comment of disapprove is required.'));
                return $this->resultRedirect->setPath('*/*/edit', ['id' => $this->assignId]);
            }

            if($this->isMobiliarioProduct($this->assignProduct->getProductId())) {
                $this->assignLinkedProducts();
            }

            $this->messageManager->addSuccess(__("Status updated successfully."));
            return $this->resultRedirect->setPath('*/*/edit', ['id' => $this->assignId]);
        }
        $this->messageManager->addError("Something went wrong.");
        return $this->resultRedirect->setPath('*/*/');
    }

    private function assignLinkedProducts()
    {
        $linkedProducts = $this->getLinkedProducts();
        $this->message .= " Updated Mobiliario Products: ";
        foreach ($linkedProducts as $linked)
        {
            $this->message .= $linked->getSku() . " ";
            if($linked->getId() != $this->product->getId())
            {
                $sellerId = $this->_assignHelper->getAssignProduct($this->assignId)->getSellerId();
                $lnkedAssignId = $this->_assignHelper->getAssignId($linked->getId(), $sellerId);
                $this->initialize($lnkedAssignId);
                $this->manageAssign();
            }
        }
    }

    private function initialize($assignId=null)
    {
        $this->assignId = $assignId == null ? $this->getRequest()->getParam('id') : $assignId;
        $this->requestedStatus = $this->getRequest()->getParam('product_status');
        $this->comment = $this->getRequest()->getParam('comment', '');
        $this->assignProduct = $this->_items->create()->load($this->assignId);
        $this->status = $this->assignProduct->getStatus();
        $this->type = $this->assignProduct->getType();
    }

    private function manageAssign()
    {
        $result = true;
        if ($this->type != 'configurable') {
            if ($this->requestedStatus == 1) {
                $this->approve();
            } else if ($this->requestedStatus == 0) {
                $result = $this->disapprove();
            } else if ($this->requestedStatus == 2) {
                $this->pending();
            } else {
                if ($this->assignId) {
                    $assignProduct = $this->_helperDataProducts->getAssignProduct($this->assignId);
                    $assignProduct->setStatus($this->requestedStatus);
                    $assignProduct->setComment($this->comment);
                    $assignProduct->save();
                }
            }
        } elseif ($this->type == 'configurable') {
            $result = $this->manageAssignConfigurable();
        }

        return $result;
    }

    private function getLinkedProducts()
    {
        $linkedProducts = [];
        $linkedProductSkus = explode(',', $this->product->getCustomAttribute('linked_products')->getValue());
        foreach ($linkedProductSkus as $sku)
        {
            $product = $this->productRepository->create()->loadByAttribute('sku', $sku);
            $linkedProducts[] = $product;
        }
        return $linkedProducts;
    }

    private function approve()
    {
        $assignProduct = $this->_assignHelper->approveProduct($this->assignId, $this->comment);
        $this->_assignHelper->sendStatusMail($assignProduct);
    }

    private function disapprove()
    {
        $success = true;
        if ($this->comment == '' && $this->status == 0) {
            $success = false;
        }

        $assignProduct = $this->_assignHelper->disApproveProduct($this->assignId, 1, 1, 0, $this->comment);
        $this->_assignHelper->sendStatusMail($assignProduct, 1);

        return $success;
    }

    private function pending()
    {
        $this->assignProduct = $this->_assignHelper->pendingProduct($this->assignId, 1, 1, 0, $this->requestedStatus, $this->comment);
    }

    private function isMobiliarioProduct($productId)
    {
        $this->product = $prod = $this->productRepository->create()->load($productId);
        foreach ($prod->getWebsiteIds() as $websiteId)
        {
            $websiteTemp = $this->websiteFactory->create()->load($websiteId, 'website_id');
            if ($websiteTemp->getCode() == Data::WEBSITE_CODE) {
                return true;
            }
        }
        return false;
    }

	/**
	 * @return void
	 */
	public function manageAssignConfigurable()
	{
        $success = true;
        $associates = $this->_assignHelper->getAssociates($this->assignId);

		$statusesArray = [];
		$statuses = [
			'disapproved' => 0,
			'approved' => 0,
			'pending' => 0,
			'disabled_by_dispersion' => 0,
			'disabled_by_price' => 0,
			'disabled_by_no_traded_product' => 0,
			'disabled_by_empty_stock' => 0,
			'disabled_by_penalty' => 0
		];


        foreach ($associates as $associate) {
            $status = $associate->getStatus();
            $requestedStatus = $this->getRequest()->getParam('product_status_' . $associate->getProductId());
            $comment = $this->getRequest()->getParam('comment_' . $associate->getProductId());
            if ($requestedStatus == 1) {
                //Approve Product
                $associate = $this->_assignHelper->approveChildProduct($associate, $comment);
                $this->_assignHelper->sendChildStatusMail($associate, $this->assignProduct);
                $this->approveRegulatedProductFile($associate->getParentProductId(), $this->assignProduct->getSellerId());
            } else if ($requestedStatus == 0) {
                //Disapprove Product
                if ($comment === '') {
                    $success = false;
                }

                if ($success) {
                    $associate = $this->_assignHelper->disApproveChildProduct($associate, 1, 1, 0, $comment);
                    $this->_assignHelper->sendChildStatusMail($associate, $this->assignProduct, 1);
                }
            } else {
                $associate = $this->_assignHelper->pendingChildProduct($associate, 1, 1, 0, $requestedStatus, $comment);
            }

			switch ($status) {
				case 0: $statuses['disapproved']++; break;
				case 1: $statuses['approved']++; break;
				case 2: $statuses['pending']++; break;
				case 3: $statuses['disabled_by_dispersion']++; break;
				case 4: $statuses['disabled_by_price']++; break;
				case 5: $statuses['disabled_by_no_traded_product']++; break;
				case 6: $statuses['disabled_by_empty_stock']++; break;
				case 7: $statuses['disabled_by_penalty']++; break;
			}
        }

		$statusesArray = $this->evaluateAssociatesStatuses($statusesArray, $statuses);

		$statusesArray[$this->assignId]['products_count'] = count($associates);
		$this->assignProduct->setAssociates(json_encode($statusesArray));

		$this->_assignHelper->setConfigStatus($this->assignProduct);

        return $success;
    }


	/**
	 * @param $statusesArray
	 * @param $status
	 * @return array
	 */
	public function evaluateAssociatesStatuses($statusesArray, $statuses): array
	{
		$assignId = $this->assignId;

		($statuses['disapproved'] > 0) ? $statusesArray[$assignId]['statuses']['disapproved'] = $statuses['disapproved'] : null;
		($statuses['approved'] > 0) ? $statusesArray[$assignId]['statuses']['approved'] = $statuses['approved'] : null;
		($statuses['pending'] > 0) ? $statusesArray[$assignId]['statuses']['pending'] = $statuses['pending'] : null;
		($statuses['disabled_by_dispersion'] > 0) ? $statusesArray[$assignId]['statuses']['disabled_by_dispersion'] = $statuses['disabled_by_dispersion'] : null;
		($statuses['disabled_by_price'] > 0) ? $statusesArray[$assignId]['statuses']['disabled_by_price'] = $statuses['disabled_by_price'] : null;
		($statuses['disabled_by_no_traded_product'] > 0) ? $statusesArray[$assignId]['statuses']['disabled_by_no_traded_product'] = $statuses['disabled_by_no_traded_product'] : null;
		($statuses['disabled_by_empty_stock'] > 0) ? $statusesArray[$assignId]['statuses']['disabled_by_empty_stock'] = $statuses['disabled_by_empty_stock'] : null;
		($statuses['disabled_by_penalty'] > 0) ? $statusesArray[$assignId]['statuses']['disabled_by_penalty'] = $statuses['disabled_by_penalty'] : null;

		return $statusesArray;
    }

    /**
     * Activation of regulated product file status
     *
     * @param $productId
     * @param $seller_id
     * @return void|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function approveRegulatedProductFile($productId, $sellerId)
    {
        $fileCollection = $this->regulatedData->getAssingProductFile($productId, $sellerId, 0);

        if ($productId > 0 && $sellerId > 0 && !empty($fileCollection) && $fileCollection->getStatus() == 0) {
            $table = 'dccp_assign_product';
            $field = 'status';
            $sql = 'UPDATE ' . $table . ' SET `' . $field . '` = "1" WHERE `product_id` = '
                . $productId . ' AND `seller_id` = ' . $sellerId . ';';
            $this->setup->getConnection()->query($sql);

        }
        return null;
    }

    /**
     * Method validate if observation is required
     *
     * @param array $params
     */
    private function validateObservationOnStatus($params)
    {
        $dataFromProduct = [];
        if (!isset($params['id'])) {
            $this->messageManager->addError(
                __("Id is required")
            );
            return false;
        }
        $productId = $this->assignProduct->getProductId();
        foreach ($params as $key => $value) {
            switch ($key) {
                case "key":
                case "active_tab":
                case "form_key":
                case "id":
                    break;
                default:
                    $arrD   = explode("_", $key);
                    $idProd = $this->tryToGetIdProduct($arrD, 2);
                    //validatate if struct of data of product status is correct
                    if (isset($arrD[0]) && $arrD[0]=='product' && $idProd>0) {
                        $dataFromProduct[$idProd]['status'] = $value;
                    }
                    $idProd = $this->tryToGetIdProduct($arrD, 1);
                    //validate if struct data of comment of status is correct
                    if (isset($arrD[0]) && $arrD[0]=='comment' && $idProd>0) {
                        $dataFromProduct[$idProd]['comment'] = $value;
                    }
                    break;
            }
        }
        $dataProducts = $this->_helperDataProducts->getAssociatesData($params['id']);
        $isValid = true;
        foreach ($dataFromProduct as $id => $item) {
            if (isset($dataFromProduct[$id])
                && isset($dataProducts)
                && is_array($dataProducts)
                && count($dataProducts) > 0
                && $dataProducts[$id]['status'] != $item['status']
                && $item['status'] == $this->_helperDataProducts::DISAPPROVE_STATUS // Update Status Deprecated
                && empty($dataFromProduct[$id]['comment'])) {
                $product = $this->_productRepositoryInterface->getById($id);
                $name = $product->getName();
                $this->messageManager->addError(
                    __('Comment of product %1 is required', $name)
                );
                $isValid = false;
            }
        }
        // this validation is only the seller have one product asing
        if (!isset($dataProducts) || !is_array($dataProducts) || count($dataProducts) < 1) {
            foreach ($dataFromProduct as $id => $item) {
                if ($dataFromProduct[$id]['status'] == $this->_helperDataProducts::DISAPPROVE_STATUS
                    && empty($dataFromProduct[$id]['comment'])) {
                    $product = $this->_productRepositoryInterface->getById($id);
                    $name = $product->getName();
                    $this->messageManager->addError(
                        __('Comment of product %1 is required', $name)
                    );
                    $isValid = false;
                }
            }
        }
        return $isValid;
    }

    /**
     * Method to get Id product to update
     *
     * @param array $arrD
     * @param int $index
     * @return int
     */
    private function tryToGetIdProduct($arrD, $index)
    {
        if (isset($arrD[$index])) {
            return (int)$arrD[$index];
        } else {
            $productId = (int)$this->assignProduct->getProductId();
            if (isset($productId)) {
                return $productId;
            } else {
                return 0;
            }
        }
    }
}
