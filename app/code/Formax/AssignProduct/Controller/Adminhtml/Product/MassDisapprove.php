<?php

namespace Formax\AssignProduct\Controller\Adminhtml\Product;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class MassDisapprove.
 */
class MassDisapprove extends \Webkul\MpAssignProduct\Controller\Adminhtml\Product\MassDisapprove
{
    /**
     * Execute action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        foreach ($collection as $item) {
            $assignId = $item->getId();
            $qty = $item->getQty();
            $status = $item->getStatus();
            $type = $item->getType();
            $assignProduct = $this->_assignHelper->getAssignProduct($assignId);

            if ($status != 0 && $type != 'configurable') {
                $assignProduct = $this->_assignHelper->disApproveProduct($assignId, 1, 1);
                $this->_assignHelper->sendStatusMail($assignProduct, 1);
            } else if ($type == 'configurable') {
                $associates = $this->_assignHelper->getAssociates($assignId);

                foreach($associates as $associate) {
                    $status = $associate->getStatus();
                    //Disapprove Product
                    if ($status != 0) {
                        $associate = $this->_assignHelper->disApproveChildProduct($associate, 1, 1, 0);
                        $this->_assignHelper->sendChildStatusMail($associate, $assignProduct, 1);
                    }
                }

                $this->_assignHelper->setConfigStatus($assignProduct);
            }
        }

        $msg = 'A total of %1 Product(s) have been disapproved.';
        $this->messageManager->addSuccess(__($msg, $collection->getSize()));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
