<?php

namespace Formax\AssignProduct\Controller\Product;

use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

class Save extends \Webkul\MpAssignProduct\Controller\Product\Save
{
    /**
     * @var \Formax\AssignProduct\Model\AssignProductFactory
     */
    protected $assignProduct;

    /**
     * File Uploader factory.
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var Filesystem
     */
    protected $_mediaDirectory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var int
     */
    protected $websiteId;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var \Formax\StatusChange\Helper\Data
     */
    protected $helperSellerStatus;

    /**
     * @param Context $context
     * @param \Magento\Customer\Model\Url $url
     * @param \Magento\Customer\Model\Session $session
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Formax\AssignProduct\Model\AssignProductFactory $assignProduct
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Formax\StatusChange\Helper\Data $helperSellerStatus
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Psr\Log\LoggerInterface $logger
     * @param Filesystem $filesystem
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Url $url,
        \Magento\Customer\Model\Session $session,
        \Webkul\MpAssignProduct\Helper\Data $helper,
        \Magento\Catalog\Model\Product\Copier $productCopier,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        StockConfigurationInterface $stockConfiguration,
        StockRegistryInterface $stockRegistry,
        ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\Product\Gallery\Processor $imageProcessor,
        \Magento\Catalog\Model\ResourceModel\Product\Gallery $productGallery,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Formax\AssignProduct\Model\AssignProductFactory $assignProduct,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Formax\StatusChange\Helper\Data $helperSellerStatus,
        \Magento\Framework\App\ResourceConnection $resource,
        \Psr\Log\LoggerInterface $logger,
        Filesystem $filesystem
    ) {
        $this->resource = $resource;
        $this->connection = $this->resource->getConnection();
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->assignProduct = $assignProduct;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->helperSellerStatus = $helperSellerStatus;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->websiteId = (int)$this->storeManager->getStore()->getWebsiteId();

        parent::__construct(
            $context,
            $url,
            $session,
            $helper,
            $productCopier,
            $mpHelper,
            $stockConfiguration,
            $stockRegistry,
            $productRepository,
            $productFactory,
            $imageProcessor,
            $productGallery
        );
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page|void
     */
    public function execute()
    {
        $this->connection->beginTransaction();

        try {
            $helper = $this->_assignHelper;
            $data = $this->getRequest()->getParams();
            $data['image'] = '';
            $data['price'] = preg_replace('([.,\-+])', '', $data['price']);
            $dccpStatus = $this->helperSellerStatus->getDccpStatusSeller();

            $productId = $data['product_id'];
            $product = $helper->getProduct($productId);
            $productType = $product->getTypeId();
            $customerId = $this->getCustomerLoggedId();
            $sellerId = $helper->getSellerIdBySubAccount();
            $customer = $this->customerRepositoryInterface->getById($sellerId);
            $customerLoguedIn = $this->customerRepositoryInterface->getById($customerId);

            $token = $customerLoguedIn->getCustomAttribute('user_rest_atk') ?
                $customerLoguedIn->getCustomAttribute('user_rest_atk')->getValue() : '';
            $userId = $customer->getCustomAttribute('user_rest_id') ?
                $customer->getCustomAttribute('user_rest_id')->getValue() : '';
            $agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
                $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : '';
            $sellerStatus = $customer->getCustomAttribute('wkv_dccp_state_details') ?
                $customer->getCustomAttribute('wkv_dccp_state_details')->getValue() : '';
            $dccpSellerStatus = isset($dccpStatus[$sellerStatus]) ? $dccpStatus[$sellerStatus] : 0;

            if (empty($token) || empty($userId) || empty($agreementId) || $dccpSellerStatus == 0) {
                $this->messageManager->addError(__('Params missing to execute assign product service.'));
                $this->connection->rollBack();
                return $this->resultRedirectFactory->create()->setPath('*/*/view');
            }

            if (empty($data["product_id"])) {
                $this->messageManager->addError(__('Something went wrong.'));
                return $this->resultRedirectFactory->create()->setPath('*/*/view');
            }

            if (!empty($data["assign_id"])) {
                $flag = 1;
            } else {
                $flag = 0;
                $data['del'] = 0;
            }

            if ($productType == 'configurable') {
                foreach ($data['products'] as $kItem => $item) {
                    $p = $helper->getProduct($kItem);
                    $stock = (float)$item['qty'] > 0 ? true : false;
                    $params = [
                        'idOrganismo' => $userId,
                        'idProducto' => $p->getSku(),
                        'idConvenioMarco' => $agreementId,
                        'idEstadoProductoProveedorConvenio' => $dccpSellerStatus,
                        'tieneStock' => $stock,
                        'precio' => (float)$item['price']
                    ];
                    $validateServiceAssign = $flag === 1 ? $helper->setAssignProductUpdate($token, $params) :
                        $helper->setAssignProduct($token, $params);

                    if (!isset($validateServiceAssign['success']) ||
                        (isset($validateServiceAssign['success']) && isset($validateServiceAssign['success']) != 'OK')) {
                        break;
                    }
                }
            } else {
                $stock = (float)$data['qty'] > 0 ? true : false;
                $params = [
                    'idOrganismo' => $userId,
                    'idProducto' => $product->getSku(),
                    'idConvenioMarco' => $agreementId,
                    'idEstadoProductoProveedorConvenio' => $dccpSellerStatus,
                    'tieneStock' => $stock,
                    'precio' => (float)$data['price']
                ];
                $validateServiceAssign = $flag === 1 ? $helper->setAssignProductUpdate($token, $params) :
                    $helper->setAssignProduct($token, $params);
            }

            if (isset($validateServiceAssign['success']) && $validateServiceAssign['success'] == 'OK') {
                $result = $helper->validateData($data, $productType, true, (bool)$flag);

                if ($result['error']) {
                    $this->messageManager->addError(__($result['msg']));
                    $this->connection->rollBack();
                    return $this->resultRedirectFactory->create()->setPath('*/*/view');
                }

                $result = $helper->processAssignProduct($data, $productType, $flag);

                if (($productType == 'virtual' && isset($result['error']) && $result['error']) || ($productType == 'simple' && isset($result['error']) && $result['error'])) {
                    $this->messageManager->addError(__($result['msg']));
                    $this->connection->rollBack();
                    return $this->resultRedirectFactory->create()->setPath('*/*/view');
                } else if ($productType == 'configurable' && isset($result['associates']['error']) && $result['associates']['error']) {
                    $this->messageManager->addError(__($result['associates']['msg']));
                    $this->connection->rollBack();
                    return $this->resultRedirectFactory->create()->setPath('*/*/view');
                } else {
                    if ($result['assign_id']) {
                        $helper->processProductStatus($result);
                        $helper->manageImages($data, $result);
                        // Save upload file
                        $this->uploadFile($this->getRequest()->getFiles());
                        $this->connection->commit();

                        $this->messageManager->addSuccess(__('Product is saved successfully.'));
                        return $this->resultRedirectFactory->create()->setPath('*/*/productlist');
                    } else {
                        $this->messageManager->addError(__('There was some error while processing your request.'));
                        $this->connection->rollBack();
                        return $this->resultRedirectFactory->create()->setPath('*/*/add', ['id' => $data['product_id']]);
                    }
                }
            } else {
                if (isset($validateServiceAssign['success']) && $validateServiceAssign['success'] == 'NOK' &&
                    isset($validateServiceAssign['errores']) && is_array($validateServiceAssign['errores'])) {
                    foreach ($validateServiceAssign['errores'] as $error) {
                        $this->messageManager->addError(__($error['descripcion']));
                    }
                } elseif (!isset($validateServiceAssign['success']) && isset($validateServiceAssign['error']) &&
                    isset($validateServiceAssign['message'])) {
                    $message = $validateServiceAssign['error'] . '. ' . $validateServiceAssign['message'];
                    $this->messageManager->addError(__($message));
                } else {
                    $this->messageManager->addError(__('There was some error while processing your request.'));
                }

                $this->connection->rollBack();
                return $this->resultRedirectFactory->create()->setPath('*/*/view');
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            $this->connection->rollBack();
            $this->messageManager->addError(__('There was some error while processing your request.'));
        }
    }

    /**
     * Upload file
     *
     * @param array $files
     * @param array $data
     * @param string $productType
     */
    public function uploadFile($files)
    {
        if ($files) {
            try {
                $isAseoRenewal = $this->_storeManager->getStore()->getCode() == AseoRenewalConstants::WEBSITE_CODE;
                $fileToUpload = false;
                $customerId = 0;
                $target = $this->_mediaDirectory->getAbsolutePath('chilecompra/files/assignproducts');
                if ($this->_session->isLoggedIn()) {
                    $customerId = (int)$this->_assignHelper->getSellerIdBySubAccount();
                }

                $docs = [];

                if (!empty($files["products"]) && (!$this->isInsumosWebsite() || !$isAseoRenewal)) {
                    foreach ($files['products'] as $key => $file) {
                        if (isset($file['name']) && !empty($file['name'])) {
                            $docs[$key] = null;
                        }
                    }
                } else {
                    foreach ($files as $key => $file) {
                        if (isset($file['name']) && !empty($file['name'])) {
                            $docs[$key] = null;
                        }
                    }
                }

                if ( count($docs) > 0 ) {

                    foreach($docs as $key => $doc){
                        if (!empty($files["products"]) && (!$this->isInsumosWebsite() || !$isAseoRenewal)) {
                            $uploader = $this->_fileUploaderFactory->create(['fileId' => $files['products'][$key]]);
                        } else {
                            $fileToUpload = $key;
                            $uploader = $this->_fileUploaderFactory->create(['fileId' => $key]);
                        }
                        $uploader->setAllowedExtensions(['pdf', 'png']);
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(true);
                        $docs[$key] = $uploader->save($target);

                        if(!isset($docs[$key]['path']) || !isset($docs[$key]['file']) || !isset($docs[$key]['type'])){
                            $docs[$key]['file'] = null;
                        }

                        if ($this->isInsumosWebsite() || $isAseoRenewal) {
                            $productId = (int)substr($fileToUpload, strrpos($fileToUpload, '_' )+1);
                        } else {
                            $productId = (int)substr($key, strrpos($key, '_' )+1);
                        }

                        if ($productId > 0 && $customerId > 0) {
                            $baseId = 'document_';
                            $configurableBaseId = 'document_child_';
                            $aseoId = 'doc_aseo_';
                            $anexoId = 'doc_anexo_';
                            $regulatedId = 'document_regulated_';
                            $authorizedLetterId = 'authorized_letter_';
                            $model = $this->assignProduct->create()->getCollection()
                                ->addFieldToFilter('product_id', $productId)
                                ->addFieldToFilter('seller_id', $customerId)
                                ->addFieldToFilter('website_id', $this->websiteId);

                            if($model->getSize() > 0){
                                $model = $model->getFirstItem();
                            }else{
                                $model = $this->assignProduct->create();
                            }

                            $model->setProductId($productId);
                            $model->setSellerId($customerId);
                            $model->setWebsiteId($this->websiteId);
                            if( isset( $docs[$baseId.$productId] ) ){
                                $model->setFile($docs[$baseId.$productId]['file']);
                            }
                            if( isset( $docs[$configurableBaseId.$productId] ) ){
                                $model->setFile($docs[$configurableBaseId.$productId]['file']);
                            }
                            if( isset( $docs[$regulatedId.$productId] ) ){
                                $model->setRegulatedProductFilePath($docs[$regulatedId.$productId]['file']);
                            }
                            if( isset( $docs[$aseoId.$productId] ) ){
                                $model->setFileAseo($docs[$aseoId.$productId]['file']);
                            }
                            if( isset( $docs[$anexoId.$productId] ) )
                            {
                                $model->setFileAnexo($docs[$anexoId.$productId]['file']);
                            }
                            if (isset($docs[$authorizedLetterId.$productId])) {
                                $model->setAuthorizedLetter($docs[$authorizedLetterId.$productId]['file']);
                            }
                            $model->setStatus(1);
                            if ($this->isInsumosWebsite() && isset( $docs[$regulatedId.$productId] )) {
                                $model->setStatus(0);
                            }
                            if ($isAseoRenewal && isset($docs[$authorizedLetterId.$productId])) {
                                $model->setStatus(0);
                            }
                            $model->save();
                        }
                    }
                }
            } catch(\Exception $e) {
                $this->logger->critical($e->getMessage());
                $this->messageManager->addError(
                    __('This file type is not allowed. Only Pdf')
                );
                return $this;
            }
        }
    }

    /**
     * Get Current Customer Id
     *
     * @return int
     */
    protected function getCustomerLoggedId()
    {
        $customerId = 0;
        if ($this->_session->isLoggedIn()) {
            $customerId = (int) $this->_session->getCustomerId();
        }
        return $customerId;
    }

    /**
     * Get if current store if Insumos Medicos
     *
     * @return bool
     */
    protected function isInsumosWebsite(): bool
    {
        try {
            $result = $this->_storeManager->getStore()->getCode() == InsumosConstants::WEBSITE_CODE;
        } catch(\Exception $e){
            $result = false;
        }
        return $result;
    }
}
