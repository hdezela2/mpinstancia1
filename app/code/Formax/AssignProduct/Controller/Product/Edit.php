<?php

namespace Formax\AssignProduct\Controller\Product;

class Edit extends \Webkul\Marketplace\Controller\Product\Edit
{
    /**
     * Seller Product Edit Action.
     *
     * @return \Magento\Framework\Controller\Result\RedirectFactory
     */
    public function execute()
    {
        $helper = $this->_objectManager->create(
            'Webkul\Marketplace\Helper\Data'
        );
        $isPartner = $helper->isSeller();
        if ($isPartner == 1) {
            $productId = (int) $this->getRequest()->getParam('id');
            $rightseller = $helper->isRightSeller($productId);
            if ($rightseller) {
                $helper = $this->_objectManager->get(
                    'Webkul\Marketplace\Helper\Data'
                );
                /* PHPSTAN - productBuilder property dynamically declared in 3rd party module*/
                $product = $this->productBuilder->build( /** @phpstan-ignore-line */
                    $this->getRequest()->getParams(),
                    $helper->getCurrentStoreId()
                );

                if ($productId && !$product->getId()) {
                    $this->messageManager->addError(
                        __('This product no longer exists.')
                    );
                    /*
                     * @var \Magento\Backend\Model\View\Result\Redirect
                     */
                    $resultRedirect = $this->resultRedirectFactory->create();

                    return $resultRedirect->setPath(
                        '*/*/productlist',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                }
                if ($productId) {
                    /** @var \Magento\Framework\View\Result\Page $resultPage */
                    $resultPage = $this->_resultPageFactory->create();
                    if ($helper->getIsSeparatePanel()) {
                        $resultPage->addHandle('marketplace_layout2_product_edit');
                    }
                    $resultPage->getConfig()->getTitle()->set(
                        __('Edit Product')
                    );

                    $collectionFactory = $this->_objectManager->get(
                        'Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory'
                    );
                    /**
                     * update notification for products
                     */
                    $collection = $collectionFactory->create()
                    ->addFieldToFilter(
                        'mageproduct_id',
                        $productId
                    )->addFieldToFilter(
                        'seller_pending_notification',
                        1
                    );
                    if ($collection->getSize()) {
                        $type = \Webkul\Marketplace\Model\Notification::TYPE_PRODUCT;
                        $this->_objectManager->get(
                            'Webkul\Marketplace\Helper\Notification'
                        )->updateNotificationCollection(
                            $collection,
                            $type
                        );
                    }

                    return $resultPage;
                } else {
                    return $this->resultRedirectFactory->create()->setPath(
                        '*/*/add',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                }
            } else {
                return $this->resultRedirectFactory->create()->setPath(
                    'mpassignproduct/product/productlist',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
}
