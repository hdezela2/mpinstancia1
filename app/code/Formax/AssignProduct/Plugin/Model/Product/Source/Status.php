<?php

namespace Formax\AssignProduct\Plugin\Model\Product\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Status
{
    /**
     * Get options
     *
     * @return array
     */
    public function aroundToOptionArray(
        \Webkul\MpAssignProduct\Model\Product\Source\Status $subject, \Closure $proceed
    ) {
        $availableOptions = ['Disapproved', 'Approved', 'Pending', 'Disabled by dispersion', 'Disabled by price', 'Disabled by No traded Product', 'Disabled by Empty Stock', 'Disabled by penalty'];
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }

        return $options;
    }
}
