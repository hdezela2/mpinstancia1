<?php
namespace Formax\AssignProduct\Plugin;

use Formax\AssignProduct\Helper\MpAssignProduct as FormaxHelper;
use Webkul\MpAssignProduct\Helper\Data as WebkulHelper;

class AfterValidateFirstPrice
{
    /** @var FormaxHelper */
    protected $_helper;

    public function __construct(
        FormaxHelper $helper
    ) {
        $this->_helper = $helper;
    }

    public function afterValidateFirstPrice(WebkulHelper $subject, $result, $data)
    {
        /** Validation if there exists a linked product offer */
        $product = $this->_helper->getProduct($data['product_id']);
        $linkedProductsBasePrice = $this->_helper->getLinkedProductBasePrice($product);
        if ($linkedProductsBasePrice['offered']) {
            if ($linkedProductsBasePrice['value'] > 0 && $data['base_price'] > $linkedProductsBasePrice['value']) {
                $msg = __("El precio base no puede ser mayor a %1", $linkedProductsBasePrice['base_price']);
                $result['error'] = true;
                $result['msg'] = $msg;
            }
        }
        return $result;
    }
}