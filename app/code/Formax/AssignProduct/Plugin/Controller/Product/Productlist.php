<?php
namespace Formax\AssignProduct\Plugin\Controller\Product;

use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Framework\App\Cache\TypeListInterface;

class Productlist
{
    /** @var TypeListInterface */
    protected $_cacheTypeList;

    /** @var Pool */
    protected $_cacheFrontendPool;

    public function __construct(
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool
    ) {
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
    }

    public function beforeExecute(\Webkul\MpAssignProduct\Controller\Product\Productlist $subject)
    {
        $this->_cacheTypeList->cleanType('layout');
        foreach ($this->_cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}
