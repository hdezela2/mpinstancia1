<?php

namespace Formax\AssignProduct\Block\Product;

use Summa\StockManagementConfiguration\Helper\Data;
use Formax\Offer\Helper\Data as SellerHelper;

class Add extends \Webkul\MpAssignProduct\Block\Product\Add
{
    /**
     * @var \Formax\AssignProduct\Helper\MpAssignProduct
     */
    protected $helper;

    /**
     * @var \Summa\StockManagementConfiguration\Helper\Data
     */
    protected $stockManagementHelper;

    /**
     * @var \Formax\Offer\Helper\Data
     */
    protected $sellerHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Formax\AssignProduct\Helper\MpAssignProduct $helper
     * @param \Summa\StockManagementConfiguration\Helper\Data $stockManagementHelper
     * @param \Formax\Offer\Helper\Data $sellerHelper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Formax\AssignProduct\Helper\MpAssignProduct $helper,
        SellerHelper $sellerHelper,
        Data $stockManagementHelper
    ) {
        $this->helper = $helper;
        $this->stockManagementHelper = $stockManagementHelper;
        $this->sellerHelper = $sellerHelper;
        parent::__construct($context);
    }

    /**
     * Define if upload file field must shown
     * 
     * @param int $productId
     * @return bool
     */
    public function showUploadFileField($productId, $isAseoDoc = false)
    {
        return !$this->helper->isProductAssignToSeller($productId, $isAseoDoc);
    }

    public function getMaxBasePrice($productId)
    {
        return $this->helper->getMaxBasePrice($productId);
    }

    public function getMaxShippingPrice($productId)
    {
        return $this->helper->getMaxShippingPrice($productId);
    }

    public function shouldRenderStockOptions()
    {
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $enable = $this->stockManagementHelper->getGeneralConfig('enable', $websiteId) == 1;
        if(!$enable)
            return true;

        $sellerId = $this->sellerHelper->getSellerId();
        $seller = $this->sellerHelper->getCustomerById($sellerId);
        $difference = $this->getDateDifference($seller->getData()["wkv_dccp_date_of_entry"], date("Y-m-d"));

        return  $difference >= $this->stockManagementHelper->getGeneralConfig('number_of_days', $websiteId);
    }

    protected function getDateDifference($firstDate, $secondDate)
    {
        $first = date_create($firstDate);
        $second = date_create($secondDate);
        $diffInYears = date_diff($first, $second)->format('%r%y');
        return $diffInYears * 12 + date_diff($first, $second)->format('%r%m');
    }

    public function getStockDisableText($key)
    {
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        return $this->stockManagementHelper->getGeneralConfig($key, $websiteId);
    }

    /**
     * @return int
     */
    public function getWebsiteId()
    {
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();

        return $websiteId;
    }
}
