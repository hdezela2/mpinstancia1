<?php

namespace Formax\AssignProduct\Ui\DataProvider;

use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemCollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;

/**
 * Class AssignProduct
 */
class AssignProduct extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var MarketplaceHelper
     */
    protected $_marketplaceHelper;

    /**
     * @var CustomerSession
     */
    protected $_itemCollectionFactory;

    /**
     * @var EavAttribute
     */
    protected $_eavAttribute;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param MarketplaceHelper $marketplaceHelper
     * @param ItemCollectionFactory $itemCollectionFactory
     * @param EavAttribute $eavAttribute
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        MarketplaceHelper $marketplaceHelper,
        ItemCollectionFactory $itemCollectionFactory,
        EavAttribute $eavAttribute,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );

        $this->_eavAttribute = $eavAttribute;
        $this->_marketplaceHelper = $marketplaceHelper;
        $this->_itemCollectionFactory = $itemCollectionFactory;

        $customerId = $this->_marketplaceHelper->getCustomerId();
        $attrProductName = $this->_eavAttribute->getIdByCode('catalog_product', 'name');
        $sellercollection = $this->_itemCollectionFactory->create();
        $sellercollection->addFieldToSelect('id')
                        ->addFieldToSelect('price')
                        ->addFieldToSelect('description')
                        ->addFieldToSelect('qty')
                        ->addFieldToSelect('comment')
                        ->addFieldToSelect('dis_dispersion')
                        ->addFieldToSelect('status');
        $sellercollection->addFieldToFilter('main_table.seller_id', $customerId)
                ->getSelect()->join(
                    ['e' => $sellercollection->getResource()->getTable('catalog_product_entity')],
                    'main_table.product_id = e.entity_id',
                    ['sku', 'type_id']
                )->join(
                    ['cpev' => $sellercollection->getResource()->getTable('catalog_product_entity_varchar')],
                    'e.row_id = cpev.row_id AND cpev.store_id = 0 AND cpev.attribute_id = ' . $attrProductName,
                    ['name' => 'cpev.value']
                )->joinLeft(
                    ['doc' => $sellercollection->getResource()->getTable('dccp_assign_product')],
                    'main_table.product_id = doc.product_id AND main_table.seller_id = doc.seller_id',
                    ['file', 'file_aseo', 'file_anexo']
                );
        $sellercollection->addFilterToMap('id', 'main_table.id');
        $sellercollection->addFilterToMap('name', 'cpev.value');
        $sellercollection->setOrder('main_table.created_at', 'desc');
        
        $this->collection = $sellercollection;
    }
}
