<?php

namespace Formax\AssignProduct\Ui\DataProvider;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemCollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;

/**
 * Class AdminAssignProduct
 */
class AdminAssignProduct extends AbstractDataProvider
{
    /**
     * @var CustomerSession
     */
    protected $_itemCollectionFactory;

    /**
     * @var EavAttribute
     */
    protected $_eavAttribute;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string  $requestFieldName
     * @param ItemCollectionFactory $itemCollectionFactory
     * @param EavAttribute $eavAttribute
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        ItemCollectionFactory $itemCollectionFactory,
        EavAttribute $eavAttribute,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        
        $this->_eavAttribute = $eavAttribute;
        $this->_itemCollectionFactory = $itemCollectionFactory;
        
        $sellercollection = $this->_itemCollectionFactory->create();

        $proAttId = $this->_eavAttribute->getIdByCode('catalog_product', 'name');
        $proPriceAttId = $this->_eavAttribute->getIdByCode('catalog_product', 'price');
        $proWeightAttId = $this->_eavAttribute->getIdByCode('catalog_product', 'weight');

        $customerGridFlat = $sellercollection->getResource()->getTable('customer_grid_flat');
        $catalogProductEntity = $sellercollection->getResource()->getTable('catalog_product_entity');
        $catalogProductEntityVarchar = $sellercollection->getResource()->getTable('catalog_product_entity_varchar');
        $catalogProductEntityDecimal = $sellercollection->getResource()->getTable('catalog_product_entity_decimal');
        $cataloginventoryStockItem = $sellercollection->getResource()->getTable('cataloginventory_stock_item');
        $dccpAssignProductFile = $sellercollection->getResource()->getTable('dccp_assign_product');
        $associatedProducts = $sellercollection->getResource()->getTable('marketplace_assignproduct_associated_products');

        $sql = $catalogProductEntity . ' as e';
        $cond = 'main_table.product_id = e.entity_id';
        $fields = ['sku', 'product_created_at' => 'created_at'];
        $sellercollection->getSelect()
            ->joinLeft($sql, $cond, $fields);
        
        $sql = $customerGridFlat . ' as cgf';
        $cond = 'main_table.seller_id = cgf.entity_id';
        $fields = ['name' => 'name', 'website_id' => 'website_id', 'wkv_dccp_rut' => 'wkv_dccp_rut'];
        $sellercollection->getSelect()
            ->joinLeft($sql, $cond, $fields);
        $sellercollection->addFilterToMap('name', 'cgf.name');

        $sql = $catalogProductEntityVarchar . ' as cpev';
        $cond = 'main_table.product_row_id = cpev.row_id AND cpev.store_id = 0 AND cpev.attribute_id = ' . $proAttId;
        $fields = ['product_name' => 'value'];
        $sellercollection->getSelect()
            ->joinLeft($sql, $cond, $fields);
        $sellercollection->addFilterToMap('product_name', 'cpev.value');

        $sql = $catalogProductEntityDecimal . ' as cped';
        $cond = 'main_table.product_id = cped.row_id AND cped.store_id = 0 AND (cped.attribute_id = ' . $proPriceAttId . ' OR cped.attribute_id = ' . $proWeightAttId . ')';
        $fields = ['product_price' => 'value'];
        $sellercollection->getSelect()
            ->joinLeft($sql, $cond, $fields);
        
        $sql = $dccpAssignProductFile . ' as fl';
        $cond = 'main_table.seller_id = fl.seller_id and main_table.product_id = fl.product_id';
        $fields = ['file' => 'file', 'file_aseo' => 'file_aseo', 'regulated_product_file_path' => 'regulated_product_file_path', 'file_anexo' => 'file_anexo'];
        $sellercollection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sql = $associatedProducts . ' as asso';
        $cond = 'main_table.id = asso.parent_id';
        $fields = [
            'pending_children_count' => new \Zend_Db_Expr('SUM(CASE WHEN asso.status = 2 THEN 1 ELSE 0 END)'),
            'approved_children_count' => new \Zend_Db_Expr('SUM(CASE WHEN asso.status = 1 THEN 1 ELSE 0 END)'),
            'disapproved_children_count' => new \Zend_Db_Expr('SUM(CASE WHEN asso.status = 0 THEN 1 ELSE 0 END)'),
            'disabled_children_count' => new \Zend_Db_Expr('SUM(CASE WHEN NOT asso.status IN (0, 1, 2) THEN 1 ELSE 0 END)')
        ];
        $sellercollection->getSelect()
            ->joinLeft($sql, $cond, $fields);

        $sellercollection->getSelect()->group('main_table.id');
        $sellercollection->addFilterToMap('id', 'main_table.id');
        $sellercollection->addFilterToMap('product_price', 'cped.value');
        $sellercollection->addFilterToMap('main_qty', 'csi.qty');
        $sellercollection->addFilterToMap('status', 'main_table.status');
        $sellercollection->addFilterToMap('product_created_at', 'e.created_at');
        $sellercollection->addFilterToMap('created_at', 'main_table.created_at');
        $sellercollection->addFilterToMap('website_id', 'cgf.website_id');
       
        $this->collection = $sellercollection;
    }

    /**
     * @param \Magento\Framework\Api\Filter $filter
     * @return void
     */
    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        if ($filter->getField() == 'file' || $filter->getField() == 'file_aseo') {
            $val = [];
            $conditon = '';
            $i = 0;
            foreach ($filter->getValue() as $value) {
                if ($value == '1') {
                    $conditon = 'notnull';
                    $val = true;
                } else {
                    $conditon = 'null';
                    $val = true;
                }
                $i++;
            }

            if ($i == 1) {
                $this->collection->addFieldToFilter(
                    $filter->getField(),
                    [$conditon => $val]
                );
            }
        } else if ($filter->getField() == 'associates') {
			$this->collection->addFieldToFilter(
				$filter->getField(),
				["like" => $filter->getValue()]
			);
		} else {
            parent::addFilter($filter);
        }
    }
}
