<?php

namespace Formax\AssignProduct\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;
use Formax\AssignProduct\Helper\MpAssignProduct as Helper;

class FileAnexo extends Column
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param Helper $helper
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = array(),
        Helper $helper,
        array $data = array()
    ) {
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );

        $this->helper = $helper;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['file_anexo']) && $item['file_anexo'] != '') {
                    $item[$name] = '<a href="' . $this->helper->getMediaBaseUrl($item['file_anexo']) . '" target="_blank">' . __('View Document') . '</a>';
                }
            }
        }
        return $dataSource;
    }
}