<?php

namespace Formax\AssignProduct\Ui\Component\Listing\Column;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;

class Quantity extends Column
{
    /**
     * @var HelperSoftware
     */
    protected $helperSoftware;

    /**
     * SpecialPrice constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param HelperSoftware $helperSoftware
     * @param Helper $helper
     * @param VoucherHelper $voucherHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        HelperSoftware $helperSoftware,
        array $components = array(),
        array $data = array()
    ) {
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );

        $this->helperSoftware = $helperSoftware;
    }

    /**
     * Prepare component configuration
     *
     * @return void
     */
    public function prepare()
    {
        parent::prepare();

        if ($this->helperSoftware->getCurrentStoreCode() == HelperSoftware::SOFTWARE_STOREVIEW_CODE || $this->helperSoftware->getCurrentStoreCode() == SoftwareRenewalConstants::STORE_CODE) {
            $this->_data['config']['componentDisabled'] = true;
        }
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if ($item['type_id'] == 'configurable') {
                    $item[$name] = '-';
                } else {
                    if(isset($item['dis_dispersion']) && $item['dis_dispersion'] == 1){
                        $item[$name] = __('Blocked by dispersion');
                    }else{
                        if (isset($item['qty']) && (float)$item['qty'] > 0) {
                            $item[$name] = __('In Stock');
                        } else {
                            $item[$name] = __('Without Stock');
                        }
                    }
                }
            }
        }
        return $dataSource;
    }
}
