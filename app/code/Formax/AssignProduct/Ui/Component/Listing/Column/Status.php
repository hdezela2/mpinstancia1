<?php

namespace Formax\AssignProduct\Ui\Component\Listing\Column;

use Formax\AssignProduct\Helper\MpAssignProduct;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Formax\ConfigCmSoftware\Helper\Data as HelperSoftware;

class Status extends Column
{

    /** @var MpAssignProduct */
    protected $_helper;

    public function __construct(
        MpAssignProduct $helper,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->_helper = $helper;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                if ( (isset($item['type_id']) && $item['type_id'] == 'configurable') || (isset($item['type']) && $item['type'] == 'configurable') ) {
                    $associateStatus = $this->_helper->getAssociatesStatus($item['id']);
                    if ($item[$name] == 2) {
                        $item[$name] = '';
                    }
                    switch ($this->getAssociateStatus($associateStatus)) {
                        case 2:
                            $item[$name] = 2;
                            break;
                        case 1:
                            $item[$name] = 1;
                            break;
                        case 0:
                            $item[$name] = 0;
                            break;
                        default:
                            $item[$name] = $this->getAssociateStatus($associateStatus);
                    }
                }
            }
        }
        return $dataSource;
    }

    protected function getAssociateStatus($associateStatus) {
        foreach ($associateStatus as $status) {
            if ($status == 2)
                return 2;
        }
        foreach ($associateStatus as $status) {
            if ($status == 1)
                return 1;
        }
        foreach ($associateStatus as $status) {
            if ($status == 0)
                return 0;
        }
        foreach ($associateStatus as $status) {
            return $status;
        }
    }
}