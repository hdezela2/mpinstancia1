<?php
namespace Formax\MustLogin\Block;

class LoggedIn extends \Magento\Framework\View\Element\Template{

  /**
   * @var \Magento\Framework\App\Http\Context
   */
  protected $httpContext;

  /**
   * View constructor.
   * @param \Magento\Backend\Block\Template\Context $context
   * @param \Magento\Framework\App\Http\Context $httpContext
   * @param array $data
   */
  public function __construct(
      \Magento\Backend\Block\Template\Context $context,
      \Magento\Framework\App\Http\Context $httpContext,
      array $data = []
  ) {
      parent::__construct($context, $data);
      $this->httpContext = $httpContext;
    }

    /**
     * get if user is logged in
     * @return boolean
     */
    public function isLoggedIn()
    {
        return $this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }
}
