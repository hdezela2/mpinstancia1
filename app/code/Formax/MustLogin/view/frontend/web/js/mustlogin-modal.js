define([
      "jquery",
      "ko",
      "Magento_Ui/js/modal/modal"
    ], function($, ko){

        var MustLoginModal = {

            initModal: function(config) {

                var $target = $(config.modalContainer);
                var selector = '[data-ccrole="'+config.modalRole+'"]';

                var options = {
                  responsive: true,
                  innerScroll: true,
                  modalClass: 'mustlogin-modal',
                  title: '¡Lo sentimos!',
                  closeText: 'Ok',
                  buttons: [
                    {
                      text: 'Ok',
                      class: 'action-primary',
                      click: function(){
                        $target.modal('closeModal');
                      }
                    }
                  ]
                };
                $target.modal(options);

                $(document).on('click', selector, function(event){
                  if(Number(config.isLoggedIn) != 1){
                    event.preventDefault();
                    event.stopPropagation();
                    $target.modal('openModal');
                  }
                });

            }
        };
        return {
            'mustlogin-modal': MustLoginModal.initModal
        };
    }
);
