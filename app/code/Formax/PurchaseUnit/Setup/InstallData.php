<?php
namespace Formax\PurchaseUnit\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Customer\Setup\CustomerSetupFactory;

class InstallData implements InstallDataInterface
{
    /**
     * @var Magento\Customer\Setup\CustomerSetupFactory
     */
    protected $_customerSetupFactory;

    /**
     * @var Magento\Sales\Setup\SalesSetupFactory
     */
    protected $_salesSetupFactory;

    /**
     * @var Magento\Quote\Setup\QuoteSetupFactory
     */
    protected $_quoteSetupFactory;

    protected $_attributeSetFactory;

    /**
     * @param SalesSetupFactory $salesSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
        CustomerSetupFactory $customerSetupFactory,
        \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
    ) {
        $this->_salesSetupFactory = $salesSetupFactory;
        $this->_quoteSetupFactory = $quoteSetupFactory;
        $this->_customerSetupFactory = $customerSetupFactory;
        $this->_attributeSetFactory = $attributeSetFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {

        /** @var \Magento\Quote\Setup\QuoteSetup $quoteInstaller */
        $quoteInstaller = $this->_quoteSetupFactory
                        ->create(
                            [
                                'resourceName' => 'quote_setup',
                                'setup' => $setup
                            ]
                        );
        $salesInstaller = $this->_salesSetupFactory
                        ->create(
                            [
                                'resourceName' => 'sales_setup',
                                'setup' => $setup
                            ]
                        );

        $customerInstaller = $this->_customerSetupFactory
                        ->create(
                            [
                                'resourceName' => 'customer_setup',
                                'setup' => $setup
                            ]
                        );
        $type = \Magento\Framework\DB\Ddl\Table::TYPE_TEXT;
        $this->addAttributes("quote", "purchase_unit", $type, 255, $quoteInstaller);
        $this->addAttributes("order", "purchase_unit", $type, 255, $salesInstaller);

        /*$customerEntity = $customerInstaller->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        
        $attributeSet = $this->_attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $customerInstaller->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'user_rest_partner_organizations', [
            'type' => 'text',
            'label' => 'Lista de Organizaciones(MP)',
            'input' => 'text',
            'required' => false,
            'visible' => false,
            'user_defined' => true,
            'position' =>999,
            'system' => 0,
        ]);

        $attribute = $customerInstaller->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'user_rest_partner_organizations')
        ->addData([
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroupId,
            'used_in_forms' => ['adminhtml_customer'],
        ]);

        $attribute->save();*/
    }

    public function addAttributes($entity, $code, $type, $length, $installer) {
        $installer->addAttribute($entity, $code, ['type' => $type, 'length'=> $length, 'visible' => false, 'nullable' => true]);
    }
}