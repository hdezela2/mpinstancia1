<?php
namespace Formax\PurchaseUnit\Block\Checkout;

class Unit extends \Magento\Framework\View\Element\Template
{
    /**
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $_cart;
    /**
     *
     * @var \Magento\Customer\Model\Customer
     */
    protected $_customerRepository;

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Customer $customerRepository,
        \Magento\Checkout\Model\Session $cart,
        array $data = []
    ) {
        $this->_cart = $cart;
        $this->_customerRepository = $customerRepository;
        parent::__construct($context, $data);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * units list
     * @return array
     */
    public function getUnits()
    {
        $quote = $this->_cart->getQuote();
        $customerQuote = $quote->getCustomer();
        $customer = $this->_customerRepository->load($customerQuote->getId());
        $defaultOrganization = $customer->getUserRestIdOrganization();
        $organizationsText = $customer->getUserRestPartnerOrganizations();
        if (!$quote->getPurchaseUnit() || $quote->getPurchaseUnit() == '') {
            $quote->setPurchaseUnit($defaultOrganization);
            $quote->save();
        }
        if (!$organizationsText || $organizationsText == "") {
            return ["null"=>["is_default"=>true, "text"=>"NO_OPTIONS"]];
        }
        $partnerOrganizations = json_decode($organizationsText);
        $options = [];
        foreach($partnerOrganizations as $organization) {
            $options[$organization->idOrganizacion] = ["is_default"=>false, "text"=>$organization->nombre];
            if ($organization->idOrganizacion == $quote->getPurchaseUnit()) {
                $options[$organization->idOrganizacion]["is_default"] = true;
            }
        }
        return $options;
    }
}
