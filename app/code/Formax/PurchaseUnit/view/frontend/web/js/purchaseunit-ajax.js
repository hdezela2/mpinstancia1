define([
    "jquery"
], function($){
    var PurchaseUnitAjax = {
        initPurchaseUnit: function(config) {
          
          $('select[name="unit_code"]').data('selected', $('select[name="unit_code"]').val());
          $('select[name="unit_code"]').off().on('change',function(){
            let unit = $(this).val();
            if($(this).data('selected') != unit){
                $('body').trigger('processStart');
                updateQuotePurchaseUnitData(unit);
            }else{
                $(this).data('selected', $(this).val());
            }
          });

          function updateQuotePurchaseUnitData(unit){
            var form = $('#purchaseunit-applier-form');

            if(!form.length){
              var form = document.createElement('form'),
                  punit = document.createElement('input'),
                  formkey = document.createElement('input');
              form.method = 'POST';
              form.action = config.serviceUrl;
              form.id = 'purchaseunit-applier-form';
              formkey.name = 'form_key';
              formkey.type = 'hidden';
              formkey.value = getFormKey();
              punit.name = 'purchase_unit';
              punit.value = unit;
              punit.type = 'hidden';
              form.appendChild(punit);
              form.appendChild(formkey);
              document.body.appendChild(form);
              form.submit();
            }else{
              form.find('input[name="purchase_unit"]').val(unit);
              form.find('input[name="form_key"]').val(getFormKey());
              form.submit();
            }
          }

          function getFormKey(){
            return $.cookie('form_key');
          }
        }
    };
    return PurchaseUnitAjax.initPurchaseUnit;
}
);
