<?php
namespace Formax\PurchaseUnit\Controller\Checkout;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class ApplyAjax extends Action
{ 
    protected $resultJsonFactory;
    /**
     *
     */
    protected $checkoutSession;
    /**
     *
     */
    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();

        $fieldValues = $this->getRequest()->getParams();
        $quote = $this->checkoutSession->getQuote();

        if(isset($fieldValues['purchase_unit'])) {
          $quote->setPurchaseUnit($fieldValues['purchase_unit']);
          $quote->save();
          $result->setData(['updatedQuote'=>true]);
        }else{
          $result->setData(['updatedQuote'=>false]);
        }
        return $result;
    }
}
