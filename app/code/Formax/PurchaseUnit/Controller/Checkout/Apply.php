<?php
namespace Formax\PurchaseUnit\Controller\Checkout;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Apply extends Action
{
    /**
     *
     */
    protected $checkoutSession;

    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $fieldValues = $this->getRequest()->getParams();
        $quote = $this->checkoutSession->getQuote();
        if(isset($fieldValues['purchase_unit'])) {
          $quote->setPurchaseUnit($fieldValues['purchase_unit']);
          $quote->save();
        }
        return $this->resultRedirectFactory
            ->create()
            ->setPath('checkout/cart', ['_secure' => $this->getRequest()->isSecure()]);
    }
}
