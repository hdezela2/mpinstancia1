<?php
namespace Formax\PriceDispersion\Helper;

use Formax\AgreementInfo\Helper\Data as AgreementHelper;
use Formax\PriceDispersion\Model\DispersionHistory;
use Formax\PriceDispersion\Model\DispersionHistoryFactory;
use Formax\PriceDispersion\Model\Repository\DispersionHistoryRepository;
use Intellicore\EmergenciasRenewal\Constants as EmergenciasRenewalConstants;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\Filter;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\MpAssignProduct\Model\AssociatesFactory as AssociatesFactory;
use Webkul\MpAssignProduct\Model\ItemsFactory as AssignItemsFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollectionFactory;
use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Formax\Offer\Helper\Data as HelperOffer;

class Data extends AbstractHelper
{
    /** @var StoreManagerInterface */
    protected $storeManager;

    /** @var CategoryRepositoryInterface */
    protected $categoryRepository;

    /** @var AgreementHelper */
    protected $_agreementHelper;

    /** @var array */
    protected $categoriesEmergencia;

    /** @var array */
    protected $categoriesMobiliario;

    /** @var integer */
     protected $baseCategoryEmergencia;

    /** @var integer */
    protected $baseCategoryMobiliario;
    private \Magento\Catalog\Model\CategoryFactory $categoryFactory;

    /** @var AssignItemsFactory */
    private $assignItems;

    /** @var ResourceConnection */
    private $resourceConnection;

    /** @var CategoryCollectionFactory */
    private $categoryCollectionFactory;

    /** @var AssociatesFactory */
    private $assignAssociates;

    /** @var ProductCollectionFactory */
    private $productCollectionFactory;

    /** @var RequestInterface */
    private $request;

    /** @var DispersionHistoryFactory */
    protected $_dispersionHistoryFactory;

    /** @var DispersionHistoryRepository */
    protected $_dispersionHistoryRepository;

    /** @var SearchCriteria */
    protected $_searchCriteria;

    /** @var FilterGroup */
    protected $_filterGroup;

    /** @var Filter */
    protected $_filter;

    /** @var AssociatesCollectionFactory */
    protected $_associatesCollection;

    /** @var DateTime */
    protected $date;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;

    /**
     * @var array
     */
    private $sellerStatuses;

    /**
     * @var HelperOffer
     */
    protected $helperOffer;

    /**
     * @param Context $context
     * @param ProductCollectionFactory $productCollectionFactory
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param AssignItemsFactory $assignItems
     * @param AssociatesFactory $assignAssociates
     * @param StoreManagerInterface $storeManager
     * @param ResourceConnection $resourceConnection
     * @param CategoryRepositoryInterface $categoryRepository
     * @param AgreementHelper $agreementHelper
     * @param DispersionHistoryFactory $dispersionHistoryFactory
     * @param DispersionHistoryRepository $dispersionHistoryRepository
     * @param SearchCriteria $searchCriteria
     * @param FilterGroup $filterGroup
     * @param Filter $filter
     * @param AssociatesCollectionFactory $associatesCollection
     * @param DateTime $date
     */
    public function __construct(
        Context $context,
        ProductCollectionFactory $productCollectionFactory,
        CategoryCollectionFactory $categoryCollectionFactory,
        AssignItemsFactory $assignItems,
        AssociatesFactory $assignAssociates,
        StoreManagerInterface $storeManager,
        ResourceConnection $resourceConnection,
        CategoryRepositoryInterface $categoryRepository,
        AgreementHelper $agreementHelper,
        DispersionHistoryFactory $dispersionHistoryFactory,
        DispersionHistoryRepository $dispersionHistoryRepository,
        SearchCriteria $searchCriteria,
        FilterGroup $filterGroup,
        Filter $filter,
        AssociatesCollectionFactory $associatesCollection,
        DateTime $date,
        CustomerRepositoryInterface $customerRepositoryInterface,
        HelperOffer $helperOffer
    ) {
        $this->request = $context->getRequest();
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->assignItems = $assignItems;
        $this->assignAssociates = $assignAssociates;
        $this->storeManager = $storeManager;
        $this->resourceConnection = $resourceConnection;
        $this->categoryRepository = $categoryRepository;
        $this->_agreementHelper = $agreementHelper;
        $this->_dispersionHistoryFactory = $dispersionHistoryFactory;
        $this->_dispersionHistoryRepository = $dispersionHistoryRepository;
        $this->_searchCriteria = $searchCriteria;
        $this->_filterGroup = $filterGroup;
        $this->_filter = $filter;
        $this->_associatesCollection = $associatesCollection;
        $this->date = $date;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->helperOffer = $helperOffer;
        parent::__construct($context);
    }

    public function getPostData()
    {
        $dispersionHistoryId = $this->request->getParam('dispersion_history_id');

        if ($dispersionHistoryId) {
            try {
                $dispersionHistory = $this->_dispersionHistoryRepository->getById($dispersionHistoryId);
                $categories = $dispersionHistory->getCategories();
                $percentage = $dispersionHistory->getPricePercentage();
                $allCategories = $this->getAllCategories($categories);

                $response['pricePercentage'] = $percentage;
                $response['allCategories'] = implode(',', array_merge($allCategories, $this->categoriesEmergencia, $this->categoriesMobiliario));
                $response['assignProductIds'] = $dispersionHistory->getAssignProductIds();
                $response['dispersionHistoryId'] = $dispersionHistoryId;
                $response['newIndex'] = $dispersionHistory->getProductCountActual();
            } catch (NoSuchEntityException $e) {
                $response['error'] = $e->getMessage();
            }
            return $response;
        } else {

        $categories = $this->request->getParam('categories');
        $percentage = $this->request->getParam('update_percentage');
        $allCategories = $this->getAllCategories($categories);

        $response['pricePercentage'] = $percentage;
        $response['allCategories'] = implode(',', array_merge($allCategories, $this->categoriesEmergencia, $this->categoriesMobiliario));
        $response['assignProductIds'] = [];
        $response['newIndex'] = 0;
        $i=0;

        // Categories Standar.
        if (!empty($allCategories)) {
            $collection = $this->productCollectionFactory->create()
                ->addFieldToSelect('*')
                ->addCategoriesFilter(['in' => $allCategories])
                ->load();
            $allProductIds = $collection->getAllIds();
            if (!empty($allProductIds)) {
                $assignProducts = $this->assignItems->create()
                    ->getCollection()
                    ->addFieldToFilter('product_id', ['in' => [$allProductIds]]);
                $assignProducts->getSelect()->group('product_id')->order('price DESC');

                foreach ($assignProducts as $assignProductItem) {
                    if (in_array($this->_agreementHelper->getAgreementId(), [
                            InsumosConstants::ID_AGREEMENT,
                            EmergenciasRenewalConstants::ID_AGREEMENT_EMERGENCY_2021_09,
                            \Linets\AseoRenewalSetup\Model\AseoRenewalConstants::ID_AGREEMENT
                    ])) {
                        $associateAssignProducts = $this->getMinPriceSupplierIdInsumos($assignProductItem->getProductId(), $assignProductItem->getType(), $percentage);
                    } else {
                        //Recorro todos los productos y obtengo el id del precio más bajo por proveedor
                        $associateAssignProducts = $this->getMinPriceSupplierId($assignProductItem->getProductId(), $assignProductItem->getType(), $percentage);
                    }
                    foreach ($associateAssignProducts as $associateAssignProductItem) {
                        if(is_array($associateAssignProductItem)){
                            $response['assignProductIds'][$i]['id'] = $associateAssignProductItem['id'];
                            $response['assignProductIds'][$i]['product_id'] = $associateAssignProductItem['product_id'];
                            $response['assignProductIds'][$i]['type'] = $associateAssignProductItem['type'];
                            $response['assignProductIds'][$i]['seller_id'] = $associateAssignProductItem['seller_id'];
                        }
                        $i++;
                    }
                }
            }
        }

        // Categories Emergencias.
        if (!empty($this->categoriesEmergencia)){
            $collection = $this->productCollectionFactory->create()
                ->addFieldToSelect('*')
                ->addCategoriesFilter(['in' => $this->categoriesEmergencia])
                ->load();
            $allProductIds = $collection->getAllIds();
            if (!empty($allProductIds)) {
                $assignProducts = $this->assignItems->create()
                    ->getCollection()
                    ->addFieldToFilter('product_id', ['in' => [$allProductIds]]);
                $assignProducts->getSelect()->group('product_id')->order('base_price DESC');
                foreach ($assignProducts as $assignProductItem) {
                    //Recorro todos los productos y obtengo el id del precio más bajo por proveedor
                    $associateAssignProducts = $this->getMinPriceSupplierIdForEmergencia($assignProductItem->getProductId(), $assignProductItem->getType(), $percentage);
                    foreach ($associateAssignProducts as $associateAssignProductItem) {
                        if(is_array($associateAssignProductItem)){
                            $response['assignProductIds'][$i]['id'] = $associateAssignProductItem['id'];
                            $response['assignProductIds'][$i]['product_id'] = $associateAssignProductItem['product_id'];
                            $response['assignProductIds'][$i]['type'] = $associateAssignProductItem['type'];
                            $response['assignProductIds'][$i]['seller_id'] = $associateAssignProductItem['seller_id'];
                        }
                        $i++;
                    }
                }
            }
        }
        // Categories Mobiliario.
        if (!empty($this->categoriesMobiliario)){
            /*
             * begin check this changes
             * - porque se matchea el producto con las ofertas y se los ordena si siempre se analizan todos los
             * productos de oforma individual? Esto es un posible cambio alternativo.
             *
            $productCollection = $this->productCollectionFactory->create()
                ->addFieldToSelect('entity_id')
                ->addFieldToSelect('type_id')
                ->addCategoriesFilter(['in' => $this->categoriesMobiliario]);

            $productList = $productCollection->getItems();

            foreach ($productList as $productItem) {
                //Recorro todos los productos y obtengo el id del precio más bajo por proveedor
                $associateAssignProducts = $this->getMinPriceSupplierIdForBasePrice($productItem->getEntityId(), $productItem->getTypeId(), $percentage);
                foreach ($associateAssignProducts as $associateAssignProductItem) {
                    if(is_array($associateAssignProductItem)){
                        $response['assignProductIds'][$i]['id'] = $associateAssignProductItem['id'];
                        $response['assignProductIds'][$i]['product_id'] = $associateAssignProductItem['product_id'];
                        $response['assignProductIds'][$i]['type'] = $associateAssignProductItem['type'];
                        $response['assignProductIds'][$i]['seller_id'] = $associateAssignProductItem['seller_id'];
                    }
                    $i++;
                }
            }
            * fin check this changes
            */

            //---------
            $collection = $this->productCollectionFactory->create()
                ->addFieldToSelect('*')
                ->addCategoriesFilter(['in' => $this->categoriesMobiliario])
                ->load();
            //
            //para este website se buscan todos los linkeados de las cat
            //$skuList = $collection->getFirstItem()->getSku();
            //foreach ($collection as $product){
            //    if (!empty($product->getLinkedProducts())){
            //        $skuList = $skuList .','.$product->getLinkedProducts();
            //    }else{
            //        $skuList = $skuList .','.$product->getSku();
            //    }
            //}
            //$collectionWithLinkeds = $this->productCollectionFactory->create()
            //    ->addFieldToSelect('id')
            //    ->addFieldToFilter('sku', ['in' => $skuList])
            //    ->load();
            //$allProductIds = $collectionWithLinkeds->getAllIds();

            $allProductIds = $collection->getAllIds();
            if (!empty($allProductIds)) {
                $assignProducts = $this->assignItems->create()
                    ->getCollection()
                    ->addFieldToFilter('product_id', ['in' => [$allProductIds]]);
                $assignProducts->getSelect()->group('product_id')->order('base_price DESC');
                foreach ($assignProducts as $assignProductItem) {
                    //Recorro todos los productos y obtengo el id del precio más bajo por proveedor
                    $associateAssignProducts = $this->getMinPriceSupplierIdForBasePrice($assignProductItem->getProductId(), $assignProductItem->getType(), $percentage);
                    foreach ($associateAssignProducts as $associateAssignProductItem) {
                        if(is_array($associateAssignProductItem)){
                            $response['assignProductIds'][$i]['id'] = $associateAssignProductItem['id'];
                            $response['assignProductIds'][$i]['product_id'] = $associateAssignProductItem['product_id'];
                            $response['assignProductIds'][$i]['type'] = $associateAssignProductItem['type'];
                            $response['assignProductIds'][$i]['seller_id'] = $associateAssignProductItem['seller_id'];
                        }
                        $i++;
                    }
                }
            }
        }

        $products = $this->multiUnique($response['assignProductIds']);
        $response['assignProductIds']=array();
        $i=0;
        foreach ($products as $key => $productId) {
            $response['assignProductIds'][$i]['id']=$productId['id'];
            $response['assignProductIds'][$i]['product_id']=$productId['product_id'];
            $response['assignProductIds'][$i]['type']=$productId['type'];
            $response['assignProductIds'][$i]['seller_id']=$productId['seller_id'];
            $i++;
        }

            $response = $this->createDispersionHistory($categories, $response);
        }

        return $response;
    }

    public function createDispersionHistory($categories, $response)
    {
        $productCount = count($response['assignProductIds']);

        $dispersionHistory = $this->_dispersionHistoryFactory->create();
        $dispersionHistory->setCategories($categories);
        $allCategories = explode(',', $response['allCategories']);
        $dispersionHistory->setAllCategories($allCategories);
        $dispersionHistory->setPricePercentage($response['pricePercentage']);
        $dispersionHistory->setFirstAssignProductIds($response['assignProductIds']);
        $dispersionHistory->setAssignProductIds($response['assignProductIds']);
        $dispersionHistory->setProductCountTotal($productCount);
        $dispersionHistory->setProductCountActual(0);
        $dispersionHistory->setStatus(DispersionHistory::STATUS_IN_PROGRESS);

        $this->_dispersionHistoryRepository->save($dispersionHistory);

        $response['dispersionHistoryId'] = $dispersionHistory->getId();
        return $response;
    }

    public function getNotCompletedDispersionHistory()
    {
        $filter = $this->_filter->setField('status')->setValue(DispersionHistory::STATUS_INCOMPLETED);
        $filterGroups = $this->_filterGroup->setFilters([$filter]);
        $searchCriteria = $this->_searchCriteria->setFilterGroups([$filterGroups])->setPageSize(1)->setCurrentPage(1);
        return $this->_dispersionHistoryRepository->getList($searchCriteria)->getItems();
    }

    public function getInProgressDispersionHistory()
    {
        $filter = $this->_filter->setField('status')->setValue(DispersionHistory::STATUS_IN_PROGRESS);
        $filterGroups = $this->_filterGroup->setFilters([$filter]);
        $searchCriteria = $this->_searchCriteria->setFilterGroups([$filterGroups]);
        return $this->_dispersionHistoryRepository->getList($searchCriteria)->getItems();
    }

    public function updateDispersionHistoryStatus($dispersionHistoryId, $status)
    {
        $dispersionHistory = $this->_dispersionHistoryRepository->getById($dispersionHistoryId);
        $dispersionHistory->setStatus($status);
        $this->_dispersionHistoryRepository->save($dispersionHistory);
    }

    public function updateLog($productId, $itemPrice, $sellerId, $pricePercentage, $originalPrice, $allCategories)
    {
        $date = $this->date->gmtDate();
        $allCategoriesString = implode(',', $allCategories);
        $data = [
            'magecategory_id'      => $allCategoriesString,
            'mageproduct_id'       => $productId,
            'mageproduct_price'    => $originalPrice,
            'updatedproduct_price' => $itemPrice,
            'update_percentage'    => $pricePercentage,
            'created_at'           => $date,
            'updated_at'           => $date,
            'seller_id'            => $sellerId
        ];
        $this->resourceConnection->getConnection()->insert(
            $this->resourceConnection->getTableName('price_dispersion'),
            $data
        );
    }

    public function getBaseCategoryEmergencia(){
        if (!$this->baseCategoryEmergencia){
            $collection = $this->categoryCollectionFactory
                ->create()
                ->addAttributeToFilter('name','Emergencia - Default Category')
                ->setPageSize(1);

            if ($collection->getSize()) {
                $this->baseCategoryEmergencia =  $collection->getFirstItem()->getId();
            }
        }
        return  $this->baseCategoryEmergencia;

    }

    public function getBaseCategoryMobiliario(){
        if (!$this->baseCategoryMobiliario){
            $collection = $this->categoryCollectionFactory
                ->create()
                ->addAttributeToFilter('name','Mobiliario - Default Category')
                ->setPageSize(1);

            if ($collection->getSize()) {
                $this->baseCategoryMobiliario =  $collection->getFirstItem()->getId();
            }
        }
        return  $this->baseCategoryMobiliario;

    }

    public function getAllCategories($cat)
    {
        $allCategories = [];
        $this->categoriesEmergencia = [];
        $this->categoriesMobiliario = [];

        $baseCategoryEmergencia = $this->getBaseCategoryEmergencia();
        $baseCategoryMobiliario = $this->getBaseCategoryMobiliario();

        $agreementId = $this->_agreementHelper->getAgreementId();

        foreach ($cat as $catIds) {
            $categorySelected = $this->categoryRepository->get($catIds);

            if ($agreementId == \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_EMERGENCY) {
                if ($catIds == $baseCategoryEmergencia || strpos( $categorySelected->getPath(), '/'.$baseCategoryEmergencia.'/') !== false){
                    array_push($this->categoriesEmergencia, $catIds);
                    $categories = $this->categoryCollectionFactory->create()
                        ->addAttributeToSelect('*')
                        ->addFieldToFilter('path', ["like" => '%/'.$catIds.'/%']);
                    foreach ($categories as $category) {
                        array_push($this->categoriesEmergencia, $category->getId());
                    }
                }
            } elseif ($agreementId == \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_FURNITURE) {
                if ($catIds == $baseCategoryMobiliario || strpos( $categorySelected->getPath(), '/'.$baseCategoryMobiliario.'/') !== false){
                    array_push($this->categoriesMobiliario, $catIds);
                    $categories = $this->categoryCollectionFactory->create()
                        ->addAttributeToSelect('*')
                        ->addFieldToFilter('path', ["like" => '%/'.$catIds.'/%']);
                    foreach ($categories as $category) {
                        array_push($this->categoriesMobiliario, $category->getId());
                    }
                }
            } else {
                array_push($allCategories, $catIds);
                $categories = $this->categoryCollectionFactory->create()
                    ->addAttributeToSelect('*')
                    ->addFieldToFilter('path', ["like" => '%/'.$catIds.'/%']);
                foreach ($categories as $category) {
                    array_push($allCategories, $category->getId());
                }
            }
        }

        return $allCategories;
    }

    public function getMinPriceSupplierIdForEmergencia($productId, $type, $percentage){

        $price_p='';
        $percentage=round($percentage);
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from(['products' => 'marketplace_assignproduct_items'], ['id', 'product_id', 'type', 'price', 'base_price', 'seller_id', 'dis_dispersion']);
        $select->join(
            ['sellers' => 'marketplace_userdata'],
            'products.seller_id = sellers.seller_id',
            ['*']
        );
        $select->where('products.product_id = '. $productId);
        $select->where('products.type = "'. $type.'"');
        $select->where('products.status = 1');
        $select->where('products.seller_id != 0');
        /** MPMT-17 En la dispersión de precios, se debe deshabilitar también las ofertas sin stock */
        //$select->where('products.qty != 0');
        $select->where('sellers.is_seller != 0');
        $select->order('products.base_price','asc');
        $result_prices=$resourceConnection->fetchAssoc($select);
        $ids_toremove[]='';

        if (is_array($result_prices)) {
            reset($result_prices);
            //Fetch the key from the current element.
            $minPrice_id = key($result_prices);
            foreach ($result_prices as $key => $product) {
                if($type == 'configurable'){
                    $simples_configurable = $resourceConnection->select();
                    $simples_configurable->from('marketplace_assignproduct_associated_products', ['id', 'product_id', 'parent_product_id', 'base_price']);
                    $simples_configurable->where('parent_product_id = '. $productId);
                    $simples_configurable->order ('base_price','asc');
                    $result_simples_configurables=$resourceConnection->fetchAssoc($simples_configurable);
                    if (is_array($result_simples_configurables)) {
                        reset($result_simples_configurables);
                        //Fetch the key from the current element.
                        $minPriceConf_id = key($result_simples_configurables);
                        foreach ($result_simples_configurables as $configurable_key => $configurable_product) {
                            if (isset($result_simples_configurables[$minPriceConf_id]['base_price']) && $result_simples_configurables[$minPriceConf_id]['base_price'] > 0) {
                                $minPriceConf = $result_simples_configurables[$minPriceConf_id]['base_price'];
                                $priceProductConf = $configurable_product['base_price'];
                                $percentageProductConf = (($priceProductConf*100)/$minPriceConf)-100;
                                $result_simples_configurables[$configurable_key]['mayor']=1;
                                if ($percentageProductConf>$percentage) {
                                    $ids_toremove[$configurable_key]['id']=$configurable_product['id'];
                                    $ids_toremove[$configurable_key]['product_id']=$configurable_product['product_id'];
                                    $ids_toremove[$configurable_key]['type']=$type;
                                    $ids_toremove[$configurable_key]['seller_id']=$product['seller_id'];
                                    unset($result_simples_configurables[$configurable_key]);
                                }
                            }

                        }
                    }

                }
                if (isset($result_prices[$minPrice_id]['base_price']) && $result_prices[$minPrice_id]['base_price'] > 0) {
                    $minPrice = $result_prices[$minPrice_id]['base_price'];
                    $priceProduct = $product['base_price'];
                    $percentageProduct = (($priceProduct*100)/$minPrice)-100;
                    $result_prices[$key]['mayor']=1;
                    if ($percentageProduct>$percentage) {
                        $ids_toremove[$key]['id']=$product['id'];
                        $ids_toremove[$key]['product_id']=$product['product_id'];
                        $ids_toremove[$key]['type']=$type;
                        $ids_toremove[$key]['seller_id']=$product['seller_id'];
                        unset($result_prices[$key]);
                    }
                }
            }
        }

        return $ids_toremove;
    }

    public function getMinPriceSupplierIdForBasePrice($productId, $type, $percentage){
        $percentage=round($percentage);

        //----------- Codigo viejo, con error de ID en MpAssignProduct-----------
        //$tableSellers = $this->resourceConnection->getTableName('marketplace_userdata');
        //$assignProductsCollection = $this->assignItems->create()->getCollection();
        //$assignProductsCollection->addFieldToSelect(['id', 'product_id', 'type', 'price', 'base_price', 'seller_id', 'dis_dispersion'])
        //    ->addFieldToFilter('main_table.product_id', ['eq' => $productId])
        //    ->addFieldToFilter('main_table.type', ['eq' => $type])
        //    ->addFieldToFilter('main_table.status', ['eq' => 1])
        //    ->addFieldToFilter('main_table.seller_id', ['neq' => 0]);
        //$condition = [
        //    "main_table.seller_id = sellers.seller_id",
        //    "sellers.is_seller != 0"
        //];
        //$assignProductsCollection->getSelect()->joinInner(
        //    ['sellers' => $tableSellers],
        //    implode(' AND ', $condition),
        //    [])
        //    ->order('main_table.base_price','asc');
        //
        //$result_prices = $assignProductsCollection->getItems();
        //$idsToRemove[]='';
        //-------------------------------------------------------------------------

        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from(['products' => 'marketplace_assignproduct_items'], ['id', 'product_id', 'type', 'price','base_price', 'seller_id', 'dis_dispersion']);
        $select->join(
            ['sellers' => 'marketplace_userdata'],
            'products.seller_id = sellers.seller_id',
            ['*']
        );
        $select->where('products.product_id = '. $productId);
        $select->where('products.type = "'. $type.'"');
        $select->where('products.status = 1');
        $select->where('products.seller_id != 0');
        $select->where('products.qty != 0');
        $select->where('sellers.is_seller != 0');
        $select->order('products.base_price','asc');
        $result_prices=$resourceConnection->fetchAssoc($select);
        $idsToRemove[]='';

        if (is_array($result_prices)) {
            reset($result_prices);
            //Fetch the key from the current element.
            $minPriceId = key($result_prices);
            foreach ($result_prices as $key => $product) {
                //Codigo no probado porque no se posee productos configurables cargados.
                if($type == 'configurable'){
                    $simplesConfigurableCollection = $this->assignAssociates->create()->getCollection();
                    $simplesConfigurableCollection->addFieldToFilter('parent_product_id', ['eq' => $productId])
                        ->addOrder('base_price','asc');
                    $resultSimplesConfigurables=$simplesConfigurableCollection->getItems();
                    if (is_array($resultSimplesConfigurables)) {
                        reset($resultSimplesConfigurables);
                        //Fetch the key from the current element.
                        $minPriceConfId = key($resultSimplesConfigurables);
                        foreach ($resultSimplesConfigurables as $configurableKey => $configurableProduct) {
                            if (isset($resultSimplesConfigurables[$minPriceConfId]['base_price']) && $resultSimplesConfigurables[$minPriceConfId]['base_price'] > 0) {
                                $minPriceConf = $resultSimplesConfigurables[$minPriceConfId]['base_price'];
                                $priceProductConf = $configurableProduct['base_price'];
                                $percentageProductConf = (($priceProductConf*100)/$minPriceConf)-100;
                                $resultSimplesConfigurables[$configurableKey]['mayor']=1;
                                if ($percentageProductConf>$percentage) {
                                    $idsToRemove[$configurableKey]['id']=$configurableProduct['id'];
                                    $idsToRemove[$configurableKey]['product_id']=$configurableProduct['product_id'];
                                    $idsToRemove[$configurableKey]['type']=$type;
                                    $idsToRemove[$configurableKey]['seller_id']=$product['seller_id'];
                                    unset($resultSimplesConfigurables[$configurableKey]);
                                }
                            }

                        }
                    }
                }
                if (isset($result_prices[$minPriceId]['base_price']) && $result_prices[$minPriceId]['base_price'] > 0) {
                    $minPrice = $result_prices[$minPriceId]['base_price'];
                    $priceProduct = $product['base_price'];
                    $percentageProduct = (($priceProduct*100)/$minPrice)-100;
                    $result_prices[$key]['mayor']=1;
                    if ($percentageProduct>$percentage) {
                        $idsToRemove[$key]['id']=$product['id'];
                        $idsToRemove[$key]['product_id']=$product['product_id'];
                        $idsToRemove[$key]['type']=$type;
                        $idsToRemove[$key]['seller_id']=$product['seller_id'];
                        unset($result_prices[$key]);
                    }
                }
            }
        }
        return $idsToRemove;
    }

    public function getMinPriceSupplierId($productId, $type, $percentage)
    {
        $price_p='';
        $percentage=round($percentage);
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from(['products' => 'marketplace_assignproduct_items'], ['id', 'product_id', 'type', 'price', 'seller_id', 'dis_dispersion']);
        $select->join(
                ['sellers' => 'marketplace_userdata'],
                'products.seller_id = sellers.seller_id',
                ['*']
            );
        $select->where('products.product_id = '. $productId);
        $select->where('products.type = "'. $type.'"');
        $select->where('products.seller_id != 0');
        if ($type != 'configurable') {
            $select->where('products.status = 1');
            $select->where('products.qty != 0');
        }
        $select->where('sellers.is_seller != 0');
        $select->order('products.price','asc');
        $result_prices=$resourceConnection->fetchAssoc($select);
	$ids_toremove[]='';

        if (is_array($result_prices)) {
            reset($result_prices);
            //Fetch the key from the current element.
            $minPrice_id = key($result_prices);
            foreach ($result_prices as $key => $product) {
                //si es producto configurable -> price = 0
                if (isset($result_prices[$minPrice_id]['price']) && $result_prices[$minPrice_id]['price'] > 0) {
                    $minPrice = $result_prices[$minPrice_id]['price'];
                    $priceProduct = $product['price'];
                    $percentageProduct = (($priceProduct*100)/$minPrice)-100;
                    $result_prices[$key]['mayor']=1;
                    if ($percentageProduct>$percentage) {
                        $ids_toremove[$key]['id']=$product['id'];
                        $ids_toremove[$key]['product_id']=$product['product_id'];
                        $ids_toremove[$key]['type']=$type;
                        $ids_toremove[$key]['seller_id']=$product['seller_id'];
                        unset($result_prices[$key]);
                    }
                }
            }
            if ($type == "configurable"){
                //obtengo la lista de todos los minimos de entre todas las ofertas de los hijos configurables.
                $queryMinPriceChilds = $resourceConnection->select();
                $queryMinPriceChilds->from('marketplace_assignproduct_associated_products', ['product_id', 'parent_product_id',  'min(price) as price']);
                $queryMinPriceChilds->where('parent_product_id = '. $productId);
                $queryMinPriceChilds->where('status = 1');
                $queryMinPriceChilds->where('qty != 0');
                $queryMinPriceChilds->group('product_id');
                $minPriceChilds = $resourceConnection->fetchAssoc($queryMinPriceChilds);

                //obtengo todas las ofertas hijo del producto que estoy analizando.
                $simples_configurable = $resourceConnection->select();
                $simples_configurable->from('marketplace_assignproduct_associated_products', ['id', 'product_id', 'parent_product_id', 'parent_id', 'price']);
                $simples_configurable->where('parent_product_id = '. $productId);
                $simples_configurable->where('status = 1');
                $simples_configurable->where('qty != 0');
                $simples_configurable->order ('price','asc');
                $result_simples_configurables=$resourceConnection->fetchAssoc($simples_configurable);
                if (is_array($result_simples_configurables)) {
                    foreach ($result_simples_configurables as $configurable_key => $configurable_product) {
                        if(isset($minPriceChilds[$configurable_product['product_id']])
                            && isset($minPriceChilds[$configurable_product['product_id']]['price'])
                            && $minPriceChilds[$configurable_product['product_id']]['price'] > 0){
                            $minPriceConf = $minPriceChilds[$configurable_product['product_id']]['price'];
                            $priceProductConf = $configurable_product['price'];
                            $percentageProductConf = (($priceProductConf*100)/$minPriceConf)-100;
                            $result_simples_configurables[$configurable_key]['mayor']=1;
                            if ($percentageProductConf > $percentage && isset($result_prices[$configurable_product['parent_id']])) {
                                $ids_toremove[$configurable_key]['id']=$configurable_product['id'];
                                $ids_toremove[$configurable_key]['product_id']=$configurable_product['product_id'];
                                $ids_toremove[$configurable_key]['type']=$type;
                                $ids_toremove[$configurable_key]['seller_id']=$result_prices[$configurable_product['parent_id']]['seller_id'];
                            }
                        }
                    }
                }
            }
        }

        return $ids_toremove;
    }

    public function getMinPriceSupplierIdInsumos($productId, $type, $percentage)
    {
        $price_p='';
        $percentage=round($percentage);
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from(['products' => 'marketplace_assignproduct_items'], ['id', 'product_id', 'type', 'price', 'seller_id', 'dis_dispersion']);
        $select->join(
                ['sellers' => 'marketplace_userdata'],
                'products.seller_id = sellers.seller_id',
                ['*']
            );
        $select->where('products.product_id = '. $productId);
        $select->where('products.type = "'. $type.'"');
        $select->where('products.seller_id != 0');
        if ($type != 'configurable') {
            $select->where('products.status = 1');
            $select->where('products.qty != 0');
        }
        $select->where('sellers.is_seller != 0');
        $select->order('products.price','asc');
        $result_prices=$resourceConnection->fetchAssoc($select);
	$ids_toremove[]='';

        if (is_array($result_prices)) {
            reset($result_prices);
            //Fetch the key from the current element.
            $minPrice_id = key($result_prices);
            foreach ($result_prices as $key => $product) {
                //si es producto configurable -> price = 0
                if (isset($result_prices[$minPrice_id]['price']) && $result_prices[$minPrice_id]['price'] > 0) {
                    $minPrice = $result_prices[$minPrice_id]['price'];
                    $priceProduct = $product['price'];
                    $percentageProduct = (($priceProduct*100)/$minPrice)-100;
                    $result_prices[$key]['mayor']=1;
                    if ($percentageProduct>$percentage) {
                        $ids_toremove[$key]['id']=$product['id'];
                        $ids_toremove[$key]['product_id']=$product['product_id'];
                        $ids_toremove[$key]['type']=$type;
                        $ids_toremove[$key]['seller_id']=$product['seller_id'];
                        unset($result_prices[$key]);
                    }
                }
            }
            if ($type == "configurable"){
                //obtengo todas las ofertas hijo del producto que estoy analizando.
                $simples_configurable = $resourceConnection->select();
                $simples_configurable->from('marketplace_assignproduct_associated_products', ['id', 'product_id', 'parent_product_id', 'parent_id', 'price']);
                $simples_configurable->where('parent_product_id = '. $productId);
                $simples_configurable->where('status = 1');
                $simples_configurable->where('qty != 0'); //error
                $simples_configurable->order ('price','asc');
                $result_simples_configurables=$resourceConnection->fetchAssoc($simples_configurable);

                $validOffersIds = [];
                foreach ($result_simples_configurables as $key => $configurableProduct) {
                    $parentId = $configurableProduct['parent_id'];
                    $sellerIdSelect = $resourceConnection->select();
                    $sellerIdSelect->from('marketplace_assignproduct_items', ['seller_id'])
                        ->where('id = '. $parentId);
                    $sellerId = $resourceConnection->fetchOne($sellerIdSelect);
                    $sellerStatus = $this->getSellerStatus($sellerId);
                    if ($sellerStatus == '70') {
                        $validOffersIds[] = $configurableProduct['id'];
                    } else {
                        unset($result_simples_configurables[$key]);
                    }
                }
                foreach ($result_simples_configurables as $key => $configurableProduct) {
                    $hasOffer = $this->helperOffer->checkForSpecificSpecialOfferOnAssociate($configurableProduct['product_id'], $configurableProduct['parent_id']);
                    if ($hasOffer['has_offer']) {
                        $result_simples_configurables[$key]['price'] = $hasOffer['price'];
                    }
                }
                //obtengo la lista de todos los minimos de entre todas las ofertas de los hijos configurables.
                $minPriceChilds = $this->getMinPricesForInsumos($productId, $validOffersIds);
                if (is_array($result_simples_configurables)) {
                    foreach ($result_simples_configurables as $configurable_key => $configurable_product) {
                        if(isset($minPriceChilds[$configurable_product['product_id']])
                            && isset($minPriceChilds[$configurable_product['product_id']]['price'])
                            && $minPriceChilds[$configurable_product['product_id']]['price'] > 0){
                            $minPriceConf = $minPriceChilds[$configurable_product['product_id']]['price'];
                            $priceProductConf = $configurable_product['price'];
                            $percentageProductConf = (($priceProductConf*100)/$minPriceConf)-100;
                            $result_simples_configurables[$configurable_key]['mayor']=1;
                            if ($percentageProductConf > $percentage && isset($result_prices[$configurable_product['parent_id']])) {
                                $ids_toremove[$configurable_key]['id']=$configurable_product['id'];
                                $ids_toremove[$configurable_key]['product_id']=$configurable_product['product_id'];
                                $ids_toremove[$configurable_key]['type']=$type;
                                $ids_toremove[$configurable_key]['seller_id']=$result_prices[$configurable_product['parent_id']]['seller_id'];
                            }
                        }
                    }
                }
            }
        }

        return $ids_toremove;
    }

    private function getMinPricesForInsumos($parentProductId, $validOffersIds)
    {
        $minPricesArray = [];
        if(!empty($validOffersIds)) {
            $resourceConnection = $this->resourceConnection->getConnection();
            $productIdsSelect = $resourceConnection->select();
            $productIdsSelect->from('marketplace_assignproduct_associated_products', ['product_id']);
            $productIdsSelect->where('parent_product_id = '. $parentProductId);
            $productIdsSelect->where('id IN (' . implode(',', $validOffersIds) . ')');
            $productIdsSelect->group('product_id');
            $productIds = $resourceConnection->fetchAssoc($productIdsSelect);
            foreach ($productIds as $productId) {
                $minPrice = $this->getMinPricesByProductId($productId['product_id'], $validOffersIds);
                $minPricesArray[$productId['product_id']] = $minPrice;
            }
        }

        return $minPricesArray;
    }

    private function getMinPricesByProductId($productId, $validOffersIds)
    {
        $resourceConnection = $this->resourceConnection->getConnection();
        $queryMinPriceChilds = $resourceConnection->select();
        $queryMinPriceChilds->from('marketplace_assignproduct_associated_products', ['product_id', 'parent_product_id',  'price', 'parent_id']);
        $queryMinPriceChilds->where('product_id = '. $productId);
        $queryMinPriceChilds->where('status = 1');
        $queryMinPriceChilds->where('qty != 0');
        $queryMinPriceChilds->where('id IN (' . implode(',', $validOffersIds) . ')');
        $minPriceChilds = $resourceConnection->fetchAll($queryMinPriceChilds);
        $minPrice = null;
        $minPriceData = [];
        foreach ($minPriceChilds as $minPriceChild) {
            $hasOffer = $this->helperOffer->checkForSpecificSpecialOfferOnAssociate($minPriceChild['product_id'], $minPriceChild['parent_id']);
            if ($hasOffer['has_offer']) {
                $minPriceChild['price'] = $hasOffer['price'];
            }
            if ($minPrice === null) {
                $minPrice = (float) $minPriceChild['price'];
                $minPriceData = $minPriceChild;
            } elseif ($minPrice > (float) $minPriceChild['price']) {
                $minPrice = (float) $minPriceChild['price'];
                $minPriceData = $minPriceChild;
            }
        }

        return $minPriceData;
    }

    public function array_sort($array, $on, $order=SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    public function multiUnique($src)
    {
        $output = array_map("unserialize",
         array_unique(array_map("serialize", $src)));
        return $output;
    }

    /**
     * Retrieve website ID
     *
     * @return int
     */
    public function getWebsiteCode()
    {
        return $this->storeManager->getWebsite()->getCode();
    }

    /**
     * @param int $sellerId
     * @return int
     */
    public function getSellerStatus($sellerId)
    {
        if (!isset($this->sellerStatuses[$sellerId])) {
            $seller = $this->_customerRepositoryInterface->getById($sellerId);
            $sellerStatus = $seller->getCustomAttribute('wkv_dccp_state_details') ?
                $seller->getCustomAttribute('wkv_dccp_state_details')->getValue() : '';
            $this->sellerStatuses[$sellerId] = $sellerStatus;
        }

        return $this->sellerStatuses[$sellerId];
    }
}
