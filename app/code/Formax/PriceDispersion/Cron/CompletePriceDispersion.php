<?php

namespace Formax\PriceDispersion\Cron;

use Formax\PriceDispersion\Model\DispersionHistory;
use Formax\PriceDispersion\Model\Repository\DispersionHistoryRepository;
use Psr\Log\LoggerInterface;
use Formax\PriceDispersion\Helper\Data as Helper;
use Webkul\MpAssignProduct\Model\AssociatesFactory as AssociatesFactory;
use Webkul\MpAssignProduct\Model\ItemsFactory as AssignItemsFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollectionFactory;

class CompletePriceDispersion
{
    /** @var Helper */
    protected $helper;

    /** @var DispersionHistoryRepository */
    protected $_dispersionHistoryRepository;

    /** @var AssignItemsFactory */
    protected $_assignItems;

    /** @var AssociatesCollectionFactory */
    protected $_associatesCollection;

    /** @var AssociatesFactory */
    protected $_assignAssociates;

    /** @var LoggerInterface */
    protected $_logger;

    public function __construct(
        Helper $helper,
        DispersionHistoryRepository $dispersionHistoryRepository,
        AssignItemsFactory $assignItems,
        AssociatesFactory $assignAssociates,
        AssociatesCollectionFactory $associatesCollection,
        LoggerInterface $logger
    ) {
        $this->helper = $helper;
        $this->_dispersionHistoryRepository = $dispersionHistoryRepository;
        $this->_assignItems = $assignItems;
        $this->_associatesCollection = $associatesCollection;
        $this->_assignAssociates = $assignAssociates;
        $this->_logger = $logger;
    }

    public function execute()
    {
        // get not completed dispersion history
        $notCompletedDispersionHistory = $this->helper->getNotCompletedDispersionHistory();

        foreach ($notCompletedDispersionHistory as $dispersionHistory) {

            $pricePercentage = $dispersionHistory->getPricePercentage();
            $allCategories = $dispersionHistory->getAllCategories();
            $assignProductIds = $dispersionHistory->getAssignProductIds();
            $productCountActual = $dispersionHistory->getProductCountActual();
            $productCountTotal = $dispersionHistory->getProductCountTotal();
            $dispersionHistoryId = $dispersionHistory->getEntityId();

            $this->_logger->info('Completing dispersion history ID:'. $dispersionHistoryId);
            $this->_logger->info('Product count total: ' . $productCountTotal);
            $this->_logger->info('Product count actual: ' . $productCountActual);

            $this->_logger->info('Start updating assign products');

            // set status of dispersion history to "Processing"
            $dispersionHistory->setStatus(DispersionHistory::STATUS_IN_PROGRESS);
            foreach ($assignProductIds as $key => $value) {
                $assignItem = $this->_assignItems->create()->load($value['id']);

                if ($value['type'] == 'simple') {
                    $assignItem->setStatus('3');
                    $assignItem->setQty('0');
                    $assignItem->setDisDispersion('1');
                    $assignItem->save();

                    $countConfigurable = $this->_associatesCollection->create()
                        ->addFieldToFilter('parent_product_id', ['eq' => $value['id']])
                        ->getSize();
                    if ($countConfigurable) {
                        $countConfigurableDis = $this->_associatesCollection->create()
                            ->addFieldToFilter('parent_product_id', ['eq' => $value['id']])
                            ->addFieldToFilter('dis_dispersion', ['eq' => 1])
                            ->getSize();
                        $associateItem = $this->_assignAssociates->create()->load($value['id']);
                        if ($countConfigurable == $countConfigurableDis) {
                            // qty = 0, dis_dispersion = 1
                            $associateItem->setQty('0');
                            $associateItem->setDisDispersion('1');
                        } else {
                            //qty = 9999, dis_dispersion = 0
                            $associateItem->setQty('9999');
                            $associateItem->setDisDispersion('0');
                        }
                        $associateItem->save();
                    }
                } elseif ($value['type'] == 'configurable') {
                    $associateItem = $this->_assignAssociates->create()->load($value['id']);
                    // qty = 0, dis_dispersion = 1
                    $associateItem->setQty('0');
                    $associateItem->setDisDispersion('1');
                    $associateItem->save();
                }
                $this->_logger->info('Updated assign product ID: ' . $value['id']);

                // remove item from assignProductIds
                unset($assignProductIds[$key]);
                $dispersionHistory->setAssignProductIds($assignProductIds);
                // update productCountActual
                $productCountActual++;
                $dispersionHistory->setProductCountActual($productCountActual);
                $this->_logger->info('Updated ' . $productCountActual . ' of ' . $productCountTotal);
                // save changes
                $this->_dispersionHistoryRepository->save($dispersionHistory);

                // log changes into price_dispersion table
                $assignItemPrice = $assignItem->getPrice();
                $this->helper->updateLog($value['product_id'], $assignItemPrice, $value['seller_id'], $pricePercentage, $assignItemPrice, $allCategories);
            }
            $this->_logger->info('Finished updating assign products');
            // update dispersionHistory status to completed
            $dispersionHistory->setStatus(DispersionHistory::STATUS_COMPLETED);
            $this->_dispersionHistoryRepository->save($dispersionHistory);
            $this->_logger->info('Completed dispersion history ID:'. $dispersionHistoryId);
        }
    }
}
