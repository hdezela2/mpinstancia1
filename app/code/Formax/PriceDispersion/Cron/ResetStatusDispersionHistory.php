<?php

namespace Formax\PriceDispersion\Cron;

use Formax\PriceDispersion\Model\DispersionHistory;
use Formax\PriceDispersion\Model\Repository\DispersionHistoryRepository;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Psr\Log\LoggerInterface;
use Formax\PriceDispersion\Helper\Data as Helper;

class ResetStatusDispersionHistory
{
    /** @var Helper */
    protected $helper;

    /** @var DispersionHistoryRepository */
    protected $_dispersionHistoryRepository;

    /** @var DateTime */
    protected $date;

    /** @var LoggerInterface */
    protected $_logger;

    public function __construct(
        Helper $helper,
        DispersionHistoryRepository $dispersionHistoryRepository,
        DateTime $date,
        LoggerInterface $logger
    ) {
        $this->helper = $helper;
        $this->_dispersionHistoryRepository = $dispersionHistoryRepository;
        $this->date = $date;
        $this->_logger = $logger;
    }

    public function execute()
    {
        // get in progress dispersion history that were not updated on the last 5 minutes
        $inProgressDispersionHistory = $this->helper->getInProgressDispersionHistory();

        foreach ($inProgressDispersionHistory as $dispersionHistory) {

            $dispersionHistoryLastUpdated = $dispersionHistory->getUpdatedAt()->format("Y-m-d H:i:s");
            $date1 = strtotime($dispersionHistoryLastUpdated);
            $dispersionHistoryId = $dispersionHistory->getEntityId();

            $currentDate = $this->date->gmtDate("Y-m-d H:i:s");
            $date2 = strtotime($currentDate);
            $diff = abs($date2 - $date1);

            // If last updated is grater than 5 minutes ago, update the status to uncompleted
            if ($diff > 300) {
                $this->_logger->info('Checking dispersion history ID:'. $dispersionHistoryId);
                $this->_logger->info('Current date: ' . $currentDate);
                $this->_logger->info('Last updated at: ' . $dispersionHistoryLastUpdated);

                $this->_logger->info('Updating dispersion history status to incompleted');

                // set status of dispersion history to 0 so it is on status "Incomplete"
                $dispersionHistory->setStatus(DispersionHistory::STATUS_INCOMPLETED);
                $this->_dispersionHistoryRepository->save($dispersionHistory);
                $this->_logger->info('Successfully updated dispersion history ID:'. $dispersionHistoryId);
            }
        }
    }
}
