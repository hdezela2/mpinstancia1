<?php

namespace Formax\PriceDispersion\Block\Adminhtml\ProductPrice;

use Formax\AgreementInfo\Helper\Data as AgreementHelper;
use Formax\AgreementInfo\Model\ResourceModel\Agreement;
use Formax\PriceDispersion\Helper\Data;
use Magento\Backend\Block\Widget\Form\Container;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;
use Magento\Store\Model\ResourceModel\Group\CollectionFactory as StoreGroupCollection;

class Edit extends Container
{
   /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /** @var AgreementHelper */
    private $_helper;

    /** @var StoreGroupCollection */
    private $_storeGroupCollection;
    private CollectionFactory $categoryCollectionFactory;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $categoryCollectionFactory,
        Registry $registry,
        AgreementHelper $agreementHelper,
        StoreGroupCollection $storeGroupColection,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->_helper = $agreementHelper;
        $this->_storeGroupCollection = $storeGroupColection;
        parent::__construct($context, $data);
    }

    /**
     * Class constructor
     *  
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_productPrice';
        $this->_blockGroup = 'Formax_PriceDispersion';

        parent::_construct();

        $this->buttonList->update('save', 'onclick', 'return confirmFunction(\'save\')');
        $this->buttonList->update('save', 'data_attribute', [
            'mage-init' => ['button' => ['event' => 'mysave', 'target' => '#edit_form']],
        ]);
    }

    /**
     * Retrieve text for header element depending on loaded productprice
     * 
     * @return string
     */
    public function getHeaderText()
    {
        return __('Update Product Price');
    }
 
    /**
     * Prepare layout
     *
     * @return \Magento\Framework\View\Element\AbstractBlock
     */
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * Prepare form Html. call the phtm file with form.
     *
     * @return string
     */
    public function getFormHtml()
    {
       // get the current form as html content.
        $html = parent::getFormHtml();
        //Append the phtml file after the form content.
        $html .= $this->setTemplate('Formax_PriceDispersion::category-tree.phtml')->toHtml(); 
        return $html;
    }

    public function getCategoriesTree()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $collection */
        $collection = $this->categoryCollectionFactory->create()->addAttributeToSelect('*');

        $agreementId = $this->_helper->getAgreementId();
        if ($agreementId == \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_DEPOT) {
            $rootCategoryId = $this->_storeGroupCollection->create()->addFieldToFilter('website_id', ["eq" => 1])->getFirstItem()->getRootCategoryId();
            $collection->addAttributeToFilter(
                array(
                    array('attribute' => 'path', 'like' => '1/'.$rootCategoryId.'/%'),
                    array('attribute' => 'path', 'eq' => '1/'.$rootCategoryId)
                )
            );
        }

        if ($agreementId == \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_FURNITURE) {
            $collection->addAttributeToFilter('name', ['nlike' => '%armado']);
        }

        $sellerCategory = [
            Category::TREE_ROOT_ID => [
                'value' => Category::TREE_ROOT_ID,
                'optgroup' => null,
            ],
        ];

        foreach ($collection as $category) {
            $catId = $category->getId();
            $catParentId = $category->getParentId();
            foreach ([$catId, $catParentId] as $categoryId) {
                if (!isset($sellerCategory[$categoryId])) {
                    $sellerCategory[$categoryId] = ['value' => $categoryId];
                }
            }

            $sellerCategory[$catId]['is_active'] = $category->getIsActive();
            $sellerCategory[$catId]['label'] = $category->getName();
            $sellerCategory[$catParentId]['optgroup'][] = &$sellerCategory[$catId];
        }
        return json_encode($sellerCategory[Category::TREE_ROOT_ID]['optgroup']);

    }

    public function getSaveUrl()
    {
        return $this->getUrl('pricedispersion/index/run');
    }
}
