<?php

namespace Formax\PriceDispersion\Block\Adminhtml\ProductPrice\Edit;

use \Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic
{

   /**
    * @var \Magento\Store\Model\System\Store
    */
   protected $_systemStore;

   /**
    * @param \Magento\Backend\Block\Template\Context $context
    * @param \Magento\Framework\Registry $registry
    * @param \Magento\Framework\Data\FormFactory $formFactory
    * @param \Magento\Store\Model\System\Store $systemStore
    * @param array $data
    */
   public function __construct(
       \Magento\Backend\Block\Template\Context $context,
       \Magento\Framework\Registry $registry,
       \Magento\Framework\Data\FormFactory $formFactory,
       \Magento\Store\Model\System\Store $systemStore,
       array $data = []
   ) {
       $this->_systemStore = $systemStore;
       parent::__construct($context, $registry, $formFactory, $data);
   }

   /**
    * Init form
    *
    * @return void
    */
   protected function _construct()
   {
       parent::_construct();
       $this->setId('productprice_form');
       $this->setTitle(__('Product Price Information'));
   }

   /**
    * Prepare form
    *
    * @return $this
    */
   protected function _prepareForm()
   {
       /** @var \Magento\Framework\Data\Form $form */
       $form = $this->_formFactory->create(
           ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
       );

       $form->setHtmlIdPrefix('productprice');

       $fieldset = $form->addFieldset(
           'base_fieldset',
           ['legend' => __(''), 'class' => 'fieldset-wide']
       );

       $fieldset->addField(
            'categories',
            'text',
            ['name' => 'categories', 'label' => __('Categories'), 'title' => __('Categories'), 'required' => true]
       );

       $fieldset->addField(
            'update_percentage',
            'text',
            [
                'name' => 'update_percentage',
                'label' => __('Price Percentage'),
                'title' => __('Price Percentage'),
                'required' => true,
                'class' => 'validate-number required-number required-entry'
            ]
       );

       $form->setUseContainer(true);
       $this->setForm($form);

       return parent::_prepareForm();
   }
}