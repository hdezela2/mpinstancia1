<?php
declare(strict_types=1);
namespace Formax\PriceDispersion\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Formax\PriceDispersion\Api\Data\DispersionHistoryInterface;
use Formax\PriceDispersion\Api\Data\DispersionHistorySearchResultsInterface;

interface DispersionHistoryRepositoryInterface
{
    /**
     * @param DispersionHistoryInterface $dispersionHistory
     * @param int|null $storeId
     * @return void
     */
    public function save(DispersionHistoryInterface $dispersionHistory, ?int $storeId = 0);

    /**
     * @param $entityID
     * @return \Formax\PriceDispersion\Api\Data\DispersionHistoryInterface
     */
    public function getById($entityID);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return \Formax\PriceDispersion\Api\Data\DispersionHistorySearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param DispersionHistoryInterface $dispersionHistory
     * @return void
     */
    public function delete(DispersionHistoryInterface $dispersionHistory);

    /**
     * @param $entityID
     * @return void
     */
    public function deleteById($entityID);
}
