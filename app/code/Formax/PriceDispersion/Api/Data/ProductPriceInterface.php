<?php

namespace Formax\PriceDispersion\Api\Data;

/**
 * PriceDispersion ProductPrice interface.
 * @api
 */
interface ProductPriceInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID    = 'entity_id';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Formax\PriceDispersion\Api\Data\ProductPriceInterface
     */
    public function setId($id);
}
