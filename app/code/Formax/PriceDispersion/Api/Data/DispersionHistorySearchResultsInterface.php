<?php
declare(strict_types=1);
namespace Formax\PriceDispersion\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface DispersionHistorySearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Formax\PriceDispersion\Api\Data\DispersionHistoryInterface[]
     */
    public function getItems();

    /**
     * @param \Formax\PriceDispersion\Api\Data\DispersionHistoryInterface[] $items
     * @return \Formax\PriceDispersion\Api\Data\DispersionHistorySearchResultsInterface
     */
    public function setItems(array $items);
}