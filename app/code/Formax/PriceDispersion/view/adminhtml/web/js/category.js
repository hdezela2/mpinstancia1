
define([
    "jquery",
    "mage/translate",
    'Magento_Ui/js/modal/alert',
    "jquery/ui",
    ], function ($, $t, alert) {
        'use strict';
        $.widget('pricedispersion.category', {
            options: {},
            _create: function () {
                var self = this;
                $(document).ready(function () {
                    var skipCount = 0;
                    var importUrl = self.options.importUrl;
                    var finishUrl = self.options.finishUrl;
                    var notCompletedUrl = self.options.notCompletedUrl;
                    var total = self.options.productCount;
                    var completeLabel = self.options.completeLabel;
                    var notCompletedLabel = self.options.notCompletedLabel;
                    var dispersionHistoryId = self.options.dispersionHistoryId;
                    var newIndex = self.options.newIndex;
                    if (total > 0) {
                        var productData = self.options.productData;
                        var pricePercentage = self.options.pricePercentage;
                        var allCategories = self.options.allCategories;
                        updatePrices(1, newIndex);
                    }
                    function updatePrices(count, newIndex)
                    {
                        $.ajax({
                            type: 'post',
                            url:importUrl,
                            async: true,
                            dataType: 'json',
                            data : { 'count': count+newIndex, 'product': productData[count-1+newIndex], 'pricePercentage': pricePercentage, 'allCategories' : allCategories, 'dispersionHistoryId' : dispersionHistoryId },
                            success:function (data) {
                                if (data['error'] == 1) {
                                    $(".fieldset").append(data['msg']);
                                    skipCount++;
                                } else {
                                    $(".fieldset").append(data['msg']);
                                }
                                localStorage.removeItem("keySize");
                                var width = (100/total)*count;
                                $(".wk-mu-progress-bar-current").animate({width: width+"%"},'slow', function () {
                                    if (total == 1 && skipCount ==1) {
                                        $(".wk-mu-info-bar").addClass("wk-no-padding");
                                        $(".wk-mu-importing-loader").remove();
                                        $(".wk-mu-info-bar-content").text(completeLabel);
                                    } else {
                                        if (count == total) {
                                            finishUpdating(count, newIndex, skipCount, dispersionHistoryId);
                                        } else {
                                            count++;
                                            $(".wk-current").text(count);
                                            updatePrices(count, newIndex);
                                        }
                                    }
                                });
                            },
                            error: function() {
                                $(".wk-mu-info-bar").addClass("wk-no-padding");
                                $(".wk-mu-importing-loader").remove();
                                $(".wk-mu-info-bar-content").text(notCompletedLabel);
                                $(".wk-mu-note").text("Ejecución pausada.");
                                $(".wk-mu-notice").text("Ha ocurrido un error inesperado mientras se realizaba la dispersión de precios, el proceso será finalizado por un cron automático.");
                                $(".wk-mu-progress-bar").css("border-color", "#ef5f5f");
                                $(".wk-mu-progress-bar-current").css("background-color", "#ef5f5f");
                                setHistoryDispersionToNotCompleted(count, newIndex, skipCount, dispersionHistoryId);
                                // alert({
                                //     // content: $t('An unexpected error ocurred while running price dispersion, this process will be finished by a cron job.')
                                //     content: $t('Ha ocurrido un error inesperado mientras se realizaba la dispersión de precios, el proceso será finalizado por un cron automático.'),
                                //     actions: {
                                //         always: setHistoryDispersionToNotCompleted(count, newIndex, skipCount, dispersionHistoryId)
                                //     }
                                // });
                            }
                        });
                    }
                    function finishUpdating(count, newIndex, skipCount, dispersionHistoryId)
                    {
                        $.ajax({
                            type: 'post',
                            url:finishUrl,
                            async: true,
                            dataType: 'json',
                            data : { row:count+newIndex, skip:skipCount, dispersionHistoryId:dispersionHistoryId },
                            success:function (data) {
                                $(".fieldset").append(data['msg']);
                                $(".wk-mu-info-bar").addClass("wk-no-padding");
                                $(".wk-mu-info-bar").text(completeLabel);
                            }
                        });
                    }
                    function setHistoryDispersionToNotCompleted(count, newIndex, skipCount, dispersionHistoryId)
                    {
                        $.ajax({
                            type: 'post',
                            url:notCompletedUrl,
                            async: true,
                            dataType: 'json',
                            data : { row:count+newIndex, skip:skipCount, dispersionHistoryId:dispersionHistoryId },
                            success:function (data) {
                                $(".fieldset").append(data['msg']);
                                $(".wk-mu-info-bar").addClass("wk-no-padding");
                                $(".wk-mu-info-bar").text(notCompletedLabel);
                            }
                        });
                    }
                });
            }
        });
        return $.pricedispersion.category;
    });
    
