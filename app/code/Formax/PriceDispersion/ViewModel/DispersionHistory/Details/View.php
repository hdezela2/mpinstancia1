<?php

namespace Formax\PriceDispersion\ViewModel\DispersionHistory\Details;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Formax\PriceDispersion\Api\DispersionHistoryRepositoryInterface;

class View implements ArgumentInterface
{
    /** @var DispersionHistoryRepositoryInterface */
    private $dispersionHistoryRepository;

    /** @var RequestInterface */
    private $request;

    /** @var CategoryRepositoryInterface */
    protected $_categoryRepository;

    public function __construct(
        DispersionHistoryRepositoryInterface $dispersionHistoryRepository,
        CategoryRepositoryInterface $categoryRepository,
        RequestInterface $request
    ) {
        $this->dispersionHistoryRepository = $dispersionHistoryRepository;
        $this->request = $request;
        $this->_categoryRepository = $categoryRepository;
    }

    public function getDispersionHistory()
    {
        $dispersionHistoryId = $this->request->getParam('dispersion_history_id');
        return $this->dispersionHistoryRepository->getById($dispersionHistoryId);
    }

    public function getCategories()
    {
        $dispersionHistory = $this->getDispersionHistory();
        $categories = $dispersionHistory->getAllCategories();
        $return = '';
        foreach ($categories as $category){
            if ($return) {
                $return .= ', ' . $this->_categoryRepository->get($category)->getName();
            } else {
                $return = $this->_categoryRepository->get($category)->getName();
            }
        }
        return $return;
    }
}
