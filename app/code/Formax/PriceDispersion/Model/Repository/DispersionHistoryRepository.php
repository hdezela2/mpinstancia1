<?php
declare(strict_types=1);
namespace Formax\PriceDispersion\Model\Repository;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Formax\PriceDispersion\Api\Data\DispersionHistoryInterface;
use Formax\PriceDispersion\Api\Data\DispersionHistorySearchResultsInterface;
use Formax\PriceDispersion\Api\Data\DispersionHistorySearchResultsInterfaceFactory as DispersionHistorySearchResultsFactory;
use Formax\PriceDispersion\Api\DispersionHistoryRepositoryInterface;
use Formax\PriceDispersion\Model\DispersionHistoryFactory;
use Formax\PriceDispersion\Model\ResourceModel\DispersionHistory as DispersionHistoryResource;
use Formax\PriceDispersion\Model\ResourceModel\DispersionHistory\CollectionFactory;

class DispersionHistoryRepository implements DispersionHistoryRepositoryInterface
{
    /** @var DispersionHistoryResource */
    protected $resource;

    /** @var DispersionHistoryFactory */
    protected $dispersionHistoryFactory;

    /** @var CollectionFactory */
    protected $collectionFactory;

    /** @var DispersionHistorySearchResultsFactory */
    protected $searchResultsFactory;

    /** @var CollectionProcessorInterface */
    private $collectionProcessor;
    /** @var EmailNotification */
    private $emailNotification;

    public function __construct(
        DispersionHistoryResource $resource,
        DispersionHistoryFactory $dispersionHistoryFactory,
        CollectionFactory $collectionFactory,
        DispersionHistorySearchResultsFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->dispersionHistoryFactory = $dispersionHistoryFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    public function save(DispersionHistoryInterface $dispersionHistory, ?int $storeId = 0)
    {
        try {
            $this->resource->save($dispersionHistory);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $dispersionHistory;
    }

    public function getById($entityId)
    {
        $dispersionHistory = $this->dispersionHistoryFactory->create();
        $this->resource->load($dispersionHistory, $entityId);
        if (!$dispersionHistory->getId()) {
            throw new NoSuchEntityException(__('The dispersion history with ID: %1 doesn\'t exist.', $entityId));
        }
        return $dispersionHistory;
    }

    public function getList(SearchCriteriaInterface $criteria)
    {
        /** @var \Formax\PriceDispersion\Model\ResourceModel\DispersionHistory\Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var DispersionHistorySearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    public function delete(DispersionHistoryInterface $dispersionHistory)
    {
        try {
            $this->resource->delete($dispersionHistory);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    public function deleteById($entityID)
    {
        return $this->delete($this->getById($entityID));
    }
}
