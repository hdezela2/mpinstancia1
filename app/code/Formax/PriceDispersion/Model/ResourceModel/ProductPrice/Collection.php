<?php

namespace Formax\PriceDispersion\Model\ResourceModel\ProductPrice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Formax\PriceDispersion\Model\ProductPrice',
            'Formax\PriceDispersion\Model\ResourceModel\ProductPrice'
        );
        $this->_map['fields']['entity_id'] = 'main_table.entity_id';
    }
}
