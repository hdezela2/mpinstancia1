<?php
declare(strict_types=1);
namespace Formax\PriceDispersion\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class DispersionHistory extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('price_dispersion_history', 'entity_id');
    }
}