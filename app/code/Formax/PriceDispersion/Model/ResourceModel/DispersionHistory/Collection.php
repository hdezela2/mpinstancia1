<?php
declare(strict_types=1);
namespace Formax\PriceDispersion\Model\ResourceModel\DispersionHistory;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Formax\PriceDispersion\Model\DispersionHistory as DispersionHistoryModel;
use Formax\PriceDispersion\Model\ResourceModel\DispersionHistory as DispersionHistoryResourceModel;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(DispersionHistoryModel::class, DispersionHistoryResourceModel::class);
    }
}