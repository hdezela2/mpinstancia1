<?php


namespace Formax\PriceDispersion\Model;

use Magento\Framework\Model\AbstractModel;
use Formax\PriceDispersion\Api\Data\ProductPriceInterface;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * PriceDispersion Product Model.
 *
 * @method \Formax\PriceDispersion\Model\ResourceModel\ProductPrice _getResource()
 * @method \Formax\PriceDispersion\Model\ResourceModel\ProductPrice getResource()
 */
class ProductPrice extends AbstractModel implements ProductPriceInterface, IdentityInterface
{
    /**
     * No route page id.
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**
     * PriceDispersion Product cache tag.
     */
    const CACHE_TAG = 'price_dispersion';

    /**
     * @var string
     */
    protected $_cacheTag = 'price_dispersion';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'price_dispersion';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Formax\PriceDispersion\Model\ResourceModel\ProductPrice');
    }

    /**
     * Load object data.
     *
     * @param int|null $id
     * @param string   $field
     *
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteProduct();
        }

        return parent::load($id, $field);
    }

    /**
     * Load No-Route Product.
     *
     * @return \Formax\Marketplace\Model\Product
     */
    public function noRouteProduct()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Get ID.
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Formax\Marketplace\Api\Data\ProductInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
}
