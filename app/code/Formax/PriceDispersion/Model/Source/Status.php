<?php

namespace Formax\PriceDispersion\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Formax\PriceDispersion\Model\DispersionHistory;

class Status implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Completed'),
                'value' => DispersionHistory::STATUS_COMPLETED
            ],
            [
                'label' => __('Incompleted'),
                'value' => DispersionHistory::STATUS_INCOMPLETED
            ],
            [
                'label' => __('In Progress'),
                'value' => DispersionHistory::STATUS_IN_PROGRESS
            ]
        ];
    }
}