<?php

namespace Formax\PriceDispersion\Console\Command;

use Formax\PriceDispersion\Model\DispersionHistory;
use Formax\PriceDispersion\Model\Repository\DispersionHistoryRepository;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Formax\PriceDispersion\Helper\Data as Helper;
use Webkul\MpAssignProduct\Model\AssociatesFactory as AssociatesFactory;
use Webkul\MpAssignProduct\Model\ItemsFactory as AssignItemsFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollectionFactory;

class CompletePriceDispersion extends Command
{
    /**
     * @var State
     */
    protected $appState;

    /**
     * @var Helper
     */
    protected $helper;

    /** @var DispersionHistoryRepository */
    protected $_dispersionHistoryRepository;

    /** @var AssignItemsFactory */
    protected $_assignItems;

    /** @var AssociatesCollectionFactory */
    protected $_associatesCollection;

    /** @var AssociatesFactory */
    protected $_assignAssociates;

    public function __construct(
        State $appState,
        Helper $helper,
        DispersionHistoryRepository $dispersionHistoryRepository,
        AssignItemsFactory $assignItems,
        AssociatesFactory $assignAssociates,
        AssociatesCollectionFactory $associatesCollection,
        $name = null
    ) {
        parent::__construct($name);
        $this->helper = $helper;
        $this->appState = $appState;
        $this->_dispersionHistoryRepository = $dispersionHistoryRepository;
        $this->_assignItems = $assignItems;
        $this->_associatesCollection = $associatesCollection;
        $this->_assignAssociates = $assignAssociates;
    }

    protected function configure()
    {
        $this->setName('product:completepricedispersion');
        $this->setDescription("Complete all price dispersion that are incomplete.");

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(Area::AREA_GLOBAL);

        // get not completed dispersion history
        $notCompletedDispersionHistory = $this->helper->getNotCompletedDispersionHistory();

        foreach ($notCompletedDispersionHistory as $dispersionHistory) {

            $pricePercentage = $dispersionHistory->getPricePercentage();
            $allCategories = $dispersionHistory->getAllCategories();
            $assignProductIds = $dispersionHistory->getAssignProductIds();
            $productCountActual = $dispersionHistory->getProductCountActual();
            $productCountTotal = $dispersionHistory->getProductCountTotal();
            $dispersionHistoryId = $dispersionHistory->getEntityId();

            $output->writeln('<info>Completing dispersion history ID:'. $dispersionHistoryId .'</info>');
            $output->writeln('Product count total: ' . $productCountTotal);
            $output->writeln('Product count actual: ' . $productCountActual);

            $output->writeln('<info>Start updating assign products</info>');

            // set status of dispersion history to "In Progress"
            $dispersionHistory->setStatus(DispersionHistory::STATUS_IN_PROGRESS);
            foreach ($assignProductIds as $key => $value) {

                if ($value['type'] == 'simple') {
                    $assignItem = $this->_assignItems->create()->load($value['id']);

                    $assignItem->setStatus('3');
                    $assignItem->setQty('0');
                    $assignItem->setDisDispersion('1');
                    $assignItem->save();

                    $countConfigurable = $this->_associatesCollection->create()
                        ->addFieldToFilter('parent_product_id', ['eq' => $value['id']])
                        ->getSize();
                    if ($countConfigurable) {
                        $countConfigurableDis = $this->_associatesCollection->create()
                            ->addFieldToFilter('parent_product_id', ['eq' => $value['id']])
                            ->addFieldToFilter('dis_dispersion', ['eq' => 1])
                            ->getSize();
                        $associateItem = $this->_assignAssociates->create()->load($value['id']);
                        if ($countConfigurable == $countConfigurableDis) {
                            // qty = 0, dis_dispersion = 1
                            $associateItem->setQty('0');
                            $associateItem->setDisDispersion('1');
                        } else {
                            //qty = 9999, dis_dispersion = 0
                            $associateItem->setQty('9999');
                            $associateItem->setDisDispersion('0');
                        }
                        $associateItem->save();
                    }
                } elseif ($value['type'] == 'configurable') {
                    $associateItem = $this->_assignAssociates->create()->load($value['id']);
                    // qty = 0, dis_dispersion = 1
                    $associateItem->setQty('0');
                    $associateItem->setDisDispersion('1');
                    $associateItem->save();
                }
                $output->writeln('Updated assign product ID: ' . $value['id']);

                // remove item from assignProductIds
                unset($assignProductIds[$key]);
                $dispersionHistory->setAssignProductIds($assignProductIds);
                // update productCountActual
                $productCountActual++;
                $dispersionHistory->setProductCountActual($productCountActual);
                $output->writeln('Updated ' . $productCountActual . ' of ' . $productCountTotal);
                // save changes
                $this->_dispersionHistoryRepository->save($dispersionHistory);

                // log changes into price_dispersion table
                $assignItemPrice = 0;
                if ($value['type'] == 'simple') {
                    $assignItemPrice = $assignItem->getPrice();
                } elseif ($value['type'] == 'configurable') {
                    $assignItemPrice = $associateItem->getPrice();
                }

                $this->helper->updateLog($value['product_id'], $assignItemPrice, $value['seller_id'], $pricePercentage, $assignItemPrice, $allCategories);
            }
            $output->writeln('<info>Finished updating assign products</info>');
            // update dispersionHistory status to completed
            $dispersionHistory->setStatus(DispersionHistory::STATUS_COMPLETED);
            $this->_dispersionHistoryRepository->save($dispersionHistory);
            $output->writeln('<info>Completed dispersion history ID:'. $dispersionHistoryId .'</info>');
        }
    }
}
