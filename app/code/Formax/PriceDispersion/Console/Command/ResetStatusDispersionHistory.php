<?php

namespace Formax\PriceDispersion\Console\Command;

use Formax\PriceDispersion\Model\DispersionHistory;
use Formax\PriceDispersion\Model\Repository\DispersionHistoryRepository;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Formax\PriceDispersion\Helper\Data as Helper;

class ResetStatusDispersionHistory extends Command
{
    /** @var State */
    protected $appState;

    /** @var Helper */
    protected $helper;

    /** @var DispersionHistoryRepository */
    protected $_dispersionHistoryRepository;

    /** @var DateTime */
    protected $date;

    public function __construct(
        State $appState,
        Helper $helper,
        DispersionHistoryRepository $dispersionHistoryRepository,
        DateTime $date,
        $name = null
    ) {
        parent::__construct($name);
        $this->helper = $helper;
        $this->appState = $appState;
        $this->_dispersionHistoryRepository = $dispersionHistoryRepository;
        $this->date = $date;
    }

    protected function configure()
    {
        $this->setName('product:updateinprogressdispersionhistory');
        $this->setDescription("Update status of in progress dispersion history.");

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(Area::AREA_GLOBAL);

        // get in progress dispersion history that were not updated on the last 5 minutes
        $inProgressDispersionHistory = $this->helper->getInProgressDispersionHistory();

        foreach ($inProgressDispersionHistory as $dispersionHistory) {

            $dispersionHistoryLastUpdated = $dispersionHistory->getUpdatedAt()->format("Y-m-d H:i:s");
            $date1 = strtotime($dispersionHistoryLastUpdated);
            $dispersionHistoryId = $dispersionHistory->getEntityId();

            $currentDate = $this->date->gmtDate("Y-m-d H:i:s");
            $date2 = strtotime($currentDate);
            $diff = abs($date2 - $date1);

            if ($diff > 300) {
                $output->writeln('<info>Checking dispersion history ID:'. $dispersionHistoryId .'</info>');
                $output->writeln('Current date: ' . $currentDate);
                $output->writeln('Last updated at: ' . $dispersionHistoryLastUpdated);

                $output->writeln('<info>Updating dispersion history status to incompleted</info>');

                // set status of dispersion history to 0 so it is on status "Incomplete"
                $dispersionHistory->setStatus(DispersionHistory::STATUS_INCOMPLETED);
                $this->_dispersionHistoryRepository->save($dispersionHistory);
                $output->writeln('<info>Successfully updated dispersion history ID:'. $dispersionHistoryId .'</info>');
            }
        }
    }
}
