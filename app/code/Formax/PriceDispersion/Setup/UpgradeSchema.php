<?php


namespace Formax\PriceDispersion\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '2.0.2') < 0) {
            /**
             * Update tables 'price_dispersion'
             */
            $setup->getConnection()->addColumn(
                $setup->getTable('price_dispersion'),
                'seller_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'unsigned' => true,
                    'nullable' => false,
                    'comment' => 'seller id'
                ]
            );
            /**
             * Update tables 'marketplace_assignproduct_associated_products'
             */
            $setup->getConnection()->addColumn(
                $setup->getTable('marketplace_assignproduct_associated_products'),
                'dis_dispersion',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    null,
                    'nullable' => false,
                    'default' => '0',
                    'comment' => 'Disable product'
                ]
            );
            /**
             * Update tables 'marketplace_assignproduct_associated_products'
             */
            $setup->getConnection()->addColumn(
                $setup->getTable('marketplace_assignproduct_items'),
                'dis_dispersion',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    null,
                    'nullable' => false,
                    'default' => '0',
                    'comment' => 'Disable product'
                ]
            );
        }
        $setup->endSetup();
    }
}
