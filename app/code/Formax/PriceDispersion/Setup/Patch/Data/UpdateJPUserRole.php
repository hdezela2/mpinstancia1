<?php
declare(strict_types=1);

namespace Formax\PriceDispersion\Setup\Patch\Data;

use Magento\Authorization\Model\ResourceModel\Role\Grid\CollectionFactory;
use Magento\Authorization\Model\RulesFactory;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class UpdateJPUserRole implements DataPatchInterface
{
    /** @var RulesFactory */
    protected $rulesFactory;

    /** @var CollectionFactory */
    protected $roleCollectionFactory;

    public function __construct(
        CollectionFactory $roleCollectionFactory,
        RulesFactory $rulesFactory
    ) {
        $this->rulesFactory = $rulesFactory;
        $this->roleCollectionFactory = $roleCollectionFactory;
    }

    public function apply() {
        // Update resources for all JP users to allow see price dispersion and category product price history pages
        $roles = $this->roleCollectionFactory->create();
        $roles->addFieldToFilter('role_name', ["like" => '5800%']);

        foreach ($roles as $role) {
            $resource = [
                'Magento_Backend::dashboard',

                'Webkul_CategoryProductPrice::categoryproductprice',
                'Webkul_CategoryProductPrice::index',
                'Cleverit_CategoryProductPrice::history',

                'Formax_PriceDispersion::pricedispersion',
                'Formax_PriceDispersion::index',
                'Formax_PriceDispersion::history',

                'Magento_Analytics::analytics',
                'Magento_Analytics::analytics_api',

                'Webkul_Marketplace::marketplace',
                'Webkul_Marketplace::menu',
                'Webkul_Marketplace::seller',
                'Webkul_MpAssignProduct::product',
                'Formax_RegionalCondition::regional_condition',

                'Webkul_Mpshipping::menu',
                'Webkul_Mpshipping::mpshipping',
                'Webkul_Mpshipping::mpshippingset',

                'Webkul_Requestforquote::requestforquote',
                'Webkul_Requestforquote::quote_index',
                'Webkul_Requestforquote::index_index',

                'Magento_Catalog::catalog',
                'Magento_CatalogPermissions::catalog_magento_catalogpermissions',
                'Magento_Catalog::catalog_inventory',
                'Magento_Catalog::products',
                'Magento_PricePermissions::read_product_price',
                'Magento_PricePermissions::edit_product_price',
                'Magento_PricePermissions::edit_product_status',
                'Magento_Catalog::categories',

                'Magento_Customer::customer',
                'Magento_Customer::manage',
                'Magento_Reward::reward_balance',

                'Magento_Reports::report',
                'Magento_Reports::salesroot',
                'Formax_CustomReport::Report',

                'Magento_Backend::system',
                'Magento_Backend::convert',
                'Magento_ImportExport::export'
            ];

            /* Array of resource ids which we want to allow this role*/
            $this->rulesFactory->create()->setRoleId($role->getId())->setResources($resource)->saveRel();
        }
    }

    /** @return string[] */
    public static function getDependencies(): array
    {
        return [];
    }

    /** @return array */
    public function getAliases(): array
    {
        return [];
    }
}
