<?php

namespace Formax\PriceDispersion\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;

class Dispersion extends Column
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name'); 
                if(isset($item['dis_dispersion']) && $item['dis_dispersion'] == 1){
                    $item[$name] = __('Blocked by dispersion');
                }else{
                    $item[$name] = '-';
                }
            }
        }
        return $dataSource;
    }
}