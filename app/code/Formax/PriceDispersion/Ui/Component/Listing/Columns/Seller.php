<?php


namespace Formax\PriceDispersion\Ui\Component\Listing\Columns;

use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class Seller extends \Magento\Ui\Component\Listing\Columns\Column
{
    private CustomerFactory $customerFactory;

    /**
     * Seller constructor.
     * @param ContextInterface $context
     * @param CustomerFactory $customerFactory
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        CustomerFactory $customerFactory,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->customerFactory = $customerFactory;
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $sellerId = $item[$fieldName];
                $customer = $this->customerFactory->create()->load($sellerId);
                $item[$fieldName] = $customer->getFirstname().' '.$customer->getLastname();
            }
        }

        return $dataSource;
    }
}
