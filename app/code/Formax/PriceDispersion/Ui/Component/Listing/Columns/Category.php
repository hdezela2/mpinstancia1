<?php


namespace Formax\PriceDispersion\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class Category extends \Magento\Ui\Component\Listing\Columns\Column
{
    private \Magento\Catalog\Model\CategoryFactory $categoryFactory;

    /**
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $categoryIds = explode(',', $item[$fieldName]);
                $item[$fieldName] = '';
                foreach ($categoryIds as $catId) {
                    $catId = str_replace('"','',$catId);
                    $catId = str_replace('[','',$catId);
                    $catId = str_replace(']','',$catId);
                    $category = $this->categoryFactory->create()->load($catId);
                    if ($item[$fieldName]) {
                        $item[$fieldName] .= ', '.$category->getName();
                    } else {
                        $item[$fieldName] .= $category->getName();
                    }
                    
                }
            }
        }

        return $dataSource;
    }
}
