<?php
declare(strict_types=1);

namespace Formax\PriceDispersion\Ui\Component\Listing\Columns;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class DispersionHistoryActions extends Column
{
    const URL_PATH_RESUME = 'pricedispersion/history/resume';

    const URL_PATH_DELETE = 'pricedispersion/history/delete';

    const URL_PATH_DETAILS = 'pricedispersion/history/details';

    /** @var UrlInterface */
    private $urlBuilder;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (!isset($dataSource['data']['items'])) {
            return $dataSource;
        }

        foreach ($dataSource['data']['items'] as & $item) {
            if (!isset($item['entity_id'])) {
                continue;
            }

            $entityId = $item['entity_id'];

            $hideResume = $item['status'] ? true : false;

            $item[$this->getData('name')] = [
//                'resume' => [
//                    'href' => $this->urlBuilder->getUrl(
//                        static::URL_PATH_RESUME,
//                        [
//                            'dispersion_history_id' => $item['entity_id'],
//                        ]
//                    ),
//                    'label' => __('Resume'),
//                    '__disableTmpl' => true,
//                    'hidden' => $hideResume
//                ],
                'details' => [
                    'href' => $this->urlBuilder->getUrl(
                        static::URL_PATH_DETAILS,
                        [
                            'dispersion_history_id' => $item['entity_id']
                        ]
                    ),
                    'label' => __('Details'),
                    '__disableTmpl' => true,
                ],
//                'delete' => [
//                    'href' => $this->urlBuilder->getUrl(
//                        static::URL_PATH_DELETE,
//                        [
//                            'dispersion_history_id' => $item['entity_id'],
//                        ]
//                    ),
//                    'label' => __('Delete'),
//                    'confirm' => [
//                        'title' => __('Delete %1', $entityId),
//                        'message' => __('Are you sure you want to delete the %1 id record?', $entityId),
//                    ],
//                    'post' => true,
//                    '__disableTmpl' => true,
//                ],
            ];
        }
        return $dataSource;
    }
}