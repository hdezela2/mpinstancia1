<?php
declare(strict_types=1);
namespace Formax\PriceDispersion\Ui\Component;

use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider as AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    public function getData()
    {
        return parent::getData();
    }
}