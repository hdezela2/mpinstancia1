<?php

namespace Formax\PriceDispersion\Controller\Adminhtml\Index;

use Formax\PriceDispersion\Helper\Data as DispersionHelper;
use Formax\PriceDispersion\Model\DispersionHistory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Json\Helper\Data as JsonHelper;


class Finish extends Action
{

    /** @var Context */
    protected $context;

    /** @var DispersionHelper */
    protected $dispersionHelper;

    /** @var JsonHelper */
    protected $jsonHelper;

    public function __construct(
        Context $context,
        DispersionHelper $dispersionHelper,
        JsonHelper $jsonHelper
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->dispersionHelper = $dispersionHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = [];
        $total = (int) $this->getRequest()->getParam('row');
        $skipCount = (int) $this->getRequest()->getParam('skip');
        $total = $total - $skipCount;
        $msg = '<div class="wk-mu-success wk-mu-box">';
        $msg .= __('Total %1 Product(s) Price Updated.', $total);
        $msg .= '</div>';
        $msg .= '<div class="wk-mu-note wk-mu-box">';
        $msg .= __('Finished Execution.');
        $msg .= '</div>';
        $result['msg'] = $msg;
        $result = $this->jsonHelper->jsonEncode($result);

        $dispersionHistoryId = $this->getRequest()->getParam('dispersionHistoryId');
        $this->dispersionHelper->updateDispersionHistoryStatus($dispersionHistoryId, DispersionHistory::STATUS_COMPLETED);

        $this->getResponse()->representJson($result);
    }

    /**
     * Check for is allowed.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Formax_PriceDispersion::index');
    }
}
