<?php

namespace Formax\PriceDispersion\Controller\Adminhtml\Index;

use Formax\PriceDispersion\Model\DispersionHistoryFactory;
use Formax\PriceDispersion\Model\ProductPriceFactory;
use Formax\PriceDispersion\Model\Repository\DispersionHistoryRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Result\PageFactory;
use Webkul\MpAssignProduct\Model\AssociatesFactory;
use Webkul\MpAssignProduct\Model\ItemsFactory;
use Webkul\MpAssignProduct\Helper\Data as AssignHelper;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollectionFactory;
use Zend\Db\Adapter\Exception\ErrorException;

class Save extends Action
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var ItemsFactory
     */
    private $assignItems;

    /**
     * @var AssociatesFactory
     */
    private $assignAssociates;

    /**
     * @var ProductPriceFactory
     */
    private $productPriceFactory;

    /**
     * @var Configurable
     */
    private $configurableProductType;

    /**
     * @var Data
     */
    private $jsonHelper;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var ResourceConnection
     */
    private $connection;

    /**
     * @var string
     */
    protected $priceDispersionTable;

    /** @var string */
    protected $assignproductItemsTable;

    /** @var string */
    protected $assignproductItemsAssociateTable;

    /** @var DispersionHistoryFactory */
    protected $_dispersionHistoryFactory;

    /** @var DispersionHistoryRepository */
    protected $_dispersionHistoryRepository;

    /**
     * @var AssociatesCollectionFactory
     */
    protected $_associatesCollection;

    /**
     * @var AssignHelper
     */
    private $assignHelper;

    public function __construct(
        Context $context,
        ProductFactory $productFactory,
        ItemsFactory $assignItems,
        AssociatesFactory $assignAssociates,
        AssociatesCollectionFactory $associatesCollection,
        ProductPriceFactory $productPriceFactory,
        Configurable $configurableProductType,
        Data $jsonHelper,
        DateTime $date,
        CategoryFactory $categoryFactory,
        PageFactory $resultPageFactory,
        ResourceConnection $resourceConnection,
        AssignHelper $assignHelper,
	    DispersionHistoryFactory $dispersionHistoryFactory,
        DispersionHistoryRepository $dispersionHistoryRepository
    ) {
        parent::__construct($context);
        $this->productFactory = $productFactory;
        $this->assignAssociates = $assignAssociates;
        $this->assignItems = $assignItems;
        $this->categoryFactory = $categoryFactory;
        $this->jsonHelper = $jsonHelper;
        $this->configurableProductType = $configurableProductType;
        $this->date = $date;
        $this->productPriceFactory = $productPriceFactory;
        $this->resultPageFactory = $resultPageFactory;

        $this->connection = $resourceConnection->getConnection();
        $this->priceDispersionTable = $resourceConnection->getTableName('price_dispersion');
        $this->assignproductItemsTable = $resourceConnection->getTableName('marketplace_assignproduct_items');
        $this->assignproductItemsAssociateTable = $resourceConnection->getTableName('marketplace_assignproduct_associated_products');
        $this->assignHelper = $assignHelper;
	    $this->_dispersionHistoryFactory = $dispersionHistoryFactory;
        $this->_dispersionHistoryRepository = $dispersionHistoryRepository;
        $this->_associatesCollection = $associatesCollection;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $productData = $this->getRequest()->getParam('product');
        $pricePercentage = $this->getRequest()->getParam('pricePercentage');
        $allCategories = $this->getRequest()->getParam('allCategories');
        $dispersionHistoryId = $this->getRequest()->getParam('dispersionHistoryId');
        $count = $this->getRequest()->getParam('count');
        $newIndex = $this->getRequest()->getParam('newIndex');
        try {
            $responseData = $this->updatePrice($productData, $pricePercentage, $allCategories);
            $result['error'] = 0;
            $result['msg'] = '<div class="wk-mu-success wk-mu-box">' . __('Successfully updated') . " " . $responseData['name'] . '</div>';
        } catch (\Exception $e) {
            $result['error'] = 1;
            $result['msg'] = '<div class="wk-mu-error wk-mu-box">' . __('Error in updating price') . '</div>';
        }
        $this->updateDispersionHistory($dispersionHistoryId, $count, $productData, $newIndex);
        $result = $this->jsonHelper->jsonEncode($result);
        $this->getResponse()->representJson($result);
    }

    public function updatePrice($productData, $pricePercentage, $allCategories)
    {
        $product = $this->productFactory->create();
        $select = $this->connection->select();

        if ($productData['type'] == 'configurable') {
            $collection = $this->_associatesCollection->create();
            $collection->addFieldToFilter('main_table.id', ['eq' => $productData['id']])
                ->getSelect()->join(
                    ["mai" => $collection->getTable('marketplace_assignproduct_items')],
                    "main_table.parent_id = mai.id",
                    ['seller_id']
                );
            $assignItem = $collection->getFirstItem();
            $product->setId($assignItem->getParentProductId());

            $select->from($this->assignproductItemsAssociateTable, ['price'])
                ->where('id = ?', $productData['id']);
            $assignItemPrice = $this->connection->fetchOne($select);

        } else {
            $assignItem = $this->assignItems->create()->load($productData['id']);
            $product->setId($assignItem->getProductId());

            $select->from($this->assignproductItemsTable, ['price'])
                ->where('id = ?', $productData['id']);
            $assignItemPrice = $this->connection->fetchOne($select);
        }


        $productName = $product->getResource()->getAttributeRawValue(
            $product->getId(),
            'name',
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
        );
        $data['name'] = $productName;
        $this->connection->beginTransaction();
        if ($productData['type'] == 'simple') {
            // Changed status to 3 (Disabled by dispersion)
            $update_product = 'UPDATE  marketplace_assignproduct_items set status = 3, qty = 0, dis_dispersion = 1 where id = ' . (int) $productData['id'];
            $this->connection->query($update_product);

            $queryConfigurable = 'SELECT COUNT(*) FROM marketplace_assignproduct_associated_products where parent_product_id = ' . (int) $productData['id'];
            $countConfigurable = $this->connection->fetchOne($queryConfigurable);

            $queryConfigurableDis = 'SELECT COUNT(*) FROM marketplace_assignproduct_associated_products where parent_product_id = ' . (int) $productData['id'] . ' and dis_dispersion = 1';
            $countConfigurableDis = $this->connection->fetchOne($queryConfigurableDis);

            if ($countConfigurable == $countConfigurableDis) {
                $update_product = 'UPDATE  marketplace_assignproduct_associated_products set qty = 0, dis_dispersion = 1 where id = ' . (int) $productData['id'];
                $this->connection->query($update_product);
            } else {
                $update_product = 'UPDATE  marketplace_assignproduct_associated_products set qty = 9999, dis_dispersion = 0 where id = ' . (int) $productData['id'];
                $this->connection->query($update_product);
            }
        } elseif ($productData['type'] == 'configurable') {
            $update_product = 'UPDATE  marketplace_assignproduct_associated_products set qty = 0, status = 3, dis_dispersion = 1 where id = ' . (int) $productData['id'];
            $this->connection->query($update_product);
            $assignItem = $this->assignAssociates->create()->load($productData['id']);
            $product = $this->productFactory->create()->load($assignItem->getProductId());
            $data['name'] = $product->getName();
            $assignProduct = $this->assignItems->create()->load($assignItem->getParentId());
            $this->assignHelper->setConfigStatus($assignProduct);
        }
        $this->connection->commit();
        $this->updateLog($product, $assignItemPrice, $productData['seller_id'], $pricePercentage, $assignItemPrice, $allCategories);
        return $data;
    }

    public function updateLog($product, $itemPrice, $sellerId, $pricePercentage, $originalPrice, $allCategories)
    {
        $date = $this->date->gmtDate();
        $data = [
            'magecategory_id'      => $allCategories,
            'mageproduct_id'       => $product->getId(),
            'mageproduct_price'    => $originalPrice,
            'updatedproduct_price' => $itemPrice,
            'update_percentage'    => $pricePercentage,
            'created_at'           => $date,
            'updated_at'           => $date,
            'seller_id'            => $sellerId
        ];


        $this->connection->insert(
            $this->priceDispersionTable,
            $data
        );
    }

    public function updateDispersionHistory($dispersionHistoryId, $count, $productData, $newIndex = 0)
    {
        $dispersionHistory = $this->_dispersionHistoryRepository->getById($dispersionHistoryId);
        $assignProductIds = $dispersionHistory->getAssignProductIds();

        $countAssignProductIdsBefore = count($assignProductIds);

        // remove the actual assign product item id from the asignProductIds and update the assignProductIds value on the database
        $assignProductId = $productData['id'];
        foreach ($assignProductIds as $key => $value) {
            if ($value['id'] == $assignProductId) {
                unset($assignProductIds[$key]);
            }
        }

        $countAssignProducIdsAfter = count($assignProductIds);

        if ($countAssignProductIdsBefore != $countAssignProducIdsAfter) {
            $dispersionHistory->setAssignProductIds($assignProductIds);
        }
        $productCountActual = $count + $newIndex;
        $dispersionHistory->setProductCountActual($productCountActual);
        $this->_dispersionHistoryRepository->save($dispersionHistory);
    }

    /**
     * Check Dispersion History Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Formax_PriceDispersion::index');
    }
}