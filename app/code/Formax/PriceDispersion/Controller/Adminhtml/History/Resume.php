<?php

namespace Formax\PriceDispersion\Controller\Adminhtml\History;

use Formax\PriceDispersion\Helper\Data as DispersionHelper;
use Formax\PriceDispersion\Model\DispersionHistory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Resume extends Action
{
    /** @var PageFactory */
    protected $resultPageFactory;

    /** @var DispersionHelper */
    protected $dispersionHelper;

    public function __construct(
        Context $context,
        DispersionHelper $dispersionHelper,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->dispersionHelper = $dispersionHelper;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Formax_PriceDispersion::history');
    }

    /**
     * Save action.
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $dispersionHistoryId = $this->getRequest()->getParam('dispersion_history_id');

        if ($dispersionHistoryId) {
            $this->dispersionHelper->updateDispersionHistoryStatus($dispersionHistoryId, DispersionHistory::STATUS_IN_PROGRESS);
            $resultPage = $this->resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->prepend(__('Reading Data'));
            return $resultPage;
        }
        $this->messageManager->addError(__('Unable to get the dispersion history selected'));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/index');

    }
}
