<?php
namespace Formax\PriceDispersion\Controller\Adminhtml\History;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Formax\PriceDispersion\Api\DispersionHistoryRepositoryInterface;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;

class Delete extends Action
{
    /** @var DispersionHistoryRepositoryInterface */
    protected $dispersionHistoryRepository;

    public function __construct(
        Context $context,
        DispersionHistoryRepositoryInterface $dispersionHistoryRepository
    ) {
        $this->dispersionHistoryRepository = $dispersionHistoryRepository;
        parent::__construct($context);
    }

    /**
     * Delete dispersion history.
     *
     * @return Redirect
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $id = $this->getRequest()->getParam('dispersion_history_id');
        if ($id) {
            try {
                $dispersionHistory = $this->dispersionHistoryRepository->getById($id);
                $this->dispersionHistoryRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(
                    __(
                        'You have deleted dispersion history with ID:  %dispersionHistoryId.',
                        ['dispersionHistoryId' => $dispersionHistory ? $dispersionHistory->getEntityId() : '']
                    )
                );
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Something went wrong. Please try again later.'));
            }
        }
        $resultRedirect->setPath('*/*/');
        return $resultRedirect;
    }

    /**
     * Check Dispersion History Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Formax_PriceDispersion::history');
    }
}