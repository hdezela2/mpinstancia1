<?php

namespace Formax\Warranty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;

class Data extends AbstractHelper
{
    /**
     *
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     *
     * @var \Formax\Warranty\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var Context
     */
    private $context;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Formax\Warranty\Logger\Logger $logger
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        Context $context,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Formax\Warranty\Logger\Logger $logger,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->httpClientFactory = $httpClientFactory;
        $this->logger = $logger;
        $this->context = $context;
        $this->jsonHelper = $jsonHelper;

        parent::__construct($context);
    }

    /**
     * Get warranty data from REST service
     *
     * @param string $token
     * @param string $userId
     * @param string $agreementId
     * @return Zend_Http_Response
     */
    public function getWarrantyInfo($token, $userId, $agreementId)
    {
        $response = null;

        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_warranty_data/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');

            if (!empty($endpoint) && !empty($token) && !empty($userId) && !empty($agreementId)) {
                $httpHeaders = [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . trim($token)
                ];

                $endpoint .= '/proveedor/' . $userId . '/convenio/' . $agreementId . '/garantias';
                $client = $this->httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                    'timeout' => 30]
                );
                $client->setHeaders($httpHeaders);
                $response = $client->request(\Zend_Http_Client::GET)->getBody();

                $this->logger->info('Seller ID: ' . $userId . ' Agreement ID: ' . $agreementId . ' Service Response: ' . $response);
                $response = $this->jsonHelper->jsonDecode($response);
            } else {
                throw new LocalizedException(__('Define endpoint and token warranties webservice'));
            }
        } catch(LocalizedException $e) {
            $this->logger->warning('Seller ID: ' . $userId . ' Agreement ID: ' . $agreementId . ' Error message: ' . $e->getMessage());
        } catch(\Exception $e) {
            $this->logger->critical('Seller ID: ' . $userId . ' Agreement ID: ' . $agreementId . ' Error message: ' . $e->getMessage());
        }

        return $response;
    }
}
