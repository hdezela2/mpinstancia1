<?php

namespace Formax\Warranty\Model;

class ServiceData
{
    /**
     * @var \Formax\Warranty\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     * @param \Formax\Warranty\Helper\Data $helper
     */
    public function __construct(
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Formax\Warranty\Helper\Data $helper
    ) {
        $this->helper = $helper;
        $this->authSession = $authSession;
        $this->dataObjectFactory = $dataObjectFactory;
    }

    /**
     * @param string $token
     * @param string $userId
     * @param string $agreementId
     * @return mixed \Magento\Framework\DataObject
     */
    public function getData($token, $userId, $agreementId)
    {
        $result = null;
        $formatOut = 'd-m-Y';

        if ($token !== '' && $userId !== '' && $agreementId !== '') {
            $sessionKey = 'warranties_' . $userId . '_' . $agreementId;
            $result = $this->authSession->getData($sessionKey);
            if ($result === null) {
                $warrantyData = $this->helper->getWarrantyInfo($token, $userId, $agreementId);
                if ($warrantyData !== null) {
                    if (isset($warrantyData['success']) && $warrantyData['success'] === 'OK'
                        && isset($warrantyData['payload'])) {
                        foreach ($warrantyData['payload'] as $item) {
                            $dataObject = $this->dataObjectFactory->create();
                            $emisionDate = new \DateTime(trim($item['fechaemision']));

                            if (isset($item['fechavencimiento']) && $item['fechavencimiento'] != '') {
                                $expirationDate = new \DateTime(trim($item['fechavencimiento']));
                                $dataObject->setData('expiration_date', $expirationDate->format($formatOut));
                            } else {
                                $dataObject->setData('expiration_date', __('Information not available'));
                            }

                            $admissionDate = new \DateTime(trim($item['fechaingreso']));
                            $deliveryDate = new \DateTime(trim($item['fechaentrega']));
                            
                            $dataObject->setData('id', trim($item['id']));
                            $dataObject->setData('agreement_id', trim($item['contratoid']));
                            $dataObject->setData('seller_id', trim($item['proveedorid']));
                            $dataObject->setData('number', trim($item['numero']));
                            $dataObject->setData('emision_date', $emisionDate->format($formatOut));
                            $dataObject->setData('currency', trim($item['moneda']));
                            $dataObject->setData('amount', trim($item['monto']));
                            $dataObject->setData('glosa', trim($item['glosa']));
                            $dataObject->setData('bank', trim($item['banco']));
                            $dataObject->setData('location', trim($item['ubicacionfisica']));
                            $dataObject->setData('observations', trim($item['observaciones']));
                            $dataObject->setData('admission_date', $admissionDate->format($formatOut));
                            $dataObject->setData('delivery_date', $deliveryDate->format($formatOut));
                            $dataObject->setData('type', trim($item['tipo']));
                            $dataObject->setData('status', trim($item['estado']));
                            $result[] = $dataObject;
                            
                        }
                        
                        $this->authSession->setData($sessionKey, $result);
                    }
                }
            }
        }
        
        return $result;
    }
}