<?php

namespace Formax\Warranty\Block\Adminhtml\CustomerEdit\Tab;

class View extends \Magento\Backend\Block\Template
    implements \Magento\Ui\Component\Layout\Tabs\TabInterface
{
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'tab/warranties.phtml';

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var \Formax\Warranty\Model\ServiceData
     */
    protected $serviceData;

    /**
     * @var bool
     */
    protected $showTab = false;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $currentCustomer;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var \Magento\Framework\Registry
     */
    private $_coreRegistry;

    /**
     * View constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Formax\Warranty\Model\ServiceData $serviceData
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Formax\Warranty\Model\ServiceData $serviceData,
        \Magento\Backend\Model\Auth\Session $authSession,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->authSession = $authSession;
        $this->serviceData = $serviceData;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->_coreRegistry = $registry;

        $customerId = $this->getCustomerId();
        if ((int)$customerId > 0) {
            $customer = $this->customerRepositoryInterface->getById($customerId);
            if ($customer) {
                $this->currentCustomer = $customer;
                $isSeller = $customer->getCustomAttribute('is_vendor_group') ?
                    (int)$customer->getCustomAttribute('is_vendor_group')->getValue() : 0;
                //$this->showTab = $isSeller === 1 ? true : false;
                $this->showTab = true;
            }
        }
    }

    /**
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(\Magento\Customer\Controller\RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Current Warranties');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Current Warranties');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        if ($this->getCustomerId()) {
            return $this->showTab;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        if ($this->getCustomerId()) {
            return !$this->showTab;
        }
        return true;
    }

    /**
     * Tab class getter
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return '';
    }

    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return false;
    }

    /**
     * Retrieve warranties info from session or web service
     *
     * @return array
     */
    public function getWarranties()
    {
        $customer = $this->currentCustomer;
        $result = null;
        $authSession = $this->authSession->getUser();

        if ($authSession && $customer) {
            $agreementId = $customer->getCustomAttribute('user_rest_id_active_agreement') ?
                $customer->getCustomAttribute('user_rest_id_active_agreement')->getValue() : '';
            $userId = $customer->getCustomAttribute('user_rest_id') ?
                $customer->getCustomAttribute('user_rest_id')->getValue() : '';
            $token = $authSession->getUserRestAtk();
            if ($token !== '' && $agreementId !== '' && $userId !== '') {
                $result = $this->serviceData->getData(trim($token), trim($userId), trim($agreementId));
            }
        }

        return $result;
    }
}
