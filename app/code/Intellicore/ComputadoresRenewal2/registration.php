<?php
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Intellicore_ComputadoresRenewal2',
    __DIR__
);
