<?php
/**
 * @namespace   Intellicore
 * @module      ComputadoresRenewal2
 * @author      Harshil Patwa
 * @email       h.patwa@intellicore.cl
 * @date        14/10/21
 */
namespace Intellicore\ComputadoresRenewal2\Setup\Patch\Data;

use Intellicore\ComputadoresRenewal2\Constants as ComputadoresRenewal2Constants;
use MageMoto\ElasticsearchStoreSwitch\Block\Store as StoreBlock;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class StoreConfig implements DataPatchInterface
{
    /** @var StoreFactory */
    protected $_storeFactory;

    /** @var WriterInterface */
    protected $_configWriter;

    /** @var ScopeConfigInterface */
    protected $_scopeConfig;

    public function __construct(
        StoreFactory $storeFactory,
        WriterInterface $configWriter,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_storeFactory = $storeFactory;
        $this->_configWriter = $configWriter;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $store = $this->_storeFactory->create();
        $store->load(ComputadoresRenewal2Constants::STORE_VIEW_CODE);
        if ($store->getId()) {
            /** Add store to search box on front */
            $data = $this->_scopeConfig->getValue(StoreBlock::XML_PATH_EMAIL_RECIPIENT);
            $arrayValue = $data ? explode(',', $data) : [];
            $arrayValue[] = $store->getId();
            $strValue = implode(',', $arrayValue);
            $this->_configWriter->save(StoreBlock::XML_PATH_EMAIL_RECIPIENT, $strValue);

            /** Disable reviews for product on front */
            $websiteId = $store->getWebsiteId();
            $scope = ScopeInterface::SCOPE_WEBSITES;
            $this->_configWriter->save(ComputadoresRenewal2Constants::XML_PATH_REVIEW_ACTIVE, 0, $scope, $websiteId);
            $this->_configWriter->save(ComputadoresRenewal2Constants::XML_PATH_REVIEW_ALLOW_GUEST, 0, $scope, $websiteId);
        }
    }

    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [
            ComputadoresRenewalSetup::class
        ];
    }
}