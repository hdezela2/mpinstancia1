<?php
declare(strict_types=1);

namespace Intellicore\ComputadoresRenewal2\Setup\Patch\Data;

use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Directory\Model\Currency;
use Intellicore\ComputadoresRenewal2\Constants;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class AssignWebsiteCurrency implements DataPatchInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var ConfigInterface
     */
    private $configInterface;

    /**
     * @var WebsiteRepositoryInterface
     */
    private $websiteRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ConfigInterface $configInterface
     * @param WebsiteRepositoryInterface $websiteRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        ConfigInterface $configInterface,
        WebsiteRepositoryInterface $websiteRepository,
        LoggerInterface $logger
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->configInterface = $configInterface;
        $this->websiteRepository = $websiteRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function apply() {
        $this->moduleDataSetup->startSetup();
        try {
            $website = $this->websiteRepository->get(Constants::WEBSITE_CODE);
            $configs = [
                Currency::XML_PATH_CURRENCY_ALLOW,
                Currency::XML_PATH_CURRENCY_DEFAULT,
                Currency::XML_PATH_CURRENCY_BASE
            ];
            foreach ($configs as $config) {
                $this->configInterface->saveConfig(
                    $config,
                    'USD',
                    ScopeInterface::SCOPE_WEBSITES,
                    $website->getId()
                );
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        $this->moduleDataSetup->endSetup();
    }

    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [
            ComputadoresRenewalSetup::class
        ];
    }

}
