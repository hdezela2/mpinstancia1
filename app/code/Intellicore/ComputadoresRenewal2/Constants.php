<?php
declare(strict_types=1);
/**
 * @namespace   Intellicore
 * @module      ComputadoresRenewal2
 * @author      
 * @email       
 * @date        
 */
namespace Intellicore\ComputadoresRenewal2;

class Constants
{
    const ID_AGREEMENT_COMPUTERS_2021_12 = 5800307;
    const WEBSITE_NAME = 'Computadores 202201 Website';
    const WEBSITE_CODE = 'computadores202201';
    const STORE_NAME = 'Computadores 202201 Store';
    const STORE_CODE = 'computadores202201';
    const STORE_VIEW_NAME = 'Computadores 202201';
    const STORE_VIEW_CODE = 'computadores202201';
    const ROOT_CATEGORY_NAME = 'DCCP Computadores202201';
    const FRONT_THEME = 'Summa/Computadores';
    const XML_PATH_REVIEW_ACTIVE = 'catalog/review/active';
    const XML_PATH_REVIEW_ALLOW_GUEST = 'catalog/review/allow_guest';
    const XML_PATH_NUMBER_OF_DAYS = 'stock_management_configuration/general/number_of_days';
    const XML_PATH_STOCK_MANAGEMENT_ENABLED = 'stock_management_configuration/general/enable';
}