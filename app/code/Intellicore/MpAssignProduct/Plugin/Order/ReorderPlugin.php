<?php
namespace Intellicore\MpAssignProduct\Plugin\Order;
use Webkul\Mpsplitcart\Helper\Data;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Webkul\MpAssignProduct\Controller\Order\Reorder;

class ReorderPlugin
{    

    public function __construct(
        Data $mpSplitCartHelper,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->mpSplitCartHelper = $mpSplitCartHelper;
        $this->customerRepository = $customerRepository;
    }
    public function beforeProcessItem(Reorder $subject, $item, $cart)
    {   
        $info = $item->getProductOptionByCode('info_buyRequest');
        $sellerId = $this->mpSplitCartHelper->getSellerIdFromMpassign(
                        $info['mpassignproduct_id']
                    );
        $sellerData = $this->customerRepository->getById($sellerId);
        $status = $sellerData->getCustomAttribute('wkv_dccp_state_details')->getValue();
        
        if ($status != 70) {
            throw new LocalizedException(__('Sorry, The provider is disabled temporarily.'));
        }
    }
}
?>