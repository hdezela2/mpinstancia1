<?php
declare(strict_types=1);
/**
 * @namespace   Intellicore
 * @module      EmergenciasRenewal
 * @author      Dario Grau
 * @email       dario.grau@chilecompra.cl
 * @date        02/8/21
 */
namespace Intellicore\EmergenciasRenewal;

class Constants
{
    const ID_AGREEMENT_EMERGENCY_2021_09 = 5800300;
    const WEBSITE_NAME = 'Emergencias 202109 Website';
    const WEBSITE_CODE = 'emergencias202109';
    const STORE_NAME = 'Emergencias 202109 Store';
    const STORE_CODE = 'emergencias202109';
    const STORE_VIEW_NAME = 'Emergencias 202109';
    const STORE_VIEW_CODE = 'emergencias202109';
    const ROOT_CATEGORY_NAME = 'DCCP Emergencias202109';
    const FRONT_THEME = 'Summa/Emergencias';
    const FRONT_THEME_DEFAULT = 'Formax/ChileCompra';
    const XML_PATH_REVIEW_ACTIVE = 'catalog/review/active';
    const XML_PATH_REVIEW_ALLOW_GUEST = 'catalog/review/allow_guest';
    const XML_PATH_NUMBER_OF_DAYS = 'stock_management_configuration/general/number_of_days';
    const XML_PATH_STOCK_MANAGEMENT_ENABLED = 'stock_management_configuration/general/enable';
    const XML_PATH_MIN_OFFER_DAYS = 'catalog/dccp_offer/ini_range_offer';
    const MIN_OFFER_DAYS = 1;
    const XML_PATH_MAX_OFFER_DAYS = 'catalog/dccp_offer/end_range_offer';
    const MAX_OFFER_DAYS = 4;
    const XML_PATH_REQUEST_FOR_QUOTE_ACCOUNT = 'requestforquote/requestforquote_settings/requestforquote_account';
}