<?php


namespace Intellicore\EmergenciasRenewal\Setup\Patch\Data;

use Intellicore\EmergenciasRenewal\Constants;
use Magento\Authorization\Model\ResourceModel\Role\Grid\CollectionFactory;
use Magento\Authorization\Model\RulesFactory;
use Magento\Authorization\Model\ResourceModel\Role as RoleResourceModel;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchInterface;

class UserRoleSetupReplace implements DataPatchInterface
{

    const EMERGENCY_RESOURCES_REPLACE = [

        'Magento_Backend::dashboard',

        'Webkul_CategoryProductPrice::categoryproductprice',
        'Webkul_CategoryProductPrice::index',

        'Formax_PriceDispersion::pricedispersion',
        'Formax_PriceDispersion::index',

        'Magento_Analytics::analytics',
        'Magento_Analytics::analytics_api',

        'Webkul_Marketplace::marketplace',
        'Webkul_Marketplace::menu',
        'Webkul_Marketplace::seller',
        'Webkul_MpAssignProduct::product',
        'Formax_RegionalCondition::regional_condition',


        'Webkul_Mpshipping::menu',
        'Webkul_Mpshipping::mpshipping',
        'Webkul_Mpshipping::mpshippingset',

        'Webkul_Requestforquote::requestforquote',
        'Webkul_Requestforquote::quote_index',
        'Webkul_Requestforquote::index_index',

        'Magento_Catalog::catalog',
        'Magento_CatalogPermissions::catalog_magento_catalogpermissions',
        'Magento_Catalog::catalog_inventory',
        'Magento_Catalog::products',
        'Magento_PricePermissions::read_product_price',
        'Magento_PricePermissions::edit_product_price',
        'Magento_PricePermissions::edit_product_status',
        'Magento_Catalog::categories',

        'Magento_Customer::customer',
        'Magento_Customer::manage',
        'Magento_Reward::reward_balance',

        'Magento_Reports::report',
        'Magento_Reports::salesroot',
        'Formax_CustomReport::Report',

        'Magento_Backend::system',
        'Magento_Backend::convert',
        'Magento_ImportExport::export',

        'Summa_TemporaryPriceAdjustment::menu',
        'Summa_TemporaryPriceAdjustment::index',
        'Formax_PriceDispersion::history',
        'Cleverit_CategoryProductPrice::history',
    ];

    /**
     * @var RulesFactory
     */
    protected $rulesFactory;

    /**
     * @var RoleResourceModel
     */
    protected $roleResourceModel;

    /**
     * @var CollectionFactory
     */
    private $roleCollectionFactory;


    /**
     * UserRoleSetupReplace constructor.
     * @param CollectionFactory $roleCollectionFactory
     * @param RulesFactory $rulesFactory
     * @param RoleResourceModel $roleResourceModel
     */

    public function __construct(
        CollectionFactory $roleCollectionFactory,
        RulesFactory $rulesFactory,
        RoleResourceModel $roleResourceModel
    )
    {
        $this->rulesFactory = $rulesFactory;
        $this->roleResourceModel = $roleResourceModel;
        $this->roleCollectionFactory = $roleCollectionFactory;
    }

    public function apply()
    {
        $agreementId = Constants::ID_AGREEMENT_EMERGENCY_2021_09;
        $roles = $this->roleCollectionFactory->create();

        foreach ($roles->getData() as $role) {
            if ($role['role_name'] == $agreementId) {
                $this->rulesFactory->create()
                    ->setRoleId($role['role_id'])
                    ->setResources(self::EMERGENCY_RESOURCES_REPLACE)
                    ->saveRel();
            }
        }


    }


    public static function getDependencies(): array
    {
        return [
            EmergenciasRenewalSetup::class,
            UserRoleSetup::class
        ];
    }

    public function getAliases(): array
    {
        return [];
    }


}