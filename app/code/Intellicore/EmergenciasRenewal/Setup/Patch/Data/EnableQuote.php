<?php

namespace Intellicore\EmergenciasRenewal\Setup\Patch\Data;

use Intellicore\EmergenciasRenewal\Constants as EmergenciasRenewalConstants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class EnableQuote implements DataPatchInterface
{
    /** @var \Magento\Store\Model\StoreFactory */
    protected $storeFactory;

    /** @var \Magento\Framework\App\Config\Storage\WriterInterface */
    protected $configWriter;

    public function __construct(
        StoreFactory $storeFactory,
        WriterInterface $configWriter
    ) {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
    }

    public static function getDependencies(): array
    {
        return [
            EmergenciasRenewalSetup::class
        ];
    }

    /**
     * @return void
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(EmergenciasRenewalConstants::STORE_VIEW_CODE);
        if ($store->getId()) {
            $websiteId = $store->getWebsiteId();
            $scope = ScopeInterface::SCOPE_WEBSITES;
            $this->configWriter->save(
                EmergenciasRenewalConstants::XML_PATH_REQUEST_FOR_QUOTE_ACCOUNT,
                1,
                $scope,
                $websiteId
            );
        }
    }

    public function getAliases(): array
    {
        return [];
    }
}
