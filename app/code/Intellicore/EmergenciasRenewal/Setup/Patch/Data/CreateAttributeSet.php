<?php
declare(strict_types=1);
/**
 * @namespace   Intellicore
 * @module      EmergenciasRenewal
 * @author      Harshil Patwa
 * @email       h.patwa@intellicore.cl
 * @date        06/8/21
 */

namespace Intellicore\EmergenciasRenewal\Setup\Patch\Data;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Api\AttributeSetRepositoryInterface;

class CreateAttributeSet implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /** @var EavSetupFactory */
    private $eavSetupFactory;

    private $attributeSetFactory;
    private $categorySetupFactory;
    private $attributeSetRepository;

    /**
     * AddRecommendedAttribute constructor.
     *
     * @param ModuleDataSetupInterface  $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        AttributeSetFactory $attributeSetFactory, 
        CategorySetupFactory $categorySetupFactory,
        EavSetupFactory $eavSetupFactory,
        AttributeSetRepositoryInterface $attributeSetRepository
    ) {
        $this->moduleDataSetup = $moduleDataSetup;     
        $this->attributeSetFactory = $attributeSetFactory; 
        $this->categorySetupFactory = $categorySetupFactory;
        $this->eavSetupFactory = $eavSetupFactory; 
        $this->attributeSetRepository = $attributeSetRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        /** Create Attribute Set */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $categorySetup = $this->categorySetupFactory->create(['setup' => $this->moduleDataSetup]);
              
        $attributeSet = $this->attributeSetFactory->create();
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);

        /*Attribute Set Names*/
        $attributeSetData = [
            'ARRIENDO DIARIO ESTUFA',
            'ARRIENDO POR HORA CAMIÓN ALJIBE AGUA POTABLE',
            'ARRIENDO POR HORA CAMIÓN ALJIBE EXTINCIÓN DE INCENDIOS',
            'ARRIENDO SEMANAL CARPA',
            'BAÑO MODULAR PARA VIVIENDA DE EMERGENCIA',
            'BOTAS DE AGUA',
            'KIT DE ALIMENTACIÓN',
            'KIT DE ASEO DOMICILIARIO',
            'KIT DE HIGIENE',
            'SACO DEFENSA FLUVIAL',
            'TRAJE PARA AGUA PVC',
            'VIVIENDA DE EMERGENCIA CON BAÑO',
            'VIVIENDA DE EMERGENCIA SIN BAÑO'
        ];

        foreach ($attributeSetData as $value) {
            $data = [
                'attribute_set_name' => $value, 
                'entity_type_id' => $entityTypeId,
                'sort_order' => 200,
            ];

            /*Set Attribute Set Data*/
            $attributeSet->setData($data);
            $attributeSet->validate();
            $attributeSet->save();
            $attributeSet->initFromSkeleton($attributeSetId);
            $attributeSet->save();
        }

        $ATTRIBUTE_GROUP = 'General';
        $ATTRIBUTE_CODE = 'region';

        $attributeSetIds = $eavSetup->getAllAttributeSetIds($entityTypeId);
        
        foreach ($attributeSetIds as $attributeSetIdData) {
            /*Get Attribute Set Name from the Attribute Set Id*/
            $attributeSetName = $this->attributeSetRepository->get($attributeSetIdData)->getAttributeSetName();

            if (in_array($attributeSetName, $attributeSetData)) {
                $group_id = $eavSetup->getAttributeGroupId($entityTypeId, $attributeSetIdData, $ATTRIBUTE_GROUP);
                $eavSetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetIdData,
                    $group_id,
                    $ATTRIBUTE_CODE,
                    999
               );
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}