<?php
/**
 * @namespace   Intellicore
 * @module      EmergenciasRenewal
 * @author      Harshil Patwa
 * @email       h.patwa@intellicore.cl
 * @date        04/8/21
 */
namespace Intellicore\EmergenciasRenewal\Model\Source;

class SituationDropdown extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource 
{
    public function getAllOptions() {
        if ($this->_options === null) {
            /*Set the Options for Situation attribute*/
            $this->_options = [
                ['label' => __('--Select--'), 'value' => ''],
                ['label' => __('Emergencia'), 'value' => 1],
                ['label' => __('Prevención'), 'value' => 2],
                ['label' => __('Retiro en Tienda'), 'value' => 3]
            ];
        }
        return $this->_options;
    }
}