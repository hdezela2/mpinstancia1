<?php
namespace Cleverit\CategoryProductPrice\Controller\Adminhtml\Index;

use Formax\CategoryProductPrice\Helper\Data as CategoryProductPriceHelper;
use Cleverit\CategoryProductPrice\Model\ProductPriceHistory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Json\Helper\Data as JsonHelper;

class Finish extends \Webkul\CategoryProductPrice\Controller\Adminhtml\Index\Finish
{
    /** @var Context */
    protected $context;

    /** @var CategoryProductPriceHelper */
    protected $categoryProductPriceHelper;

    /** @var JsonHelper */
    protected $jsonHelper;

    public function __construct(
        Context $context,
        CategoryProductPriceHelper $categoryProductPriceHelper,
        JsonHelper $jsonHelper
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->categoryProductPriceHelper = $categoryProductPriceHelper;
        parent::__construct($context, $jsonHelper);
    }

    public function execute()
    {
        $result = [];
        $total = (int) $this->getRequest()->getParam('row');
        $skipCount = (int) $this->getRequest()->getParam('skip');
        $total = $total - $skipCount;
        $msg = '<div class="wk-mu-success wk-mu-box">';
        $msg .= __('Total %1 Product(s) Price Updated.', $total);
        $msg .= '</div>';
        $msg .= '<div class="wk-mu-note wk-mu-box">';
        $msg .= __('Finished Execution.');
        $msg .= '</div>';
        $result['msg'] = $msg;
        $result = $this->jsonHelper->jsonEncode($result);

        $productPriceHistoryId = $this->getRequest()->getParam('productPriceHistoryId');
        if ($productPriceHistoryId) {
            $this->categoryProductPriceHelper->updateCategoryProductPriceHistory($productPriceHistoryId, ProductPriceHistory::STATUS_COMPLETED);
        }

        $this->getResponse()->representJson($result);
    }
}
