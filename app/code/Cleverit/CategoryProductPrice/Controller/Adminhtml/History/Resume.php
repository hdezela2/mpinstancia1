<?php

namespace Cleverit\CategoryProductPrice\Controller\Adminhtml\History;

use Formax\CategoryProductPrice\Helper\Data as CategoryProductPriceHelper;
use Cleverit\CategoryProductPrice\Model\ProductPriceHistory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Resume extends Action
{
    /** @var PageFactory */
    protected $resultPageFactory;

    /** @var CategoryProductPriceHelper */
    protected $categoryProductPriceHelper;

    public function __construct(
        Context $context,
        CategoryProductPriceHelper $categoryProductPriceHelper,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->categoryProductPriceHelper = $categoryProductPriceHelper;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Cleverit_CategoryProductPrice::history');
    }

    /**
     * Save action.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $productPriceHistoryId = $this->getRequest()->getParam('product_price_history_id');

        if ($productPriceHistoryId) {
            $this->categoryProductPriceHelper->updateCategoryProductPriceHistory($productPriceHistoryId, ProductPriceHistory::STATUS_IN_PROGRESS);
            $resultPage = $this->resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->prepend(__('Reading Data'));
            return $resultPage;
        }
        $this->messageManager->addError(__('Unable to get the product price history selected'));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/index');
    }
}
