<?php
namespace Cleverit\CategoryProductPrice\Controller\Adminhtml\History;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Cleverit\CategoryProductPrice\Api\ProductPriceHistoryRepositoryInterface;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;

class Delete extends Action
{
    /** @var ProductPriceHistoryRepositoryInterface */
    protected $productPriceHistoryRepository;

    public function __construct(
        Context $context,
        ProductPriceHistoryRepositoryInterface $productPriceHistoryRepository
    ) {
        $this->productPriceHistoryRepository = $productPriceHistoryRepository;
        parent::__construct($context);
    }

    /**
     * Delete dispersion history.
     *
     * @return Redirect
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $id = $this->getRequest()->getParam('product_price_history_id');
        if ($id) {
            try {
                $productPriceHistory = $this->productPriceHistoryRepository->getById($id);
                $this->productPriceHistoryRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(
                    __(
                        'You have deleted product price history with ID:  %productPriceHistoryId.',
                        ['productPriceHistoryId' => $productPriceHistory ? $productPriceHistory->getEntityId() : '']
                    )
                );
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Something went wrong. Please try again later.'));
            }
        }
        $resultRedirect->setPath('*/*/');
        return $resultRedirect;
    }

    /**
     * Check Dispersion History Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Cleverit_CategoryProductPrice::history');
    }
}