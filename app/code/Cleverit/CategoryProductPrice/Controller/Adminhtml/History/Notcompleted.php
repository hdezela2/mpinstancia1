<?php
namespace Cleverit\CategoryProductPrice\Controller\Adminhtml\History;

use Formax\CategoryProductPrice\Helper\Data as CategoryProductPriceHelper;
use Cleverit\CategoryProductPrice\Model\ProductPriceHistory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Json\Helper\Data as JsonHelper;

class Notcompleted extends Action
{
    /** @var Context */
    protected $context;

    /** @var CategoryProductPriceHelper */
    protected $categoryProductPriceHelper;

    /** @var JsonHelper */
    protected $jsonHelper;

    public function __construct(
        Context $context,
        CategoryProductPriceHelper $categoryProductPriceHelper,
        JsonHelper $jsonHelper
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->categoryProductPriceHelper = $categoryProductPriceHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = [];
        $total = (int) $this->getRequest()->getParam('row');
        $skipCount = (int) $this->getRequest()->getParam('skip');
        $total = $total - $skipCount;
        $msg = '<div class="wk-mu-success wk-mu-box">';
        $msg .= __('Total %1 Product(s) Price Updated.', $total);
        $msg .= '</div>';
        $msg .= '<div class="wk-mu-note wk-mu-box">';
        $msg .= __('Execution not finished. This process will be automatically finished by a cron job.');
        $msg .= '</div>';
        $result['msg'] = $msg;
        $result = $this->jsonHelper->jsonEncode($result);

        $categoryProductPriceHistoryId = $this->getRequest()->getParam('productPriceHistoryId');
        $this->categoryProductPriceHelper->updateCategoryProductPriceHistory($categoryProductPriceHistoryId, ProductPriceHistory::STATUS_INCOMPLETED);

        $this->getResponse()->representJson($result);
    }

    /**
     * Check for is allowed.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Cleverit_CategoryProductPrice::history');
    }
}
