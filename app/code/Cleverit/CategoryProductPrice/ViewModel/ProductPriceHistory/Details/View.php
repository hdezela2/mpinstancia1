<?php

namespace Cleverit\CategoryProductPrice\ViewModel\ProductPriceHistory\Details;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Cleverit\CategoryProductPrice\Api\ProductPriceHistoryRepositoryInterface;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemCollectionFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollectionFactory;

class View implements ArgumentInterface
{
    /** @var ProductPriceHistoryRepositoryInterface */
    protected $_productPriceHistoryRepository;

    /** @var RequestInterface */
    protected $_request;

    /** @var CategoryRepositoryInterface */
    protected $_categoryRepository;

    /** @var ItemCollectionFactory */
    protected $_itemCollectionFactory;

    /** @var AssociatesCollectionFactory */
    protected $_associatesCollectionFactory;

    public function __construct(
        ProductPriceHistoryRepositoryInterface $productPriceHistoryRepository,
        CategoryRepositoryInterface $categoryRepository,
        ItemCollectionFactory $itemCollectionFactory,
        AssociatesCollectionFactory $associatesCollectionFactory,
        RequestInterface $request
    ) {
        $this->_productPriceHistoryRepository = $productPriceHistoryRepository;
        $this->_request = $request;
        $this->_categoryRepository = $categoryRepository;
        $this->_itemCollectionFactory = $itemCollectionFactory;
        $this->_associatesCollectionFactory = $associatesCollectionFactory;
    }

    public function getProductPriceHistory()
    {
        $productPriceHistoryId = $this->_request->getParam('product_price_history_id');
        return $this->_productPriceHistoryRepository->getById($productPriceHistoryId);
    }

    public function getCategories()
    {
        $productPriceHistory = $this->getProductPriceHistory();
        $categories = $productPriceHistory->getAllCategories();
        $return = '';
        foreach ($categories as $category){
            if ($return) {
                $return .= ', ' . $this->_categoryRepository->get($category)->getName();
            } else {
                $return = $this->_categoryRepository->get($category)->getName();
            }
        }
        return $return;
    }

    public function getAssignItemInfo($assignItemId)
    {
        $collection = $this->_itemCollectionFactory->create()
            ->addFieldToFilter('id', $assignItemId);

//        $collection->getSelect()->join(
//            ["cpe" => $collection->getTable('catalog_product_entity')],
//            "main_table.product_id = cpe.entity_id",
//            ['type_id']
//        );

        $collection->setPageSize(1)->getFirstItem();
        if ($collection->getSize()) {
            return $collection->getData();
        }

        $collection = $this->_associatesCollectionFactory->create()
            ->addFieldToFilter('main_table.id', $assignItemId)->setPageSize(1);
        $collection->getSelect()->join(
            ["mai" => $collection->getTable('marketplace_assignproduct_items')],
            "main_table.parent_id = mai.id",
            ['seller_id']
        );
        $collection->setPageSize(1)->getFirstItem();

        return $collection->getData();
    }
}
