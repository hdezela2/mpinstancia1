<?php

namespace Cleverit\CategoryProductPrice\Cron;

use Cleverit\CategoryProductPrice\Model\ProductPriceHistory;
use Cleverit\CategoryProductPrice\Model\Repository\ProductPriceHistoryRepository;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Psr\Log\LoggerInterface;
use Formax\CategoryProductPrice\Helper\Data as Helper;

class ResetStatusCategoryProductPriceHistory
{
    /** @var Helper */
    protected $helper;

    /** @var ProductPriceHistoryRepository */
    protected $_categoryProductPriceHistoryRepository;

    /** @var DateTime */
    protected $date;

    /** @var LoggerInterface */
    protected $_logger;

    public function __construct(
        Helper $helper,
        ProductPriceHistoryRepository $categoryProductPriceHistoryRepository,
        DateTime $date,
        LoggerInterface $logger
    ) {
        $this->helper = $helper;
        $this->_categoryProductPriceHistoryRepository = $categoryProductPriceHistoryRepository;
        $this->date = $date;
        $this->_logger = $logger;
    }

    public function execute()
    {
        // get in progress dispersion history that were not updated on the last 5 minutes
        $inProgressCategoryProductPriceHistory = $this->helper->getInProgressCategoryProductPriceHistory();

        foreach ($inProgressCategoryProductPriceHistory as $categoryProductPriceHistory) {

            $categoryProductPriceHistoryLastUpdated = $categoryProductPriceHistory->getUpdatedAt()->format("Y-m-d H:i:s");
            $date1 = strtotime($categoryProductPriceHistoryLastUpdated);
            $categoryProductPriceHistoryId = $categoryProductPriceHistory->getEntityId();

            $currentDate = $this->date->gmtDate("Y-m-d H:i:s");
            $date2 = strtotime($currentDate);
            $diff = abs($date2 - $date1);

            // If last updated is grater than 5 minutes ago, update the status to uncompleted
            if ($diff > 300) {
                $this->_logger->info('Checking category product price history ID:'. $categoryProductPriceHistoryId);
                $this->_logger->info('Current date: ' . $currentDate);
                $this->_logger->info('Last updated at: ' . $categoryProductPriceHistoryLastUpdated);

                $this->_logger->info('Updating category product price history status to incompleted');

                // set status of dispersion history to 0 so it is on status "Incomplete"
                $categoryProductPriceHistory->setStatus(ProductPriceHistory::STATUS_INCOMPLETED);
                $this->_categoryProductPriceHistoryRepository->save($categoryProductPriceHistory);
                $this->_logger->info('Successfully updated category product price history ID:'. $categoryProductPriceHistoryId);
            }
        }
    }
}
