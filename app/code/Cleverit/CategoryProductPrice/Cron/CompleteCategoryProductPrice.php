<?php

namespace Cleverit\CategoryProductPrice\Cron;

use Cleverit\CategoryProductPrice\Model\ProductPriceHistory;
use Magento\Framework\App\State;
use Psr\Log\LoggerInterface;
use Webkul\MpAssignProduct\Model\AssociatesFactory as AssociatesFactory;
use Webkul\MpAssignProduct\Model\ItemsFactory as AssignItemsFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollectionFactory;
use Cleverit\CategoryProductPrice\Model\Repository\ProductPriceHistoryRepository;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\ResourceConnection;
use Summa\CategoryProductPrice\Helper\Data as CategoryProductPriceHelper;
use Formax\CategoryProductPrice\Helper\Data as Helper;

class CompleteCategoryProductPrice
{
    /** @var State */
    protected $appState;

    /** @var Helper */
    protected $helper;

    /** @var ProductPriceHistoryRepository */
    protected $_productPriceHistoryRepository;

    /** @var AssignItemsFactory */
    protected $_assignItems;

    /** @var AssociatesCollectionFactory */
    protected $_associatesCollection;

    /** @var AssociatesFactory */
    protected $_assignAssociates;

    /** @var ProductFactory */
    protected $_productFactory;

    /** @var CategoryProductPriceHelper */
    protected $_categoryProductPriceHelper;

    /** @var ResourceConnection */
    private $connection;

    /** @var string */
    protected $offerProductTable;

    /** @var string */
    protected $assignproductItemsTable;

    public function __construct(
        State $appState,
        Helper $helper,
        ProductPriceHistoryRepository $productPriceHistoryRepository,
        AssignItemsFactory $assignItems,
        AssociatesFactory $assignAssociates,
        AssociatesCollectionFactory $associatesCollection,
        ProductFactory $productFactory,
        ResourceConnection $resourceConnection,
        CategoryProductPriceHelper $categoryProductPriceHelper,
        LoggerInterface $logger
    ) {
        $this->helper = $helper;
        $this->appState = $appState;
        $this->_productPriceHistoryRepository = $productPriceHistoryRepository;
        $this->_assignItems = $assignItems;
        $this->_associatesCollection = $associatesCollection;
        $this->_assignAssociates = $assignAssociates;
        $this->_productFactory = $productFactory;
        $this->connection = $resourceConnection->getConnection();
        $this->offerProductTable = $resourceConnection->getTableName('dccp_offer_product');
        $this->assignproductItemsTable = $resourceConnection->getTableName('marketplace_assignproduct_items');
        $this->_categoryProductPriceHelper = $categoryProductPriceHelper;
        $this->_logger = $logger;
    }

    public function execute()
    {
        // get not completed category product price history
        $notCompletedCategoryProductPriceHistory = $this->helper->getNotCompletedCategoryProductPriceHistory();

        foreach ($notCompletedCategoryProductPriceHistory as $categoryProductPriceHistory) {

            $pricePercentage = $categoryProductPriceHistory->getPricePercentage();
            $allCategories = $categoryProductPriceHistory->getAllCategories();
            $assignProductIds = $categoryProductPriceHistory->getAssignProductIds();
            $productCountActual = $categoryProductPriceHistory->getProductCountActual();
            $productCountTotal = $categoryProductPriceHistory->getProductCountTotal();
            $categoryProductPriceHistoryId = $categoryProductPriceHistory->getEntityId();

            $this->_logger->info('Completing category product price history ID: '. $categoryProductPriceHistoryId);
            $this->_logger->info('Product count total: ' . $productCountTotal);
            $this->_logger->info('Product count actual: ' . $productCountActual);

            // set status of price history to "In Progress"
            $categoryProductPriceHistory->setStatus(ProductPriceHistory::STATUS_IN_PROGRESS);

            $this->_logger->info('Start updating assign products');
            foreach ($assignProductIds as $key => $value) {

                $product = $this->_productFactory->create();
                if ($value['child']) {
                    $collection = $this->_associatesCollection->create();
                    $collection
                        ->addFieldToFilter('main_table.id', ['eq' => $value['id']])
                        ->getSelect()->join(
                            ["mai" => $collection->getTable('marketplace_assignproduct_items')],
                            "main_table.parent_id = mai.id",
                            ['seller_id']
                        );
                    $assignItem = $collection->getFirstItem();
                    $product->setId($assignItem->getParentProductId());
                } else {
                    $assignItem = $this->_assignItems->create()->load($value['id']);
                    $product->setId($assignItem->getProductId());
                }

                $codeConvenioProducto = $this->_categoryProductPriceHelper->productConvenio($product);

                $isChild = false;
                if (isset($value['child'])) {
                    $isChild = (is_string($value['child'])) ? $value['child'] == "true" : $value['child'] == true;
                }

                //for configurable product
                if ($isChild) {
                    //si es un producto configurable - tenemos en el Item el Id de la tabla AssignproductAssociated.
                    $assignProducts = $this->_assignAssociates->create()->getCollection();
                    $assignProducts->addFieldToFilter('main_table.id', $value['id']);
                    $assignProducts->getSelect()
                        ->joinInner(
                            ['mai' => $this->assignproductItemsTable],
                            'mai.id = main_table.parent_id',
                            ['seller_id']
                        );
                    foreach ($assignProducts as $assignProduct) {
                        //consulto si tiene una offerta especial activa (precio con descuento - info de otra tabla
                        $productModel = $this->_productFactory->create();
                        $productId = $assignProduct->getProductId();
                        $productModel->setId($productId);

                        // get seller id
                        $basePrice = 0;
                        $select = $this->connection->select();
                        $select->from($this->offerProductTable, ['base_price'])
                            ->where('product_id = ?', $assignProduct->getProductId())
                            ->where('start_date >= DATE(NOW())')
                            ->where('end_date <= DATE(NOW())')
                            ->where('status = 3');
                        $basePrice = $this->connection->fetchOne($select);

                        if ($basePrice) {
                            $prices = $this->_categoryProductPriceHelper->updatePriceOffer($basePrice, $assignProduct, $pricePercentage, $codeConvenioProducto);
                        } else {
                            $prices = $this->_categoryProductPriceHelper->updatePriceNoOffer($assignProduct, $pricePercentage, $codeConvenioProducto, true);
                        }
                        // $prices = $this->updatePriceNoOffer($assignItem, $percentage, $codeConvenioProducto);
                        // $this->updateLog($productModel, $assignProduct->getPrice(), $assignProduct->getSellerId(), $pricePercentage, $originalPrice, $allCategories);
                        $this->_categoryProductPriceHelper->updateLog($product, $prices['updated_price'], $assignItem->getSellerId(), $pricePercentage, $prices['original_price'], $allCategories);
                    }
                } else {

                    $basePrice = 0;
                    $select = $this->connection->select();
                    $select->from($this->offerProductTable, ['base_price'])
                        ->where('product_id = ?', $assignItem->getProductId())
                        ->where('seller_id = ?', $assignItem->getSellerId())
                        ->where('start_date >= DATE(NOW())')
                        ->where('end_date <= DATE(NOW())')
                        ->where('status = 3');

                    $basePrice = $this->connection->fetchOne($select);

                    if ($basePrice) {
                        $prices = $this->_categoryProductPriceHelper->updatePriceOffer($basePrice, $assignItem, $pricePercentage, $codeConvenioProducto);
                    } else {
                        $prices = $this->_categoryProductPriceHelper->updatePriceNoOffer($assignItem, $pricePercentage, $codeConvenioProducto, false);
                    }

                }

                $this->_categoryProductPriceHelper->updateLog($product, $prices['updated_price'], $assignItem->getSellerId(), $pricePercentage, $prices['original_price'], $allCategories);

                $this->_logger->info('Updated assign product ID: ' . $value['id']);

                // remove item from assignProductIds
                unset($assignProductIds[$key]);
                $categoryProductPriceHistory->setAssignProductIds($assignProductIds);
                // update productCountActual
                $productCountActual++;
                $categoryProductPriceHistory->setProductCountActual($productCountActual);
                $this->_logger->info('Updated ' . $productCountActual . ' of ' . $productCountTotal);
                // save changes
                $this->_productPriceHistoryRepository->save($categoryProductPriceHistory);
            }
            $this->_logger->info('Finished updating assign products');
            // update dispersionHistory status to completed
            $categoryProductPriceHistory->setStatus(ProductPriceHistory::STATUS_COMPLETED);
            $this->_productPriceHistoryRepository->save($categoryProductPriceHistory);
            $this->_logger->info('Completed category product price history ID: '. $categoryProductPriceHistoryId);
        }
    }
}
