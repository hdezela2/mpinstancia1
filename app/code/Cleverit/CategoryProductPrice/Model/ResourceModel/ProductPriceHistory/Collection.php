<?php
declare(strict_types=1);
namespace Cleverit\CategoryProductPrice\Model\ResourceModel\ProductPriceHistory;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Cleverit\CategoryProductPrice\Model\ProductPriceHistory as ProductPriceHistoryModel;
use Cleverit\CategoryProductPrice\Model\ResourceModel\ProductPriceHistory as ProductPriceHistoryResourceModel;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(ProductPriceHistoryModel::class, ProductPriceHistoryResourceModel::class);
    }
}