<?php
declare(strict_types=1);
namespace Cleverit\CategoryProductPrice\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ProductPriceHistory extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('category_product_price_history', 'entity_id');
    }
}