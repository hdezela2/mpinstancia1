<?php
declare(strict_types=1);

namespace Cleverit\CategoryProductPrice\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Cleverit\CategoryProductPrice\Api\Data\ProductPriceHistoryInterface;
use Cleverit\CategoryProductPrice\Model\ResourceModel\ProductPriceHistory as ProductPriceHistoryResource;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json;

class ProductPriceHistory extends AbstractModel implements ProductPriceHistoryInterface
{
    const STATUS_INCOMPLETED = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_IN_PROGRESS = 2;

    protected $_eventPrefix = 'product_price_history';

    /** @var Json */
    private $_json;

    protected function _construct()
    {
        $this->_init(ProductPriceHistoryResource::class);
    }

    public function __construct(
        Json $serializer,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_json = $serializer;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function setCategories(array $value)
    {
        $this->setData('categories', $this->_json->serialize($value));
    }

    public function getCategories()
    {
        return (array)$this->_json->unserialize($this->getData('categories'));
    }

    public function setAllCategories(array $value)
    {
        $this->setData('all_categories', $this->_json->serialize($value));
    }

    public function getAllCategories()
    {
        return (array)$this->_json->unserialize($this->getData('all_categories'));
    }

    public function setPricePercentage(float $value)
    {
        $this->setData('price_percentage', $value);
    }

    public function getPricePercentage()
    {
        return (float)$this->getData('price_percentage');
    }

    public function setFirstAssignProductIds(array $value)
    {
        $this->setData('first_assign_product_ids', $this->_json->serialize($value));
    }

    public function getFirstAssignProductIds()
    {
        return (array)$this->_json->unserialize($this->getData('first_assign_product_ids'));
    }

    public function setAssignProductIds(array $value)
    {
        $this->setData('assign_product_ids', $this->_json->serialize($value));
    }

    public function getAssignProductIds()
    {
        return (array)$this->_json->unserialize($this->getData('assign_product_ids'));
    }

    public function setProductCountTotal(int $value)
    {
        $this->setData('product_count_total', $value);
    }

    public function getProductCountTotal()
    {
        return (int)$this->getData('product_count_total');
    }

    public function setProductCountActual(int $value)
    {
        $this->setData('product_count_actual', $value);
    }

    public function getProductCountActual()
    {
        return (int)$this->getData('product_count_actual');
    }

    public function setUpdatedAt(\DateTime $value)
    {
        $this->setData('updated_at', $value->format('Y-m-d h:i:s'));
    }

    public function getUpdatedAt()
    {
        return new \DateTime($this->getData('updated_at'));
    }

    public function setStatus(int $value): void
    {
        $this->setData('status', $value);
    }

    public function getStatus(): ?int
    {
        return (int)$this->getData('status');
    }

    public function setCreatedAt(\DateTime $value)
    {
        $this->setData('created_at', $value->format('Y-m-d h:i:s'));
    }

    public function getCreatedAt()
    {
        return new \DateTime($this->getData('created_at'));
    }
}