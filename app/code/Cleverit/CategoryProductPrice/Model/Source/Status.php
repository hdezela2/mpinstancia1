<?php

namespace Cleverit\CategoryProductPrice\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Cleverit\CategoryProductPrice\Model\ProductPriceHistory;

class Status implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Completed'),
                'value' => ProductPriceHistory::STATUS_COMPLETED
            ],
            [
                'label' => __('Uncompleted'),
                'value' => ProductPriceHistory::STATUS_INCOMPLETED
            ],
            [
                'label' => __('In Progress'),
                'value' => ProductPriceHistory::STATUS_IN_PROGRESS
            ]
        ];
    }
}