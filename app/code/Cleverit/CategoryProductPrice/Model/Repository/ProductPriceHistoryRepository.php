<?php
declare(strict_types=1);
namespace Cleverit\CategoryProductPrice\Model\Repository;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Cleverit\CategoryProductPrice\Api\Data\ProductPriceHistoryInterface;
use Cleverit\CategoryProductPrice\Api\Data\ProductPriceHistorySearchResultsInterface;
use Cleverit\CategoryProductPrice\Api\Data\ProductPriceHistorySearchResultsInterfaceFactory as ProductPriceHistorySearchResultsFactory;
use Cleverit\CategoryProductPrice\Api\ProductPriceHistoryRepositoryInterface;
use Cleverit\CategoryProductPrice\Model\ProductPriceHistoryFactory;
use Cleverit\CategoryProductPrice\Model\ResourceModel\ProductPriceHistory as ProductPriceHistoryResource;
use Cleverit\CategoryProductPrice\Model\ResourceModel\ProductPriceHistory\CollectionFactory;

class ProductPriceHistoryRepository implements ProductPriceHistoryRepositoryInterface
{
    /** @var ProductPriceHistoryResource */
    protected $resource;

    /** @var ProductPriceHistoryFactory */
    protected $_productPriceHistoryFactory;

    /** @var CollectionFactory */
    protected $collectionFactory;

    /** @var ProductPriceHistorySearchResultsFactory */
    protected $searchResultsFactory;

    /** @var CollectionProcessorInterface */
    private $collectionProcessor;

    /** @var EmailNotification */
    private $emailNotification;

    public function __construct(
        ProductPriceHistoryResource $resource,
        ProductPriceHistoryFactory $productPriceHistoryFactory,
        CollectionFactory $collectionFactory,
        ProductPriceHistorySearchResultsFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->_productPriceHistoryFactory = $productPriceHistoryFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    public function save(ProductPriceHistoryInterface $productPriceHistory, ?int $storeId = 0)
    {
        try {
            $this->resource->save($productPriceHistory);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $productPriceHistory;
    }

    public function getById($entityId)
    {
        $productPriceHistory = $this->_productPriceHistoryFactory->create();
        $this->resource->load($productPriceHistory, $entityId);
        if (!$productPriceHistory->getId()) {
            throw new NoSuchEntityException(__('The product price history with ID: %1 doesn\'t exist.', $entityId));
        }
        return $productPriceHistory;
    }

    public function getList(SearchCriteriaInterface $criteria)
    {
        /** @var \Cleverit\CategoryProductPrice\Model\ResourceModel\ProductPriceHistory\Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var ProductPriceHistorySearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    public function delete(ProductPriceHistoryInterface $productPriceHistory)
    {
        try {
            $this->resource->delete($productPriceHistory);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    public function deleteById($entityID)
    {
        return $this->delete($this->getById($entityID));
    }
}
