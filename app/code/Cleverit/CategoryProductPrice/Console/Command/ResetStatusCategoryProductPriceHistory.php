<?php

namespace Cleverit\CategoryProductPrice\Console\Command;

use Cleverit\CategoryProductPrice\Model\ProductPriceHistory;
use Cleverit\CategoryProductPrice\Model\Repository\ProductPriceHistoryRepository;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Formax\CategoryProductPrice\Helper\Data as Helper;

class ResetStatusCategoryProductPriceHistory extends Command
{
    /** @var State */
    protected $appState;

    /** @var Helper */
    protected $helper;

    /** @var ProductPriceHistoryRepository */
    protected $_categoryProductPriceHistoryRepository;

    /** @var DateTime */
    protected $date;

    public function __construct(
        State $appState,
        Helper $helper,
        ProductPriceHistoryRepository $categoryProductPriceHistoryRepository,
        DateTime $date,
        $name = null
    ) {
        parent::__construct($name);
        $this->helper = $helper;
        $this->appState = $appState;
        $this->_categoryProductPriceHistoryRepository = $categoryProductPriceHistoryRepository;
        $this->date = $date;
    }

    protected function configure()
    {
        $this->setName('product:updateinprogresscategoryproductpricehistory');
        $this->setDescription("Update status of in progress category product price history.");

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(Area::AREA_GLOBAL);

        // get in progress dispersion history that were not updated on the last 5 minutes
        $inProgressCategoryProductPriceHistory = $this->helper->getInProgressCategoryProductPriceHistory();

        foreach ($inProgressCategoryProductPriceHistory as $categoryProductPriceHistory) {

            $categoryProductPriceHistoryLastUpdated = $categoryProductPriceHistory->getUpdatedAt()->format("Y-m-d H:i:s");
            $date1 = strtotime($categoryProductPriceHistoryLastUpdated);
            $categoryProductPriceHistoryId = $categoryProductPriceHistory->getEntityId();

            $currentDate = $this->date->gmtDate("Y-m-d H:i:s");
            $date2 = strtotime($currentDate);
            $diff = abs($date2 - $date1);

            if ($diff > 300) {
                $output->writeln('<info>Checking category product price history ID:'. $categoryProductPriceHistoryId .'</info>');
                $output->writeln('Current date: ' . $currentDate);
                $output->writeln('Last updated at: ' . $categoryProductPriceHistoryLastUpdated);

                $output->writeln('<info>Updating category product price history status to uncompleted</info>');

                // set status of dispersion history to 0 so it is on status "Incomplete"
                $categoryProductPriceHistory->setStatus(ProductPriceHistory::STATUS_INCOMPLETED);
                $this->_categoryProductPriceHistoryRepository->save($categoryProductPriceHistory);
                $output->writeln('<info>Successfully updated category product price history ID:'. $categoryProductPriceHistoryId .'</info>');
            }
        }
    }
}
