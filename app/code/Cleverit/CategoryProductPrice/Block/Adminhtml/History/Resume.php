<?php

namespace Cleverit\CategoryProductPrice\Block\Adminhtml\History;

use Magento\Backend\Block\Template\Context;

class Resume extends \Magento\Backend\Block\Template
{
    public function __construct(Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }
}
