<?php
declare(strict_types=1);
namespace Cleverit\CategoryProductPrice\Api\Data;

interface ProductPriceHistoryInterface
{
    /**
     * @param $value
     */
    public function setEntityId($value);

    /**
     * @return mixed
     */
    public function getEntityId();

    /**
     * @param array $value
     */
    public function setCategories(array $value);

    /**
     * @return array|null
     */
    public function getCategories();

    /**
     * @param array $value
     */
    public function setAllCategories(array $value);

    /**
     * @return array|null
     */
    public function getAllCategories();

    /**
     * @param float $value
     */
    public function setPricePercentage(float $value);

    /**
     * @return float|null
     */
    public function getPricePercentage();

    /**
     * @param array $value
     */
    public function setFirstAssignProductIds(array $value);

    /**
     * @return array|null
     */
    public function getFirstAssignProductIds();

    /**
     * @param array $value
     */
    public function setAssignProductIds(array $value);

    /**
     * @return array|null
     */
    public function getAssignProductIds();

    /**
     * @param int $value
     */
    public function setProductCountTotal(int $value);

    /**
     * @return int|null
     */
    public function getProductCountTotal();

    /**
     * @param int $value
     */
    public function setProductCountActual(int $value);

    /**
     * @return int|null
     */
    public function getProductCountActual();

    /**
     * @param \DateTime $value
     */
    public function setCreatedAt(\DateTime $value);

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt();

    /**
     * @param \DateTime $value
     */
    public function setUpdatedAt(\DateTime $value);

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt();

    /**
     * @param int $status
     */
    public function setStatus(int $status);

    /**
     * @return int|null
     */
    public function getStatus();
}