<?php
declare(strict_types=1);
namespace Cleverit\CategoryProductPrice\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ProductPriceHistorySearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Cleverit\CategoryProductPrice\Api\Data\ProductPriceHistoryInterface[]
     */
    public function getItems();

    /**
     * @param \Cleverit\CategoryProductPrice\Api\Data\ProductPriceHistoryInterface[] $items
     * @return \Cleverit\CategoryProductPrice\Api\Data\ProductPriceHistorySearchResultsInterface
     */
    public function setItems(array $items);
}