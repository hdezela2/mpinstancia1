<?php
declare(strict_types=1);
namespace Cleverit\CategoryProductPrice\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Cleverit\CategoryProductPrice\Api\Data\ProductPriceHistoryInterface;
use Cleverit\CategoryProductPrice\Api\Data\ProductPriceHistorySearchResultsInterface;

interface ProductPriceHistoryRepositoryInterface
{
    /**
     * @param ProductPriceHistoryInterface $productPriceHistory
     * @param int|null $storeId
     * @return void
     */
    public function save(ProductPriceHistoryInterface $productPriceHistory, ?int $storeId = 0);

    /**
     * @param $entityID
     * @return ProductPriceHistoryInterface
     */
    public function getById($entityID);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return ProductPriceHistorySearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param ProductPriceHistoryInterface $productPriceHistory
     * @return void
     */
    public function delete(ProductPriceHistoryInterface $productPriceHistory);

    /**
     * @param $entityID
     * @return void
     */
    public function deleteById($entityID);
}
