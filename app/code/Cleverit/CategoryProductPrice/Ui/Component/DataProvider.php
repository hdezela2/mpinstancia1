<?php
declare(strict_types=1);
namespace Cleverit\CategoryProductPrice\Ui\Component;

use Cleverit\CategoryProductPrice\Model\Repository\ProductPriceHistoryRepository;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider as AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    /** @var ProductPriceHistoryRepository */
    protected $productPriceHistoryRepository;

    public function __construct(
        ProductPriceHistoryRepository $productPriceHistoryRepository,
        $name,
        $primaryFieldName,
        $requestFieldName,
        ReportingInterface $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
        $this->productPriceHistoryRepository = $productPriceHistoryRepository;
    }

    public function getData()
    {
        return parent::getData();

        // TODO: Add filter to data so it only shows current CM related data.
//        $allItems = parent::getData();

//        foreach ($allItems['items'] as $item) {
//            $categoryProductPriceHistoryId = $item['entity_id'];
//            $categoryProductPriceHistory = $this->productPriceHistoryRepository->getById($categoryProductPriceHistoryId);
//            $categories = $categoryProductPriceHistory->getCategories();
//        }
//        return $allItems;
    }
}