<?php

namespace Cleverit\ChatBot\Block;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;
use Webkul\Marketplace\Helper\Data as HelperData;

class ChatBot extends Template
{
    /** @var  HelperData */
    protected $_helper;

    /** @var ScopeConfigInterface */
    protected $_scopeConfig;

    /** @var Session */
    protected $_session;

    /** @var CustomerRepositoryInterface */
    protected $_customerRepositoryInterface;

    public function __construct(
        Template\Context $context,
        HelperData $helper,
        ScopeConfigInterface $scopeConfig,
        Session $session,
        CustomerRepositoryInterface $customerRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_helper = $helper;
        $this->_scopeConfig = $scopeConfig;
        $this->_session = $session;
        $this->_customerRepositoryInterface = $customerRepository;
    }

    public function getDelayTimeout(){
        return (int)$this->_scopeConfig->getValue('chatbot_configuration/general/delay_timeout', ScopeInterface::SCOPE_STORE);
    }

    public function isModuleEnabled()
    {
        return $this->_scopeConfig->getValue('chatbot_configuration/general/enable', ScopeInterface::SCOPE_STORE);
    }

    public function getAppId()
    {
        return $this->_scopeConfig->getValue('chatbot_configuration/general/app_id', ScopeInterface::SCOPE_STORE);
    }

    public function getAccessToken()
    {
        if (!$this->isLoggedIn()){
            return false;
        }
        $customerId = (int) $this->_session->getCustomer()->getId();
        $customerLoggedIn = $this->_customerRepositoryInterface->getById($customerId);
        $token = $customerLoggedIn->getCustomAttribute('user_rest_atk') ?
            $customerLoggedIn->getCustomAttribute('user_rest_atk')->getValue() : '';
        return $token;
    }

    public function isSeller()
    {
        if (!$this->isLoggedIn()){
            return false;
        }
        return $this->_helper->isSeller();
    }

    public function isLoggedIn()
    {
        return $this->_session->isLoggedIn();
    }
}