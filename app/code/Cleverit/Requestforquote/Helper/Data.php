<?php

namespace Cleverit\Requestforquote\Helper;

use Formax\QuotesSupplier\Ui\Component\Listing\Columns\Date;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Webkul\Requestforquote\Model\QuoteFactory;
use Webkul\Requestforquote\Model\ResourceModel\Quote\CollectionFactory as QuoteCollectionFactory;

class Data extends AbstractHelper
{
    /** @var QuoteCollectionFactory */
    protected $_quoteCollection;

    /** @var TimezoneInterface */
    protected $_timezoneInterface;

    /** @var QuoteFactory */
    protected $_quote;

    /** @var ZendClientFactory */
    protected $_httpClientFactory;

    /** @var Json */
    protected $_serializerJson;

    public function __construct(
        Context $context,
        QuoteCollectionFactory $quoteCollection,
        TimezoneInterface $timezone,
        QuoteFactory $quote,
        ZendClientFactory $httpClientFactory,
        Json $serializerJson
    ) {
        parent::__construct($context);
        $this->_quoteCollection = $quoteCollection;
        $this->_timezoneInterface = $timezone;
        $this->_quote = $quote;
        $this->_httpClientFactory = $httpClientFactory;
        $this->_serializerJson = $serializerJson;
    }

    /**
     * @param $status1
     * @param null $status2
     * @return \Webkul\Requestforquote\Model\ResourceModel\Quote\Collection
     */
    public function getQuotesByStatus($agreementId, $status1, $status2 = null)
    {
        if ($agreementId) {
            $quotes = $this->getQuotesByAgreementId($agreementId);
        } else {
            $quotes = $this->_quoteCollection->create();
        }
        return $quotes->addFieldToSelect('*')
            ->addFieldToFilter('status', ['in' => [$status1, $status2]])
            ->addFieldToFilter('publication_start', ['notnull' => true]);
    }

    public function getQuoteByID($quoteId)
    {
        return $this->_quoteCollection->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('quote_id', ['eq' => $quoteId]);
    }

    public function getQuotesIdsByAgreementId($agreementId)
    {
        $quoteCollection = $this->_quoteCollection->create();
        $quoteCollection->addFieldToSelect('entity_id');
        $quoteCollection->distinct(true);
        $quoteCollection->getSelect()->join(
            ['requestforquote_quote_info'],
            'main_table.entity_id = requestforquote_quote_info.quote_id',
            ''
        );
        $quoteCollection->getSelect()->join(
            ['marketplace_userdata'],
            "requestforquote_quote_info.seller_id = marketplace_userdata.seller_id AND marketplace_userdata.shop_url like '%" . $agreementId . "%'",
            ''
        );
        $quoteCollection->getSelect()->group('main_table.entity_id');
        return $quoteCollection;
    }

    public function getQuotesByAgreementId($agreementId)
    {
        $quoteIds = $this->getQuotesIdsByAgreementId($agreementId);
        $agreementQuoteIdsArray = [];
        foreach ($quoteIds->getData() as $quote) {
            $agreementQuoteIdsArray[] = $quote['entity_id'];
        }
        $collection = $this->_quoteCollection->create();
        $collection->addFieldToFilter('entity_id', ['in' => $agreementQuoteIdsArray]);
        return $collection;
    }

    public function getIncompleteQuotesByAgreementId($agreementId)
    {
        $collection = $this->getQuotesByAgreementId($agreementId);
        return $collection->addFieldToFilter('publication_start', ['null' => true]);
    }

    /**
     * @return string
     */
    public function getCurrentDate()
    {
        return $this->_timezoneInterface->date()->format("Y-m-d H:i:s");
    }

    public function calculateBusinessDate($startDate, $days)
    {
        $response = null;
        try {
            $endpoint = rtrim($this->scopeConfig->getValue(
                'dccp_endpoint/endpoint_holidays/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ), '/');

            if ($endpoint && $startDate && (int)$days > 0) {
                $endpoint = sprintf($endpoint, $startDate, $days);
                $client = $this->_httpClientFactory->create();
                $client->setUri($endpoint);
                $client->setConfig(
                    ['maxredirects' => 0,
                        'timeout' => 30]
                );
                $response = $client->request(\Zend_Http_Client::GET)->getBody();

                $this->_logger->info('Holidays Start Date: ' . $startDate . ' Days: ' . $days . ' Service Response: ' . $response);
                $response = $this->_serializerJson->unserialize($response);
            } else {
                $this->_logger->error('Define endpoint and params of holidays webservice');
            }
        } catch(LocalizedException $e) {
            $this->_logger->warning('Holidays Error message: ' . $e->getMessage());
        } catch(\Exception $e) {
            $this->_logger->critical('Holidays Error message: ' . $e->getMessage());
        }

        return $response;
    }

    public function updateQuoteStatus($quoteId, $status)
    {
        $quote = $this->_quote->create()->load($quoteId);
        $quote->setStatus($status)->save();
    }

    /**
     * Validate if previous Friday is holiday
     * @param $endDate
     * @return mixed
     */
    public function validateEndDateFriday($endDate)
    {
        $date = $endDate->format('Y-m-d');
        $holidayArray = [
            '2020-04-11',
            '2020-05-02',
            '2020-09-19',
            '2020-12-26',
            '2021-04-03',
            '2021-05-22',
            '2021-07-17',
            '2021-09-18',
            '2022-04-16'
        ];
        if (in_array($date, $holidayArray)) {
            $endDate = $endDate->sub(new \DateInterval('P1D'));
        }
        return $endDate;
    }

    /**
     * Set console output color
     *
     * @param $color
     */
    public function setConsoleColor($color) {
        switch ($color) {
            case 'yellow':
                echo "\033[0;33m";
                break;
            case 'green':
                echo "\033[32m";
                break;
            case 'red':
                echo "\033[0;31m";
                break;
            case 'white':
                echo "\033[0m";
                break;
        }
    }

    /**
     * show a status bar in the console
     *
     * <code>
     * for($x=1;$x<=100;$x++){
     *     show_status($x, 100);
     *     usleep(100000);
     * }
     * </code>
     *
     * @param   int     $done   how many items are completed
     * @param   int     $total  how many items are to be done total
     * @param   int     $size   optional size of the status bar
     * @return  void
     *
     */
    public function showStatus($done, $total, $size = 30) {
        static $start_time;

        // if we go over our bound, just ignore it
        if($done > $total) return;

        if(empty($start_time) || $done == 0) $start_time=time();
        $now = time();
        $perc = (double) ($done/$total);
        $bar = floor($perc * $size);
        $this->setConsoleColor('white');
        $statusBar="\r[";
        $statusBar .= str_repeat("#", $bar);
        if ($bar < $size) {
            $statusBar .= "";
            $statusBar .= str_repeat(" ", $size-$bar);
        } else {
            $statusBar .= "#";
        }
        $disp = number_format($perc * 100, 0);
        $statusBar .= "]";
        $this->setConsoleColor('green');
        $statusBar .= " $disp% - $done processed of $total in total";
        echo "$statusBar";
        flush();
        // when done, send a newline
        if($done === $total) {
            echo "\n";
        }
    }

}
