<?php

namespace Cleverit\Requestforquote\Console\Command;

use AWS\CRT\Log;
use Chilecompra\LoginMp\Controller\Account\Loggedin;
use Chilecompra\LoginMp\Helper\Helper as LoginMpHelper;
use Formax\QuotesSupplier\Helper\Data as QuotesSupplierHelper;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Cleverit\Requestforquote\Helper\Data as Helper;
use Symfony\Component\Console\Question\ChoiceQuestion;

class UpdateQuoteStatus extends Command
{
    const AGREEMENT_ID = 'agreement_id';

    /** @var State */
    protected $_appState;

    /** @var Helper */
    protected $_helper;

    public function __construct(
        State $appState,
        Helper $helper,
        string $name = null
    ) {
        parent::__construct($name);
        $this->_helper = $helper;
        $this->_appState = $appState;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('quotes:updatequotestatus');
        $this->setDescription('Update quote status.');
        $this->setDefinition([
            new InputArgument(
                self::AGREEMENT_ID,
                InputArgument::OPTIONAL,
                "ID Convenio Marco"
            ),
            new InputOption(
                self::AGREEMENT_ID,
                null,
                InputOption::VALUE_OPTIONAL,
                "ID Convenio Marco"
            ),
        ]);
        parent::configure();
    }

    /**
     * CLI command description: Update quote status
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->_appState->setAreaCode(Area::AREA_GLOBAL);

        $agreementId = $input->getArgument(self::AGREEMENT_ID);
        $agreementIdOption = $input->getOption(self::AGREEMENT_ID);
        if (is_null($agreementId)) {
            if ($agreementIdOption) {
                $agreementId = $agreementIdOption;
            } else {
                $helper = $this->getHelper('question');
                $question = new ChoiceQuestion(
                    'Enter AGREEMENT ID (defaults to '.Loggedin::ID_AGREEMENT_SOFTWARE.')',
                    [
                        LoginMpHelper::ID_AGREEMENT_SOFTWARE,
                        LoginMpHelper::ID_AGREEMENT_DEPOT,
                        LoginMpHelper::ID_AGREEMENT_EMERGENCY,
                        LoginMpHelper::ID_AGREEMENT_EMERGENCY_2021_09,
                        LoginMpHelper::ID_AGREEMENT_ASEO,
                        LoginMpHelper::ID_AGREEMENT_VOUCHER,
                        LoginMpHelper::ID_AGREEMENT_FUELS,
                        LoginMpHelper::ID_AGREEMENT_FURNITURE,
                        LoginMpHelper::ID_AGREEMENT_FOODS,
                        LoginMpHelper::ID_AGREEMENT_DESKITEMS,
                        LoginMpHelper::ID_AGREEMENT_COMPUTERS_2021_01,
                        LoginMpHelper::ID_AGREEMENT_SOFTWARE_2022
                    ],
                    0
                );
                $question->setErrorMessage('AGREEMENT ID %s is invalid.');
                $agreementId = $helper->ask($input, $output, $question);
                $output->writeln('You have just selected: ' . $agreementId);
            }
        }

        if ($agreementId == LoginMpHelper::ID_AGREEMENT_SOFTWARE || LoginMpHelper::ID_AGREEMENT_SOFTWARE_2022) {
            // Check if Publicated Quotes needs to be updated to Publication Closed
            $output->writeln('<info>Checking if there are PUBLISHED quotes that needs to be updated to PUBLICATION CLOSED on CM Marco ID: ' . $agreementId . '.</info>');
            $publishedQuotes = $this->_helper->getQuotesByStatus($agreementId,QuotesSupplierHelper::PUBLISHED);
            $output->writeln('<comment>There exist(s) ' . $publishedQuotes->getSize() . ' PUBLISHED quote(s).</comment>');
            foreach ($publishedQuotes as $quote) {
                $currentDate = $this->_helper->getCurrentDate();
                $quotePubicationEndDate = $quote->getPublicationEnd();
                $diff = strtotime($currentDate) - strtotime($quotePubicationEndDate);
                if ($diff > 0) {
                    $quote->setStatus(QuotesSupplierHelper::PUBLICATION_CLOSED);
                    $quote->save();
                    $output->writeln('Updated quote ID: '. $quote->getEntityId() .' to PUBLICATION CLOSED.');
                } else {
                    $output->writeln('Quote ID: '. $quote->getEntityId() .' do not needs to be updated.');
                }
            }

            // Check if Publicated or Publicated closed Quotes need to be updated to EVALUATION
            $output->writeln('<info>Checking if there are PUBLISHED or PUBLICATION CLOSED quotes that needs to be updated to EVALUATION on CM Marco ID: ' . $agreementId . '.</info>');
            $publishedAndPublicationClosedQuotes = $this->_helper->getQuotesByStatus($agreementId,QuotesSupplierHelper::PUBLISHED, QuotesSupplierHelper::PUBLICATION_CLOSED);
            $output->writeln('<comment>There exist(s) ' . $publishedAndPublicationClosedQuotes->getSize() . ' PUBLISHED or PUBLICATION CLOSED quote(s).</comment>');
            foreach ($publishedAndPublicationClosedQuotes as $quote) {
                $currentDate = $this->_helper->getCurrentDate();
                $quoteEvaluationStartDate = $quote->getEvaluationStart();
                $diff = strtotime($currentDate) - strtotime($quoteEvaluationStartDate);
                if ($diff > 0) {
                    $quote->setStatus(QuotesSupplierHelper::EVALUATION);
                    $quote->save();
                    $output->writeln('Updated quote ID: '. $quote->getEntityId() .' to EVALUATION.');
                } else {
                    $output->writeln('Quote ID: '. $quote->getEntityId() .' do not needs to be updated.');
                }
            }

            // Check if on Evaluation Quotes needs to be updated to Evaluation Closed
            $output->writeln('<info>Checking if there are on EVALUATION quotes that needs to be updated to EVALUATION CLOSED on CM Marco ID: ' . $agreementId . '.</info>');
            $evaluationQuotes = $this->_helper->getQuotesByStatus($agreementId,QuotesSupplierHelper::EVALUATION);
            $output->writeln('<comment>There exist(s) ' . $evaluationQuotes->getSize() . ' on EVALUATION quote(s).</comment>');
            foreach ($evaluationQuotes as $quote) {
                $currentDate = $this->_helper->getCurrentDate();
                $quoteEvaluationEndDate = $quote->getEvaluationEnd();
                $diff = strtotime($currentDate) - strtotime($quoteEvaluationEndDate);
                if ($diff > 0) {
                    $quote->setStatus(QuotesSupplierHelper::EVALUATION_CLOSED);
                    $quote->save();
                    $output->writeln('Updated quote ID: '. $quote->getEntityId() .' to EVALUATION CLOSED.');
                } else {
                    $output->writeln('Quote ID: '. $quote->getEntityId() .' do not needs to be updated.');
                }
            }
        } else {
            $output->writeln('<error>There\'s no logic for updating quotes on CM Marco ID: ' . $agreementId . '.</error>');
        }
    }
}
