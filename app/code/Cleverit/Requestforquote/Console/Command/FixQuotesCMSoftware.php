<?php
namespace Cleverit\Requestforquote\Console\Command;

use Chilecompra\LoginMp\Controller\Account\Loggedin;
use Cleverit\Requestforquote\Logger\Logger;
use Formax\ConfigCmSoftware\Helper\Data as CmSoftwareHelper;
use Formax\QuotesSupplier\Helper\Data as QuotesSupplierHelper;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Cleverit\Requestforquote\Helper\Data as Helper;
use Symfony\Component\Console\Question\Question;
use Webkul\Requestforquote\Model\QuoteFactory;

class FixQuotesCMSoftware extends Command
{
    const SECURITY_CODE = 'code';
    const SECURITY_CODE_VALUE_CONFIG = 'dccp_command_line_security/general/security_code';

    /** @var State */
    protected $_appState;

    /** @var Helper */
    protected $_helper;

    /** @var Logger */
    protected $_logger;

    /** @var TimezoneInterface */
    protected $_timezoneInterface;

    /** @var QuoteFactory */
    protected $_quoteFactory;

    /** @var CmSoftwareHelper */
    private $_cmSoftwareHelper;

    public function __construct(
        State $appState,
        Helper $helper,
        QuoteFactory $quote,
        TimezoneInterface $timezone,
        Logger $logger,
        CmSoftwareHelper $cmSoftwareHelper,
        string $name = null
    ) {
        parent::__construct($name);
        $this->_helper = $helper;
        $this->_appState = $appState;
        $this->_logger = $logger;
        $this->_timezoneInterface = $timezone;
        $this->_quoteFactory = $quote;
        $this->_cmSoftwareHelper = $cmSoftwareHelper;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('quotes:fixquotescmsoftware');
        $this->setDescription('Fix all quotes on CM Software. Updates the publication_end, evaluation_start, evaluation_end and status.');
        $this->setDefinition([
            new InputArgument(
                self::SECURITY_CODE,
                InputArgument::OPTIONAL,
                "Security code"
            ),
            new InputOption(
                self::SECURITY_CODE,
                null,
                InputOption::VALUE_OPTIONAL,
                "Security code"
            ),
        ]);
        parent::configure();
    }

    /**
     * CLI command description: Fix all quotes on CM Software. Updates the publication_end, evaluation_start, evaluation_end and status.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $publicationEndDate = '';
        $evaluationEndDate = '';
        $questionEndDate = '';
        $this->_appState->setAreaCode(Area::AREA_GLOBAL);
        $securityCodeValue = $this->_cmSoftwareHelper->getSystemValue(self::SECURITY_CODE_VALUE_CONFIG);

        $securityCode = $input->getArgument(self::SECURITY_CODE);
        $securityCodeOption = $input->getOption(self::SECURITY_CODE);
        if (is_null($securityCode)) {
            if ($securityCodeOption) {
                $securityCode = $securityCodeOption;
            } else {
                $helper = $this->getHelper('question');
                $question = new Question('Enter the security code: ');
                $question->setValidator(function ($value) use ($securityCodeValue) {
                    if (trim($value) == '') {
                        throw new \Exception('The security code cannot be empty');
                    }
                    if (trim($value) != $securityCodeValue) {
                        throw new \Exception('The security code is incorrect');
                    }
                    return $value;
                });
                $question->setHidden(true);
                $question->setHiddenFallback(false);
                $question->setMaxAttempts(3);
                $securityCode = $helper->ask($input, $output, $question);
            }
        }
        if ($securityCode != $securityCodeValue) {
            throw new \RuntimeException(
                'The security code is incorrect.'
            );
        }

        // Check all quotes from CM Software that needs to complete all dates data.
        $this->_helper->setConsoleColor('green');
        echo "\nChecking all quotes from CM Software that needs to fix the publication_end, evaluation_start, evaluation_end and status.\n\n";
        $this->_logger->info('Checking all quotes from CM Software that needs to fix the publication_end, evaluation_start, evaluation_end and status.');

        $quotes =  $this->_helper->getIncompleteQuotesByAgreementId(Loggedin::ID_AGREEMENT_SOFTWARE);
        $quotesSize = $quotes->getSize();
        $this->_logger->info('There exist(s) ' . $quotesSize . ' quote(s) from CM Software that needs to fix the publication_end, evaluation_start, evaluation_end and status.');
        $this->_helper->setConsoleColor('white');
        echo "There exist(s) " . $quotesSize . " quote(s) from CM Software that needs to fix the publication_end, evaluation_start, evaluation_end and status.\n";

        if ($quotesSize) {
            $currentCount = 1;
            $this->_helper->showStatus(0, $quotesSize);
            foreach ($quotes as $quote) {
                if (!$quote->getPublicationStart()) {
                    if ($currentCount == 1) {
                        $this->_logger->info('-----------------------------------------------------------------------');
                    }
                    $this->_logger->info('Updating QUOTE ID: ' . $quote->getEntityId());

                    $createdAt = $quote->getCreatedAt();
                    $this->_logger->info('Created At: ' . $createdAt);
                    $createdAtDate = $this->_timezoneInterface->date(strtotime($createdAt))->format('d-m-Y');
                    $this->_logger->info('Created At Date: ' . $createdAtDate);
                    $publicationTerm = $quote->getPublicationTerm();
                    $this->_logger->info('Publication Term: ' . $publicationTerm);
                    $evaluationTerm = $quote->getEvaluationTerm();
                    $this->_logger->info('Evaluation Term: ' . $evaluationTerm);
                    $makeQuestionTerm = $quote->getMakeQuestionTerm();
                    $this->_logger->info('Make Question Term: ' . $makeQuestionTerm);

                    //Publication start day begins at next workable day from created at date
                    $response = $this->_helper->calculateBusinessDate($createdAtDate, 1);
                    if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                        $date = $response['payload']['fecha_termino'];
                        $createdAtDate = new \DateTime($date);
                    } else {
                        $this->_logger->info('An error occurred trying to get the publication start date.');
                    }
                    $this->_logger->info('Publication Start (new Created At): ' . $createdAtDate->format('Y-m-d H:i:s'));

                    // Get publication end date
                    $response = $this->_helper->calculateBusinessDate($createdAtDate->format('d-m-Y'), $publicationTerm);
                    if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                        $date = $response['payload']['fecha_termino'];
                        $publicationEndDate = new \DateTime($date);
                    } else {
                        $this->_logger->info('An error occurred trying to get the publication end date.');
                    }
                    $this->_logger->info('Publication End: ' . $publicationEndDate->format('Y-m-d H:i:s'));

                    // The evaluation start date will be the same of the publication end date since it starts at 00.00
                    $evaluationStartDate = clone $publicationEndDate;
                    $this->_logger->info('Evaluation Start: ' . $evaluationStartDate->format('Y-m-d H:i:s'));

                    //If PublicationEndDate is monday it must be replaced for the latest saturday
                    if ($publicationEndDate->format('N') == "1") {
                        $this->_logger->info('Publication End date is Monday, so we replace it for the last Saturday');
                        $publicationEndDate = $publicationEndDate->sub(new \DateInterval('P2D'));
                        $publicationEndDate = $this->_helper->validateEndDateFriday($publicationEndDate);
                        $this->_logger->info('Publication End (updated): ' . $publicationEndDate->format('Y-m-d H:i:s'));
                        $this->_logger->info('(Checking) Evaluation Start: ' . $evaluationStartDate->format('Y-m-d H:i:s'));
                    }

                    // Calculate evaluation end date, we add 1 day because we start counting the term days 1 day after the evaluation starts
                    $response = $this->_helper->calculateBusinessDate($evaluationStartDate->format('d-m-Y'), $evaluationTerm+1);
                    if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                        $date = $response['payload']['fecha_termino'];
                        $evaluationEndDate = new \DateTime($date);
                    } else {
                        $this->_logger->info('An error occurred trying to get the evaluation end date.');
                    }
                    $this->_logger->info('Evaluation End: ' . $evaluationEndDate->format('Y-m-d H:i:s'));

                    //If EvaluationEndDate is monday it must be replaced for the latest saturday
                    if ($evaluationEndDate->format('N') == "1"){
                        $this->_logger->info('Evaluation End date is Monday, so we replace it for the last Saturday');
                        $evaluationEndDate = $evaluationEndDate->sub(new \DateInterval('P2D'));
                        $evaluationEndDate = $this->_helper->validateEndDateFriday($evaluationEndDate);
                        $this->_logger->info('Evaluation End (updated): ' . $evaluationEndDate->format('Y-m-d H:i:s'));
                    }

                    // Get make questions date
                    $response = $this->_helper->calculateBusinessDate($createdAtDate->format('d-m-Y'), $makeQuestionTerm);
                    if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
                        $date = $response['payload']['fecha_termino'];
                        $questionEndDate = new \DateTime($date);
                    } else {
                        $this->_logger->info('An error occurred trying to get the make questions end date.');
                    }
                    $this->_logger->info('Question End: ' . $questionEndDate->format('Y-m-d H:i:s'));

                    $model = $this->_quoteFactory->create()->load($quote->getEntityId());
                    $data['publication_start'] = $createdAtDate->format('Y-m-d');
                    $data['publication_end'] = $publicationEndDate->format('Y-m-d');
                    $data['evaluation_start'] = $evaluationStartDate->format('Y-m-d');
                    $data['evaluation_end'] = $evaluationEndDate->format('Y-m-d');
                    $data['question_end'] = $questionEndDate->format('Y-m-d');
                    $data['entity_id'] = $quote->getEntityId();

                    $currentDate = $this->_helper->getCurrentDate();
                    if (in_array($quote->getStatus(), [0, QuotesSupplierHelper::PUBLISHED])) {
                        if ( strtotime($currentDate) > $evaluationEndDate->getTimestamp() ) {
                            $data['status'] = QuotesSupplierHelper::EVALUATION_CLOSED;
                        } elseif ( strtotime($currentDate) > $evaluationStartDate->getTimestamp() ) {
                            $data['status'] = QuotesSupplierHelper::EVALUATION;
                        } elseif ( strtotime($currentDate) > $publicationEndDate->getTimestamp() ) {
                            $data['status'] = QuotesSupplierHelper::PUBLICATION_CLOSED;
                        } else {
                            $data['status'] = QuotesSupplierHelper::PUBLISHED;
                        }
                    }
                    $this->_logger->info('Data to be updated: ');
                    $this->_logger->info(print_r($data, true));

                    $model->setData($data);
                    $model->save();
                    $this->_logger->info('-----------------------------------------------------------------------');
                }
                $this->_helper->showStatus($currentCount, $quotesSize);
                $currentCount++;
            }
        }
    }
}
