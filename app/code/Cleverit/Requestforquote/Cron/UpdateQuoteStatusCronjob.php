<?php

namespace Cleverit\Requestforquote\Cron;

use Chilecompra\LoginMp\Controller\Account\Loggedin;
use Formax\QuotesSupplier\Helper\Data as QuotesSupplierHelper;
use Cleverit\Requestforquote\Helper\Data as Helper;
use Cleverit\Requestforquote\Logger\Logger;

class UpdateQuoteStatusCronjob
{
    const AGREEMENTS_TO_UPDATE_QUOTE_STATUS = [
        Loggedin::ID_AGREEMENT_SOFTWARE_2022,
        Loggedin::ID_AGREEMENT_SOFTWARE
    ];

    /** @var Helper */
    protected $_helper;

    /** @var Logger $logger */
    protected $_logger;

    public function __construct(
        Helper $helper,
        Logger $logger
    )
    {
        $this->_helper = $helper;
        $this->_logger = $logger;
    }

    /**
     * Cronjob Description
     *
     * @return void
     */
    public function execute(): void
    {
        // THIS CRON WILL RUN FOR CM SOFTWARE ONLY //
        foreach (self::AGREEMENTS_TO_UPDATE_QUOTE_STATUS as $agreementId) {
            $this->_logger->info('UpdateQuoteStatusCronjob running on Quotes of CM with Agreement Id: ' . $agreementId);
            // Check if Published Quotes needs to be updated to Publication Closed
            $this->_logger->info('Checking if there are PUBLISHED quotes that needs to be updated to PUBLICATION CLOSED.');
            $publishedQuotes = $this->_helper->getQuotesByStatus($agreementId, QuotesSupplierHelper::PUBLISHED);
            $this->_logger->info('There exist(s) ' . $publishedQuotes->getSize() . ' PUBLISHED quote(s).');
            foreach ($publishedQuotes as $quote) {
                $currentDate = $this->_helper->getCurrentDate();
                $quotePubicationEndDate = $quote->getPublicationEnd();
                $diff = strtotime($currentDate) - strtotime($quotePubicationEndDate);
                if ($diff > 0) {
                    $quote->setStatus(QuotesSupplierHelper::PUBLICATION_CLOSED);
                    $quote->save();
                    $this->_logger->info('Updated quote ID: ' . $quote->getEntityId() . ' to PUBLICATION CLOSED.');
                } else {
                    $this->_logger->info('Quote ID: ' . $quote->getEntityId() . ' do not needs to be updated.');
                }
            }

            // Check if Publicated or Publicated closed Quotes need to be updated to EVALUATION
            $this->_logger->info('Checking if there are PUBLISHED or PUBLICATION CLOSED quotes that needs to be updated to EVALUATION.');
            $publishedAndPublicationClosedQuotes = $this->_helper->getQuotesByStatus($agreementId, QuotesSupplierHelper::PUBLISHED, QuotesSupplierHelper::PUBLICATION_CLOSED);
            $this->_logger->info('There exist(s) ' . $publishedAndPublicationClosedQuotes->getSize() . ' PUBLISHED or PUBLICATION CLOSED quote(s).');
            foreach ($publishedAndPublicationClosedQuotes as $quote) {
                $currentDate = $this->_helper->getCurrentDate();
                $quoteEvaluationStartDate = $quote->getEvaluationStart();
                $diff = strtotime($currentDate) - strtotime($quoteEvaluationStartDate);
                if ($diff > 0) {
                    $quote->setStatus(QuotesSupplierHelper::EVALUATION);
                    $quote->save();
                    $this->_logger->info('Updated quote ID: ' . $quote->getEntityId() . ' to EVALUATION.');
                } else {
                    $this->_logger->info('Quote ID: ' . $quote->getEntityId() . ' do not needs to be updated.');
                }
            }

            // Check if on Evaluation Quotes needs to be updated to Evaluation Closed
            $this->_logger->info('Checking if there are on EVALUATION quotes that needs to be updated to EVALUATION CLOSED.');
            $evaluationQuotes = $this->_helper->getQuotesByStatus($agreementId, QuotesSupplierHelper::EVALUATION);
            $this->_logger->info('There exist(s) ' . $evaluationQuotes->getSize() . ' on EVALUATION quote(s).');
            foreach ($evaluationQuotes as $quote) {
                $currentDate = $this->_helper->getCurrentDate();
                $quoteEvaluationEndDate = $quote->getEvaluationEnd();
                $diff = strtotime($currentDate) - strtotime($quoteEvaluationEndDate);
                if ($diff > 0) {
                    $quote->setStatus(QuotesSupplierHelper::EVALUATION_CLOSED);
                    $quote->save();
                    $this->_logger->info('Updated quote ID: ' . $quote->getEntityId() . ' to EVALUATION CLOSED.');
                } else {
                    $this->_logger->info('Quote ID: ' . $quote->getEntityId() . ' do not needs to be updated.');
                }
            }
        }
    }
}
