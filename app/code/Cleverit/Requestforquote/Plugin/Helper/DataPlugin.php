<?php
namespace Cleverit\Requestforquote\Plugin\Helper;

use \Webkul\Requestforquote\Helper\Data as RfqHelper;

class DataPlugin
{
    public function beforeGetDateTimeAccordingTimeZone(RfqHelper $subject, $dateTime)
    {
        $dateTime = new \DateTime($dateTime);
        return [$dateTime->getTimestamp()];
    }
}