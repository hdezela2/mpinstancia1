<?php
namespace Cleverit\Requestforquote\Plugin\Controller\Seller;

use Formax\QuotesSupplier\Helper\Data as QuotesSupplierHelper;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Message\ManagerInterface;
use Webkul\Requestforquote\Controller\Seller\View;
use Webkul\Requestforquote\Model\InfoFactory;
use Webkul\Requestforquote\Model\QuoteFactory;

class ViewPlugin
{
    /** @var QuoteFactory */
    protected $_quote;

    /** @var InfoFactory */
    protected $_info;

    /** @var ManagerInterface */
    protected $_messageManager;

    /** @var RedirectInterface */
    protected $_redirect;

    public function __construct(
        QuoteFactory $quote,
        InfoFactory $info,
        ManagerInterface $messageManager,
        RedirectInterface $redirect
    ){
        $this->_quote = $quote;
        $this->_info = $info;
        $this->_messageManager = $messageManager;
        $this->_redirect = $redirect;
    }

    public function aroundExecute(View $subject, callable $proceed)
    {
        $id = $subject->getRequest()->getParam('id');
        $sellerQuote = $this->getQuoteInfoById($id);
        $quoteId = $sellerQuote['quote_id'];
        $quote = $this->getQuoteById($quoteId);
        $quoteStatus = $quote['status'];
        if (!in_array($quoteStatus,[QuotesSupplierHelper::PUBLISHED, QuotesSupplierHelper::PENDING])) {
            $this->_messageManager->addWarningMessage(__('The publication period for quote #%1 expired', $quoteId));
            $this->_redirect->redirect($subject->getResponse(), 'requestforquote/seller/lists/');
        }
        return $proceed();
    }

    private function getQuoteInfoById($id)
    {
        return $this->_info->create()->load($id);
    }

    private function getQuoteById($quoteId)
    {
        return $this->_quote->create()->load($quoteId);
    }
}