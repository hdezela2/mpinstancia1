<?php
/**
 * @namespace   Cleverit
 * @module      ComputadoresRenewal
 * @author      Dario Grau
 * @email       dario.grau@chilecompra.cl
 * @date        7/1/21
 */
namespace Cleverit\ComputadoresRenewal\Setup\Patch\Data;

use Cleverit\ComputadoresRenewal\Constants as ComputadoresRenewalConstants;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Store\Model\WebsiteRepository;
use Magento\Authorization\Model\RoleFactory;
use Magento\Authorization\Model\RulesFactory;

class UserRoleSetup implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    protected $_moduleDataSetup;

    /** @var EavSetupFactory */
    protected $_eavSetupFactory;

    /** @var WebsiteRepository */
    protected $_websiteRepository;

    /** @var RoleFactory */
    protected $_roleFactory;

    /** @var RulesFactory */
    protected $_rulesFactory;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        WebsiteRepository $websiteRepository,
        RoleFactory $roleFactory,
        RulesFactory $rulesFactory
    ) {
        $this->_moduleDataSetup = $moduleDataSetup;
        $this->_eavSetupFactory = $eavSetupFactory;
        $this->_websiteRepository = $websiteRepository;
        $this->_roleFactory = $roleFactory;
        $this->_rulesFactory = $rulesFactory;
    }

    public function apply()
    {
        $computadoresRenewalAgreementId = ComputadoresRenewalConstants::ID_AGREEMENT_COMPUTERS_2021_01;

        $website = $this->_websiteRepository->get(ComputadoresRenewalConstants::WEBSITE_CODE);

        ##### CREATE USER ROLE #####
        $role = $this->_roleFactory->create();
        // Set Role Name with the Agreement ID of the new Convenio Marco
        $role->setName($computadoresRenewalAgreementId)
            ->setPid(0) //set parent role id of your role
            ->setRoleType(RoleGroup::ROLE_TYPE)
            ->setUserType(UserContextInterface::USER_TYPE_ADMIN)
            ->setGwsIsAll(0) //Doesn't have permission in all Stores
            ->setGwsWebsites($website->getId()) //array with the website ids
            ->setGwsStoreGroups($website->getId());

        $role->save();
        /* Now we set that which resources we allow to this role */
        $resource=[
            'Magento_Backend::dashboard',

            'Webkul_CategoryProductPrice::categoryproductprice',
            'Webkul_CategoryProductPrice::index',

            'Formax_PriceDispersion::pricedispersion',
            'Formax_PriceDispersion::index',

            'Magento_Analytics::analytics',
            'Magento_Analytics::analytics_api',

            'Webkul_Marketplace::marketplace',
            'Webkul_Marketplace::menu',
            'Webkul_Marketplace::seller',
            'Webkul_MpAssignProduct::product',
            'Formax_RegionalCondition::regional_condition',

            'Webkul_Mpshipping::menu',
            'Webkul_Mpshipping::mpshipping',
            'Webkul_Mpshipping::mpshippingset',

            'Webkul_Requestforquote::requestforquote',
            'Webkul_Requestforquote::quote_index',
            'Webkul_Requestforquote::index_index',

            'Magento_Catalog::catalog',
            'Magento_CatalogPermissions::catalog_magento_catalogpermissions',
            'Magento_Catalog::catalog_inventory',
            'Magento_Catalog::products',
            'Magento_PricePermissions::read_product_price',
            'Magento_PricePermissions::edit_product_price',
            'Magento_PricePermissions::edit_product_status',
            'Magento_Catalog::categories',

            'Magento_Customer::customer',
            'Magento_Customer::manage',
            'Magento_Reward::reward_balance',

            'Magento_Reports::report',
            'Magento_Reports::salesroot',
            'Formax_CustomReport::Report',

            'Magento_Backend::system',
            'Magento_Backend::convert',
            'Magento_ImportExport::export'

        ];
        /* Array of resource ids which we want to allow this role*/
        $this->_rulesFactory->create()->setRoleId($role->getId())->setResources($resource)->saveRel();

        return $this;
    }

    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [
            ComputadoresRenewalSetup::class
        ];
    }
}
