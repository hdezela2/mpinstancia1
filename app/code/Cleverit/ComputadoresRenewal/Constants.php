<?php
declare(strict_types=1);
/**
 * @namespace   Cleverit
 * @module      ComputadoresRenewal
 * @author      Dario Grau
 * @email       dario.grau@chilecompra.cl
 * @date        4/1/21
 */
namespace Cleverit\ComputadoresRenewal;

class Constants
{
    const ID_AGREEMENT_COMPUTERS_2021_01 = 5800289;
    const WEBSITE_NAME = 'Computadores 202101 Website';
    const WEBSITE_CODE = 'computadores202101';
    const STORE_NAME = 'Computadores 202101 Store';
    const STORE_CODE = 'computadores202101';
    const STORE_VIEW_NAME = 'Computadores 202101 Store View';
    const STORE_VIEW_CODE = 'computadores202101';
    const ROOT_CATEGORY_NAME = 'Computadores 202101 - Default Category';
    const FRONT_THEME = 'Summa/Computadores';
}