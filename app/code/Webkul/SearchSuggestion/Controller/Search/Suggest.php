<?php
/**
 * Webkul Software.
 * Webkul SearchSuggestion Controller.
 * @category  Webkul
 * @package   Webkul_SearchSuggestion
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\SearchSuggestion\Controller\Search;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Search\Model\AutocompleteInterface;
use Magento\Framework\Controller\ResultFactory;

class Suggest extends Action
{
    /**
     * @var  \Magento\Search\Model\AutocompleteInterface
     */
    private $autocomplete;
    /**
     * @var  \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $product;
    /**
     * @var  \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var  \Magento\Search\Model\AutocompleteInterface
     */
    private $priceCurrency;
    /**
     * @var  \Magento\Search\Model\AutocompleteInterface
     */
    private $reviewFactory;
    /**
     * @var  \Magento\Backend\Block\Template\Context
     */
    private $scopeConfig;
    /**
     * @var  \Magento\Catalog\Model\CategoryFactory
     */
    private $category;
    /**
     * @var  \Magento\Catalog\Helper\Image $imageHelper
     */
    private $imageHelper;

    /**
     * @param  Context                                                        $context,
     * @param  AutocompleteInterface                                          $autocomplete,
     * @param  \Magento\Backend\Block\Template\Context                        $context1,
     * @param  \Magento\Review\Model\ReviewFactory                            $reviewFactory,
     * @param  \Magento\Framework\Pricing\PriceCurrencyInterface              $priceCurrency,
     * @param  \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $product,
     * @param  \Magento\Catalog\Model\CategoryFactory                         $category,
     * @param  \Magento\Catalog\Helper\Image                                  $imageHelper
     */
    public function __construct(
        Context $context,
        AutocompleteInterface $autocomplete,
        \Magento\Backend\Block\Template\Context $context1,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $product,
        \Magento\Catalog\Model\CategoryFactory $category,
        \Magento\Catalog\Helper\Image $imageHelper
    ) {
        $this->priceCurrency = $priceCurrency;
        $this->autocomplete = $autocomplete;
        $this->product = $product;
        $this->storeManager = $context1->getStoreManager();
        $this->scopeConfig = $context1->getScopeConfig();
        $this->reviewFactory = $reviewFactory;
        $this->category = $category;
        $this->imageHelper = $imageHelper;
        parent::__construct($context);
        $this->_values = $this->getValues();
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if (!$this->getRequest()->getParam('q', false)) {
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_url->getBaseUrl());
            return $resultRedirect;
        }
        $i=1;
        //$autocompleteData = $this->autocomplete->getItems();
        $responseData = [];
        /*foreach ($autocompleteData as $resultItem) {
            if (($i <= $this->_values['show_terms'] && $this->_values['display_terms'] == 1) ||
                ($this->_values['show_terms'] == '' && $this->_values['display_terms'] == 1)) {
                if ($this->_values['display_terms_num'] == 0) {
                    $resultItem['num_results']=" ";
                }
                $responseData[] = $resultItem->toArray();
            }
            $i++;
        }*/
        $curr=$this->storeManager->getStore()->getCurrentCurrency()->getCode();
        $key=$this->getRequest()->getParam('q');
        $data=[];
        $productData=[];
        if ($this->_values['show_products'] == '') {
            $this->_values['show_products']=2;
        }
        if ($this->_values['display_product'] == 1 && $this->_values['show_products']>0) {
            $data = $this->product->create()->addAttributeToSelect('*');
            $data->addFieldToFilter('status', '1');
            $data->addFieldToFilter('visibility', ['neq' => '1']);
            $data->addFieldToFilter('name', ['like' => '%'.$key.'%']);
            $data->addStoreFilter($this->storeManager->getStore());
            $data->setPageSize($this->_values['show_products']);
        }
        $category_items=[];
        foreach ($data as $key => $pro) {
            $this->reviewFactory->create()->getEntitySummary($pro, $this->storeManager->getStore()->getId());
            $productData[$pro->getId()]['rate'] = $pro->getRatingSummary()->getRatingSummary();
            $productData[$pro->getId()]['name']=$pro->getName();
            $productData[$pro->getId()]['price']=$pro->getprice();
            $productData[$pro->getId()]['id']=$pro->getId();
            $productData[$pro->getId()]['product_url']=$pro->getProductUrl();
            $productData[$pro->getId()]['currency']=$pro->getCurrency();
            $categoryid=$pro->getCategoryIds();
            if ($pro->getVisibility() == 4) {
                $category_items[$pro->getId()]=$this->getCategoryData($categoryid, $pro);
            }
            $image = 'category_page_list';
            $productData[$pro->getId()]['image_url'] = $this->imageHelper
                ->init($pro, $image)
                ->constrainOnly(false)
                ->keepAspectRatio(true)
                ->keepFrame(false)
                ->resize(50)
                ->getUrl();
            $current_date=date('Y-m-d H:i:s');
            $specialFromDate=$pro->getSpecialFromDate();
            $specialToDate=$pro->getSpecialToDate();
            if ($pro->getSpecialPrice() && (($current_date >= $specialFromDate) &&
                ( $specialToDate >= $current_date))) {
                $productData[$pro->getId()]['price']=$this->getFormatedPrice($pro->getSpecialPrice(), $curr);
            } else {
                $productData[$pro->getId()]['price']=$this->getFormatedPrice($pro->getprice(), $curr);
            }
        }
        $allData['category']=$category_items;
        $allData['terms'] =$responseData;
        $allData['items'] =$productData;
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($allData);
        return $resultJson;
    }
    
    /**
     * Get Formated Price.
     *
     * @return Formated price
     */

    private function getFormatedPrice($price, $currency)
    {
        $precision = 2;
        return $this->priceCurrency->format(
            $price,
            $includeContainer = true,
            $precision,
            $scope = null,
            $currency
        );
    }

    /**
     * Get Sytem Configuration Search Settings.
     *
     * @return array
     */

    private function getValues()
    {
        $options = [];
        $sectionId = 'searchsuggestion';
        $groupId = 'settings';
        $optionArray = [
                        'display_terms',
                        'display_product',
                        'display_categorie',
                        'show_terms',
                        'show_products',
                        'display_terms_num'
                    ];
        foreach ($optionArray as $option) {
            $value = $sectionId.'/'.$groupId.'/'.$option;
            $value = $this->scopeConfig->getValue($value, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $options[$option] = $value;
        }

        return $options;
    }

    /**
     * Get Category Data.
     *
     * @return Category Data Array
     */

    private function getCategoryData($categoryid, $pro)
    {
        $cat_data = [];
        if ($this->_values['display_categorie'] == 1) {
            foreach ($categoryid as $cat) {
                if ($cat != $this->storeManager->getStore()->getRootCategoryId()) {
                    $category = $this->loadCategory($cat);
                    $cat_data['cat'] = $pro->getName();
                    $cat_data['cat_name'] = $category->getName();
                    $cat_data['cat_url'] = $category->getUrl();
                }
            }
        }
        return $cat_data;
    }

    /**
     * load category
     *
     * @param int $cat
     * @return CategoryFactory
     */
    private function loadCategory($cat)
    {
        return $this->category->create()->load($cat);
    }
}
