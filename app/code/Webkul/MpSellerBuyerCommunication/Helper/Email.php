<?php
/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MpSellerBuyerCommunication
 * @author      Webkul
 * @copyright   Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerBuyerCommunication\Helper;

/**
 * MpSellerBuyerCommunication Email helper
 */
class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_EMAIL_ASK_PRODUCT_QUERY_TO_ADMIN = 'mpsellerbuyercommunication/email/askproductquery_admin_template';
    const XML_PATH_EMAIL_ASK_PRODUCT_QUERY_TO_CUSTOMER   = 'mpsellerbuyercommunication/email/askproductquery_customer_template';
    const XML_PATH_EMAIL_CONVERSATION_TO_ADMIN   = 'mpsellerbuyercommunication/email/conversation_admin_template';
    const XML_PATH_EMAIL_CONVERSATION   = 'mpsellerbuyercommunication/email/conversation_template';

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    protected $tempId;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param Magento\Framework\App\Helper\Context $context
     * @param Magento\Framework\ObjectManagerInterface $objectManager
     * @param Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param Magento\Store\Model\StoreManagerInterface $_storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_messageManager = $messageManager;
        $this->scopeConfig = $context->getScopeConfig();
    }

    /**
     * Return store
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }

    /**
     * [generateTemplate description]
     * @param  Mixed $emailTemplateVariables
     * @param  Mixed $senderInfo
     * @param  Mixed $receiverInfo
     * @return void
     */
    public function generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $template =  $this->_transportBuilder->setTemplateIdentifier($this->tempId)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->_storeManager->getStore()->getId(),
                    ]
                )
                ->setTemplateVars($emailTemplateVariables)
                ->setFrom($senderInfo)
                ->addTo($receiverInfo['email'], $receiverInfo['name']);
        return $this;
    }

    /*transaction email template*/
    /**
     * [sendQuerypartnerEmail description]
     * @param  Mixed $data
     * @param  Mixed $emailTemplateVariables
     * @param  Mixed $senderInfo
     * @param  Mixed $receiverInfo
     * @return void
     */
    public function sendQuerypartnerEmailToCustomer($data, $emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $this->tempId = $this->getTemplateId(self::XML_PATH_EMAIL_ASK_PRODUCT_QUERY_TO_CUSTOMER);
        $this->inlineTranslation->suspend();
    
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        
        $this->inlineTranslation->resume();
    }

    /*transaction email template*/
    /**
     * [sendQuerypartnerEmail description]
     * @param  Mixed $data
     * @param  Mixed $emailTemplateVariables
     * @param  Mixed $senderInfo
     * @param  Mixed $receiverInfo
     * @return void
     */
    public function sendQuerypartnerEmailToAdmin($data, $emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $this->tempId = $this->getTemplateId(self::XML_PATH_EMAIL_ASK_PRODUCT_QUERY_TO_ADMIN);
        $this->inlineTranslation->suspend();
    
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        
        $this->inlineTranslation->resume();
    }

    /*transaction email template*/
    /**
     * [sendQuerypartnerEmail description]
     * @param  Mixed $data
     * @param  Mixed $emailTemplateVariables
     * @param  Mixed $senderInfo
     * @param  Mixed $receiverInfo
     * @return void
     */
    public function sendCommunicationEmail($data, $emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $this->tempId = $this->getTemplateId(self::XML_PATH_EMAIL_CONVERSATION);
        $this->inlineTranslation->suspend();
    
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->inlineTranslation->resume();
    }

    /*transaction email template*/
    /**
     * [sendQuerypartnerEmail description]
     * @param  Mixed $data
     * @param  Mixed $emailTemplateVariables
     * @param  Mixed $senderInfo
     * @param  Mixed $receiverInfo
     * @return void
     */
    public function sendCommunicationEmailToAdmin($data, $emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $this->tempId = $this->getTemplateId(self::XML_PATH_EMAIL_CONVERSATION_TO_ADMIN);
        $this->inlineTranslation->suspend();
    
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
        $this->inlineTranslation->resume();
    }

    /**
     * Return template id.
     *
     * @return mixed
     */
    public function getTemplateId($xmlPath)
    {
        return $this->getConfigValue($xmlPath, $this->getStore()->getStoreId());
    }

   /**
    * Return store configuration value.
    *
    * @param string $path
    * @param int    $storeId
    *
    * @return mixed
    */
    protected function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
