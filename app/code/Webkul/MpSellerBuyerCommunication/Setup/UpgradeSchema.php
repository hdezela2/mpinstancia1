<?php
/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MpSellerBuyerCommunication
 * @author      Webkul
 * @copyright   Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerBuyerCommunication\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '2.0.0') < 0) {
            // Get module table
            $tableName = $setup->getTable('marketplace_sellerbuyercommunication');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'status' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => false,
                        'comment' => 'query status',
                    ],
                    'query_status' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => false,
                        'comment' => 'query status',
                    ],
                    'support_type' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => false,
                        'comment' => 'query support type',
                    ],
                    'attachment_status' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => false,
                        'comment' => 'file attachments',
                    ],
                ];

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }

                $tableName = $setup->getTable('marketplace_sellerbuyercommunication_conversation');

                $columns = [
                    'attachments' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => false,
                        'comment' => 'all filles attachments',
                    ],
                    'response_time' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => false,
                        'comment' => 'response time',
                    ]
                ];

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }

            $setup->getConnection()->addColumn(
                $setup->getTable('marketplace_sellerbuyercommunication'),
                'product_name',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 225,
                        'nullable' => true,
                        'comment' => 'product name'
                    ]
            );

            $this->addForeignKeys($setup);
        }
        
        $setup->endSetup();
    }

    public function addForeignKeys($setup)
    {
        try {
            /**
             * Add foreign keys for Seller ID
             */
            $setup->getConnection()->addForeignKey(
                $setup->getFkName(
                    'marketplace_sellerbuyercommunication',
                    'seller_id',
                    'customer_entity',
                    'entity_id'
                ),
                $setup->getTable('marketplace_sellerbuyercommunication'),
                'seller_id',
                $setup->getTable('customer_entity'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );

            /**
             * Add foreign keys for seller buyer communication
             */
            $setup->getConnection()->addForeignKey(
                $setup->getFkName(
                    'marketplace_sellerbuyercommunication_conversation',
                    'comm_id',
                    'marketplace_sellerbuyercommunication',
                    'entity_id'
                ),
                $setup->getTable('marketplace_sellerbuyercommunication_conversation'),
                'comm_id',
                $setup->getTable('marketplace_sellerbuyercommunication'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        } catch (\Exception $e) {
            //
        }
    }
}
