<?php
/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MpSellerBuyerCommunication
 * @author      Webkul
 * @copyright   Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */

/**
 * MpSellerBuyerCommunication Saveconversation controller.
 *
 */

namespace Webkul\MpSellerBuyerCommunication\Controller\Seller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;

use Magento\Customer\Model\Session;
use Magento\Customer\Model\Customer;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Saveconversation extends Action
{
    const XML_SENDER_TYPE = 1;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $_customer;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $_formKeyValidator;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * object of Filesystem
     * @var [type]
     */
    protected $_filesystem;

    /**
     * @var \Webkul\MpSellerBuyerCommunication\Model\CommunicationRepository
     */
    protected $_communicationRepository;

    /**
     * @var \Webkul\MpSellerBuyerCommunication\Helper\Data
     */
    protected $_helper;

    /**
     * @param Filesystem $filesystem
     * @param Context $context
     * @param Session $customerSession
     * @param Customer $customer
     * @param Product $product
     * @param FormKeyValidator $formKeyValidator
     * @param \Magento\Framework\Stdlib\DateTime\DateTime
     * @param PageFactory $resultPageFactory
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Webkul\MpSellerBuyerCommunication\Model\ConversationRepository $conversationFactory
     * @param \Webkul\MpSellerBuyerCommunication\Model\CommunicationRepository $communicationRepository
     */
    public function __construct(
        Filesystem $filesystem,
        Context $context,
        Session $customerSession,
        Customer $customer,
        FormKeyValidator $formKeyValidator,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        PageFactory $resultPageFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Webkul\MpSellerBuyerCommunication\Model\ConversationRepository $conversationFactory,
        \Webkul\MpSellerBuyerCommunication\Model\CommunicationRepository $communicationRepository,
        \Webkul\MpSellerBuyerCommunication\Helper\Data $helper,
        \Magento\Framework\UrlInterface $urlInterface,
        TimezoneInterface $localeDate
    )
    {
        $this->_helper = $helper;
        $this->_customerSession = $customerSession;
        $this->_customer = $customer;
        $this->resultPageFactory = $resultPageFactory;
        $this->_formKeyValidator = $formKeyValidator;
        $this->_date = $date;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_conversationFactory = $conversationFactory;
        $this->_communicationRepository = $communicationRepository;
        $this->urlInterface = $urlInterface;
        $this->localeDate = $localeDate;
        parent::__construct($context);
    }

    /**
     * Send mail to seller and save message in database
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $data = $this->getRequest()->getParams();
            $currentDate = $this->localeDate->date()->format('Y-m-d H:i:s');

            if ($data['message']) {
                $sellerId = $this->_helper->getCurrentCustomer();
                $sellerName = $this->_customerSession->getCustomer()->getName();
                $sellerEmail = $this->_customerSession->getCustomer()->getEmail();

                /*save new message in database*/
                $collectionData = $this->_objectManager->create(
                    'Webkul\MpSellerBuyerCommunication\Model\SellerBuyerCommunication'
                )
                    ->getCollection()
                    ->addFieldToFilter(
                        'seller_id',
                        $sellerId
                    )->addFieldToFilter(
                        'entity_id',
                        $data["comm_id"]
                    );

                $convCollection = $this->_conversationFactory->getCollectionByQueryId($data["comm_id"]);
                $convCollection->setOrder(
                    'created_at',
                    'desc'
                );
                $currentTimeZone = $this->_helper->getCurrentTimezone();
                $lastRepliedTime = $this->getLastRecentReplyOfSeller($convCollection);

                $customerRepliedTime = new \DateTime($lastRepliedTime);
                $customerRepliedTime->setTimezone(new \DateTimeZone($currentTimeZone));

                $currentReplyTime = new \DateTime($this->_date->gmtDate());

                $interval = date_diff($customerRepliedTime, $currentReplyTime);

                $responseTime = $interval->format('%h:%i:%s');

                if ($collectionData->getSize()) {
                    $this->changeQueryStatus($collectionData, $data);

                    $model = $this->_objectManager->create(
                        'Webkul\MpSellerBuyerCommunication\Model\Conversation'
                    );
                    $model->setCommId($data["comm_id"]);
                    $model->setMessage($this->_helper->removeScriptFromData($data["message"]));
                    $model->setSender($sellerName);
                    $model->setSenderType(self::XML_SENDER_TYPE);
                    $model->setCreatedAt($currentDate);
                    $model->setUpdatedAt($currentDate);
                    $model->setResponseTime($responseTime);
                    $lastConvId = $model->save()->getId();

                    $customerId = 0;
                    $subject = '';
                    foreach ($collectionData as $value) {
                        $subject = $value['subject'];
                        $customerId = $value['customer_id'];
                        $buyerEmail = $value['email_id'];
                    }

                    /*send mail to customer*/
                    $emailTemplateVariables = [];
                    $senderInfo = [];
                    $receiverInfo = [];
                    if ($customerId) {
                        $customerData = $this->_customer->load($customerId);

                        $buyerName = $customerData->getName();
                        $buyerEmail = $customerData->getEmail();
                    } else {
                        $buyerName = __('Guest');
                    }

                    $subject = (strlen($subject) > 50) ? substr($subject, 0, 50) . '..' : $subject;
                    $emailTemplateVariables['myvar1'] = $buyerName;
                    $emailTemplateVariables['myvar2'] = $subject;
                    $emailTemplateVariables['myvar3'] = __('Seller');
                    $emailTemplateVariables['myvar4'] = $this->_helper->removeScriptFromData($data["message"]);
                    $emailTemplateVariables['myvar5'] = $this->urlInterface->getUrl(
                        'mpsellerbuyercommunication/customer/view',
                        ['_secure' => $this->getRequest()->isSecure(), 'id' => $data["comm_id"]]
                    );
                    $senderInfo = [
                        'name' => $sellerName,
                        'email' => $sellerEmail,
                    ];
                    $receiverInfo = [
                        'name' => $buyerName,
                        'email' => $buyerEmail,
                    ];
                    $this->_objectManager->create(
                        'Webkul\MpSellerBuyerCommunication\Helper\Email'
                    )->sendCommunicationEmail(
                        $data,
                        $emailTemplateVariables,
                        $senderInfo,
                        $receiverInfo
                    );

                    /*send notification mail to admin if enabled*/
                    if ($this->_objectManager->create(
                            'Webkul\MpSellerBuyerCommunication\Helper\Data'
                        )->getAdminNotificationStatus() == 1
                    ) {
                        $senderInfo = [];
                        $receiverInfo = [];
                        $adminStoreId = $this->_objectManager->create(
                            'Webkul\Marketplace\Helper\Data'
                        )->getAdminEmailId();
                        $adminEmail = $adminStoreId ? $adminStoreId : $this->_objectManager->create(
                            'Webkul\Marketplace\Helper\Data'
                        )->getDefaultTransEmailId();
                        $adminUsername = __('Admin');
                        $emailTemplateVariables['myvar1'] = $adminUsername;
                        $senderInfo = [
                            'name' => $sellerName,
                            'email' => $sellerEmail,
                        ];
                        $receiverInfo = [
                            'name' => $adminUsername,
                            'email' => $adminEmail,
                        ];
                        $this->_objectManager->create(
                            'Webkul\MpSellerBuyerCommunication\Helper\Email'
                        )->sendCommunicationEmailToAdmin(
                            $data,
                            $emailTemplateVariables,
                            $senderInfo,
                            $receiverInfo
                        );
                    }
                    $this->saveAttachments($data['attach_count'], $data["comm_id"], $lastConvId);
                }
            }
            $this->messageManager->addSuccess(__('The message has been sent successfully.'));
            return $this->resultRedirectFactory->create()->setPath(
                'mpsellerbuyercommunication/seller/view',
                ['id' => $data["comm_id"], '_secure' => $this->getRequest()->isSecure()]
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            return $this->resultRedirectFactory->create()->setPath(
                'mpsellerbuyercommunication/seller/view',
                ['id' => $data["comm_id"], '_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

    /**
     * get recent replay time of customer
     * @param object $collection
     * @return timestamp
     */
    private function getLastRecentReplyOfSeller($collection)
    {
        $lastRepliedTime = null;
        foreach ($collection as $record) {
            if (!$record->getSenderType()) {
                $lastRepliedTime = $record->getUpdatedAt();
            } else {
                if (!empty($lastRepliedTime)) {
                    break;
                }
            }
        }
        return $lastRepliedTime;
    }

    /**
     * change query status
     * @param object $collection
     * @param array $data
     */
    private function changeQueryStatus($collection, $data)
    {
        foreach ($collection as $key => $sellerData) {
            $sellerData->setQueryStatus($data['query_status']);
            $sellerData->save();
        }
    }

    /**
     * save attachment
     * @param int $attachCount
     * @param int $commentId
     * @param int $lastConvId
     */
    private function saveAttachments($attachCount, $commentId, $lastConvId)
    {
        $files = $this->getRequest()->getFiles()->toArray();
        $imageData = [];

        $imageUploadPath = $this->_filesystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath('sellerbuyercommunication/' . $commentId . '/' . $lastConvId . '/');
        if (!is_dir($imageUploadPath)) {
            mkdir($imageUploadPath, 0755, true);
        }

        foreach ($files as $key => $file) {
            if ($file['error'] != 4) {
                try {
                    $uploader = $this->_fileUploaderFactory->create(['fileId' => $key]);
                    $uploader->setAllowedExtensions(['jpeg', 'jpg', 'png', 'gif', 'zip', 'doc', 'pdf', 'rar']);
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);
                    $resultLogo = $uploader->save($imageUploadPath);

                    if ($resultLogo['file']) {
                        $imageData[] = $resultLogo['file'];
                    }
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
                }
            }
        }


        if (!empty($imageData)) {
            $commConnection = $this->_communicationRepository
                ->getCollectionByEntityId($commentId);
            if (!$commConnection->getStatus()) {
                $commConnection->setAttachmentStatus('1')
                    ->setEntityId($commentId)->save();
            }
            $collection = $this->_conversationFactory->getCollectionByEntityId($lastConvId);
            $collection->setAttachments(implode(',', $imageData))
                ->setEntityId($collection->getEntityId())->save();
        }
    }
}
