<?php
/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MpSellerBuyerCommunication
 * @author      Webkul
 * @copyright   Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */
/**
 * MpSellerBuyerCommunication Sendmail controller.
 *
 */
namespace Webkul\MpSellerBuyerCommunication\Controller\Seller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

use Magento\Customer\Model\Session;
use Magento\Customer\Model\Customer;
use Magento\Catalog\Model\Product;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Sendmail extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer;

    protected $_product;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $_formKeyValidator;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Webkul\MpSellerBuyerCommunication\Helper\Data
     */
    protected $_helper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param Customer $customer
     * @param Product $product
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date,
     * @param PageFactory $resultPageFactory,
     * @codeCoverageIgnore
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Customer $customer,
        Product $product,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        PageFactory $resultPageFactory,
        \Webkul\MpSellerBuyerCommunication\Helper\Data $helper,
        TimezoneInterface $localeDate
    ) {
        $this->_customerSession = $customerSession;
        $this->Customer = $customer;
        $this->_product = $product;
        $this->resultPageFactory = $resultPageFactory;
        $this->_formKeyValidator = $formKeyValidator;
        $this->_date = $date;
        $this->_helper = $helper;
        $this->localeDate = $localeDate;
        parent::__construct($context);
    }

    /**
     * Send mail to seller and save message in database
     *
     * @return json string
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $data['subject'] = "";
        $currentDate = $this->localeDate->date()->format('Y-m-d H:i:s');
        if ($data['seller-id']) {
            $this->_eventManager->dispatch(
                'mp_send_querymail',
                [$data]
            );
            if (isset($data['product-id'])) {
                $productId = $data['product-id'];
                $productName = $this->_product->load($productId)->getName();
                ;
            } else {
                $productId = '';
                $productName = '';
            }
            $sellerId = $data['seller-id'];
            $ask = $this->_helper->removeScriptFromData($data['ask']);
            $subject = $data['subject'];
            $buyerEmail = $data['email'];
            $supportType = $data['support_type'];
            $buyerId = 0;
            $queryStatus = 0;

            $status = $this->_helper->checkQueryAutoApprovalStatus();
            if (!$status) {
                $status = 0;
            }

            if ($this->_customerSession->isLoggedIn()) {
                $buyerId = $this->_helper->getCurrentCustomer();
                $buyerName = $this->_customerSession->getCustomer()->getName();
                $buyerEmail = $this->_customerSession->getCustomer()->getEmail();
            } else {
                $buyerData = $this->_objectManager
                    ->create('Magento\Customer\Model\Customer')
                    ->getCollection()
                    ->addFieldToFilter('email', ['eq'=>$buyerEmail]);
                if ($buyerData->getSize()) {
                    foreach ($buyerData as $value) {
                        $buyerId = $value->getId();
                        $buyerName = $this->_objectManager
                                ->create('Magento\Customer\Model\Customer')
                                ->load($buyerId)->getName();
                    }
                } else {
                    $buyerName = 'Guest';
                }
            }
            /*save new message in database*/
            $model = $this->_objectManager->create('Webkul\MpSellerBuyerCommunication\Model\SellerBuyerCommunication');
            $model->setProductId($productId);
            $model->setProductName($productName);
            $model->setSellerId($sellerId);
            $model->setCreatedAt($currentDate);
            $model->setUpdatedAt($currentDate);
            $model->setCustomerId($buyerId);
            $model->setSubject($subject);
            $model->setEmailId($buyerEmail);
            $model->setSupportType($supportType);
            $model->setStatus($status);
            $model->setQueryStatus($queryStatus);
            $saved = $model->save();
            $lastId = $saved->getId();
            $model = $this->_objectManager->create('Webkul\MpSellerBuyerCommunication\Model\Conversation');
            $model->setCommId($lastId);
            $model->setMessage($ask);
            $model->setSender($buyerName);
            $model->setSenderType(0);
            $model->setCreatedAt($currentDate);
            $model->setUpdatedAt($currentDate);
            $converId = $model->save()->getId();

            /*send mail to seller*/

            if ($status) {
                $emailTemplateVariables = [];
                $senderInfo = [];
                $receiverInfo = [];
                $seller=$this->Customer->load($data['seller-id']);
                $emailTemplateVariables['myvar1'] =$seller->getName();
                $sellerEmail = $seller->getEmail();
                if (!isset($data['product-id'])) {
                    $data['product-id'] = 0 ;
                    $emailTemplateVariables['myvar3'] = "";
                } else {
                    $emailTemplateVariables['myvar3'] = $this->_product->load($data['product-id'])->getName();
                }
                $emailTemplateVariables['myvar4'] =strip_tags($data['ask']);
                $emailTemplateVariables['myvar5'] =$buyerEmail;
                $subject = (strlen($data['subject']) > 50) ? substr($data['subject'], 0, 50).'..' : $data['subject'];
                $emailTemplateVariables['myvar6'] =$subject;
                $senderInfo = [
                    'name' => $buyerName,
                    'email' => $buyerEmail,
                ];
                $receiverInfo = [
                    'name' => $seller->getName(),
                    'email' => $sellerEmail,
                ];
                $this->_objectManager->create(
                    'Webkul\Marketplace\Helper\Email'
                )->sendQuerypartnerEmail(
                    $data,
                    $emailTemplateVariables,
                    $senderInfo,
                    $receiverInfo
                );

                /*send notification mail to customer*/
                $senderInfo = [];
                $receiverInfo = [];
                $emailTemplateVariables['myvar1'] =$buyerName;
                $senderInfo = [
                    'name' => $seller->getName(),
                    'email' => $sellerEmail,
                ];
                $receiverInfo = [
                    'name' => $buyerName,
                    'email' => $buyerEmail,
                ];
                $this->_objectManager->create(
                    'Webkul\MpSellerBuyerCommunication\Helper\Email'
                )->sendQuerypartnerEmailToCustomer(
                    $data,
                    $emailTemplateVariables,
                    $senderInfo,
                    $receiverInfo
                );
            }

            /*send notification mail to admin if enabled*/
            if ($this->_helper->getAdminNotificationStatus()) {
                $senderInfo = [];
                $receiverInfo = [];
                $adminStoreEmail = $this->_objectManager->create('Webkul\Marketplace\Helper\Data')->getAdminEmailId();
                $adminEmail=$adminStoreEmail? $adminStoreEmail:$this->_objectManager
                ->create('Webkul\Marketplace\Helper\Data')
                ->getDefaultTransEmailId();
                $adminUsername = 'Admin';
                $emailTemplateVariables['myvar1'] = $adminUsername;
                if (!isset($data['product-id'])) {
                    $data['product-id'] = 0 ;
                    $emailTemplateVariables['myvar3'] ='';
                } else {
                    $emailTemplateVariables['myvar3'] = $this->_product
                    ->load($data['product-id'])->getName();
                }
                $emailTemplateVariables['myvar6'] =$data['subject'];
                $emailTemplateVariables['myvar7'] =$buyerName;
                $emailTemplateVariables['myvar8'] =$buyerEmail;
                $senderInfo = [
                    'name' => $buyerName,
                    'email' => $buyerEmail,
                ];
                $receiverInfo = [
                    'name' => $adminUsername,
                    'email' => $adminEmail,
                ];
                $this->_objectManager
                    ->create(
                        'Webkul\MpSellerBuyerCommunication\Helper\Email'
                    )->sendQuerypartnerEmailToAdmin(
                        $data,
                        $emailTemplateVariables,
                        $senderInfo,
                        $receiverInfo
                    );
            }
        }
        $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($converId)
        );
    }
}
