<?php
/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MpSellerBuyerCommunication
 * @author      Webkul
 * @copyright   Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerBuyerCommunication\Controller\Adminhtml\Query;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\MpSellerBuyerCommunication\Model\ResourceModel\SellerBuyerCommunication\CollectionFactory;
use Webkul\MpSellerBuyerCommunication\Model\ResourceModel\Conversation\CollectionFactory as ConversationCollectionFactory;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Customer;
use Magento\Catalog\Model\Product;

/**
 * Class Query MassDelete
 */
class MassApprove extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \Webkul\MpSellerBuyerCommunication\Model\ConversationRepository
     */
    protected $_conversationFactory;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $_customer;
    
    /**
     *
     * @var Product object
     */
    protected $_product;

    /**
     * @param Customer                                                        $customer
     * @param Product                                                         $product
     * @param Context                                                         $context
     * @param Filter                                                          $filter
     * @param CollectionFactory                                               $collectionFactory
     * @param \Webkul\MpSellerBuyerCommunication\Model\ConversationRepository $conversationFactory
     * @param \Webkul\MpSellerBuyerCommunication\Helper\Data                  $helper
     */
    public function __construct(
        Customer $customer,
        Product $product,
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Webkul\MpSellerBuyerCommunication\Model\ConversationRepository $conversationFactory
    ) {
        $this->_customer = $customer;
        $this->_product = $product;
        $this->filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
        $this->_conversationFactory = $conversationFactory;
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $count = 0;
        $collection = $this->filter->getCollection($this->_collectionFactory->create());

        foreach ($collection as $value) {
            $value->setStatus(1)->save();
             /*send mail to seller*/
            $emailTemplateVariables = [];
            $senderInfo = [];
            $receiverInfo = [];
            $buyerEmail = $value->getEmailId();

            $collection = $this->_conversationFactory
                        ->getCollectionByQueryId($value->getEntityId())
                        ->getFirstItem();

            $queryContent = strip_tags($collection->getMessage());

            $buyerData = $this->_objectManager
                    ->create('Magento\Customer\Model\Customer')
                    ->getCollection()->addFieldToFilter('email', ['eq'=> $buyerEmail]);
            if ($buyerData->getSize()) {
                foreach ($buyerData as $buyer) {
                    $buyerId = $buyer->getId();
                    $buyerName = $this->_objectManager
                        ->create('Magento\Customer\Model\Customer')
                        ->load($buyerId)->getName();
                }
            } else {
                $buyerName = 'Guest';
            }
            $seller = $this->_customer->load($value->getSellerId());
            $emailTemplateVariables['myvar1'] =$seller->getName();
            $sellerEmail = $seller->getEmail();

            $emailTemplateVariables['myvar3'] =$this->_product->load($value->getProductId())->getName();

            $data = [
                'product-id' => $value->getProductId()
            ];
            $emailTemplateVariables['myvar4'] = $queryContent;
            $emailTemplateVariables['myvar5'] = $value->getEmailId();

            $subject = (strlen($value->getSubject()) > 50) ? substr($value->getSubject(), 0, 50).' ..' : $value->getSubject();
            $emailTemplateVariables['myvar6'] = $subject;
            $senderInfo = [
                'name' => $buyerName,
                'email' => $buyerEmail,
            ];
            $receiverInfo = [
                'name' => $seller->getName(),
                'email' => $sellerEmail,
            ];
            $this->_objectManager->create(
                'Webkul\Marketplace\Helper\Email'
            )->sendQuerypartnerEmail(
                $data,
                $emailTemplateVariables,
                $senderInfo,
                $receiverInfo
            );

            /*send notification mail to customer*/
            $senderInfo = [];
            $receiverInfo = [];
            $emailTemplateVariables['myvar1'] =$buyerName;
            $senderInfo = [
                'name' => $seller->getName(),
                'email' => $sellerEmail,
            ];
            $receiverInfo = [
                'name' => $buyerName,
                'email' => $buyerEmail,
            ];
            $this->_objectManager->create(
                'Webkul\MpSellerBuyerCommunication\Helper\Email'
            )->sendQuerypartnerEmailToCustomer(
                $data,
                $emailTemplateVariables,
                $senderInfo,
                $receiverInfo
            );

            $count++;
        }
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been approved.', $count));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('mpsellerbuyercommunication/query/index/');
    }

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_MpSellerBuyerCommunication::query_view');
    }
}
