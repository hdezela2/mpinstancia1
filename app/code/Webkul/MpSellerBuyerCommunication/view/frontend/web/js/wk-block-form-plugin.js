/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MpSellerBuyerCommunication
 * @author      Webkul
 * @copyright   Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
define([
    "jquery",
    'mage/translate',
    "mage/template",
    "Magento_Ui/js/modal/modal",
    "mage/adminhtml/wysiwyg/tiny_mce/setup"
], function ($, $t, mageTemplate,modal) {
    'use strict';
    $.widget('mage.wkBlockFormplugin', {
        _create: function () {
            var self = this;
            var askDataForm = $(self.options.formValidator);
            var name = '';
            var email = '';
            askDataForm.mage('validation', {});
            $('.product-info-main').append($('#mp-wk-block'));
            $('#mp-wk-block').show();

            $(".wk-block-rating").mouseover(function () {
                $(".wk-seller-rating").show();
            }).mouseout(function () {
                $(".wk-seller-rating").hide();
            });

            $('#askque').click(function () {
                $('#ask-form input,#ask-form textarea').removeClass('mage-error');
                $('#ask-form div.mage-error').remove();

                $('#wk-mp-ask-data').modal('openModal');
            });


            var options_ques = {
                type: 'popup',responsive: true,innerScroll: true,title: 'Contact Seller',
                buttons: [{
                        text: 'Reset',
                        class:'',
                        click: function () {
                            if ($('#ask-form').find('#name').attr('readonly')=='readonly') {
                                name = $('#ask-form').find('#name').val();
                            }
                            if ($('#ask-form').find('#email').attr('readonly')=='readonly') {
                                email = $('#ask-form').find('#email').val();
                            }
                            $('#ask-form')[0].reset();
                            $('#ask-form').find('#name').val(name);
                            $('#ask-form').find('#email').val(email);
                        } //handler on button click
                    },{
                        text: 'Submit',
                        class: 'wk-seller-buyer-askbtn clickask',
                        click: function () {
                            // -----save question

                        } //handler on button click
                    }
                ]
            };
            var popup = modal(options_ques, $('#wk-mp-ask-data'));

            $("body").on('click', ".action-primary", function () {
                $('.wk-close,#resetbtn').trigger('click');
                window.location.reload();
            });

            var i=2;
            
            $("body").on('click', '.product_images', function () {
                console.log("adasd");
                if ($('body').find('.wk_imagevalidate').length >= 1) {
                    $('body').find('.wk_imagevalidate').siblings('.remove_attch').css("display", "inline-block");
                }
                var progressTmpl = mageTemplate(self.options.attachmentTemplate),
                    tmpl;
                tmpl = progressTmpl({
                    fields: {
                        index: i
                    }
                });
        
                $('#otherimages').append(tmpl);
                i++;
            });

            $("body").on('click','.remove_attch',function (e) {
                e.preventDefault();
                if ($('body').find('.wk_imagevalidate').length > 1) {
                    $(this).closest('div').remove();
                    i--;
                }
            });


            $("body").on('change',".wk_imagevalidate",function () {
                var validExtensions = ['jpeg', 'jpg', 'png', 'gif', 'zip', 'doc', 'pdf', 'rar'];
                var ext = $(this).val().split('.').pop().toLowerCase();
                if ($.inArray(ext, validExtensions) == -1) {
                    $(this).val('');
                    alert('Invalid extension! allowed extension are '+validExtensions.join(', '));
                }
            });

            var files;var it = 0;
            // Add events
            $(document).on('change','input[type=file]', prepareUpload);

            // Grabed the files and set them to the variable
            function prepareUpload(event)
            {
                files = event.target.files;
                prepareFormData(files);
            }

            var data = new FormData();
            function prepareFormData()
            {
                $.each(files, function (key, value) {
                    data.append('attachment_'+it, value);
                    it++;
                });
            }

            $(".wk-ask-question-link").mouseover(function () {
                $(".wk-seller-response-container").show();
            }).mouseout(function () {
                $(".wk-seller-response-container").hide();
            });

            $('.wk-seller-buyer-askbtn').on('click', function () {
                if (askDataForm.valid()!=false) {
                    if ($('#queryquestion_ifr').length) {
                        var desc = $('#queryquestion_ifr').contents().find('#tinymce').text();
                        $('#queryquestion-error').remove();
                        if (desc === "" || desc === null) {
                            $('#queryquestion-error').remove();
                            $('#queryquestion').parent().find('.file').before('<div class="mage-error" generated="true" id="queryquestion-error">This is a required field.</div>');
                            return false;
                        }
                    }
                    var thisthis = $(this);
                    if (thisthis.hasClass("clickask")) {
                        if (self.options.captchenable == '1') {
                            var total = parseInt($('#wk-mp-captchalable1').text()) + parseInt($('#wk-mp-captchalable2').text());
                            var wk_mp_captcha = $('#wk-mp-captcha').val();
                            if (total != wk_mp_captcha) {
                                $('#wk-mp-captchalable1').text(Math.floor((Math.random()*10)+1));
                                $('#wk-mp-captchalable2').text(Math.floor((Math.random()*100)+1));
                                $('#wk-mp-captcha').val('');
                                $('#wk-mp-captcha').addClass('mage-error');
                                $(this).addClass('mage-error');
                                $('#ask_form .errormail').text(self.options.varificationMsg).slideDown('slow').delay(2000).slideUp('slow');
                            } else {
                                thisthis.removeClass('clickask');
                                $('#wk-mp-ask-data').addClass('mail-procss');
                                $.ajax({
                                    url:self.options.targetAjaxUrl,
                                    data:$('#ask-form').serialize(),
                                    type:'post',
                                    dataType:'json',
                                    showLoader:true,
                                    success:function (d) {
                                        if ($.isNumeric(d)) {
                                            $.ajax({
                                                url:self.options.saveAjaxFileUrl+'id/'+d+'/itr/'+it,
                                                data:  data,
                                                contentType: false,
                                                cache: false,
                                                processData:false,
                                                type:'POST',
                                                dataType:'json',
                                                showLoader:true,
                                                success:function (d) {
                                                    thisthis.addClass('clickask');
                                                    $('#wk-mp-ask-data').removeClass('mail-procss')
                                                    if (d=="true") {
                                                        alert('Message has been sent.');
                                                    } else {
                                                        alert('Mail Send but image/files is invalid');
                                                    }
                                                    $('.action-close').trigger('click');
                                                    window.location.reload();
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        } else {
                            thisthis.removeClass('clickask');
                            $('#wk-mp-ask-data').addClass('mail-procss');
                            $.ajax({
                                url: self.options.targetAjaxUrl,
                                data:$('#ask-form').serialize(),
                                type:'post',
                                dataType:'json',
                                showLoader:true,
                                success:function (d) {
                                    if ($.isNumeric(d)) {
                                        $.ajax({
                                            url:self.options.saveAjaxFileUrl+'id/'+d+'/itr/'+it,
                                            data:  data,
                                            contentType: false,
                                            cache: false,
                                            processData:false,
                                            type:'POST',
                                            dataType:'json',
                                            showLoader:true,
                                            success:function (d) {
                                                thisthis.addClass('clickask');
                                                $('#wk-mp-ask-data').removeClass('mail-procss')
                                                if (d=="true") {
                                                    alert('Message has been sent');
                                                } else {
                                                    alert('Mail Send but image/files is invalid');
                                                }
                                                $('.action-close').trigger('click');
                                                window.location.reload();
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                    return false;
                }
            });
        }
    });
    return $.mage.wkBlockFormplugin;
});
