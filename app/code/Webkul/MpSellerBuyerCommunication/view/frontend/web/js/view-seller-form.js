/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MpSellerBuyerCommunication
 * @author      Webkul
 * @copyright   Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
define([
    "jquery",
    'mage/translate',
    'Magento_Ui/js/modal/alert',
    "mage/template"
], function ($,$t, alert, mageTemplate) {
    'use strict';
    $.widget('mage.viewSellerForm', {
        _create: function () {
            var self = this;
            var i=1;
            $('.wk-mp-btn').on('click', function (event) {
                 event.preventDefault();
                $('.attach_count').val(i);
                var validateForm = $('#wk-sellercommunication-save-form');
                if (validateForm.valid()!=false) {
                    $('#wk-sellercommunication-save-form').submit();
                    $(this).attr('disabled','disabled');
                } else {
                    return false;
                }


            });

            $("body").on('change',".wk_imagevalidate",function () {
                var validExtensions = ['jpeg', 'jpg', 'png', 'gif', 'zip', 'doc', 'pdf', 'rar'];
                var ext = $(this).val().split('.').pop().toLowerCase();
                if ($.inArray(ext, validExtensions) == -1) {
                    $(this).val('');
                    alert({content : $t('Invalid extension! allowed extension are '+validExtensions.join(', '))});
                }


            });


            $("body").on('click', '.add_more_images', function () {
                if ($('body').find('.wk_imagevalidate').length >= 1) {
                    $('body').find('.wk_imagevalidate').siblings('.remove_attch').css("display", "inline-block");
                }
                var progressTmpl = mageTemplate(self.options.attachmentTemplate),
                    tmpl;
                tmpl = progressTmpl({
                    fields: {
                        index: i
                    }
                });

                $('#otherimages').append(tmpl);
                i++;
            });

            $("body").on('click','.remove_attch',function (e) {
                e.preventDefault();

                //Se elimina la condición de que exista un adjunto al menos, al menos que cambien las reglas y deba existir al menos un combox de imagen.

                // if ($('body').find('.wk_imagevalidate').length > 1) {
                //     $(this).closest('div').remove();
                // }

                $(this).closest('div').remove()
            });


            $("body").on('change',".wk_imagevalidate",function () {
                var validExtensions = ['jpeg', 'jpg', 'png', 'gif', 'zip', 'doc', 'pdf', 'rar'];
                var ext = $(this).val().split('.').pop().toLowerCase();
                if ($.inArray(ext, validExtensions) == -1) {
                    $(this).val('');
                    alert('Invalid extension! allowed extension are '+validExtensions.join(', '));
                } else {
                    $(this).siblings('label').children('.upload-message').text(this.files[0].name);
                }
            });

            var files;var it = 0;
            // Add events
            $(document).on('change','input[type=file]', prepareUpload);

            // Grabed the files and set them to the variable
            function prepareUpload(event)
            {
                files = event.target.files;
                prepareFormData(files);
            }

            var data = new FormData();
            function prepareFormData()
            {
                $.each(files, function (key, value) {
                    data.append('attachment_'+it, value);
                    it++;
                });
            }


        }
    });
    return $.mage.viewSellerForm;
});
