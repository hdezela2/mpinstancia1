<?php
/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MpSellerBuyerCommunication
 * @author      Webkul
 * @copyright   Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerBuyerCommunication\Block\Seller;

use Magento\Customer\Model\Customer;
use Webkul\MpSellerBuyerCommunication\Model\ResourceModel\SellerBuyerCommunication\CollectionFactory;
use Webkul\SellerSubAccount\Api\SubAccountRepositoryInterface;

class History extends \Magento\Framework\View\Element\Template
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var \Webkul\MpSellerBuyerCommunication\Model\ResourceModel\SellerBuyerCommunication\CollectionFactory
     */
    protected $_sellerCommCollectionFactory;

    /** @var \Webkul\MpSellerBuyerCommunication\Model\SellerBuyerCommunication */
    protected $sellerBuyerCommunicationLists;

    /**
     *
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    public $_productRepository;

    /**
     * @var SubAccountRepositoryInterface
     */
    public $_subAccountRepository;

    /**
     * @param Context $context
     * @param array $data
     * @param Customer $customer
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $_productRepository
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webkul\SellerSubAccount\Api\SubAccountRepositoryInterface $_subAccountRepository
     */
    public function __construct(
        Customer $customer,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        CollectionFactory $sellerCommCollectionFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        SubAccountRepositoryInterface $_subAccountRepository,
        array $data = []
    ) {
        $this->_sellerCommCollectionFactory = $sellerCommCollectionFactory;
        $this->customer = $customer;
        $this->_customerSession = $customerSession;
        $this->imageHelper = $context->getImageHelper();
        $this->_productRepository = $productRepository;
        $this->_subAccountRepository = $_subAccountRepository;
        parent::__construct($context, $data);
        $this->_objectManager = $objectManager;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Communication History'));
    }

    /**
     * get customer id
     * @return int
     */
    public function getCustomerId()
    {   
        $customerId = $this->_customerSession->getCustomerId();
        if($parentCustomer = $this->_subAccountRepository->getByCustomerId($customerId)){
            if($parentCustomer->getSellerId()){
                $customerId = $parentCustomer->getSellerId();
            }
        }
        return $customerId;        
    }

    /**
     * get customer name
     * @param  int $customerId
     * @return string
     */
    public function getCustomerNameId($customerId)
    {
        return $this->customer->load($customerId)->getName();
    }

    /**
     * get product url
     * @param  int $productId
     * @return string
     */
    public function getProductUrlById($productId)
    {
        try {
            $product = $this->_productRepository->getById($productId);
            if ($product->getStatus()==2) {
                return '#';
            }
            return $product->getProductUrl();
        } catch (\Exception $e) {
            return '#';
        }
    }

    /**
     * get Product name
     * @param  int $productId
     * @return string
     */
    public function getProductNameById($productId)
    {
        try {
            $product = $this->_productRepository->getById($productId);
            return $product->getName();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return bool|\Webkul\MpSellerBuyerCommunication\Model\ResourceModel\SellerBuyerCommunication\Collection
     */

    public function getAllCommunicationData()
    {
        if (!($sellerId = $this->getCustomerId())) {
            return false;
        }
        if (!$this->sellerBuyerCommunicationLists) {
            $collection = $this->_sellerCommCollectionFactory->create()
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            )->addFieldToFilter(
                'status',
                [
                    'eq'=> 1
                ]
            );

            $filterText = '';
            $paramData = $this->getRequest()->getParams();

            if (!empty($paramData['filter_by']) && !empty($paramData['s'])) {
                $filterText = $paramData['s'] != ""?$paramData['s']:"";

                if ($paramData['filter_by'] == 'email_id') {
                    $collection->addFieldToFilter(
                        'email_id',
                        ['like' => "%".$filterText."%"]
                    );
                } else {
                    $collection = $this->getFilterCollectionByContent($filterText, $collection);
                }
            }

            $collection->setOrder(
                'created_at',
                'desc'
            );
            $this->sellerBuyerCommunicationLists = $collection;
        }

        return $this->sellerBuyerCommunicationLists;
    }

    /**
     * filter by content
     * @param  string $filterText
     * @param  object $collection
     * @return object
     */
    public function getFilterCollectionByContent($filterText, $collection)
    {
        $coversationTable = $this->_objectManager->create(
            'Webkul\MpSellerBuyerCommunication\Model\ResourceModel\Conversation\Collection'
        )->getTable('marketplace_sellerbuyercommunication_conversation');

        $collection->getSelect()->joinLeft(
            $coversationTable.' as cpev',
            'main_table.entity_id = cpev.comm_id',
            ['comm_id','message']
        )->where(
            "cpev.message like '%".$filterText."%' OR
            main_table.subject like '%".$filterText."%'"
        );
        $collection->getSelect()->group('comm_id');

        return $collection;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getAllCommunicationData()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'MpSellerBuyerCommunication.seller.pager'
            )
            ->setCollection(
                $this->getAllCommunicationData()
            );
            $this->setChild('pager', $pager);
            $this->getAllCommunicationData()->load();
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * get current url
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->_urlBuilder->getCurrentUrl(); // Give the current url of recently viewed page
    }
}
