<?php
/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MpSellerBuyerCommunication
 * @author      Webkul
 * @copyright   Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerBuyerCommunication\Model;

use Webkul\MpSellerBuyerCommunication\Api\Data\SellerBuyerCommunicationInterface;
use Webkul\MpSellerBuyerCommunication\Model\ResourceModel\SellerBuyerCommunication\Collection;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class CommunicationRepository implements \Webkul\MpSellerBuyerCommunication\Api\CommunicationRepositoryInterface
{
    /**
     * resource model
     * @var \Webkul\MpSellerBadge\Model\ResourceModel\Badge
     */
    protected $_resourceModel;

    /**
     * @param BadgeFactory                                                      $badgeFactory
     * @param \Webkul\MpSellerBadge\Model\ResourceModel\Badge\CollectionFactory $collectionFactory
     * @param \Webkul\MpSellerBadge\Model\ResourceModel\Badge                   $resourceModel
     */
    public function __construct(
        SellerBuyerCommunicationFactory $communicationFactory,
        \Webkul\MpSellerBuyerCommunication\Model\ResourceModel\SellerBuyerCommunication\CollectionFactory $collectionFactory,
        \Webkul\MpSellerBuyerCommunication\Model\ResourceModel\SellerBuyerCommunication $resourceModel
    ) {
    
        $this->_resourceModel = $resourceModel;
        $this->_communicationFactory = $communicationFactory;
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * get collection by entity id
     * @param  integer $entityId entity id
     * @return object
     */
    public function getCollectionByEntityId($entityId)
    {
        $collection = $this->_communicationFactory->create()->load($entityId);
        
        return $collection;
    }

    /**
     * get all queries list by product id
     * @param  int $productId
     * @return object
     */
    public function getAllCollectionByProductId($productId)
    {
        $collection = $this->_communicationFactory->create()->getCollection()
            ->addFieldToFilter(
                'product_id',
                [
                    'eq'=>$productId
                ]
            );
        return $collection;
    }

    /**
     * get all queries list by seller id
     * @param  int $productId
     * @return object
     */
    public function getAllCollectionBySeller($sellerId)
    {
        $collection = $this->_communicationFactory->create()->getCollection()
            ->addFieldToFilter(
                'seller_id',
                [
                    'eq'=>$sellerId
                ]
            );
        return $collection;
    }
}
