<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_CategoryProductPrice
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\CategoryProductPrice\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     *
     */
    protected $coreSession;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Webkul\MpAssignProduct\Model\ItemsFactory $assignItems,
        \Webkul\MpAssignProduct\Model\AssociatesFactory $assignAssociates,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->request = $context->getRequest();
        $this->storeManager = $storeManager;
        $this->coreSession = $coreSession;
        $this->assignAssociates = $assignAssociates;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->categoryFactory = $categoryFactory;
        $this->assignItems = $assignItems;
        parent::__construct($context);
    }


    public function getPostData()
    {
        $categories = $this->request->getParam('categories');
        $percentage = $this->request->getParam('update_percentage');
        $response['pricePercentage'] = $percentage;
        $allCategories = $this->getAllCategories($categories);
        $response['allCategories'] = implode(',', $allCategories);
        $response['assignProductIds'] = [];
        $collection = $this->productCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addCategoriesFilter(['in' => $allCategories])
            ->load();
        $allProductIds = $collection->getAllIds();
        if ($allProductIds){
            $assignProducts = $this->assignItems->create()
                ->getCollection()
                ->addFieldToFilter('product_id',['in' => [$allProductIds]]);
            $i=0;
            foreach ($assignProducts as $assignProductItem) {
                if ($assignProductItem->getType() == 'configurable') {
                    $associateAssignProducts = $this->assignAssociates->create()->getCollection()
                        ->addFieldToFilter('parent_product_id', $assignProductItem->getProductId())
                        ->addFieldToFilter('parent_id', $assignProductItem->getId());
                    foreach ($associateAssignProducts as $associateAssignProductItem) {
                        $response['assignProductIds'][$i]['id'] = $associateAssignProductItem->getId();
                        // $response['assignProductIds'][$i]['productId'] = $associateAssignProductItem->getProductId();
                        $response['assignProductIds'][$i]['child'] = true;
                        $i++;
                    }
                }
                else {
                    $response['assignProductIds'][$i]['id'] = $assignProductItem->getId();
                    // $response['assignProductIds'][$i]['productId'] = $assignProductItem->getProductId();
                    $response['assignProductIds'][$i]['child'] = false;
                    $i++;
                }
            }
        }
        return $response;
    }

    public function getAllCategories($cat)
    {
        $allCat = [];
        foreach ($cat as $catIds) {
            array_push($allCat, $catIds);
            $categories = $this->categoryCollectionFactory->create()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('path', ["like" => '%/'.$catIds.'/%']);
            foreach ($categories as $category){
                array_push($allCat, $category->getId());
            }
        }
        return $allCat;
    }
}
