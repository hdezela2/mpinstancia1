<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_CategoryProductPrice
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\CategoryProductPrice\Controller\Adminhtml\Index;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Webkul\CategoryProductPrice\Model\ProductPriceFactory;
use Webkul\MpAssignProduct\Model\AssociatesFactory;
use Webkul\MpAssignProduct\Model\ItemsFactory;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;
    protected $_categoryCollectionFactory;
    protected $_productRepository;
    protected $_registry;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var AssociatesFactory
     */
    private $assignAssociates;

    /**
     * @var ItemsFactory
     */
    private $assignItems;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var Data
     */
    private $jsonHelper;

    /**
     * @var Configurable
     */
    private $configurableProductType;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var ProductPriceFactory
     */
    private $productPrice;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param ProductFactory $productFactory
     * @param ItemsFactory $assignItems
     * @param AssociatesFactory $assignAssociates
     * @param ProductPriceFactory $productPrice
     * @param Configurable $configurableProductType
     * @param Data $jsonHelper
     * @param DateTime $date
     * @param CategoryFactory $categoryFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        ProductFactory $productFactory,
        ItemsFactory $assignItems,
        AssociatesFactory $assignAssociates,
        ProductPriceFactory $productPrice,
        Configurable $configurableProductType,
        Data $jsonHelper,
        DateTime $date,
        CategoryFactory $categoryFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context);
        $this->productFactory = $productFactory;
        $this->assignAssociates = $assignAssociates;
        $this->assignItems = $assignItems;
        $this->categoryFactory = $categoryFactory;
        $this->jsonHelper = $jsonHelper;
        $this->configurableProductType = $configurableProductType;
        $this->date = $date;
        $this->productPrice = $productPrice;
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->_registry = $registry;
    }

    /**
     * Mapped eBay Order List page.
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $productData = $this->getRequest()->getParam('product');
        $pricePercentage = $this->getRequest()->getParam('pricePercentage');
        $allCategories = $this->getRequest()->getParam('allCategories');

        try {
            $responseData = $this->updatePrice($productData, $pricePercentage, $allCategories);
            $result['error'] = 0;
            $result['msg'] = '<div class="wk-mu-success wk-mu-box">'.__('Successfully updated '.$responseData['name']).'</div>';
        } catch (\Exception $e) {
            $result['error'] = 1;
            $result['msg'] = '<div class="wk-mu-error wk-mu-box">'.__('Error in updating price').'</div>';
        }

        $result = $this->jsonHelper->jsonEncode($result);
        $this->getResponse()->representJson($result);

    }

    public function updatePrice($productData, $percentage, $allCategories)
    {
        $resourceConnection = $this->resourceConnection->getConnection();
        $pricePercentage = $this->getRequest()->getParam('pricePercentage');
        $originalPrice = 0;
        $product = $this->productFactory->create();
        //for configurable product
        if ($productData['child'] != "false") {
            $assignProducts = $this->assignAssociates->create()->getCollection()->addFieldToFilter('id', $productData['id']);
            foreach ($assignProducts as $assignProduct) {
                $product = $this->productFactory->create()->load($assignProduct->getProductId());
                // get seller id
                $assignItem = $this->assignItems->create()->load($assignProduct['parent_id']);
                $is_offer=0;
                $select = $resourceConnection->select();
                $select->from('dccp_offer_product', ['status']);
                $select->where('product_id = '. $assignProduct->getProductId());
                $select->where('start_date >= DATE(NOW())');
                $select->where('end_date <= DATE(NOW())');
                $select->where('status = 1');
                $is_offer=$resourceConnection->fetchOne($select);
                if($is_offer){
                    $select = $resourceConnection->select();
                    $select->from('dccp_offer_product', ['base_price']);
                    $select->where('product_id = '. $assignProduct->getProductId());
                    $select->where('start_date >= DATE(NOW())');
                    $select->where('end_date <= DATE(NOW())');
                    $select->where('status = 1');
                    $basePrice=$resourceConnection->fetchOne($select);
                    if ($basePrice > 0) {
                        $updatedPrice = round((($percentage*$basePrice/100) + $basePrice),0,PHP_ROUND_HALF_DOWN);
                    } else {
                        $updatedPrice = 0;
                    }
                    $resourceConnection->beginTransaction();
                    $update_offer = 'UPDATE  dccp_offer_product set base_price = "'.$updatedPrice.'" where product_id = '.(int)$assignProduct->getProductId().' and status = 1';
                    $resourceConnection->query($update_offer);
                    $resourceConnection->commit();
                }else{

                    $originalPrice = $assignProduct->getPrice();

                    if ($originalPrice > 0) {
                        $updatedPrice = round((($percentage*$originalPrice/100) + $originalPrice),0,PHP_ROUND_HALF_DOWN);
                    } else {
                        $updatedPrice = 0;
                    }
                    $assignProduct->setPrice($updatedPrice);
                    $assignProduct->save();
                }
                $this->updateLog($product, $assignProduct->getPrice(), $assignItem->getSellerId(), $pricePercentage, $originalPrice, $allCategories);
            }
            $data['name'] = $product->getName();
            return $data;
        }

        $assignItem = $this->assignItems->create()->load($productData['id']);
        $product = $this->productFactory->create()->load($assignItem->getProductId());
        $is_offer=0;
        $select = $resourceConnection->select();
        $select->from('dccp_offer_product', ['status']);
        $select->where('product_id = '. $assignItem->getProductId());
        $select->where('start_date >= DATE(NOW())');
        $select->where('end_date <= DATE(NOW())');
        $select->where('status = 1');
        $is_offer=$resourceConnection->fetchOne($select);
        if($is_offer){
            $select = $resourceConnection->select();
            $select->from('dccp_offer_product', ['base_price']);
            $select->where('product_id = '. $assignItem->getProductId());
            $select->where('start_date >= DATE(NOW())');
            $select->where('end_date <= DATE(NOW())');
            $select->where('status = 1');
            $basePrice=$resourceConnection->fetchOne($select);
            if ($basePrice > 0) {
                $updatedPrice = round((($percentage*$basePrice/100) + $basePrice),0,PHP_ROUND_HALF_DOWN);
            } else {
                $updatedPrice = 0;
            }
            $resourceConnection->beginTransaction();
            $update_offer = 'UPDATE  dccp_offer_product set base_price = "'.$updatedPrice.'" where product_id = '.(int)$assignItem->getProductId().' and status = 1';
            $resourceConnection->query($update_offer);
            $resourceConnection->commit();
        }else{
            $originalPrice = $assignItem->getPrice();
            if ($originalPrice > 0) {
                $updatedPrice = round((($percentage*$originalPrice/100) + $originalPrice),0,PHP_ROUND_HALF_DOWN);
            } else {
                $updatedPrice = 0;
            }
            $assignItem->setPrice($updatedPrice);
            $assignItem->save();
        }


        $data['name'] = $product->getName();
        $this->updateLog($product, $assignItem->getPrice(), $assignItem->getSellerId(), $pricePercentage, $originalPrice, $allCategories);
        return $data;
    }

    public function updateLog($product, $itemPrice, $sellerId, $pricePercentage, $originalPrice, $allCategories)
    {
        //$categories = $allCategories;
        $allCategories = implode(',', $product->getCategoryIds());
        $parentProductId = $this->configurableProductType->getParentIdsByChild($product->getId());
        if ($parentProductId) {
            $parentProduct = $this->productFactory->create()->load($parentProductId[0]);
            $allCategories = implode(',', $parentProduct->getCategoryIds());
        }
        $productPriceObj = $this->productPrice->create();
        $productPriceObj->setMagecategoryId($allCategories);
        $productPriceObj->setMageproductId($product->getId());
        $productPriceObj->setMageproductPrice($originalPrice);

        $productPriceObj->setUpdatedproductPrice($itemPrice);
        $productPriceObj->setUpdatePercentage($pricePercentage);
        $productPriceObj->setSellerId($sellerId);
        $productPriceObj->setCreatedAt($this->date->gmtDate());
        $productPriceObj->setUpdatedAt($this->date->gmtDate());
        $productPriceObj->save();
    }

    /**
     * Check Order Import Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_CategoryProductPrice::index');
    }
}
