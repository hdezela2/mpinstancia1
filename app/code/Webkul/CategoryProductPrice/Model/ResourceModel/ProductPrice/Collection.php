<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\CategoryProductPrice\Model\ResourceModel\ProductPrice;

/**
 * Webkul CategoryProductPrice ResourceModel ProductPrice collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Webkul\CategoryProductPrice\Model\ProductPrice',
            'Webkul\CategoryProductPrice\Model\ResourceModel\ProductPrice'
        );
        $this->_map['fields']['entity_id'] = 'main_table.entity_id';
    }
}
