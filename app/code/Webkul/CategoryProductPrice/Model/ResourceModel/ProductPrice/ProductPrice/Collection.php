<?php

namespace Webkul\CategoryProductPrice\Model\ResourceModel\ProductPrice\ProductPrice;

class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $adminSession;

    public function __construct(
        \Magento\Backend\Model\Auth\Session $adminSession,
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        $mainTable = 'category_product_price',
        $resourceModel = '\Webkul\CategoryProductPrice\Model\ResourceModel\ProductPrice',
        \Magento\Framework\App\RequestInterface $request
    )
    {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
        $this->adminSession = $adminSession;
    }

    public function _beforeLoad()
    {

        $this->adminSession;
        $websiteList = $this->adminSession->getUser()->getRole()->getGwsWebsites();

        //add filter by websites into the role configuration
        $this->join(
            ['secondTable' => $this->getTable('catalog_product_website')],
            'main_table.mageproduct_id = secondTable.product_id',
            ['secondTable.website_id' => 'website_id'])
            ->addFieldToFilter('secondTable.website_id', array('in' => $websiteList ));

        return parent::_beforeLoad();
    }

}

