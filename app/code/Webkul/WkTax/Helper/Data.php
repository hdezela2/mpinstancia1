<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_WkTax
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\WkTax\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
      return $this->scopeConfig->getValue(
          'wktax/general_settings/enable',
          ScopeInterface::SCOPE_STORE
      );
    }

    /**
     * @return string
     */
    public function getConfigData($field)
    {
      return $this->scopeConfig->getValue(
          'wktax/general_settings/'.$field,
          ScopeInterface::SCOPE_STORE
      );
    }
    /**
     * getAllRules codes filled in the configuration
     * @return mixed
     */
    public function getAllRules() {
      $rules = [];
      $rulesData = [];
      if($this->getConfigData('tax_rule1') != "") {
        $taxRule1 = $this->getConfigData('tax_rule1');
        $rules[] = $taxRule1;
        $rulesData[$taxRule1] = $this->getConfigData('tax_rule1_text');
      }
      if($this->getConfigData('tax_rule2') != "") {
        $taxRule2 = $this->getConfigData('tax_rule2');
        $rules[] = $taxRule2;
        $rulesData[$taxRule2] = $this->getConfigData('tax_rule2_text');
      }
      if($this->getConfigData('tax_rule3') != "") {
        $taxRule3 = $this->getConfigData('tax_rule3');
        $rules[] = $this->getConfigData('tax_rule3');
        $rulesData[$taxRule3] = $this->getConfigData('tax_rule3_text');
      }
      return [$rules, $rulesData];
    }
}
