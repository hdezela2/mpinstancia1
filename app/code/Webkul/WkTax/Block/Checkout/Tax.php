<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_WkTax
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\WkTax\Block\Checkout;
use Magento\Tax\Model\ResourceModel\Calculation\Rule\CollectionFactory;

class Tax extends \Magento\Framework\View\Element\Template
{
    /**
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $_cart;
    /**
     * @var \Webkul\WkTax\Helper\Data
     */
    protected $taxHelper;
    /**
     * @var CollectionFactory
     */
    protected $collection;

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $cart,
        \Webkul\WkTax\Helper\Data $taxHelper,
        CollectionFactory $collection,
        array $data = []
    ) {
        $this->_cart = $cart;
        $this->taxHelper = $taxHelper;
        $this->collection = $collection;
        parent::__construct($context, $data);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    /**
     * [getOrderComment description]
     * @return string
     */
    public function getOrderComment() {
      $quote = $this->_cart->getQuote();
      return $quote->getWkTaxOrderComment();
    }
    /**
     * reward list
     * @return array
     */
    public function getRules()
    {
        $helper = $this->taxHelper;
        list($rules, $rulesData) = $helper->getAllRules();
        $options = [];
        $quote = $this->_cart->getQuote();
        $defaultTaxRule = $quote->getWkTaxRule();
        if(!$defaultTaxRule) {
          $defaultTaxRule = $helper->getConfigData('default_tax_rule');
        }
        $commentTaxRule = $helper->getConfigData('comment_rule');
        if(count($rules)) {
          $collection = $this->collection->create()
                        ->addFieldToFilter('code',['in'=> $rules]);
          foreach ($collection as $value) {
            $status = false;
            $allowComment = false;
            $code = $value->getCode();
            if($code == $defaultTaxRule) {
              $status = true;
            }
            if($code == $commentTaxRule) {
              $allowComment = true;
            }
            $options[$code] = [
              'code' => $code,
              'text' => isset($rulesData[$code]) ? $rulesData[$code] : "",
              'is_default' => $status,
              'allow_comment' => $allowComment
            ];
          }
        }
        return $options;
    }
}
