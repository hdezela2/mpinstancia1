<?php
namespace Webkul\WkTax\Plugin\Tax\Model\Sales\Total\Quote;
use Webkul\WkTax\Helper\Data;

class Subtotal
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var \Magento\Tax\Model\Calculation\Rule
     */
    protected $taxRule;
    /**
     * Class constructor
     *
     * @param Data $helper
     */
    public function __construct(
      Data $helper,
      \Magento\Tax\Model\Calculation\RuleFactory $taxRule
    ) {
        $this->helper = $helper;
        $this->taxRule = $taxRule;
    }

    /**
     * @param \Magento\Tax\Model\Sales\Total\Quote\Subtotal $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return mixed
     */
    public function aroundCollect(
        \Magento\Tax\Model\Sales\Total\Quote\Subtotal $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
         if($this->helper->isEnabled()) {
           $code = $quote->getWkTaxRule();
           if(!$code) {
             $code = $this->helper->getConfigData('default_tax_rule');
           }
           $taxRule = $this->taxRule->create()->load($code,'code');
           $taxClass = $taxRule->getProductTaxClasses();
           $taxClassId = isset($taxClass[0]) ? $taxClass[0] : 0;
           if($taxClassId) {
             foreach ($quote->getAllVisibleItems() as $quote_item) {
                 $product = $quote_item->getProduct();
                 $product->setTaxClassId($taxClassId);
             }
             $quote->save();
           }
        }
        return $proceed($quote, $shippingAssignment, $total);
    }
}
