<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_WkTax
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\WkTax\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        $installer->getConnection()->addColumn(
            $setup->getTable('quote'),
            'wk_tax_rule',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'visible'   => false,
                'comment' => 'Selected Tax Rule'
            ]
        );
        $installer->getConnection()->addColumn(
            $setup->getTable('quote'),
            'wk_tax_order_comment',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'visible'   => false,
                'comment' => 'Tax Order Comment'
            ]
        );

        $installer->endSetup();
    }
}
