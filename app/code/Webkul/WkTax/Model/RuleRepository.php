<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_WkTax
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software protected Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\WkTax\Model;

use Magento\Framework\Session\SessionManager;

class RuleRepository implements \Webkul\WkTax\Api\RuleRepositoryInterface
{
    /**
     * @var SessionManager
     */
    protected $session;
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Webkul\WkTax\Helper\Data
     */
    protected $wkTaxHelper;

    public function __construct(
        SessionManager $session,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Webkul\WkTax\Helper\Data $wkTaxHelper
    ) {
        $this->session = $session;
        $this->request = $request;
        $this->checkoutSession = $checkoutSession;
        $this->wkTaxHelper = $wkTaxHelper;
    }

    /**
     * Save Tax Rule.
     *
     * @param \Webkul\WkTax\Api\RuleRepositoryInterface $ruleData
     *
     * @return array $ruleInfo
     *
     * @throws \Magento\Framework\Exception\InputException If there is a problem with the input
     */
    public function save($ruleData = [])
    {
        $fieldValues = $this->request->getParams();
        if(count($fieldValues)) {
          $code = "";
          $taxComment = "";
          $commentTaxRule = $this->wkTaxHelper->getConfigData('comment_rule');
          if(isset($fieldValues['code'])) {
            $code = $fieldValues['code'];
          }
          $quote = $this->checkoutSession->getQuote();
          if($code != "") {
            if($code == $commentTaxRule) {
              $taxComment =$fieldValues['ordernote'];
            }
            $quote->setWkTaxRule($code);
            $quote->setWkTaxOrderComment($taxComment)->save();
          }
        } else {
          throw new \Magento\Framework\Exception\LocalizedException(
                 __('Something went wrong!')
             );
        }
    }
}
