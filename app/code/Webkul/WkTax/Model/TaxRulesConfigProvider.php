<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_WkTax
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\WkTax\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session;
use Magento\Tax\Model\ResourceModel\Calculation\Rule\CollectionFactory;

class TaxRulesConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Session
     */
    protected $cart;

    /**
     * @var \Webkul\WkTax\Helper\Data
     */
    protected $wkTaxHelper;

    /**
     * @var CollectionFactory
     */
    protected $collection;

    /**
     * @param \Magento\Checkout\Model\Session    $cart
     * @param \Webkul\WkTax\Helper\Data          $wkTaxHelper
     * @param CollectionFactory                  $collection
     */
    public function __construct(
        Session $cart,
        \Webkul\WkTax\Helper\Data $wkTaxHelper,
        CollectionFactory $collection
    ) {
        $this->cart = $cart;
        $this->wkTaxHelper = $wkTaxHelper;
        $this->collection = $collection;
    }

    /**
     * set data in window.checkout.config for checkout page.
     *
     * @return array $options
     */
    public function getConfig()
    {
        $helper = $this->wkTaxHelper;
        $options = [
            'rules' => [],
            'tax_rule_status' => $helper->isEnabled(),
        ];
        $quote = $this->cart->getQuote();
        list($rules, $rulesData) = $helper->getAllRules();
        $options = [];
        $defaultTaxRule = $quote->getWkTaxRule();
        if(!$defaultTaxRule) {
          $defaultTaxRule = $helper->getConfigData('default_tax_rule');
        }
        $commentTaxRule = $helper->getConfigData('comment_rule');
        if(count($rules)) {
          $collection = $this->collection->create()
                        ->addFieldToFilter('code',['in'=> $rules]);
          foreach ($collection as $value) {
            $status = false;
            $checked = "";
            $allowComment = false;
            $code = $value->getCode();
            if($code == $defaultTaxRule) {
              $status = true;
              $checked = $code;
            }
            if($code == $commentTaxRule) {
              $allowComment = true;
            }
            $options['rules'][$code] = [
              'code' => $code,
              'text' => isset($rulesData[$code]) ? $rulesData[$code] : "",
              'is_default' => $status,
              'checked' => $checked,
              'allow_comment' => $allowComment,
              'comment' => $quote->getWkTaxOrderComment()
            ];
          }
        }

        return $options;
    }
}
