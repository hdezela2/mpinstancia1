<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_WkTax
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\WkTax\Controller\Checkout;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class ApplyAjax extends Action
{ 
    protected $resultJsonFactory;
    /**
     *
     */
    protected $checkoutSession;
    /**
     *
     */
    protected $wkHelper;
    /**
     * @param Context          $context
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Webkul\WkTax\Helper\Data $wkHelper
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->wkHelper = $wkHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();

        $fieldValues = $this->getRequest()->getParams();
        $quote = $this->checkoutSession->getQuote();
        $commentTaxRule = $this->wkHelper->getConfigData('comment_rule');
        $taxComment ="";
        if(isset($fieldValues['rule_code'])) {
          if($fieldValues['rule_code'] == $commentTaxRule) {
            $taxComment =$fieldValues['tax_comment'];
          }
          $quote->setWkTaxRule($fieldValues['rule_code']);
          $quote->setWkTaxOrderComment($taxComment)->save();
          $result->setData(['updatedQuote'=>true]);
        }else{
          $result->setData(['updatedQuote'=>false]);
        }
        return $result;
    }
}
