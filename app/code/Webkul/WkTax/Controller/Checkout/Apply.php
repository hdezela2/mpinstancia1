<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_WkTax
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\WkTax\Controller\Checkout;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Apply extends Action
{
    /**
     *
     */
    protected $checkoutSession;
    /**
     *
     */
    protected $wkHelper;
    /**
     * @param Context          $context
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Webkul\WkTax\Helper\Data $wkHelper
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->wkHelper = $wkHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $fieldValues = $this->getRequest()->getParams();
        $quote = $this->checkoutSession->getQuote();
        $commentTaxRule = $this->wkHelper->getConfigData('comment_rule');
        $taxComment ="";
        if(isset($fieldValues['rule_code'])) {
          if($fieldValues['rule_code'] == $commentTaxRule) {
            $taxComment =$fieldValues['tax_comment'];
          }
          $quote->setWkTaxRule($fieldValues['rule_code']);
          $quote->setWkTaxOrderComment($taxComment)->save();
        }
        return $this->resultRedirectFactory
            ->create()
            ->setPath('checkout/cart', ['_secure' => $this->getRequest()->isSecure()]);
    }
}
