define([
    'ko',
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Webkul_WkTax/js/model/resource-url-manager',
    'Magento_Checkout/js/model/error-processor',
    'Webkul_WkTax/js/model/tax/tax-messages',
    'mage/storage',
    'mage/translate',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/model/totals',
    'Magento_Checkout/js/model/cart/cache',
    'Magento_Checkout/js/model/cart/totals-processor/default',
    'Magento_Checkout/js/model/full-screen-loader'
], function (ko, $, quote, urlManager, errorProcessor, messageContainer, storage, $t, getPaymentInformationAction,
    totals, cartCache, defaultTotal, fullScreenLoader
) {
    'use strict';

    return function (couponCode, isApplied) {
        var quoteId = quote.getQuoteId(),
            url = urlManager.getApplyRuleUrl(couponCode, quoteId),
            message = $t('Your Tax Rule was successfully applied.');

        fullScreenLoader.startLoader();

        return storage.put(
            url,
            {},
            false
        ).done(function (response) {
            var deferred;

            if (response) {
                deferred = $.Deferred();
                //totals.isLoading(true);
                fullScreenLoader.stopLoader(null);
                cartCache.set('totals',null);
                defaultTotal.estimateTotals();
                /*getTotalsAction([], deferred);*/
                $.when(deferred).done(function () {
                    paymentService.setPaymentMethods(
                        paymentMethodList()
                    );
                });
                messageContainer.addSuccessMessage({'message': message});
            }
        }).fail(function (response) {
            fullScreenLoader.stopLoader();
            totals.isLoading(false);
            errorProcessor.process(response, messageContainer);
        });
    };
});
