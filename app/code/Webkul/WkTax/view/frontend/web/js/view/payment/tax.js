define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Webkul_WkTax/js/action/set-tax-rule'
], function ($, ko, Component, quote, setTaxRuleAction) {
    'use strict';

    var totals = quote.getTotals(),
        couponCode = ko.observable(null),
        isApplied;
    var taxRules = [];
    taxRules = window.checkoutConfig.rules;
    var rulesData = [];
    var status = false;
    $.each(taxRules, function ( i, v) {
        rulesData.push(v);
    });
    if (totals()) {
        couponCode(totals()['coupon_code']);
    }
    isApplied = ko.observable(couponCode() != null);
    return Component.extend({
        rules: ko.observableArray(rulesData),
        isVisible: ko.observable(status),
        defaults: {
            template: 'Webkul_WkTax/payment/tax'
        },


        /**
         * Applied flag
         */
        isApplied: isApplied,

        /**
         * Coupon code application procedure
         */
        apply: function () {
            if (this.validate()) {
                var formData = $('#form-tax').serialize();
                var value = $('input[name=rule_code]:checked').val();
                var ordernote = $('#rule-text').val();
                var params = formData+"&code="+value+"&ordernote="+ordernote;
                setTaxRuleAction(params, isApplied);
            }
        },

        checkStatus: function (status, checked) {
            this.isVisible(status);
            return checked;
        },

        applyautoselection: function () {
            var status = false;
            $.each(taxRules, function ( i, v) {
                if(v.is_default && v.allow_comment) {
                  status = true;
                }
            });
            this.isVisible(status);
            return 1;
        },
        /**
         * Coupon form validation
         *
         * @returns {Boolean}
         */
        validate: function () {
            var form = '#form-tax';

            return $(form).validation() && $(form).validation('isValid');
        }
    });
});
