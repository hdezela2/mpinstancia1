<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_WkTax
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\WkTax\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Api\OrderRepositoryInterface;
use Webkul\WkTax\Helper\Data as WkTaxHelper;
use Magento\Framework\Session\SessionManager;

/**
 * Webkul WkTax SalesOrderPlaceAfterObserver Observer Model.
 */
class SalesOrderPlaceAfterObserver implements ObserverInterface
{

    /**
     * @var QuoteRepository
     */
    protected $_quoteRepository;

    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var WkTaxHelper
     */
    protected $_wkTaxHelper;

    /**
     * @var Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * [$_coreSession description].
     *
     * @var SessionManager
     */
    protected $_coreSession;

    /**
     * @param QuoteRepository                             $quoteRepository
     * @param OrderRepositoryInterface                    $orderRepository
     * @param WkTaxHelper                                 $wkTaxHelper
     */
    public function __construct(
        QuoteRepository $quoteRepository,
        OrderRepositoryInterface $orderRepository,
        WkTaxHelper $wkTaxHelper,
        \Magento\Sales\Model\Order\Status\HistoryFactory $history,
        \Magento\Sales\Api\OrderStatusHistoryRepositoryInterface $orderStatusHistory,
        \Magento\Checkout\Model\Session $checkoutSession,
        SessionManager $coreSession
    ) {
        $this->_quoteRepository = $quoteRepository;
        $this->_orderRepository = $orderRepository;
        $this->_wkTaxHelper = $wkTaxHelper;
        $this->history = $history;
        $this->orderStatusHistory = $orderStatusHistory;
        $this->_checkoutSession = $checkoutSession;
        $this->_coreSession = $coreSession;
    }

    /**
     * Sales Order Place After event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $isMultiShipping = $this->_checkoutSession->getQuote()->getIsMultiShipping();
        if (!$isMultiShipping) {
            /** @var $orderInstance Order */
            $order = $observer->getOrder();
            $quoteId = $order->getQuoteId();
            $quote = $this->_quoteRepository->get($quoteId);
            $note = $quote->getWkTaxOrderComment();
            if($note && $note !="") {
              $lastOrderId = $observer->getOrder()->getId();
              $this->orderPlacedOperations($order, $note);
            }
        } else {
            $quoteId = $this->_checkoutSession->getLastQuoteId();
            $quote = $this->_quoteRepository->get($quoteId);
            $note = $quote->getWkTaxOrderComment();
            if (($quote->getIsMultiShipping() == 1 || $isMultiShipping == 1) && $note && $note !="") {
                $orderIds = $this->_coreSession->getOrderIds();
                foreach ($orderIds as $ids => $orderIncId) {
                    $lastOrderId = $ids;
                    /** @var $orderInstance Order */
                    $order = $this->_orderRepository->get($lastOrderId);
                    $this->orderPlacedOperations($order, $note);
                }
            }
        }
    }

    /**
     * Order Place Operation method.
     *
     * @param \Magento\Sales\Model\Order $order
     * @param string $note
     */
    public function orderPlacedOperations($order, $note)
    {
      $history = $this->history->create();
      $history->setParentId($order->getId())
          ->setComment($note)
          ->setEntityName('order');

      $this->orderStatusHistory->save($history);

    }
}
