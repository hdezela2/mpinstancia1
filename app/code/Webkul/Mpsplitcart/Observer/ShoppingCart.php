<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Mpsplitcart
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Mpsplitcart\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Webkul Mpsplitcart ShoppingCart Observer
 */
class ShoppingCart implements ObserverInterface
{
    /**
     * @var \Webkul\Mpsplitcart\Helper\Data
     */
    private $helper;

    /**
     * @param \Webkul\Mpsplitcart\Helper\Data $helper
     */
    public function __construct(
        \Webkul\Mpsplitcart\Helper\Data $helper,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->helper = $helper;
        $this->request = $request;
    }

    /**
     * [executes on controller_action_predispatch_checkout_cart_index event
     *  and used to add virtual cart items into quote]
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $fullname = $this->request->getFullActionName();
            $array = [
            'checkout_index_index','mpsplitcart_cartover_proceedtocheckout',
            'mpsplitcart_cartover_multishipping','banner_ajax_load',
            'customer_section_load','checkout_cart_add','customer_account_logout','customer_account_logoutSuccess',
            'checkout_sidebar_removeItem','checkout_sidebar_updateItemQty',
            'checkout_cart_delete','checkout_cart_updatePost','checkout_onepage_success','multishipping_checkout_success',
            'checkout_cart_updateItemOptions', 'multishipping_checkout_index',
            'multishipping_checkout_addresses', 'multishipping_checkout_addressesPost',
            'multishipping_checkout_shipping', 'multishipping_checkout_backtoaddresses',
            'multishipping_checkout_address_editshipping', 'multishipping_checkout_shippingPost',
            'comunas_index_index', 'greatbuy_validate_index',
            'multishipping_checkout_billing','multishipping_checkout_overview','multishipping_checkout_overviewPost'
          ];
            if ($this->helper->checkMpsplitcartStatus() && !in_array($fullname, $array)) {
                $this->helper->logDataInLogger(
                    "Observer_ShoppingCart_execute FullactionName : ".$fullname
                );
                $this->helper->removeCustomQuote();
                $this->helper->addVirtualCartToQuote();
                $this->helper->addQuoteToVirtualCart();
            }
        } catch (\Exception $e) {
            $this->helper->logDataInLogger(
                "Observer_ShoppingCart_execute Exception : ".$e->getMessage()
            );
        }
    }
}
