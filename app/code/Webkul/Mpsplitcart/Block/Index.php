<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Mpsplitcart
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Mpsplitcart\Block;

/**
 * Mpsplitcart Block
 */
class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\Mpsplitcart\Helper\Data
     */
    private $helper;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cartModel;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    private $priceHelper;

    /**
     * @var \Magento\Multishipping\Helper\Data
     */
    private $multishippingHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\Mpsplitcart\Helper\Data                  $helper
     * @param \Magento\Checkout\Model\Cart                     $cart
     * @param \Magento\Framework\Pricing\Helper\Data           $priceHelper
     * @param \Magento\Multishipping\Helper\Data               $multishippingHelper
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Mpsplitcart\Helper\Data $helper,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Multishipping\Helper\Data $multishippingHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $data
        );
        $this->helper = $helper;
        $this->cartModel = $cart;
        $this->multishippingHelper = $multishippingHelper;
        $this->priceHelper = $priceHelper;
    }

    /**
     * getSellerData get seller array in order to
     * show items at shopping cart accr. to sellers
     *
     * @return array
     */
    public function getSellerData()
    {
        $cartArray = [];
        try {
            $cart = $this->cartModel->getQuote();
            foreach ($cart->getAllItems() as $item) {
                if (!$item->hasParentItemId()) {
                    $options = $item->getBuyRequest()->getData();

                    if (array_key_exists("mpassignproduct_id", $options)) {
                        $mpAssignId = $options["mpassignproduct_id"];
                        $sellerId = $this->helper->getSellerIdFromMpassign(
                            $mpAssignId
                        );
                    } else {
                        $sellerId = $this->helper->getSellerId($item->getProductId());
                    }

                    $price =  $item->getRowTotal();
                    $taxAmount = $item->getTaxAmount();
                    if ($this->helper->getCatalogPriceIncludingTax()) {
                        $price = $item->getRowTotalInclTax();
                        $taxAmount = 0;
                    }

                    $formattedPrice = $this->priceHelper->currency(
                        $price,
                        true,
                        false
                    );
                    $cartArray[$sellerId][$item->getId()] = $formattedPrice;

                    if (!isset($cartArray[$sellerId]['total'])
                        || $cartArray[$sellerId]['total']==null
                    ) {
                        $cartArray[$sellerId]['total'] = $price;
                    } else {
                        $cartArray[$sellerId]['total'] += $price;
                    }
                    if (!isset($cartArray[$sellerId]['tax'])
                        || $cartArray[$sellerId]['tax']==null
                    ) {
                        $cartArray[$sellerId]['tax'] = $taxAmount;
                    } else {
                        $cartArray[$sellerId]['tax'] += $taxAmount;
                    }
                    $formattedPrice = $this->priceHelper->currency(
                        $cartArray[$sellerId]['total'],
                        true,
                        false
                    );
                    $formattedTaxPrice = $this->priceHelper->currency(
                        $cartArray[$sellerId]['tax'],
                        true,
                        false
                    );
                    $formattedOrderTotal = $this->priceHelper->currency(
                        $cartArray[$sellerId]['tax'] + $cartArray[$sellerId]['total'],
                        true,
                        false
                    );
                    $cartArray[$sellerId]['formatted_total'] = $formattedPrice;
                    $cartArray[$sellerId]['formatted_tax'] = $formattedTaxPrice;
                    $cartArray[$sellerId]['formatted_order_total'] = $formattedOrderTotal;
                }
            }
        } catch (\Exception $e) {
            $this->helper->logDataInLogger(
                "Block_Index_getSellerData Exception : ".$e->getMessage()
            );
        }
        return $cartArray;
    }

    /**
     * @return string
     */
    public function getCheckoutUrl($sellerId = 0)
    {
        return $this->getUrl('mpsplitcart/cartover/multishipping', ['id' => $sellerId, '_secure' => true]);
    }


    /**
     * multicheckoutStatus get multicheckout is enable or not
     *
     * @return bool
     */
    public function multicheckoutStatus() {
      if (!$this->multishippingHelper->isMultishippingCheckoutAvailable()) {
          return false;
      }
      return true;
    }

    /**
     * getMpsplitcartEnable get splitcart is enable or not
     *
     * @return void
     */
    public function getMpsplitcartEnable()
    {
        try {
            return $this->helper->checkMpsplitcartStatus();
        } catch (\Exception $e) {
            $this->helper->logDataInLogger(
                "Block_Index_getMpsplitcartEnable Exception : ".$e->getMessage()
            );
        }
    }

    /**
     * getCartTotal used to get cart total
     *
     * @return string [returns formatted total price]
     */
    public function getCartTotal()
    {
        $cartTotal = 0;
        try {
            $cart = $this->cartModel->getQuote();
            foreach ($cart->getAllItems() as $item) {
                if (!$item->hasParentItemId()) {
                    $sellerId=$this->helper->getSellerId($item->getProductId());
                    $price =  $item->getProduct()->getQuoteItemRowTotal();

                    if (!$price) {
                        $price =  $item->getBaseRowTotal();
                    }
                    $cartTotal += $price;
                }
            }
            $formattedPrice = $this->priceHelper->currency(
                $cartTotal,
                true,
                false
            );
            $cartTotal = $formattedPrice;
        } catch (\Exception $e) {
            $this->helper->logDataInLogger(
                "Block_Index_getCartTotal Exception : ".$e->getMessage()
            );
        }
        return $cartTotal;
    }
}
