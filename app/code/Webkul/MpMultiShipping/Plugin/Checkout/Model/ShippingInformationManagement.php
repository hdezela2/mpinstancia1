<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpMultiShipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpMultiShipping\Plugin\Checkout\Model;

use Magento\Framework\Session\SessionManager;

class ShippingInformationManagement
{
    /**
     * @var Magento\Framework\Session\SessionManager
     */
    protected $_coreSession;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Webkul\MpMultiShipping\Logger\Logger
     */
    protected $logger;

    /**
     * @param SessionManager $coreSession
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Webkul\MpMultiShipping\Logger\Logger $logger
     */
    public function __construct(
        SessionManager $coreSession,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Webkul\MpMultiShipping\Logger\Logger $logger
    ) {
        $this->_coreSession = $coreSession;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        try {
            $extAttributes = $addressInformation->getExtensionAttributes();
            $selectedShipping = $extAttributes->getSelectedShipping();
            $multiCustomship = $extAttributes->getMultiCustomship();
            $this->_coreSession->setSelectedAmount($multiCustomship);
            $this->_coreSession->setSelectedMethods($this->jsonHelper->jsonDecode($selectedShipping));
        } catch (\Exception $e) {
            $this->logger->addError('beforeSaveAddressInformation : '.$e->getMessage());
        }
    }
}
