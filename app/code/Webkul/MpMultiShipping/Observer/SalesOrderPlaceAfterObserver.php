<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpMultiShipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpMultiShipping\Observer;

use Magento\Framework\Event\Manager;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManager;
use Magento\Checkout\Model\Session as CheckoutSession;
use Webkul\Marketplace\Model\OrdersFactory as MpOrdersFactory;
use Webkul\MpMultiShipping\Logger\Logger;

class SalesOrderPlaceAfterObserver implements ObserverInterface
{
    /**
     * @var string
     */
    private $methodForOrder = null;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Webkul\MpMultiShipping\Logger\Logger
     */
    private $logger;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var Webkul\Marketplace\Model\OrdersFactory
     */
    private $mpOrdersFactory;

    /**
     * @param SessionManager $session
     * @param CheckoutSession $checkoutSession
     * @param \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Sales\Model\Order\ItemFactory $itemFactory
     * @param \Magento\Store\Model\StoreManagerInterface $store
     * @param MpOrdersFactory $mpOrdersFactory
     * @param Logger $logger
     */
    public function __construct(
        SessionManager $session,
        CheckoutSession $checkoutSession,
        \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order\ItemFactory $itemFactory,
        \Magento\Store\Model\StoreManagerInterface $store,
        MpOrdersFactory $mpOrdersFactory,
        Logger $logger
    ) {
        $this->saleslistFactory = $saleslistFactory;
        $this->session = $session;
        $this->itemFactory = $itemFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_currencyFactory = $currencyFactory;
        $this->checkoutSession = $checkoutSession;
        $this->_storeManager = $store;
        $this->mpOrdersFactory = $mpOrdersFactory;
        $this->logger = $logger;
    }

    /**
     * after place order event handler
     * Distribute Shipping Price for sellers
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $order = $observer->getOrder();
            $orders = $order ? [$order] : $observer->getOrders();
            foreach ($orders as $order) {
                $shippingmethod = $order->getShippingMethod();
                $lastOrderId = $order->getId();
                $fromCurrency = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
                $toCurrency = $this->_storeManager->getStore()->getDefaultCurrency()->getCode();
                if (strpos($shippingmethod, 'mpmultishipping') !== false) {
                    $allorderitems = $order->getAllItems();
                    $shipmethod = explode('_', $shippingmethod, 2);
                    $shippingAll = $this->session->getSelectedMethods() ? $this->session->getSelectedMethods() : [];
                    if ($order->getQuote()->isMultipleShippingAddresses()) {
                        $shippingAll = $shippingAll[$order->getShippingAddress()->getCustomerAddressId()];
                    }
                    $scopeStore = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                    $fieldId = "carriers/mpmultishipping/shipping_mode";
                    $enabledProductWise = $this->scopeConfig->getValue($fieldId, $scopeStore);

                    if ($enabledProductWise == 1) {
                        foreach ($shippingAll as $shipdata) {
                            $price = $shipdata['price'];
                            $price = $this->getConvertedPrice($fromCurrency, $toCurrency, $price);
                            $collection = $this->mpOrdersFactory->create()->getCollection()
                                        ->addFieldToFilter('order_id', ['eq'=>$lastOrderId])
                                        ->addFieldToFilter('seller_id', ['eq' => $shipdata['sellerid']])
                                        ->setPageSize(1)->getFirstItem();
                                $this->checkAndUpdateCollection($collection, $shipdata, $price);
                        }
                    }
                    /**
                     * for saving shipping rates item wise
                     * 2 for product wise shiping enabled from admin config
                     */
                    if ($enabledProductWise == 2) {
                        $shippingMethodsSelectedArr = $this->session->getSelectedMethods();
                        if ($order->getQuote()->isMultipleShippingAddresses()) {
                            $shippingMethodsSelectedArr = $shippingMethodsSelectedArr[$order->getShippingAddress()->getCustomerAddressId()];
                        }
                        $totalSellerShippingArr = [];
                        foreach ($shippingMethodsSelectedArr as $shippingMethodsSelected) {
                            /**
                             * create wise seller array of shipping rates
                             */
                            $sellerId = $shippingMethodsSelected['sellerid'];
                            $totalSellerShippingArr = $this->populateTotalSellerShippingArr($totalSellerShippingArr, $shippingMethodsSelected, $sellerId);

                            $quoteItemId = $shippingMethodsSelected['itemid'];
                            $shippingMethod = $shippingMethodsSelected['code'];
                            $price = $shippingMethodsSelected['price'];
                            
                            $price = $this->getConvertedPrice($fromCurrency, $toCurrency, $price);
                            
                            $orderItemId = $this->getorderItemIdFromQuoteItemId($quoteItemId);
                            $mpSalesList = $this->saleslistFactory->create()
                                                                ->getCollection()
                                                                ->addFieldToFilter('order_item_id', $orderItemId)
                                                                ->getFirstItem();
                            $mpSalesList->setShippingMethod($shippingMethod)
                                        ->setShippingPrice($price);
                            $this->saveObj($mpSalesList);
                        }
                        $totalShippingAmount = 0;
                        foreach ($totalSellerShippingArr as $sellerId => $shippingAmount) {
                            $totalShippingAmount += $shippingAmount;
                            $collection = $this->mpOrdersFactory->create()->getCollection()
                                        ->addFieldToFilter('order_id', ['eq'=>$lastOrderId])
                                        ->addFieldToFilter('seller_id', ['eq' => $sellerId])
                                        ->setPageSize(1)->getFirstItem();

                            $collection->setShippingCharges($shippingAmount);
                            $this->saveObj($collection);
                        }
                        
                        $order->setShippingAmount($totalShippingAmount);
                    }
                    $order->setShippingDescription("Webkul Multishipping -".$this->methodForOrder);
                    $this->saveObj($order);
                }
            }
        } catch (\Exception $e) {
            $this->logger->addError('SalesOrderPlaceAfterObserver : '.$e->getMessage());
        }
        $this->session->unsShippingMethod();
        $this->session->unsMultiAddressRates();
        $this->checkoutSession->setSellerMethod('');
    }

    /**
     * converts price to different curency
     * @param string $fromCurrency
     * @param string $toCurrency
     * @param int $price
     * @return int $price
     **/
    public function convertPriceToCurrency($fromCurrencyCode, $toCurrencyCode, $price)
    {
        $rate = $this->_currencyFactory->create()->load($fromCurrencyCode)->getAnyRate($toCurrencyCode);
        $price = $price * $rate;
        return $price;
    }

    /**
     * get order_item_id from quote_item_id
     *
     * @param int $quoteItemId
     * @return int
     */
    public function getorderItemIdFromQuoteItemId($quoteItemId)
    {
        $data = $this->itemFactory->create()->getCollection()
                                            ->addFieldToFilter('quote_item_id', $quoteItemId)
                                            ->getFirstItem();
        return $data->getItemId();
    }

    /**
     * returns price as per store config
     *
     * @param string $fromCurrency
     * @param string $toCurrency
     * @param int $price
     * @return int
     */
    public function getConvertedPrice($fromCurrency, $toCurrency, $price)
    {
        if ($fromCurrency != $toCurrency) {
            $price = $this->convertPriceToCurrency($fromCurrency, $toCurrency, $price);
        }
        return $price;
    }

    /**
     * checks and updates collection
     *
     * @param Collection $collection
     * @return void
     */
    public function checkAndUpdateCollection($collection, $shipdata, $price)
    {
        try {
            if ($collection->getId()) {
                if (empty($this->methodForOrder)) {
                    $this->methodForOrder = $shipdata['method'];
                } else {
                    $this->methodForOrder .= ", ".$shipdata['method'];
                }
                $collection->setCarrierName($shipdata['method']);
                $collection->setShippingCharges($price);
                $collection->setMultishipMethod($shipdata['code']);
                $collection->save();
            }
        } catch (\Exception $e) {
            $this->logger->addError('SalesOrderPlaceAfterObserver : '.$e->getMessage());
        }
    }

    /**
     * calculates seller wise shipping
     *
     * @param array $totalSellerShippingArr
     * @param array $shippingMethodsSelected
     * @param int $sellerId
     * @return array
     */
    public function populateTotalSellerShippingArr($totalSellerShippingArr, $shippingMethodsSelected, $sellerId)
    {
        try {
            if (isset($totalSellerShippingArr[$sellerId])) {
                $totalSellerShippingArr[$sellerId] += $shippingMethodsSelected['price'];
            } else {
                $totalSellerShippingArr[$sellerId] = $shippingMethodsSelected['price'];
            }
            return $totalSellerShippingArr;
        } catch (\Exception $e) {
            return $totalSellerShippingArr;
        }
    }

    /**
     * calls save at object
     *
     * @param object $object
     * @return void
     */
    public function saveObj($object)
    {
        $object->save();
    }
}
