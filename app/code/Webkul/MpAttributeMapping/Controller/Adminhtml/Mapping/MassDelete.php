<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAttributeMapping\Controller\Adminhtml\Mapping;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\MpAttributeMapping\Model\ResourceModel\Mapping\CollectionFactory;
use Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory;

class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory
     */
    protected $_collectionGroupFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param \Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory $collectionGroupFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory $collectionGroupFactory
    ) {
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_collectionGroupFactory=$collectionGroupFactory;
        parent::__construct($context);
    }

    /**
     * checks allowed role
     *
     * @return void
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_MpAttributeMapping::mapping');
    }

    /**
     * execute
     *
     * @return void
     */
    public function execute()
    {
        $mappingIds = [];
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        foreach ($collection as $mapping) {
            if ($this->deleteSubMapping($mapping->getId())) {
                $mappingIds[] = $mapping->getId();
                $this->removeItem($mapping);
            }
        }
        $this->messageManager->addSuccess(__('Mapping(s) deleted succesfully'));
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Remove Item
     *
     * @param object $item
     */
    protected function removeItem($item)
    {
        $item->delete();
    }

    /**
     * delete submapping
     *
     * @param int $mappingId
     * @return boolean
     */
    protected function deleteSubMapping($mappingId)
    {
        if ($mappingId) {
            try {
                $groupCollection=$this->_collectionGroupFactory->create();
                $filterGroupColl=$groupCollection->getCollection()
                ->addFieldToFilter('mapping_id', ['eq'=>$mappingId]);
                $filterGroupColl->walk('delete');
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }
}
