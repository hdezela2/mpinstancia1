<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpAttributeMapping\Controller\Adminhtml\Mapping;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Exception\LocalizedException;
use Webkul\MpAttributeMapping\Model\MappingFactory;
use Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory;

class Save extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;
    /**
     * @var Webkul\MpAttributeMapping\Model\MappingFactory
     */
    protected $mappingCollection;

    /**
     * @var \Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory
     */
    protected $mapGroupCollection;

    /**
     * @var \Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory
     */
    protected $messageManager;

    /**
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Webkul\MpAttributeMapping\Model\MappingFactory $mappingCollection
     * @param \Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory $mapGroupCollection
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Webkul\MpAttributeMapping\Model\MappingFactory $mappingCollection,
        \Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory $mapGroupCollection,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->mappingCollection=$mappingCollection;
        $this->mapGroupCollection=$mapGroupCollection;
        $this->resultPageFactory = $resultPageFactory;
        $this->messageManager = $messageManager;
    }

    /**
     * Product list page.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->getRequest()->getParams()) {
            $this->messageManager->addError(__("Something went wrong"));
            return $resultRedirect->setPath('*/*/');
        }
       
        try {
            $attributeGroupId='';
            $attributeIdArray = [];
            $requestParameterArray = $this->getRequest()->getParams();
            $mapCategoryArray  =  $requestParameterArray['map_category_attributes'];
            $attributeSetId  =   $mapCategoryArray['attribute_set_id'];
            $categoryId  =   $mapCategoryArray['category_id'];
            if (isset($requestParameterArray['map_category_attributes'])) {
                if (empty($mapCategoryArray['attribute_set_id']) || empty($attributeSetId)) {
                    $this->messageManager->addError(__("Attribute Set Id is Required."));
                    return $resultRedirect->setPath('*/*/');
                }
                if (empty($mapCategoryArray['category_id']) || empty($categoryId)) {
                    $this->messageManager->addError(__("Category Values are Required."));
                    return $resultRedirect->setPath('*/*/');
                }
                $mappingCollection = $this->mappingCollection->create();
                $filterResultCollection = $mappingCollection->getCollection()
                ->addFieldToFilter('attribute_set_id', ['eq'=>$attributeSetId])
                ->addFieldToFilter('category_id', ['eq'=>$categoryId]);
                $filterResultCollection->setPageSize(1, 1);
                if ($filterResultCollection->getSize()>0) {
                    $this->messageManager->addNotice(__("The mapping already exists in records."));
                    $mappingId = $filterResultCollection->getLastItem()->getEntityId();
                } else {
                    $mappingId = $this->saveMappingCollectiveData($mapCategoryArray, $categoryId, $attributeSetId);
                }
            }
            if (isset($requestParameterArray['attribute_groups'])) {
                $saveAssociatedResults = $this->saveAssociatedAttributesData($requestParameterArray, $mappingId);
                if ($saveAssociatedResults) {
                    $this->messageManager->addSuccess(__("Attributes Groups Successfully Saved"));
                }
            }
             return $resultRedirect->setPath('*/*/');
        } catch (\Exception $e) {
            $this->messageManager->addError(__("Something went wrong"));
            return $resultRedirect->setPath('*/*/');
        }
    }

    /**
     * Check for is allowed.
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
       
        return $this->_authorization->isAllowed('Webkul_MpAttributeMapping::attributemapping');
    }

   /**
    * update timestamp
    *
    * @param int $mappingId
    * @return void
    */
    protected function updateTimestamp($mappingId)
    {
        $getRecord = $this->mapGroupCollection->create()->load($mappingId, 'mapping_id');
        if ($getRecord->getId()) {
            $mappingCollection =$this->mappingCollection->create()->load($mappingId, 'entity_id');
            if ($mappingCollection->getId()) {
                $mappingCollection->setUpdatedAt($getRecord->getUpdatedAt());
                $mappingCollection->save();
            }
        }
    }

    /**
     * attribute id array
     *
     * @param object $collection
     * @return array
     */
    protected function getOnlyAttributeIds($collection)
    {
        $attributeIds=[];
        if ($collection->getSize()>0) {
            foreach ($collection as $attributeId) {
                 array_push($attributeIds, $attributeId->getAttributeId());
            }
        }
        return $attributeIds;
    }

    /**
     * save collectuve mapping data
     *
     * @param array $mapCategoryArray
     * @param int $categoryId
     * @param int $attributeSetId
     * @return void
     */
    protected function saveMappingCollectiveData($mapCategoryArray, $categoryId, $attributeSetId)
    {
        try {
            if (isset($mapCategoryArray['entity_id'])) {
                $filterCollection = $this->mappingCollection->create()->
                load($mapCategoryArray['entity_id'], 'entity_id');
                if ($filterCollection->getAttributeSetId()!= $attributeSetId &&
                $filterCollection->getCategoryId()!= $categoryId) {
                    $updateMapping= $this->mappingCollection->create()->getCollection()
                    ->addFieldToFilter('attribute_set_id', ['eq'=>$attributeSetId])
                    ->addFieldToFilter('category_id', ['eq'=>$categoryId]);
                    if ($updateMapping->getSize()>0) {
                        $this->messageManager->addSuccess(__("The mapping already exists in records."));
                        return $resultRedirect->setPath('*/*/');
                    } else {
                        $filterCollection->setAttributeSetId($attributeSetId);
                        $filterCollection->setCategoryId($categoryId);
                        $filterCollection->save();
                        $this->messageManager->addSuccess(__("The Record has been Updated successfully"));
                    }
                } elseif ($filterCollection->getAttributeSetId()!=$attributeSetId &&
                $filterCollection->getCategoryId()==$categoryId) {
                    $filterCollection->setAttributeSetId($attributeSetId);
                    $filterCollection->setCategoryId($categoryId);
                    $filterCollection->save();
                    $this->messageManager->addSuccess(__("The Record has been Updated successfully"));
                } elseif ($filterCollection->getAttributeSetId()==$attributeSetId &&
                $filterCollection->getCategoryId()!=$categoryId) {
                    $updateCategory=$this->mappingCollection->create()->load($categoryId, 'category_id');
                    if ($updateCategory->getId()>0) {
                        $this->messageManager->addError(__("The category is already mapped with an attribute set"));
                        return $resultRedirect->setPath('*/*/');
                    }
                    $filterCollection->setAttributeSetId($attributeSetId);
                    $filterCollection->setCategoryId($categoryId);
                    $filterCollection->save();
                    $this->messageManager->addSuccess(__("The Record has been Updated successfully"));
                } else {
                    $filterCollection->setAttributeSetId($attributeSetId);
                    $filterCollection->setCategoryId($categoryId);
                    $filterCollection->save();
                    $this->messageManager->addSuccess(__("The Mapping has been Updated successfully"));
                }
                
                $mappingId = $filterCollection->getEntityId();
            } else {
                $filterCollection = $this->mappingCollection->create()->load($categoryId, 'category_id');
                if ($filterCollection->getId()>0) {
                      $this->messageManager->addError(__("The category is already mapped with an attribute set"));
                      return $resultRedirect->setPath('*/*/');
                }
                $mappingCollection =$this->mappingCollection->create();
                $mappingCollection->setAttributeSetId($attributeSetId);
                $mappingCollection->setCategoryId($categoryId);
                $mappingCollection->save();
                $mappingId = $mappingCollection->getEntityId();
            }
            return $mappingId;
        } catch (\Exception $e) {
            $mappingId ='';
            return $mappingId;
        }
    }

    /**
     * save associated attributes
     *
     * @param array $data
     * @param int $mappingId
     * @return boolean
     */
    protected function saveAssociatedAttributesData($data, $mappingId)
    {
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $totalRecords= count($data['attribute_groups']);
            if ($totalRecords==0) {
                $this->messageManager->addError(__("Please select allowed attributes for attribute set"));
                 return $resultRedirect->setPath('*/*/');
            }
            if (empty($mappingId)) {
                $this->messageManager->addError(__("Mapping Id for the attributes do not exist"));
                 return $resultRedirect->setPath('*/*/');
            }
            $groupCollection = $this->mapGroupCollection->create();
            $filterGroupCollection = $groupCollection->getCollection()
            ->addFieldToFilter('mapping_id', ['eq'=>$mappingId]);
            $dbAttributeIds = $this->getOnlyAttributeIds($filterGroupCollection);
            $totalAttributesRecord = count($filterGroupCollection);
            $totalPostArrayAttributes=count($data['attribute_groups']);

            if ($totalAttributesRecord == $totalPostArrayAttributes) {
                foreach ($data['attribute_groups'] as $attributes) {
                    $attributeGroup = explode('-', $attributes);
                    array_push($attributeIdArray, $attributeGroup['1']);
                    $bulkInsert[] = [
                        'mapping_id' => $mappingId,
                        'attribute_group_id' => $attributeGroup['0'],
                        'attribute_id'  =>$attributeGroup['1']
                    ];
                }
                $mergedArray = array_merge(
                    array_diff($attributeIdArray, $dbAttributeIds),
                    array_diff($dbAttributeIds, $attributeIdArray)
                );
                $filterGroupCollection->walk('delete');
                $groupCollection->insertMultiple('marketplace_attributes_to_group_mapping', $bulkInsert);
                $groupCollection->save();
                if (!empty($mergedArray)) {
                    $this->updateTimestamp($mappingId);
                }
            } else {
                foreach ($data['attribute_groups'] as $attributes) {
                    $attributeGroup = explode('-', $attributes);
                    $bulkInsert[] = [
                        'mapping_id' => $mappingId,
                        'attribute_group_id' => $attributeGroup['0'],
                        'attribute_id'  =>$attributeGroup['1']
                    ];
                }
                $filterGroupCollection->walk('delete');
                $groupCollection->insertMultiple('marketplace_attributes_to_group_mapping', $bulkInsert);
                $groupCollection->save();
                $this->updateTimestamp($mappingId);
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
