<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAttributeMapping\Controller\Adminhtml\Ajax;

use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as AttributeColl;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory  as AttributeGroupCollection;
 
class ShowGroups extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    
    /**
     * @var \Webkul\MpAttributeMapping\Helper\Data
     */
    protected $helper;

    /**
     * @var AttributeGroupCollection
     */
    protected $attributeGroupCollection;

    /**
     * @var AttributeColl
     */
    protected $_productAttributeCollection;

    /**
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param AttributeColl $productAttribute
     * @param AttributeGroupCollection $attributeGroupCollection
     * @param \Webkul\MpAttributeMapping\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        AttributeColl $productAttribute,
        AttributeGroupCollection $attributeGroupCollection,
        \Webkul\MpAttributeMapping\Helper\Data $helper
    ) {
       
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_productAttributeCollection = $productAttribute;
        $this->attributeGroupCollection = $attributeGroupCollection;
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * execute
     *
     * @return void
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        try {
              $html='';
              $attributeIds = [];
              $postParams = $this->getRequest()->getParams();
              $attributeSetId = $postParams['attribute_set_id'];
              $mappingId = $postParams['mapping_id'];
            if ($mappingId!="") {
                  $groupAtrCollection=$this->helper->getGroupMapping($mappingId);
                if ($groupAtrCollection->getSize()>0) {
                    foreach ($groupAtrCollection as $groupColl) {
                        array_push($attributeIds, $groupColl->getAttributeId());
                    }
                }
            }
            if ($attributeSetId!="") {
                   $groupCollection = $this->attributeGroupCollection->create()
                   ->setAttributeSetFilter($attributeSetId)
                   ->setOrder('sort_order', 'ASC')
                   ->load(); // product attribute group collection
                if ($groupCollection->getSize()>0) {
                    $html = $this->getHtmlContentForGroups($groupCollection->getItems(), $attributeIds);
                } else {
                    $html.='<option selected="" value="">No Available Options</option>';
                    return $result->setData(['success' => false,'value'=>$html]);
                }
            }
                  return $result->setData(['success' => true,'value'=>$html]);
        } catch (\Exception $e) {
            $html="";
              return $result->setData(['success' => false,'value'=>$html]);
        }
    }

    /**
     * html content for div
     *
     * @param collection $groupCollection
     * @return void
     */
    protected function getHtmlContentForGroups($groupCollection, $attributeIds)
    {
        try {
            $html='';
            foreach ($groupCollection as $groups) {
                if ($this->checkZeroAttributesWithinGroup($groups->getId())) {
                    $html.='<optgroup label="'.$groups->getAttributeGroupName().'" id="'.$groups->getAttributeGroupId().'">';
                    $groupAttributesCollection = $this->loadGroupAttributesCollection($groups->getId());
                    $attributeIdValues = [];
                    foreach ($groupAttributesCollection->getItems() as $attribute) {
                         array_push($attributeIdValues, $attribute->getAttributeId());
                        $selected='';
                        if (in_array($attribute->getAttributeId(), $attributeIds)) {
                            $selected='selected=""';
                        }
                        $html.=' <option value="'.$groups->getAttributeGroupId().'-'.$attribute->getAttributeId().'" '.$selected.' >'.$attribute->getFrontendLabel().'</option>';
                    }
                     $html.='</optgroup>';
                }
            }
            return $html;
        } catch (\Exception $e) {
            return $html;
        }
    }

    /**
     * load attribute groups
     *
     * @param int $groupId
     * @return void
     */
    protected function loadGroupAttributesCollection($groupId)
    {
        $groupAttributesCollection = [];
        $groupAttributesCollection = $this->_productAttributeCollection->create()
                ->setAttributeGroupFilter($groupId)
                ->addFieldToFilter('is_user_defined', ['eq' => 1])
                ->addVisibleFilter()
                ->load();
        if ($groupAttributesCollection->getSize()) {
            return $groupAttributesCollection;
        }
        return $groupAttributesCollection;
    }

    /**
     * check whether option group have userdefined attributes
     *
     * @param int $groupId
     * @return boolean
     */
    private function checkZeroAttributesWithinGroup($groupId)
    {
        $groupAttributesCollection = $this->_productAttributeCollection->create()
                ->setAttributeGroupFilter($groupId)
                ->addFieldToFilter('is_user_defined', ['eq' => 1])
                ->addVisibleFilter()
                ->load(); // product attribute collection
        if ($groupAttributesCollection->getSize()) {
            return true;
        }
        return false;
    }
}
