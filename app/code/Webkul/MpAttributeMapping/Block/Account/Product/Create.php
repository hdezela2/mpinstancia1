<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAttributeMapping\Block\Account\Product;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as AttributeColl;
use Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory as MapAttributesToGroups;
use Webkul\MpAttributeMapping\Model\MappingFactory as MappingFactory;
use Webkul\MpAttributeMapping\Model\ProductMappingFactory as ProductMappingFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Webkul MpAttributeMapping Product Create Block.
 */
class Create extends \Magento\Framework\View\Element\Template
{
    /**
     * @var CollectionFactory
     */
    protected $_attributeGroupCollection;

    /**
     * @var AttributeColl
     */
    protected $_productAttributeCollection;

    /**
     * @var \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet
     */
    protected $attributeSet;

    /**
     * @var \Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory
     */
    protected $groupsMapping;

    /**
     * @var \Webkul\MpAttributeMapping\Model\MappingFactory
     */
    protected $mapping;

    /**
     * @var \Webkul\MpAttributeMapping\Model\ProductMappingFactory
     */
    protected $productMapping;

  /**
   * @param \Magento\Framework\View\Element\Template\Context $context
   * @param CollectionFactory $attributeGroup
   * @param AttributeColl $productAttribute
   * @param \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet
   * @param MapAttributesToGroups $groupsMapping
   * @param MappingFactory $mapping
   * @param ProductMappingFactory $productMapping
   * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
   * @param \Magento\Catalog\Model\ResourceModel\Product $product
   * @param \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributesInfo
   * @param \Magento\Catalog\Model\ProductFactory $productLoader
   * @param array $data
   */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        CollectionFactory $attributeGroup,
        AttributeColl $productAttribute,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet,
        MapAttributesToGroups $groupsMapping,
        MappingFactory $mapping,
        ProductMappingFactory $productMapping,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Product $product,
        \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributesInfo,
        \Magento\Catalog\Model\ProductFactory $productLoader,
        array $data = []
    ) {
        $this->_attributeGroupCollection = $attributeGroup;
        $this->_productAttributeCollection = $productAttribute;
        $this->attributeSet = $attributeSet;
        $this->groupsMapping=$groupsMapping;
        $this->mapping = $mapping;
        $this->productMapping = $productMapping;
        $this->categoryFactory = $categoryFactory;
        $this->product=$product;
        $this->attributesInfo=$attributesInfo;
        $this->productLoader=$productLoader;
        parent::__construct($context, $data);
    }

    /**
     * collect all mapped attributes
     * @param  int $attributeSetId
     * @return array $categoryid
     */
    public function getMappedAttributes($mappingId)
    {
        $associatedGroupAttributes=[];
        if (!empty($mappingId)) {
            $associatedGroupAttributes= $this->getMappedGroupAttributes($mappingId);
        } else {
            $associatedGroupAttributes=$this->showDefaultAttributeGroups();
        }
         return $associatedGroupAttributes;
    }

   /**
    * get mapped group attributes
    *
    * @param int $mappingId
    * @return array
    */
    public function getMappedGroupAttributes($mappingId)
    {
        $groupMapping=[];
        if ($mappingId) {
            $groupMapping=$this->groupsMapping->create()->getCollection()->
            addFieldToFilter('mapping_id', ['eq'=>$mappingId]);
            if ($groupMapping->getSize()>0) {
                return $groupMapping;
            }
        }
        return $groupMapping;
    }

   /**
    * return default attributes groups
    *
    * @return void
    */
    public function showDefaultAttributeGroups()
    {
        $groupAttributesCollection = [];
        $attributeSetId=$this->product->getTypeId();
        $groupCollection = $this->_attributeGroupCollection->create()
        ->setAttributeSetFilter($attributeSetId)
        ->load(); // product attribute group collection
        foreach ($groupCollection as $group) {
            $groupAttributesCollection = $this->getGroupAttributeCollection($group->getId());
        }
        return $groupAttributesCollection;
    }

    /**
     * get group attribute collection
     *
     * @param int $groupId
     * @return array
     */
    public function getGroupAttributeCollection($groupId)
    {
        $groupAttributesCollection = [];
        if ($groupId) {
            return $groupAttributesCollection = $this->_productAttributeCollection->create()
            ->setAttributeGroupFilter($groupId)
            ->addVisibleFilter()
            ->load(); // product attribute collection
        }
        return $groupAttributesCollection;
    }

    /**
     * get all parent category
     *
     * @param int $categoryId
     * @return void
     */
    public function getAllParentCategory($categoryId)
    {
        $parentCategoryArray=[];
        if (!empty($categoryId)) {
            try {
                $category=$this->categoryFactory->create()->load($categoryId);
                // Parent Categories
                $parentCategories = $category->getParentCategories();
                foreach ($parentCategories as $category) {
                    array_push($parentCategoryArray, $category->getParentId());
                }
                rsort($parentCategoryArray);
                return $parentCategoryArray;
            } catch (\Exception $e) {
                return  $parentCategoryArray;
            }
        }
    }

  /**
   * get attribute info
   *
   * @param array $attributeArray
   * @return void
   */
    public function getAttributesInfo($attributeArray)
    {
        $attributeInfoCollection=[];
        if (!empty($attributeArray)) {
            try {
                $attributeInfoCollection = $this->attributesInfo->create()->getCollection()
                ->addFieldToFilter("attribute_id", ["in" => $attributeArray]);
            } catch (\Exception $e) {
                return $attributeInfoCollection;
            }
        }

        return $attributeInfoCollection;
    }

   /**
    * returns product collection
    *
    * @param int $productId
    * @return void
    */
    public function getProductCollection($productId)
    {
        $product=[];
        if ($productId) {
            try {
                return $product =  $this->productLoader->create()->load($productId);
            } catch (\Exception $e) {
                return $product;
            }
        }
    }

    /**
     * return mappingId
     *
     * @param int $productId
     * @return int
     */
    public function getEditProductMappingId($productId)
    {
        $mappingId="";
        if ($productId) {
            try {
                $productMappingCollection = $this->productMapping->create()->load($productId, 'product_id');
                if ($productMappingCollection->getId()) {
                    return $mappingId = $productMappingCollection->getMappingId();
                }
            } catch (\Exception $e) {
                return $mappingId;
            }
        }
    }
}
