<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAttributeMapping\Block\Adminhtml\Mapping;

use Magento\Customer\Controller\RegistryConstants;
use Webkul\MpAttributeMapping\Model\MappingFactory as MappingCollection;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory as AttributeGroupColl;

class MapAttributesToSet extends \Magento\Backend\Block\Template
{
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'mapping/map_attribute_to_set.phtml';
    
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $mapping;

    /**
     * @var Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory
     */
    protected $attributeGroupCollection;

   /**
    * @param \Magento\Backend\Block\Template\Context $context
    * @param MappingCollection $mapping
    * @param AttributeGroupColl $attributeGroupCollection
    * @param array $data
    */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        MappingCollection $mapping,
        AttributeGroupColl $attributeGroupCollection,
        array $data = []
    ) {
        $this->mapping = $mapping;
        $this->attributeGroupCollection=$attributeGroupCollection;
        parent::__construct($context, $data);
    }

    /**
     * get mapping Id
     *
     * @return int
     */
    public function getMappingId()
    {
        if ($this->getRequest()->getParam('id')) {
            return $this->getRequest()->getParam('id');
        }
        return '';
    }

   /**
    * Undocumented function
    *
    * @return void
    */
    public function getAttributeSetId()
    {
        $attributeSetId='';
        $mappingRowId=$this->getMappingId();
        if ($mappingRowId) {
            $mappingColl = $this->mapping->create()->load($mappingRowId, 'entity_id');
            if ($mappingColl->getId()) {
                $attributeSetId = $mappingColl->getAttributeSetId();
            }
        }
        return $attributeSetId;
    }

   /**
    * get attribute groups
    *
    * @return array
    */
    public function getAttributeGroups()
    {
        $attributeSetId = $this->getAttributeSetId();
        $groupIds = [];
        $attributeids = [];
        $groupCollection='';
        if ($attributeSetId) {
            $groupCollection = $this->attributeGroupCollection->create()
            ->setAttributeSetFilter($attributeSetId)
            ->setOrder('sort_order', 'ASC')
            ->load(); // product attribute group collection
        }

        return $groupCollection;
    }
}
