<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 namespace Webkul\MpAttributeMapping\Plugin\Controller\Product;

use Webkul\MpAttributeMapping\Helper\Data as MappingHelper;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;
use Magento\Customer\Model\Session;
 
class Create
{
    /**
     * @param \Webkul\MpAttributeMapping\Model\ProductMappingFactory $productMapping
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param MappingHelper $mappingHelper
     * @param MarketplaceHelper $marketplaceHelper
     */
    public function __construct(
        \Webkul\MpAttributeMapping\Model\ProductMappingFactory $productMapping,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        MappingHelper $mappingHelper,
        MarketplaceHelper $marketplaceHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        PageFactory $resultPageFactory,
        FormKeyValidator $formKeyValidator,
        Session $customerSession
    ) {
        $this->productMapping=$productMapping;
        $this->productRepository = $productRepository;
        $this->_mappingHelper=$mappingHelper;
        $this->_marketplaceHelper=$marketplaceHelper;
        $this->messageManager = $messageManager;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_formKeyValidator = $formKeyValidator;
        $this->_customerSession = $customerSession;
    }

    /**
     * after execute
     *
     * @param \Webkul\Marketplace\Controller\Product\Save $subject
     * @param object $result
     * @return void
     */
    public function aroundExecute(
        \Webkul\Marketplace\Controller\Product\Create $subject,
        callable $proceed
    ) {
        if ($this->_mappingHelper->getModuleEnabled()) {
            $set = "";
            $mappingId="";
            $allowedCategoryArray = [];
            $isPartner = $this->_marketplaceHelper->isSeller();
            if ($isPartner == 1) {
                try {
                    $categoryId = $subject->getRequest()->getParam('category_id');
                    if (empty($categoryId)) {
                        $allowedCategory = $this->_marketplaceHelper->getAllowedCategoryIds();
                        $searchString = ',';
                        if (empty($allowedCategory)) {
                            $allowedCategoryArray =[];
                        } else {
                            if (strpos($allowedCategory, $searchString) !== false) {
                                $allowedCategoryArray = explode(',', $allowedCategory);
                            } else {
                                $allowedCategoryArray =[$allowedCategory] ;
                            }
                        }
                        if (count($allowedCategoryArray) == 1) {
                            $categoryId =  $allowedCategoryArray[0];
                        }
                    }
                        $associatedAttributeArray = $this->_mappingHelper->checkMappingExists($categoryId);
                    if (!empty($associatedAttributeArray)) {
                        $associatedAttributeSet = $associatedAttributeArray['attribute_set_id'];
                        $mappingId = $associatedAttributeArray['mapping_id'];
                        $set = $associatedAttributeSet;
                    }
                       
                        $this->_mappingHelper->setSessionCategoryVal($categoryId);
                        $this->_mappingHelper->setSessionMappingVal($mappingId);
                       
                    $allowedAttributesetIds = $this->_marketplaceHelper->getAllowedAttributesetIds();
                    $allowedProductType = $this->_marketplaceHelper->getAllowedProductType();
                    $allowedSets = [];
                    $allowedTypes = [];
                    if (trim($allowedAttributesetIds)) {
                        $allowedSets = explode(',', $allowedAttributesetIds);
                    }
                    if (trim($allowedProductType)) {
                        $allowedTypes = explode(',', $allowedProductType);
                    }
                    if (count($allowedSets) > 1 || count($allowedTypes) > 1 || count($allowedCategoryArray)!=1) {
                        if (!$subject->getRequest()->isPost()) {
                            /** @var \Magento\Framework\View\Result\Page $resultPage */
                            $resultPage = $this->_resultPageFactory->create();
                            if ($this->_marketplaceHelper->getIsSeparatePanel()) {
                                $resultPage->addHandle('marketplace_layout2_product_create');
                            }
                            $resultPage->getConfig()->getTitle()->set(
                                __('Add New Product')
                            );
    
                            return $resultPage;
                        }
                        if (!$this->_formKeyValidator->validate($subject->getRequest())) {
                            return $this->resultRedirectFactory->create()->setPath(
                                '*/*/create',
                                ['_secure' => $subject->getRequest()->isSecure()]
                            );
                        }
                        $type = $subject->getRequest()->getParam('type');
                        if (isset($set) && isset($type)) {
                            if (empty($categoryId)) {
                                $this->messageManager->addError(
                                    'Category is Invalid Or Not Allowed'
                                );
                                return $this->resultRedirectFactory->create()
                                ->setPath(
                                    '*/*/create',
                                    ['_secure' => $subject->getRequest()->isSecure()]
                                );
                            }
                            if (!in_array($type, $allowedTypes) || !in_array($set, $allowedSets)) {
                                $this->messageManager->addError(
                                    'Category is Invalid Or Not Allowed'
                                );
    
                                return $this->resultRedirectFactory->create()
                                ->setPath(
                                    '*/*/create',
                                    ['_secure' => $subject->getRequest()->isSecure()]
                                );
                            }
                          
                            if (!isset($categoryId) || empty($categoryId)) {
                                $this->messageManager->addError(
                                    'Category type Invalid or Not allowed'
                                );
    
                                return $this->resultRedirectFactory->create()
                                ->setPath(
                                    '*/*/create',
                                    ['_secure' => $subject->getRequest()->isSecure()]
                                );
                            }
                            $this->_getSession()->setAttributeSet($set);
                            return $this->resultRedirectFactory->create()
                            ->setPath(
                                '*/*/add',
                                [
                                    'set' => $set,
                                    'type' => $type,
                                    '_secure' => $subject->getRequest()->isSecure(),
                                ]
                            );
                        } else {
                            $this->messageManager->addError(
                                __('Please select attribute set and product type.')
                            );
    
                            return $this->resultRedirectFactory->create()
                            ->setPath(
                                '*/*/create',
                                ['_secure' => $subject->getRequest()->isSecure()]
                            );
                        }
                    } elseif (count($allowedSets) == 0 || count($allowedTypes) == 0) {
                        $this->messageManager->addError(
                            'Please ask admin to configure product settings properly to add products.'
                        );
    
                        return $this->resultRedirectFactory->create()
                        ->setPath(
                            'marketplace/account/dashboard',
                            ['_secure' => $subject->getRequest()->isSecure()]
                        );
                    } else {
                        if (!empty($mappingId) && $this->_mappingHelper->getAttributeSetFromMapping($mappingId) != $allowedSets[0]) {
                            $this->messageManager->addError(
                                'Please ask admin to allow the mapped attribute set from configuration to add products.'
                            );
                            return $this->resultRedirectFactory->create()
                            ->setPath(
                                'marketplace/account/dashboard',
                                ['_secure' => $subject->getRequest()->isSecure()]
                            );
                        }
                        $this->_getSession()->setAttributeSet($allowedSets[0]);
    
                        return $this->resultRedirectFactory->create()
                        ->setPath(
                            '*/*/add',
                            [
                                'set' => $allowedSets[0],
                                'type' => $allowedTypes[0],
                                '_secure' => $subject->getRequest()->isSecure(),
                            ]
                        );
                    }
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
    
                    return $this->resultRedirectFactory->create()
                    ->setPath(
                        '*/*/create',
                        ['_secure' => $subject->getRequest()->isSecure()]
                    );
                }
            } else {
                return $this->resultRedirectFactory->create()
                ->setPath(
                    'marketplace/account/becomeseller',
                    ['_secure' => $subject->getRequest()->isSecure()]
                );
            }
        } else {
            $result= $proceed();
            return $result;
        }
    }

    /**
     * Retrieve customer session object.
     *
     * @return \Magento\Customer\Model\Session
     */
    protected function _getSession()
    {
        return $this->_customerSession;
    }
}
