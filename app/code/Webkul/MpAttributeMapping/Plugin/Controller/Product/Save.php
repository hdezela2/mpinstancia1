<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 namespace Webkul\MpAttributeMapping\Plugin\Controller\Product;

 use Webkul\MpAttributeMapping\Model\ProductMappingFactory;
 
class Save
{
   /**
    * @param \Webkul\MpAttributeMapping\Model\ProductMappingFactory $productMapping
    * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    * @param \Webkul\MpAttributeMapping\Helper\Data $attributeHelper
    * @param \Webkul\Marketplace\Helper\Data $mpHelper
    * @param \Magento\Framework\Message\ManagerInterface $messageManager
    * @param \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
    */
    public function __construct(
        \Webkul\MpAttributeMapping\Model\ProductMappingFactory $productMapping,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Webkul\MpAttributeMapping\Helper\Data $attributeHelper,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
    ) {
        $this->productMapping=$productMapping;
        $this->productRepository = $productRepository;
        $this->attributeHelper = $attributeHelper;
        $this->mpHelper = $mpHelper;
        $this->messageManager = $messageManager;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    /**
     * after execute
     *
     * @param \Webkul\Marketplace\Controller\Product\Save $subject
     * @param object $result
     * @return void
     */
    public function afterExecute(
        \Webkul\Marketplace\Controller\Product\Save $subject,
        $result
    ) {
        if ($this->attributeHelper->getModuleEnabled()) {
            try {
                $allowedSets = [];
                $wholedata = $subject->getRequest()->getParams();
                if (!empty($wholedata['set']) && !empty($wholedata['product_id'])) {
                    $productId = $wholedata['product_id'];
                    $selectedSet = $wholedata['set'];
                    $mpAllowedSets =  $this->mpHelper->getAllowedSets();
                    foreach ($mpAllowedSets as $setId) {
                        array_push($allowedSets, $setId['value']);
                    }
                    
                    if (!in_array($selectedSet, $allowedSets)) {
                        $this->messageManager->addError(
                            __('The attribute set is not allowed on the product. Please ask admin to allow mapped attribute set')
                        );
                        return $this->resultRedirectFactory->create()->setPath(
                            '*/*/edit',
                            [
                                'id' => $productId,
                                '_secure' => $subject->getRequest()->isSecure(),
                            ]
                        );
                    }
                }
                return $result;
            } catch (\Exception $e) {
                return $result;
            }
        }
        return $result;
    }
}
