<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 namespace Webkul\MpAttributeMapping\Plugin\Controller\Product;

 use Webkul\MpAttributeMapping\Model\ProductMappingFactory;

class SaveProduct
{
    /**
     * @var\Magento\Catalog\Model\Product
     */
    protected $_product;

   /**
    * @param \Webkul\MpAttributeMapping\Model\ProductMappingFactory $productMapping
    * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    * @param \Webkul\MpAttributeMapping\Helper\Data $helper
    */
    public function __construct(
        \Webkul\MpAttributeMapping\Model\ProductMappingFactory $productMapping,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Webkul\MpAttributeMapping\Helper\Data $helper
    ) {
        $this->productMapping=$productMapping;
        $this->productRepository = $productRepository;
        $this->helper = $helper;
    }
    public function afterSaveProductData(
        \Webkul\Marketplace\Controller\Product\SaveProduct $subject,
        $result
    ) {
        try {
            if ($this->attributeHelper->getModuleEnabled()) {
                $wholedata = $this->helper->getRequestParam();
                $mappingId="";
                if (isset($wholedata['mapping_id'])) {
                    $mappingId=$wholedata['mapping_id'];
                }
                $productId = $result['product_id'];
                $productMappingColletion=$this->productMapping->create();
                if ($productId && $mappingId!="") {
                    $productMappingColletion->setProductId($productId);
                    $productMappingColletion->setMappingId($mappingId);
                    $productMappingColletion->save();
                }
                return $result;
            }
            return $result;
        } catch (\Exception $e) {
            return $result;
        }
    }
}
