<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 namespace Webkul\MpAttributeMapping\Plugin\Block\Product;

 use Webkul\MpAttributeMapping\Model\ProductMappingFactory;
 use Magento\Framework\App\Request\DataPersistorInterface;
 
class Create
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

   /**
    * @param \Webkul\MpAttributeMapping\Model\ProductMappingFactory $productMapping
    * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    * @param \Webkul\MpAttributeMapping\Helper\Data $helper
    * @param DataPersistorInterface $dataPersistor
    */
    public function __construct(
        \Webkul\MpAttributeMapping\Model\ProductMappingFactory $productMapping,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Webkul\MpAttributeMapping\Helper\Data $helper,
        DataPersistorInterface $dataPersistor
    ) {
        $this->productMapping=$productMapping;
        $this->productRepository = $productRepository;
        $this->helper=$helper;
        $this->dataPersistor = $dataPersistor;
    }

    /**
     * get persistent data
     *
     * @param \Webkul\Marketplace\Block\Product\Create $subject
     * @param \Closure $proceed
     * @return void
     */
    public function aroundGetPersistentData(
        \Webkul\Marketplace\Block\Product\Create $subject,
        \Closure $proceed
    ) {
        $categoryId=$this->helper->getSessionCategoryVal();
        if (!empty($categoryId)) {
            $categoryArray=$this->helper->setParentCategoriesInCategoryUi($categoryId);
            
            if (empty($persistentData['product']['category_ids'])) {
                $persistentData['product']['category_ids'] = $categoryArray;
            }
            $result = $proceed($persistentData);
            $result['product']['category_ids']=$categoryArray;
            return $result;
        }
        return $proceed();
    }
}
