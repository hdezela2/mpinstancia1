<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 namespace Webkul\MpAttributeMapping\Plugin\Block\Product\Helper\Form\Gallery;

 use Webkul\MpAttributeMapping\Model\ProductMappingFactory;
 use Magento\Framework\App\Request\DataPersistorInterface;
 use Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory as MapAttributesToGroups;
 use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as AttributeColl;
 use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;
 
class Content
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var MapAttributesToGroups
     */
    protected $groupsMapping;

   /**
    * @param \Webkul\MpAttributeMapping\Model\ProductMappingFactory $productMapping
    * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    * @param \Webkul\MpAttributeMapping\Helper\Data $helper
    * @param DataPersistorInterface $dataPersistor
    * @param MapAttributesToGroups $groupsMapping
    * @param \Magento\Catalog\Model\ResourceModel\Product $product
    * @param CollectionFactory $attributeGroup
    * @param AttributeColl $productAttribute
    * @param \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributesInfo
    * @param \Magento\Framework\App\Request\Http $request
    */
    public function __construct(
        \Webkul\MpAttributeMapping\Model\ProductMappingFactory $productMapping,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Webkul\MpAttributeMapping\Helper\Data $helper,
        DataPersistorInterface $dataPersistor,
        MapAttributesToGroups $groupsMapping,
        \Magento\Catalog\Model\ResourceModel\Product $product,
        CollectionFactory $attributeGroup,
        AttributeColl $productAttribute,
        \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributesInfo,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->productMapping=$productMapping;
        $this->productRepository = $productRepository;
        $this->helper=$helper;
        $this->dataPersistor = $dataPersistor;
        $this->groupsMapping=$groupsMapping;
        $this->product=$product;
        $this->_attributeGroupCollection = $attributeGroup;
        $this->_productAttributeCollection = $productAttribute;
        $this->attributesInfo=$attributesInfo;
        $this->request = $request;
    }

  /**
   * around get product media attributes
   *
   * @param \Webkul\Marketplace\Block\Product\Helper\Form\Gallery\Content $subject
   * @param \Closure $proceed
   * @return void
   */
    public function aroundGetProductMediaAttributes(
        \Webkul\Marketplace\Block\Product\Helper\Form\Gallery\Content $subject,
        \Closure $proceed
    ) {
        try {
            if ($this->helper->getModuleEnabled()) {
                $productId=$this->request->getParam('id');
                if ($this->request->getFullActionName() == 'marketplace_product_edit') {
                    $mappingId = $this->getEditProductMappingId($productId);
                }
                if ($this->request->getFullActionName() == 'marketplace_product_add') {
                    $mappingId = $this->helper->getSessionMappingVal();
                }
                   
                    $userDefinedMediaAttributes  = [];
                    $attributeIdArray = [];
                    $associatedAttributeCollection = [];
                    $attributeSet = $proceed();
                if (!empty($mappingId)) {
                    $attributeIds = $this->getMappedAttributes($mappingId);
                    foreach ($attributeIds as $attributeId) {
                        array_push($attributeIdArray, $attributeId->getAttributeId());
                    }
                    $associatedAttributeCollection = $this->getAttributesInfo($attributeIdArray);
                    if (!empty($associatedAttributeCollection)) {
                        foreach ($associatedAttributeCollection as $mappedAttributes) {
                            if ($mappedAttributes->getIsUserDefined() == 1 && $mappedAttributes->getFrontendInput() =='media_image') {
                                array_push($userDefinedMediaAttributes, $mappedAttributes->getAttributeCode());
                                if (!in_array($mappedAttributes->getAttributeCode(), $attributeSet)) {
                                    $attributeSet[$mappedAttributes->getAttributeCode()] = $mappedAttributes;
                                }
                            }
                        }
                    }
                    foreach ($attributeSet as $key => $data) {
                        if ($data->getIsUserDefined() == 1 && !in_array($data->getAttributeCode(), $userDefinedMediaAttributes)) {
                            unset($attributeSet[$key]);
                        }
                    }
                    return $attributeSet;
                }
            }
        } catch (\Exception $e) {
            return $proceed();
        }
      
        return $proceed();
    }

    /**
     * get edit product mapping id
     *
     * @param int $productId
     * @return void
     */
    private function getEditProductMappingId($productId)
    {
        $mappingId="";
        if ($productId) {
            try {
                $productMappingCollection = $this->productMapping->create()->load($productId, 'product_id');
                if ($productMappingCollection->getId()) {
                    return $mappingId = $productMappingCollection->getMappingId();
                }
                return $mappingId;
            } catch (\Exception $e) {
                return $mappingId;
            }
        }
    }

    /**
     * get mapped attributes
     *
     * @param int $mappingId
     * @return void
     */
    private function getMappedAttributes($mappingId)
    {
        $associatedGroupAttributes=[];
        if (!empty($mappingId)) {
            $associatedGroupAttributes= $this->getMappedGroupAttributes($mappingId);
        } else {
            $associatedGroupAttributes=$this->showDefaultAttributeGroups();
        }
      
         return $associatedGroupAttributes;
    }
    
    /**
     * get mapped group
     *
     * @param int $mappingId
     * @return void
     */
    private function getMappedGroupAttributes($mappingId)
    {
        $groupMapping=[];
        if ($mappingId) {
            $groupMapping=$this->groupsMapping->create()->getCollection()->
            addFieldToFilter('mapping_id', ['eq'=>$mappingId]);
            if ($groupMapping->getSize()>0) {
                return $groupMapping;
            }
        }
        return $groupMapping;
    }

    /**
     * show default attribute group
     *
     * @return void
     */
    public function showDefaultAttributeGroups()
    {
        $groupAttributesCollection = [];
        $attributeSetId=$this->product->getTypeId();
        $groupCollection = $this->_attributeGroupCollection->create()
        ->setAttributeSetFilter($attributeSetId)
        ->load(); // product attribute group collection
        foreach ($groupCollection as $group) {
            $groupAttributesCollection = $this->getGroupAttributeCollection($group->getId());
        }
        return $groupAttributesCollection;
    }

    /**
     * get group attribute collection
     *
     * @param int $groupId
     * @return void
     */
    public function getGroupAttributeCollection($groupId)
    {
        $groupAttributesCollection = [];
        if ($groupId) {
            return $groupAttributesCollection = $this->_productAttributeCollection->create()
            ->setAttributeGroupFilter($groupId)
            ->addVisibleFilter()
            ->load(); // product attribute collection
        }
        return $groupAttributesCollection;
    }

    /**
     * get attribute info
     *
     * @param array $attributeArray
     * @return void
     */
    public function getAttributesInfo($attributeArray)
    {
        $attributeInfoCollection=[];
        if (!empty($attributeArray)) {
            try {
                $attributeInfoCollection = $this->attributesInfo->create()->getCollection()
                ->addFieldToFilter("attribute_id", ["in" => $attributeArray]);
            } catch (\Exception $e) {
                return $attributeInfoCollection;
            }
        }
        return $attributeInfoCollection;
    }
}
