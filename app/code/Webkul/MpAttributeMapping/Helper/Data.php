<?php
/*
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright(c)Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAttributeMapping\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Model\Session;
use Magento\Quote\Model\Quote\ItemFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\App\ProductMetadataInterface;

/**
 * Class Helper Data
 */
class Data extends AbstractHelper
{
    /**
     * @var Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory
     */
    protected $_mappingGroups;
   
    /**
     * @var Webkul\MpAttributeMapping\Model\MappingFactory
     */
    private $_mapping;
    /**
     * @var Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var  \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $_categories;
    
   /**
    *
    * @param Context $context
    * @param StoreManagerInterface $storeManager
    * @param \Webkul\MpAttributeMapping\Model\MappingFactory $mapping
    * @param \Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory $mappingGroups
    * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categories
    * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
    * @param \Magento\Catalog\Model\ResourceModel\Product $product
    * @param \Magento\Catalog\Ui\Component\Product\Form\Categories\OptionsFactory $formCategoryOptions
    * @param \Magento\Framework\Session\SessionManagerInterface $sessionManager
    * @param \Magento\Framework\App\Response\Http $response
    * @param \Magento\Framework\Message\ManagerInterface $messageManager
    * @param \Magento\Framework\Registry $registry
    */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        \Webkul\MpAttributeMapping\Model\MappingFactory $mapping,
        \Webkul\MpAttributeMapping\Model\MapAttributesToGroupsFactory $mappingGroups,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categories,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Product $product,
        \Magento\Catalog\Ui\Component\Product\Form\Categories\OptionsFactory $formCategoryOptions,
        \Magento\Framework\Session\SessionManagerInterface $sessionManager,
        \Magento\Framework\App\Response\Http $response,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Registry $registry
    ) {
        $this->_storeManager        =   $storeManager;
        $this->_mapping      =   $mapping;
        $this->_mappingGroups      =   $mappingGroups;
        $this->_categories                 =   $categories;
        $this->categoryFactory=$categoryFactory;
        $this->product=$product;
        $this->formCategoryOptions=$formCategoryOptions;
        $this->sessionManager = $sessionManager;
        $this->response=$response;
        $this->request=$request;
        $this->messageManager = $messageManager;
        $this->_registry = $registry;
        parent::__construct($context);
    }

    /**
     * return config values
     *
     * @param int $field
     * @param int $storeId
     * @return void
     */
    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue($field, ScopeInterface::SCOPE_STORE, $storeId);
    }

   /**
    * return module enabled or not
    *
    * @return boolean
    */
    public function getModuleEnabled()
    {
        return  $this->scopeConfig->getValue(
            'attributemapping/enable/status',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
   
    /**
     * return session category value
     *
     * @param int $category
     * @return string
     */
    public function setSessionCategoryVal($category)
    {
        $this->sessionManager->setSelectedCategory($category);
    }
   
    /**
     * get category id from session
     *
     * @return string
     */
    public function getSessionCategoryVal()
    {
        return $this->sessionManager->getSelectedCategory();
    }
   
    /**
     * get mapping id from session
     *
     * @param int $mappingId
     * @return int
     */
    public function setSessionMappingVal($mappingId)
    {
        return $this->sessionManager->setSelectedMappingId($mappingId);
    }

    /**
     * get mapping value from session
     *
     * @return void
     */
    public function getSessionMappingVal()
    {
        return $this->sessionManager->getSelectedMappingId();
    }
   
    /**
     * collect all root categories of store
     *
     * @return collection
     */
    public function getRootCategoriesOfStore()
    {
        $level = 2;
        $categoryCollection = $this->_categories->create()
        ->addAttributeToSelect('*')
        ->addFieldToFilter('level', ['eq'=>$level])
        ->setStore($this->_storeManager->getStore());
         // select categories of  level one
        if ($level) {
            $categoryCollection->addLevelFilter($level);
        }
        return $categoryCollection;
    }
   
    /**
     * collect all sub categories of selected cat
     *
     * @param int $parentId
     * @return collection
     */
    public function getSubCategories($parentId)
    {
       
        $rootCategoryCollection = "";
        if ($parentId!='') {
            $rootCategoryCollection = $this->_categories->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('parent_id', ['eq'=>$parentId])
            ->setStore($this->_storeManager->getStore());
        }
        return $rootCategoryCollection;
    }
   
    /**
     * check all mapping of the selected category id
     *
     * @param int $categoryId
     * @return array
     */
    public function checkMappingExists($categoryId)
    {
        $reqParamsArray = [];
        $implodedCategory = "";
        if (!empty($categoryId)) {
            $mappingCollection = $this->_mapping->create()
            ->load($categoryId, 'category_id');
            if ($mappingCollection->getId()) {
                $attributeSetId=$mappingCollection->getAttributeSetId();
                $mappingId=$mappingCollection->getEntityId();
                $reqParamsArray=[
                    'attribute_set_id'=>$attributeSetId,
                    'mapping_id'=>$mappingId
                ];
                return $reqParamsArray;
            } else {
                $parentCategoryArray = $this->getAllParentCategory($categoryId);
                $implodedCategory = implode(',', $parentCategoryArray);
                $mappingCollection = $this->_mapping->create()->getCollection()
                ->addFieldToFilter('category_id', ['in'=>$implodedCategory]);
                if ($mappingCollection->getSize()>0) {
                    foreach ($mappingCollection as $collection) {
                        $attributeSetId=$collection->getAttributeSetId();
                        $reqParamsArray=[
                            'attribute_set_id'=>$attributeSetId,
                            'mapping_id'=>$collection->getEntityId()
                        ];
                        return $reqParamsArray;
                    }
                } else {
                    $attributeSetId=$this->product->getTypeId();
                    $reqParamsArray=[
                        'attribute_set_id'=>$attributeSetId,
                        'mapping_id'=>''
                    ];
                    return $reqParamsArray;
                }
            }
        }
    }
    
    /**
     * check parent mapping of the selected category id
     *
     * @param int $categoryId
     * @return int
     */
    public function getParentCategoryMapping($categoryId)
    {
        if (!empty($categoryId)) {
            try {
                $categoryCollection = $this->_categories->create()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('entity_id', ['eq'=>$categoryId])
                ->setStore($this->_storeManager->getStore());
                if ($categoryCollection->getSize()>0) {
                    foreach ($categoryCollection as $item) {
                        $parentId=$item->getParentId();
                        if ($parentId==0) {
                            $attributeSetId=$this->product->getTypeId();
                            return $attributeSetId;
                        } else {
                            $attributeSetId = $this->checkMappingExists($parentId);
                            return $attributeSetId;
                        }
                    }
                }
            } catch (\Exception $e) {
                return '';
            }
        }
    }
  
    /**
     * check associated attribute of selected attribute set
     *
     * @param int $mappingId
     * @return object
     */
    public function getGroupMapping($mappingId)
    {
        $groupMapping=[];
        if ($mappingId) {
            try {
                $groupMapping= $this->_mappingGroups->create()
                ->getCollection()
                ->addFieldToFilter(
                    'mapping_id',
                    ['eq'=>$mappingId]
                );
                if ($groupMapping->getSize()>0) {
                    return $groupMapping;
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return $groupMapping;
    }
   
    /**
     * return encoded string of categories
     *
     * @return string
     */
    public function getCategoriesTree()
    {
       
         $categories = $this->formCategoryOptions->create()->toOptionArray();
         $categoryEncoded = json_encode($categories);
         return $categoryEncoded;
    }

    /**
     * return array of parent categories
     *
     * @param int $categoryId
     * @return array
     */
    public function getAllParentCategory($categoryId)
    {
        $parentCatArr=[];
        if (!empty($categoryId)) {
            try {
                $category = $this->categoryFactory->create()->load($categoryId);
                if (!empty($category)) {
                    if (\strpos($category->getPath(), '/') !== false) {
                        $parentCatArr = preg_split("#/#", $category->getPath());
                    }
                }
                rsort($parentCatArr);
                return $parentCatArr;
            } catch (\Exception $e) {
                return $parentCatArr;
            }
        }
    }

    /**
     * validate attribute set
     *
     * @param int $setId
     * @param int $mappingId
     * @param text $path
     * @return string
     */
    public function validateAttributeSet($setId, $mappingId, $path)
    {
        if (!empty($mappingId)) {
            $mappingCollection = $this->_mapping->create()
            ->load($mappingId, 'entity_id');
            if ($mappingCollection->getId()) {
                $attributeSetId=$mappingCollection->getAttributeSetId();
                if ($attributeSetId!=$setId) {
                    $this->messageManager->addError(
                        'Invalid Selection of Attribute set'
                    );
                     $this->response->setRedirect($path);
                }
            }
        } else {
            $attributeSetId=$this->product->getTypeId();
            if ($attributeSetId!=$setId) {
                $this->messageManager->addError(
                    'Invalid Selection of Attribute set'
                );
                    $this->response->setRedirect($path);
            }
        }
    }

    /**
     * return config settings in category
     *
     * @return string
     */
    public function getCategoryTreeSetting()
    {
        return  $this->scopeConfig->getValue(
            'attributemapping/category_settings/category',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get category Id from mapping table
     *
     * @param integer $mappingId
     * @return Int
     */
    public function getCategoryFromMapping($mappingId)
    {
        $categoryId="";
        if ($mappingId) {
            $mappingCollection = $this->_mapping->create()
            ->load($mappingId, 'entity_id');
            if ($mappingCollection->getId()) {
                $categoryId=$mappingCollection->getCategoryId();
                return $categoryId;
            }
        }
    }

   /**
    * to show category select in category tree
    *
    * @param int $categoryId
    * @return array
    */
    public function setParentCategoriesInCategoryUi($categoryId)
    {
          $parentCategoryArray=[];
        try {
            if ($this->getCategoryTreeSetting()) {
                $parentCategoryArray=$this->getAllParentCategory($categoryId);
                array_push($parentCategoryArray, $categoryId);
            } else {
                array_push($parentCategoryArray, $categoryId);
            }
        } catch (\Exception $e) {
            return $parentCategoryArray;
        }
        return $parentCategoryArray;
    }
    
    /**
     * get category Id from mapping table
     *
     * @param integer $mappingId
     * @return Int
     */
    public function getAttributeSetFromMapping($mappingId)
    {
        $attributeSetId="";
        try {
            if ($mappingId) {
                $mappingCollection = $this->_mapping->create()
                ->load($mappingId, 'entity_id');
                if ($mappingCollection->getId()) {
                    $attributeSetId = $mappingCollection->getAttributeSetId();
                }
            }
            return $attributeSetId;
        } catch (\Exception $e) {
            return $attributeSetId;
        }
    }
    
    /**
     * get request param
     *
     * @return array
     */
    public function getRequestParam()
    {
        return $this->request->getParams();
    }
}
