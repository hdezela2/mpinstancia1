<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c)Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAttributeMapping\Model\Product\AttributeSet;

class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var null|array
     */
    protected $options;

    /**
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $collectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product $product
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $collectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product $product
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->product = $product;
    }

    /**
     * @return array|null
     */
    public function toOptionArray()
    {
        $result=[];
        $result[0]=[
        'label' => 'Select Attribute Set',
        'value' => ''
        ];
        $count=1;
       
        if (null == $this->options) {
            $this->options = $this->collectionFactory->create()
                 ->setEntityTypeFilter($this->product->getTypeId());
            foreach ($this->options as $data) {
                $result[$count]= [
                    'label' => $data->getAttributeSetName(),
                    'value' => $data->getAttributeSetId()
                ];
                $count++;
            }
        }
        return $result;
    }
}
