<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpAttributeMapping\Model;

use Magento\Framework\Model\AbstractModel;
use Webkul\MpAttributeMapping\Api\Data\GroupMappingInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\App\ResourceConnection ;
use \Magento\Framework\App\Action\Context as Context;

/**
 * MpAttributeMapping MapAttributesToGroup Model.
 */
class MapAttributesToGroups extends AbstractModel implements GroupMappingInterface, IdentityInterface
{

    /**
     * DB Storage table name
     */
    const TABLE_NAME = 'marketplace_attributes_to_group_mapping';
 
    /**
     * Code of "Integrity constraint violation: 1062 Duplicate entry" error
     */
    const ERROR_CODE_DUPLICATE_ENTRY = 23000;
 
    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;
 
    /**
     * @var Resource
     */
    protected $resourceconn;

    /**
     * No route page id.
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**#@+
     * Product's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;
    /**#@-*/

    /**
     * Marketplace Product cache tag.
     */
    const CACHE_TAG = 'marketplace_attributes_to_group_mapping';

    /**
     * @var string
     */
    protected $_cacheTag = 'marketplace_attributes_to_group_mapping';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'marketplace_attributes_to_group_mapping';
    protected $resourceConn;

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Webkul\MpAttributeMapping\Model\ResourceModel\MapAttributesToGroups');
    }

    /**
     * Load object data.
     *
     * @param int|null $id
     * @param string   $field
     *
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteProduct();
        }

        return parent::load($id, $field);
    }

    /**
     * Load No-Route Product.
     *
     * @return \Webkul\MpAttributeMapping\Model\Mapping
     */
    public function noRouteProduct()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Get ID.
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\MpAttributeMapping\Api\Data\GroupMappingInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
    
    /**
     * Insert multiple
     *
     * @param array $data
     * @return void
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Exception
     */
    public function insertMultiple($tableName = self::TABLE_NAME, $data)
    {
        try {
            $tableName = $this->getResource()->getTable(self::TABLE_NAME);
            $this->connection = $this->getResource()->getConnection();
            return $this->connection->insertMultiple($tableName, $data);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
