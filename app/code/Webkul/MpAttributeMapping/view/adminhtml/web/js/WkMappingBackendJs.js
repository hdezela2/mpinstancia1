/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define([
    "jquery",
    "jquery/ui",
    'uiComponent',
    'mage/storage',
    'mage/url',
    'domReady!'
], function ($) {
    "use strict";
    $.widget('attributemapping.WkMappingBackendJs', {
        _create: function () {
            var self = this;
            var formData = this.options;
            var adminBaseUrl=formData.adminBaseUrl;
            $(document).on('click',"[id='tab_map_attributes_to_attrset']",function () {
                var showLoader=true;
                var attributeSetId=requirejs('uiRegistry').get('attributemapping_mapping_mapcategoryattribute.mapping_form_data_source').data.map_category_attributes.attribute_set_id;
                var mappingId=requirejs('uiRegistry').get('attributemapping_mapping_mapcategoryattribute.mapping_form_data_source').data.map_category_attributes.entity_id;
               
                if (typeof(attributeSetId) != "undefined" && typeof(mappingId) != "undefined") {
                    var showLoader=true;
                    self._loadAttributesGroups(attributeSetId,showLoader,mappingId,adminBaseUrl);
                } else {
                    self._loadAttributesGroups(attributeSetId,showLoader,mappingId='',adminBaseUrl);
                }
            });
            $(document).on('change',"[name='map_category_attributes[attribute_set_id]']",function () {
                var attributeSetId=requirejs('uiRegistry').get('attributemapping_mapping_mapcategoryattribute.mapping_form_data_source').data.map_category_attributes.attribute_set_id;
                var showLoader=false;
                var mappingId="";
                self._loadAttributesGroups(attributeSetId,showLoader,mappingId,adminBaseUrl);
            });
        },
     
        _loadAttributesGroups: function (attributeSetId,showLoaderVal,mappingId,adminBaseUrl) {
            var attributeSetId=requirejs('uiRegistry').get('attributemapping_mapping_mapcategoryattribute.mapping_form_data_source').data.map_category_attributes.attribute_set_id;
            if (attributeSetId!="") {
                var setId = 'attribute_set_id='+ attributeSetId+'&mapping_id='+mappingId;
                $.ajax({
                    showLoader: showLoaderVal,
                    url: adminBaseUrl,
                    data: setId,
                    type: "GET",
                    dataType: 'json'
                    }).done(function (data) {
                      $('#attribute_groups').empty();
                       $('#attribute_groups').append(data.value);
                    
                });
            }
        }
    });
    return $.attributemapping.WkMappingBackendJs;
});