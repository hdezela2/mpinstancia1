/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
define([
    "jquery",
    'mage/translate',
    "mage/template",
    "mage/mage",
    "mage/calendar",
], function ($, $t,mageTemplate, alert) {
    'use strict';
    $.widget('mage.WkMappingJs', {

        options: {
            count : 0,
        },
        _create: function () {
            var self = this;
            $(self.options.dateTypeSelector).calendar({ dateFormat:'mm/dd/yy'});
            $("#attribute-set-id").prop('disabled', true);
        },
       
    });
return $.mage.WkMappingJs;
});
