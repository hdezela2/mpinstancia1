/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
var config = {
    map: {
        '*': {
            WkMappingJs: 'Webkul_MpAttributeMapping/js/WkMappingJs',
        }
    },
};
