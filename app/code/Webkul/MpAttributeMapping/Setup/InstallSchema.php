<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c)Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpAttributeMapping\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Install Schema
 */
class InstallSchema implements InstallSchemaInterface
{
   /**
    * install script
    *
    * @param SchemaSetupInterface $setup
    * @param ModuleContextInterface $context
    * @return void
    */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        /*
        * Create table 'marketplace_attributeset_category_mapping'
        *
        */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('marketplace_attributeset_category_mapping'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity ID'
            )
            ->addColumn(
                'attribute_set_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Attribute Set ID'
            )
            ->addColumn(
                'parent_category_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Parent Category ID'
            )
            ->addColumn(
                'category_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Category ID'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' =>  \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Created at'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' =>  \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Updated at'
            )
            ->setComment('Marketplace Attribute Set and Category Mapping Table');
            $installer->getConnection()->createTable($table);
        /*
         * Create table 'marketplace_attributes_to_group_mapping'
         *
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('marketplace_attributes_to_group_mapping'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity ID'
            )
            ->addColumn(
                'mapping_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Attribute Set ID'
            )
            ->addColumn(
                'attribute_group_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Attribute Group ID'
            )
            ->addColumn(
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Attribute ID'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' =>  \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Created at'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' =>  \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Updated at'
            )
            ->setComment('Attribute group and Attributes Mapping Table');
            $installer->getConnection()->createTable($table);
    }
}
