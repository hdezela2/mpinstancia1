<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAttributeMapping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAttributeMapping\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgarde schema
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * upgrade
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        /*
         * Create table 'marketplace_edit_product_mapping'
         */
        $table = $setup->getConnection()
            ->newTable($setup->getTable('marketplace_edit_product_mapping'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity ID'
            )
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Product Id'
            )
            ->addColumn(
                'mapping_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Mapping Id'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' =>  \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Created at'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' =>  \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Updated at'
            )
            ->setComment('Marketplace Product Edit Mapping Table');
            $setup->getConnection()->createTable($table);
    }
}
