<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Customattribute
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Customattribute\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Webkul Customattribute SetTierPriceData Observer.
 */
class SetTierPriceData implements ObserverInterface
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ProductRepositoryInterface $productRepository
    ) {
        $this->_productRepository = $productRepository;
    }

    /**
     * Marketplace Custom Attribute Set Tier Price Observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $wholeData = $observer->getEvent()->getData();
        try {
            foreach ($wholeData as $data) {
                $tierPrices = [];
                if (isset($data['product'])) {
                    if (isset($data['product']['tier_price'])) {
                        $tierPrices = $data['product']['tier_price'];
                    }
                    $sku = $data['product']['sku'];
                    $product = $this->_productRepository->get(
                        $sku,
                        ['edit_mode' => true]
                    );
                    
                    $product->setData('tier_price', $tierPrices);

                    $this->_productRepository->save($product);
                }
            }
        } catch (\Exception $e) {
            error_log("error: ".$e->getMessage());
            throw new \Magento\Framework\Exception\CouldNotSaveException(__('Could not save product tier price'));
        }
    }
}
