<?php
declare(strict_types=1);

namespace Chilecompra\Customer\Model\Validator;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Validator\Name as MagentoName;
use Magento\Framework\Validator\AbstractValidator;

class Name extends MagentoName
{
    private const PATTERN_NAME = '/(?:[\p{L}\p{M}\,\-\_\.\'\s\d\[\]\(\)]){1,255}+/u';

    /**
     * Validate name fields.
     *
     * @param Customer $customer
     * @return bool
     */
    public function isValid($customer)
    {
        if (!$this->isValidName($customer->getFirstname())) {
            AbstractValidator::_addMessages([['firstname' => 'First Name is not valid!']]);
        }

        if (!$this->isValidName($customer->getLastname())) {
            AbstractValidator::_addMessages([['lastname' => 'Last Name is not valid!']]);
        }

        if (!$this->isValidName($customer->getMiddlename())) {
            AbstractValidator::_addMessages([['middlename' => 'Middle Name is not valid!']]);
        }

        return count($this->_messages) == 0;
    }

    /**
     * Check if name field is valid.
     *
     * @param string|null $nameValue
     * @return bool
     */
    private function isValidName($nameValue)
    {
        if ($nameValue != null) {
            if (preg_match(static::PATTERN_NAME, $nameValue, $matches)) {
                return $matches[0] == $nameValue;
            }
        }

        return true;
    }
}
