<?php
/**
 * Chilecompra.
 *
 * @category  Chilecompra
 * @package   WebkulSellerSubAccount
 * @author    Chilecompra
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Chilecompra_WebkulMpVendorAttributeManager',
    __DIR__
);
