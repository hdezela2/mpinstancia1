<?php

namespace Chilecompra\MpSellerBuyerCommunication\Plugin;

use Webkul\MpSellerBuyerCommunication\Helper\Email;

class EmailPlugin
{
    /**
     * @param Email $subject
     * @param mixed $emailTemplateVariables
     * @param mixed $senderInfo
     * @param mixed $receiverInfo
     * @return array
     */
    public function beforeGenerateTemplate(Email $subject, $emailTemplateVariables, $senderInfo, $receiverInfo): array
    {
        if (is_object($receiverInfo["name"])) {
            $receiverInfo["name"] = $receiverInfo["name"]->getText();
        }

        return [$emailTemplateVariables, $senderInfo, $receiverInfo];
    }
}
