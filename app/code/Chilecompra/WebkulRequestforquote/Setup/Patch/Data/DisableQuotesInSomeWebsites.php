<?php

namespace Chilecompra\WebkulRequestforquote\Setup\Patch\Data;

use Intellicore\EmergenciasRenewal\Constants as EmergenciasRenewalConstants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\ResourceModel\Store\CollectionFactory as StoreCollectionFactory;

class DisableQuotesInSomeWebsites implements DataPatchInterface
{
    /** @var \Magento\Store\Model\StoreFactory */
    protected $storeFactory;

    /** @var \Magento\Framework\App\Config\Storage\WriterInterface */
    protected $configWriter;

    /** @var StoreCollectionFactory */
    protected $storeCollection;

    public function __construct(
        StoreFactory $storeFactory,
        WriterInterface $configWriter,
        StoreCollectionFactory $storeCollection
    ) {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
        $this->storeCollection = $storeCollection;
    }

    public static function getDependencies(): array
    {
        return [
            \Intellicore\EmergenciasRenewal\Setup\Patch\Data\EnableQuote::class,
            \Linets\VehiculosSetUp\Setup\Patch\Data\EnableQuote::class
        ];
    }

    /**
     * @return void
     */
    public function apply()
    {
        $storeCollection = $this->storeCollection->create()
            ->addFieldToFilter('code', ['nlike' => 'software%'])
            ->addFieldToFilter('code', ['nlike' => 'admin']);

        $storesToDisableQuotesCodes = [];
        foreach ($storeCollection->getData() as $item) {
            array_push($storesToDisableQuotesCodes, $item['code']);
        }

        foreach ($storesToDisableQuotesCodes as $storeCode) {
            $store = $this->storeFactory->create();
            $store->load($storeCode);
            if ($store->getId()) {
                $websiteId = $store->getWebsiteId();
                $scope = ScopeInterface::SCOPE_WEBSITES;
                $this->configWriter->save(
                    EmergenciasRenewalConstants::XML_PATH_REQUEST_FOR_QUOTE_ACCOUNT,
                    0,
                    $scope,
                    $websiteId
                );
            }
        }
    }

    public function getAliases(): array
    {
        return [];
    }
}
