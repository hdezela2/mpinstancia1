<?php
namespace Chilecompra\WebkulRequestforquote\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '3.0.2', '<')) {
            $setup->getConnection()->changeColumn(
                $setup->getTable('requestforquote_quote_conversation'),
                'quote_price',
                'quote_price',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => false,
                    'default' => '0.0000',
                    'comment' => 'quote_price'
                ]
            );
            $setup->getConnection()->changeColumn(
                $setup->getTable('requestforquote_quote_conversation'),
                'quote_shipping_price',
                'quote_shipping_price',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => false,
                    'default' => '0.0000',
                    'comment' => 'quote_shipping_price'
                ]
            );
        }
        $setup->endSetup();
    }
}
