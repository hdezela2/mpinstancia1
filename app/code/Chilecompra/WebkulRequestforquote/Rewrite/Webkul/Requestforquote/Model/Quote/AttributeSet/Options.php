<?php
declare(strict_types=1);

namespace Chilecompra\WebkulRequestforquote\Rewrite\Webkul\Requestforquote\Model\Quote\AttributeSet;

class Options extends \Webkul\Requestforquote\Model\Quote\AttributeSet\Options
{
    const DISPERSION_DISABLED = 2;
    const PRICE_DISABLED = 3;
    const NO_TRADED_PRODUCT_DISABLED = 4;
    const STOCK_DISABLED = 5;
    const STATUS_COMPLETE = 6; //is 2 in 3rd party original module
}
