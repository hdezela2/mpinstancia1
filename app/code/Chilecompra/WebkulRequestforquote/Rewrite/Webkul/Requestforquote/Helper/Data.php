<?php
declare(strict_types=1);

namespace Chilecompra\WebkulRequestforquote\Rewrite\Webkul\Requestforquote\Helper;

class Data extends \Webkul\Requestforquote\Helper\Data
{
    /**
     * Clean Cache
     */
    public function clearCache()
    {
        return $this;
    }
}