<?php

namespace Chilecompra\Catalog\Helper;

use Magento\Customer\Model\Context as ContextAuth;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Http\Context as HTTPContext;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    //customer has logged in
    const XML_PATH_URL_CONVENIO_MARCO = 'elasticsearch_storeswitch/general/url_cm';

    // Customer is not logged in
    const XML_PATH_URL_CONVENIO_MARCO_NOT_LOGGED_IN = 'elasticsearch_storeswitch/general/url_cm_not_logged_in';

    /** @var HTTPContext  */
    protected HTTPContext $httpContext;

    /**
     * @param HTTPContext $httpContext
     * @param Context     $context
     */
    public function __construct(
        HTTPContext $httpContext,
        Context     $context

    )
    {
        $this->httpContext = $httpContext;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getUrlConvenioMarco()
    {
        if ((bool)$this->httpContext->getValue(ContextAuth::CONTEXT_AUTH)) {
            return $this->scopeConfig->getValue(self::XML_PATH_URL_CONVENIO_MARCO, ScopeInterface::SCOPE_WEBSITE);
        } else {
            return $this->scopeConfig->getValue(self::XML_PATH_URL_CONVENIO_MARCO_NOT_LOGGED_IN,
                ScopeInterface::SCOPE_WEBSITE);
        }
    }
}
