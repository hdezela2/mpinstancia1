<?php
/**
 * Chilecompra.
 *
 * @category  Chilecompra
 * @package   Chilecompra_Catalog
 * @author    Chilecompra
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Chilecompra_Catalog',
    __DIR__
);