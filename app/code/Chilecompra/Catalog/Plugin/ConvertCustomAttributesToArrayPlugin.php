<?php
declare(strict_types=1);

namespace Chilecompra\Catalog\Plugin;

use WeltPixel\OwlCarouselSlider\Block\Product\ImageFactory as WeltPixelImageFactory;
use Magento\Catalog\Block\Product\Image as ImageBlock;
use Magento\Catalog\Model\Product;

/**
 * @see WeltPixelImageFactory
 */
class ConvertCustomAttributesToArrayPlugin
{
    /**
     * There's a custom module WeltPixel\OwlCarouselSlider\Block\Product\ImageFactory
     * overriding Magento\Catalog\Block\Product\ImageFactory and it does a conversion
     * of the custom_attributes to String which causes an Invalid argument supplied for
     * foreach() issue on vendor/magento/module-catalog/view/frontend/templates/product/image_with_borders.phtml
     * We are basically converting it into an array again as originally designed by the Core team through this plugin
     */
    public function afterCreate(
        WeltPixelImageFactory $subject,
        ImageBlock $result,
        Product $product,
        string $imageId,
        array $attributes = null
    ): ImageBlock
    {
        if (!is_array($result->getCustomAttributes())) {
            $result->setData('custom_attributes', [$result->getCustomAttributes()]);
        }

        return $result;
    }
}
