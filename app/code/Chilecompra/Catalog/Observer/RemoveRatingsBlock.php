<?php

namespace Chilecompra\Catalog\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Layout;
use Magento\Store\Model\ScopeInterface;

class RemoveRatingsBlock implements ObserverInterface
{
    /*
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct
    (
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function execute(Observer $observer)
    {
        /** @var Layout $layout */
        $layout = $observer->getLayout();
        $block = $layout->getBlock('product.info.review');  // here block reference name to remove

        if ($block) {
            $showBlock = $this->scopeConfig->getValue('catalog/review/active', ScopeInterface::SCOPE_STORE);
            if (!$showBlock) {
                $layout->unsetElement('product.info.review');
            }
        }
    }
}