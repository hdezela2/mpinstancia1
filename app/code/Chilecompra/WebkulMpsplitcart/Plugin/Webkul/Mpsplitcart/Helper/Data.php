<?php
namespace Chilecompra\WebkulMpsplitcart\Plugin\Webkul\Mpsplitcart\Helper;
class Data
{
    public function afterValidateVirtualCart(
        \Webkul\Mpsplitcart\Helper\Data $subject,
        $result,
        $virtualCart
    ) {
        if (is_array($result)) {
            $response = [];
            foreach ($result as $sellerId => $productArray) {
                $tmpItems = [];
                $tmpItems[$sellerId] = [];
                $tmp = [];
                foreach ($productArray as $productId => $itemInfo) {
                    if (isset($itemInfo['mpassignproduct_id'])) {
                        if (array_key_exists($itemInfo['mpassignproduct_id'], $tmpItems[$sellerId])) {
                            if ($tmpItems[$sellerId][$itemInfo['mpassignproduct_id']] < $productId) {
                                $tmpItems[$sellerId][$itemInfo['mpassignproduct_id']] = $productId;
                                $tmp = [$productId => $itemInfo];
                            }
                        } else {
                            $tmpItems[$sellerId][$itemInfo['mpassignproduct_id']] = $productId;
                            $tmp = [$productId => $itemInfo];
                        }
                    }
                }
                $response[$sellerId] = $tmp;
            }
            $result = $response;
        }
        return $result;
    }
}
