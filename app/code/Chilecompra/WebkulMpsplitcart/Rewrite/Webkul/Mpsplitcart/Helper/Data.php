<?php
namespace Chilecompra\WebkulMpsplitcart\Rewrite\Webkul\Mpsplitcart\Helper;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;

/**
 * Mpsplitcart data helper.
 */
class Data extends \Webkul\Mpsplitcart\Helper\Data
{
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cart;

    /**
     * @var Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerDataFactory
     * @param \Magento\Customer\Model\Customer\Mapper $customerMapper
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Webkul\Marketplace\Model\Product $mpModel
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Webkul\Mpsplitcart\Cookie\Guestcart $guestCart
     * @param \Webkul\Mpsplitcart\Logger\Logger $logger
     * @param \Magento\Framework\Event\Manager $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerDataFactory,
        \Magento\Customer\Model\Customer\Mapper $customerMapper,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Webkul\Marketplace\Model\Product $mpModel,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Webkul\Mpsplitcart\Cookie\Guestcart $guestCart,
        \Webkul\Mpsplitcart\Logger\Logger $logger,
        \Magento\Framework\Event\Manager $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct(
            $context,
            $objectManager,
            $customerSession,
            $cart,
            $customerRepository,
            $customerDataFactory,
            $customerMapper,
            $dataObjectHelper,
            $checkoutSession,
            $productRepository,
            $mpModel,
            $mpHelper,
            $guestCart,
            $logger,
            $eventManager
        );
        $this->cart = $cart;
        $this->storeManager = $storeManager;
    }
    /**
     * addVirtualCartToQuote used to add products in cart from virtual cart
     *
     * @return void
     */
    public function addVirtualCartToQuote()
    {
        try {
            $quote       = $this->cart->getQuote();
            $virtualCart = $this->getVirtualCart();
            $oldVirtualCart = $virtualCart;
            $itemIds = [];
            $proIds  = [];
            foreach ($quote->getAllVisibleItems() as $item) {
                if (!$item->hasParentItemId()) {
                    $itemIds[$item->getId()] = $item->getProductId();
                    $options = $item->getBuyRequest()->getData();
                    //checks for seller assign product
                    if (array_key_exists('mpassignproduct_id', $options)) {
                        $proIds[$item->getProductId()] = $options['mpassignproduct_id'];
                    }
                }
            }
            if ($virtualCart
                && is_array($virtualCart)
                && $virtualCart !== ''
                && $this->checkMpsplitcartStatus()
            ) {
                $addCart = $this->prepareDataForCart($virtualCart, $itemIds, $proIds);
                if ($addCart ||
                    $this->storeManager->getWebsite()->getCode() == \Formax\SoftwareSetUp\Helper\Data::WEBSITE_CODE || $this->storeManager->getWebsite()->getCode() == SoftwareRenewalConstants::WEBSITE_CODE
                ) {
                    $this->saveCart();
                }
            }
            $this->checkMpQuoteSystem($oldVirtualCart);
            $this->unsetCheckoutRemoveSession();
            $this->updateCart();
        } catch (\Exception $e) {
            $this->logDataInLogger(
                "Helper_Data_addVirtualCartToQuote Exception : ".$e->getMessage()
            );
        }
    }
}
