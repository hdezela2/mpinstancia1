<?php
/**
 * Chilecompra.
 *
 * @category  Chilecompra
 * @package   WebkulMpsplitcart
 * @author    Chilecompra
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Chilecompra_WebkulMpsplitcart',
    __DIR__
);
