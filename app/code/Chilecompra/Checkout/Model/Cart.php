<?php

namespace Chilecompra\Checkout\Model;

use Magento\Catalog\Model\Product;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

/**
 * This class override was present in the Webkul_MpAssignProduct module
 * prior to the upgrade to 2.4.3. Even though this override
 * has been removed, it is still required.
 * That said, we are adding it back.
 *
 * See https://chilecompra.atlassian.net/browse/MU24-51
 */
class Cart extends \Magento\Checkout\Model\Cart
{
    /**
     * Add product to shopping cart (quote)
     *
     * @param int|Product $productInfo
     * @param DataObject|int|array $requestInfo
     * @return \Magento\Checkout\Model\Cart
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function addProduct($productInfo, $requestInfo = null): \Magento\Checkout\Model\Cart
    {
        $product = $this->_getProduct($productInfo);
        $productId = $product->getId();

        if ($productId) {
            $request = $this->getQtyRequest($product, $requestInfo);
            try {
                $this->_eventManager->dispatch(
                    'checkout_cart_product_add_before',
                    ['info' => $requestInfo, 'product' => $product]
                );
                $result = $this->getQuote()->addProduct($product, $request);
            } catch (LocalizedException $e) {
                $this->_checkoutSession->setUseNotice(false);
                $result = $e->getMessage();
            }
            /**
             * String we can get if prepare process has error
             */
            if (is_string($result)) {
                if ($product->hasOptionsValidationFail()) {
                    $redirectUrl = $product->getUrlModel()->getUrl(
                        $product,
                        ['_query' => ['startcustomization' => 1]]
                    );
                } else {
                    $redirectUrl = $product->getProductUrl();
                }
                $this->_checkoutSession->setRedirectUrl($redirectUrl);
                if ($this->_checkoutSession->getUseNotice() === null) {
                    $this->_checkoutSession->setUseNotice(true);
                }
                throw new LocalizedException(__($result));
            }
        } else {
            throw new LocalizedException(__('The product does not exist.'));
        }

        $this->_eventManager->dispatch(
            'checkout_cart_product_add_after',
            ['quote_item' => $result, 'product' => $product, 'info' => $requestInfo]
        );
        $this->_checkoutSession->setLastAddedProductId($productId);
        return $this;
    }

    /**
     * Get request quantity
     *
     * @param Product $product
     * @param DataObject|int|array $request
     * @return DataObject
     * @throws LocalizedException
     */
    private function getQtyRequest(Product $product, $request = 0): DataObject
    {
        $request = $this->_getProductRequest($request);
        $stockItem = $this->stockRegistry->getStockItem($product->getId(), $product->getStore()->getWebsiteId());
        $minimumQty = $stockItem->getMinSaleQty();
        //If product quantity is not specified in request and there is set minimal qty for it
        if ($minimumQty
            && $minimumQty > 0
            && !$request->getQty()
        ) {
            $request->setQty($minimumQty);
        }

        return $request;
    }
}
