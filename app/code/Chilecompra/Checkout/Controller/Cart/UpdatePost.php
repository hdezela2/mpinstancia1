<?php

namespace Chilecompra\Checkout\Controller\Cart;


class UpdatePost extends \Magento\Checkout\Controller\Cart\UpdatePost
{
    /**
     * Empty customer's shopping cart
     *
     * @return void
     */
    protected function _emptyShoppingCart()
    {
        try {
            $cartData = $this->getRequest()->getParam('cart');
            foreach($cartData as $itemId => $data) {
                $this->cart->removeItem($itemId);
            }
            $this->cart->getQuote()->setTotalsCollectedFlag(false);
            $this->cart->save();
        } catch (\Exception $exception) {
            $this->messageManager->addExceptionMessage($exception, __('We can\'t update the shopping cart.'));
        }
    }
}
