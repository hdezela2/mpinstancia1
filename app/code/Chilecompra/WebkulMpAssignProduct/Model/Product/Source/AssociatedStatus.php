<?php
declare(strict_types=1);

namespace Chilecompra\WebkulMpAssignProduct\Model\Product\Source;

use Magento\Framework\Data\OptionSourceInterface;

class AssociatedStatus implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        $availableOptions = [
            '%disapproved%' => __('Disapproved'),
            '%approved%' => __('Approved'),
            '%pending%' => __('Pending'),
            '%disabled_by_dispersion%' => __('Disabled by dispersion'),
            '%disabled_by_price%'  => __('Disabled by price'),
            '%disabled_by_no_traded_product%' => __('Disabled by No traded Product'),
            '%disabled_by_empty_stock%'  => __('Disabled by Empty Stock'),
            '%disabled_by_penalty%' => __('Disabled by penalty')
        ];
        $options = [];

        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }

        return $options;
    }
}
