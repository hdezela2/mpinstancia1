<?php

namespace Chilecompra\WebkulMpAssignProduct\Plugin\Webkul\MpAssignProduct\Model\ResourceModel\Items;

use Magento\Store\Model\Store;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\Collection as Subject;

class Collection
{
    /**
     * The original Webkul\MpAssignProduct\Model\ResourceModel\Items\Collection::addStoreFilter
     * calls an undefined method, so we are making sure it doesn't call the undefined method.
     * We don't want to call the original method and that means we are not calling proceed
     *
     * @param Subject $subject
     * @param callable $proceed
     * @param array|int|Store $store
     * @param bool $withAdmin
     * @return Collection
     */
    public function aroundAddStoreFilter(
        Subject $subject,
        callable $proceed,
                   $store,
        bool $withAdmin = true
    ): Collection {
        return $this;
    }
}
