<?php
namespace Chilecompra\WebkulMpAssignProduct\Plugin\Webkul\MpAssignProduct\Block\Product;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Products
{
    /**
     * @param \Webkul\MpAssignProduct\Block\Product\Products $subject
     * @param array $result
     * @param object $product
     * @return array
     */
    public function afterGetFormatedArray(
        \Webkul\MpAssignProduct\Block\Product\Products $subject,
        array $result,
        object $product
    ) {
        $result['base_price'] = $product->getBasePrice();
        $result['shipping_price'] = $product->getShippingPrice();
        $result['assembly_price'] = $product->getAssemblyPrice();
        if ($product->getType() == "configurable") {
            //configurable-fix: change in default value button addToCart
            $displyAddToCart = true;
            $result['displyAddToCart'] = $displyAddToCart;
        }
        return $result;
    }
}
