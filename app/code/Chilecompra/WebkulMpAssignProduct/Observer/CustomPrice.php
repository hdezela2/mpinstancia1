<?php
namespace Chilecompra\WebkulMpAssignProduct\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

/**
 * This observer was present in the Webkul_MpAssignProduct module
 * prior to the upgrade to 2.4.3. Even though this observer
 * has been removed, it is still required.
 * That said, we are adding it back.
 *
 * See https://chilecompra.atlassian.net/browse/MU24-51
 */
class CustomPrice implements ObserverInterface
{
    /**
     * Request instance.
     *
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $_assignHelper;

    /**
     * @var \Magento\Checkout\Model\CartFactory
     */
    protected $_cart;

    /**
     * @param RequestInterface $request
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     * @param \Magento\Checkout\Model\CartFactory $cartFactory
     */
    public function __construct(
        RequestInterface $request,
        \Webkul\MpAssignProduct\Helper\Data $helper,
        \Magento\Checkout\Model\CartFactory $cartFactory
    ) {
        $this->_request = $request;
        $this->_assignHelper = $helper;
        $this->_cart = $cartFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $helper = $this->_assignHelper;
        $item = $observer->getEvent()->getData('quote_item');
        $info = $observer->getEvent()->getData('info');
        if (is_object($info)) {
            $info = $info->getData();
        }
        $data = $this->_request->getParams();
        $product = $observer->getEvent()->getData('product');
        $assignId = 0;
        $childAssignId = 0;
        $qty = 1;
        $productId = $product->getId();
        if (is_array($info)) {
            if (array_key_exists("mpassignproduct_id", $info)) {
                $assignId = $info['mpassignproduct_id'];
            }
            if (array_key_exists("associate_id", $info)) {
                $childAssignId = $info['associate_id'];
            }
            if (array_key_exists("qty", $info)) {
                $qty = $info['qty'];
            }
        }
        if ($product->getTypeId() == "configurable") {
            if (!$helper->isConfigQtyAllowed($info, $product)) {
                $error = __('Requested quantity not available from seller.');
                throw new \Magento\Framework\Exception\LocalizedException($error);
            }
        } else {
            if (!$helper->isQtyAllowed($qty, $productId, $assignId, $childAssignId)) {
                $error = __('Requested quantity not available from seller.');
                throw new \Magento\Framework\Exception\LocalizedException($error);
            }
        }

        $itemId = (int) $item->getId();
        if ($assignId > 0) {
            if ($helper->isEnabled($assignId)) {
                if ($childAssignId > 0) {
                    $childAssignId = $childAssignId;
                    $price = $helper->getAssocitePrice($assignId, $childAssignId);
                } else {
                    $price = $helper->getAssignProductPrice($assignId);
                }
                $price = $helper->getFinalPrice($price);
                $item->setCustomPrice($price);
                $item->setOriginalCustomPrice($price);
                $item->getProduct()->setIsSuperMode(true);
            } else {
                $error = __('Product is currently not available from seller.');
                throw new \Magento\Framework\Exception\LocalizedException($error);
            }
        }
    }
}
