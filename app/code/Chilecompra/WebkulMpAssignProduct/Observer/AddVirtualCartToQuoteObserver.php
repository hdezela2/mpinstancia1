<?php
namespace Chilecompra\WebkulMpAssignProduct\Observer;

use Magento\Framework\Event\ObserverInterface;

class AddVirtualCartToQuoteObserver implements ObserverInterface
{
    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $_assignHelper;

    /**
     * @var \Webkul\MpAssignProduct\Model\QuoteFactory
     */
    protected $_quote;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     * @param \Webkul\MpAssignProduct\Model\QuoteFactory $quoteFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Webkul\MpAssignProduct\Helper\Data $helper,
        \Webkul\MpAssignProduct\Model\QuoteFactory $quoteFactory,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_assignHelper = $helper;
        $this->_quote = $quoteFactory;
        $this->checkoutSession = $checkoutSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $status = $observer->getEvent()->getData('status');
        if($status) {
          $helper = $this->_assignHelper;
          $quote = $this->checkoutSession->getQuote();
          $quoteId = $quote->getId();
          $helper = $this->_assignHelper;
          foreach ($quote->getAllVisibleItems() as $quoteItem) {
            $infoBuyRequest = $quoteItem->getBuyRequest()->getData();
            $assignId = 0;
            if (isset($infoBuyRequest['mpassignproduct_id'])) {
                $assignId = $infoBuyRequest['mpassignproduct_id'];
            }
            $childAssignId = 0;
            if (isset($infoBuyRequest['associate_id'])) {
                $childAssignId = $infoBuyRequest['associate_id'];
            }
            $productId = $quoteItem->getProductId();
            $ownerId = $helper->getSellerIdByProductId($productId);
            $flag = 0;
            if ($assignId > 0) {
                $sellerId = $helper->getAssignSellerIdByAssignId($assignId);
            } else {
                $sellerId = $ownerId;
            }
            $itemId = $quoteItem->getId();
            $qty = $quoteItem->getQty();
            if ($helper->isNewProduct($productId, $assignId) && $itemId > 0) {
                $quoteModel = $this->_quote->create()->getCollection()
                              ->addFieldToFilter('item_id', $itemId)
                              ->addFieldToFilter('seller_id', $sellerId)
                              ->addFieldToFilter('owner_id', $ownerId)
                              ->addFieldToFilter('assign_id', $assignId);
                if($quoteModel->getSize()) {
                  foreach ($quoteModel as $model) {
                    $model->setQty($qty)->save();
                  }
                } else {
                  $model = $this->_quote->create();
                  $quoteData = [
                                  'item_id' => $itemId,
                                  'seller_id' => $sellerId,
                                  'owner_id' => $ownerId,
                                  'qty' => $qty,
                                  'product_id' => $productId,
                                  'assign_id' => $assignId,
                                  'child_assign_id' => $childAssignId,
                                  'quote_id' => $quoteId,
                              ];
                  $model->setData($quoteData)->save();
                }
            }
          }
        }
    }
}
