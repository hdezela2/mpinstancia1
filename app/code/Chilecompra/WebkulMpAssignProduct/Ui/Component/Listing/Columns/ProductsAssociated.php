<?php
declare(strict_types=1);

namespace Chilecompra\WebkulMpAssignProduct\Ui\Component\Listing\Columns;

use Formax\AssignProduct\Helper\MpAssignProduct;
use Linets\GasSetup\Constants;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Type;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ProductConfigurable;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Listing\Columns\Column;
use Webkul\MpAssignProduct\Helper\Data;
use Magento\Framework\App\ResourceConnection;

class ProductsAssociated extends Column
{
    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var MpAssignProduct
     */
    private $helperData;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * @var Data
     */
    private $helperWkData;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * Constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param ProductRepositoryInterface $productRepository
     * @param MpAssignProduct $helperData
     * @param Data $helperWkData
     * @param StoreManagerInterface $storeManager
     * @param StoreRepositoryInterface $storeRepository
     * @param ResourceConnection $resourceConnection
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        ProductRepositoryInterface $productRepository,
        MpAssignProduct $helperData,
        Data $helperWkData,
        StoreManagerInterface $storeManager,
        StoreRepositoryInterface $storeRepository,
        ResourceConnection $resourceConnection,
        array $components = [],
        array $data = []
    ) {
        $this->_urlBuilder = $urlBuilder;
        $this->helperData = $helperData;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->storeRepository = $storeRepository;
        $this->helperWkData = $helperWkData;
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');

            foreach ($dataSource['data']['items'] as &$item) {
                if ($item['type'] === Type::DEFAULT_TYPE) {
                    $item[$fieldName] = 'Producto Simple';
                } else {

                    $tmpItem = $this->getProductStatus($item['id']);
                    $associatesCount = $tmpItem['products_count'];

                    if ($associatesCount == 0) {
                        $item[$fieldName] = __('Sin hijos asociados');
                        continue;
                    }
                    $labels = [];
                    $finalString = "<p>".$associatesCount." Productos Hijos</p><br>";

                    if (count($tmpItem['statuses']) > 0) {
                        foreach ($tmpItem['statuses'] as $key => $value) {
                            if ($value['label'] == 'Deshabilitado') {
                                $value['label'] = 'Desaprobado';
                            }
                            $labels[] = $value['label'].' '.__((ucfirst(str_replace('_', ' ', $value['count']))));
                        }
                        if (count($labels) > 0) {
                            $finalString .= "<p>Estados:<br> ".implode('</p><p>', $labels)."</p>";
                        }
                    }

                    $item[$fieldName] = $finalString;
                }
            }
        }
        return $dataSource;
    }

    /**
     * @param int $itemId
     * @param int|false $status
     * @return int
     */
    private function getProductCount($itemId, $status = false)
    {
        $connection = $this->resourceConnection->getConnection();
        $table = $connection->getTableName('marketplace_assignproduct_associated_products');
        $query = "SELECT COUNT(DISTINCT product_id) FROM " . $table . ' AS asso WHERE '. $itemId .' = asso.parent_id';
        if ($status !== false) {
            $query .= ' AND asso.status = ' . $status;
        }
        $associatesCountResult = $connection->fetchAll($query);
        $associatesCount = $this->extractProductCount($associatesCountResult);

        return $associatesCount;
    }

    /**
     * @param array $associatesCountResult
     * @return null|int
     */
    private function extractProductCount($associatesCountResult)
    {
        $associatesCount = null;
        foreach ($associatesCountResult as $queryResults) {
            if (is_array($queryResults)) {
                foreach ($queryResults as $queryExecuted => $queryResult) {
                    $associatesCount = (int) $queryResult;
                }
            }
        }

        return $associatesCount;
    }

    /**
     * @param int $itemId
     * @return array
     */
    private function getAllPossibleStatus($itemId)
    {
        $connection = $this->resourceConnection->getConnection();
        $table = $connection->getTableName('marketplace_assignproduct_associated_products');
        $query = "SELECT DISTINCT `status` FROM " . $table . ' AS asso WHERE '. $itemId .' = asso.parent_id';
        $possibleStatusResponse = $connection->fetchAll($query);
        $possibleStatus = $this->extractStatus($possibleStatusResponse);

        return $possibleStatus;
    }

    /**
     *
     * @param array $possibleStatus
     * @return array
     */
    private function extractStatus($possibleStatus)
    {
        $status = [];
        foreach ($possibleStatus as $value) {
            if (is_array($value) && isset($value['status'])) {
                $status[] = $value['status'];
            }
        }

        return $status;
    }

    /**
     * @param int $statusId
     * @return string
     */
    private function getStatusLabel($statusId)
    {
        $connection = $this->resourceConnection->getConnection();
        $table = $connection->getTableName('dccp_special_offer_status');
        $query = "SELECT `value` FROM `dccp_special_offer_status` WHERE `id_status` = ". $statusId;
        $statusLabelResponse = $connection->fetchAll($query);
        $statusLabel = $this->extractLabel($statusLabelResponse);

        return $statusLabel;
    }

    /**
     * @param int $itemId
     * @return array
     */
    private function getProductStatus($itemId)
    {
        $possibleStatus = $this->getAllPossibleStatus($itemId);
        $productCount = $this->getProductCount($itemId);
        $statusArray = [];
        foreach ($possibleStatus as $status) {
            $statusLabel = $this->getStatusLabel($status);
            $statusCount = $this->getProductCount($itemId, $status);
            $statusArray[$status] = [
                'count' => $statusCount,
                'label' => $statusLabel
            ];
        }
        $associates = [
            'products_count' => $productCount,
            'statuses' => $statusArray
        ];

        return $associates;
    }

    /**
     *
     * @param array $labelResponse
     * @return string
     */
    private function extractLabel($labelResponse)
    {
        $statusLabel = '';
        foreach ($labelResponse as $value) {
            if (is_array($value) && isset($value['value'])) {
                $statusLabel = $value['value'];
            }
        }

        return $statusLabel;
    }
}
