<?php

namespace Chilecompra\WebkulMpAssignProduct\Rewrite\Webkul\MpAssignProduct\Block\Adminhtml\Product\Edit;

use Exception;

class Tabs extends \Webkul\MpAssignProduct\Block\Adminhtml\Product\Edit\Tabs
{
    /**
     * Prepare Layout
     *
     * @throws Exception
     */
    protected function _prepareLayout()
    {
        $block = 'Webkul\MpAssignProduct\Block\Adminhtml\Product\Edit\Tab\Details';
        $url = $this->getUrl('*/*/conversation', ['_current' => true]);
        $this->addTab(
            'product_info',
            [
                'label' => __('Product Information'),
                'content' => $this->getLayout()
                    ->createBlock($block, 'product_info')
                    ->setTemplate("product.phtml")
                    ->toHtml(),
            ]
        );

        return \Magento\Backend\Block\Widget\Tabs::_prepareLayout();
    }
}
