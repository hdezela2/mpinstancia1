<?php

namespace Chilecompra\WebkulMpAssignProduct\Rewrite\Webkul\MpAssignProduct\Controller\Order;

use Magento\Checkout\Model\CartFactory;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action;
use Magento\Framework\Registry;
use Magento\Sales\Controller\AbstractController\OrderLoaderInterface;
use Webkul\MpAssignProduct\Helper\Data;
use Webkul\MpAssignProduct\Model\QuoteFactory;


class Reorder extends \Webkul\MpAssignProduct\Controller\Order\Reorder
{
    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var QuoteFactory
     */
    protected $_quote;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @param Action\Context $context
     * @param OrderLoaderInterface $orderLoader
     * @param Registry $registry
     * @param CartFactory $cartFactory
     * @param Session $session
     * @param Data $helper
     * @param QuoteFactory $quoteFactory
     * @param Session $checkoutSession
     */
    public function __construct(
        Action\Context       $context,
        OrderLoaderInterface $orderLoader,
        Registry             $registry,
        CartFactory          $cartFactory,
        Session              $session,
        Data                 $helper,
        QuoteFactory         $quoteFactory,
        Session              $checkoutSession
    ) {
        parent::__construct(
            $context,
            $orderLoader,
            $registry,
            $cartFactory,
            $session
        );
        $this->_helper = $helper;
        $this->_quote = $quoteFactory;
        $this->checkoutSession = $checkoutSession;
    }

    public function processItem($item, $cart)
    {
        if ($item->getProductType() === 'configurable') {
            $info = $item->getProductOptionByCode('info_buyRequest');

            $cart->addOrderItem($item);
            $cart->save();
            $this->_eventManager->dispatch(
                'checkout_cart_add_product_complete',
                [
                    'product' => $item->getProductId(),
                    'info' => $info,
                    'request' => $this->getRequest(),
                    'response' => $this->getResponse()
                ]
            );
        } else {
            $helper = $this->_helper;
            $quote = $this->checkoutSession->getQuote();
            $info = $item->getProductOptionByCode('info_buyRequest');
            $flag = false;

            $assignIdOrder = 0;
            if (array_key_exists('mpassignproduct_id', $info)) {
                $assignIdOrder = $info['mpassignproduct_id'];
            }

            $qtyOrder = 0;
            if (array_key_exists('qty', $info)) {
                $qtyOrder = $info['qty'];
            }

            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                $infoBuyRequest = $quoteItem->getBuyRequest()->getData();

                $assignId = 0;
                if (isset($infoBuyRequest['mpassignproduct_id'])) {
                    $assignId = $infoBuyRequest['mpassignproduct_id'];
                }

                $childAssignId = 0;
                if (isset($infoBuyRequest['associate_id'])) {
                    $childAssignId = $infoBuyRequest['associate_id'];
                }

                $productId = $quoteItem->getProductId();
                $ownerId = $helper->getSellerIdByProductId($productId);
                $itemId = $quoteItem->getId();
                $qty = $quoteItem->getQty() + $qtyOrder;

                if ($assignId > 0) {
                    $sellerId = $helper->getAssignSellerIdByAssignId($assignId);
                } else {
                    $sellerId = $ownerId;
                }

                if($assignId == $assignIdOrder){
                    $flag = !$flag;
                    $quoteModel = $this->_quote->create()->getCollection()
                        ->addFieldToFilter('item_id', $itemId)
                        ->addFieldToFilter('seller_id', $sellerId)
                        ->addFieldToFilter('owner_id', $ownerId)
                        ->addFieldToFilter('assign_id', $assignId);
                    if($quoteModel->getSize()) {
                        foreach ($quoteModel as $model) {
                            $model->setQty($qty)->save();
                            $quoteItem->setQty($qty);
                        }
                    }
                }

            }

            if(!$flag){
                $cart->addOrderItem($item);
                $cart->save();
                $this->_eventManager->dispatch(
                    'checkout_cart_add_product_complete',
                    [
                        'product' => $item->getProductId(),
                        'info' => $info,
                        'request' => $this->getRequest(),
                        'response' => $this->getResponse()
                    ]
                );
            }else{
                $cart->save();
            }

        }
    }
}
