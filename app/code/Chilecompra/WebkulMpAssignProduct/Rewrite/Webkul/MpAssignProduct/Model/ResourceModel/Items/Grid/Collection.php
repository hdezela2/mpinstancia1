<?php

namespace Chilecompra\WebkulMpAssignProduct\Rewrite\Webkul\MpAssignProduct\Model\ResourceModel\Items\Grid;

class Collection extends \Webkul\MpAssignProduct\Model\ResourceModel\Items\Grid\Collection
{
    /**
     * Join store relation table if there is store filter.
     */
    protected function _renderFiltersBefore()
    {
        /* PHPSTAN - attributeFactory property dynamically declared in 3rd party module*/
        $eavAttribute = $this->attributeFactory->create(); /** @phpstan-ignore-line */
        $productAttributeId = $eavAttribute->getIdByCode('catalog_product', 'name');
        $proPriceAttId = $eavAttribute->getIdByCode('catalog_product', 'price');
        $proWeightAttId = $eavAttribute->getIdByCode('catalog_product', 'weight');

        $customerGridFlat = $this->getTable('customer_grid_flat');
        $catalogProductEntityVarchar = $this->getTable('catalog_product_entity_varchar');
        $catalogProductEntityDecimal = $this->getTable('catalog_product_entity_decimal');
        $cataloginventoryStockItem = $this->getTable('cataloginventory_stock_item');

        $sql = $customerGridFlat.' as cgf';
        $cond = 'main_table.seller_id = cgf.entity_id';
        $fields = ['name' => 'name'];
        $this->getSelect()
            ->join($sql, $cond, $fields);
        $this->addFilterToMap('name', 'cgf.name');

        $sql = $catalogProductEntityVarchar.' as cpev';
        $cond = 'main_table.product_row_id = cpev.row_id';
        $fields = ['product_name' => 'value'];
        $this->getSelect()
            ->join($sql, $cond, $fields)
            ->where('cpev.store_id = 0 AND cpev.attribute_id = '.$productAttributeId);
        $this->addFilterToMap('product_name', 'cpev.value');

        $sql = $catalogProductEntityDecimal.' as cped';
        $cond = 'main_table.product_id = cped.row_id';
        $fields = ['product_price' => 'value'];
        $this->getSelect()
            ->join($sql, $cond, $fields)
            ->where('cped.store_id = 0 AND (cped.attribute_id =
            '.$proPriceAttId.' OR cped.attribute_id ='.$proWeightAttId.')');
        $this->getSelect()->join(
            $cataloginventoryStockItem.' as csi',
            'main_table.assign_product_id = csi.product_id',
            ["product_qty" => "qty"]
        )->where("csi.website_id = 0 OR csi.website_id = 1");
        $this->addFilterToMap("product_qty", "csi.qty");
        $this->addFilterToMap('product_price', 'cped.value');
        $this->getSelect()->group('id');
        \Webkul\MpAssignProduct\Model\ResourceModel\Items\Collection::_renderFiltersBefore();
    }
}
