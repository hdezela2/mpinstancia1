<?php

namespace Chilecompra\WebkulMpAssignProduct\Rewrite\Webkul\MpAssignProduct\Model;

class Items extends \Webkul\MpAssignProduct\Model\Items
{
    /**
     * Load object data.
     *
     * @param int|null $id
     * @param string   $field
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            //return $this->noRoutePreorder(); //this method does not exist in Webkul code
        }

        return \Magento\Framework\Model\AbstractModel::load($id, $field);
    }
}
