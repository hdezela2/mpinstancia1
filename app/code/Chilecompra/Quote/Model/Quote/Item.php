<?php

namespace Chilecompra\Quote\Model\Quote;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\ObjectManager;

/**
 * This class override was present in the Webkul_MpAssignProduct module
 * prior to the upgrade to 2.4.3. Even though this override
 * has been removed, it is still required.
 * That said, we are adding it back.
 *
 * See https://chilecompra.atlassian.net/browse/MU24-51
 * and also https://chilecompra.atlassian.net/browse/MU24-61
 */
class Item extends \Magento\Quote\Model\Quote\Item
{
    /**
     * Check product representation in item
     *
     * @param   Product $product
     * @return  bool
     */
    public function representProduct($product): bool
    {
        $objectManager = ObjectManager::getInstance();
        $helper = $objectManager->get('Webkul\MpAssignProduct\Helper\Data');
        if ($helper->isNewProduct() || $helper->getFullActionName() == "mpassignproduct_order_reorder") {
            return false;
        }
        $itemProduct = $this->getProduct();
        if (!$product || $itemProduct->getId() != $product->getId()) {
            return false;
        }
        /*
         * Check maybe product is planned to be a child of some quote item - in this case we limit search
         * only within same parent item
         */
        $stickWithinParent = $product->getStickWithinParent();
        if ($stickWithinParent) {
            if ($this->getParentItem() !== $stickWithinParent) {
                return false;
            }
        }
        // Check options
        $itemOptions = $this->getOptionsByCode();
        $productOptions = $product->getCustomOptions();
        if ($helper->compareOptions($itemOptions)) {
            return false;
        }
        if (!$this->compareOptions($itemOptions, $productOptions)) {
            return false;
        }
        if (!$this->compareOptions($productOptions, $itemOptions)) {
            return false;
        }

        return true;
    }
}
