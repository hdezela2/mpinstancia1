<?php
declare(strict_types=1);

namespace Chilecompra\MpVendorAttributeManager\Plugin;

use Exception;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Model\AttributeFactory;
use Magento\Customer\Model\Customer\Mapper;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Message\ManagerInterface;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Linets\SoftwareRenewalSetup\Controller\Seller\SaveAttribute;
use Webkul\MpVendorAttributeManager\Helper\Data;
use Webkul\MpVendorAttributeManager\Model\ResourceModel\VendorAttribute\CollectionFactory;
use Webkul\SellerSubAccount\Api\SubAccountRepositoryInterface;

/**
 * @see SaveAttribute
 */
class SaveAttributePlugin
{
    private SubAccountRepositoryInterface $subAccountRepository;
    private ManagerInterface $messageManager;
    private RedirectFactory $resultRedirectFactory;
    private SessionFactory $customerSessionFactory;
    private CustomerRepositoryInterface $_customerRepository;
    private CustomerInterfaceFactory $_customerDataFactory;
    private Mapper $_customerMapper;
    private DataObjectHelper $_dataObjectHelper;
    private Filesystem $filesystem;
    private AttributeFactory $attributeFactory;
    private UploaderFactory $fileUploaderFactory;
    private Data $helper;
    private CollectionFactory $vendorAttributeCollectionFactory;

    public function __construct(
        CustomerInterfaceFactory      $customerDataFactory,
        DataObjectHelper              $dataObjectHelper,
        Mapper                        $customerMapper,
        CustomerRepositoryInterface   $customerRepository,
        SessionFactory                $customerSessionFactory,
        Filesystem                    $filesystem,
        AttributeFactory              $attributeFactory,
        UploaderFactory               $fileUploaderFactory,
        Data                          $helper,
        CollectionFactory             $vendorAttributeCollectionFactory,
        SubAccountRepositoryInterface $subAccountRepository,
        ManagerInterface              $messageManager,
        RedirectFactory               $resultRedirectFactory
    )
    {
        $this->customerSessionFactory = $customerSessionFactory;
        $this->_customerRepository = $customerRepository;
        $this->_customerDataFactory = $customerDataFactory;
        $this->_customerMapper = $customerMapper;
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->filesystem = $filesystem;
        $this->attributeFactory = $attributeFactory;
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->helper = $helper;
        $this->vendorAttributeCollectionFactory = $vendorAttributeCollectionFactory;
        $this->subAccountRepository = $subAccountRepository;
        $this->messageManager = $messageManager;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    /**
     * Prior to the Magento 2 upgrade to 2.4.3, there was a customization done directly to the
     * Webkul\MpVendorAttributeManager\Controller\Seller\SaveAttribute class
     * Since we no longer change 3rd party modules code directly, we are relying on this plugin
     * to accomplish the previous customization
     *
     * @param SaveAttribute $subject
     * @return Redirect|array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function beforeExecute(SaveAttribute $subject)
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $paramData = $subject->getRequest();
        $customerId = $this->customerSessionFactory->create()->getCustomerId();

        try {
            if ($parentCustomer = $this->subAccountRepository->getByCustomerId($customerId)) {
                if ($parentCustomer->getSellerId()) {
                    $customerId = $parentCustomer->getSellerId();
                }
            }
        } catch (NoSuchEntityException | LocalizedException $e) {
            /** No-op. Exit to the original method */
            return [];
        }

        /**
         * Early return to the original method
         */
        if (!$parentCustomer->getSellerId()) {
            return [];
        }

        $vendorAttributeCollection = $this->vendorAttributeCollectionFactory->create()->getVendorAttributeCollection();

        $error = [];
        $sellerData = $paramData->getPostValue();

        $sellerData = $this->setBooleanData($sellerData, $subject);
        $discountVendorAttributes = ['wkv_sw_profesional_tramo2','wkv_sw_profesional_tramo3','wkv_sw_profesional_tramo1','wkv_sw_desarrollo_tramo2','wkv_sw_desarrollo_tramo1','wkv_sw_desarrollo_tramo3'];
        $percentageError = 0;
        foreach ($vendorAttributeCollection as $vendorAttribute) {
            foreach ($sellerData as $attributeCode => $attributeValue) {
                if (in_array($attributeCode,$discountVendorAttributes) && intval($attributeValue) > 20) {
                    $error[] = $vendorAttribute->getAttributeCode();
                    $percentageError = 1;
                }
                if ($attributeCode == $vendorAttribute->getAttributeCode()) {
                    if ($vendorAttribute->getIsRequired() && empty($attributeValue)) {
                        $error[] = $vendorAttribute->getAttributeCode();
                    }
                }
            }
        }
        if (!empty($error)) {
            $percentageError = 0
                ? $this->messageManager->addErrorMessage(__('Vendor Required Attributes can\'t be Empty.'))
                : $this->messageManager->addErrorMessage(__('Discount Percentage must be less than or equal to 20%.'));
        } else {
            $savedCustomerData = $this->_customerRepository->getById($customerId);

            $customer = $this->_customerDataFactory->create();

            $sellerData = array_merge(
                $this->_customerMapper->toFlatArray($savedCustomerData),
                $sellerData
            );
            $sellerData['id'] = $customerId;

            $files = $subject->getRequest()->getFiles();
            try {
                foreach ($files as $fileAttributeCode => $value) {
                    if ($value['error'] == 0) {
                        $result = $this->uploadFileForAttribute($fileAttributeCode);
                        $sellerData[$fileAttributeCode] = $result['file'];
                    }
                }
                $this->_dataObjectHelper->populateWithArray(
                    $customer,
                    $sellerData,
                    CustomerInterface::class
                );
                $customer->setData('ignore_validation_flag', true);
                $this->_customerRepository->save($customer);
                $this->messageManager->addSuccessMessage(__('Vendor Attributes has been saved.'));
            } catch (Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        }
        return $resultRedirect->setPath('marketplace/account/editprofile/');
    }

    private function setBooleanData(array $sellerData, SaveAttribute $subject): array
    {
        $vendorAttributeType = [0, 2];
        $booleanAttributes = $this->vendorAttributeCollectionFactory->create()
            ->getVendorAttributeCollection()
            ->addFieldToFilter("frontend_input", "boolean")
            ->addFieldToFilter("wk_attribute_status", 1)
            ->addFieldToFilter("attribute_used_for", ["in" => $vendorAttributeType]);

        if ($booleanAttributes->getSize()) {
            foreach ($booleanAttributes as $attribute) {
                $attributeCode = $attribute->getAttributeCode();
                $attributeValue = (boolean)$subject->getRequest()->getParam($attributeCode, false);
                $sellerData[$attributeCode] = $attributeValue;
            }
        }
        return $sellerData;
    }

    private function uploadFileForAttribute(string $attributeCode): array
    {
        $path = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)
            ->getAbsolutePath('vendorfiles/');

        $attributeType = $this->attributeFactory->create()
            ->load($attributeCode, "attribute_code")
            ->getFrontendInput();

        $allowedExtensions = explode(',', $this->helper->getConfigData('allowede_' . $attributeType . '_extension'));
        $uploader = $this->fileUploaderFactory->create(['fileId' => $attributeCode]);
        $uploader->setAllowedExtensions($allowedExtensions);
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(false);

        return $uploader->save($path . $attributeType);
    }
}
