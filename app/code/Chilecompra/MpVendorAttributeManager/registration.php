<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Chilecompra_MpVendorAttributeManager', __DIR__);
