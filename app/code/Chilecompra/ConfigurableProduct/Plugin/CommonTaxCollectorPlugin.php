<?php

namespace Chilecompra\ConfigurableProduct\Plugin;

use Magento\ConfigurableProduct\Plugin\Tax\Model\Sales\Total\Quote\CommonTaxCollector as OriginalCommonTaxCollectorPlugin;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector;
use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\Tax\Api\Data\QuoteDetailsItemInterface;
use Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory;

/**
 * The Magento 2 Core team has introduced a new plugin \Magento\ConfigurableProduct\Plugin\Tax\Model\Sales\Total\Quote\CommonTaxCollector::afterMapItem
 * that overrides the tax rate applied on configurable products by setting the tax_class_id of its child items instead.
 * See https://chilecompra.atlassian.net/browse/MU24-119
 */
class CommonTaxCollectorPlugin
{
    private StoreManagerInterface $storeManager;

    public function __construct(StoreManagerInterface $storeManager)
    {
        $this->storeManager = $storeManager;
    }

    /**
     * @param OriginalCommonTaxCollectorPlugin $subject
     * @param callable $proceed
     * @param CommonTaxCollector $originalSubject
     * @param QuoteDetailsItemInterface $result
     * @param QuoteDetailsItemInterfaceFactory $itemDataObjectFactory
     * @param AbstractItem $item
     * @return QuoteDetailsItemInterface|void
     */
    public function aroundAfterMapItem(
        OriginalCommonTaxCollectorPlugin $subject,
        callable $proceed,
        CommonTaxCollector $originalSubject,
        QuoteDetailsItemInterface $result,
        QuoteDetailsItemInterfaceFactory $itemDataObjectFactory,
        AbstractItem $item
    ): QuoteDetailsItemInterface {
        return $result;
    }
}
