<?php
/**
 * This file is part of the MassiveSync Module.
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  MassiveSync
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

namespace Chilecompra\MassiveSync\Model;

/**
 * MassiveSync Offers API Methods.
 *
 * @category Chilecompra
 * @package  MassiveSync
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */
class Offers implements \Chilecompra\MassiveSync\Api\OffersInterface
{
    /* @var \Webkul\MpAssignProduct\Model\ItemsFactory Required for DI */
    protected $_modelItemsFactory;

    /* @var \Webkul\MpAssignProduct\Model\AssociatesFactory Required for DI */
    protected $_modelConfigurablesFactory;

    /* @var \Magento\Eav\Model\Entity\Attribute Required for DI */
    protected $_attributeEntity;
    private string $items_table;
    private string $customer_entity_varchar_table;
    private string $catalog_product_entity_table;
    private string $dccp_assign_status_table;

    /**
     * Load dependencies (DI)
     *
     * @param \Webkul\MpAssignProduct\Model\ItemsFactory         $modelItemsFactory         Simple products
     * @param \Webkul\MpAssignProduct\Model\AssociatesFactory    $modelConfigurablesFactory Configurable products
     * @param \Magento\Eav\Model\Entity\Attribute                $attributeEntity           Attribute Entity
     */
    public function __construct(
        \Webkul\MpAssignProduct\Model\ItemsFactory $modelItemsFactory,
        \Webkul\MpAssignProduct\Model\AssociatesFactory $modelConfigurablesFactory,
        \Magento\Eav\Model\Entity\Attribute $attributeEntity
    ) {
        $this->_modelItemsFactory = $modelItemsFactory;
        $this->_modelConfigurablesFactory = $modelConfigurablesFactory;
        $this->_attributeEntity = $attributeEntity;
    }

    /**
     * Returns offers collection updated since given date.
     *
     * @api
     *
     * @param string $date Last successful sync date.
     *
     * @return offers[] $offers Array of result offers.
     */
    public function getByDate($date)
    {

        /**
        * Se corrige el offset de la zona horaria.
        * Magento cambia todas las consultas a la BD a UTC.
        * La hora que se recibe está en UTC -04 ó UTC -03
        * Para ambos casos, aseguramos tener valores devueltos
        * con un desfase de máximo 1 hora, restando -03
        * a la hora recibida (los que serán sumados por Magento).
        */
        $dateTime = new \DateTime($date.'-03');
        $dateTime->setTimezone(new \DateTimeZone('UTC'));
        $date = $dateTime->format('Y-m-d H:i:s');

        $entityType = 'customer';
        $attributeCode = 'user_rest_id_organization';
        $sellerIdCode = $this->_attributeEntity->loadByCode(
            $entityType,
            $attributeCode
        )->getId();

        $attributeCode2 = 'user_rest_id_active_agreement';
        $userRestIdActiveAgreement = $this->_attributeEntity->loadByCode(
            $entityType,
            $attributeCode2
        )->getId();

        $this->items_table = 'marketplace_assignproduct_items';
        $this->customer_entity_varchar_table = 'customer_entity_varchar';
        $this->catalog_product_entity_table = 'catalog_product_entity';
        $this->dccp_assign_status_table = 'dccp_assign_status';

        // get Items
        $itemsModel = $this->_modelItemsFactory->create();
        $items = $itemsModel->getCollection();
        $items->getSelect()
            ->join(
                ['cpe' => $this->catalog_product_entity_table],
                'main_table.product_id = cpe.entity_id',
                [
                    'idProducto' => 'cpe.sku'
                ]
            )
            ->join(
                ['cev' => $this->customer_entity_varchar_table],
                'main_table.seller_id = cev.entity_id',
                [
                    'idOrganismo' => 'cev.value'
                ]
            )
            ->join(
                ['cev2' => $this->customer_entity_varchar_table],
                'main_table.seller_id = cev2.entity_id',
                [
                    'idConvenio' => 'cev2.value'
                ]
            )
            ->join(
                ['das' => $this->dccp_assign_status_table],
                'main_table.status = das.mage_status',
                [
                    'idEstadoProductoProveedorConvenio' => 'das.dccp_status'
                ]
            )
            ->where("cev.attribute_id = " . $sellerIdCode . " AND cev2.attribute_id = " . $userRestIdActiveAgreement);

        $items->addFieldToSelect('price', 'precio')
            ->addFieldToSelect('qty', 'tieneStock')
            ->addFieldToFilter(
                'main_table.type',
                [
                    'in' => ['simple', 'virtual']
                ]
            )
            ->addFieldToFilter(
                'main_table.updated_at',
                [
                    'gteq' => $date
                ]
            )->addFieldToFilter('main_table.status',
                [
                    'nin' => ['0', '2']
                ]
            );

        // getConfigurables
        $configurablesModel = $this->_modelConfigurablesFactory->create();
        $configurables = $configurablesModel->getCollection();
        $configurables->getSelect()
            ->join(
                ['items_table' => $this->items_table],
                'main_table.parent_id = items_table.id',
                [
                    'idEstadoProductoProveedorConvenio' => 'items_table.status'
                ]
            )
            ->join(
                ['cpe' => $this->catalog_product_entity_table],
                'main_table.product_id = cpe.entity_id',
                [
                    'idProducto' => 'cpe.sku'
                ]
            )
            ->join(
                ['cev' => $this->customer_entity_varchar_table],
                'items_table.seller_id = cev.entity_id',
                [
                    'idOrganismo' => 'cev.value'
                ]
            )
            ->join(
                ['cev2' => $this->customer_entity_varchar_table],
                'items_table.seller_id = cev2.entity_id',
                [
                    'idConvenio' => 'cev2.value'
                ]
            )
            ->join(
                ['das' => $this->dccp_assign_status_table],
                'main_table.status = das.mage_status',
                [
                    'idEstadoProductoProveedorConvenio' => 'das.dccp_status'
                ]
            )
            ->where("cev.attribute_id = " . $sellerIdCode . " AND cev2.attribute_id = " . $userRestIdActiveAgreement);

        $configurables
            ->addFieldToSelect('qty', 'tieneStock')
            ->addFieldToSelect('price', 'precio')
            //->addFieldToSelect('status', 'status')
//            ->addFieldToSelect(new \Zend_Db_Expr(
//                "CASE WHEN main_table.dis_dispersion = 0
//                    THEN 2
//                    ELSE main_table.dis_dispersion
//                END AS idEstadoProductoProveedorConvenio"
//            ))
//            ->addFieldToSelect('status', 'idEstadoProductoProveedorConvenio')
            ->addFieldToFilter(
                ['main_table.updated_at', 'items_table.updated_at'],
                [
                    ['gteq' => $date],
                    ['gteq' => $date]
                ]
            );
        //die($configurables->getSelect()->__toString());

        $offers = array_merge($items->getData(), $configurables->getData());

        return $offers;
    }
}
