<?php
/**
 * This file is part of the MassiveSync Module.
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  MassiveSync
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

namespace Chilecompra\MassiveSync\Api;

/**
 * MassiveSync Offers Interface.
 *
 * @category Chilecompra
 * @package  MassiveSync
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */
interface OffersInterface
{
    /**
     * Returns offers collection updated since given date.
     *
     * @api
     *
     * @param string $date Last successful sync date.
     *
     * @return offers[] $offers Array of result offers.
     */
    public function getByDate($date);
}
