<?php
/**
 * This file is part of the MassiveSync Module.
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  MassiveSync
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

namespace Chilecompra\MassiveSync\Setup;
 
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * MassiveSync Setup Upgrade Schema Class.
 *
 * @category Chilecompra
 * @package  MassiveSync
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
 
    /**
     * Returns offers collection updated since given date.
     *
     * @param SchemaSetupInterface   $setup   Setup Interface.
     * @param ModuleContextInterface $context Context Interface.
     *
     * @return void
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('marketplace_assignproduct_items'),
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ],
                'Updated At'
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('marketplace_assignproduct_associated_products'),
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ],
                'Updated At'
            );

        }

        $setup->endSetup();
    }
}

?>