<?php
declare(strict_types=1);

namespace Chilecompra\WebkulSellerSubAccount\Rewrite\Webkul\SellerSubAccount\Helper;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Model\Customer\Mapper as CustomerMapper;
use Magento\Customer\Model\ResourceModel\Group\Collection as GroupCollection;
use Magento\Customer\Model\Session;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\View\Result\PageFactory as ResultPageFactory;
use Webkul\Marketplace\Model\ControllersRepository;
use Webkul\SellerSubAccount\Api\SubAccountRepositoryInterface;
use Webkul\SellerSubAccount\Model\SubAccount;

class Data extends \Webkul\SellerSubAccount\Helper\Data
{

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    public $httpContext;

    /**
     * @param Context $context
     * @param SubAccountRepositoryInterface $subAccountRepository
     * @param SubAccount $subAccount
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param CustomerMapper $customerMapper
     * @param CustomerInterfaceFactory $customerFactory
     * @param AccountManagementInterface $accountManagement
     * @param DataObjectHelper $dataObjectHelper
     * @param GroupCollection $groupCollection
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Webkul\Marketplace\Helper\Data $helperData
     * @param ControllersRepository $controllersRepository
     * @param \Webkul\Marketplace\Model\SellerFactory $sellerModel
     * @param \Webkul\SellerSubAccount\Model\SubAccountFactory $subAccountSeller
     * @param ResultPageFactory $resultPageFactory
     * @param \Magento\Framework\App\Http\Context $httpContext
     */
    public function __construct(
        Context $context,
        SubAccountRepositoryInterface $subAccountRepository,
        SubAccount $subAccount,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        CustomerMapper $customerMapper,
        CustomerInterfaceFactory $customerFactory,
        AccountManagementInterface $accountManagement,
        DataObjectHelper $dataObjectHelper,
        GroupCollection $groupCollection,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Webkul\Marketplace\Helper\Data $helperData,
        ControllersRepository $controllersRepository,
        \Webkul\Marketplace\Model\SellerFactory $sellerModel,
        \Webkul\SellerSubAccount\Model\SubAccountFactory $subAccountSeller,
        ResultPageFactory $resultPageFactory,
        \Magento\Framework\App\Http\Context $httpContext
    ) {

        parent::__construct(
            $context,
            $subAccountRepository,
            $subAccount,
            $customerSession,
            $customerRepository,
            $customerMapper,
            $customerFactory,
            $accountManagement,
            $dataObjectHelper,
            $groupCollection,
            $moduleList,
            $helperData,
            $controllersRepository,
            $sellerModel,
            $subAccountSeller,
            $resultPageFactory
        );

        $this->httpContext = $httpContext;
    }

    public function getCustomerId()
    {
        return $this->getCustomerSession()->getCustomerId() ?? $this->httpContext->getValue('customer_id');
    }
}
