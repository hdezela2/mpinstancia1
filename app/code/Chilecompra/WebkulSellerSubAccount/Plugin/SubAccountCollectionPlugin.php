<?php

namespace Chilecompra\WebkulSellerSubAccount\Plugin;

use Magento\Framework\DataObject;
use Magento\Store\Model\Store;
use Webkul\SellerSubAccount\Model\ResourceModel\SubAccount\Collection;

class SubAccountCollectionPlugin
{
    /**
     * Filters configuration
     *
     * @var DataObject[]
     */
    private $_filters = [];

    /**
     * Filter rendered flag
     *
     * @var bool
     */
    private $_isFiltersRendered = false;

    /**
     * The original Webkul\SellerSubAccount\Model\ResourceModel\SubAccount\Collection::addStoreFilter
     * calls an undefined method, so we are adding the missing method performAddStoreFilter
     * in here and calling it through our plugin. We don't want to call the original method
     * and that means we are not calling proceed
     *
     * @param Collection $subject
     * @param callable $proceed
     * @param int|array|Store $store
     * @param bool $withAdmin
     * @return $this
     */
    public function aroundAddStoreFilter(
        Collection $subject,
        callable $proceed,
        $store,
        bool $withAdmin = true
    ): SubAccountCollectionPlugin {
        if (!$subject->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }

    /**
     * @param int|array|Store $store
     * @param bool $withAdmin
     * @return void
     */
    private function performAddStoreFilter($store, bool $withAdmin)
    {
        if ($store instanceof Store) {
            $store = [$store->getId()];
        }

        if (!is_array($store)) {
            $store = [$store];
        }

        if ($withAdmin) {
            $store[] = Store::DEFAULT_STORE_ID;
        }

        $this->addFilter('store', ['in' => $store], 'public');
    }

    /**
     * @param string $field
     * @param string|array $value
     * @param string $type
     * @return $this
     */
    private function addFilter(string $field, $value, string $type = 'and')
    {
        $filter = new DataObject();
        // implements ArrayAccess
        $filter['field'] = $field;
        $filter['value'] = $value;
        $filter['type'] = strtolower($type);

        $this->_filters[] = $filter;
        $this->_isFiltersRendered = false;
        return $this;
    }
}
