<?php
/**
 * This file is part of the LoginMp Module.
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  LoginMp
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

namespace Chilecompra\LoginMp\Setup;
 
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var Magento\Customer\Setup\CustomerSetupFactory
     */
    protected $_customerSetupFactory;

    protected $_attributeSetFactory;    

    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
    ){
        $this->_customerSetupFactory = $customerSetupFactory;
        $this->_attributeSetFactory = $attributeSetFactory;
    }

    /**
     * Upgrade data function.
     *
     * @param SchemaSetupInterface   $setup   Setup Interface.
     * @param ModuleContextInterface $context Context Interface.
     *
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.3') < 0) {          

            /** @var Magento\Customer\Setup\CustomerSetupFactory $customerInstaller */
            $customerInstaller = $this->_customerSetupFactory->create(['resourceName' => 'customer_setup', 'setup' => $setup]);
            
            $customerEntity = $customerInstaller->getEavConfig()->getEntityType('customer');
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();
            
            $attributeSet = $this->_attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

            $customerInstaller->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'user_rest_partner_organizations', [
                'type' => 'text',
                'label' => 'Lista de Organizaciones(MP)',
                'input' => 'textarea',
                'required' => false,
                'visible' => false,
                'user_defined' => true,
                'position' =>999,
                'system' => 0,
            ]);

            $attribute = $customerInstaller->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'user_rest_partner_organizations')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer'],
            ]);

            $attribute->save();

        }    
        $setup->endSetup();
    }
}

?>