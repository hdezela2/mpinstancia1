<?php

/**
 * This file is part of the LoginMp Module.
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  LoginMp
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

namespace Chilecompra\LoginMp\Helper;

use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Exception;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;

/**
 * LoginMp General Helper. It creates Customer and Vendor users.
 *
 * @category Chilecompra
 * @package  LoginMp
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */
class Helper extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $storeManager;
    protected $customerFactory;
    protected $addressFactory;
    protected $messageManager;
    protected $escaper;
    protected $session;
    protected $cart;
    protected $logger;

    const ID_AGREEMENT_ASEO = 5800266;
    const CODE_AGREEMENT_ASEO = 'convenio_aseo';
    const CODE_STORE_AGREEMENT_ASEO = 'aseo';

    const ID_AGREEMENT_EMERGENCY = 5800262;
    const CODE_AGREEMENT_EMERGENCY = 'emergencias';
    const CODE_STORE_AGREEMENT_EMERGENCY = 'emergencias';

    const ID_AGREEMENT_EMERGENCY_2021_09 = 5800300;
    const CODE_AGREEMENT_EMERGENCY_2021_09 = 'emergencias202109';
    const CODE_STORE_AGREEMENT_EMERGENCY_2021_09 = 'emergencias202109';

    const ID_AGREEMENT_COMPUTERS = 5800272;
    const CODE_AGREEMENT_COMPUTERS = 'computadores';
    const CODE_STORE_AGREEMENT_COMPUTERS = 'computadores';

    const ID_AGREEMENT_COMPUTERS_2021_01 = 5800289;
    const CODE_AGREEMENT_COMPUTERS_2021_01 = 'computadores202101';
    const CODE_STORE_AGREEMENT_COMPUTERS_2021_01 = 'computadores202101';

    const ID_AGREEMENT_COMPUTERS_2021_12 = 5800307;
    const CODE_AGREEMENT_COMPUTERS_2021_12 = 'computadores202201';
    const CODE_STORE_AGREEMENT_COMPUTERS_2021_12 = 'computadores202201';

    const ID_AGREEMENT_VOUCHER = 5800271;
    const CODE_AGREEMENT_VOUCHER = 'convenio_voucher';
    const CODE_STORE_AGREEMENT_VOUCHER = 'voucher';

    const ID_AGREEMENT_VOUCHER_2021_06 = 5800295;
    const CODE_AGREEMENT_VOUCHER_2021_06 = 'voucher202106';
    const CODE_STORE_AGREEMENT_VOUCHER_2021_06 = 'voucher202106';

    const ID_AGREEMENT_SOFTWARE = 5800275;
    const CODE_AGREEMENT_SOFTWARE = 'convenio_software';
    const CODE_STORE_AGREEMENT_SOFTWARE = 'software';

    const ID_AGREEMENT_FUELS = 5800276;
    const CODE_AGREEMENT_FUELS = 'combustibles';
    const CODE_STORE_AGREEMENT_FUELS = 'combustibles';

    const ID_AGREEMENT_COMBUSTIBLES202110 = 5802326;
    const CODE_AGREEMENT_COMBUSTIBLES202110 = 'combustibles202110';
    const CODE_STORE_AGREEMENT_COMBUSTIBLES202110 = 'combustibles202110';

    const ID_AGREEMENT_DESKITEMS = 5800280;
    const CODE_AGREEMENT_DESKITEMS = 'escritorio';
    const CODE_STORE_AGREEMENT_DESKITEMS = 'escritorio';

    const ID_AGREEMENT_FURNITURE = 5800277;
    const CODE_AGREEMENT_FURNITURE = 'mobiliario';
    const CODE_STORE_AGREEMENT_FURNITURE = 'mobiliario';

    const ID_AGREEMENT_DEPOT = 5800254;
    const CODE_AGREEMENT_DEPOT = 'base';
    const CODE_STORE_AGREEMENT_DEPOT = 'ferreteria';

    const ID_AGREEMENT_FOODS = 5800279;
    const CODE_AGREEMENT_FOODS = 'alimentos';
    const CODE_STORE_AGREEMENT_FOODS = 'alimentos';

    const ID_AGREEMENT_CARS = 5800296;
    const CODE_AGREEMENT_CARS = 'vehiculos202106';
    const CODE_STORE_AGREEMENT_CARS = 'vehiculos';

    const ID_AGREEMENT_MED_SUPPLIES = 5800305;
    const CODE_AGREEMENT_MED_SUPPLIES = 'insumos';
    const CODE_STORE_AGREEMENT_MED_SUPPLIES = 'insumos';

    const ID_AGREEMENT_SOFTWARE_2022 = SoftwareRenewalConstants::ID_AGREEMENT;
    const CODE_AGREEMENT_SOFTWARE_2022 = SoftwareRenewalConstants::WEBSITE_CODE;
    const CODE_STORE_AGREEMENT_SOFTWARE_2022 = SoftwareRenewalConstants::STORE_CODE;

    const ID_AGREEMENT_ASEO_2022 = AseoRenewalConstants::ID_AGREEMENT;
    const CODE_AGREEMENT_ASEO_2022 = AseoRenewalConstants::WEBSITE_CODE;
    const CODE_STORE_AGREEMENT_ASEO_2022 = AseoRenewalConstants::STORE_CODE;

    const CONVENIOS_WEBSITES = [
        'base' => 'ferreteria',
        self::CODE_AGREEMENT_COMPUTERS => self::CODE_STORE_AGREEMENT_COMPUTERS,
        self::CODE_AGREEMENT_COMPUTERS_2021_01 => self::CODE_STORE_AGREEMENT_COMPUTERS_2021_01,
        self::CODE_AGREEMENT_COMPUTERS_2021_12 => self::CODE_STORE_AGREEMENT_COMPUTERS_2021_12,
        self::CODE_AGREEMENT_EMERGENCY => self::CODE_STORE_AGREEMENT_EMERGENCY,
        self::CODE_AGREEMENT_EMERGENCY_2021_09 => self::CODE_STORE_AGREEMENT_EMERGENCY_2021_09,
        self::CODE_AGREEMENT_ASEO => self::CODE_STORE_AGREEMENT_ASEO,
        self::CODE_AGREEMENT_VOUCHER => self::CODE_STORE_AGREEMENT_VOUCHER,
        self::CODE_AGREEMENT_VOUCHER_2021_06 => self::CODE_STORE_AGREEMENT_VOUCHER_2021_06,
        self::CODE_AGREEMENT_SOFTWARE => self::CODE_STORE_AGREEMENT_SOFTWARE,
        self::CODE_AGREEMENT_FUELS => self::CODE_STORE_AGREEMENT_FUELS,
        self::CODE_AGREEMENT_FURNITURE => self::CODE_STORE_AGREEMENT_FURNITURE,
        self::CODE_AGREEMENT_DESKITEMS => self::CODE_STORE_AGREEMENT_DESKITEMS,
        self::CODE_AGREEMENT_FOODS => self::CODE_STORE_AGREEMENT_FOODS,
        self::CODE_AGREEMENT_CARS => self::CODE_STORE_AGREEMENT_CARS,
        self::CODE_AGREEMENT_FOODS => self::CODE_STORE_AGREEMENT_FOODS,
        self::CODE_AGREEMENT_MED_SUPPLIES => self::CODE_STORE_AGREEMENT_MED_SUPPLIES,
        self::CODE_AGREEMENT_ASEO_2022 => self::CODE_STORE_AGREEMENT_ASEO_2022,
        self::CODE_AGREEMENT_SOFTWARE_2022 => self::CODE_STORE_AGREEMENT_SOFTWARE_2022
    ];


    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\AddressFactory $addressFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Framework\Logger\Monolog $logger
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Escaper $escaper,
        \Magento\Customer\Model\Session $session,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Logger\Monolog $logger
    ) {
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->addressFactory = $addressFactory;
        $this->messageManager = $messageManager;
        $this->escaper = $escaper;
        $this->session = $session;
        $this->cart = $cart;
        $this->logger = $logger;

        parent::__construct($context);
    }

    /**
     * Create customer or vendor (they're saved using the same models).
     *
     * @param string $userId New User's MP User Id.
     * @param string $firstName New User's First Name.
     * @param string $lastName New User's Last Name.
     * @param string $email New User's email (generated by convention).
     * @param string $contactEmail New User's email (for communication purposes).
     * @param string $companyId New User's Mercado Público Company ID.
     * @param string $company New User's Mercado Público Company Id.
     * @param string $organization New User's Mercado Público Organization.
     * @param string $rut New User's Mercado Público RUT.
     * @param array $address New User's Address.
     * @param string $token New User's Current Session Token.
     * @param string $activeAgreement New User's Mercado Público Active Agreement.
     * @param string $partnerOrganizations New User's SERIALIZED partner organizations.
     *
     * @return int|bool $customerId Magento's Customer Id
     * @throws NoSuchEntityException|LocalizedException
     */
    public function createCustomer(
        $userId,
        $firstName,
        $lastName,
        $email,
        $contactEmail,
        $companyId,
        $company,
        $organization,
        $rut,
        $address,
        $token,
        $activeAgreement,
        $partnerOrganizations
    ) {

        if ($activeAgreement == self::ID_AGREEMENT_VOUCHER) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_VOUCHER)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_VOUCHER)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_VOUCHER_2021_06) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_VOUCHER_2021_06)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_VOUCHER_2021_06)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_ASEO) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_ASEO)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_ASEO)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_SOFTWARE) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_SOFTWARE)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_SOFTWARE)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_FUELS) {
            $websiteId = $this->storeManager->getWebsite(self::CODE_AGREEMENT_FUELS)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_FUELS)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_COMBUSTIBLES202110) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_COMBUSTIBLES202110)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_COMBUSTIBLES202110)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_DESKITEMS) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_DESKITEMS)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_DESKITEMS)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_FOODS) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_FOODS)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_FOODS)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_EMERGENCY) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_EMERGENCY)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_EMERGENCY)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_EMERGENCY_2021_09) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_EMERGENCY_2021_09)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_EMERGENCY_2021_09)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_COMPUTERS) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_COMPUTERS)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_COMPUTERS)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_COMPUTERS_2021_01) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_COMPUTERS_2021_01)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_COMPUTERS_2021_01)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_COMPUTERS_2021_12) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_COMPUTERS_2021_12)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_COMPUTERS_2021_12)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_FURNITURE) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_FURNITURE)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_FURNITURE)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_CARS) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_CARS)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_CARS)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_MED_SUPPLIES) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_MED_SUPPLIES)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_MED_SUPPLIES)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_SOFTWARE_2022) {
            $websiteId = $this->storeManager->getWebsite(self::CODE_AGREEMENT_SOFTWARE_2022)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_SOFTWARE_2022)->getId();
        } elseif ($activeAgreement == self::ID_AGREEMENT_ASEO_2022) {
            $websiteId  = $this->storeManager->getWebsite(self::CODE_AGREEMENT_ASEO_2022)->getWebsiteId();
            $storeId = $this->storeManager->getStore(self::CODE_STORE_AGREEMENT_ASEO_2022)->getId();
        } else {
            $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
            $storeId = $this->storeManager->getStore()->getId();
        }

        // instantiate customer object
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->setStoreId($storeId);


        // check if customer is already present
        // if customer is already present, then only do login
        // else create new customer
        $customerId = $customer->loadByEmail($email)->getId();
        if ($customerId) {
            $_customer = $customer->loadByEmail($email);

            // update custom attributes
            $_customer->setUserRestAtk($token);
            $_customer->setUserRestEmail($contactEmail);
            $_customer->setFirstname($firstName);
            $_customer->setLastname($lastName);
            $_customer->setUserRestIdOrganization($companyId);
            $_customer->setUserRestPartnerOrganizations($partnerOrganizations);
            $_customer->setUserRestIdActiveAgreement($activeAgreement);
            $_customer->setWkvDccpRut($rut);
            $_customer->setWkvDccpBusinessName($organization);
            $_customer->save();

            $context = [
                'elasticsearch' => true,
                'userId' => $_customer->getUserRestId(),
                'firstName' => $_customer->getFirstname(),
                'lastName' => $_customer->getLastname(),
                'email' => $_customer->getEmail(),
                'contactEmail' => $_customer->getUserRestEmail(),
                'companyId' => $_customer->getUserRestIdOrganization(),
                'company' => $_customer->getCompany(),
                'agreementId' => $_customer->getUserRestIdActiveAgreement()
            ];
            $this->logger->info('Usuario actualizado', $context);

            $this->loginCustomer($_customer);
            return $customerId;
        } else {
            // prepare customer data
            $customer->setEmail($email);
            $customer->setFirstname($firstName);
            $customer->setLastname($lastName);
            $customer->setWebsiteId($websiteId);
            $customer->setStoreId($storeId);

            /**
             * Prior to the upgrade to Magento 2.4.3 we used to pass null to setPassword
             * however it no longer works as it causes an exception.
             *
             * See https://chilecompra.atlassian.net/browse/MU24-32
             */
            $customer->setPassword((string)time());

            // set the customer as confirmed
            $customer->setForceConfirmed(true);

            // set custom attributes
            $customer->setUserRestEmail($contactEmail);
            $customer->setUserRestAtk($token);
            $customer->setUserRestId($userId);
            $customer->setUserRestIdOrganization($companyId);
            $customer->setUserRestPartnerOrganizations($partnerOrganizations);
            $customer->setUserRestIdActiveAgreement($activeAgreement);
            $customer->setWkvDccpRut($rut);
            $customer->setWkvDccpBusinessName($organization);

            // Save customer
            try {
                $customer->save();
                $context = [
                    'elasticsearch' => true,
                    'userId' => $customer->getUserRestId(),
                    'firstName' => $customer->getFirstname(),
                    'lastName' => $customer->getLastname(),
                    'email' => $customer->getEmail(),
                    'contactEmail' => $customer->getUserRestEmail(),
                    'companyId' => $customer->getUserRestIdOrganization(),
                    'company' => $customer->getCompany(),
                    'agreementId' => $customer->getUserRestIdActiveAgreement()
                ];
                $this->logger->info('Usuario creado', $context);

                // save customer address
                if (!is_null($address) && isset($address['comuna'])) {
                    $customerAddress = $this->addressFactory->create();
                    $customerAddress->setCustomerId($customer->getId())
                        ->setFirstname($firstName)
                        ->setLastname($lastName)
                        ->setCountryId('CL')
                        ->setRegion($address['region'])
                        ->setPostcode($address['comuna'])
                        ->setComuna($address['comuna'])
                        ->setCity($address['comuna'])
                        ->setTelephone($companyId)
                        ->setCompany($company)
                        ->setStreet(
                            array(
                                '0' => $address['direccion'],
                                '1' => ''
                            )
                        )
                        ->setIsDefaultBilling('1')
                        ->setIsDefaultShipping('1')
                        ->setSaveInAddressBook('1');

                    try {
                        // save customer address
                        $customerAddress->save();
                    } catch (Exception $e) {
                        $this->messageManager->addException(
                            $e,
                            __('Error saving the customer address.')
                        );
                    }
                }

                $this->loginCustomer($customer);
                return $customer->getId();
            } catch (StateException $e) {
                $message = 'There is already an account with this email address.';
                $this->messageManager->addError(
                    $message
                );
            } catch (InputException $e) {
                $this->messageManager->addError(
                    $this->escaper->escapeHtml($e->getMessage())
                );
                foreach ($e->getErrors() as $error) {
                    $this->messageManager->addError(
                        $this->escaper->escapeHtml($error->getMessage())
                    );
                }
            } catch (LocalizedException $e) {
                $this->messageManager->addError(
                    $this->escaper->escapeHtml($e->getMessage())
                );
            } catch (Exception $e) {
                $context = [
                    'elasticsearch' => true,
                    'userId' => $userId,
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'email' => $email,
                    'contactEmail' => $contactEmail,
                    'companyId' => $companyId,
                    'company' => $company,
                    'agreementId' => $activeAgreement
                ];
                $this->logger->error('Error al crear usuario', $context);
                $this->messageManager->addException(
                    $e,
                    __('Error saving the customer.' . $e->getMessage())
                );
            }
        }
        return true;
    }

    /**
     * Log-out previous user and log-in by given user entity
     *
     * @param \Magento\Customer\Model\Customer $customer Customer Entity
     *
     * @return void
     * @throws NoSuchEntityException
     */
    public function loginCustomer($customer)
    {
        if ($this->session->getId()) {
            /* Logout if logged in */
            $this->session->logout()->setLastCustomerId($customer->getId());
        } else {
            /* Remove items from guest cart */
            foreach ($this->cart->getQuote()->getAllVisibleItems() as $item) {
                $this->cart->removeItem($item->getId());
            }
            $this->cart->save();
        }
        if (!$customer->getId()) {
            throw new NoSuchEntityException(__("Customer doesn't exists."), 1);
        }
        $this->session->loginById($customer->getId());
        //$this->session->setCustomerAsLoggedIn($customer);
        $this->session->regenerateId();
        $context = [
            'elasticsearch' => true,
            'userId' => $customer->getUserRestId(),
            'firstName' => $customer->getFirstname(),
            'lastName' => $customer->getLastname(),
            'email' => $customer->getEmail(),
            'contactEmail' => $customer->getUserRestEmail(),
            'companyId' => $customer->getUserRestIdOrganization(),
            'company' => $customer->getCompany(),
            'agreementId' => $customer->getUserRestIdActiveAgreement()
        ];
        $this->logger->info('Login exitoso', $context);
    }
}
