<?php
/**
 * This file is part of the LoginMp Module.
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  LoginMp
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

namespace Chilecompra\LoginMp\Helper;

use Webkul\SellerSubAccount\Model\SubAccount;
use Webkul\SellerSubAccount\Api\SubAccountRepositoryInterface;
use Chilecompra\LoginMp\Controller\Account\Loggedin;

/**
 * LoginMp Vendor helper. It creates, validates and logs in vendor users.
 *
 * @category Chilecompra
 * @package  LoginMp
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */
class HelperVendor extends \Magento\Framework\App\Helper\AbstractHelper
{
    const FLAG_NO_DISPATCH = 'no-dispatch';

    /* @var \Magento\Customer\Model\Url Url Model. */
    protected $url;

    /* @var \Magento\Customer\Model\Session Session Model.*/
    protected $customerSession;

    /* @var SubAccount Webkul Subaccount Model.*/
    protected $subAccount;

    /* @var SubAccountRepositoryInterface Webkul Subaccount RepositoryInterface.*/
    protected $subAccountRepository;

    /* @var CustomerCollection Customer Collection Resource Model*/
    protected $customerCollection;

    /**
     * @var array
     */
    private $globalResourcesDisable;

    /**
     * Helper Constructor
     *
     * @param Context                         $context              The context
     * @param \Magento\Customer\Model\Url     $url                  URL Model
     * @param \Magento\Customer\Model\Session $customerSession      Customer Session
     * @param SubAccount                      $subAccount           SubAccount Model
     * @param SubAccountRepositoryInterface   $subAccountRepository Repository
     * @param Collection                      $customerCollection   Cust. Collection
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Url $url,
        \Magento\Customer\Model\Session $customerSession,
        SubAccount $subAccount,
        SubAccountRepositoryInterface $subAccountRepository,
        \Magento\Customer\Model\ResourceModel\Customer\Collection $customerCollection
    ) {
        $this->url = $url;
        $this->customerSession = $customerSession;
        $this->subAccount = $subAccount;
        $this->subAccountRepository = $subAccountRepository;
        $this->customerCollection = $customerCollection;
        parent::__construct($context);

        $this->globalResourcesDisable = [
            'marketplace/product/productlist',
            'marketplace/product/add',
            'marketplace/product/create',
            'marketplace/product/edit'
        ];
    }

    /**
     * Creates and validates Vendor subaccount.
     *
     * @param string $sellerId   Magento's Vendor Id
     * @param string $customerId Magento's Customer Id
     * @param string|null $agreementId Chilecompra's Agreement Id
     *
     * @return void
     */
    public function createVendor($sellerId, $customerId, $agreementId = null)
    {
        if ($this->_isSubAccount($customerId)) {
            return $customerId;
        }
        $list = $this->scopeConfig->getValue(
            'sellersubaccount/sub_account_permission/manage_sub_account_permission',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if (Loggedin::ID_AGREEMENT_SOFTWARE == $agreementId) {
            $list = $this->softwarePermissions($list);
        }

        $subAccount = $this->subAccount;
        $subAccount->setSellerId($sellerId);
        $subAccount->setCustomerId($customerId);
        $subAccount->setPermissionType($list);
        $subAccount->setStatus(1);

        try{
            $id = $subAccount->save()->getId();
            return $id;
        } catch (\Exception $ex) {
            var_export($ex->getMessage()); die();
        }
    }

    /**
     * Returns Magento's Vendor Id from a given MP's Organization Id.
     *
     * @param string $organizationId Mercado Publico's Organization Id.
     *
     * @return int $parentSellerId Magento's Vendor Id
     */
    public function getVendorId($organizationId, $agreementId)
    {
        $parentSellerId = $this->customerCollection->addAttributeToSelect('*')
            ->addAttributeToFilter('is_vendor_group', 1)
            ->addAttributeToFilter('user_rest_id_organization', $organizationId)
            ->addAttributeToFilter('user_rest_id_active_agreement', $agreementId)
            ->getFirstItem()
            ->getData('entity_id');

        return $parentSellerId;
    }

    /**
     * Checks if subaccount already exists.
     *
     * @param string $customerId Magento's Customer Id.
     *
     * @return bool
     */
    private function _isSubAccount($customerId)
    {
        $subAccount = $this->subAccountRepository->getByCustomerId($customerId);
        if ($subAccount->getId()) {
            return true;
        }
        return false;
    }

    private function softwarePermissions($list)
    {
        $newPermissions = [];
        $permissions = explode(',', $list);
        foreach ($permissions as $key => $permission) {
            if ($permission != 'mpassignproduct/product/view' &&
                !in_array($permission, $this->globalResourcesDisable)) {
                $newPermissions[$key] = $permission;
            }
        }

        return implode(',', $newPermissions);
    }
}
