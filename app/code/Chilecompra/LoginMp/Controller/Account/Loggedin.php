<?php

/**
 * This file is part of the LoginMp Module.
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  LoginMp
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

namespace Chilecompra\LoginMp\Controller\Account;

use Chilecompra\LoginMp\Helper\Helper;
use Chilecompra\LoginMp\Helper\HelperAdmin;
use Chilecompra\LoginMp\Helper\HelperVendor;
use Formax\CustomerData\Plugin\CustomerData\Customer;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Logger\Monolog;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Summa\CustomerData\Helper\Data as CustomerData;
use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Customer\Model\ResourceModel\CustomerFactory;
use Zend_Http_Client;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;

/**
 * LoginMp Loggedin Class. Endpoint account controller.
 *
 * @category Chilecompra
 * @package  LoginMp
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */
class Loggedin
    extends Action
    implements CsrfAwareActionInterface
{
    protected $urlInterface;
    protected $scopeConfig;
    protected $helper;
    protected $helperAdmin;
    protected $helperVendor;
    protected $httpClientFactory;
    protected $logger;
    protected $resultJsonFactory;
    protected $resultRedirectFactory;
    protected $_storeManager;
    protected $customerData;
    protected $customerModel;
    protected $customerFactory;
    private   $customerSession;

    const ID_AGREEMENT_ASEO = 5800266;
    const ID_AGREEMENT_EMERGENCY = 5800262;
    const ID_AGREEMENT_EMERGENCY_2021_09 = ConstantsEmergency202109::ID_AGREEMENT_EMERGENCY_2021_09;
    const ID_AGREEMENT_VOUCHER = 5800271;
    const ID_AGREEMENT_VOUCHER_2021_06 = 5800295;
    const ID_AGREEMENT_SOFTWARE = 5800275;
    const ID_AGREEMENT_FUELS = 5800276;
    const ID_AGREEMENT_FURNITURE = 5800277;
    const ID_AGREEMENT_COMPUTERS = 5800272;
    const ID_AGREEMENT_COMPUTERS_2021_01 = 5800289;
    const ID_AGREEMENT_GAS = 5800297;
    const ID_AGREEMENT_COMBUSTIBLES202110 = 5802326;
    const ID_AGREEMENT_COMPUTERS_2021_12 = 5800307;
    const ID_AGREEMENT_SOFTWARE_2022 = SoftwareRenewalConstants::ID_AGREEMENT;
    const ID_AGREEMENT_ASEO_2022 = AseoRenewalConstants::ID_AGREEMENT;

    /**
     * Loggedin constructor.
     * @param Context $context
     * @param UrlInterface $urlInterface
     * @param ScopeConfigInterface $scopeConfig
     * @param Helper $helper
     * @param HelperAdmin $helperAdmin
     * @param HelperVendor $helperVendor
     * @param ZendClientFactory $httpClientFactory
     * @param Monolog $logger
     * @param JsonFactory $resultJsonFactory
     * @param RedirectFactory $resultRedirectFactory
     * @param StoreManagerInterface $storeManager
     * @param CustomerData $customerData
     * @param CustomerModel $customerModel
     * @param CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        Context $context,
        UrlInterface $urlInterface,
        ScopeConfigInterface $scopeConfig,
        Helper $helper,
        HelperAdmin $helperAdmin,
        HelperVendor $helperVendor,
        ZendClientFactory $httpClientFactory,
        Monolog $logger,
        JsonFactory $resultJsonFactory,
        RedirectFactory $resultRedirectFactory,
        StoreManagerInterface $storeManager,
        CustomerData $customerData,
        CustomerModel $customerModel,
        CustomerFactory $customerFactory,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->urlInterface = $urlInterface;
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
        $this->helperAdmin = $helperAdmin;
        $this->helperVendor = $helperVendor;
        $this->httpClientFactory = $httpClientFactory;
        $this->logger = $logger;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->_storeManager = $storeManager;
        $this->customerData = $customerData;
        $this->customerModel = $customerModel;
        $this->customerFactory = $customerFactory;
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * Csrf Validation Exception (for allowing POST from external server).
     *
     * @param RequestInterface $request
     * @return InvalidRequestException|null
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ? InvalidRequestException {
        return null;
    }

    /**
     * Csrf Validation (for allowing POST from external server).
     *
     * @param RequestInterface $request Controller Request.
     *
     * @return bool True if is allowed.
     */
    public function validateForCsrf(
        RequestInterface $request
    ): ? bool {
        $allowedOrigins = [
            "https://local.chilecompra.com/",
            "http://local.chilecompra.com/",
            "http://127.0.0.1",
            "http://localhost",
            "https://www.mercadopublico.cl",
            "http://www.mercadopublico.cl",
            "https://mercadopublico.cl",
            "http://mercadopublico.cl",
            "https://mercadopublico.cl",
            "http://www.mercadopublico.cl",
            "https://www.mercadopublico.cl",
            "http://desarrollo.mercadopublico.cl",
            "https://desarrollo.mercadopublico.cl",
            "http://testqa.mercadopublico.cl",
            "http://desarrollo.mercadopublico.cl",
            "https://desarrollo.mercadopublico.cl",
            "http://pre.mercadopublico.cl",
            "https://pre.mercadopublico.cl",
            "http://pre1.mercadopublico.cl",
            "https://pre1.mercadopublico.cl",
            "http://pre2.mercadopublico.cl",
            "https://pre2.mercadopublico.cl",
            "http://pro1.mercadopublico.cl",
            "https://pro1.mercadopublico.cl",
            "http://pro2.mercadopublico.cl",
            "https://pro2.mercadopublico.cl",
            "http://pro3.mercadopublico.cl",
            "https://pro3.mercadopublico.cl",
            "http://pro4.mercadopublico.cl",
            "https://pro4.mercadopublico.cl",
            "http://pro5.mercadopublico.cl",
            "https://pro5.mercadopublico.cl",
            "http://pro6.mercadopublico.cl",
            "https://pro6.mercadopublico.cl",
            "http://pro7.mercadopublico.cl",
            "https://pro7.mercadopublico.cl",
            "http://pro8.mercadopublico.cl",
            "https://pro8.mercadopublico.cl",
            "http://pro9.mercadopublico.cl",
            "https://pro9.mercadopublico.cl",
            "http://206.48.140.40",
            "https://206.48.140.40",
            false
        ];
        return in_array($request->getHeader('origin'), $allowedOrigins);
    }

    /**
     * Controller Action.
     *
     * @return void
     */
    public function execute()
    {
        $resultRedirectFactory = $this->resultRedirectFactory->create();

        if (
            $this->_request->getParam('token')
            || $this->_request->getParam('access_token')
        ) {
            $token = $this->_request->getParam('token')
                ? $this->_request->getParam('token')
                : $this->_request->getParam('access_token');
        } else {
            $jsonResult = $this->resultJsonFactory->create();
            $errorArr = ['Error' => 'Wrong parameters'];
            $jsonResult->setData($errorArr);
            return $jsonResult;
        }
        //$token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJlN01mM25YdXVzYlRGbXM4OUl1Zi03NlFpdGdRaUJlR3BNd1lfX0ptT3hVIn0.eyJqdGkiOiIyMjVmNGZlYS1hNzAxLTRiM2MtYjhiNy1kMGI0YTUyYTY3YWUiLCJleHAiOjE1NzM3NzQ2NTQsIm5iZiI6MCwiaWF0IjoxNTczNzQ1ODU0LCJpc3MiOiJodHRwczovL3Nzby1xYS1zZWd1cmlkYWQtcmhzc28uYXBwcy5jaGlsZWNvbXByYS5jbC9hdXRoL3JlYWxtcy9jaGlsZWNvbXByYXJlYWxtIiwic3ViIjoiZjpmYmMxYWE5OC02ODNjLTQwYjctYmU3MC02YzQwZmJlZDIyYjY6MTIyMTE5NF81MDA5NzciLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJtZXJjYWRvUHVibGljb0NsaWVudCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjBjZjQ2NjllLTkyMTUtNDUwMC1iYzhjLWFmMzYzNmU4NDAyZCIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiY29uc3Vtb3NlcnZpY2lvIiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiY29kaWdvT3JnYW5pc21vIjoiNTAwOTc3IiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJjb2RpZ29Vc3VhcmlvIjoiMTIyMTE5NCIsIm5hbWUiOiJDcmlzdGlhbiAgTXXDsW96ICIsInRpcG9Vc3VhcmlvIjoiQ29tcHJhZG9yIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiMTIyMTE5NF81MDA5NzciLCJnaXZlbl9uYW1lIjoiQ3Jpc3RpYW4gIE11w7FveiAifQ.GeU6nW-Jcb-74JA4Gv-qs3dlg87hG7VWQg6fZSNVh6J0TjVGkviUbKeRxdFj11OnVmWFilsp9kglHdKEGyMyjP8YT-iKuYHNdAHwY_tR1MrHU-2Dw_YtCdj8a0_UfOl0s6Of5LbTIZL_9iL7S5_KZrPhrC_MI12HHG3C14aqICmenDJWxeLbqtrpcyoKaGXoNgcEzcPPgbGf-lyROE_O-VZeV0So8yLEZ6bV8eVGdDkgdwptW0jaE_xZquB7FvMyFF_S9HbCQ96tut9p_fEKdB0fft2SPxZ7GwXEn2x25CGBwRr41yRO1y2zsUPVoHJX5XQYhCc-D7P0qPf809vsXg";

        $urlBase = $this->urlInterface->getBaseUrl();
        $userData = $this->_getUserData($token);

        if (isset($userData['Error'])) {
            $jsonResult = $this->resultJsonFactory->create();
            $jsonResult->setData($userData);
            return $jsonResult;
        }

        $userId = $this->_decrypt($userData['userid']);
        $vatId = $userData['rut'];
        $firstName = ($userData['nombre'] == '' || is_null($userData['nombre']))
            ? '-'
            : $userData['nombre'];
        $lastName = ($userData['apellido'] == '' || is_null($userData['apellido']))
            ? '-'
            : $userData['apellido'];
        $contactEmail = $userData['email'];
        $companyId = $userData['organizacion']['idOrganizacion'];
        $company = $userData['organizacion']['nombre'];
        $organization = isset($userData['organizacion']['nombreLegal']) ? $userData['organizacion']['nombreLegal'] : '';
        $rut = isset($userData['organizacion']['rut']) ? $userData['organizacion']['rut'] : '';
        $email = $userId . "@email.com";
        $contactEmail = $userData['email'];
        $partnerOrganizations = (array_key_exists('organizaciones_asociadas', $userData) && is_array($userData['organizaciones_asociadas']))
            ? json_encode($userData['organizaciones_asociadas'])
            : '';

        // Get shipping Address
        $address = $this->getAddressDCCPInfo($userData);

        $userType = $userData['organizacion']['tipo'];
        $userRoles = $userData['organizacion']['roles'];

        $agreementId = $this->_request->getParam('idConvenio');

        if ($userType == 'COMPRADOR' && count($userRoles) > 1) {

            $userName = $userId . '-' . $agreementId;
            $email = $userName . "@email.com";
            if (!$this->helperAdmin->userExists($userName)) {
                $password = $userData['userid'] . $userId;
                $this->helperAdmin->createAdmin(
                    $firstName,
                    $lastName,
                    $email,
                    $contactEmail,
                    $userName,
                    $userId,
                    $password,
                    $agreementId,
                    $token
                );
            } else {
                $this->helperAdmin->updateUserToken($userName, $token);
            }
            $this->helperAdmin->loginByUsername($userName, $token);
            exit();
        }

        if ($userType == 'PROVEEDOR') {
            $email = "{$userId}-{$companyId}-{$agreementId}@email.com";
        }

        $customerId = $this->helper->createCustomer(
            $userId,
            $firstName,
            $lastName,
            $email,
            $contactEmail,
            $companyId,
            $company,
            $organization,
            $rut,
            $address,
            $token,
            $agreementId,
            $partnerOrganizations
        );
        $this->customerSession->setCustomerOrganizationName($organization);
        if ($userType == 'PROVEEDOR' && $customerId) {
            $sellerId = $this->helperVendor->getVendorId($companyId, $agreementId);
            if (!$sellerId) {
                $jsonResult = $this->resultJsonFactory->create();
                $errorArr = ['Error' => "Company $companyId doesn't exists in Magento"];
                $jsonResult->setData($errorArr);
                return $jsonResult;
            }

            $this->helperVendor->createVendor($sellerId, $customerId, $agreementId);

            $_urlVendorAgreement = '';


            if ($agreementId == self::ID_AGREEMENT_COMPUTERS) {
                $_urlVendorAgreement .= 'computadores';
            }

            if ($agreementId == self::ID_AGREEMENT_COMPUTERS_2021_01) {
                $_urlVendorAgreement .= 'computadores202101';
            }

            if ($agreementId == self::ID_AGREEMENT_COMPUTERS_2021_12) {
                $_urlVendorAgreement .= 'computadores202201';
            }

            if ($agreementId == self::ID_AGREEMENT_FURNITURE) {
                $_urlVendorAgreement .= 'mobiliario';
            }

            if ($agreementId == self::ID_AGREEMENT_ASEO) {
                $_urlVendorAgreement .=  'aseo';
            }

            if ($agreementId == self::ID_AGREEMENT_EMERGENCY) {
                $_urlVendorAgreement .=  'emergencias';
            }

            if ($agreementId == self::ID_AGREEMENT_EMERGENCY_2021_09) {
                $_urlVendorAgreement .= ConstantsEmergency202109::WEBSITE_CODE;
            }

            if ($agreementId == self::ID_AGREEMENT_FUELS) {
                $_urlVendorAgreement .=  'combustibles';
            }

            if ($agreementId == self::ID_AGREEMENT_COMBUSTIBLES202110) {
                $_urlVendorAgreement .=  'combustibles202110';
            }

            if ($agreementId == self::ID_AGREEMENT_VOUCHER) {
                $_urlVendorAgreement .= 'voucher';
            }
            if ($agreementId == self::ID_AGREEMENT_VOUCHER_2021_06) {
                $_urlVendorAgreement .= 'voucher202106';
            }

            if ($agreementId == self::ID_AGREEMENT_SOFTWARE) {
                $_urlVendorAgreement .=  'software';
            }

            if ($agreementId == Helper::ID_AGREEMENT_DESKITEMS) {
                $_urlVendorAgreement .= 'escritorio';
            }

            if ($agreementId == Helper::ID_AGREEMENT_FOODS) {
                $_urlVendorAgreement .= 'alimentos';
            }

			if($agreementId == self::ID_AGREEMENT_GAS) {
				$_urlVendorAgreement .= 'gas';
			}

			if($agreementId == self::ID_AGREEMENT_SOFTWARE_2022){
			    $_urlVendorAgreement .= \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_SOFTWARE_2022;
            }

            if ($agreementId == \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_CARS) {
                $_urlVendorAgreement .= \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_CARS;
            }

            if ($agreementId == self::ID_AGREEMENT_ASEO_2022) {
                $_urlVendorAgreement .= \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_ASEO_2022;
            }

            if ($agreementId == \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_MED_SUPPLIES) {
                $_urlVendorAgreement .= \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_MED_SUPPLIES;
            }

			$_urlVendorAgreement .=  '/marketplace/account/dashboard/';
            $_urlRedirect = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_WEB) . $_urlVendorAgreement;

            $resultRedirectFactory->setUrl($_urlRedirect);
        } else {
            $_urlRedirect = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
            $convenio = '';
            if ($agreementId == self::ID_AGREEMENT_ASEO) {
                $_urlRedirect = $_urlRedirect . 'aseo';
                $convenio = 'aseo';
            }

            if ($agreementId == self::ID_AGREEMENT_EMERGENCY) {
                $_urlRedirect = $_urlRedirect . 'emergencias';
                $convenio = 'emergencias';
            }

            if ($agreementId == self::ID_AGREEMENT_EMERGENCY_2021_09) {
                $_urlRedirect = $_urlRedirect . ConstantsEmergency202109::WEBSITE_CODE;
                $convenio = ConstantsEmergency202109::WEBSITE_CODE;
            }

            if($agreementId == Helper::ID_AGREEMENT_COMPUTERS){
                $_urlRedirect = $_urlRedirect . 'computadores';
                $convenio = 'computadores';
            }

            if($agreementId == Helper::ID_AGREEMENT_COMPUTERS_2021_01){
                $_urlRedirect = $_urlRedirect . 'computadores202101';
                $convenio = 'computadores202101';
            }

            if($agreementId == Helper::ID_AGREEMENT_COMPUTERS_2021_12){
                $_urlRedirect = $_urlRedirect . 'computadores202201';
                $convenio = 'computadores202201';
            }

            if ($agreementId == self::ID_AGREEMENT_FUELS) {
                $_urlRedirect = $_urlRedirect . 'combustibles';
                $convenio = 'combustibles';
            }

            if ($agreementId == self::ID_AGREEMENT_COMBUSTIBLES202110) {
                $_urlRedirect = $_urlRedirect . 'combustibles202110';
                $convenio = 'combustibles202110';
            }

            if($agreementId == Helper::ID_AGREEMENT_FURNITURE) {
                $_urlRedirect = $_urlRedirect . 'mobiliario';
                $convenio = 'mobiliario';
            }

            if ($agreementId == self::ID_AGREEMENT_VOUCHER) {
                $_urlRedirect = $_urlRedirect . 'voucher';
                $convenio = 'voucher';
            }

            if ($agreementId == self::ID_AGREEMENT_VOUCHER_2021_06) {
                $_urlRedirect = $_urlRedirect . 'voucher202106';
                $convenio = 'voucher202106';
            }

            if ($agreementId == self::ID_AGREEMENT_SOFTWARE) {
                $_urlRedirect = $_urlRedirect . 'software';
                $convenio = 'software';
            }

            if($agreementId == Helper::ID_AGREEMENT_DESKITEMS) {
                $_urlRedirect = $_urlRedirect .  'escritorio';
                $convenio = 'escritorio';
            }

            if ($agreementId == Helper::ID_AGREEMENT_FOODS) {
                $_urlRedirect = $_urlRedirect .  'alimentos';
                $convenio = 'alimentos';
            }

			if ($agreementId == self::ID_AGREEMENT_GAS) {
				$_urlRedirect = $_urlRedirect . 'gas';
				$convenio = 'gas';
			}

			if ($agreementId == self::ID_AGREEMENT_SOFTWARE_2022){
			    $_urlRedirect = $_urlRedirect . \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_SOFTWARE_2022;
			    $convenio = \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_SOFTWARE_2022;
            }

            if ($agreementId == self::ID_AGREEMENT_ASEO_2022){
                $_urlRedirect = $_urlRedirect . \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_ASEO_2022;
                $convenio = \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_ASEO_2022;
            }

            if ($agreementId == \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_CARS) {
                $_urlRedirect = $_urlRedirect .  \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_CARS;
                $convenio = \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_CARS;
            }

            if ($agreementId == \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_MED_SUPPLIES) {
                $_urlRedirect = $_urlRedirect .  \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_MED_SUPPLIES;
                $convenio = \Chilecompra\LoginMp\Helper\Helper::CODE_STORE_AGREEMENT_MED_SUPPLIES;
            }

            $customerIp = $this->getCustomerIp();

            if ($this->customerData->isModuleEnable($convenio)) {
                if ($this->customerData->shouldRenderData($customerIp, $customerId, $convenio)) {
                    $this->setAbTester(1, $customerId);
                } else {
                    $this->setAbTester(0, $customerId);
                }
            }

            $resultRedirectFactory->setUrl($_urlRedirect);
        }

        return $resultRedirectFactory;
    }

    private function setAbTester($value, $customerId)
    {
        $customer = $this->customerModel->load($customerId);
        $customerData = $customer->getDataModel();
        $customerData->setCustomAttribute('abtester', $value);
        $customer->updateData($customerData);
        $customerResource = $this->customerFactory->create();
        $customerResource->saveAttribute($customer, 'abtester');
    }


    private function getCustomerIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    /**
     * Gets shipping address from service's response from MP.
     *
     * @param array $userData user information.
     *
     * @return array $address Shipping address.
     */
    private function getAddressDCCPInfo($userData)
    {
        $address = null;
        $_shippingAddress = null;

        if ($this->keyValidInArray('organizacion', $userData)) {
            if ($this->keyValidInArray('direccionDespacho', $userData['organizacion'])) {

                $_shippingAddress = $userData['organizacion']['direccionDespacho'];

                if (
                    $this->keyValidInArray('region', $_shippingAddress) &&
                    $this->keyValidInArray('comuna', $_shippingAddress) &&
                    $this->keyValidInArray('direccion', $_shippingAddress)
                ) {
                    $address = [
                        'region' => $_shippingAddress['region'],
                        'comuna' => $_shippingAddress['comuna'],
                        'direccion' => $_shippingAddress['direccion']
                    ];
                }
            }
        }

        return $address;
    }

    /**
     * Checks if a key is valid in array.
     *
     * @param string $_key to find in array.
     * @param array $_arr user-organization information.
     *
     * @return boolean
     */
    private function keyValidInArray($_key, $_arr)
    {
        return (array_key_exists($_key, $_arr) && isset($_arr[$_key]));
    }

    /**
     * Fixes cypher key length.
     *
     * @param string $key Cipher Key.
     *
     * @return string $key Fixed Cipher Key.
     */
    private function _fixKey($key)
    {
        $cipherKeyLength = 16;

        if (strlen($key) < $cipherKeyLength) {
            return str_pad("$key", $cipherKeyLength, " ");
        }

        if (strlen($key) > $cipherKeyLength) {
            return substr($key, 0, $cipherKeyLength);
        }
        return $key;
    }

    /**
     * Decrypts string.
     *
     * @param string $encrypted Encrypted data.
     *
     * @return string $decryptedData Decryption Result.
     */
    private function _decrypt($encrypted)
    {
        $openSSLCipherName = "AES-128-CBC";

        $ivKey = $this->_fixKey("chilecompra");
        $key = $this->_fixKey("dccp");

        $decryptedData = openssl_decrypt(
            base64_decode($encrypted),
            $openSSLCipherName,
            $key,
            OPENSSL_RAW_DATA,
            $ivKey
        );
        return $decryptedData;
    }

    /**
     * Gets user information from DCCP WS.
     *
     * @param string $token User Token.
     *
     * @return array $response Server Response Payload.
     */
    private function _getUserData($token)
    {
        $endpoint = rtrim(
            $this->scopeConfig->getValue(
                'dccp_login/endpoint_user_data/endpoint',
                ScopeInterface::SCOPE_STORE
            ),
            '/'
        );

        if (empty($endpoint)) {
            return ['Error' => "Empty user endpoint.
                Go to Stores->Configuration->Endpoints DCCP Login"];
        }

        if (filter_var($endpoint, FILTER_VALIDATE_URL) === false) {
            return ['Error' => "Wrong endpoint. Not a valid URL.
                Go to Stores->Configuration->Endpoints DCCP Login"];
        }

        $client = $this->httpClientFactory->create();
        $client->setUri($endpoint);
        $client->setMethod(Zend_Http_Client::GET);
        $client->setHeaders(Zend_Http_Client::CONTENT_TYPE, 'application/json');
        $client->setHeaders('Accept', 'application/json');
        $client->setHeaders("Authorization", "Bearer {$token}");
        $response = json_decode($client->request()->getBody(), true);

        if (isset($response['payload']) && $response['success'] == 'OK') {
            return $response['payload'];
        }

        $context = [
            "endpoint" => $endpoint,
            "token" => $token,
            "response" => $response
        ];
        $this->logger->error('Lamada a servicio de login fallida', $context);
        return ['Error' => 'Invalid token or server response'];
    }
}
