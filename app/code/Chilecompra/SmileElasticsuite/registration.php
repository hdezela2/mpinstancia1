<?php
/**
 * Chilecompra.
 *
 * @category  Chilecompra
 * @package   Chilecompra_SmileElasticsuite
 * @author    Chilecompra
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Chilecompra_SmileElasticsuite',
    __DIR__
);