<?php
namespace Chilecompra\SmileElasticsuite\Block\Navigation\Renderer;

use Smile\ElasticsuiteCatalog\Block\Navigation\Renderer\Attribute as SmileAttribute;

class Attribute extends SmileAttribute
{
    const JS_COMPONENT = 'Chilecompra_SmileElasticsuite/js/attribute-filter';

    /**
     * {@inheritDoc}
     */
    public function getJsLayout()
    {
        $filterItems    = $this->getFilter()->getItems();

        $jsLayoutConfig = [
            'component'           => static::JS_COMPONENT,
            'maxSize'             => (int) $this->getFilter()->getAttributeModel()->getFacetMaxSize(),
            'displayProductCount' => (bool) $this->displayProductCount(),
            'hasMoreItems'        => (bool) $this->getFilter()->hasMoreItems(),
            'ajaxLoadUrl'         => $this->getAjaxLoadUrl(),
        ];

        foreach ($filterItems as $item) {
            $jsLayoutConfig['items'][] = $item->toArray(['label', 'count', 'url', 'is_selected']);
        }

        return json_encode($jsLayoutConfig);
    }

    /**
     * Get the AJAX load URL (used by the show more and the search features).
     *
     * @return string
     */
    private function getAjaxLoadUrl()
    {
        $qsParams = ['filterName' => $this->getFilter()->getRequestVar()];

        $currentCategory = $this->getFilter()->getLayer()->getCurrentCategory();

        if ($currentCategory && $currentCategory->getId() && $currentCategory->getLevel() > 1) {
            $qsParams['cat'] = $currentCategory->getId();
        }

        $urlParams = ['_current' => true, '_query' => $qsParams];

        return $this->_urlBuilder->getUrl('catalog/navigation_filter/ajax', $urlParams);
    }
}
