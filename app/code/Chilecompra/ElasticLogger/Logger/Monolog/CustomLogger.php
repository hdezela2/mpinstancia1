<?php
/**
 * This file is part of the ElasticLogger Module.
 * Intercepts Magento\Framework\Logger\Monolog
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  ElasticLogger
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

namespace Chilecompra\ElasticLogger\Logger\Monolog;

use Monolog\Handler\ElasticSearchHandler;
use Magento\Framework\Logger\Monolog;
use Elastica\Client;

/**
 * ElasticLogger CustomLogger Interceptor.
 *
 * @category Chilecompra
 * @package  ElasticLogger
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */
class CustomLogger extends Monolog//Logger
{
    private $_helper;

    /**
     * Load dependencies and calls parent constructor with given parameters
     *
     * @param Helper             $helper     Module configuration helper
     * @param string             $name       The logging channel
     * @param HandlerInterface[] $handlers   Optional stack of handlers.
     * @param callable[]         $processors Optional array of processors
     */
    public function __construct(
        \Chilecompra\ElasticLogger\Helper\Data $helper,
        $name = '',
        array $handlers = [],
        array $processors = []
    ) {
        /**
         * TODO: This should be eliminated with MAGETWO-53989
         */
        $this->name = $name;
        $handlers = array_values($handlers);

        $this->_helper = $helper;
        parent::__construct($this->name, $handlers, $processors);
    }

    /**
     * Adds a log record.
     *
     * @param integer $level   The logging level
     * @param string  $message The log message
     * @param array   $context The log context
     *
     * @return Boolean Whether the record has been processed
     */
    public function addRecord($level, $message, array $context = [])
    {
        /**
         * To preserve compatibility with Exception messages.
         * And support PSR-3 context standard.
         *
         * @link http://www.php-fig.org/psr/psr-3/#context PSR-3 context standard
         */
        if ($message instanceof \Exception && !isset($context['exception'])) {
            $context['exception'] = $message;
        }

        $message = $message instanceof \Exception
            ? $message->getMessage()
            : $message;
        if (isset($context['elasticsearch'])) {
            unset($context['elasticsearch']);
            $this->addElasticHandler($level);
        }

        return parent::addRecord($level, $message, $context);
    }

    /**
     * Adds Elasticsearch handler when conditions are met
     *
     * @param string $level Level number
     *
     * @return void
     */
    public function addElasticHandler($level)
    {
        if ($this->_helper->isActive() == 1) {
            $logLevels = $this->_helper->getLogLevels();

            if (in_array($level, $logLevels)) {
                $handlerPushed = false;
                foreach ($this->getHandlers() as $dahandler) {
                    $handlerClass = get_class($dahandler);
                    if(strpos($handlerClass, 'ElasticSearchHandler') !== false){
                        $handlerPushed = true;
                        break;
                    }
                }

                if (!$handlerPushed) {
                    $clientParams = [
                        'transport' => $this->_helper->getTransport(),
                        'host' => $this->_helper->getHost(),
                        'port' => $this->_helper->getPort(),
                    ];

                    $client = new Client($clientParams);
                    $options = [
                        'index' => $this->_helper->getIndex(),
                        'type'  => 'elastic_doc_type',
                    ];

                    $handler = new ElasticSearchHandler($client, $options);
                    $this->pushHandler($handler);
                }
            }
        }
    }

}
