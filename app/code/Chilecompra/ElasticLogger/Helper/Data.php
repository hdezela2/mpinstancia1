<?php
/**
 * This file is part of the ElasticLogger Module.
 * Get backoffice configurations related to ElasticLogger Module.
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  ElasticLogger
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

namespace Chilecompra\ElasticLogger\Helper;

/**
 * ElasticLogger helper
 *
 * It contains a stack of functions for getting
 * backoffice configurations.
 *
 * @category Chilecompra
 * @package  ElasticLogger
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */
class Data
{
    protected $scopeConfig;

    /**
     * Load dependencies.
     *
     * @param ScopeConfigInterface $scopeConfig For getting backoffice params
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get module availability
     *
     * @return string
     */
    public function isActive()
    {
        $isActive = $this->scopeConfig->getValue(
            "dccp_logger/server_config_data/active",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $isActive;
    }

    /**
     * Get Elasticsearch Transport
     *
     * @return string Server Tansport (before ://)
     */
    public function getTransport()
    {
        $hostUrl = $this->scopeConfig->getValue(
            "dccp_logger/server_config_data/host",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return substr($hostUrl, 0, strpos($hostUrl, ':'));
    }

    /**
     * Get Elasticsearch host
     *
     * @return string Server Host (after ://)
     */
    public function getHost()
    {
        $hostUrl = $this->scopeConfig->getValue(
            "dccp_logger/server_config_data/host",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return substr($hostUrl, strpos($hostUrl, '//') + 2, strlen($hostUrl));
    }

    /**
     * Get Elasticsearch server port
     *
     * @return string Server Port
     */
    public function getPort()
    {
        $port = $this->scopeConfig->getValue(
            "dccp_logger/server_config_data/port",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $port;
    }

    /**
     * Get Elasticsearch collection name (index)
     *
     * @return string
     */
    public function getIndex()
    {
        $index = $this->scopeConfig->getValue(
            "dccp_logger/server_config_data/index",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $index;
    }

    /**
     * Get level selection list to log
     *
     * @return array Selected log levels
     */
    public function getLogLevels()
    {
        $list = $this->scopeConfig->getValue(
            "dccp_logger/server_config_data/list",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $list !== null ? explode(',', $list) : [];
    }
}

?>