<?php
/**
 * This file is part of the ElasticLogger Module.
 * Log levels source.
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  ElasticLogger
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

namespace Chilecompra\ElasticLogger\Model\Adminhtml\System\Config\Source\Logs;

/**
 * ElasticLogger log levels source.
 *
 * @category Chilecompra
 * @package  ElasticLogger
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */
class Level implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Detailed debug information
     */
    const DEBUG = 100;

    /**
     * Interesting events
     *
     * Examples: User logs in, SQL logs.
     */
    const INFO = 200;

    /**
     * Uncommon events
     */
    const NOTICE = 250;

    /**
     * Exceptional occurrences that are not errors
     *
     * Examples: Use of deprecated APIs, poor use of an API,
     * undesirable things that are not necessarily wrong.
     */
    const WARNING = 300;

    /**
     * Runtime errors
     */
    const ERROR = 400;

    /**
     * Critical conditions
     *
     * Example: Application component unavailable, unexpected exception.
     */
    const CRITICAL = 500;

    /**
     * Action must be taken immediately
     *
     * Example: Entire website down, database unavailable, etc.
     * This should trigger the SMS alerts and wake you up.
     */
    const ALERT = 550;

    /**
     * Urgent alert.
     */
    const EMERGENCY = 600;

    /**
     * Get error levels label/value options list 
     *
     * @return array options list
     */
    public function toOptionArray()
    {
        $attributesArrays = [
            [
                'label' => 'DEBUG',
                'value' => self::DEBUG
            ],
            [
                'label' => 'INFO',
                'value' => self::INFO
            ],
            [
                'label' => 'NOTICE',
                'value' => self::NOTICE
            ],
            [
                'label' => 'WARNING',
                'value' => self::WARNING
            ],
            [
                'label' => 'ERROR',
                'value' => self::ERROR
            ],
            [
                'label' => 'CRITICAL',
                'value' => self::CRITICAL
            ],
            [
                'label' => 'ALERT',
                'value' => self::ALERT
            ],
            [
                'label' => 'EMERGENCY',
                'value' => self::EMERGENCY
            ]
        ];

        return $attributesArrays;
    }
}