<?php
/**
 * This file is part of the ElasticLogger Module.
 * Registration file.
 * php version 7.2.19
 *
 * @category Chilecompra
 * @package  ElasticLogger
 * @author   José Santos <jose.santos@chilecompra.cl>
 * @license  DCCP 2019 - All rights reserved
 * @link     https://www.chilecompra.cl
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Chilecompra_ElasticLogger',
    __DIR__
);
