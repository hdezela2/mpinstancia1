<?php
declare(strict_types=1);

namespace Chilecompra\CombustiblesRenewal\Model;

class Constants
{
    const ID_AGREEMENT_COMBUSTIBLES_2021_10 = 5802326;
    const WEBSITE_NAME = 'Combustibles 202110 Website';
    const WEBSITE_CODE = 'combustibles202110';
    const STORE_NAME = 'Combustibles 202110 Store';
    const STORE_CODE = 'combustibles202110';
    const STORE_VIEW_NAME = 'Combustibles 202110';
    const STORE_VIEW_CODE = 'combustibles202110';
    const ROOT_CATEGORY_NAME = 'DCCP Combustibles202110';
    const FRONT_THEME = 'Summa/Combustibles';
    const FRONT_THEME_DEFAULT = 'Formax/ChileCompra';
    const XML_PATH_REVIEW_ACTIVE = 'catalog/review/active';
    const XML_PATH_REVIEW_ALLOW_GUEST = 'catalog/review/allow_guest';
    const XML_PATH_NUMBER_OF_DAYS = 'stock_management_configuration/general/number_of_days';
    const XML_PATH_STOCK_MANAGEMENT_ENABLED = 'stock_management_configuration/general/enable';
}