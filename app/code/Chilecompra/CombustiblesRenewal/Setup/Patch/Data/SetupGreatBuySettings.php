<?php
declare(strict_types=1);

namespace Chilecompra\CombustiblesRenewal\Setup\Patch\Data;

use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Chilecompra\CombustiblesRenewal\Model\Constants;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class SetupGreatBuySettings implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var ConfigInterface
     */
    private $configInterface;

    /**
     * @var WebsiteRepositoryInterface
     */
    private $websiteRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ConfigInterface $configInterface
     * @param WebsiteRepositoryInterface $websiteRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        ConfigInterface $configInterface,
        WebsiteRepositoryInterface $websiteRepository,
        LoggerInterface $logger
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->configInterface = $configInterface;
        $this->websiteRepository = $websiteRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        try {
            $website = $this->websiteRepository->get(Constants::WEBSITE_CODE);
            $configs = [
                'dccp_endpoint/greatbuy/minimumutm' => 0,
                'dccp_endpoint/greatbuy/utmforgb' => 1000,
                'dccp_endpoint/greatbuy/maximumutm' => 15000

            ];
            foreach ($configs as $config=>$value) {
                $this->configInterface->saveConfig(
                    $config,
                    $value,
                    ScopeInterface::SCOPE_WEBSITES,
                    $website->getId()
                );
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        $this->moduleDataSetup->endSetup();

        return $this;
    }

    /**
     * @return array
     */
    public static function getDependencies(): array
    {
        return [CombustiblesRenewalSetup::class];
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }
}
