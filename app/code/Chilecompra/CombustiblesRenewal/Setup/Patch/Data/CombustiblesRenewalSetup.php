<?php
namespace Chilecompra\CombustiblesRenewal\Setup\Patch\Data;

use Chilecompra\CombustiblesRenewal\Model\Constants;
use Exception;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group as GroupResource;
use Magento\Store\Model\ResourceModel\Website as WebsiteResource;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory as ThemeCollectionFactory;
use Magento\Theme\Model\Theme;

class CombustiblesRenewalSetup implements DataPatchInterface
{
    /** @var WebsiteFactory */
    private $websiteFactory;

    /** @var WebsiteResource */
    private $websiteResourceModel;

    /** @var StoreFactory */
    private $storeFactory;

    /** @var GroupFactory */
    private $groupFactory;

    /** @var GroupResource */
    private $groupResourceModel;

    /** @var CategoryFactory */
    private $categoryFactory;

    /** @var ConfigInterface */
    private $configInterface;

    /** @var ThemeCollectionFactory */
    private $themeCollectionFactory;

    /**
     * @param WebsiteFactory $websiteFactory
     * @param WebsiteResource $websiteResourceModel
     * @param GroupResource $groupResourceModel
     * @param StoreFactory $storeFactory
     * @param GroupFactory $groupFactory
     * @param CategoryFactory $categoryFactory
     * @param ConfigInterface $configInterface
     * @param ThemeCollectionFactory $themeCollectionFactory
     */
    public function __construct(
        WebsiteFactory $websiteFactory,
        WebsiteResource $websiteResourceModel,
        GroupResource $groupResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory,
        CategoryFactory $categoryFactory,
        ConfigInterface $configInterface,
        ThemeCollectionFactory $themeCollectionFactory
    ) {
        $this->websiteFactory = $websiteFactory;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->storeFactory = $storeFactory;
        $this->groupFactory = $groupFactory;
        $this->groupResourceModel = $groupResourceModel;
        $this->categoryFactory = $categoryFactory;
        $this->configInterface = $configInterface;
        $this->themeCollectionFactory = $themeCollectionFactory;
    }

    /**
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws Exception
     */
    public function apply()
    {
        ##### CREATE WEBSITE #####
        $website = $this->websiteFactory->create();
        $website->load(Constants::WEBSITE_CODE);
        if(!$website->getId()){
            $website->setCode(Constants::WEBSITE_CODE);
            $website->setName(Constants::WEBSITE_NAME);
            $this->websiteResourceModel->save($website);
        }

        ##### CREATE ROOT CATEGORY #####
        $category = $this->createOrUpdateRootCategory();

        ##### CREATE STORE ######
        if($website->getId()){
            $group = $this->groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName(Constants::STORE_NAME);
            $group->setCode(Constants::STORE_CODE);
            $group->setRootCategoryId($category->getId());
            $group->setDefaultStoreId(0);
            $this->groupResourceModel->save($group);
        }

        ##### CREATE STORE VIEW #####
        $store = $this->storeFactory->create();
        $store->load(Constants::STORE_VIEW_CODE);
        if(!$store->getId()){
            $group = $this->groupFactory->create();
            $group->load(Constants::STORE_NAME, 'name');
            $store->setCode(Constants::STORE_VIEW_CODE);
            $store->setName(Constants::STORE_VIEW_NAME);
            $store->setWebsiteId($website->getId());
            $store->setGroupId($group->getId());
            $store->setIsActive(1);
            $store->save();
        }

        ##### ASSIGN THEME TO STORE VIEW #####
        $themes = $this->themeCollectionFactory->create()->loadRegisteredThemes();
        /** @var Theme $theme */
        foreach ($themes as $theme) {
            if ($theme->getArea() == 'frontend' && $theme->getCode() == Constants::FRONT_THEME) {
                $this->configInterface->saveConfig('design/theme/theme_id', $theme->getId(), 'stores', $store->getId());
            }
        }

        return $this;
    }

    public function getAliases(): array
    {
        return [];
    }

    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return Category
     * @throws Exception
     */
    private function createOrUpdateRootCategory(): Category
    {
        $category = $this->categoryFactory->create();
        $category->setName(Constants::ROOT_CATEGORY_NAME);
        $category->setIsActive(true);
        $category->setStoreId(0);
        $parentCategory = $this->categoryFactory->create();
        $parentCategory->load(Category::TREE_ROOT_ID);
        $category->setDisplayMode(Category::DM_PRODUCT);
        $category->setPath($parentCategory->getPath());
        $category->save();
        return $category;
    }
}
