<?php
declare(strict_types=1);

namespace Chilecompra\CombustiblesRenewal\Setup\Patch\Data;

use Formax\GreatBuy\Helper\Data as HelperData;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Chilecompra\CombustiblesRenewal\Model\Constants;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class RequestQuoteDisableInstaller implements DataPatchInterface
{
	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;

	/**
	 * @var ConfigInterface
	 */
	private $configInterface;

	/**
	 * @var WebsiteRepositoryInterface
	 */
	private $websiteRepository;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @param ModuleDataSetupInterface $moduleDataSetup
	 * @param ConfigInterface $configInterface
	 * @param WebsiteRepositoryInterface $websiteRepository
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		ConfigInterface $configInterface,
		WebsiteRepositoryInterface $websiteRepository,
		LoggerInterface $logger
	) {
		$this->moduleDataSetup = $moduleDataSetup;
		$this->configInterface = $configInterface;
		$this->websiteRepository = $websiteRepository;
		$this->logger = $logger;
	}

	/**
	 * @inheritDoc
	 */
	public function apply()
	{
		$this->moduleDataSetup->startSetup();

		try {
			$website = $this->websiteRepository->get(Constants::WEBSITE_CODE);
			$this->configInterface->saveConfig(
				'requestforquote/requestforquote_settings/requestforquote_account',
				'0',
				ScopeInterface::SCOPE_WEBSITES,
				$website->getId()
			);
		} catch (\Exception $exception) {
			$this->logger->error($exception->getMessage());
		}

		$this->moduleDataSetup->endSetup();

        return $this;
	}

	/**
	 * @return array
	 */
	public static function getDependencies(): array
	{
		return [CombustiblesRenewalSetup::class];
	}

	/**
	 * @return array
	 */
	public function getAliases(): array
	{
		return [];
	}
}
