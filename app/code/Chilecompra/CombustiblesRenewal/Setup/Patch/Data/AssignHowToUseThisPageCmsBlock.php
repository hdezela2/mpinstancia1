<?php
namespace Chilecompra\CombustiblesRenewal\Setup\Patch\Data;

use Chilecompra\CombustiblesRenewal\Model\Constants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\StoreFactory;
use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Api\BlockRepositoryInterface;

class AssignHowToUseThisPageCmsBlock implements DataPatchInterface
{
    /** @var StoreFactory */
    private $storeFactory;

    /** @var BlockFactory */
    private $blockFactory;

    /** @var BlockRepositoryInterface */
    private $blockrepository;

    /**
     * @param StoreFactory $storeFactory
     * @param BlockFactory $blockFactory
     * @param BlockRepositoryInterface $blockRepository
     */
    public function __construct(
        StoreFactory $storeFactory,
        BlockFactory $blockFactory,
        BlockRepositoryInterface $blockRepository
    ) {
        $this->storeFactory = $storeFactory;
        $this->blockFactory = $blockFactory;
        $this->blockrepository = $blockRepository;
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $block = $this->blockFactory->create()->load('how-to-use-this', 'identifier');
        if ($block->getId()) {
            $currentStoreId = $block->getStoreId();
            $store = $this->storeFactory->create();
            $store->load(Constants::STORE_VIEW_CODE);
            if ($store->getId()) {
                $currentStoreId[] = $store->getId();
                $block->setStoreId($currentStoreId);
                $block->setStores($currentStoreId);
                $this->blockrepository->save($block);
            }
        }
    }

    public function getAliases(): array
    {
        return [];
    }

    public static function getDependencies(): array
    {
        return [
            CombustiblesRenewalSetup::class
        ];
    }
}