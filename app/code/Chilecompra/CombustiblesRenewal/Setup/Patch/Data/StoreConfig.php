<?php
namespace Chilecompra\CombustiblesRenewal\Setup\Patch\Data;

use Chilecompra\CombustiblesRenewal\Model\Constants;
use MageMoto\ElasticsearchStoreSwitch\Block\Store as StoreBlock;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class StoreConfig implements DataPatchInterface
{
    /** @var StoreFactory */
    private $storeFactory;

    /** @var WriterInterface */
    private $configWriter;

    /** @var ScopeConfigInterface */
    private $scopeConfig;

    /**
     * @param StoreFactory $storeFactory
     * @param WriterInterface $configWriter
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        StoreFactory $storeFactory,
        WriterInterface $configWriter,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(Constants::STORE_VIEW_CODE);
        if ($store->getId()) {
            /** Add store to search box on front */
            $data = $this->scopeConfig->getValue(StoreBlock::XML_PATH_EMAIL_RECIPIENT);
            $arrayValue = $data ? explode(',', $data) : [];
            $arrayValue[] = $store->getId();
            $strValue = implode(',', $arrayValue);
            $this->configWriter->save(StoreBlock::XML_PATH_EMAIL_RECIPIENT, $strValue);

            /** Disable reviews for product on front */
            $websiteId = $store->getWebsiteId();
            $scope = ScopeInterface::SCOPE_WEBSITES;
            $this->configWriter->save(Constants::XML_PATH_REVIEW_ACTIVE, 0, $scope, $websiteId);
            $this->configWriter->save(Constants::XML_PATH_REVIEW_ALLOW_GUEST, 0, $scope, $websiteId);
        }
    }

    public function getAliases(): array
    {
        return [];
    }

    public static function getDependencies(): array
    {
        return [
            CombustiblesRenewalSetup::class
        ];
    }
}