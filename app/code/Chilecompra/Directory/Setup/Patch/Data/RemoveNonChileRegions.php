<?php

namespace Chilecompra\Directory\Setup\Patch\Data;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * This patch removes the non Chile regions from the DB Tables
 * directory_country_region and directory_country_region_name
 * See: https://chilecompra.atlassian.net/browse/MU24-50?focusedCommentId=19465
 */
class RemoveNonChileRegions implements DataPatchInterface
{
    private const COUNTRY_REGION_TABLE_NAME = 'directory_country_region';
    private const COUNTRY_REGION_NAME_TABLE_NAME = 'directory_country_region_name';

    private ResourceConnection $resourceConnection;

    public function __construct(ResourceConnection $resourceConnection) {
        $this->resourceConnection = $resourceConnection;
    }

    public static function getDependencies(): array
    {
        return [];
    }

    public function getAliases(): array
    {
        return [];
    }

    public function apply(): RemoveNonChileRegions
    {
        $this->deleteCountryRegion();
        $this->deleteCountryRegionName();

        return $this;
    }

    private function deleteCountryRegion()
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $connection->getTableName(self::COUNTRY_REGION_TABLE_NAME);

        $whereConditions = $connection->quoteInto('country_id != ?', 'CL')
            . " OR "
            . $connection->quoteInto('code LIKE ?', '%CL-%');

        $connection->delete($tableName, $whereConditions);
    }

    private function deleteCountryRegionName()
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $connection->getTableName(self::COUNTRY_REGION_NAME_TABLE_NAME);

        $whereCondition = $connection->quoteInto('locale != ?', 'es_CL');
        $connection->delete($tableName, $whereCondition);
    }
}
