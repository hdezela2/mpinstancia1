<?php

namespace Chilecompra\Logging\Model;

use Formax\Offer\Model\Offer;
use Formax\RegionalCondition\Model\RegionalConditionSeller;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\MessageInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Logging\Model\Event;
use Magento\Logging\Model\Handler\Controllers as ControllersLoggingHandler;
use Magento\Logging\Model\Handler\ControllersFactory as LoggingHandlerFactory;
use Webkul\MpAssignProduct\Model\Items;

class Processor extends \Magento\Logging\Model\Processor
{
    /**
     * @var CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Serializer Instance
     *
     * @var Json
     */
    protected $json;

    /**
     * Logger model
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $_actionLogger;

    /**
     * Constructor: initialize configuration model, controller and model handler
     *
     * @param \Magento\Logging\Model\Config $config
     * @param \Magento\Logging\Model\Handler\Models $modelsHandler
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param LoggingHandlerFactory $handlerControllersFactory
     * @param \Magento\Logging\Model\EventFactory $eventFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress
     * @param \Magento\Logging\Model\Event\ChangesFactory $changesFactory
     * @param CustomerRegistry $customerRegistry
     * @param ProductRepositoryInterface $productRepository
     * @param Json $json
     * @param \Psr\Log\LoggerInterface $actionLogger
     * @param ControllersLoggingHandler $controllersHandler
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Logging\Model\Config $config,
        \Magento\Logging\Model\Handler\Models $modelsHandler,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Psr\Log\LoggerInterface $logger,
        LoggingHandlerFactory $handlerControllersFactory,
        \Magento\Logging\Model\EventFactory $eventFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \Magento\Logging\Model\Event\ChangesFactory $changesFactory,
        CustomerRegistry $customerRegistry,
        ProductRepositoryInterface $productRepository,
        Json $json,
        \Psr\Log\LoggerInterface $actionLogger,
        ControllersLoggingHandler $controllersHandler = null
    ) {
        parent::__construct(
            $config,
            $modelsHandler,
            $authSession,
            $messageManager,
            $objectManager,
            $logger,
            $handlerControllersFactory,
            $eventFactory,
            $request,
            $remoteAddress,
            $changesFactory,
            $controllersHandler
        );

        $this->customerRegistry = $customerRegistry;
        $this->productRepository = $productRepository;
        $this->json = $json;
        $this->_actionLogger = $actionLogger;
    }

    /**
     * Action model processing.
     *
     * Get difference between data & orig_data and store in the internal modelsHandler container.
     *
     * @param AbstractModel $model
     * @param string $action
     * @return $this|bool
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function modelActionAfter($model, $action)
    {
        if ($this->_skipNextAction) {
            return false;
        }
        /**
         * These models used when we merge action models with action group models
         */
        $groupExpectedModels = null;
        if ($this->_eventConfig) {
            $eventGroupNode = $this->_config->getEventGroupConfig($this->_eventConfig['group_name']);
            if (isset($eventGroupNode['expected_models'])) {
                $groupExpectedModels = $eventGroupNode['expected_models'];
            }
        }

        /**
         * Exact models in exactly action node
         */
        $expectedModels = isset(
            $this->_eventConfig['expected_models']
        ) ? $this->_eventConfig['expected_models'] : false;

        if (!$expectedModels || empty($expectedModels)) {
            if (empty($groupExpectedModels)) {
                return false;
            }
            $usedModels = $groupExpectedModels;
        } else {
            if (isset(
                    $expectedModels['@']
                ) && isset(
                    $expectedModels['@']['extends']
                ) && $expectedModels['@']['extends'] == 'merge'
            ) {
                $groupExpectedModels = array_replace_recursive($groupExpectedModels, $expectedModels);
                $usedModels = $groupExpectedModels;
            } else {
                $usedModels = $expectedModels;
            }
        }

        $additionalData = $skipData = [];
        /**
         * Log event changes for each model
         */
        foreach ($usedModels as $className => $params) {
            /**
             * Add custom skip fields per expecetd model
             */
            if (isset($params['skip_data'])) {
                $skipData = array_unique($params['skip_data']);
            }

            /**
             * Add custom additional fields per expecetd model
             */
            if (isset($params['additional_data'])) {
                $additionalData = array_unique($params['additional_data']);
            }
            /**
             * Clean up additional data with skip data
             */
            $additionalData = array_diff($additionalData, $skipData);

            /** START Fix for additional model logs not saved */
            if (!$model instanceof $className) {
                continue;
                //return false;
            }
            /** END Fix for additional model logs not saved */

            $callback = sprintf('model%sAfter', ucfirst($action));
            $this->collectAdditionalData($model, $additionalData);
            $changes = $this->_modelsHandler->{$callback}($model, $this);

            /* $changes will not be an object in case of view action */
            if (!is_object($changes)) {
                return $this;
            }
            $changes->cleanupData($skipData);
            if ($changes->hasDifference()) {
                $changes->setSourceName($className);
                $changes->setSourceId($model->getId());
                $this->addEventChanges($changes);
            }

            $this->loadAdditionalData($model);
        }
        return $this;
    }

    /**
     * Force load additional models to save their data to logs
     *
     * @param AbstractModel $model
     * @return void
     */
    public function loadAdditionalData($model)
    {
        if ($model instanceof Items || $model instanceof Offer) {
            $this->_collectSellerInfo($model->getSellerId());
            $this->_collectProductInfo($model->getProductId());
        }

        if ($model instanceof \Formax\PriceDispersion\Model\ProductPrice || $model instanceof \Webkul\CategoryProductPrice\Model\ProductPrice) {
            $this->_collectSellerInfo($model->getSellerId());
            $this->_collectProductInfo($model->getMageproductId());
        }

        if ($model instanceof RegionalConditionSeller) {
            $this->_collectSellerInfo($model->getSellerId());
        }
    }

    /**
     * @param int $sellerId
     */
    protected function _collectSellerInfo($sellerId)
    {
        try {
            $seller = $this->customerRegistry->retrieve($sellerId);
            $this->collectAdditionalData($seller, [
                'user_rest_id_active_agreement',
                'user_rest_id_organization',
                'user_rest_id',
            ]);
        } catch (LocalizedException $e) {
        }
    }

    /**
     * @param int $productId
     */
    protected function _collectProductInfo($productId)
    {
        try {
            $product = $this->productRepository->getById($productId);
            $this->collectAdditionalData($product, ['sku']);
        } catch (LocalizedException $e) {
        }
    }

    /**
     * Post-dispatch action handler
     *
     * @return \Magento\Logging\Model\Processor|bool
     */
    public function logAction()
    {
        if (!$this->_initAction) {
            return false;
        }

        if ($this->_actionName == 'denied') {
            $this->logDeniedAction();
            return $this;
        }

        if ($this->_skipNextAction || !is_array($this->_eventConfig)) {
            return false;
        }

        $loggingEvent = $this->initLoggingEvent();
        $loggingEvent->setAction($this->_eventConfig['action']);
        $loggingEvent->setEventCode($this->_eventConfig['group_name']);

        try {
            if (!$this->callPostDispatchCallback($loggingEvent)) {
                return false;
            }

            /* Prepare additional info */
            if ($this->getCollectedAdditionalData()) {
                $loggingEvent->setAdditionalInfo($this->getCollectedAdditionalData());
            }
            $loggingEvent->save();
            $this->saveEventChanges($loggingEvent);
            $this->logChangesToFile($loggingEvent);
        } catch (\Exception $e) {
            $this->_logger->critical($e);
            return false;
        }
        return $this;
    }

    /**
     * Initialize logging event
     *
     * @return Event
     */
    private function initLoggingEvent()
    {
        $username = null;
        $userId = null;
        if ($this->_authSession->isLoggedIn()) {
            $userId = $this->_authSession->getUser()->getId();
            $username = $this->_authSession->getUser()->getUsername();
        }
        $errors = $this->messageManager->getMessages()->getErrors();
        $closure = function (MessageInterface $message) {
            return $message->toString();
        };
        /** @var \Magento\Logging\Model\Event $loggingEvent */
        $loggingEvent = $this->_eventFactory->create()->setData(
            [
                'ip' => $this->_remoteAddress->getRemoteAddress(),
                'x_forwarded_ip' => $this->_request->getServer('HTTP_X_FORWARDED_FOR'),
                'user' => $username,
                'user_id' => $userId,
                'is_success' => empty($errors),
                'fullaction' => $this->_initAction,
                'error_message' => implode("\n", array_map($closure, $errors)),
            ]
        );
        return $loggingEvent;
    }

    /**
     * Call post dispatch callback
     *
     * @param Event $loggingEvent
     * @return \Magento\Logging\Model\Processor|bool
     */
    private function callPostDispatchCallback($loggingEvent)
    {
        $handler = $this->_controllersHandler;
        $callback = 'postDispatchGeneric';

        if (isset($this->_eventConfig['post_dispatch'])) {
            $classPath = explode('::', $this->_eventConfig['post_dispatch']);
            if (count($classPath) == 2) {
                $handler = $this->_objectManager->get(str_replace('__', '/', $classPath[0]));
                $callback = $classPath[1];
            } else {
                $callback = $classPath[0];
            }
            if (!$handler || !$callback || !method_exists($handler, $callback)) {
                $this->_logger->critical(
                    new \Magento\Framework\Exception\LocalizedException(
                        __('Unknown callback function: %1::%2', $handler, $callback)
                    )
                );
            }
        }

        if (!$handler) {
            return false;
        }

        if (!$handler->{$callback}($this->_eventConfig, $loggingEvent, $this)) {
            return false;
        }
        return $this;
    }

    /**
     * Save event changes
     *
     * @param Event $loggingEvent
     * @return \Magento\Logging\Model\Processor|bool
     * @throws \Exception
     */
    private function saveEventChanges($loggingEvent)
    {
        if (!$loggingEvent->getId()) {
            return false;
        }
        foreach ($this->_eventChanges as $changes) {
            if ($changes && ($changes->getOriginalData() || $changes->getResultData())) {
                $changes->setEventId($loggingEvent->getId());
                $changes->save();
            }
        }
        return $this;
    }

    /**
     * Log event changes to file
     *
     * @param Event $loggingEvent
     * @return \Magento\Logging\Model\Processor
     */
    private function logChangesToFile($loggingEvent)
    {
        $data = $loggingEvent->getData();
        if (isset($data['additional_info'])) {
            unset($data['additional_info']);
        }

        foreach ($this->_eventChanges as $changes) {
            if ($changes->getId()) {
                $data['changes'][] = $changes->getData();
            }
        }
        $this->_actionLogger->info($this->json->serialize($data));
        return $this;
    }
}
