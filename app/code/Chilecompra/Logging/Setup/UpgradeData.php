<?php

namespace Chilecompra\Logging\Setup;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var WriterInterface
     */
    private $configWriter;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param WriterInterface $configWriter
     * @param Json $serializer
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        WriterInterface $configWriter,
        Json $serializer
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->serializer = $serializer;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $moduleVersion = $context->getVersion();

        if (version_compare($moduleVersion, '1.0.1', '<')) {
            $this->addActionsToConfig(['offerassignproduct', 'vendorattribute', 'regionalcondition']);
        }
    }

    /**
     * @param array $newActions
     */
    private function addActionsToConfig($newActions)
    {
        $path = 'admin/magento_logging/actions';
        $actions = $this->serializer->unserialize($this->scopeConfig->getValue($path));
        foreach ($newActions as $newAction) {
            $actions[$newAction] = "1";
        }
        $this->configWriter->save($path, $this->serializer->serialize($actions));
    }
}
