<?php

namespace Chilecompra\Logging\Observer\Plugin;

use Magento\Customer\Model\Customer;
use Magento\Framework\Event\Observer;
use Magento\Logging\Observer\ModelSaveAfterObserver;

/**
 * Move customer model processing to another observer in order to have orig_data
 * https://github.com/magento/magento2/issues/8788
 *
 * @see \Chilecompra\Logging\Observer\CustomerSaveAfterObserver
 */
class ModelSaveAfterObserverPlugin
{
    /**
     * @param ModelSaveAfterObserver $subject
     * @param \Closure $proceed
     * @param Observer $observer
     * @return void
     */
    public function aroundExecute(
        ModelSaveAfterObserver $subject,
        \Closure $proceed,
        Observer $observer
    ) {
        $object = $observer->getEvent()->getObject();
        if ($object instanceof Customer) {
            return;
        }
        $proceed($observer);
    }
}
