<?php

namespace Chilecompra\Logging\Observer;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Logging\Model\Processor;

class CustomerSaveAfterObserver implements ObserverInterface
{
    /**
     * @var Processor
     */
    protected $_processor;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @param Processor $processor
     * @param CustomerFactory $customerFactory
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        Processor $processor,
        CustomerFactory $customerFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->_processor = $processor;
        $this->customerFactory = $customerFactory;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * Model after save observer.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $object = $observer->getEvent()->getCustomerDataObject();
        $origObject = $observer->getEvent()->getOrigCustomerDataObject();

        $customerData = $this->extensibleDataObjectConverter->toNestedArray($object, [], CustomerInterface::class);
        $origCustomerData = $this->extensibleDataObjectConverter->toNestedArray($origObject, [], CustomerInterface::class);

        $customerModel = $this->customerFactory->create();
        if (isset($origCustomerData['id'])) {
            $idField = $customerModel->getIdFieldName();
            $id = $origCustomerData['id'];
            $origCustomerData[$idField] = $id;
            $customerData[$idField] = $id;
        }
        $customerModel->setData($origCustomerData)->setOrigData()->setData($customerData);

        $this->_processor->modelActionAfter($customerModel, 'save');
    }
}
