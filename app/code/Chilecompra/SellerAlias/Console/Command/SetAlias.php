<?php

namespace Chilecompra\SellerAlias\Console\Command;

use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \Chilecompra\SellerAlias\Helper\Data as Helper;

class SetAlias extends Command
{
    protected $appState;

    protected $helper;

    public function __construct(
        \Magento\Framework\App\State $appState,
        Helper $helper,
        $name = null
    ) {
        $this->helper = $helper;
        $this->appState = $appState;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('seller:set-alias');
        $this->setDescription('Generate random seller\'s alias for AB testing');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $result = $this->helper->generateAlias();
        $output->writeln('Row upgraded count: '.$result);
        return Cli::RETURN_SUCCESS;
    }
}
