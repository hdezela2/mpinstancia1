<?php 

namespace Chilecompra\SellerAlias\Cron;

use \Magento\Framework\App\State;
use \Chilecompra\SellerAlias\Helper\Data as Helper;

class SetAlias
{
    protected $appState;
    
    protected $helper;

    public function __construct(
        State $appState,
        Helper $helper)
    {
        $this->appState = $appState;
        $this->helper = $helper;
    }

    public function execute()
    {
        $this->helper->generateAlias();
    }
}