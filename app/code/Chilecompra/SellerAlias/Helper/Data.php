<?php
namespace Chilecompra\SellerAlias\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollection;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Summa\CustomerData\Logger\Logger;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {
    const XML_PATH_STOCK_MANAGEMENT = 'alias_data/';

    protected $customerCollection;
    protected $customerRepository;
    protected $aliasedAgreements;
    protected $minIdConfig;
    protected $maxIdConfig;

    public function __construct(
        Context $context, 
        CustomerCollection $customerCollection, 
        CustomerRepositoryInterface $customerRepository,
        Logger $logger)
    {
        $this->customerCollection = $customerCollection;
        $this->customerRepository = $customerRepository;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    public function generateAlias() {
        $this->aliasedAgreements = explode(',', $this->getGeneralConfig('agreementids'));
        $this->minIdConfig = (int)$this->getGeneralConfig('minid');
        $this->maxIdConfig = (int)$this->getGeneralConfig('maxid');

        $count = 0;

        try{
            if(!empty($this->aliasedAgreements)){
                foreach ($this->aliasedAgreements as $agreementId) {
                    
                    $collection = $this->customerCollection->create();

                    $collection->getSelect()->distinct()
                        ->where('email', ['like'=>'%-'.$agreementId.'@email.com']);

                    $collection->getSelect()->join(
                        ["usr" => $collection->getTable("marketplace_userdata")],
                        "e.entity_id = usr.seller_id",
                        [])
                        ->where('usr.is_seller', 1);

                    foreach($collection as $item){
                        $customer = $this->customerRepository->getById($item["entity_id"]);
                        $value = (string)rand($this->minIdConfig, $this->maxIdConfig);
                        $customer->setCustomAttribute("seller_alias", $value);
                        $this->customerRepository->save($customer);

                        $count++;
                    }
                }
            }
        }   
        catch(\Exception $ex){
            $this->_logger->critical($ex);
        }

        return $count;
    }

    public function getGeneralConfig($code)
    {
        return $this->getConfigValue(self::XML_PATH_STOCK_MANAGEMENT .'alias_group/'. $code);
    }

    public function getConfigValue($field)
    {
        return $this->scopeConfig->getValue($field, ScopeInterface::SCOPE_WEBSITE);
    }
}