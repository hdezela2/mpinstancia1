<?php
/**
 * Chilecompra.
 *
 * @category  Chilecompra
 * @package   Chilecompra_WebkulMarketplace
 * @author    Chilecompra
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Chilecompra_WebkulMarketplace',
    __DIR__
);