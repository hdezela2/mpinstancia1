<?php

namespace Chilecompra\WebkulMarketplace\Plugin;

use Magento\Framework\UrlInterface;
use Webkul\Marketplace\Ui\Component\Listing\Columns\Flaglink;

/**
 * @see Flaglink
 */
class FlaglinkPlugin
{
    private UrlInterface $urlBuilder;

    public function __construct(UrlInterface $urlBuilder)
    {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * The original Webkul\Marketplace\Ui\Component\Listing\Columns\Flaglink::prepareDataSource
     * is buggy, so we are adding a condition to make sure the index is defined
     * That said, we are not calling proceed because we don't want to call the original method
     */
    public function aroundPrepareDataSource(Flaglink $subject, callable $proceed, array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $subject->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['entity_id'])) {
                    if (isset($item[$fieldName])) {
                        $item[$fieldName] = "<a href='" . $this->urlBuilder->getUrl('customer/index/edit', ['id' => $item['seller_id'], 'seller_panel' => 1]) . "' target='blank' title='" . __('View Flags') . "'>" . $item[$fieldName] . '</a>';
                    }
                }
            }
        }

        return $dataSource;
    }
}
