<?php

namespace Chilecompra\WebkulMarketplace\Plugin;

use Magento\Store\Model\Store;
use Webkul\Marketplace\Model\ResourceModel\Feedback\Collection;

class FeedbackCollectionPlugin
{
    /**
     * The original Webkul\Marketplace\Model\ResourceModel\Feedback\Collection::addStoreFilter
     * calls an undefined method and also adds an invalid condition to the where clause
     * when querying the marketplace_datafeedback database table, so we are making sure it doesn't
     * call the undefined method as well as add the invalid condition to the where clause.
     * We don't want to call the original method and that means we are not calling proceed
     *
     * @param Collection $subject
     * @param callable $proceed
     * @param array|int|Store $store
     * @param bool $withAdmin
     * @return FeedbackCollectionPlugin
     */
    public function aroundAddStoreFilter(
        Collection $subject,
        callable $proceed,
        $store,
        bool $withAdmin = true
    ): FeedbackCollectionPlugin {
        return $this;
    }
}
