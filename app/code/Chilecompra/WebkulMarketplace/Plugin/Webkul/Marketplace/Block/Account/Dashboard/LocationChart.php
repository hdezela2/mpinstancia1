<?php
namespace Chilecompra\WebkulMarketplace\Plugin\Webkul\Marketplace\Block\Account\Dashboard;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class LocationChart
{
    /**
     * @param \Webkul\Marketplace\Block\Account\Dashboard\LocationChart $subject
     * @param AbstractCollection $collection
     * @param array $orderSaleArr
     * @return array
     */
    public function beforeGetArrayData(
        \Webkul\Marketplace\Block\Account\Dashboard\LocationChart $subject,
        AbstractCollection $collection,
        array $orderSaleArr
    ) {
        foreach ($collection as $key => $record) {
            if ($record->getOrderApprovalStatus() != 1) {
                $collection->removeItemByKey($key);
            }
        }
        return [$collection, $orderSaleArr];
    }
}
