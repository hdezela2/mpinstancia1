<?php

namespace Chilecompra\WebkulMarketplace\Rewrite\Webkul\Marketplace\Observer;

use Magento\Framework\Event\Observer;

class CatalogProductDeleteAfterObserver extends \Webkul\Marketplace\Observer\CatalogProductDeleteAfterObserver
{
    /**
     * Product delete after event handler.
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            $productId = $observer->getProduct()->getId();
            $rowId = $observer->getProduct()->getRowId();
            $productCollection = $this->mpProductFactory->create()
                ->getCollection()
                ->addFieldToFilter(
                    'mageproduct_id',
                    $productId
                )
                ->addFieldToFilter(
                    'mage_pro_row_id',
                    $rowId
                );

            /**
             * This is a temporary snippet code to track delete operations
             * Do not use Object Manager like so as this isn't a good practice.
             * We want to know how the offers are getting removed,
             * so we are adding log entries like the one below.
             *
             * See https://chilecompra.atlassian.net/browse/MCDS-101
             */
            $exception = new \Exception();
            $traceMessage = $exception->getTraceAsString();
            $logger = \Magento\Framework\App\ObjectManager::getInstance()->create(\Psr\Log\LoggerInterface::class);
            $message = "\n##########################";
            $message .= "\nTRACKING DELETE OPERATIONS";
            $message .= "\nProduct ID: ".$productId;
            $message .= "\nRow ID: ".$rowId;
            $message .= "\nCollection: ".var_export($productCollection->getData(), true);
            $message .= "\n##########################\n";
            $message .= __FILE__ . "\n";
            $message .= $traceMessage;
            $logger->debug($message);

            foreach ($productCollection as $product) {
                $this->mpProductFactory->create()
                    ->load($product->getEntityId())->delete();
            }
        } catch (\Exception $e) {
            $this->mpHelper->logDataInLogger(
                "Observer_CatalogProductDeleteAfterObserver execute : ".$e->getMessage()
            );
            $this->messageManager->addError($e->getMessage());
        }
    }
}
