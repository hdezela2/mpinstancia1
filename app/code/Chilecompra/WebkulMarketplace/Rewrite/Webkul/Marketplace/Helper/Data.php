<?php

namespace Chilecompra\WebkulMarketplace\Rewrite\Webkul\Marketplace\Helper;

/**
 * Chilecompra Marketplace Helper Data
 * Rewrite Webkul\Marketplace
 */
class Data extends \Webkul\Marketplace\Helper\Data
{
    /**
     * Ignore actions
     * @var array
     */
    protected $ignoreActions = [
        'marketplace_account_becomesellerpost',
        'marketplace_account_deletesellerlogo',
        'marketplace_account_editprofilepost',
        'vendorattribute_account_becomesellerpost'
    ];

    /**
     * Clean Cache
     */
    public function clearCache()
    {
        return $this;
    }

    public function getSellerInformation($sellerIds)
    {
        /* PHPSTAN - _productCollectionFactory property dynamically declared in 3rd party module*/
        $collection = $this->_productCollectionFactory->create(); /** @phpstan-ignore-line */
        $collection->joinSellerProducts();
        $collection->resetColumns();
        $collection->addFieldToCollection("count", "count(mp_product.seller_id)");
        $collection->getSelect()->where("mp_product.seller_id IN(?)", $sellerIds);
        $query = $collection->getSelectSql(true);
        $sellerCollection = $this->getSellerCollection();
        $sellerCollection->addFieldToFilter('seller_id', ['in' => $sellerIds]);
        $sellerCollection = $this->joinCustomer($sellerCollection);
        $sellerCollection->resetColumns();
        $fields = ["entity_id", "shop_title", "shop_url", "company_locality", "logo_pic", "seller_id"];
        $sellerCollection->addFieldsToCollection($fields);
        $sellerCollection->addFieldToCollection("product_count", "($query)");
        $cloneColelction = clone $sellerCollection;
        $sellerCollection->addFieldToFilter('store_id', $this->getCurrentStoreId());
        $cloneColelction->addFieldToFilter('store_id', 0);
        $sellerData = [];
        foreach ($cloneColelction as $seller) {
            $sellerData[$seller->getSellerId()] = [
                "entity_id" => $seller->getId(),
                "shop_title" => $seller->getShopTitle(),
                "shop_url" => $seller->getShopUrl(),
                "company_locality" => $seller->getCompanyLocality(),
                "logo_pic" => $seller->getLogoPic()
            ];
        }
        foreach ($sellerCollection as $seller) {
            if (array_key_exists($seller->getSellerId(), $sellerData)) {
                $sellerData[$seller->getSellerId()] = [
                    "entity_id" => $seller->getId(),
                    "shop_title" => $seller->getShopTitle(),
                    "shop_url" => $seller->getShopUrl(),
                    "company_locality" => $seller->getCompanyLocality(),
                    "logo_pic" => $seller->getLogoPic()
                ];
            }
        }
        return $sellerData;
    }

    /**
     * Get Seller Name.
     *
     * @param string | $id
     * @param int |   $mpAssignId
     *
     * @return int
     */
    public function getUserInfo($id, $mpAssignId)
    {
        $sellerId = '';
        if ($mpAssignId) {
            $assignProduct = $this->_objectManager->get(
                'Webkul\MpAssignProduct\Model\Items'
            )->load($mpAssignId);
            if ($assignProduct->getId() > 0) {
                $sellerId = $assignProduct->getSellerId();
            }
        } else {
            $marketplaceProductCollection = $this->_objectManager->get(
                'Webkul\Marketplace\Model\Product'
            )
                ->getCollection()
                ->addFieldToFilter(
                    'mageproduct_id',
                    ['eq' => $id]
                );
            if (count($marketplaceProductCollection)) {
                foreach ($marketplaceProductCollection as $product) {
                    $sellerId = $product->getSellerId();
                }
            }
        }
        return $sellerId;
    }
}
