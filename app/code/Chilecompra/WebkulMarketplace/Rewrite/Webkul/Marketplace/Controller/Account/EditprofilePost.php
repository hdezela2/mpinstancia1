<?php

namespace Chilecompra\WebkulMarketplace\Rewrite\Webkul\Marketplace\Controller\Account;

class EditprofilePost extends \Webkul\Marketplace\Controller\Account\EditprofilePost
{

    /**
     * Update Seller Profile Informations.
     *
     * @return \Magento\Framework\Controller\Result\RedirectFactory
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($this->getRequest()->isPost()) {
            try {
                if (!$this->_formKeyValidator->validate($this->getRequest())) {
                    return $this->resultRedirectFactory->create()->setPath(
                        '*/*/editProfile',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                }

                $fields = $this->getRequest()->getParams();
                $errors = $this->validateprofiledata($fields);
                $sellerId = $this->helper->getCustomerId();
                $storeId = $this->helper->getCurrentStoreId();
                $img1 = '';
                $img2 = '';
                if (empty($errors)) {
                    $autoId = 0;
                    $collection = $this->sellerModel->create()
                        ->getCollection()
                        ->addFieldToFilter('seller_id', $sellerId)
                        ->addFieldToFilter('store_id', $storeId);
                    foreach ($collection as $value) {
                        $autoId = $value->getId();
                    }
                    $fields = $this->getSellerProfileFields($fields);
                    // If seller data doesn't exist for current store
                    if (!$autoId) {
                        $sellerDefaultData = [];
                        $collection = $this->sellerModel->create()
                            ->getCollection()
                            ->addFieldToFilter('seller_id', $sellerId)
                            ->addFieldToFilter('store_id', 0);
                        foreach ($collection as $value) {
                            $sellerDefaultData = $value->getData();
                        }
                        foreach ($sellerDefaultData as $key => $value) {
                            if (empty($fields[$key]) && $key != 'entity_id') {
                                $fields[$key] = $value;
                            }
                        }
                    }

                    // Save seller data for current store
                    $value = $this->sellerModel->create()->load($autoId);
                    $value->addData($fields);
                    if (!$autoId) {
                        $value->setCreatedAt($this->_date->gmtDate());
                    }
                    $value->setUpdatedAt($this->_date->gmtDate());
                    $value->save();

                    if ($fields['company_description']) {
                        $fields['company_description'] = str_replace(
                            'script',
                            '',
                            $fields['company_description']
                        );
                    }
                    $value->setCompanyDescription($fields['company_description']);

                    if (isset($fields['return_policy'])) {
                        $fields['return_policy'] = str_replace(
                            'script',
                            '',
                            $fields['return_policy']
                        );
                        $value->setReturnPolicy($fields['return_policy']);
                    }

                    if (isset($fields['shipping_policy'])) {
                        $fields['shipping_policy'] = str_replace(
                            'script',
                            '',
                            $fields['shipping_policy']
                        );
                        $value->setShippingPolicy($fields['shipping_policy']);
                    }

                    if (isset($fields['privacy_policy'])) {
                        $fields['privacy_policy'] = str_replace(
                            'script',
                            '',
                            $fields['privacy_policy']
                        );
                        $value->setPrivacyPolicy($fields['privacy_policy']);
                    }

                    if (array_key_exists('meta_description', $fields)) {
                        $value->setMetaDescription($fields['meta_description']);
                    }

                    $target = $this->_mediaDirectory->getAbsolutePath('avatar/');

                    /**
                     * set taxvat number for seller
                     */
                    if (array_key_exists('taxvat', $fields)) {
                        if ($fields['taxvat']) {
                            $customer = $this->customerModel->create()->load($sellerId);
                            $customer->setTaxvat($fields['taxvat']);
                            $customer->setId($sellerId)->save();
                        }

                        try {
                            /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
                            $uploader = $this->_fileUploaderFactory->create(
                                ['fileId' => 'banner_pic']
                            );
                            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                            $uploader->setAllowRenameFiles(true);
                            $result = $uploader->save($target);
                            if ($result['file']) {
                                $value->setBannerPic($result['file']);
                            }
                        } catch (\Exception $e) {
                            $this->helper->logDataInLogger(
                                "Controller_Account_EditProfilePost execute : ".$e->getMessage()
                            );

                            if ($e->getMessage() != 'The file was not uploaded.') {
                                $this->messageManager->addError($e->getMessage());
                                $this->dataPersistor->set('seller_profile_data', $fields);
                            }
                        }
                    }
                    try {
                        /** @var $uploaderLogo \Magento\MediaStorage\Model\File\Uploader */
                        $uploaderLogo = $this->_fileUploaderFactory->create(
                            ['fileId' => 'logo_pic']
                        );
                        $uploaderLogo->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        $uploaderLogo->setAllowRenameFiles(true);
                        $resultLogo = $uploaderLogo->save($target);
                        if ($resultLogo['file']) {
                            $value->setLogoPic($resultLogo['file']);
                        }
                    } catch (\Exception $e) {
                        $this->helper->logDataInLogger(
                            "Controller_Account_EditProfilePost execute : ".$e->getMessage()
                        );
                        if ($e->getMessage() != 'The file was not uploaded.') {
                            $this->messageManager->addError($e->getMessage());
                            $this->dataPersistor->set('seller_profile_data', $fields);
                        }
                    }

                    if (array_key_exists('country_pic', $fields)) {
                        $value->setCountryPic($fields['country_pic']);
                    }
                    $value->save();

                    if (array_key_exists('country_pic', $fields)) {
                        $value->setCountryPic($fields['country_pic']);
                    }
                    $value->setStoreId($storeId);
                    $value->save();
                    try {
                        // clear cache
                        $this->helper->clearCache();
                        if (!empty($errors)) {
                            foreach ($errors as $message) {
                                $this->messageManager->addError($message);
                            }
                            $this->dataPersistor->set('seller_profile_data', $fields);
                        } else {
                            $this->messageManager->addSuccess(
                                __('Profile information was successfully saved')
                            );
                            $this->dataPersistor->clear('seller_profile_data');
                        }

                        return $this->resultRedirectFactory->create()->setPath(
                            '*/*/editProfile',
                            ['_secure' => $this->getRequest()->isSecure()]
                        );
                    } catch (\Exception $e) {
                        $this->helper->logDataInLogger(
                            "Controller_Account_EditProfilePost execute : ".$e->getMessage()
                        );
                        $this->messageManager->addException($e, __('We can\'t save the customer.'));
                    }

                    return $this->resultRedirectFactory->create()->setPath(
                        '*/*/editProfile',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                } else {
                    foreach ($errors as $message) {
                        $this->messageManager->addError($message);
                    }
                    $this->dataPersistor->set('seller_profile_data', $fields);

                    return $this->resultRedirectFactory->create()->setPath(
                        '*/*/editProfile',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                }
            } catch (\Exception $e) {
                $this->helper->logDataInLogger(
                    "Controller_Account_EditProfilePost execute : ".$e->getMessage()
                );
                $this->messageManager->addError($e->getMessage());
                $this->dataPersistor->set('seller_profile_data', $fields);

                return $this->resultRedirectFactory->create()->setPath(
                    '*/*/editProfile',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/editProfile',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
}
