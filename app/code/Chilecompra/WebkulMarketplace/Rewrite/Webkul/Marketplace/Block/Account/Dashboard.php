<?php

namespace Chilecompra\WebkulMarketplace\Rewrite\Webkul\Marketplace\Block\Account;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;

class Dashboard extends \Webkul\Marketplace\Block\Account\Dashboard
{
    /**
     * @return array
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function getTopSaleProducts()
    {
        $sellerId = $this->getCustomerId();
        $mpOrderscollection = $this->mpOrderCollectionFactory->create();
        $mpOrderscollection->addFieldToFilter('seller_id', $sellerId);
        $salesOrder = $mpOrderscollection->getTable('sales_order');
        $mpOrderscollection->getSelect()->join(
            $salesOrder.' as so',
            'main_table.order_id = so.entity_id'
        )->where("so.order_approval_status=1");
        $allOrderIds = $mpOrderscollection->getAllIds();
        $collection = $this->mpSaleslistCollectionFactory->create()
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            )
            ->addFieldToFilter(
                'order_id',
                ['in' => $allOrderIds]
            )
            ->addFieldToFilter(
                'parent_item_id',
                ['null' => 'true']
            )
            ->getAllOrderProducts();
        $name = '';
        $resultData = [];
        foreach ($collection as $coll) {
            $item = $this->orderItemRepository->get($coll['order_item_id']);
            $product = $item->getProduct();
            if ($product) {
                $resultData[$coll->getId()]['name'] = $product->getName();
                $resultData[$coll->getId()]['url'] = $product->getProductUrl();
                $resultData[$coll->getId()]['qty'] = $coll['qty'];
            } else {
                $resultData[$coll->getId()]['name'] = $item->getName();
                $resultData[$coll->getId()]['url'] = '';
                $resultData[$coll->getId()]['qty'] = $coll['qty'];
            }
        }
        return $resultData;
    }

    /**
     * @return int
     */
    public function getTotalCustomers()
    {
        $sellerId = $this->getCustomerId();
        $mpOrderscollection = $this->mpOrderCollectionFactory->create();
        $mpOrderscollection->addFieldToFilter('seller_id', $sellerId);
        $salesOrder = $mpOrderscollection->getTable('sales_order');
        $mpOrderscollection->getSelect()->join(
            $salesOrder.' as so',
            'main_table.order_id = so.entity_id'
        )->where("so.order_approval_status=1");
        $allOrderIds = $mpOrderscollection->getAllIds();
        $collection = $this->mpSaleslistCollectionFactory->create()
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            )
            ->addFieldToFilter(
                'order_id',
                ['in' => $allOrderIds]
            )
            ->getTotalCustomersCount();
        return count($collection);
    }

    /**
     * @return int
     */
    public function getTotalCustomersCurrentMonth()
    {
        $sellerId = $this->getCustomerId();
        $mpOrderscollection = $this->mpOrderCollectionFactory->create();
        $mpOrderscollection->addFieldToFilter('seller_id', $sellerId);
        $salesOrder = $mpOrderscollection->getTable('sales_order');
        $mpOrderscollection->getSelect()->join(
            $salesOrder.' as so',
            'main_table.order_id = so.entity_id'
        )->where("so.order_approval_status=1");
        $allOrderIds = $mpOrderscollection->getAllIds();
        $collection = $this->mpSaleslistCollectionFactory->create()
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            )->addFieldToFilter(
                'order_id',
                ['in' => $allOrderIds]
            )->addFieldToFilter(
                'created_at',
                [
                    'datetime' => true,
                    'from' => date('Y-m').'-01 00:00:00',
                    'to' => date('Y-m').'-31 23:59:59',
                ]
            )->getTotalCustomersCount();
        return count($collection);
    }

    /**
     * @return int
     */
    public function getTotalCustomersLastMonth()
    {
        $sellerId = $this->getCustomerId();
        $mpOrderscollection = $this->mpOrderCollectionFactory->create();
        $mpOrderscollection->addFieldToFilter('seller_id', $sellerId);
        $salesOrder = $mpOrderscollection->getTable('sales_order');
        $mpOrderscollection->getSelect()->join(
            $salesOrder.' as so',
            'main_table.order_id = so.entity_id'
        )->where("so.order_approval_status=1");
        $allOrderIds = $mpOrderscollection->getAllIds();
        $collection = $this->mpSaleslistCollectionFactory->create()
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            )->addFieldToFilter(
                'order_id',
                ['in' => $allOrderIds]
            )->addFieldToFilter(
                'created_at',
                [
                    'datetime' => true,
                    'from' => date('Y-m', strtotime('last month')).'-01 00:00:00',
                    'to' => date('Y-m', strtotime('last month')).'-31 23:59:59',
                ]
            )->getTotalCustomersCount();
        return count($collection);
    }
}
