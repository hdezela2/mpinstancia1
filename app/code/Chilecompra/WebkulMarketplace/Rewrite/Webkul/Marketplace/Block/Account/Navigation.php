<?php

namespace Chilecompra\WebkulMarketplace\Rewrite\Webkul\Marketplace\Block\Account;

use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Model\Config as PaymentConfig;
use Magento\Sales\Model\OrderFactory;
use Magento\Shipping\Model\Config as ShippingConfig;
use \Magento\Store\Model\StoreManagerInterface;
use Webkul\Marketplace\Model\OrdersFactory as WKOrdersFactory;
use Webkul\Marketplace\Model\ProductFactory as WKProductFactory;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as WKCollectionFactory;
use Webkul\Marketplace\Model\SaleslistFactory as WKSaleslistFactory;
use Webkul\Marketplace\Model\SellertransactionFactory as WKSellertransactionFactory;
use Webkul\Marketplace\Helper\Data as MpHelper;

class Navigation extends \Webkul\Marketplace\Block\Account\Navigation
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param Context $context
     * @param DateTime $date
     * @param Session $customerSession
     * @param WKProductFactory $productFactory
     * @param WKOrdersFactory $ordersFactory
     * @param WKCollectionFactory $productCollection
     * @param WKSellertransactionFactory $sellertransaction
     * @param ProductFactory $productModel
     * @param OrderFactory $orderModel
     * @param WKSaleslistFactory $saleslistModel
     * @param ShippingConfig $shipconfig
     * @param PaymentConfig $paymentConfig
     * @param MpHelper $mpHelper
     * @param array $data
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        DateTime $date,
        Session $customerSession,
        WKProductFactory $productFactory,
        WKOrdersFactory $ordersFactory,
        WKCollectionFactory $productCollection,
        WKSellertransactionFactory $sellertransaction,
        ProductFactory $productModel,
        OrderFactory $orderModel,
        WKSaleslistFactory $saleslistModel,
        ShippingConfig $shipconfig,
        PaymentConfig $paymentConfig,
        MpHelper $mpHelper,
        array $data,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct(
            $context,
            $date,
            $customerSession,
            $productFactory = null,
            $ordersFactory = null,
            $productCollection = null,
            $sellertransaction = null,
            $productModel = null,
            $orderModel = null,
            $saleslistModel = null,
            $shipconfig = null,
            $paymentConfig = null,
            $mpHelper,
            $data = []
        );
        $this->storeManager = $storeManager;
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function shouldRenderQuoteButton()
    {
        $forbiddenSites =
            [
                \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE,
                \Summa\CombustiblesSetUp\Helper\Data::WEBSITE_CODE,
                \Chilecompra\CombustiblesRenewal\Model\Constants::WEBSITE_CODE
            ];
        return !in_array($this->_storeManager->getStore()->getCode(), $forbiddenSites);
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function shouldRenderAddProductButton()
    {
        $forbiddenSites =
            [
                \Summa\CombustiblesSetUp\Helper\Data::WEBSITE_CODE,
                \Chilecompra\CombustiblesRenewal\Model\Constants::WEBSITE_CODE,
                SoftwareRenewalConstants::WEBSITE_CODE
            ];
        return !in_array($this->_storeManager->getStore()->getCode(), $forbiddenSites);
    }

    public function shouldRenderRegionalConditionsMenu()
    {
        $forbiddenSites =
            [
                \Formax\ConfigCmSoftware\Helper\Data::SOFTWARE_STOREVIEW_CODE,
                 SoftwareRenewalConstants::STORE_CODE
            ];
        return !in_array($this->_storeManager->getStore()->getCode(), $forbiddenSites);
    }
}
