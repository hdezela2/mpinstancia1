define([
    'jquery',
    'jquery/ui',
], function ($) {
    'use strict';
    return function (widget) {
        $.widget('mage.editSellerProfile', widget, {
            _create: function () {
                var self = this;
                $(self.options.countryPicSelector).on('change', function () {
                    self.callGoogleMapApiAjaxFunction(this);
                });

                $(self.options.bannerPicSelector).on('change', function () {
                    var imagename=$(this).val().toLowerCase();
                    var image=imagename.split(".");
                    var extIndex = image.length - 1;
                    image=image[extIndex];
                    if (image!='jpg' && image!='jpeg' && image!='png' && image!='gif') {
                        alert({
                            content: self.options.errorMessageForAllowedExtensions
                        });
                        $(this).val('');
                    }
                });

                $(self.options.logoPicSelector).on('change', function () {
                    var imagename=$(this).val().toLowerCase();
                    var image=imagename.split(".");
                    var extIndex = image.length - 1;
                    image=image[extIndex];
                    if (image!='jpg' && image!='jpeg' && image!='png' && image!='gif') {
                        alert({
                            content: self.options.errorMessageForlogoAllowedExtensions
                        });
                        $(this).val('');
                    }
                });

                $(self.options.leftButtonSelector).insertAfter(self.options.buttonsSetLastSelector);

                $(self.options.inputTextSelector).on('change', function () {
                    var thisValue = $(this).val();
                    var regex = /(<([^>]+)>)/ig;
                    var result = thisValue.replace(regex,"");
                    $(this).val(result);
                });

                $(self.options.profileimageSetSpanSelector).click(function (event) {
                    var dicisionapp = confirm(self.options.confirmMessageForBannerDelete);
                    if (dicisionapp) {
                        var thisthis = $(this);
                        $(self.options.bannerSelector).css('opacity', self.options.opacity);
                        $.ajax({
                            url: self.options.bannerDeleteAjaxUrl,
                            type: "POST",
                            data: {
                                file:'banner'
                            },
                            dataType: 'html',
                            success:function (response) {
                                thisthis.parent(self.options.setimageSelector).remove();
                            },
                            error: function (response) {
                                alert({
                                    content: self.options.ajaxErrorMessage
                                });
                            }
                        });
                    }
                });

                $(self.options.profileImageDeleteSelector).mouseover(function (event) {
                    $(event.target).css('width', self.options.logoImageDeleteMouseOverWidth);
                });

                $(self.options.profileImageDeleteSelector).mouseout(function (event) {
                    $(event.target).css('width', self.options.logoImageDeleteMouseOutWidth);
                });

                $(self.options.logoImageSetSpanSelector).click(function (event) {
                    var dicisionapp = confirm(self.options.confirmMessageForLogoDelete);
                    if (dicisionapp) {
                        var thisthis = $(this);
                        $(self.options.logoSelector).css('opacity', self.options.opacity);
                        $.ajax({
                            url: self.options.logoDeleteAjaxUrl,
                            type: "POST",
                            data: {
                                file:'logo'
                            },
                            dataType: 'html',
                            success:function (response) {
                                thisthis.parent(self.options.setimageSelector).remove();
                            },
                            error: function (response) {
                                alert({
                                    content: self.options.ajaxErrorMessage
                                });
                            }
                        });
                    }
                });

                $(self.options.logoImageDeleteSelector).mouseover(function (event) {
                    $(event.target).css('width', self.options.logoImageDeleteMouseOverWidth);
                });

                $(self.options.logoImageDeleteSelector).mouseout(function (event) {
                    $(event.target).css('width', self.options.logoImageDeleteMouseOutWidth);
                });
            }
        });
        return $.mage.editSellerProfile;
    };
});
