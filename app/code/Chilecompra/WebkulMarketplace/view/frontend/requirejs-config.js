var config = {
    config: {
        'mixins': {
            'Webkul_Marketplace/js/account/edit-seller-profile': {
                'Chilecompra_WebkulMarketplace/js/account/edit-seller-profile': true
            }
        }
    },
    map: {
        '*': {
            'Webkul_Marketplace/js/grid/provider':'Chilecompra_WebkulMarketplace/js/grid/provider'
        }
    }
};
