<?php
namespace Chilecompra\WebkulMpShipping\Helper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session as CustomerSession;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	/**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;
    /**
     * @var \Magento\Quote\Model\Quote\Item\OptionFactory
     */
    protected $quoteOptionFactory;
    /**
     * @var \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory
     */
    protected $assignProductCollectionFactory;
    /**
     * @var \Webkul\Marketplace\Model\ProductFactory
     */
    protected $marketplaceProductFactory;
    protected $quoteMappingData = null;

    /**
     * @var Quote
     */
    private $quote;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Checkout\Model\Cart $cart
	 * @param \Magento\Quote\Model\Quote\Item\OptionFactory $quoteOptionFactory
     * @param \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory $assignProductCollectionFactory
     * @param \Webkul\Marketplace\Model\ProductFactory $marketplaceProductFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Quote\Model\Quote\Item\OptionFactory $quoteOptionFactory,
        \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory $assignProductCollectionFactory,
        \Webkul\Marketplace\Model\ProductFactory $marketplaceProductFactory,
        Quote $quote,
        StoreManagerInterface $storeManager,
        CustomerSession $customerSession
    ) {
        parent::__construct($context);
        $this->cart = $cart;
        $this->quoteOptionFactory = $quoteOptionFactory;
        $this->assignProductCollectionFactory = $assignProductCollectionFactory;
        $this->marketplaceProductFactory = $marketplaceProductFactory;
        $this->quote = $quote;
        $this->storeManager = $storeManager;
        $this->customerSession = $customerSession;
    }

    /**
     * @return Quote\Item[]
     * @throws NoSuchEntityException
     */
    private function getItems(): array
    {
        if ($this->cart->hasData('quote')) {
            $quote = $this->cart->getQuote();
        } else {
            $websiteId = $this->storeManager->getStore()->getWebsiteId();
            $quoteId = $this->getQuoteId($websiteId);
            if ($quoteId) {
                $quote = $this->quote->loadByIdWithoutStore($quoteId);
            } else {
                $customerId = $this->customerSession->getCustomerId();
                $quote = $this->quote->loadByCustomer($customerId);
            }
        }

        return $quote->getAllVisibleItems();
    }

    /**
     * @param int $websiteId
     * @return mixed
     */
    private function getQuoteId(int $websiteId)
    {
        /**
         * Workaround for the infinite loop issue. This is not a definitive solution.
         * We should never access session like this but due to the circumstances
         * that's all we can do at the moment to avoid the infinite loops.
         *
         * We can't call $cart->getQuote() or $cart->getQuoteId() because this causes a infinite loop.
         * This explains why we are accessing session like so:
         */
        $quoteId = null;

        if (isset($_SESSION['checkout']['quote_id_' . $websiteId])) {
            $quoteId = $_SESSION['checkout']['quote_id_' . $websiteId];
        }

        return $quoteId;
    }

    public function getSellerIdFromQuote()
    {
        if (is_null($this->quoteMappingData)) {
            $items = $this->getItems();

            $itemIds = [];
            $productIds = [];

            foreach ($items as $item) {
                $itemIds[] = $item->getId();
                $productIds[] = $item->getProductId();
            }

            $mpassignproductIds = $this->_getAssignProductIds($itemIds);
            $sellerMapping = $this->_getSellerEntityMapping($mpassignproductIds, $productIds);
            $quoteItemMapping = [];

            foreach ($items as $item) {
                $mpassignproductId = $item->getBuyRequest()->getData('mpassignproduct_id');
                $productId = $item->getProductId();
                if (array_key_exists('assignproduct', $sellerMapping) &&
                    array_key_exists($mpassignproductId, $sellerMapping['assignproduct'])) {
                    $quoteItemMapping[$item->getId()] = $sellerMapping['assignproduct'][$mpassignproductId];
                } elseif (array_key_exists('mageproduct', $sellerMapping) &&
                    array_key_exists($productId, $sellerMapping['mageproduct'])) {
                    $quoteItemMapping[$item->getId()] = $sellerMapping['mageproduct'][$productId];
                }
            }

            $this->quoteMappingData = $quoteItemMapping;
        }

        return $this->quoteMappingData;
    }

    /**
     * Validates JSON
     */
    private function _validJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * get assign product id.
     *
     * @param object $item
     *
     * @return int
     */
    protected function _getAssignProductIds($itemIds)
    {
        $mpassignproductId = [];
        $itemOption = $this->quoteOptionFactory->create()
            ->getCollection();
        $itemOption = $itemOption->addFieldToFilter('item_id', ['in' => $itemIds])
            ->addFieldToFilter('code', ['eq' => 'info_buyRequest']);

        if ($itemOption->getSize()) {
            foreach ($itemOption as $value) {
                $optionValue = $value->getValue();
                if ($optionValue != '') {
                    $temp = [];
                    if ($this->_validJson($optionValue)) {
                        $temp = json_decode($optionValue, true);
                    } else {
                        $temp = unserialize($optionValue);
                    }
                    $mpassignproductId[] = isset($temp['mpassignproduct_id']) ? $temp['mpassignproduct_id'] : 0;
                }
            }
        }

        return $mpassignproductId;
    }
    /**
     * get seller id.
     *
     * @param int $mpassignproductId
     * @param int $proid
     *
     * @return int
     */
    protected function _getSellerEntityMapping($mpassignproductIds, $productIds)
    {
        $mapping = [];

        if ($mpassignproductIds) {
            $assignItemsCollection = $this->assignProductCollectionFactory->create();
            $assignItemsCollection->addFieldToFilter('id', ['in' => $mpassignproductIds]);
            foreach ($assignItemsCollection as $item) {
                $mapping['assignproduct'][$item->getId()] = $item->getSellerId();
            }
        } else {
            $collection = $this->marketplaceProductFactory->create()
                ->getCollection()
                ->addFieldToFilter('mageproduct_id', ['in' => $productIds]);
            foreach ($collection as $item) {
                $mapping['mageproduct'][$item->getMageproductId()] = $item->getSellerId();
            }
        }

        return $mapping;
    }
}
