<?php
namespace Chilecompra\WebkulMpShipping\Rewrite\Webkul\MpMultiShipping\Model;
class Carrier extends \Webkul\MpMultiShipping\Model\Carrier
{
	/**
     * @var \Chilecompra\WebkulMpShipping\Helper\Data
     */
    protected $shippingHelper;
	/**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param SessionManager $coreSession
     * @param \Webkul\Marketplace\Model\Orders $mpSalesModel
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webkul\MpMultiShipping\Helper\Data $currentHelper
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\App\RequestInterface $requestInterface
     * @param PriceCurrencyInterface $priceCurrency
     * @param OptionFactory $optionFactory
     * @param CustomerFactory $customerFactory
     * @param AddressFactory $addressFactory
     * @param \Webkul\Marketplace\Model\ProductFactory $mpProductFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory
     * @param ShippingSettingRepository $shippingSettingRepository
     * @param MpMultiShipLog $mpMultiShipLog
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Session\SessionManager $coreSession,
        \Webkul\Marketplace\Model\Orders $mpSalesModel,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\MpMultiShipping\Helper\Data $currentHelper,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\RequestInterface $requestInterface,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Quote\Model\Quote\Item\OptionFactory $optionFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Webkul\Marketplace\Model\ProductFactory $mpProductFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory,
        \Webkul\MarketplaceBaseShipping\Model\ShippingSettingRepository $shippingSettingRepository,
        \Webkul\MpMultiShipping\Logger\Logger $mpMultiShipLog,
        \Chilecompra\WebkulMpShipping\Helper\Data $shippingHelper,
        array $data = []
    ) {
    	$this->shippingHelper = $shippingHelper;
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $rateResultFactory,
            $rateMethodFactory,
            $regionFactory,
            $objectManager,
            $coreSession,
            $mpSalesModel,
            $checkoutSession,
            $customerSession,
            $currentHelper,
            $currencyFactory,
            $storeManager,
            $localeFormat,
            $jsonHelper,
            $requestInterface,
            $priceCurrency,
            $optionFactory,
            $customerFactory,
            $addressFactory,
            $mpProductFactory,
            $productFactory,
            $saleslistFactory,
            $shippingSettingRepository,
            $mpMultiShipLog,
            $data
        );
    }
	/**
     * sets request in case of product wise shipping
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @return void
     */
    public function setRequest(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        try {
            /* PHPSTAN - return type of getMultiShippingMode() is wrong in 3rd party module*/
            $enabledProductWiseMode = $this->getMultiShippingMode(); /** @phpstan-ignore-line */
            if ($enabledProductWiseMode) {
                $shippingdetail = [];
                $sellerMapping = $this->shippingHelper->getSellerIdFromQuote();
                foreach ($request->getAllItems() as $item) {
                    if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                        continue;
                    }
                    $sellerId = array_key_exists($item->getId(), $sellerMapping) ? $sellerMapping[$item->getId()] : 0;
                    $weight = $this->_getItemWeight($item);
                    $itemPrice = $item->getRowTotal();
                    /* PHPSTAN - return type of _getSellerOrigin() is wrong in 3rd party module*/
                    list($originPostcode, $originCountryId, $origRegionCode, $originCity) = $this->_getSellerOrigin($sellerId); /** @phpstan-ignore-line */
                    $itemId = $item->getId() ? $item->getId() : $item->getQuoteItemId();
                    $shippingdetail[$item->getProduct()->getId()] = [
                        'seller_id' => $sellerId,
                        'origin_postcode' => $originPostcode,
                        'origin_country_id' => $originCountryId,
                        'origin_region' => $origRegionCode,
                        'origin_city' => $originCity,
                        'items_weight' => $weight,
                        'total_amount'=> $itemPrice,
                        'product_name' => $item->getName(),
                        'qty' => $item->getQty(),
                        'item_id' => $itemId,
                        'price' => $item->getPrice()*$item->getQty(),
                    ];
                    $sellerProductDetails[$itemId][] = $item->getName().' x '.$item->getQty();
                }
            } else {
                return $this->setParentRequest($request);
            }
            $request->setSellerProductInfo($sellerProductDetails);
            if ($request->getShippingDetails()) {
                $shippingdetail = $request->getShippingDetails();
            }
            $request->setShippingDetails($shippingdetail);
            if ($request->getDestCountryId()) {
                $destCountry = $request->getDestCountryId();
            } else {
                $destCountry = self::USA_COUNTRY_ID;
            }
            $request->setDestCountryId($destCountry);
            if ($request->getDestPostcode()) {
                $request->setDestPostal($request->getDestPostcode());
            }
            $this->setRawRequest($request);
            return $this;
        } catch (\Exception $e) {
            /* PHPSTAN - mpMultiShipLog property dynamically declared in 3rd party module*/
            $this->mpMultiShipLog->addInfo($e->getMessage()); /** @phpstan-ignore-line */
            return $this->returnErrorFromConfig();
        }
    }
    /**
     * Prepare and set request to this instance.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function setParentRequest(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        $mpassignproductId = 0;
        $shippingdetail = [];
        $sellerProductDetails = [];
        $sellerMapping = $this->shippingHelper->getSellerIdFromQuote();
        foreach ($request->getAllItems() as $item) {
            if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                continue;
            }
            $sellerId = array_key_exists($item->getId(), $sellerMapping) ? $sellerMapping[$item->getId()] : 0;
            $weight = $this->_getItemWeight($item);
            $itemPrice = $item->getRowTotal();
            /* PHPSTAN - return type of _getSellerOrigin() is wrong in 3rd party module*/
            list($originPostcode, $originCountryId, $origRegionCode, $originCity) = $this->_getSellerOrigin($sellerId); /** @phpstan-ignore-line */
            if (empty($shippingdetail)) {
                array_push(
                    $shippingdetail,
                    [
                        'seller_id' => $sellerId,
                        'origin_postcode' => $originPostcode,
                        'origin_country_id' => $originCountryId,
                        'origin_region' => $origRegionCode,
                        'origin_city' => $originCity,
                        'items_weight' => $weight,
                        'total_amount'=> $itemPrice,
                        'product_name' => $item->getName(),
                        'qty' => $item->getQty(),
                        'item_id' => $item->getId(),
                        'price' => $item->getPrice()*$item->getQty(),
                    ]
                );
                $sellerProductDetails[$sellerId][] = $item->getName().'x'.$item->getQty();
            } else {
                $shipinfoflag = true;
                $index = 0;
                foreach ($shippingdetail as $itemship) {
                    if ($itemship['seller_id'] == $sellerId) {
                        $itemship['items_weight'] = $itemship['items_weight'] + $weight;
                        $itemship['total_amount']= $itemship['total_amount']+$itemPrice;
                        $itemship['product_name'] = $itemship['product_name'].','.$item->getName();
                        $itemship['item_id'] = $itemship['item_id'].','.$item->getId();
                        $itemship['qty'] = $itemship['qty'] + $item->getQty();
                        $itemship['price'] = $itemship['price'] + $item->getPrice()*$item->getQty();
                        $shippingdetail[$index] = $itemship;
                        $shipinfoflag = false;
                        $sellerProductDetails[$sellerId][] = $item->getName().' X '.$item->getQty();
                    }
                    ++$index;
                }
                if ($shipinfoflag == true) {
                    array_push(
                        $shippingdetail,
                        [
                            'seller_id' => $sellerId,
                            'origin_postcode' => $originPostcode,
                            'origin_country_id' => $originCountryId,
                            'origin_region' => $origRegionCode,
                            'origin_city' => $originCity,
                            'items_weight' => $weight,
                            'total_amount'=> $itemPrice,
                            'product_name' => $item->getName(),
                            'qty' => $item->getQty(),
                            'item_id' => $item->getId(),
                            'price' => $item->getPrice()*$item->getQty(),
                        ]
                    );
                    $sellerProductDetails[$sellerId][] = $item->getName().' X '.$item->getQty();
                }
            }
        }
        $request->setSellerProductInfo($sellerProductDetails);
        if ($request->getShippingDetails()) {
            $shippingdetail = $request->getShippingDetails();
        }
        $request->setShippingDetails($shippingdetail);
        if ($request->getDestCountryId()) {
            $destCountry = $request->getDestCountryId();
        } else {
            $destCountry = self::USA_COUNTRY_ID;
        }
        $request->setDestCountryId($destCountry);
        if ($request->getDestPostcode()) {
            $request->setDestPostal($request->getDestPostcode());
        }
        $this->setRawRequest($request);
        return $this;
    }

    /**
     * get product weight.
     *
     * @param object $item
     *
     * @return int
     */
    protected function _getItemWeight($item)
    {
        $weight = 0;
        if ($item->getHasChildren()) {
            $childWeight = 0;
            foreach ($item->getChildren() as $child) {
                if ($child->getProduct()->isVirtual()) {
                    continue;
                }
                $productWeight = $child->getProduct()->getWeight();
                $childWeight += $productWeight * $child->getQty();
            }
            $weight = $childWeight * $item->getQty();
        } else {
            $productWeight = $item->getProduct()->getWeight();
            $weight = $productWeight * $item->getQty();
            if ($item->getQtyOrdered()) {
                $weight = $productWeight * $item->getQtyOrdered();
            }
        }
        return $weight;
    }
}
