<?php

namespace Chilecompra\WebkulMpShipping\Rewrite\Webkul\Mpshipping\Model;

use Magento\Framework\Escaper;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Formax\RegionalCondition\Helper\Config as HelperConfig;
use Webkul\Mpshipping\Helper\Data as HelperData;
use Webkul\Mpshipping\Model\MpshippingDistFactory;
use Webkul\Mpshipping\Model\SellerLocationFactory;

class Carrier extends \Webkul\Mpshipping\Model\Carrier
{
    const UNLIMITED_COST = 99999999;

    /**
     * @var \Formax\RegionalCondition\Helper\Data
     */
    protected $helper;

    /**
     * @var int
     */
    protected $websiteId;

    /**
     * @var string
     */
    protected $websiteCode;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $dbConnection;

    /**
     * @var \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory
     */
    protected $regionalConditionCollection;

    /**
     * @var \Chilecompra\WebkulMpShipping\Helper\Data
     */
    protected $shippingHelper;

    private HelperData $helperData;
    private SerializerInterface $serializerInterface;
    protected $mpshippingDistModel;
    private Curl $curl;
    private Escaper $escaper;
    private SellerLocationFactory $sellerLocation;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface          $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory  $rateErrorFactory
     * @param \Psr\Log\LoggerInterface                                    $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory                  $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param SessionManager                                              $coreSession
     * @param \Magento\Checkout\Model\Session                             $checkoutSession
     * @param \Magento\Customer\Model\Session                             $customerSession
     * @param MpshippingmethodFactory                                     $shippingmethodFactory
     * @param MpshippingFactory                                           $mpshippingModel
     * @param MpshippingsetFactory                                        $mpshippingsetModel
     * @param HelperData                                                  $helperData
     * @param SerializerInterface                                         $serializerInterface
     * @param MpshippingDistFactory                                       $mpshippingDistModel
     * @param Curl                                                        $curl
     * @param Escaper                                                     $escaper
     * @param SellerLocationFactory                                       $sellerLocation
     * @param array                                                       $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Framework\Session\SessionManager $coreSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\App\RequestInterface $requestInterface,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Quote\Model\Quote\Item\OptionFactory $optionFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Webkul\Marketplace\Model\ProductFactory $mpProductFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory,
        \Webkul\MarketplaceBaseShipping\Model\ShippingSettingRepository $shippingSettingRepository,
        \Webkul\Mpshipping\Model\MpshippingmethodFactory $shippingmethodFactory,
        \Webkul\Mpshipping\Model\MpshippingFactory $mpshippingModel,
        \Webkul\Mpshipping\Model\MpshippingsetFactory $mpshippingsetModel,
        \Formax\RegionalCondition\Helper\Data $helper,
        \Magento\Framework\App\ResourceConnection $resource,
        \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $regionalConditionCollection,
        \Chilecompra\WebkulMpShipping\Helper\Data $shippingHelper,
        HelperData $helperData,
        SerializerInterface $serializerInterface,
        MpshippingDistFactory $mpshippingDistModel,
        Curl $curl,
        Escaper $escaper,
        SellerLocationFactory $sellerLocation,
        array $data = []
    ) {
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $rateResultFactory,
            $rateMethodFactory,
            $regionFactory,
            $coreSession,
            $checkoutSession,
            $customerSession,
            $currencyFactory,
            $storeManager,
            $localeFormat,
            $jsonHelper,
            $requestInterface,
            $priceCurrency,
            $optionFactory,
            $customerFactory,
            $addressFactory,
            $mpProductFactory,
            $productFactory,
            $saleslistFactory,
            $shippingSettingRepository,
            $shippingmethodFactory,
            $mpshippingModel,
            $mpshippingsetModel,
            $helperData,
            $serializerInterface,
            $mpshippingDistModel,
            $curl,
            $escaper,
            $sellerLocation,
            $data
        );
        $this->helper = $helper;
        $this->resource = $resource;
        $this->dbConnection = $this->resource->getConnection();
        $this->regionalConditionCollection = $regionalConditionCollection;
        $this->websiteId = $this->_storeManager->getWebsite()->getId();
        $this->websiteCode = $this->_storeManager->getWebsite()->getCode();
        $this->shippingHelper = $shippingHelper;
        $this->helperData = $helperData;
        $this->serializerInterface = $serializerInterface;
        $this->mpshippingDistModel = $mpshippingDistModel;
        $this->curl = $curl;
        $this->escaper = $escaper;
        $this->sellerLocation = $sellerLocation;
    }

    /**
     * Prepare and set request to this instance.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function setRequest(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        /**
         * @todo: Refactor $this->shippingHelper->getSellerIdFromQuote();
         *
         * Calls to $this->shippingHelper->getSellerIdFromQuote()
         * causes infinite loops, so we are initializing it as an empty array
         *
         * $sellerMapping = []; instead of
         * $sellerMapping = $this->shippingHelper->getSellerIdFromQuote()
         */

        $sellerMapping = $this->shippingHelper->getSellerIdFromQuote();
        $shippingdetail = [];
        $sellerProductDetails = [];
        foreach ($request->getAllItems() as $item) {
            if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                continue;
            }
            $sellerId = array_key_exists($item->getId(), $sellerMapping) ? $sellerMapping[$item->getId()] : 0;
            $weight = $this->_getItemWeight($item);
            $itemPrice = $item->getRowTotal();
            /* PHPSTAN - return type of _getSellerOrigin() is wrong in 3rd party module*/
            list($originPostcode, $originCountryId, $origRegionCode, $originCity) = $this->_getSellerOrigin($sellerId); /** @phpstan-ignore-line */

            if (empty($shippingdetail)) {
                array_push(
                    $shippingdetail,
                    [
                        'seller_id' => $sellerId,
                        'origin_postcode' => $originPostcode,
                        'origin_country_id' => $originCountryId,
                        'origin_region' => $origRegionCode,
                        'origin_city' => $originCity,
                        'items_weight' => $weight,
                        'total_amount'=> $itemPrice,
                        'product_name' => $item->getName(),
                        'qty' => $item->getQty(),
                        'item_id' => $item->getId(),
                        'price' => $item->getPrice()*$item->getQty(),
                    ]
                );
                $sellerProductDetails[$sellerId][] = $item->getName().'x'.$item->getQty();
            } else {
                $shipinfoflag = true;
                $index = 0;
                foreach ($shippingdetail as $itemship) {
                    if ($itemship['seller_id'] == $sellerId) {
                        $itemship['items_weight'] = $itemship['items_weight'] + $weight;
                        $itemship['total_amount']= $itemship['total_amount']+$itemPrice;
                        $itemship['product_name'] = $itemship['product_name'].','.$item->getName();
                        $itemship['item_id'] = $itemship['item_id'].','.$item->getId();
                        $itemship['qty'] = $itemship['qty'] + $item->getQty();
                        $itemship['price'] = $itemship['price'] + $item->getPrice()*$item->getQty();
                        $shippingdetail[$index] = $itemship;
                        $shipinfoflag = false;
                        $sellerProductDetails[$sellerId][] = $item->getName().' X '.$item->getQty();
                    }
                    ++$index;
                }
                if ($shipinfoflag == true) {
                    array_push(
                        $shippingdetail,
                        [
                            'seller_id' => $sellerId,
                            'origin_postcode' => $originPostcode,
                            'origin_country_id' => $originCountryId,
                            'origin_region' => $origRegionCode,
                            'origin_city' => $originCity,
                            'items_weight' => $weight,
                            'total_amount'=> $itemPrice,
                            'product_name' => $item->getName(),
                            'qty' => $item->getQty(),
                            'item_id' => $item->getId(),
                            'price' => $item->getPrice()*$item->getQty(),
                        ]
                    );
                    $sellerProductDetails[$sellerId][] = $item->getName().' X '.$item->getQty();
                }
            }
        }

        $request->setSellerProductInfo($sellerProductDetails);

        if ($request->getShippingDetails()) {
            $shippingdetail = $request->getShippingDetails();
        }
        $request->setShippingDetails($shippingdetail);

        if ($request->getDestCountryId()) {
            $destCountry = $request->getDestCountryId();
        } else {
            $destCountry = self::USA_COUNTRY_ID;
        }

        $request->setDestCountryId($destCountry);

        if ($request->getDestPostcode()) {
            $request->setDestPostal($request->getDestPostcode());
        }
        $this->setRawRequest($request);

        if ($request->getDestRegionId()) {
            $regionId = $request->getDestRegionId();
        } else {
            $regionId = '*';
        }
        $request->setDestRegionId($regionId);
        return $this;
    }

    /**
     * get product weight.
     *
     * @param object $item
     *
     * @return int
     */
    protected function _getItemWeight($item)
    {
        $weight = 0;
        if ($item->getHasChildren()) {
            $childWeight = 0;
            foreach ($item->getChildren() as $child) {
                if ($child->getProduct()->isVirtual()) {
                    continue;
                }
                $productWeight = $child->getProduct()->getWeight();
                $childWeight += $productWeight * $child->getQty();
            }
            $weight = $childWeight * $item->getQty();
        } else {
            $productWeight = $item->getProduct()->getWeight();
            $weight = $productWeight * $item->getQty();
            if ($item->getQtyOrdered()) {
                $weight = $productWeight * $item->getQtyOrdered();
            }
        }
        return $weight;
    }

    /**
     * Calculate the rate according to Tabel Rate shipping defined by the sellers.
     *
     * @param RateRequest $request
     * @return Result
     */
    public function getShippingPricedetail(RateRequest $request)
    {
        $requestData = $request;
        $submethod = [];
        $shippinginfo = [];
        $msg = '';
        $msgArray = [];
        $handling = 0;
        $totalPriceArr = [];
        $priceArr = [];
        $flag = false;
        $check = false;
        $returnError = false;
        $storePickupStatus = false;

        foreach ($requestData->getShippingDetails() as $shipdetail) {
            $thisMsg = false;
            $priceArr = [];
            $price = 0;
            $sellerId = isset($shipdetail['seller_id']) ? (int)$shipdetail['seller_id'] : 0;

            /*Calculate price itemwise for seller store pickup*/
            $itemPriceDetails = [];
            $items = explode(',', $shipdetail['item_id']);

            if ($sellerId === 0 && isset($shipdetail['item_id'])) {
                /**
                 * @todo: Refactor $this->getSellerId();
                 *
                 * Calls to $this->getSellerId()
                 * causes infinite loops, so we are initializing it as 0
                 *
                 * $sellerId = 0; instead of
                 * $sellerId = $this->getSellerId();
                 */
                $sellerId = $this->getSellerId();
                $shipdetail['seller_id'] = $sellerId;
            }

            $newShipderails = [];
            $newShipderails['seller_id'] = $shipdetail['seller_id'];
            $newShipderails['total_amount'] = $shipdetail['total_amount'];
            $newShipderails['product_name'] = $shipdetail['product_name'];
            $shipping = $this->getShippingPriceRates($shipdetail, $requestData);

            if (isset($shipdetail['item_weight_details']) &&
                isset($shipdetail['item_product_price_details']) &&
                isset($shipdetail['item_qty_details'])
            ) {
                $storePickupStatus = true;
                $itemPriceArr = [];
                $itemCostArr = [];
                foreach ($items as $itemId) {
                    $newShipderails['items_weight'] = $shipdetail['item_weight_details'][$itemId];
                    $newShipderails['price'] = $shipdetail['item_product_price_details'][$itemId] * $shipdetail['item_qty_details'][$itemId];
                    $shipping = $this->getShippingPriceRates($newShipderails, $requestData);
                    $itemPriceArr = $this->getPriceArrForRate($shipping);
                    $itemPriceArrWithMethodId = $this->getSubMethodForItemPrice($itemPriceArr);
                    $itemPriceDetails[$itemId] = $itemPriceArrWithMethodId;
                }
            }

            /*End seller store pickup*/
            if ($shipping && (
                (is_object($shipping) && $shipping->getSize() != 0) || (is_array($shipping) && count($shipping) != 0)
                )
            ) {
                $priceArr = $this->getPriceArrForRate($shipping);
            } else {
                list($msg, $msgArray) = $this->getErrorMsg($msgArray, $shipdetail, false);
            }

            if ($this->isMultiShippingActive() || $storePickupStatus) {
                if (empty($priceArr)) {
                    $totalPriceArr = [];
                    $flag = true;
                    $debugData['result'] = ['error' => 1, 'errormsg' => $msg];
                    return [];
                }
            } else {
                if (!empty($totalPriceArr)) {
                    foreach ($priceArr as $method => $price) {
                        if (array_key_exists($method, $totalPriceArr)) {
                            $check = true;
                            $totalPriceArr[$method] = $totalPriceArr[$method] + $priceArr[$method];
                        } else {
                            $thisMsg = true;
                            unset($priceArr[$method]);
                        }
                        $flag = $check == true ? false : true;
                    }
                } else {
                    $totalPriceArr = $priceArr;
                }
            }

            if (!empty($priceArr)) {
                foreach ($totalPriceArr as $method => $price) {
                    if (!array_key_exists($method, $priceArr)) {
                        unset($totalPriceArr[$method]);
                    }
                }
            } else {
                $totalPriceArr = [];
                $flag = true;
            }

            if ($flag) {
                if ($thisMsg) {
                    list($msg, $msgArray) = $this->getErrorMsg($msgArray, $shipdetail, true);
                }

                $returnError = true;
                $debugData['result'] = ['error' => 1, 'errormsg' => $msg];
            }

            $submethod = $this->getSubMethodsForRate($priceArr);
            $handling = $handling + $price;
            if (!isset($shipdetail['item_id_details'])) {
                $shipdetail['item_id_details'] = [];
            }

            if (!isset($shipdetail['item_name_details'])) {
                $shipdetail['item_name_details'] = [];
            }

            if (!isset($shipdetail['item_qty_details'])) {
                $shipdetail['item_qty_details'] = [];
            }

            array_push(
                $shippinginfo,
                [
                    'seller_id' => $shipdetail['seller_id'],
                    'methodcode' => $this->_code,
                    'shipping_ammount' => $price,
                    'product_name' => $shipdetail['product_name'],
                    'submethod' => $submethod,
                    'item_ids' => $shipdetail['item_id'],
                    'item_price_details' => $itemPriceDetails,
                    'item_id_details' => $shipdetail['item_id_details'],
                    'item_name_details' => $shipdetail['item_name_details'],
                    'item_qty_details' => $shipdetail['item_qty_details']
                ]
            );
        }

        if ($returnError && !empty($debugData)) {
            if ($this->isMultiShippingActive() || $storePickupStatus) {
                return $debugData;
            }

            return $this->_parseXmlResponse($debugData);
        }

        // Additional calc Kilo
        $totalpric = ['totalprice' => $totalPriceArr, 'costarr' => $priceArr];
        $isAdditionalKilo = false;

        if (!empty($shipping)) {
            foreach ($shipping as $__item) {
                $weightTo = is_object($__item) ? $__item->getWeightTo() : $__item;

                if ($weightTo == self::UNLIMITED_COST) {
                    $isAdditionalKilo = true;
                }
            }

            if ($isAdditionalKilo && !empty($shipdetail)) {
                $largePrice = $this->getLargePrice(
                    $requestData->getDestCountryId(),
                    (int)$requestData->getDestRegionId(),
                    (int)$requestData->getDestPostcode(),
                    (float)$shipdetail['items_weight'],
                    (int)$shipdetail['seller_id']
                );

                $__newArrayPrice = [];
                if ($largePrice !== false) {
                    foreach($totalPriceArr as $__key => $__row) {
                        $__additionalKilo = (float)$shipdetail['items_weight'] - (float)$largePrice->getWeightTo();
                        $__newArrayPrice[$__key] = ($largePrice->getPrice()+($__additionalKilo*(float)$__row));
                    }

                    $totalpric = [
                        'totalprice' => $__newArrayPrice,
                        'costarr' => $__newArrayPrice
                    ];
                }
            }
        }

        // Calc factor for website Aseo
        if (!empty($shipdetail)) {
            $factor = $this->calculateFactor(
                (int)$requestData->getDestRegionId(),
                (int)$requestData->getDestPostcode(),
                (int)$shipdetail['seller_id']
            );
        } else {
            $factor = -1;
        }

        if ($factor < 0 && !empty($shipdetail)) {
            list($msg, $msgArray) = $this->getErrorMsg($msgArray, $shipdetail, false);
            $debugData['result'] = ['error' => 1, 'errormsg' => $msg];
            if ($this->isMultiShippingActive() || $storePickupStatus) {
                return $debugData;
            }

            return $this->_parseXmlResponse($debugData);
        } else {
            if (isset($totalpric['totalprice'])) {
                $factorArrayPrice = [];
                foreach($totalpric['totalprice'] as $fKey => $fRow) {
                    $factorArrayPrice[$fKey] = $factor*(float)$fRow;
                }
                $totalpric = [
                    'totalprice' => $factorArrayPrice,
                    'costarr' => $factorArrayPrice
                ];
            }
            $result = ['handlingfee' => $totalpric, 'shippinginfo' => $shippinginfo, 'error' => 0];
        }

        $shippingAll = $this->_coreSession->getShippingInfo();
        $shippingAll[$this->_code] = $result['shippinginfo'];
        $this->_coreSession->setShippingInfo($shippingAll);

        if ($this->isMultiShippingActive() || $storePickupStatus) {
            return $result;
        }

        return $this->_parseXmlResponse($totalpric);
    }

    /**
     * [getShippingcollectionAccordingToDetails Calculate shipping according to Postal Code]
     * @param  string $countryId
     * @param  int $sellerId
     * @param  string $regionId
     * @param  int $postalCode
     * @param  float $weight
     * @return \Webkul\Mpshipping\Model\Mpshipping $shipping
     */
    public function getShippingcollectionAccordingToDetails($countryId, $sellerId, $regionId, $postalCode, $weight)
    {
        $shipping = $this->_mpShippingModel->create()
            ->getCollection()
            ->addFieldToFilter('dest_country_id', ['eq' => $countryId])
            ->addFieldToFilter('partner_id', ['eq' => $sellerId])
            ->addFieldToFilter('website_id', ['eq' => $this->websiteId])
            ->addFieldToFilter(
                ['dest_region_id','dest_region_id'],
                [
                    ['eq'=>$regionId],
                    ['eq'=>'*']
                ]
            )
            ->addFieldToFilter(
                ['dest_zip','dest_zip'],
                [
                    ['lteq'=>$postalCode],
                    ['eq'=>'*']
                ]
            )
            ->addFieldToFilter(
                ['dest_zip_to','dest_zip_to'],
                [
                    ['gteq'=>$postalCode],
                    ['eq'=>'*']
                ]
            )
            ->addFieldToFilter('weight_from', ['lteq' => $weight])
            ->addFieldToFilter('weight_to', ['gteq' => $weight]);
        return $shipping;
    }

    /**
     * Retrive shippping cost large dimension
     *
     * @param string $countryId
     * @param int $regionId
     * @param int $postalCode
     * @param int $weight
     * @param int $sellerId
     * @return mixed bool/object
     */
    private function getLargePrice($countryId, $regionId, $postalCode, $weight, $sellerId)
    {
        $shipping = $this->_mpShippingModel->create()
            ->getCollection()
            ->addFieldToFilter('dest_country_id', $countryId)
            ->addFieldToFilter('dest_region_id', $regionId)
            ->addFieldToFilter('dest_zip', $postalCode)
            ->addFieldToFilter('dest_zip_to', $postalCode)
            ->addFieldToFilter('weight_to', ['lt' => $weight])
            ->addFieldToFilter('partner_id', $sellerId)
            ->addFieldToFilter('website_id', $this->websiteId)
            ->setOrder('weight_to', 'DESC')
            ->setPageSize(1);

        if ($shipping) {
            foreach ($shipping as $item) {
                return $item;
            }
        }

        return false;
    }

    /**
     * Retrive factor to calculate shipping cost
     *
     * @param int $regionId
     * @param int $comunaId
     * @param int $sellerId
     * @return int
     */
    private function calculateFactor($regionId, $comunaId, $sellerId)
    {
        $websiteId = $this->helper->getCurrentWebsite();
        $factor = -1;
        if ($this->websiteCode === HelperConfig::WEBSITE_CODE_ASEO) {
            if ((int)$regionId > 0 && (int)$comunaId > 0 && (int)$sellerId > 0) {
                $collection = $this->regionalConditionCollection->create();
                $collection->addFieldToFilter('main_table.region_id', $regionId);
                $collection->join(
                    ['s' => $this->dbConnection->getTableName('dccp_regional_condition_seller')],
                    'main_table.id = s.regional_condition_id',
                    []
                )->join(
                    ['c' => $this->dbConnection->getTableName('dccp_comuna')],
                    'main_table.region_id = c.region_id',
                    ['tipification']
                );
                $collection->addFieldToFilter('s.website_id', $websiteId)
                    ->addFieldToFilter('c.id', $comunaId)
                    ->addFieldToFilter('s.seller_id', $sellerId)
                    ->setPageSize(1);

                if ($collection) {
                    foreach ($collection as $row) {
                        $factorValue = strtolower($row->getTipification()) === 'rural'
                            ? $row->getRuralFactor() : $row->getUrbanFactor();
                        $factor = ($factorValue === null || $factorValue === '') ? -1 : (float)$factorValue;
                    }
                }
            }
        } else {
            $factor = 1;
        }

        return $factor;
    }

    /**
     * Get Seller ID current quote
     *
     * @return bool|int
     */
    public function getSellerId()
    {
        $items = $this->_checkoutSession->getQuote()->getAllVisibleItems();
        foreach ($items as $item) {
            $mpassignproductId = $this->_getAssignProduct($item);
            $sellerId = $this->_getSellerId($mpassignproductId, $item->getProductId());
            return $sellerId;
        }

        return false;
    }
}
