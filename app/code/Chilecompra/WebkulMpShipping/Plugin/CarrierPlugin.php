<?php

namespace Chilecompra\WebkulMpShipping\Plugin;

use Webkul\Mpshipping\Model\Carrier;

class CarrierPlugin
{
    /**
     * The original \Webkul\Mpshipping\Model\Carrier::getPriceArrForRate
     * throws an error when it attempts to call a method on a flot val.
     * We are checking the object type and perform the correct call.
     * We don't want to call the original method and that means we are not calling proceed
     *
     * @param Carrier $subject
     * @param callable $proceed
     * @param object $shipping
     * @param array $priceArr
     * @return array
     */
    public function aroundGetPriceArrForRate(
        Carrier $subject,
        callable $proceed,
        $shipping,
        $priceArr = []
    ): array {
        foreach ($shipping as $ship) {
            $price = is_object($ship) ? floatval($ship->getPrice()) : $ship;
            $shipMethodId = is_object($ship) ? $ship->getShippingMethodId() : false;
            if ($shipMethodId) {
                $shipMethodName = $subject->getShipMethodNameById($shipMethodId);
            } else {
                $shipMethodName = $subject->getConfigData('title');
            }
            $priceArr[$shipMethodName] = $price;
        }
        return $priceArr;
    }
}
