<?php

namespace Chilecompra\WebkulMpShipping\Model\Quote\Address\Total;

class Subtotal extends \Magento\Quote\Model\Quote\Address\Total\Subtotal
{
    /**
     * Processing calculation of row price for address item
     *
     * @param AddressItem|Item $item
     * @param int $finalPrice
     * @param int $originalPrice
     * @return $this
     */
    protected function _calculateRowTotal($item, $finalPrice, $originalPrice = null)
    {
        if (!$originalPrice) {
            $originalPrice = $finalPrice;
        }

        $quoteItem = $item->getQuoteItem();
        $customPrice = $item->getCustomPrice();

        if($customPrice) {
            $item->setPrice($customPrice)->setBaseOriginalPrice($customPrice);
        } else if($quoteItem) {
            $customPrice = $quoteItem->getCustomPrice();

            if($customPrice){
                $item->setPrice($customPrice)->setBaseOriginalPrice($customPrice);
            }
        } else {
            $item->setPrice($finalPrice)->setBaseOriginalPrice($originalPrice);
        }

        $item->calcRowTotal();
        return $this;
    }
}