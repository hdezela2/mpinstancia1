<?php
/**
 * Created By : Rohan Hapani
 */
namespace MageMoto\ElasticsearchStoreSwitch\Model\Source;
use Magento\Framework\Option\ArrayInterface;
class Store implements ArrayInterface
{
    protected $storeRepository;
    /**
     * @param EavConfig $eavConfig
     */
    public function __construct(
        \Magento\Store\Api\StoreRepositoryInterface $storeRepository
    ) {
        $this->storeRepository = $storeRepository;
    }
    public function toOptionArray()
    {
        $storeArray = [];
        $storeList = $this->storeRepository->getList();
        
        foreach ($storeList as $key => $value) {
            if ($value->getStoreId() != 0) {
                $storeArray[] = [
                    'value' => $value->getStoreId(),
                    'label' => $value->getName(),
                ];
            }
        }
        return $storeArray;
    }
}