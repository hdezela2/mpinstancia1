<?php

namespace MageMoto\ElasticsearchStoreSwitch\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;

class Store extends Template
{
  /** Selected Store config path */
  const XML_PATH_EMAIL_RECIPIENT = 'elasticsearch_storeswitch/general/store_multiselect';

  /** @var ScopeConfigInterface */
  private $scopeConfig;

  public function __construct(
    Context $context,
    ScopeConfigInterface $scopeConfig,
    array $data = []
  ){
    $this->scopeConfig = $scopeConfig;
    parent::__construct($context, $data);
  }

  public function getSelectedStoreIds()
  {
    $storeScope = ScopeInterface::SCOPE_STORE;

    return $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, $storeScope);
  }

    /**
     * @throws NoSuchEntityException
     */
    public function getSelectedStoreValue(): array
  {
    $storeId = $this->getSelectedStoreIds();
    $storeIds = explode(",", $storeId);
    $storeArray = [];
    foreach ($storeIds as $value) {
      $storeScope = $this->_storeManager->getStore($value);

      $storeArray[] = [
          'value' => $storeScope->getStoreId(),
          'label' => $storeScope->getName(),
          'code' => $storeScope->getCode(),
      ];
    }
    return $storeArray;
  }

    /**
     * @throws NoSuchEntityException
     */
    public function getCurrentStoreId(): int
  {
    return $this->_storeManager->getStore()->getId();
  }

    /**
     * @throws NoSuchEntityException
     */
    public function getSecureBaseUrl(): string
    {
    return $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_WEB,true);
  }
}