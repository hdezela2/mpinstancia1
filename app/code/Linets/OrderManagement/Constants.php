<?php
namespace Linets\OrderManagement;

class Constants
{
	const XML_PATH_ORDER_CANCELLATION_ENABLE = 'sales/order_management/enable';
	const XML_PATH_ORDER_CANCELLATION_WEBSITE = 'sales/order_management/store';
}