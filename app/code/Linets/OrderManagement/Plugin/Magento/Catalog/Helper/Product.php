<?php
/**
 * Copyright © sdfsdf All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\OrderManagement\Plugin\Magento\Catalog\Helper;

use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Store\Model\StoreManagerInterface;

class Product
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    )
    {
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Magento\Catalog\Helper\Product $subject
     * @param $result
     * @param $buyRequest
     * @param $params
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterAddParamsToBuyRequest(
        \Magento\Catalog\Helper\Product $subject,
                                        $result,
                                        $buyRequest,
                                        $params
    )
    {
        if($this->storeManager->getWebsite()->getCode() == InsumosConstants::WEBSITE_CODE){
            $newBuyRequest = array_merge($buyRequest->getData(), $params->getCurrentConfig()->getData());
            $result->setData($newBuyRequest);
        }


        return $result;
    }
}