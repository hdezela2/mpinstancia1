<?php
declare(strict_types = 1);

namespace Linets\OrderManagement\Plugin;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Block\Order\History;
use Magento\Store\Model\StoreManagerInterface;

class HistoryPlugin
{
    /** @var StoreManagerInterface */
    protected StoreManagerInterface $storeManager;

    /**
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    )
    {
        $this->storeManager = $storeManager;
    }


    /**
     * Filter to display purchase order by Convention (StoreId)
     *
     * @throws NoSuchEntityException
     */
    public function afterGetOrders(History $subject, $result)
    {
        if ($result) {
            $result->addFieldToFilter(
                'store_id',
                $this->getStoreId()
            );
        }

        return $result;
    }

    /**
     * Get Store Id
     *
     * @throws NoSuchEntityException
     */
    public function getStoreId(): int
    {
        return (int)$this->storeManager->getStore()->getId();
    }
}
