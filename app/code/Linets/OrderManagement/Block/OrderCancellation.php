<?php

namespace Linets\OrderManagement\Block;

use Linets\OrderManagement\Constants;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;

class OrderCancellation extends Template
{
	/**
	 * @var ScopeConfigInterface
	 */
	private $configManager;

	/**
	 * @var Registry
	 */
	private $registry;

	/**
	 * OrderCancellation constructor.
	 * @param Template\Context $context
	 * @param ScopeConfigInterface $configManager
	 * @param Registry $registry
	 * @param array $data
	 */
	public function __construct(
		Template\Context $context,
		ScopeConfigInterface $configManager,
		Registry $registry,
		array $data = []
	) {
		parent::__construct($context, $data);
		$this->configManager = $configManager;
		$this->registry = $registry;
	}

	/**
	 */
	public function isFeatureEnable(): bool
	{
		$featureEnable = $this->configManager->getValue(Constants::XML_PATH_ORDER_CANCELLATION_ENABLE);
		$canCancelOrder = $this->registry->registry('current_order')->canCancel();

		return ($featureEnable && $canCancelOrder);
	}

	/**
	 * @return bool
	 */
	public function isWebsiteEnable(): bool
	{
		$enabledWebsites = $this->configManager->getValue(Constants::XML_PATH_ORDER_CANCELLATION_WEBSITE);
                $enabledWebsitesArray = $enabledWebsites ? explode(',', $enabledWebsites) : [];
		$orderStoreId = $this->registry->registry('current_order')->getStoreId();

		return (in_array($orderStoreId, $enabledWebsitesArray));
	}

	/**
	 * @return string
	 */
	public function getCancelOrderUrl(): string
	{
		return $this->_urlBuilder->getUrl(
			'sales_management/order/cancel/',
			[
				'order_id' => $this->registry->registry('current_order')->getId()
			]
		);
	}
}
