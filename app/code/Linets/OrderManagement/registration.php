<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
	ComponentRegistrar::MODULE,
	'Linets_OrderManagement',
	__DIR__
);
