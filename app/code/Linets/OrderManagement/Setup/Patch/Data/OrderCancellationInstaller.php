<?php
declare(strict_types=1);

namespace Linets\OrderManagement\Setup\Patch\Data;

use Linets\GasSetup\Constants as WebsiteConstants;
use Linets\OrderManagement\Constants as OrderManagementConstants;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class OrderCancellationInstaller implements DataPatchInterface
{
	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;

	/**
	 * @var StoreRepositoryInterface
	 */
	private $storeManager;

	/**
	 * @var WriterInterface
	 */
	private $writer;

	/**
	 * @var ScopeConfigInterface
	 */
	private $config;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * OrderCancellationInstaller constructor.
	 * @param ModuleDataSetupInterface $moduleDataSetup
	 * @param StoreRepositoryInterface $storeManager
	 * @param WriterInterface $writer
	 * @param ScopeConfigInterface $config
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		StoreRepositoryInterface $storeManager,
		WriterInterface $writer,
		ScopeConfigInterface $config,
		LoggerInterface $logger
	) {
		$this->moduleDataSetup = $moduleDataSetup;
		$this->storeManager = $storeManager;
		$this->writer = $writer;
		$this->config = $config;
		$this->logger = $logger;
	}

    /**
     * @return $this
     */
	public function apply()
	{
		$this->moduleDataSetup->startSetup();

		try {
			$gasStore = $this->storeManager->get(WebsiteConstants::GAS_STORE_CODE);
			if ($gasStore->getId()) {
				$gasStoreId = $gasStore->getId();
				$this->writer->save(
					OrderManagementConstants::XML_PATH_ORDER_CANCELLATION_WEBSITE,
					$gasStoreId
				);
				$this->writer->save(
					OrderManagementConstants::XML_PATH_ORDER_CANCELLATION_ENABLE,
					'1'
				);
			}
		} catch (NoSuchEntityException $e) {
			$this->logger->critical('Unable to set ' . WebsiteConstants::GAS_STORE_CODE . ' order cancellation config');
		}

		$this->moduleDataSetup->endSetup();
        return $this;
	}

	/**
	 * @return array
	 */
	public static function getDependencies(): array
	{
		return [];
	}

	/**
	 * @return array
	 */
	public function getAliases(): array
	{
		return [];
	}
}
