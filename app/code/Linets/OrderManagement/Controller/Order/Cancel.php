<?php
declare(strict_types=1);

namespace Linets\OrderManagement\Controller\Order;

use Exception;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\OrderRepository;

class Cancel implements HttpGetActionInterface
{
	/**
	 * @var RequestInterface
	 */
	private $request;

	/**
	 * @var OrderManagementInterface
	 */
	private $orderManagement;

	/**
	 * @var ManagerInterface
	 */
	private $messageManager;

	/**
	 * @var ResultFactory
	 */
	private $resultFactory;

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	/**
	 * Cancel constructor.
	 * @param RequestInterface $request
	 * @param OrderRepository $orderRepository
	 * @param OrderManagementInterface $orderManagement
	 * @param ManagerInterface $messageManager
	 * @param ResultFactory $resultFactory
	 */
	public function __construct(
		RequestInterface $request,
		OrderRepository $orderRepository,
		OrderManagementInterface $orderManagement,
		ManagerInterface $messageManager,
		ResultFactory $resultFactory
	) {
		$this->request = $request;
		$this->orderRepository = $orderRepository;
		$this->orderManagement = $orderManagement;
		$this->messageManager = $messageManager;
		$this->resultFactory = $resultFactory;
	}

	/**
	 * @throws Exception
	 */
	public function execute()
	{
		$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
		try {
			$orderId = $this->request->getParam('order_id');
			$order = $this->orderRepository->get($orderId);
			if (!$order->canCancel()) {
				throw new Exception('No se puede cancelar este pedido.');
			}

			$this->orderManagement->cancel($order->getId());
			$this->messageManager->addSuccessMessage(__('El pedido ha sido cancelado.'));
			$resultRedirect->setUrl('/sales/order/history/');

			return $resultRedirect;
		} catch (InputException | NoSuchEntityException $e) {
			$this->messageManager->addErrorMessage(__($e->getMessage()));

			return $resultRedirect->setUrl('*/*');
		}
	}
}
