<?php

namespace Linets\OrderManagement\Controller\Order;

use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Framework\App\Action;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Sales\Controller\AbstractController\OrderLoaderInterface;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\MpAssignProduct\Controller\Order\Reorder as WebkulReorder;
use Webkul\MpAssignProduct\Model\AssociatesFactory;

class Reorder extends WebkulReorder
{

    /**
     * @var \Webkul\MpAssignProduct\Model\AssociatesFactory
     */
    private $associatesFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        Action\Context                             $context,
        OrderLoaderInterface                       $orderLoader,
        Registry                                   $registry,
        \Magento\Checkout\Model\CartFactory        $cartFactory,
        \Magento\Checkout\Model\Session            $session,
        \Webkul\MpAssignProduct\Helper\Data        $helper,
        \Webkul\MpAssignProduct\Model\QuoteFactory $quoteFactory,
        \Magento\Checkout\Model\Session            $checkoutSession,
        AssociatesFactory                          $associatesFactory,
        StoreManagerInterface                      $storeManager
    )
    {
        parent::__construct($context, $orderLoader, $registry, $cartFactory, $session);
        $this->associatesFactory = $associatesFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        if ($this->storeManager->getStore()->getCode() == InsumosConstants::STORE_VIEW_CODE) {
            $result = $this->_orderLoader->load($this->_request);
            $productIds = [];
            if ($result instanceof \Magento\Framework\Controller\ResultInterface) {
                return $result;
            }
            $cart = $this->_cart->create();
            $order = $this->_coreRegistry->registry('current_order');
            $redirect = $this->resultRedirectFactory->create();
            $orderItems = $order->getAllVisibleItems();

            foreach ($orderItems as $item) {
                try {
                    $info = $item->getProductOptionByCode('info_buyRequest');
                    $associate = $this->associatesFactory->create();
                    $associate->load($info['associate_id']);
                    $qty = $item->getQtyOrdered();
                    if ($associate->getStatus() != '1') {
                        throw new LocalizedException(__('The product %1 is not available right now.', $item->getProduct()->getName()));
                    }
                    $existingItemData = $this->getExistingItemQty($cart, $item->getProduct()->getEntityId());
                    if ($existingItemData !== false) {
                        $qty += $existingItemData['qty'];
                        /** @var \Magento\Quote\Model\Quote\Item $existingItem */
                        $existingItem = $existingItemData['item'];
                        $existingItem->setQty($qty);
                        $cart->updateItem($existingItem->getId(), $existingItem->getBuyRequest());
                    }else {
                        $cart->addOrderItem($item);
                    }

                    $productIds[] = $item->getProduct()->getEntityId();
                } catch (LocalizedException $e) {
                    $msg = $e->getMessage();
                    if ($this->_session->getUseNotice(true)) {
                        $this->messageManager->addNoticeMessage($msg);
                    } else {
                        $this->messageManager->addErrorMessage($msg);
                    }
                    return $redirect->setPath('sales/order/history');
                } catch (\Exception $e) {
                    $msg = 'We can\'t add this item to your shopping cart right now.';
                    $this->messageManager->addExceptionMessage($e, __($msg));
                }
            }
            $cart->save();
            $this->triggerItemsSaveComplete($cart, $productIds);
            return $redirect->setPath('checkout/cart');
        }
        return parent::execute();
    }

    /**
     * @param \Magento\Checkout\Model\Cart $cart
     * @param array $productIds
     */
    public function triggerItemsSaveComplete(\Magento\Checkout\Model\Cart $cart, array $productIds = [])
    {
        foreach ($cart->getQuote()->getAllVisibleItems() as $item) {
            if (!in_array($item->getProduct()->getEntityId(), $productIds)) {
                continue;
            }
            $info = $item->getOptionByCode('info_buyRequest');
            $this->_eventManager->dispatch(
                'checkout_cart_add_product_complete',
                [
                    'product' => $item->getProduct()->getEntityId(),
                    'info' => json_decode($info->getValue(), true),
                    'request' => $this->getRequest(),
                    'response' => $this->getResponse()
                ]
            );
        }
    }

    /**
     * @param \Magento\Checkout\Model\Cart $cart
     * @param int $productId
     * @return array|bool
     */
    public function getExistingItemQty(\Magento\Checkout\Model\Cart $cart, int $productId)
    {
        foreach ($cart->getQuote()->getAllVisibleItems() as $item) {
            if ($item->getProduct()->getEntityId() == $productId) {
                return ['qty'=>$item->getQty(), 'item' => $item];
            }
        }
        return false;
    }

}
