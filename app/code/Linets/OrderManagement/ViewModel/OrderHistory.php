<?php


namespace Linets\OrderManagement\ViewModel;


use Webkul\Marketplace\Model\ResourceModel\Orders;
use Webkul\Marketplace\Model\OrdersFactory;

class OrderHistory implements \Magento\Framework\View\Element\Block\ArgumentInterface
{

    /**
     * @var Orders
     */
    private $orders;
    /**
     * @var OrdersFactory
     */
    private $ordersFactory;

    public function __construct(
        Orders $orders,
        OrdersFactory $ordersFactory
    )
    {
        $this->orders = $orders;
        $this->ordersFactory = $ordersFactory;
    }
    public function getSellerIdFromOrder($orderId){
        $orderModel = $this->ordersFactory->create();
        $this->orders->load($orderModel,$orderId,'order_id');
        return $orderModel->getSellerId();
    }
}