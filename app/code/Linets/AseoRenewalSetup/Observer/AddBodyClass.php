<?php

namespace Linets\AseoRenewalSetup\Observer;

use Linets\AseoRenewalSetup\Model\AseoRenewalConstants as Constants;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Page\Config;
use Magento\Store\Model\StoreManagerInterface;


class AddBodyClass implements ObserverInterface
{
    protected Config $config;
    protected StoreManagerInterface $storeManager;

    public function __construct(
        Config $config,
        StoreManagerInterface $storeManager
    ) {
        $this->config = $config;
        $this->storeManager = $storeManager;
    }

    public function execute(Observer $observer)
    {
        $store = $this->storeManager->getStore();
        $storeCode = $store->getCode();
        if (in_array($storeCode,[
            Constants::STORE_VIEW_CODE
        ])) {
            $this->config->addBodyClass($storeCode);
        }
    }
}
