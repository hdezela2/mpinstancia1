<?php

namespace Linets\AseoRenewalSetup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Linets\AseoRenewalSetup\Api\Setup\AttributeSetInterface;
use Linets\VehiculosSetUp\Setup\Patch\Data\AddShippingRegionIdProductAttribute;
use Linets\VehiculosSetUp\Setup\Patch\Data\AddShippingRegionNameProductAttribute;

class AddShippingAttributeToSet implements DataPatchInterface
{
    /**
    * @var \Magento\Framework\Setup\ModuleDataSetupInterface
    */
    private $moduleDataSetup;

    /**
     * @var AttributeSetInterface
     */
    private $attributeSetSetup;

    /**
    * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    */
    public function __construct(
       \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        AttributeSetInterface $attributeSetSetup
    ) {
       $this->moduleDataSetup = $moduleDataSetup;
       $this->attributeSetSetup = $attributeSetSetup;
    }

    /**
    * {@inheritdoc}
    */
    public function apply()
    {
       $this->moduleDataSetup->getConnection()->startSetup();
       $shippingAttributes = [
           AddShippingRegionIdProductAttribute::ATTR_CODE,
           AddShippingRegionNameProductAttribute::ATTR_CODE
       ];
       foreach (AttributeSetInterface::ATTRIBUTE_SETS as $attributeSetName) {
           $attributeSet = $this->attributeSetSetup->getAttributeSetByName($attributeSetName);
           foreach ($shippingAttributes as $attributeCode) {
               $sortOrder= 600;
               $this->attributeSetSetup->addAttributeIfNotExistsToAttibuteSet($attributeSet, $attributeCode, $sortOrder);
               $sortOrder += 100;
           }
       }
       $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            \Linets\AseoRenewalSetup\Setup\Patch\Data\AttributeSet::class,
            AddShippingRegionIdProductAttribute::class,
            AddShippingRegionNameProductAttribute::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
