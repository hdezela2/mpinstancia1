<?php

namespace Linets\AseoRenewalSetup\Setup\Patch\Data;

use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use MageMoto\ElasticsearchStoreSwitch\Block\Store as StoreBlock;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class AddStoreToSearchDropdown implements DataPatchInterface
{
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    private $storeFactory;
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * StoreConfig constructor.
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        StoreFactory $storeFactory,
        WriterInterface $configWriter,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class
        ];
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(AseoRenewalConstants::STORE_VIEW_CODE);
        if ($store->getId()) {
            /** Add store to search box on front */
            $data = $this->scopeConfig->getValue(StoreBlock::XML_PATH_EMAIL_RECIPIENT);
            $arrayValue = explode(',', $data);
            $arrayValue[] = $store->getId();
            $strValue = implode(',', $arrayValue);
            $this->configWriter->save(StoreBlock::XML_PATH_EMAIL_RECIPIENT, $strValue);
        }
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }
}
