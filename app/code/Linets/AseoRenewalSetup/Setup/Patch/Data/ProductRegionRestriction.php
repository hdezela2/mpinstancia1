<?php

namespace Linets\AseoRenewalSetup\Setup\Patch\Data;

use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class ProductRegionRestriction implements DataPatchInterface
{
    /**
     * @var StoreFactory
     */
    private StoreFactory $storeFactory;
    /**
     * @var WriterInterface
     */
    private WriterInterface $configWriter;

    /**
     * StoreConfig constructor.
     *
     * @param StoreFactory    $storeFactory
     * @param WriterInterface $configWriter
     */
    public function __construct(
        StoreFactory    $storeFactory,
        WriterInterface $configWriter
    )
    {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
    }

    /**
     * @return array|string[]
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return void
     * @throws LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(AseoRenewalConstants::STORE_VIEW_CODE);
        if ($store->getId()) {
            $websiteId = $store->getWebsiteId();
            $scope = ScopeInterface::SCOPE_WEBSITES;
            $this->configWriter->save(
                AseoRenewalConstants::XML_PATH_PRODUCT_REGION_RESTRICTION_ENABLED,
                AseoRenewalConstants::CHECKOUT_PRODUCT_REGION_RESTRICTION_STATUS_DEFAULT,
                $scope,
                $websiteId
            );
        }
    }

    /**
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class
        ];
    }
}
