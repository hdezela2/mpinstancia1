<?php

namespace Linets\AseoRenewalSetup\Setup\Patch\Data;

use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class DisableReviews implements DataPatchInterface
{
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    private $storeFactory;
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;

    /**
     * StoreConfig constructor.
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    public function __construct(
        StoreFactory $storeFactory,
        WriterInterface $configWriter
    ) {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
    }

    /**
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class
        ];
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(AseoRenewalConstants::STORE_VIEW_CODE);
        if ($store->getId()) {
            /** Disable reviews for product on front */
            $websiteId = $store->getWebsiteId();
            $scope = ScopeInterface::SCOPE_WEBSITES;
            $this->configWriter->save(AseoRenewalConstants::XML_PATH_REVIEW_ACTIVE, 0, $scope, $websiteId);
            $this->configWriter->save(AseoRenewalConstants::XML_PATH_REVIEW_ALLOW_GUEST, 0, $scope, $websiteId);
        }
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }
}
