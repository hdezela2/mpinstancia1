<?php

namespace Linets\AseoRenewalSetup\Setup\Patch\Data;

use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Formax\GreatBuy\Helper\Data as HelperData;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;


class UTMGreatBuyRestriction implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var ConfigInterface
     */
    private $configInterface;
    /**
     * @var WebsiteRepositoryInterface
     */
    private $websiteRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        ModuleDataSetupInterface   $moduleDataSetup,
        ConfigInterface            $configInterface,
        WebsiteRepositoryInterface $websiteRepository,
        LoggerInterface            $logger
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->configInterface = $configInterface;
        $this->websiteRepository = $websiteRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return UTMGreatBuyRestriction|void
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        try {
            /**
             * Great Buy
             */
            $website = $this->websiteRepository->get(AseoRenewalConstants::WEBSITE_CODE);
            $this->configInterface->saveConfig(
                HelperData::MINIMUM_UTM,
                '30',
                ScopeInterface::SCOPE_WEBSITES,
                $website->getId()
            );

            $this->configInterface->saveConfig(
                HelperData::MAXIMUM_UTM,
                '5000',
                ScopeInterface::SCOPE_WEBSITES,
                $website->getId()
            );

            /**
             * Great Buy Multi Shipping
             */
            $this->configInterface->saveConfig(
                HelperData::MINIMUM_UTM_SHIPPING,
                '30',
                ScopeInterface::SCOPE_WEBSITES,
                $website->getId()
            );
            $this->configInterface->saveConfig(
                HelperData::MAXIMUM_UTM_SHIPPING,
                '5000',
                ScopeInterface::SCOPE_WEBSITES,
                $website->getId()
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
        $this->moduleDataSetup->endSetup();
    }
}
