<?php

namespace Linets\AseoRenewalSetup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Linets\AseoRenewalSetup\Api\Setup\AttributeSetInterface;

class AttributeSet implements DataPatchInterface
{

    /**
    * @var \Magento\Framework\Setup\ModuleDataSetupInterface
    */
    private $moduleDataSetup;

    /**
     * @var AttributeSetInterface
     */
    private $attributeSetSetup;

    /**
    * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    */
    public function __construct(
       \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        AttributeSetInterface $attributeSetSetup
    ) {
       $this->moduleDataSetup = $moduleDataSetup;
       $this->attributeSetSetup = $attributeSetSetup;
    }

    /**
    * {@inheritdoc}
    */
    public function apply()
    {
       $this->moduleDataSetup->getConnection()->startSetup();
       $this->attributeSetSetup->createAttributeSets($this->moduleDataSetup);
       $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            WebsiteSetup::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
