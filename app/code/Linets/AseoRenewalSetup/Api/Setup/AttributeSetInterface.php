<?php

namespace Linets\AseoRenewalSetup\Api\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Api\Data\AttributeSetInterface as AttributeSetModelInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\InputException;

interface AttributeSetInterface
{

    const ATTRIBUTE_SETS = [
        'BOLSA DE BASURA',
        'ESPONJA',
        'PAÑO ABSORBENTE MULTIUSO',
        'GUANTES DE LIMPIEZA',
        'ESCOBILLÓN',
        'PALA DE ASEO',
        'TRAPERO REUTILIZABLE',
        'INSECTICIDA',
        'CLORO TRADICIONAL',
        'DESENGRASANTE COCINA',
        'LAVALOZAS CONCENTRADO LAVADO MANUAL',
        'CERA PARA PISOS',
        'LIMPIADOR DE PISOS',
        'DESINFECTANTE SUPERFICIES EN AEROSOL',
        'DESODORANTE AMBIENTAL',
        'LÍQUIDO LIMPIAVIDRIOS',
        'CLORO PARA PISCINA',
        'REGULADOR DE PH',
        'GEL SANITIZANTE PARA MANOS',
        'JABÓN',
        'PROTECTOR SOLAR',
        'PAÑUELOS DESECHABLES',
        'PAPEL HIGIÉNICO',
        'SERVILLETAS DESECHABLES',
        'TOALLA DE PAPEL',
        'PAÑAL DE ADULTO',
        'CLORO CONCENTRADO',
        'CLORO GEL'
    ];

    const PRODUCTO_AUTORIZADO_ATTRIBUTE_CODE = 'producto_autorizado';
    const PRODUCTO_AUTORIZADO_ATTRIBUTE_LABEL = 'Producto Autorizado';

    const REQUIRED_ATTRIBUTES = [
        'marca',
        'region'
    ];

    /**
     * @param ModuleDataSetupInterface $setup
     * @return void
     */
    public function createAttributeSets(ModuleDataSetupInterface $setup);

    /**
     * @param int $parentAttributeSetId
     * @param string $entityTypeId
     * @param string $attributeSetName
     * @return void
     * @throws \Exception
     */
    public function createAttributeSet($parentAttributeSetId, $entityTypeId, $attributeSetName);

    /**
     * @param AttributeSetModelInterface $attributeSet
     * @param string $attributeCode
     * @param int $sortOrder
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function addAttributeToAttibuteSet(
        AttributeSetModelInterface $attributeSet,
        $attributeCode,
        $sortOrder
    );

    /**
     * @param string $attributeSetName
     * @return AttributeSetModelInterface
     * @throws NoSuchEntityException
     */
    public function getAttributeSetByName($attributeSetName);

    /**
     * @param ModuleDataSetupInterface $setup
     * @param string $attributeName
     * @param array $attributeData
     */
    public function createAttribute(ModuleDataSetupInterface $setup, $attributeName, $attributeData);

    /**
     * @param AttributeSetModelInterface $attributeSet
     * @param string $attributeCode
     * @param int $sortOrder
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function addAttributeIfNotExistsToAttibuteSet(
        AttributeSetModelInterface $attributeSet,
        $attributeCode,
        $sortOrder
    );
}
