<?php

namespace Linets\AseoRenewalSetup\Model\Plugin\Compare;

use Formax\AgreementInfo\Helper\Store;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Catalog\Helper\Product\Compare;
use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;

class LimitToCompareProducts
{
    /**
     * because start count on 0
     */
    const LIMIT_TO_COMPARE_PRODUCTS = 3;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /** @var Compare */
    protected $helper;
    private Store $store;

    /**
     * RestrictCustomerEmail constructor.
     * @param Compare $helper
     * @param RedirectFactory $redirectFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        RedirectFactory $redirectFactory,
        Compare $helper,
        Store $store,
        ManagerInterface $messageManager
    )
    {
        $this->helper = $helper;
        $this->resultRedirectFactory = $redirectFactory;
        $this->messageManager = $messageManager;
        $this->store = $store;
    }

    public function aroundExecute(
        \Magento\Catalog\Controller\Product\Compare\Add $subject,
        \Closure $proceed
    ){

        $websiteCode = $this->store->getWebsiteCode();
        if ($websiteCode == AseoRenewalConstants::WEBSITE_CODE) {
            $count = $this->helper->getItemCount();
            if ($count > self::LIMIT_TO_COMPARE_PRODUCTS) {
                $this->messageManager->addErrorMessage(
                    __('You cannot add more than %1 products to compare',(self::LIMIT_TO_COMPARE_PRODUCTS + 1))
                );

                /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setRefererOrBaseUrl();
            }
        }
        return $proceed();
    }
}
