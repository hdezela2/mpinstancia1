<?php

namespace Linets\AseoRenewalSetup\Model;

class AseoRenewalConstants
{
    const ID_AGREEMENT = 5800319;
    const WEBSITE_NAME = 'Artículos de Aseo e Higiene';
    const WEBSITE_CODE = 'aseo2022';
    const STORE_NAME = 'Artículos de Aseo e Higiene';
    const STORE_CODE = 'aseo2022';
    const STORE_VIEW_NAME = 'Artículos de Aseo e Higiene';
    const STORE_VIEW_CODE = 'aseo2022';
    const ROOT_CATEGORY_NAME = 'DCCP ASEO2022';
    const ATTRIBUTE_GROUP = 'Artículos de Aseo e Higiene';
    const FRONT_THEME = 'Linets/Aseo';
    const FRONT_THEME_DEFAULT = 'Formax/ChileCompra';
    /**
     * Reviews config
     */
    const XML_PATH_REVIEW_ALLOW_GUEST = 'catalog/review/allow_guest';
    const XML_PATH_REVIEW_ACTIVE = 'catalog/review/active';
    /**
     * Max and min days for special offers
     */
    const XML_PATH_MIN_OFFER_DAYS = 'catalog/dccp_offer/ini_range_offer';
    const XML_PATH_MAX_OFFER_DAYS = 'catalog/dccp_offer/end_range_offer';
    const MIN_OFFER_DAYS = 1;
    const MAX_OFFER_DAYS = 4;
    /**
     * Request for quote config
     */
    const XML_PATH_REQUEST_FOR_QUOTE = 'requestforquote/requestforquote_settings/requestforquote_account';
    const REQUEST_FOR_QUOTE_STATUS = 0;
    /**
     * Max Multishipping products qty config
     */
    const XML_PATH_CHECKOUT_MULTIPLE_AVAILABLE = 'multishipping/options/checkout_multiple';
    const CHECKOUT_MULTIPLE_AVAILABLE_STATUS = 1;
    const XML_PATH_CHECKOUT_MULTIPLE_MAXIMUM_QUANTITY = 'multishipping/options/checkout_multiple_maximum_qty';
    const CHECKOUT_MULTIPLE_MAXIMUM_QUANTITY = 100;
    /**
     * Tax config
     */
    const XML_PATH_SHIPPING_TAX_RULE = 'wktax/general_settings/shipping_rulestaxes_code';
    /**
     * Categoría temporal
     */
    const XML_PATH_NUMBER_OF_DAYS = 'stock_management_configuration/general/number_of_days';
    const XML_PATH_STOCK_MANAGEMENT_ENABLED = 'stock_management_configuration/general/enable';
    const XML_PATH_REQUEST_FOR_QUOTE_ACCOUNT = 'requestforquote/requestforquote_settings/requestforquote_account';
    /**
     * Product Region Restriction
     */
    const XML_PATH_PRODUCT_REGION_RESTRICTION_ENABLED = 'summa_product_region_restriction/general/enabled';
    const CHECKOUT_PRODUCT_REGION_RESTRICTION_STATUS_DEFAULT = 1;
    /**
     * Endpoint Regional Condition Add
     */
    const XML_PATH_DCCP_REGIONAL_CONDITION_ADD = 'dccp_endpoint/endpoint_regional_condition_add/endpoint';
}
