<?php

namespace Linets\FixCategoryForm\Console\Command;

use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EnableFilters extends Command
{
    const STORE_ARGUMENT = 'store_code';
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    private $categoryCollectionFactory;
    /**
     * @var \Magento\Catalog\Model\CategoryRepository
     */
    private $categoryRepository;

    public function __construct(
        StoreManagerInterface $storeManager,
        CollectionFactory $categoryCollectionFactory,
        CategoryRepository $categoryRepository,
        string $name = null
    )
    {
        $this->storeManager = $storeManager;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->categoryRepository = $categoryRepository;
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('category:filters:enable');
        $this->setDescription('Use this command to enable the filters for specific Store category tree');
        $this->setDefinition([
            new InputArgument(self::STORE_ARGUMENT, InputArgument::REQUIRED, "Store Code"),
        ]);
        parent::configure();
    }

    /**
     * CLI command description
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $storeCode = $input->getArgument(self::STORE_ARGUMENT);
        $store = $this->storeManager->getStore($storeCode);
        $rootCategoryId = $store->getRootCategoryId();
        $categories = $this->categoryCollectionFactory->create()
            ->setStore($store)
            ->addFieldToFilter('path', ['like' => '1/' . $rootCategoryId . '/%']);
        /** @var \Magento\Catalog\Model\Category $category */
        foreach ($categories as $category) {
            $categoryModel = $this->categoryRepository->get($category->getId());
            if (!$categoryModel->getIsAnchor()) {
                $output->writeln("Enabling filters for category " . $categoryModel->getName());
                $categoryModel->setIsAnchor(1);
                $this->categoryRepository->save($categoryModel);
            }
        }
        $output->writeln("Done, Now all categories for store " . $store->getName() . ' have filters enabled');
    }
}
