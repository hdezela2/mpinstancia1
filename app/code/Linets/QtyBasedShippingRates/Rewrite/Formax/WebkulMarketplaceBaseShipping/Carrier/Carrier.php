<?php

namespace Linets\QtyBasedShippingRates\Rewrite\Formax\WebkulMarketplaceBaseShipping\Carrier;

use Chilecompra\WebkulMpShipping\Rewrite\Webkul\Mpshipping\Model\Carrier as ChilecompraCarrier;
use Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory;
use Linets\QtyBasedShippingRates\Helper\Config as LinetsConfigHelper;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Escaper;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Session\SessionManager;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\Quote\Item\OptionFactory;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Webkul\Marketplace\Model\SaleslistFactory;
use Webkul\MarketplaceBaseShipping\Model\ShippingSettingRepository;
use Webkul\Mpshipping\Helper\Data as HelperData;
use Webkul\Mpshipping\Model\MpshippingDistFactory;
use Webkul\Mpshipping\Model\MpshippingFactory;
use Webkul\Mpshipping\Model\MpshippingmethodFactory;
use Webkul\Mpshipping\Model\MpshippingsetFactory;
use Webkul\Mpshipping\Model\SellerLocationFactory;

class Carrier extends ChilecompraCarrier
{

    /**
     * @var LinetsConfigHelper
     */
    private $configHelper;

    public HelperData $helperData;
    private SerializerInterface $serializerInterface;
    protected $mpshippingDistModel;
    public Curl $curl;
    public Escaper $escaper;
    public SellerLocationFactory $sellerLocation;

    /**
     * Carrier constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Framework\Session\SessionManager $coreSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\App\RequestInterface $requestInterface
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Quote\Model\Quote\Item\OptionFactory $optionFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\AddressFactory $addressFactory
     * @param \Webkul\Marketplace\Model\ProductFactory $mpProductFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory
     * @param \Webkul\MarketplaceBaseShipping\Model\ShippingSettingRepository $shippingSettingRepository
     * @param \Webkul\Mpshipping\Model\MpshippingmethodFactory $shippingmethodFactory
     * @param \Webkul\Mpshipping\Model\MpshippingFactory $mpshippingModel
     * @param \Webkul\Mpshipping\Model\MpshippingsetFactory $mpshippingsetModel
     * @param \Formax\RegionalCondition\Helper\Data $helper
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Formax\RegionalCondition\Model\ResourceModel\RegionalCondition\CollectionFactory $regionalConditionCollection
     * @param \Chilecompra\WebkulMpShipping\Helper\Data $shippingHelper
     * @param \Linets\QtyBasedShippingRates\Helper\Config $configHelper
     * @param HelperData $helperData
     * @param SerializerInterface $serializerInterface
     * @param MpshippingDistFactory $mpshippingDistModel
     * @param Curl $curl
     * @param Escaper $escaper
     * @param SellerLocationFactory $sellerLocation
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        RegionFactory $regionFactory,
        SessionManager $coreSession,
        Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        CurrencyFactory $currencyFactory,
        StoreManagerInterface $storeManager,
        FormatInterface $localeFormat,
        Data $jsonHelper,
        RequestInterface $requestInterface,
        PriceCurrencyInterface $priceCurrency,
        OptionFactory $optionFactory,
        CustomerFactory $customerFactory,
        AddressFactory $addressFactory,
        \Webkul\Marketplace\Model\ProductFactory $mpProductFactory,
        ProductFactory $productFactory,
        SaleslistFactory $saleslistFactory,
        ShippingSettingRepository $shippingSettingRepository,
        MpshippingmethodFactory $shippingmethodFactory,
        MpshippingFactory $mpshippingModel,
        MpshippingsetFactory $mpshippingsetModel,
        \Formax\RegionalCondition\Helper\Data $helper,
        ResourceConnection $resource,
        CollectionFactory $regionalConditionCollection,
        \Chilecompra\WebkulMpShipping\Helper\Data $shippingHelper,
        LinetsConfigHelper $configHelper,
        HelperData $helperData,
        SerializerInterface $serializerInterface,
        MpshippingDistFactory $mpshippingDistModel,
        Curl $curl,
        Escaper $escaper,
        SellerLocationFactory $sellerLocation,
        array $data = []
    )
    {
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger, $rateResultFactory,
            $rateMethodFactory, $regionFactory,
            $coreSession,
            $checkoutSession,
            $customerSession,
            $currencyFactory,
            $storeManager,
            $localeFormat,
            $jsonHelper,
            $requestInterface,
            $priceCurrency,
            $optionFactory,
            $customerFactory,
            $addressFactory,
            $mpProductFactory,
            $productFactory,
            $saleslistFactory,
            $shippingSettingRepository,
            $shippingmethodFactory,
            $mpshippingModel,
            $mpshippingsetModel,
            $helper,
            $resource,
            $regionalConditionCollection,
            $shippingHelper,
            $helperData,
            $serializerInterface,
            $mpshippingDistModel,
            $curl,
            $escaper,
            $sellerLocation,
            $data
        );
        $this->configHelper = $configHelper;
    }

    /**
     * [shippingForNumericZipcode Calculate shipping for the numeric zipcode]
     * @param array $shipdetail
     * @param RateRequest $requestData
     * @return \Webkul\Mpshipping\Model\Mpshipping $shipping
     */
    public function shippingForNumericZipcode($shipdetail, $requestData)
    {
        if (!$this->configHelper->isEnabled()) {
            return parent::shippingForNumericZipcode($shipdetail, $requestData);
        }
        $shipping = $this->getShippingcollectionAccordingToDetails(
            $requestData->getDestCountryId(),
            $shipdetail['seller_id'],
            $requestData->getDestRegionId(),
            (int)($requestData->getDestPostal()),
            $shipdetail['qty']
        );
        if ($shipping->getSize() == 0) {
            $shipping = $this->getShippingcollectionAccordingastrik(
                $requestData->getDestCountryId(),
                $shipdetail['seller_id'],
                $requestData->getDestRegionId(),
                '*',
                $shipdetail['qty']
            );
        }
        if ($shipping->getSize() == 0) {
            if ($this->getConfigData('allowadmin')) {
                $price = 0;
                $shipping = $this->getAdminSuperShippingPrice($shipdetail);
                if ($shipping->getSize() == 0) {
                    $shipping = $this->getShippingcollectionAccordingToDetails(
                        $requestData->getDestCountryId(),
                        0,
                        $requestData->getDestRegionId(),
                        (int)($requestData->getDestPostal()),
                        $shipdetail['qty']
                    );
                    if ($shipping->getSize() == 0) {
                        $shipping = $this->getShippingcollectionAccordingastrik(
                            $requestData->getDestCountryId(),
                            0,
                            $requestData->getDestRegionId(),
                            '*',
                            $shipdetail['qty']
                        );
                    }
                }
            }
        }
        return $shipping;
    }

    /**
     * [getShippingcollectionAccordingastrik Calculate shipping for Astrik Postal code]
     * @param string $countryId
     * @param int $sellerId
     * @param string $regionId
     * @param string $postalCode
     * @param float $weight
     * @return \Webkul\Mpshipping\Model\Mpshipping $shipping
     */
    public function getShippingcollectionAccordingastrik($countryId, $sellerId, $regionId, $postalCode, $weight)
    {
        if (!$this->configHelper->isEnabled()) {
            return parent::getShippingcollectionAccordingastrik($countryId, $sellerId, $regionId, $postalCode, $weight);
        }
        $shipping = $this->_mpShippingModel->create()
            ->getCollection()
            ->addExpressionFieldToSelect('price', new \Zend_Db_Expr('price*' . $weight), [])
            ->addFieldToFilter('dest_country_id', ['eq' => $countryId])
            ->addFieldToFilter('partner_id', ['eq' => $sellerId])
            ->addFieldToFilter(
                ['dest_region_id', 'dest_region_id'],
                [
                    ['eq' => $regionId],
                    ['eq' => '*']
                ]
            )
            ->addFieldToFilter('dest_zip', ['eq' => $postalCode])
            ->addFieldToFilter('dest_zip_to', ['eq' => $postalCode])
            ->addFieldToFilter('weight_from', ['lteq' => $weight])
            ->addFieldToFilter('weight_to', ['gteq' => $weight]);
        return $shipping;
    }

    /**
     * [getShippingcollectionAccordingToDetails Calculate shipping according to Postal Code]
     * @param string $countryId
     * @param int $sellerId
     * @param string $regionId
     * @param int $postalCode
     * @param float $weight
     * @return \Webkul\Mpshipping\Model\Mpshipping $shipping
     */
    public function getShippingcollectionAccordingToDetails($countryId, $sellerId, $regionId, $postalCode, $weight)
    {
        if (!$this->configHelper->isEnabled()) {
            return parent::getShippingcollectionAccordingastrik($countryId, $sellerId, $regionId, $postalCode, $weight);
        }
        $shipping = $this->_mpShippingModel->create()
            ->getCollection()
            ->addExpressionFieldToSelect('price', new \Zend_Db_Expr('price*' . $weight), [])
            ->addFieldToFilter('dest_country_id', ['eq' => $countryId])
            ->addFieldToFilter('partner_id', ['eq' => $sellerId])
            ->addFieldToFilter('website_id', ['eq' => $this->websiteId])
            ->addFieldToFilter(
                ['dest_region_id', 'dest_region_id'],
                [
                    ['eq' => $regionId],
                    ['eq' => '*']
                ]
            )
            ->addFieldToFilter(
                ['dest_zip', 'dest_zip'],
                [
                    ['lteq' => $postalCode],
                    ['eq' => '*']
                ]
            )
            ->addFieldToFilter(
                ['dest_zip_to', 'dest_zip_to'],
                [
                    ['gteq' => $postalCode],
                    ['eq' => '*']
                ]
            )
            ->addFieldToFilter('weight_from', ['lteq' => $weight])
            ->addFieldToFilter('weight_to', ['gteq' => $weight]);
        return $shipping;
    }

    /**
     * This function was present in the Webkul_Mpshipping module
     * prior to the Magento upgrade to 2.4.3. The new module
     * introduced a new function completely different that
     * break changes so we are adding the previous version back.
     * See https://chilecompra.atlassian.net/browse/MU24-66
     *
     * [getShippingPriceRates Calculate Shipping for admin and seller]
     * @param  array $shipdetail
     * @param  RateRequest $requestData
     * @return object $shipping
     */
    public function getShippingPriceRates($shipdetail, $requestData)
    {
        $shipping = $this->getSuperShippingPrice($shipdetail);
        if ($shipping->getSize() == 0) {
            if (array_key_exists('items_weight', $shipdetail)) {
                if (!is_numeric($requestData->getDestPostal())) {
                    $shipping = $this->shippingForAlphanumericZipcode($shipdetail, $requestData);
                } else {
                    $shipping = $this->shippingForNumericZipcode($shipdetail, $requestData);
                }
            }
        }
        return $shipping;
    }
}
