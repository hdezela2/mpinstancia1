<?php

namespace Linets\SellerCentralizedDispatchCost\Observer;

use Magento\Catalog\Model\ResourceModel\Product as ProductResourceModel;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class DispatchPriceBunchImportAfter implements ObserverInterface
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollectionFactory;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    private $productResourceModel;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param ProductCollectionFactory $productCollectionFactory
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        LoggerInterface          $logger,
        ProductCollectionFactory $productCollectionFactory,
        ResourceConnection       $resourceConnection,
        ProductResourceModel     $productResourceModel
    )
    {
        $this->logger = $logger;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->resourceConnection = $resourceConnection;
        $this->productResourceModel = $productResourceModel;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        try {
            $idsArray = [];
            $bunch = (array)$observer->getData('bunch');
            $index = $observer->getData('currentBunch');
            $connection = $this->resourceConnection->getConnection();
            foreach ($bunch[$index] as $row) {
                $idsArray = $this->getIdsToUpdate($row);
                $ids = implode(',', $idsArray);
                $sql = 'UPDATE marketplace_assignproduct_associated_products '
                    . 'SET price = base_price+' . $row['price']
                    . ' WHERE id IN(' . $ids . ');';
                $connection->query($sql);
            }
            $this->logger->info('UPDATED', ['count' => count($idsArray)]);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), ['ids' => $ids ?? 'EMPTY']);
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getIdsToUpdate(array $data)
    {
        $macrozoneAttribute = $this->productResourceModel->getAttribute('macrozona');
        $macrozoneValueId = $macrozoneAttribute->getSource()
            ->getOptionId($data['macrozone']);
        $connection = $this->resourceConnection->getConnection();
        $query = $connection->select()
            ->from(['e' => 'catalog_product_entity'], ['sku'])
            ->join(['at_macrozona' => 'catalog_product_entity_int'],
                'at_macrozona.row_id = e.row_id AND at_macrozona.store_id = 0 '
                . 'AND at_macrozona.attribute_id = ' . $macrozoneAttribute->getAttributeId()
                . ' AND at_macrozona.value = ' . $macrozoneValueId
                . ' AND e.attribute_set_id = ' . $data['attribute_set_id']
                . ' AND e.type_id = ' . "'simple'",
                ['macrozona' => 'at_macrozona.value'])
            ->join(['maap' => 'marketplace_assignproduct_associated_products'],
                'e.entity_id = maap.product_id',
                ['maap_id' => 'maap.id', 'maap_base_price' => 'maap.base_price', 'maap_price' => 'maap.price'])
            ->join(['mai' => 'marketplace_assignproduct_items'],
                'maap.parent_id = mai.id AND seller_id = ' . $data['seller_id'],
                ['mai_id' => 'mai.id']);

        $result = $connection->fetchAssoc($query);
        $ids = [];
        foreach ($result as $row) {
            $ids[] = $row['maap_id'];
        }
        return $ids;
    }
}