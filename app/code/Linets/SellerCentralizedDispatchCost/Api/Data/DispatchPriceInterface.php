<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Api\Data;

interface DispatchPriceInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const WEBSITE_ID = 'website_id';
    const CREATED_AT = 'created_at';
    const PRICE = 'price';
    const DISPATCHPRICE_ID = 'dispatchprice_id';
    const MACROZONE = 'macrozone';
    const ATTRIBUTE_SET_ID = 'attribute_set_id';
    const SELLER_ID = 'seller_id';
    const UPDATED_AT = 'updated_at';
    const BASE_PRICE = 'base_price';
    const DISPATCHPRICE = 'dispatch_price';

    /**
     * Get dispatchprice_id
     * @return string|null
     */
    public function getDispatchpriceId();

    /**
     * Set dispatchprice_id
     * @param string $dispatchpriceId
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setDispatchpriceId($dispatchpriceId);

    /**
     * Get seller_id
     * @return string|null
     */
    public function getSellerId();

    /**
     * Set seller_id
     * @param string $sellerId
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setSellerId($sellerId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceExtensionInterface $extensionAttributes
    );

    /**
     * Get attribute_set_id
     * @return string|null
     */
    public function getAttributeSetId();

    /**
     * Set attribute_set_id
     * @param string $attributeSetId
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setAttributeSetId($attributeSetId);

    /**
     * Get macrozone
     * @return string|null
     */
    public function getMacrozone();

    /**
     * Set macrozone
     * @param string $macrozone
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setMacrozone($macrozone);

    /**
     * Get price
     * @return string|null
     */
    public function getPrice();

    /**
     * Set price
     * @param string $price
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setPrice($price);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get website_id
     * @return string|null
     */
    public function getWebsiteId();

    /**
     * Set website_id
     * @param string $websiteId
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setWebsiteId($websiteId);
}

