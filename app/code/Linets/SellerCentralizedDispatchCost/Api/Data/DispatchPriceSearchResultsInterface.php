<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Api\Data;

interface DispatchPriceSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get DispatchPrice list.
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface[]
     */
    public function getItems();

    /**
     * Set seller_id list.
     * @param \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

