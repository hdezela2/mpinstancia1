<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface DispatchPriceRepositoryInterface
{

    /**
     * Save DispatchPrice
     * @param \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface $dispatchPrice
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface $dispatchPrice
    );

    /**
     * Retrieve DispatchPrice
     * @param string $dispatchpriceId
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($dispatchpriceId);

    /**
     * Retrieve DispatchPrice matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete DispatchPrice
     * @param \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface $dispatchPrice
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface $dispatchPrice
    );

    /**
     * Delete DispatchPrice by ID
     * @param string $dispatchpriceId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($dispatchpriceId);
}

