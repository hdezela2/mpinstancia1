<?php
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Api;

use Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface;

interface DispatchPriceManagementInterface
{

    /**
     * @param int $configurableId
     * @param int $sellerId
     * @param int $websiteId
     * @return array
     */
    public function getChildDispatchPrices($configurableId, $sellerId, $websiteId);

    /**
     * @param int $sellerId
     * @param string $macrozone
     * @param int $attributeSetId
     * @param int $websiteId
     * @return DispatchPriceInterface|null
     */
    public function getDispatchPrice($sellerId, $macrozone, $attributeSetId, $websiteId);
}
