<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Plugin\Frontend\Formax\Offer\Helper;

class Data
{

    public function afterGetProductWithSpecialPriceBySeller(
        \Formax\Offer\Helper\Data $subject,
        $result
    ) {
        return $result;
    }
}

