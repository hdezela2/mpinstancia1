<?php

namespace Linets\SellerCentralizedDispatchCost\ViewModel;

use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Block\Cart\Item\Renderer as ItemRenderer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Mpsplitcart\Helper\Data;

class DispatchPrice implements ArgumentInterface
{

    /**
     * @var \Webkul\Mpsplitcart\Helper\Data
     */
    private $helper;
    /**
     * @var \Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface
     */
    private $dispatchPriceManagement;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Webkul\Mpsplitcart\Helper\Data $helper
     * @param \Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface $dispatchPriceManagement
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        Data                             $helper,
        DispatchPriceManagementInterface $dispatchPriceManagement,
        StoreManagerInterface            $storeManager
    )
    {
        $this->helper = $helper;
        $this->dispatchPriceManagement = $dispatchPriceManagement;
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param \Magento\Checkout\Block\Cart\Item\Renderer $block
     * @return float
     */
    public function getDispatchPriceHtml(AbstractItem $item, ItemRenderer $block)
    {
        $formattedPrice = "";
        try {
            $websiteId = $this->storeManager->getWebsite()->getId();
            $options = $item->getBuyRequest()->getData();

            if (array_key_exists("mpassignproduct_id", $options)) {
                $sellerId = $this->helper->getSellerIdFromMpassign($options["mpassignproduct_id"]);
            } else {
                $sellerId = $this->helper->getSellerId($item->getProductId());
            }

            $attributeSetId = $item->getProduct()->getAttributeSetId();
            $macrozone = $this->getMacrozone($item->getProduct(), $options['super_attribute']);
            $price = $this->dispatchPriceManagement->getDispatchPrice($sellerId, $macrozone, $attributeSetId, $websiteId);

            if ($price) {
                $formattedPrice = $block->convertPrice($price->getPrice(), true);
            }
        } catch (LocalizedException $e) {
            //DO NOTHING
        }

        return $formattedPrice;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array $options
     * @return string|null
     */
    private function getMacrozone(Product $product, array $options): ?string
    {
        $valueId = 0;
        $optionText = '';
        foreach ($options as $k => $v) {
            $valueId = $v;
        }
        $attribute = $product->getResource()->getAttribute('macrozona');
        if ($attribute->usesSource()) {
            $optionText = $attribute->getSource()->getOptionText($valueId);
        }
        return $optionText;
    }
}