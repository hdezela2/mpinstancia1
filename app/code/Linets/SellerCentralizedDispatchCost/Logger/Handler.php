<?php

namespace Linets\SellerCentralizedDispatchCost\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /** @var int  */
    protected $loggerType = Logger::INFO;

    /** @var string  */
    protected $fileName = '/var/log/dispatch_price.log';
}