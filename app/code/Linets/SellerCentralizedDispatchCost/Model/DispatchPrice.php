<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Model;

use Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface;
use Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterfaceFactory;
use Linets\SellerCentralizedDispatchCost\Model\ResourceModel\DispatchPrice\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class DispatchPrice extends \Magento\Framework\Model\AbstractModel
{

    protected $dispatchpriceDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'dccp_centralized_dispatch_price';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param DispatchPriceInterfaceFactory $dispatchpriceDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Linets\SellerCentralizedDispatchCost\Model\ResourceModel\DispatchPrice $resource
     * @param \Linets\SellerCentralizedDispatchCost\Model\ResourceModel\DispatchPrice\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context                       $context,
        Registry                      $registry,
        DispatchPriceInterfaceFactory $dispatchpriceDataFactory,
        DataObjectHelper              $dataObjectHelper,
        ResourceModel\DispatchPrice   $resource,
        Collection                    $resourceCollection,
        array                         $data = []
    )
    {
        $this->dispatchpriceDataFactory = $dispatchpriceDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve dispatchprice model with dispatchprice data
     * @return DispatchPriceInterface
     */
    public function getDataModel()
    {
        $dispatchpriceData = $this->getData();

        $dispatchpriceDataObject = $this->dispatchpriceDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $dispatchpriceDataObject,
            $dispatchpriceData,
            DispatchPriceInterface::class
        );

        return $dispatchpriceDataObject;
    }
}

