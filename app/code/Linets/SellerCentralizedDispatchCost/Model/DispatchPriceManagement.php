<?php
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Model;

use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface;
use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceRepositoryInterface;
use Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class DispatchPriceManagement implements DispatchPriceManagementInterface
{

    /**
     * @var DispatchPriceRepositoryInterface
     */
    private $dispatchPriceRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var array
     */
    private $dispatchPrices;

    /**
     * @param DispatchPriceRepositoryInterface $dispatchPriceRepository
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        DispatchPriceRepositoryInterface $dispatchPriceRepository,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->dispatchPriceRepository = $dispatchPriceRepository;
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->dispatchPrices = [];
    }

    /**
    * {@inheritdoc}
    */
    public function getChildDispatchPrices($configurableId, $sellerId, $websiteId)
    {
        $product = $this->getProduct($configurableId);
        $prices = [];
        if ($product) {
            $productTypeInstance = $product->getTypeInstance();
            $usedProducts = $productTypeInstance->getUsedProducts($product);
            foreach ($usedProducts as $childProduct) {
                $attributeSetId = $childProduct->getAttributeSetId();
                $macrozone = $this->getMacrozona($childProduct);
                $dispatchPrice = $this->getDispatchPrice($sellerId, $macrozone, $attributeSetId, $websiteId);
                $price = $dispatchPrice ? $dispatchPrice->getPrice() : null;
                $prices[$childProduct->getId()] = $price;
            }
        }

        return $prices;
    }

    /**
     * @param int $productId
     * @return ProductInterface|null
     */
    private function getProduct($productId)
    {
        try {
            $product = $this->productRepository->getById($productId);
        } catch (\Exception $ex) {
            $product = null;
        }

        return $product;
    }

    /**
     * @param ProductInterface $childProduct
     * @return string
     */
    private function getMacrozona($childProduct)
    {
        $attribute = $childProduct->getResource()->getAttribute('macrozona');
        $optionId = $childProduct->getMacrozona();
        $optionText = '';
        if ($attribute->usesSource()) {
            $optionText = $attribute->getSource()->getOptionText($optionId);
        }

        return $optionText;
    }

    /**
    * {@inheritdoc}
    */
    public function getDispatchPrice($sellerId, $macrozone, $attributeSetId, $websiteId)
    {
        if (!isset($this->dispatchPrices[$sellerId][$macrozone][$attributeSetId][$websiteId])) {
            $this->dispatchPrices[$sellerId][$macrozone][$attributeSetId][$websiteId] =
                $this->findDispatchPrice($sellerId, $macrozone, $attributeSetId, $websiteId);
        }

        return $this->dispatchPrices[$sellerId][$macrozone][$attributeSetId][$websiteId];
    }

    /**
     * @param int $sellerId
     * @param string $macrozone
     * @param int $attributeSetId
     * @param int $websiteId
     * @return DispatchPriceInterface|null
     */
    private function findDispatchPrice($sellerId, $macrozone, $attributeSetId, $websiteId)
    {
        $dispatchPrice = null;
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(DispatchPriceInterface::SELLER_ID, $sellerId, 'eq')
            ->addFilter(DispatchPriceInterface::MACROZONE, $macrozone, 'eq')
            ->addFilter(DispatchPriceInterface::ATTRIBUTE_SET_ID, $attributeSetId, 'eq')
            ->addFilter(DispatchPriceInterface::WEBSITE_ID, $websiteId, 'eq')
            ->create();
        $searchResults = $this->dispatchPriceRepository->getList($searchCriteria)->getItems();
        if (count($searchResults)) {
            $dispatchPrice = array_shift($searchResults);
        }
        

        return $dispatchPrice;
    }
}
