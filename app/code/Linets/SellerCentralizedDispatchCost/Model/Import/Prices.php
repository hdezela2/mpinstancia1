<?php

namespace Linets\SellerCentralizedDispatchCost\Model\Import;

use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Customer\Model\ResourceModel\Customer;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set as AttributeSetResourceModel;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\ImportExport\Helper\Data as ImportHelper;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\Entity\AbstractEntity;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\ImportExport\Model\ResourceModel\Import\Data;
use Magento\Store\Model\WebsiteFactory;

class Prices extends AbstractEntity
{
    const ENTITY_CODE = 'dispatch_price';
    const TABLE = 'dccp_centralized_dispatch_price';
    const ENTITY_ID_COLUMN = 'dispatchprice_id';

    const SELLER_EMAIL = 'seller_email';
    const SELLER_ID = 'seller_id';
    const ATTRIBUTE_SET = 'attribute_set';
    const ATTRIBUTE_SET_ID = 'attribute_set_id';
    const MACROZONE = 'macrozone';
    const PRICE = 'price';
    const WEBSITE_ID = 'website_id';

    /** @var bool */
    protected $needColumnCheck = true;

    /** @var bool */
    protected $logInHistory = true;

    /** @var string[] */
    protected $_permanentAttributes = [
        self::SELLER_EMAIL,
        self::ATTRIBUTE_SET,
        self::MACROZONE
    ];

    /** @var string[] */
    protected $validColumnNames = [
        self::SELLER_EMAIL,
        self::ATTRIBUTE_SET,
        self::MACROZONE,
        self::PRICE,
        self::WEBSITE_ID,

    ];
    /** @var string[] */
    protected $tableColumnNames = [
        self::SELLER_ID,
        self::ATTRIBUTE_SET_ID,
        self::MACROZONE,
        self::PRICE,
        self::WEBSITE_ID,
    ];

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json|mixed
     */
    private $serializer;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    private $customerResourceModel;
    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set
     */
    private $attributeSetResourceModel;
    /**
     * @var \Magento\Customer\Model\Customer
     */
    private $customerModel;
    /**
     * @var \Magento\Eav\Setup\EavSetup
     */
    private $eavSetup;
    /**
     * @var \Magento\Store\Model\WebsiteFactory
     */
    private $websiteFactory;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resource;
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $eventManager;

    /**
     * Courses constructor.
     *
     * @param JsonHelper $jsonHelper
     * @param ImportHelper $importExportData
     * @param Data $importData
     * @param ResourceConnection $resource
     * @param Helper $resourceHelper
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param \Magento\Customer\Model\ResourceModel\Customer $customerResourceModel
     * @param \Magento\Customer\Model\Customer $customerModel
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set $attributeSetResourceModel
     * @param \Magento\Eav\Setup\EavSetup $eavSetup
     * @param \Magento\Store\Model\WebsiteFactory $websiteFactory
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     */
    public function __construct(
        JsonHelper                         $jsonHelper,
        ImportHelper                       $importExportData,
        Data                               $importData,
        ResourceConnection                 $resource,
        Helper                             $resourceHelper,
        ProcessingErrorAggregatorInterface $errorAggregator,
        Customer                           $customerResourceModel,
        CustomerModel                      $customerModel,
        AttributeSetResourceModel          $attributeSetResourceModel,
        EavSetup                           $eavSetup,
        WebsiteFactory                     $websiteFactory,
        ManagerInterface                   $eventManager
    )
    {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->resource = $resource;
        $this->connection = $resource->getConnection(ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
        $this->customerResourceModel = $customerResourceModel;
        $this->attributeSetResourceModel = $attributeSetResourceModel;
        $this->customerModel = $customerModel;
        $this->eavSetup = $eavSetup;
        $this->websiteFactory = $websiteFactory;
        $this->eventManager = $eventManager;
        $this->initMessageTemplates();
    }

    /**
     * @inheritDoc
     */
    public function getEntityTypeCode()
    {
        return static::ENTITY_CODE;
    }

    /**
     * Get available columns
     *
     * @return array
     */
    public function getValidColumnNames(): array
    {
        return $this->validColumnNames;
    }

    /**
     * @inheritDoc
     */
    protected function _importData()
    {
        switch ($this->getBehavior()) {
            case Import::BEHAVIOR_DELETE:
                $this->deleteEntity();
                break;
            case Import::BEHAVIOR_APPEND:
            case Import::BEHAVIOR_REPLACE:
                $this->saveAndReplaceEntity();
                break;
        }

        return true;
    }

    /**
     * Delete entities
     *
     * @return bool
     */
    private function deleteEntity(): bool
    {
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);

                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowId = $rowData[static::ENTITY_ID_COLUMN];
                    $rows[] = $rowId;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }

        if ($rows) {
            return $this->deleteEntityFinish(array_unique($rows));
        }

        return false;
    }

    /**
     * Save and replace entities
     *
     * @return void
     */
    private function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $rows = [];
        $count = 1;
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];

            foreach ($bunch as $rowNum => $row) {
                if (!$this->validateRow($row, $rowNum)) {
                    continue;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }
                $columnValues = $row;

                $entityList[$count][] = $columnValues;
                $this->countItemsCreated++;
                $this->countItemsUpdated = 0;
            }

            if (Import::BEHAVIOR_APPEND === $behavior) {
                $this->saveEntityFinish($entityList);
            }
            $this->eventManager->dispatch('linets_seller_centralized_dispatch_price_bunch_import_after',
                [
                    'bunch' => $entityList,
                    'currentBunch' => $count
                ]
            );
            $count++;
        }
    }

    /**
     * Save entities
     *
     * @param array $entityData
     *
     * @return bool
     */
    private function saveEntityFinish(array $entityData): bool
    {
        if ($entityData) {
            $tableName = $this->connection->getTableName(static::TABLE);
            $rows = [];

            foreach ($entityData as $entityRows) {
                foreach ($entityRows as $row) {
                    $rows[] = $row;
                }
            }

            if ($rows) {
                $this->connection->insertOnDuplicate($tableName, $rows, $this->getTableColumns());

                return true;
            }
        }
        return false;
    }

    /**
     * Delete entities
     *
     * @param array $entityIds
     *
     * @return bool
     */
    private function deleteEntityFinish(array $entityIds): bool
    {
        if ($entityIds) {
            try {
                $this->countItemsDeleted += $this->connection->delete(
                    $this->connection->getTableName(static::TABLE),
                    $this->connection->quoteInto(static::ENTITY_ID_COLUMN . ' IN (?)', $entityIds)
                );
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }

        return false;
    }

    /**
     * Get available columns
     *
     * @return array
     */
    private function getAvailableColumns(): array
    {
        return $this->validColumnNames;
    }

    /**
     * Get table columns
     *
     * @return array
     */
    private function getTableColumns(): array
    {
        return $this->tableColumnNames;
    }

    /**
     * @inheritDoc
     */
    public function validateRow(array $rowData, $rowNum)
    {
        $sellerEmail = $rowData['seller_email'] ?? '';
        $attributeSet = $rowData['attribute_set'] ?? '';
        $macrozone = $rowData['macrozone'] ?? '';
        $price = $rowData['price'] ?? '';
        $websiteId = $rowData['website_id'] ?? '';

        if (!$sellerEmail && !isset($rowData['seller_id'])) {
            $this->addRowError('SellerEmailIsRequired', $rowNum);
        }/* elseif (\Zend_Validate::is(trim($sellerEmail), 'EmailAddress')) {
            $this->addRowError('SellerEmailMustBeEmail', $rowNum);
        }*/
        if (!$attributeSet && !isset($rowData['attribute_set_id'])) {
            $this->addRowError('AttributeSetIsRequired', $rowNum);
        }
        if (!$macrozone) {
            $this->addRowError('MacrozoneIsRequired', $rowNum);
        }
        if (!$price && $price != 0) {
            $this->addRowError('PriceIsRequired', $rowNum);
        } elseif (!is_numeric($price)) {
            $this->addRowError('PriceMustBeNumber', $rowNum);
        }
        if (!$websiteId) {
            $this->addRowError('WebsiteIsRequired', $rowNum);
        }

        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;

        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    /**
     * @return $this|\Linets\SellerCentralizedDispatchCost\Model\Import\Prices
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\ValidatorException
     */
    protected function _saveValidatedBunches()
    {
        $source = $this->getSource();
        $currentDataSize = 0;
        $bunchRows = [];
        $startNewBunch = false;
        $nextRowBackup = [];
        $maxDataSize = $this->_resourceHelper->getMaxDataSize();
        $bunchSize = $this->_importExportData->getBunchSize();

        $source->rewind();
        $this->_dataSourceModel->cleanBunches();

        while ($source->valid() || $bunchRows) {
            if ($startNewBunch || !$source->valid()) {
                $this->_dataSourceModel->saveBunch($this->getEntityTypeCode(), $this->getBehavior(), $bunchRows);

                $bunchRows = $nextRowBackup;
                $currentDataSize = strlen($this->getSerializer()->serialize($bunchRows));
                $startNewBunch = false;
                $nextRowBackup = [];
            }

            if ($source->valid()) {
                try {
                    $rowData = $source->current();
                    $this->_processedEntitiesCount++;
                } catch (\InvalidArgumentException $e) {
                    $this->addRowError($e->getMessage(), $this->_processedRowsCount);
                    $this->_processedRowsCount++;
                    $source->next();
                    continue;
                }

                $this->_processedRowsCount++;

                if ($this->validateRow($rowData, $source->key())) {
                    // add row to bunch for save
                    $rowData = $this->_prepareRowForDb($rowData);
                    $rowSize = strlen($this->jsonHelper->jsonEncode($rowData));

                    $isBunchSizeExceeded = $bunchSize > 0 && count($bunchRows) >= $bunchSize;

                    if ($currentDataSize + $rowSize >= $maxDataSize || $isBunchSizeExceeded) {
                        $startNewBunch = true;
                        $nextRowBackup = [$source->key() => $rowData];
                    } else {
                        $bunchRows[$source->key()] = $rowData;
                        $currentDataSize += $rowSize;
                    }
                }
                $source->next();
            }
        }


        return $this;
    }

    /**
     * Change row data before saving in DB table.
     *
     * @param array $rowData
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _prepareRowForDb(array $rowData)
    {
        $customer = clone $this->customerModel;
        $this->customerResourceModel->loadByEmail($customer, $rowData['seller_email']);
        if (!$customer || !$customer->getEntityId()) {
            throw new LocalizedException(__('Seller with email "%s" not found.', $rowData['seller_email']));
        }

        $entityTypeId = $this->eavSetup->getEntityTypeId(Product::ENTITY);
        $attributeSetId = $this->eavSetup->getAttributeSetId($entityTypeId, $rowData['attribute_set']);
        if (!$attributeSetId) {
            throw new LocalizedException(__('Attribute Set "%s" not found.', $rowData['attribute_set']));
        }

        $website = $this->websiteFactory->create();
        $website->load($rowData['website_id']);
        if (!is_numeric($rowData['website_id']) && !$website->getId()) {
            throw new LocalizedException(__('Website "%s" not found.', $rowData['website_id']));
        }

        $rowData['seller_id'] = $customer->getEntityId();
        $rowData['attribute_set_id'] = $attributeSetId;
        $rowData['website_id'] = $website->getId();

        unset($rowData['seller_email'], $rowData['attribute_set']);
        foreach ($rowData as $key => $val) {
            if ($val === '') {
                $rowData[$key] = null;
            }
        }
        return $rowData;
    }

    /**
     * Get Serializer instance
     *
     * Workaround. Only way to implement dependency and not to break inherited child classes
     *
     * @return Json
     * @deprecated 100.2.0
     */
    private function getSerializer()
    {
        if (null === $this->serializer) {
            $this->serializer = ObjectManager::getInstance()->get(Json::class);
        }
        return $this->serializer;
    }

    /**
     * Init Error Messages
     */
    private function initMessageTemplates()
    {
        foreach ($this->errorMessageTemplates as $errorCode => $message) {
            $this->getErrorAggregator()->addErrorMessageTemplate($errorCode, $message);
        }
        $this->addMessageTemplate(
            'SellerEmailIsRequired',
            __('The seller_email cannot be empty.')
        );
        $this->addMessageTemplate(
            'SellerEmailMustBeEmail',
            __('The seller_email column must contain an email.')
        );
        $this->addMessageTemplate(
            'AttributeSetIsRequired',
            __('The attribute_set cannot be empty.')
        );
        $this->addMessageTemplate(
            'MacrozoneIsRequired',
            __('The macrozone cannot be empty.')
        );
        $this->addMessageTemplate(
            'PriceIsRequired',
            __('The price cannot be empty.')
        );
        $this->addMessageTemplate(
            'PriceMustBeNumber',
            __('The price column must contain numbers only.')
        );
        $this->addMessageTemplate(
            'WebsiteIsRequired',
            __('The website_id cannot be empty, please specify a website id or code')
        );

    }
}