<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Model;

use Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterfaceFactory;
use Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceSearchResultsInterfaceFactory;
use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceRepositoryInterface;
use Linets\SellerCentralizedDispatchCost\Model\ResourceModel\DispatchPrice as ResourceDispatchPrice;
use Linets\SellerCentralizedDispatchCost\Model\ResourceModel\DispatchPrice\CollectionFactory as DispatchPriceCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class DispatchPriceRepository implements DispatchPriceRepositoryInterface
{

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $dataDispatchPriceFactory;

    protected $searchResultsFactory;

    protected $dispatchPriceCollectionFactory;

    protected $dispatchPriceFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourceDispatchPrice $resource
     * @param DispatchPriceFactory $dispatchPriceFactory
     * @param DispatchPriceInterfaceFactory $dataDispatchPriceFactory
     * @param DispatchPriceCollectionFactory $dispatchPriceCollectionFactory
     * @param DispatchPriceSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceDispatchPrice $resource,
        DispatchPriceFactory $dispatchPriceFactory,
        DispatchPriceInterfaceFactory $dataDispatchPriceFactory,
        DispatchPriceCollectionFactory $dispatchPriceCollectionFactory,
        DispatchPriceSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->dispatchPriceFactory = $dispatchPriceFactory;
        $this->dispatchPriceCollectionFactory = $dispatchPriceCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataDispatchPriceFactory = $dataDispatchPriceFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface $dispatchPrice
    ) {
        /* if (empty($dispatchPrice->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $dispatchPrice->setStoreId($storeId);
        } */
        
        $dispatchPriceData = $this->extensibleDataObjectConverter->toNestedArray(
            $dispatchPrice,
            [],
            \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface::class
        );
        
        $dispatchPriceModel = $this->dispatchPriceFactory->create()->setData($dispatchPriceData);
        
        try {
            $this->resource->save($dispatchPriceModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the dispatchPrice: %1',
                $exception->getMessage()
            ));
        }
        return $dispatchPriceModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($dispatchPriceId)
    {
        $dispatchPrice = $this->dispatchPriceFactory->create();
        $this->resource->load($dispatchPrice, $dispatchPriceId);
        if (!$dispatchPrice->getId()) {
            throw new NoSuchEntityException(__('DispatchPrice with id "%1" does not exist.', $dispatchPriceId));
        }
        return $dispatchPrice->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->dispatchPriceCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface $dispatchPrice
    ) {
        try {
            $dispatchPriceModel = $this->dispatchPriceFactory->create();
            $this->resource->load($dispatchPriceModel, $dispatchPrice->getDispatchpriceId());
            $this->resource->delete($dispatchPriceModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the DispatchPrice: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($dispatchPriceId)
    {
        return $this->delete($this->get($dispatchPriceId));
    }
}

