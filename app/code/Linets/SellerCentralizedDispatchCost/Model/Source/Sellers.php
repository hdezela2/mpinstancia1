<?php

namespace Linets\SellerCentralizedDispatchCost\Model\Source;

use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory;

class Sellers extends AbstractSource implements SourceInterface, OptionSourceInterface
{
    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Seller\Collection
     */
    protected $collection;

    /**
     * @param \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $collectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        CollectionFactory     $collectionFactory,
        StoreManagerInterface $storeManager
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * Convert collection items to select options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];

        if (!$this->collection) {
            $this->loadSellers();
        }

        foreach ($this->collection as $item) {
            $option['value'] = $item->getSellerId();
            $option['label'] = $item->getShopTitle();
            $options[] = $option;
        }
        return $options;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $options = [];
        if (!$this->collection) {
            $this->loadSellers();
        }

        foreach ($this->collection as $item) {
            $option['value'] = $item->getSellerId();
            $option['label'] = $item->getShopTitle();
            $options[] = $option;
        }
        return $options;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function loadSellers()
    {
        $websiteId = $this->storeManager->getStore(InsumosConstants::STORE_CODE)->getWebsiteId();
        $this->collection = $this->collectionFactory->create();
        $this->collection
            ->getSelect()
            ->join(['e' => 'customer_entity'], 'e.entity_id = main_table.seller_id', ['website_id'])
            ->where('e.website_id = ?', $websiteId)
            ->order('shop_title ASC');
    }
}
