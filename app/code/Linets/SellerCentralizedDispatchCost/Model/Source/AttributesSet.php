<?php
declare(strict_types = 1);

namespace Linets\SellerCentralizedDispatchCost\Model\Source;

use Linets\InsumosSetup\Api\Setup\AttributeSetInterface;
use Magento\Catalog\Model\ResourceModel\Product;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

class AttributesSet implements OptionSourceInterface
{
    /**
     * @var null|array
     */
    protected $options;
    /**
     * @var Product
     */
    private $product;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     * @param Product           $product
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Product           $product
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->product = $product;
    }

    /**
     * @return string[]
     */
    public function toOptionArray(): array
    {
        $options = [];
        if (null == $this->options) {
            $this->options = $this->collectionFactory->create()
                ->setEntityTypeFilter($this->product->getTypeId())
                ->toOptionArray();

            foreach ($this->options as $k => $v) {
                if (in_array($v['label'], AttributeSetInterface::ATTRIBUTE_SETS)) {
                    $options[] = $v;
                }
            }

            usort($options, function ($a, $b) {
                return strcmp($a["label"], $b["label"]);
            });
        }

        return !empty($options) ? $options : $this->options;
    }
}
