<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Model\ResourceModel\DispatchPrice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'dispatchprice_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Linets\SellerCentralizedDispatchCost\Model\DispatchPrice::class,
            \Linets\SellerCentralizedDispatchCost\Model\ResourceModel\DispatchPrice::class
        );
    }
}

