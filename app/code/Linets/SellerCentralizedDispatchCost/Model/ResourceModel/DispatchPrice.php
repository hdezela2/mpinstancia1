<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Model\ResourceModel;

class DispatchPrice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('dccp_centralized_dispatch_price', 'dispatchprice_id');
    }
}

