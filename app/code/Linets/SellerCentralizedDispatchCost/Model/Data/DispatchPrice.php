<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Model\Data;

use Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface;

class DispatchPrice extends \Magento\Framework\Api\AbstractExtensibleObject implements DispatchPriceInterface
{

    /**
     * Get dispatchprice_id
     * @return string|null
     */
    public function getDispatchpriceId()
    {
        return $this->_get(self::DISPATCHPRICE_ID);
    }

    /**
     * Set dispatchprice_id
     * @param string $dispatchpriceId
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setDispatchpriceId($dispatchpriceId)
    {
        return $this->setData(self::DISPATCHPRICE_ID, $dispatchpriceId);
    }

    /**
     * Get seller_id
     * @return string|null
     */
    public function getSellerId()
    {
        return $this->_get(self::SELLER_ID);
    }

    /**
     * Set seller_id
     * @param string $sellerId
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setSellerId($sellerId)
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get attribute_set_id
     * @return string|null
     */
    public function getAttributeSetId()
    {
        return $this->_get(self::ATTRIBUTE_SET_ID);
    }

    /**
     * Set attribute_set_id
     * @param string $attributeSetId
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setAttributeSetId($attributeSetId)
    {
        return $this->setData(self::ATTRIBUTE_SET_ID, $attributeSetId);
    }

    /**
     * Get macrozone
     * @return string|null
     */
    public function getMacrozone()
    {
        return $this->_get(self::MACROZONE);
    }

    /**
     * Set macrozone
     * @param string $macrozone
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setMacrozone($macrozone)
    {
        return $this->setData(self::MACROZONE, $macrozone);
    }

    /**
     * Get price
     * @return string|null
     */
    public function getPrice()
    {
        return $this->_get(self::PRICE);
    }

    /**
     * Set price
     * @param string $price
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setPrice($price)
    {
        return $this->setData(self::PRICE, $price);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get website_id
     * @return string|null
     */
    public function getWebsiteId()
    {
        return $this->_get(self::WEBSITE_ID);
    }

    /**
     * Set website_id
     * @param string $websiteId
     * @return \Linets\SellerCentralizedDispatchCost\Api\Data\DispatchPriceInterface
     */
    public function setWebsiteId($websiteId)
    {
        return $this->setData(self::WEBSITE_ID, $websiteId);
    }
}

