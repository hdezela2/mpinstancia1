# Mage2 Module Linets SellerCentralizedDispatchCost

    ``linets/module-sellercentralizeddispatchcost``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Modulo para importar flete de insumos medicos

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Linets`
 - Enable the module by running `php bin/magento module:enable Linets_SellerCentralizedDispatchCost`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require linets/module-sellercentralizeddispatchcost`
 - enable the module by running `php bin/magento module:enable Linets_SellerCentralizedDispatchCost`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - Plugin
	- afterGetProductWithSpecialPriceBySeller - Formax\Offer\Helper\Data > Linets\SellerCentralizedDispatchCost\Plugin\Frontend\Formax\Offer\Helper\Data


## Attributes



