<?php
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Setup\Patch\Data;

use Linets\InsumosSetup\Model\InsumosConstants;
use Linets\InsumosSetup\Setup\Patch\Data\UserRoleSetup;
use Linets\InsumosSetup\Setup\Patch\Data\WebsiteSetup;
use Magento\Authorization\Model\ResourceModel\Role as RoleResourceModel;
use Magento\Authorization\Model\RoleFactory;
use Magento\Authorization\Model\RulesFactory;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class UserRoleUpdate implements DataPatchInterface
{

    const ACL_RESOURCES = [
        'Linets_SellerCentralizedDispatchCost::DispatchPrice',
        'Linets_SellerCentralizedDispatchCost::DispatchPrice_view'
    ];

    /**
     * @var RoleFactory
     */
    protected $roleFactory;

    /**
     * @var RulesFactory
     */
    protected $rulesFactory;

    /**
     * @var RoleResourceModel
     */
    private $roleResourceModel;

    /**
     * @param RulesFactory $rulesFactory
     * @param RoleResourceModel $roleResourceModel
     * @param \Magento\Authorization\Model\RoleFactory $roleFactory
     */
    public function __construct(
        RulesFactory      $rulesFactory,
        RoleResourceModel $roleResourceModel,
        RoleFactory       $roleFactory
    )
    {
        $this->rulesFactory = $rulesFactory;
        $this->roleResourceModel = $roleResourceModel;
        $this->roleFactory = $roleFactory;
    }

    /**
     * @return void
     */
    public function apply()
    {
        $agreementId = InsumosConstants::ID_AGREEMENT;

        $role = $this->roleFactory->create();
        $this->roleResourceModel->load($role, $agreementId, 'role_name');
        if ($roleId = $role->getRoleId()) {
            $resources = array_merge(UserRoleSetup::ACL_RESOURCES, self::ACL_RESOURCES);
            $this->rulesFactory->create()
                ->setRoleId($roleId)
                ->setResources($resources)
                ->saveRel();
        }
    }

    /**
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class,
            UserRoleSetup::class
        ];
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }

}
