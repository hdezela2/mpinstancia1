<?php
declare(strict_types=1);

namespace Linets\SellerCentralizedDispatchCost\Helper;

use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{

    /**
     * @var DispatchPriceManagementInterface
     */
    private $dispatchPriceManagement;

    /**
     * @param Context $context
     * @param DispatchPriceManagementInterface $dispatchPriceManagement
     */
    public function __construct(
        Context                          $context,
        DispatchPriceManagementInterface $dispatchPriceManagement
    )
    {
        parent::__construct($context);
        $this->dispatchPriceManagement = $dispatchPriceManagement;
    }

    /**
     * @param int $configurableId
     * @param int $sellerId
     * @param int $websiteId
     * @return array
     */
    public function getChildDistpatchPrices($configurableId, $sellerId, $websiteId)
    {
        return $this->dispatchPriceManagement->getChildDispatchPrices($configurableId, $sellerId, $websiteId);
    }

    /**
     * @param $sellerId
     * @param $macrozone
     * @param $attributeSetId
     * @param $websiteId
     * @return int|string|null
     */
    public function getNewChildDistpatchPrices($sellerId, $macrozone, $attributeSetId, $websiteId)
    {
        $data = $this->dispatchPriceManagement->getDispatchPrice($sellerId, $macrozone, $attributeSetId, $websiteId);
        return $data ? $data->getPrice() : null;
    }
}
