<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Linets_EmergenciasRenewalSetup',
    __DIR__
);

