<?php
declare(strict_types = 1);

namespace Linets\EmergenciasRenewalSetup\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Linets\InsumosSetup\Api\Setup\AttributeSetInterface;

class AddProductAttributesToSet implements DataPatchInterface
{
    /** @var string[]  */
    const REQUIRED_ATTRIBUTES = [
        'max_base_price',
        'max_shipping_price'
    ];

    /**/
    /** @var string[]  Attribute Set Names */
    const ATTRIBUTE_SETS = [
        'ARRIENDO DIARIO ESTUFA',
        'ARRIENDO POR HORA CAMIÓN ALJIBE AGUA POTABLE',
        'ARRIENDO POR HORA CAMIÓN ALJIBE EXTINCIÓN DE INCENDIOS',
        'ARRIENDO SEMANAL CARPA',
        'BAÑO MODULAR PARA VIVIENDA DE EMERGENCIA',
        'BOTAS DE AGUA',
        'KIT DE ALIMENTACIÓN',
        'KIT DE ASEO DOMICILIARIO',
        'KIT DE HIGIENE',
        'SACO DEFENSA FLUVIAL',
        'TRAJE PARA AGUA PVC',
        'VIVIENDA DE EMERGENCIA CON BAÑO',
        'VIVIENDA DE EMERGENCIA SIN BAÑO',
        'AGUA ENVASADA SIN GAS',
        'ALMOHADA',
        'BOLSA MORTUORIA',
        'BOMBA MOCHILA',
        'CAMA',
        'CAMAROTE',
        'CARBON',
        'CARPA',
        'COLCHÓN',
        'ESTANQUE DE AGUA',
        'ESTANQUE DE AGUA',
        'FRAZADA',
        'JUEGO DE SÁBANAS',
        'KIT DE COCINA',
        'KIT DE SEGURIDAD',
        'LEÑA',
        'MANGA PLÁSTICA',
        'MANGUERA DE RIEGO',
        'RACIÓN DE ALIMENTOS'
    ];

    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /** @var EavSetupFactory */
    private $eavSetupFactory;

    /** @var AttributeSetInterface */
    private $attributeSetSetup;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory          $eavSetupFactory
     * @param AttributeSetInterface    $attributeSetSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory          $eavSetupFactory,
        AttributeSetInterface    $attributeSetSetup
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetSetup = $attributeSetSetup;

    }

    /**
     * @return AddProductAttributesToSet|void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $entity_type_id = $eavSetup->getEntityTypeId(Product::ENTITY);
        $group_name = 'Product Details';

        foreach (self::ATTRIBUTE_SETS as $attribute_set_name) {
            $attribute_set_id = $eavSetup->getAttributeSetId($entity_type_id, $attribute_set_name);
            $attribute_group_id = $eavSetup->getAttributeGroupId($entity_type_id, $attribute_set_id, $group_name);
            $sortOrder= 100;
            foreach (self::REQUIRED_ATTRIBUTES as $attributeSetName) {
                $attribute_id = $eavSetup->getAttributeId($entity_type_id, $attributeSetName);
                $eavSetup->addAttributeToSet($entity_type_id, $attribute_set_id, $attribute_group_id, $attribute_id);
                $eavSetup->addAttributeToGroup(
                    $entity_type_id,
                    $attribute_set_id,
                    $attribute_group_id,
                    $attribute_id,
                    $sortOrder++
                );
                $sortOrder++;
            }
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getAliases()
    {
        return [
            \Summa\ProductAttributes\Setup\InstallData::class
        ];
    }
}
