<?php

namespace Linets\EmergenciasRenewalSetup\Setup\Patch\Schema;

use Intellicore\EmergenciasRenewal\Constants as EmergenciasRenewalConstants;
use Intellicore\EmergenciasRenewal\Setup\Patch\Data\EmergenciasRenewalSetup;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Store\Model\StoreFactory;

class ForceCreateSalesSequences implements SchemaPatchInterface
{
    /**
     * @var StoreFactory
     */
    private $storeFactory;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @param StoreFactory     $storeFactory
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        StoreFactory     $storeFactory,
        ManagerInterface $eventManager
    )
    {
        $this->storeFactory = $storeFactory;
        $this->eventManager = $eventManager;
    }

    /**
     * @throws LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(EmergenciasRenewalConstants::STORE_VIEW_CODE);
        if ($store->getId()) {
            // Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
            $this->eventManager->dispatch('store_add', ['store' => $store]);
        }
    }

    /**
     * @return string[]
     */
    public static function getDependencies()
    {
        return [
            EmergenciasRenewalSetup::class
        ];
    }

    /**
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }
}