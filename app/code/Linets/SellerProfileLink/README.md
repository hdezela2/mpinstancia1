# Mage2 Module Linets SellerProfileLink

    ``linets/module-sellerprofilelink``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Module to add seller profile link to MP

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Linets`
 - Enable the module by running `php bin/magento module:enable Linets_SellerProfileLink`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require linets/module-sellerprofilelink`
 - enable the module by running `php bin/magento module:enable Linets_SellerProfileLink`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Endpoint (dccp_endpoint/endpoint_sellers/endpoint)


## Specifications

 - Plugin
	- afterToHtml - Webkul\Marketplace\Block\Profile > Linets\SellerProfileLink\Plugin\Frontend\Webkul\Marketplace\Block\Profile


## Attributes



