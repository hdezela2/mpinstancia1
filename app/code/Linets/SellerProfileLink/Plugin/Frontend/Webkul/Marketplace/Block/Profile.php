<?php
/**
 * Copyright © LInets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SellerProfileLink\Plugin\Frontend\Webkul\Marketplace\Block;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Model\Customer;

class Profile
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Customer\Model\Customer
     */
    private $customer;
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param Session $customerSession
     * @param Customer $customer
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Session $customerSession,
        Customer $customer
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $customerSession;
        $this->customer = $customer;
    }

    /**
     * @param \Webkul\Marketplace\Block\Profile $subject
     * @param $result
     * @return mixed|string
     */
    public function afterToHtml(
        \Webkul\Marketplace\Block\Profile $subject,
                                          $result
    )
    {
        $sellerId = $subject->getProfileDetail()->getSellerId();
        $seller = $this->customer->load($sellerId);
        $rutSeller = $seller->getData('wkv_dccp_rut');
        $token = $this->customerSession->getCustomer()->getUserRestAtk();
        $endPointSellerValue = $this->scopeConfig->getValue('dccp_endpoint/endpoint_sellers/endpoint').'ficha/';
        if(!$token) {
            //Perfil Publico
            $endPointSellerValue.= $rutSeller;
        }else {
            //perfil privado
            $endPointSellerValue.= 'callback?access_token='.$token.'&rut='.$rutSeller.'&expires_in=28800';
        }
        /**
         * TODO:
         * Use $this->scopeConfig to get dccp endpoint value
         * Evaluate if user is logged in or not
         * if user is logged in, get integration token and render the corresponding link
         */
        $name = $subject->getNameInLayout();
        if ($name == 'marketplace_seller_top_block') {
            $result .= "<div class='button-sellers-profile-link'><a class='action primary action-sellers' style='z-index:100;' target='_blank' href='".$endPointSellerValue."'>Ir a ficha del proveedor en Mercado Público</a></div>";
        }

        return $result;
    }
}

