<?php

namespace Linets\VoucherSetup\Model;

class Constants
{
    const ID_AGREEMENT_VOUCHER_2021_06 = 5800295;
    const WEBSITE_NAME = 'Voucher 202106 Website';
    const WEBSITE_CODE = 'voucher202106';
    const STORE_NAME = 'Voucher 202106 Store';
    const STORE_CODE = 'voucher202106';
    const STORE_VIEW_NAME = 'Voucher 202106';
    const STORE_VIEW_CODE = 'voucher202106';
    /** Categoria Temporal */
    const ROOT_CATEGORY_NAME = 'DCCP Voucher202106';
    const FRONT_THEME = 'Linets/Voucher';
    const FRONT_THEME_DEFAULT = 'Formax/ChileCompra';
    const XML_PATH_REVIEW_ACTIVE = 'catalog/review/active';
    const XML_PATH_REVIEW_ALLOW_GUEST = 'catalog/review/allow_guest';
    const XML_PATH_NUMBER_OF_DAYS = 'stock_management_configuration/general/number_of_days';
    const XML_PATH_STOCK_MANAGEMENT_ENABLED = 'stock_management_configuration/general/enable';
}