<?php

namespace Linets\VoucherSetup\Setup\Patch\Data;

use Linets\VoucherSetup\Model\Constants as VoucherSetupConstants;
use MageMoto\ElasticsearchStoreSwitch\Block\Store as StoreBlock;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class StoreConfig implements DataPatchInterface
{
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    private $storeFactory;
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * StoreConfig constructor.
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        StoreFactory $storeFactory,
        WriterInterface $configWriter,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(VoucherSetupConstants::STORE_VIEW_CODE);
        if ($store->getId()) {
            /** Add store to search box on front */
            $data = $this->scopeConfig->getValue(StoreBlock::XML_PATH_EMAIL_RECIPIENT);
            $arrayValue = $data ? explode(',', $data) : [];
            $arrayValue[] = $store->getId();
            $strValue = implode(',', $arrayValue);
            $this->configWriter->save(StoreBlock::XML_PATH_EMAIL_RECIPIENT, $strValue);

            /** Disable reviews for product on front */
            $websiteId = $store->getWebsiteId();
            $scope = ScopeInterface::SCOPE_WEBSITES;
            $this->configWriter->save(VoucherSetupConstants::XML_PATH_REVIEW_ACTIVE, 0, $scope, $websiteId);
            $this->configWriter->save(VoucherSetupConstants::XML_PATH_REVIEW_ALLOW_GUEST, 0, $scope, $websiteId);

            /** ToDo */
        }
    }

    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [
            VoucherSetup::class
        ];
    }
}