<?php

namespace Linets\VoucherSetup\Setup\Patch\Data;

use Linets\VoucherSetup\Model\Constants as VoucherSetupConstants;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Store\Model\Group;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group as GroupResource;
use Magento\Store\Model\ResourceModel\Website as WebsiteResource;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\Website;
use Magento\Store\Model\WebsiteFactory;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory as ThemeCollectionFactory;
use Magento\Theme\Model\Theme;

class VoucherSetup implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    protected $_moduleDataSetup;

    /** @var EavSetupFactory */
    protected $_eavSetupFactory;

    /** @var WebsiteFactory */
    protected $_websiteFactory;

    /** @var WebsiteResource */
    protected $_websiteResourceModel;

    /** @var StoreFactory */
    protected $_storeFactory;

    /** @var GroupFactory     */
    protected $_groupFactory;

    /** @var GroupResource */
    protected $_groupResourceModel;

    /** @var Store */
    protected $_storeModel;

    /** @var ManagerInterface */
    protected $_eventManager;

    /** @var CategoryFactory */
    protected $_categoryFactory;

    /** @var ConfigInterface */
    protected $_configInterface;

    /** @var ThemeCollectionFactory */
    protected $_themeCollectionFactory;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        WebsiteFactory $websiteFactory,
        WebsiteResource $websiteResourceModel,
        Store $storeModel,
        GroupResource $groupResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory,
        ManagerInterface $eventManager,
        CategoryFactory $categoryFactory,
        ConfigInterface $configInterface,
        ThemeCollectionFactory $themeCollectionFactory
    ) {
        $this->_moduleDataSetup = $moduleDataSetup;
        $this->_eavSetupFactory = $eavSetupFactory;
        $this->_websiteFactory = $websiteFactory;
        $this->_websiteResourceModel = $websiteResourceModel;
        $this->_storeFactory = $storeFactory;
        $this->_groupFactory = $groupFactory;
        $this->_groupResourceModel = $groupResourceModel;
        $this->_storeModel = $storeModel;
        $this->_eventManager = $eventManager;
        $this->_categoryFactory = $categoryFactory;
        $this->_configInterface = $configInterface;
        $this->_themeCollectionFactory = $themeCollectionFactory;
    }

    /**
     * @return $this
     *
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        ##### CREATE WEBSITE #####
        /** @var Website $website */
        $website = $this->_websiteFactory->create();
        $website->load(VoucherSetupConstants::WEBSITE_CODE);
        if (!$website->getId()) {
            $website->setCode(VoucherSetupConstants::WEBSITE_CODE);
            $website->setName(VoucherSetupConstants::WEBSITE_NAME);
            $this->_websiteResourceModel->save($website);
        }

        ##### CREATE ROOT CATEGORY #####
        $category = $this->createOrUpdateRootCategory();

        ##### CREATE STORE #####
        if ($website->getId()) {
            /** @var Group $group */
            $group = $this->_groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName(VoucherSetupConstants::STORE_NAME);
            $group->setCode(VoucherSetupConstants::STORE_CODE);
            $group->setRootCategoryId($category->getId());
            $group->setDefaultStoreId(0);
            $this->_groupResourceModel->save($group);
        }

        ##### CREATE STORE VIEW #####
        /** @var  Store $store */
        $store = $this->_storeFactory->create();
        $store->load(VoucherSetupConstants::STORE_VIEW_CODE);
        if (!$store->getId()) {
            $group = $this->_groupFactory->create();
            $group->load(VoucherSetupConstants::STORE_NAME, 'name');
            $store->setCode(VoucherSetupConstants::STORE_VIEW_CODE);
            $store->setName(VoucherSetupConstants::STORE_VIEW_NAME);
            $store->setWebsiteId($website->getId());
            $store->setGroupId($group->getId());
            $store->setIsActive(1);
            $store->save();
        }

        ##### ASSIGN THEME TO STORE VIEW #####
        $themes = $this->_themeCollectionFactory->create()->loadRegisteredThemes();
        /** @var Theme $theme */
        foreach ($themes as $theme) {
            if ($theme->getArea() == 'frontend' && $theme->getCode() == VoucherSetupConstants::FRONT_THEME) {
                $this->_configInterface->saveConfig('design/theme/theme_id', $theme->getId(), 'stores', $store->getId());
            }
        }
        return $this;
    }

    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return Category
     * @throws \Exception
     */
    private function createOrUpdateRootCategory()
    {
        $category = $this->_categoryFactory->create();
        $category->setName(VoucherSetupConstants::ROOT_CATEGORY_NAME);
        $category->setIsActive(true);
        $category->setStoreId(0);
        $parentCategory = $this->_categoryFactory->create();
        $parentCategory->load(Category::TREE_ROOT_ID);
        $category->setDisplayMode(Category::DM_PRODUCT);
        $category->setPath($parentCategory->getPath());
        $category->save();
        return $category;
    }
}
