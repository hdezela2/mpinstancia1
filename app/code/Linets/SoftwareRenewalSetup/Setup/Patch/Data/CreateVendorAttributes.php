<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Linets\SoftwareRenewalSetup\Setup\Patch\Data;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Webkul\MpVendorAttributeManager\Model\VendorAttributeFactory;
use Webkul\MpVendorAttributeManager\Model\ResourceModel\VendorAttribute as VendorAttributeResource;
use Magento\Customer\Model\ResourceModel\Attribute as CustomerAttributeResource;

/**
 * Patch is mechanism, that allows to do atomic upgrade data changes
 */
class CreateVendorAttributes implements
    DataPatchInterface
{
    const USED_FOR = 2;

    /**
     * @var SoftwareRenewalConstants
     */
    private $constants;
    /**
     * @var \Webkul\MpVendorAttributeManager\Model\VendorAttributeFactory
     */
    private $vendorAttributeFactory;
    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;
    /**
     * @var \Magento\Eav\Model\Entity\Attribute\SetFactory
     */
    private $attributeSetFactory;
    /**
     * @var \Webkul\MpVendorAttributeManager\Model\ResourceModel\VendorAttribute
     */
    private $vendorAttributeResource;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Attribute
     */
    private $customerAttributeResource;

    /**
     * CreateVendorAttributes constructor.
     * @param SoftwareRenewalConstants $constants
     * @param \Webkul\MpVendorAttributeManager\Model\VendorAttributeFactory $vendorAttributeFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
     * @param VendorAttributeResource $vendorAttributeResource
     * @param CustomerAttributeResource $customerAttributeResource
     */
    public function __construct(
        SoftwareRenewalConstants $constants,
        VendorAttributeFactory $vendorAttributeFactory,
        Config $eavConfig,
        SetFactory $attributeSetFactory,
        VendorAttributeResource $vendorAttributeResource,
        CustomerAttributeResource $customerAttributeResource
    )
    {
        $this->constants = $constants;
        $this->vendorAttributeFactory = $vendorAttributeFactory;
        $this->eavConfig = $eavConfig;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->vendorAttributeResource = $vendorAttributeResource;
        $this->customerAttributeResource = $customerAttributeResource;
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $customerEntity = $this->eavConfig->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        $sort = 350;
        foreach ($this->constants->getVendorAttributes() as $code => $label) {
            $this->createAttribute($code, $label, $attributeSetId, $attributeGroupId, $sort);
            $sort += 10;
        }
    }

    /**
     * @param $attributeCode
     * @param $label
     * @param $attributeSetId
     * @param $attributeGroupId
     * @param $sortOrder
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createAttribute($attributeCode, $label, $attributeSetId, $attributeGroupId, $sortOrder)
    {
        $vendorAttribute = $this->vendorAttributeFactory->create();
        $attribute = $this->eavConfig->getAttribute(Customer::ENTITY, $attributeCode);

        $attrData = [
            'attribute_code' => $attributeCode,
            'frontend_input' => 'text',
            'is_system' => false,
            'is_user_defined' => true,
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroupId,
            'used_in_forms' => ['adminhtml_customer'],
            'frontend_label' => $label,
            'frontend_class' => null,
            'sort_order' => $sortOrder,
            'position' => $sortOrder,
            'is_visible' => false,
            'frontend_type' => 'varchar',
            'backend_type' => 'varchar'
        ];
        $attribute->addData($attrData);
        $this->customerAttributeResource->save($attribute);

        /** save record for webkul attribute manager table **/
        $vendorAttribute->setAttributeId($attribute->getId());
        $vendorAttribute->setRequiredField(0);
        $vendorAttribute->setAttributeUsedFor(self::USED_FOR);
        $vendorAttribute->setShowInFront(0);
        $this->vendorAttributeResource->save($vendorAttribute);
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies(): array
    {
        return [
        ];
    }
}
