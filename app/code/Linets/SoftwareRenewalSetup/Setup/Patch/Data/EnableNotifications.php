<?php
declare(strict_types=1);

namespace Linets\SoftwareRenewalSetup\Setup\Patch\Data;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants as Constants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class EnableNotifications implements DataPatchInterface
{
    protected StoreFactory $_storeFactory;
    protected WriterInterface $_configWriter;

    /**
     * EnableNotifications Constructor
     *
     * @param StoreFactory $storeFactory
     * @param WriterInterface $configWriter
     */
    public function __construct(
        StoreFactory $storeFactory,
        WriterInterface $configWriter
    ) {
        $this->_storeFactory = $storeFactory;
        $this->_configWriter = $configWriter;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $store = $this->_storeFactory->create();
        $store->load(Constants::STORE_VIEW_CODE);
        $configs = [
            'seller_notification/general_settings/enable',
            'seller_notification/general_settings/enable_quote',
            'seller_notification/general_settings/seller_status',
            'seller_notification/general_settings/product_status',
            'seller_notification/general_settings/enable_penalty',
            'seller_notification/general_settings/enable_dispersion',
            'seller_notification/general_settings/price_readjustment',
        ];
        if (!$store->getId()) {
            throw new LocalizedException(__('Store %1 does not exists.', Constants::STORE_VIEW_CODE));
        }
        $websiteId = $store->getWebsiteId();
        $scope = ScopeInterface::SCOPE_WEBSITES;
        foreach ($configs as $path) {
            $this->_configWriter->save(
                $path,
                1,
                $scope,
                $websiteId
            );
        }
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAliases(): array
    {
        return [];
    }
}
