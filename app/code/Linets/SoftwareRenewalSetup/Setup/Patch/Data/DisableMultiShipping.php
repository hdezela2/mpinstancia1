<?php

namespace Linets\SoftwareRenewalSetup\Setup\Patch\Data;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Multishipping\Helper\Data as Constants;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;


class DisableMultiShipping implements DataPatchInterface
{
    /**
     * @var StoreFactory
     */
    private $storeFactory;
    /**
     * @var WriterInterface
     */
    private $configWriter;

    /**
     * StoreConfig constructor.
     *
     * @param StoreFactory    $storeFactory
     * @param WriterInterface $configWriter
     */
    public function __construct(
        StoreFactory $storeFactory,
        WriterInterface $configWriter
    )
    {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
    }

    /**
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class
        ];
    }

    /**
     * @return DisableMultiShipping|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(SoftwareRenewalConstants::STORE_VIEW_CODE);
        if ($store->getId()) {
            $websiteId = $store->getWebsiteId();
            $scope = ScopeInterface::SCOPE_WEBSITES;
            $this->configWriter->save(
                Constants::XML_PATH_CHECKOUT_MULTIPLE_AVAILABLE,
                0,
                $scope,
                $websiteId
            );
        }
    }

    public function getAliases(): array
    {
        return [];
    }
}
