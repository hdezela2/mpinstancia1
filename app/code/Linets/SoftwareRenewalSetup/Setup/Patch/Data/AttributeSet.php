<?php

namespace Linets\SoftwareRenewalSetup\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Linets\SoftwareRenewalSetup\Api\Setup\AttributeSetInterface;

class AttributeSet implements DataPatchInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private ModuleDataSetupInterface $moduleDataSetup;

    /**
     * @var AttributeSetInterface
     */
    private AttributeSetInterface $attributeSetSetup;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        AttributeSetInterface $attributeSetSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->attributeSetSetup = $attributeSetSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $this->attributeSetSetup->createAttributeSets($this->moduleDataSetup);
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            WebsiteSetup::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
