<?php


namespace Linets\SoftwareRenewalSetup\Setup\Patch\Data;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Catalog\Model\Category as Category;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Group as GroupResource;
use Magento\Store\Model\ResourceModel\Store as StoreResourceModel;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\ResourceModel\Website as WebsiteResource;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory as ThemeCollectionFactory;

class WebsiteSetup implements DataPatchInterface
{
    /** @var WebsiteFactory */
    protected $websiteFactory;

    /** @var WebsiteResource */
    protected $websiteResourceModel;

    /** @var StoreFactory */
    protected $storeFactory;

    /** @var GroupFactory */
    protected $groupFactory;

    /** @var GroupResource */
    protected $groupResourceModel;

    /** @var StoreResourceModel */
    protected $storeResourceModel;

    /** @var ManagerInterface */
    protected $eventManager;

    /** @var CategoryFactory */
    protected $categoryFactory;

    /** @var ConfigInterface */
    protected $configInterface;

    /** @var ThemeCollectionFactory */
    protected $themeCollectionFactory;

    /**
     * WebsiteSetup constructor.
     * @param WebsiteFactory $websiteFactory
     * @param WebsiteResource $websiteResourceModel
     * @param GroupResource $groupResourceModel
     * @param StoreFactory $storeFactory
     * @param GroupFactory $groupFactory
     * @param ManagerInterface $eventManager
     * @param CategoryFactory $categoryFactory
     * @param ConfigInterface $configInterface
     * @param ThemeCollectionFactory $themeCollectionFactory
     * @param StoreResourceModel $storeResourceModel
     */
    public function __construct(
        WebsiteFactory $websiteFactory,
        WebsiteResource $websiteResourceModel,
        GroupResource $groupResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory,
        ManagerInterface $eventManager,
        CategoryFactory $categoryFactory,
        ConfigInterface $configInterface,
        ThemeCollectionFactory $themeCollectionFactory,
        StoreResourceModel $storeResourceModel
    )
    {
        $this->websiteFactory = $websiteFactory;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->storeFactory = $storeFactory;
        $this->groupFactory = $groupFactory;
        $this->groupResourceModel = $groupResourceModel;
        $this->eventManager = $eventManager;
        $this->categoryFactory = $categoryFactory;
        $this->configInterface = $configInterface;
        $this->themeCollectionFactory = $themeCollectionFactory;
        $this->storeResourceModel = $storeResourceModel;
    }


    public static function getDependencies(): array
    {
        return [];
    }

    public function apply()
    {
        /**  Create Website */
        $website = $this->websiteFactory->create();
        $website->load(SoftwareRenewalConstants::WEBSITE_CODE);

        if (!$website->getId()) {
            $website->setCode(SoftwareRenewalConstants::WEBSITE_CODE);
            $website->setName(SoftwareRenewalConstants::WEBSITE_NAME);
            $this->websiteResourceModel->save($website);
        }

        /** Create root category */
        $category = $this->createOrUpdateRootCategory();

        /** CREATE STORE */

        if ($website->getId()) {
            $group = $this->groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName(SoftwareRenewalConstants::STORE_NAME);
            $group->setCode(SoftwareRenewalConstants::STORE_CODE);
            $group->setRootCategoryId($category->getId());
            $group->setDefaultStoreId(0);
            $this->groupResourceModel->save($group);
        }

        /** CREATE STORE VIEW */

        $store = $this->storeFactory->create();
        $store->load(SoftwareRenewalConstants::STORE_VIEW_CODE);

        if (!$store->getId()) {
            $group = $this->groupFactory->create();
            $group->load(SoftwareRenewalConstants::STORE_NAME, 'name');

            $store->setCode(SoftwareRenewalConstants::STORE_VIEW_CODE);
            $store->setName(SoftwareRenewalConstants::STORE_VIEW_NAME);
            $store->setWebsiteId($website->getId());
            $store->setGroupId($group->getId());
            $store->setIsActive(1);

            $this->storeResourceModel->save($store);
        }

        /** ASSIGN THEME TO STORE VIEW */

        $themes = $this->themeCollectionFactory->create()->loadRegisteredThemes();
        $isThemeAssigned = false;
        $defaultThemeId = false;

        foreach ($themes as $theme) {
            if ($theme->getArea() == 'frontend' && $theme->getCode() == SoftwareRenewalConstants::FRONT_THEME_DEFAULT) {
                $defaultThemeId = $theme->getId();
            }
            if ($theme->getArea() == 'frontend' && $theme->getCode() == SoftwareRenewalConstants::FRONT_THEME) {
                $this->configInterface->saveConfig(
                    'design/theme/theme_id',
                    $theme->getId(),
                    'store',
                    $store->getId()
                );
                $isThemeAssigned = true;
            }
        }

        if (!$isThemeAssigned && $defaultThemeId) {
            $this->configInterface->saveConfig(
                'design/theme/theme_id',
                $defaultThemeId,
                'store',
                $store->getId()
            );
        }

    }


    /**
     * @return Category
     * @throws \Exception
     */

    private function createOrUpdateRootCategory()
    {
        $category = $this->categoryFactory->create();
        $category->setName(SoftwareRenewalConstants::ROOT_CATEGORY_NAME);
        $category->setIsActive(true);
        $category->setStoreId(0);
        $parentCategory = $this->categoryFactory->create();
        $parentCategory->load(Category::TREE_ROOT_ID);
        $category->setDisplayMode(Category::DM_PRODUCT);
        $category->setPath($parentCategory->getPath());
        $category->save();
        return $category;
    }

    public function getAliases(): array
    {
        return [];
    }

}
