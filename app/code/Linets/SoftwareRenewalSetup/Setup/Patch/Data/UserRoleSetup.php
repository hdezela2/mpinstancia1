<?php


namespace Linets\SoftwareRenewalSetup\Setup\Patch\Data;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\ResourceModel\Role as RoleResourceModel;
use Magento\Authorization\Model\RoleFactory;
use Magento\Authorization\Model\RulesFactory;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\WebsiteRepository;

class UserRoleSetup implements DataPatchInterface
{

    const ACL_RESOURCES = [
        'Magento_Backend::dashboard',

        'Webkul_CategoryProductPrice::categoryproductprice',
        'Webkul_CategoryProductPrice::index',

        'Cleverit_CategoryProductPrice::history',

        'Formax_PriceDispersion::pricedispersion',
        'Formax_PriceDispersion::index',
        'Formax_PriceDispersion::history',

        'Webkul_Marketplace::marketplace',
        'Webkul_Marketplace::menu',
        'Webkul_Marketplace::seller',
        'Webkul_MpAssignProduct::product',
        'Formax_RegionalCondition::regional_condition',

        'Webkul_Mpshipping::menu',
        'Webkul_Mpshipping::mpshipping',
        'Webkul_Mpshipping::mpshippingset',

        'Webkul_Requestforquote::requestforquote',
        'Webkul_Requestforquote::quote_index',
        'Webkul_Requestforquote::index_index',

        'Magento_Analytics::analytics',
        'Magento_Analytics::analytics_api',

        'Magento_Catalog::catalog',
        'Magento_CatalogPermissions::catalog_magento_catalogpermissions',
        'Magento_Catalog::catalog_inventory',
        'Magento_Catalog::products',
        'Magento_PricePermissions::read_product_price',
        'Magento_PricePermissions::edit_product_price',
        'Magento_PricePermissions::edit_product_status',
        'Magento_Catalog::categories',

        'Magento_Customer::customer',
        'Magento_Customer::manage',
        'Magento_Reward::reward_balance',

        'Magento_Reports::report',
        'Magento_Reports::salesroot',
        'Formax_CustomReport::Report',

        'Magento_MediaGalleryUiApi::insert_assets',
        'Magento_MediaGalleryUiApi::upload_assets',
        'Magento_MediaGalleryUiApi::edit_assets',
        'Magento_MediaGalleryUiApi::delete_assets',
        'Magento_MediaGalleryUiApi::create_folder',
        'Magento_MediaGalleryUiApi::delete_folder',
        'Magento_Reports::report',
        'Magento_Reports::salesroot',

        'Magento_Backend::system',
        'Magento_Backend::convert',
        'Magento_ImportExport::export'
    ];

    public function __construct(
        WebsiteRepository $websiteRepository,
        RoleFactory $roleFactory,
        RulesFactory $rulesFactory,
        RoleResourceModel $roleResourceModel

    )
    {
        $this->websiteRepository = $websiteRepository;
        $this->roleFactory = $roleFactory;
        $this->rulesFactory = $rulesFactory;
        $this->roleResourceModel = $roleResourceModel;
    }

    /**
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class
        ];
    }

    public function apply()
    {
        $agreementId = SoftwareRenewalConstants::ID_AGREEMENT;
        $website = $this->websiteRepository->get(SoftwareRenewalConstants::WEBSITE_CODE);

        /** CREATE USER ROLE */
        $role = $this->roleFactory->create();
        //Set Role Name with Agreement ID of the new CM
        $role->setName($agreementId)
            ->setPid(0) //set parent role id of your role
            ->setRoleType(RoleGroup::ROLE_TYPE)
            ->setUserType(UserContextInterface::USER_TYPE_ADMIN)
            ->setGwsIsAll(0) // Doesn't have permission in all Store
            ->setGwsWebsites($website->getId()) //array with the website ids
            ->setGwsStoreGroups($website->getId());

        $this->roleResourceModel->save($role);

        //Set resouces to new role
        $this->rulesFactory->create()->setRoleId($role->getId())->setResources(self::ACL_RESOURCES)->saveRel();


    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }

}
