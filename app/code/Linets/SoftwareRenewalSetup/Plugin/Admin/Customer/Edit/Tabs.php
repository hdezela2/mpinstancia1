<?php
namespace Linets\SoftwareRenewalSetup\Plugin\Admin\Customer\Edit;

use Linets\SoftwareRenewalSetup\Model\SoftwareRenewalConstants as SoftwareRenewalConstants;
use Magento\Backend\Model\Auth\Session;


class Tabs
{
    /**
     * @var Session
     */
    private $authSession;

    /**
     * Tabs constructor.
     * @param Session $authSession
     */
    public function __construct(
        Session $authSession
    ) {
        $this->authSession = $authSession;
    }

    /**
     * @param \Webkul\MpVendorAttributeManager\Block\Adminhtml\Customer\Edit\Tabs $subject
     * @param $result
     */


    public function afterGetCustomerAttribtues(
        \Webkul\MpVendorAttributeManager\Block\Adminhtml\Customer\Edit\Tabs $subject,
        $result)
    {

        $currentAdmin = $this->authSession->getUser();
        $currentRole = $currentAdmin->getRole();

        if ( $currentAdmin && $currentRole &&
             in_array(
                 $currentRole->getRoleName(),
                 [SoftwareRenewalConstants::ID_AGREEMENT]
             )
        ) {
            $softwareAttributes = [
                'wkv_dccp_business_name',
                'wkv_dccp_rut',
                'wkv_dccp_state_details',
                'wkv_replegal_name',
                'wkv_replegal_mail',
                'wkv_replegal_phone',
                'wkv_dccp_name',
                'wkv_dccp_email',
                'wkv_dccp_phone',
                'wkv_region_origen',
                'wkv_dccp_commune',
                'wkv_dccp_email_notify'
            ];

            $result->addFieldToFilter("attribute_code", ["in" => $softwareAttributes]);
        }

        return $result;
    }
}
