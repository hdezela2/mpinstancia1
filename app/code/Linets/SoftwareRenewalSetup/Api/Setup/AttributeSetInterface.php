<?php

namespace Linets\SoftwareRenewalSetup\Api\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Api\Data\AttributeSetInterface as AttributeSetModelInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\InputException;

interface AttributeSetInterface
{

    const ATTRIBUTE_SETS = [
        'PROYECTOS DE DESARROLLO SOFTWARE',
        'PROYECTOS DE ELABORACIÓN DE ANTEPROYECTOS',
        'PROYECTOS DE MANTENIMIENTO DE EVOLUTIVO',
        'PROYECTOS DE DISEÑO, ADMINISTRACIÓN Y/O MANTENCIÓN BASES DE DATOS',
        'PROYECTOS DE MANTENCIÓN Y/O SOPORTE INFRAESTRUCTURA',
        'PROYECTOS DE UI/UX Y DISEÑO WEB',
        'PROYECTOS DE CONSULTORÍA INFORMÁTICA',
        'PROYECTOS DE CONSULTORÍA Y/O AUDITORÍA EN CIBERSEGURIDAD',
        'PROYECTOS DE QA Y TESTING',
        'PROYECTOS DE BUSINESS INTELLIGENCE'
    ];

    /**
     * @param ModuleDataSetupInterface $setup
     * @return void
     */
    public function createAttributeSets(ModuleDataSetupInterface $setup);

    /**
     * @param int $parentAttributeSetId
     * @param string $entityTypeId
     * @param string $attributeSetName
     * @return void
     * @throws \Exception
     */
    public function createAttributeSet($parentAttributeSetId, $entityTypeId, $attributeSetName);

    /**
     * @param AttributeSetModelInterface $attributeSet
     * @param string $attributeCode
     * @param int $sortOrder
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function addAttributeToAttibuteSet(
        AttributeSetModelInterface $attributeSet,
        $attributeCode,
        $sortOrder
    );

    /**
     * @param string $attributeSetName
     * @return AttributeSetModelInterface
     * @throws NoSuchEntityException
     */
    public function getAttributeSetByName($attributeSetName);

    /**
     * @param ModuleDataSetupInterface $setup
     * @param string $attributeName
     * @param array $attributeData
     */
    public function createAttribute(ModuleDataSetupInterface $setup, $attributeName, $attributeData);

    /**
     * @param AttributeSetModelInterface $attributeSet
     * @param string $attributeCode
     * @param int $sortOrder
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function addAttributeIfNotExistsToAttibuteSet(
        AttributeSetModelInterface $attributeSet,
        string $attributeCode,
        int $sortOrder
    );
}
