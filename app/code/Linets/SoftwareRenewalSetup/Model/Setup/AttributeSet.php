<?php

namespace Linets\SoftwareRenewalSetup\Model\Setup;

use Linets\SoftwareRenewalSetup\Api\Setup\AttributeSetInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Model\Entity\Attribute\Set;
use Magento\Eav\Api\AttributeManagementInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory as AttributeSetCollectionFactory;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Magento\Eav\Api\Data\AttributeSetInterface as AttributeSetModelInterface;
use Magento\Eav\Model\Entity\Attribute\GroupFactory;
use Magento\Eav\Model\Entity\AttributeFactory;

class AttributeSet implements AttributeSetInterface
{

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @var CategorySetupFactory
     */
    private $categorySetupFactory;

    /**
     * @var AttributeManagementInterface
     */
    private $attributeManagement;

    /**
     * @var AttributeSetCollectionFactory
     */
    private $attributeSetCollectionFactory;

    /**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;

    /**
     * @var GroupFactory
     */
    private $attrGroupFactory;

    /**
     * @var AttributeFactory
     */
    private $attributeFactory;

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     * @param CategorySetupFactory $categorySetupFactory
     * @param AttributeManagementInterface $attributeManagement
     * @param AttributeSetCollectionFactory $attributeSetCollectionFactory
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     * @param GroupFactory $attrGroupFactory
     * @param AttributeFactory $attributeFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        AttributeSetFactory $attributeSetFactory,
        CategorySetupFactory $categorySetupFactory,
        AttributeManagementInterface $attributeManagement,
        AttributeSetCollectionFactory $attributeSetCollectionFactory,
        AttributeSetRepositoryInterface $attributeSetRepository,
        GroupFactory $attrGroupFactory,
        AttributeFactory $attributeFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeManagement = $attributeManagement;
        $this->attributeSetCollectionFactory = $attributeSetCollectionFactory;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->attrGroupFactory = $attrGroupFactory;
        $this->attributeFactory = $attributeFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function createAttributeSets(ModuleDataSetupInterface $setup)
    {
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
        foreach (self::ATTRIBUTE_SETS as $attributeSetName) {
            $this->createAttributeSet($attributeSetId, $entityTypeId, $attributeSetName);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createAttributeSet($parentAttributeSetId, $entityTypeId, $attributeSetName)
    {
        $getAttributeSet = $this->getAttributeSetByName($attributeSetName);
        if (!$getAttributeSet) {
            $attributeSet = $this->attributeSetFactory->create();
            $data = [
                Set::KEY_ATTRIBUTE_SET_NAME => $attributeSetName,
                Set::KEY_ENTITY_TYPE_ID => $entityTypeId
            ];

            $attributeSet->setData($data);
            $attributeSet->validate();
            $attributeSet->save();
            $attributeSet->initFromSkeleton($parentAttributeSetId);
            $attributeSet->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addAttributeToAttibuteSet(
        AttributeSetModelInterface $attributeSet,
                                   $attributeCode,
                                   $sortOrder
    ) {
        $attributeGroupId = $attributeSet->getDefaultGroupId();
        $this->attributeManagement->assign(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeSet->getId(),
            $attributeGroupId,
            $attributeCode,
            $sortOrder
        );
    }

    /**
     * @param string $attributeSetName
     * @return int
     */
    private function getAttributeSetId($attributeSetName)
    {
        $attributeSetId = false;
        $attributeSet = $this->attributeSetCollectionFactory->create()
            ->addFieldToFilter(
                Set::KEY_ATTRIBUTE_SET_NAME,
                $attributeSetName
            )
            ->getFirstItem();
        if ($attributeSet) {
            $attributeSetId = $attributeSet->getAttributeSetId();
        }

        return $attributeSetId;
    }

    /**
     * @param $attributeSetName
     * @return false|AttributeSetModelInterface
     */
    public function getAttributeSetByName($attributeSetName)
    {
        try {
            $attributeSetId = $this->getAttributeSetId($attributeSetName);
            return $this->attributeSetRepository->get($attributeSetId);
        } catch (NoSuchEntityException $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createAttribute(ModuleDataSetupInterface $setup, $attributeName, $attributeData)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, $attributeName, $attributeData);
    }

    /**
     * {@inheritdoc}
     */
    public function addAttributeIfNotExistsToAttibuteSet(
        AttributeSetModelInterface $attributeSet,
        string                     $attributeCode,
        int $sortOrder
    ) {
        if (!$this->attributeExistsOnAttributeSet($attributeSet, $attributeCode)) {
            $this->addAttributeToAttibuteSet($attributeSet, $attributeCode, $sortOrder);
        }
    }

    /**
     * @param AttributeSetModelInterface $attributeSet
     * @param string $attributeCode
     */
    private function attributeExistsOnAttributeSet(AttributeSetModelInterface $attributeSet, $attributeCode)
    {
        $exists = false;
        $groups = $this->attrGroupFactory->create()->getResourceCollection()->setAttributeSetFilter(
            $attributeSet->getAttributeSetId()
        )->load();

        foreach ($groups as $group) {
            $groupAttributesCollection = $this->attributeFactory
                ->create()
                ->getResourceCollection()
                ->setAttributeGroupFilter(
                    $group->getId()
                )->load();
            foreach ($groupAttributesCollection as $attribute) {
                if ($attribute->getAttributeCode() == $attributeCode) {
                    $exists = true;
                    break 2;
                }
            }
        }

        return $exists;
    }
}
