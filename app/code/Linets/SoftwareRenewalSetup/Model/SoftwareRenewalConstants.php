<?php


namespace Linets\SoftwareRenewalSetup\Model;


class SoftwareRenewalConstants
{
    const ID_AGREEMENT = 5800313;
    const WEBSITE_NAME = 'Desarrollo y Mantención de Software';
    const WEBSITE_CODE = 'software2022';
    const STORE_NAME = 'Desarrollo y Mantención de Software';
    const STORE_CODE = 'software2022';
    const STORE_VIEW_NAME = 'Desarrollo y Mantención de Software';
    const STORE_VIEW_CODE = 'software2022';
    const ROOT_CATEGORY_NAME = 'Desarrollo y Mantención de Software - Default Category';
    const ATTRIBUTE_GROUP = 'Desarrollo y Mantención de Software';
    const FRONT_THEME = 'Linets/Software';
    const FRONT_THEME_DEFAULT = 'Formax/ChileCompra';

    const CUSTOM_ERROR_CODE = 555;

    const CONFIG_MIN_QUESTION_DAYS = 'requestforquote/requestforquote_settings/requestforquote_questions_days';
    const CONFIG_MIN_EVALUATION_DAYS = 'requestforquote/requestforquote_settings/requestforquote_evaluation_end_days';
    const CONFIG_MIN_PUBLICATION_DAYS = 'requestforquote/requestforquote_settings/requestforquote_publication_end_days';
    const INITIAL_RANGE_QTY_UTM = 'requestforquote/requestforquote_settings/requestforquote_utm_ini';
    const END_RANGE_QTY_UTM = 'requestforquote/requestforquote_settings/requestforquote_utm_end';

    /**
     * Reviews config
     */

    const XML_PATH_REVIEW_ACTIVE = 'catalog/review/active';
    const XML_PATH_REVIEW_ALLOW_GUEST = 'catalog/review/allow_guest';


    /**
     * Categoría temporal
     */
    const XML_PATH_NUMBER_OF_DAYS = 'stock_management_configuration/general/number_of_days';
    const XML_PATH_STOCK_MANAGEMENT_ENABLED = 'stock_management_configuration/general/enable';

    /**
     * New Customer SW Attributes
     */
    public $vendorAttributes = [
        'wkv_sw_desarrollo_meses_garantia_tramo1' => 'Garantía Categoría Servicios de Desarrollo y Mantención de Software Tramo 1 (Meses)',
        'wkv_sw_desarrollo_meses_garantia_tramo2' => 'Garantía Categoría Servicios de Desarrollo y Mantención de Software Tramo 2 (Meses)',
        'wkv_sw_desarrollo_meses_garantia_tramo3' => 'Garantía Categoría Servicios de Desarrollo y Mantención de Software Tramo 3 (Meses)',
    ];

    /**
     * @return array
     */
    public function getVendorAttributes(): array
    {
        return $this->vendorAttributes;
    }

    /**
     * @return array
     */
    public function getVendorAttributeCodes(): array
    {
        return array_keys($this->vendorAttributes);
    }

}
