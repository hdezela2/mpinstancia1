<?php

namespace Linets\RegulatedProduct\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;
use Formax\AssignProduct\Helper\MpAssignProduct as Helper;

class FileRegulated extends Column
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array              $components
     * @param Helper             $helper
     * @param array              $data
     */
    public function __construct(
        ContextInterface   $context,
        UiComponentFactory $uiComponentFactory,
        array              $components = array(),
        Helper             $helper,
        array              $data = array()
    )
    {
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );

        $this->helper = $helper;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                //echo '<pre>';
                //die(print_r($dataSource['data']));
                $name = $this->getData('name');
                if (isset($item['regulated_product_file_path']) && $item['regulated_product_file_path'] != '') {
                    $item[$name] = '<a href="'.$this->helper->getMediaBaseUrl($item['regulated_product_file_path']).'" target="_blank">'.__('View Document').'</a>';
                }
            }
        }
        return $dataSource;
    }
}