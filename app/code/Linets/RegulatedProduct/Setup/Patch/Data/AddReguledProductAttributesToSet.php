<?php

namespace Linets\RegulatedProduct\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Linets\InsumosSetup\Api\Setup\AttributeSetInterface;

class AddReguledProductAttributesToSet implements DataPatchInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var AttributeSetInterface
     */
    private $attributeSetSetup;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param AttributeSetInterface    $attributeSetSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        AttributeSetInterface    $attributeSetSetup
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->attributeSetSetup = $attributeSetSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $productoReguladoData = [
            'type' => 'int',
            'label' => 'Producto Regulado',
            'input' => 'boolean',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
            'frontend' => '',
            'required' => true,
            'backend' => '',
            'sort_order' => '30',
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'default' => '0',
            'visible' => true,
            'user_defined' => true,
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'unique' => false,
            'apply_to' => '',
            'group' => 'General',
            'used_in_product_listing' => false,
            'is_used_in_grid' => true,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false
        ];
        $this->attributeSetSetup->createAttribute($this->moduleDataSetup, AttributeSetInterface::PRODUCTO_REGULADO_ATTRIBUTE_CODE, $productoReguladoData);
        foreach (AttributeSetInterface::ATTRIBUTE_SETS as $attributeSetName) {
            $attributeSet = $this->attributeSetSetup->getAttributeSetByName($attributeSetName);
            $sortOrder = 500;
            $this->attributeSetSetup->addAttributeToAttibuteSet($attributeSet, AttributeSetInterface::PRODUCTO_REGULADO_ATTRIBUTE_CODE, $sortOrder);
        }
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            \Linets\InsumosSetup\Setup\Patch\Data\AttributeSet::class,
            \Linets\InsumosSetup\Setup\Patch\Data\AddAttributesToSet::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
