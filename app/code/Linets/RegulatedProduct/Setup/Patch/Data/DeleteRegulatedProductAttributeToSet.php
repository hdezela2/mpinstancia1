<?php
declare(strict_types = 1);

namespace Linets\RegulatedProduct\Setup\Patch\Data;

use Linets\InsumosSetup\Setup\Patch\Data\AddAttributesToSet;
use Linets\InsumosSetup\Setup\Patch\Data\AttributeSet;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Linets\InsumosSetup\Api\Setup\AttributeSetInterface;

class DeleteRegulatedProductAttributeToSet implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /** @var EavSetupFactory */
    private $eavSetupFactory;

    /** @var Config  */
    private $eavConfig;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory          $eavSetupFactory
     * @param Config                   $eavConfig
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory          $eavSetupFactory,
        Config $eavConfig
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;

    }

    /**
     * @return AddReguledProductAttributeToSet|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        if($this->isProductAttributeExists(AttributeSetInterface::PRODUCTO_REGULADO_ATTRIBUTE_CODE)) {
            $eavSetup->removeAttribute(Product::ENTITY, AttributeSetInterface::PRODUCTO_REGULADO_ATTRIBUTE_CODE);
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * Returns true if attribute exists and false if it doesn't exist
     *
     * @param string $field
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isProductAttributeExists($field): bool
    {
        $attr = $this->eavConfig->getAttribute(Product::ENTITY, $field);

        return $attr && $attr->getId();
    }

    /**
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [
            AttributeSet::class,
            AddAttributesToSet::class,
            AddReguledProductAttributesToSet::class
        ];
    }

    /**
     * @return array|string[]
     */
    public function getAliases(): array
    {
        return [];
    }
}
