<?php
declare(strict_types = 1);

namespace Linets\RegulatedProduct\Setup\Patch\Data;

use Linets\InsumosSetup\Setup\Patch\Data\AddAttributesToSet;
use Linets\InsumosSetup\Setup\Patch\Data\AttributeSet;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Linets\InsumosSetup\Api\Setup\AttributeSetInterface;

class AddRegulatedProductAttributeToSet implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /** @var EavSetupFactory */
    private $eavSetupFactory;

    /** @var AttributeSetInterface */
    private $attributeSetSetup;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory          $eavSetupFactory
     * @param AttributeSetInterface    $attributeSetSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory          $eavSetupFactory,
        AttributeSetInterface    $attributeSetSetup
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetSetup = $attributeSetSetup;

    }

    /**
     * @return AddReguledProductAttributeToSet|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttribute(
            Product::ENTITY,
            AttributeSetInterface::PRODUCTO_REGULADO_ATTRIBUTE_CODE,
            [
                'type' => 'int',
                'label' => 'Producto Regulado',
                'input' => 'boolean',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'frontend' => '',
                'required' => false,
                'backend' => '',
                'sort_order' => '30',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'default' => '0',
                'visible' => true,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'unique' => false,
                'apply_to' => '',
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false
            ]
        );

        $entity_type_id = $eavSetup->getEntityTypeId(Product::ENTITY);
        $group_name = 'Product Details';
        foreach (AttributeSetInterface::ATTRIBUTE_SETS as $attribute_set_name) {
            $attribute_set_id = $eavSetup->getAttributeSetId($entity_type_id, $attribute_set_name);
            $attribute_group_id = $eavSetup->getAttributeGroupId($entity_type_id, $attribute_set_id, $group_name);

            $attribute_id = $eavSetup->getAttributeId($entity_type_id, AttributeSetInterface::PRODUCTO_REGULADO_ATTRIBUTE_CODE);
            $eavSetup->addAttributeToSet($entity_type_id, $attribute_set_id, $attribute_group_id, $attribute_id);
            $eavSetup->addAttributeToGroup(
                $entity_type_id,
                $attribute_set_id,
                $attribute_group_id,
                $attribute_id,
                20
            );
        };

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @return string[]
     */
    public static function getDependencies()
    {
        return [
            AttributeSet::class,
            AddAttributesToSet::class,
            AddReguledProductAttributesToSet::class,
            DeleteRegulatedProductAttributeToSet::class
        ];
    }

    /**
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }
}
