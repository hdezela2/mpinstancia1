<?php
declare(strict_types = 1);

namespace Linets\RegulatedProduct\ViewModel;

use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;
use Formax\AssignProduct\Model\ResourceModel\AssignProduct\CollectionFactory as AssignProductFileCollection;

class RegulatedData implements ArgumentInterface
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var AssignProductFileCollection
     */
    protected $assignProductFileCollection;

    /**
     * @param StoreManagerInterface       $storeManager
     * @param AssignProductFileCollection $assignProductFileCollection
     */
    public function __construct(
        StoreManagerInterface       $storeManager,
        AssignProductFileCollection $assignProductFileCollection
    )
    {
        $this->storeManager = $storeManager;
        $this->assignProductFileCollection = $assignProductFileCollection;
    }

    /**
     * Get Assign Products
     * Status 1 Aprobado, Status 0 Desaprobado (only Regulated Product / Insumos)
     *
     * @param     $productId
     * @param     $sellerId
     * @param int|null $status
     * @return collection object
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAssingProductFile($productId, $sellerId, int $status = null)
    {
        $file = '';
        $fileCollection = '';
        if ($productId > 0 && $sellerId > 0) {

            $websiteId = $this->storeManager->getStore()->getWebsiteId();
            $collection = $this->assignProductFileCollection->create();
            $collection->addFieldToFilter('product_id', $productId);
            $collection->addFieldToFilter('seller_id', $sellerId);
            if ($websiteId != 1) {
                $collection->addFieldToFilter('website_id', (int)$websiteId);
            }
            if (isset($status)) {
                $collection->addFieldToFilter('status', $status);
            }
            foreach ($collection as $item) {
                if (!empty($item->getAuthorizedLetter())){
                    $file = $item->getAuthorizedLetter();
                }elseif (!empty($item->getRegulatedProductFilePath())) {
                    $file = $item->getRegulatedProductFilePath();
                }
                $fileCollection = $item;
            }
        }
        return $file ? $fileCollection : '';
    }

    /**
     * Get file url media
     *
     * @param string $file
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaBaseUrl($file): string
    {
        $urlFile = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $urlFile . 'chilecompra/files/assignproducts'. $file;
    }

}
