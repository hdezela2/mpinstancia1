<?php
declare(strict_types = 1);

namespace Linets\WebkulMpSellerBuyerCommunication\Plugin;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\MpSellerBuyerCommunication\Block\Customer\History as HistoryMpSellerBuyerCommunication;

class History
{
    /** @var StoreManagerInterface */
    protected StoreManagerInterface $storeManager;

    /** @var ResourceConnection */
    private ResourceConnection $_resource;

    /**
     * @param StoreManagerInterface $storeManager
     * @param ResourceConnection    $resource
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ResourceConnection    $resource
    )
    {
        $this->storeManager = $storeManager;
        $this->_resource = $resource;
    }

    /**
     * @param HistoryMpSellerBuyerCommunication $subject
     * @param                                   $result
     * @return bool|\Webkul\MpSellerBuyerCommunication\Model\ResourceModel\SellerBuyerCommunication\Collection
     * @throws NoSuchEntityException
     */
    public function afterGetAllCommunicationData(HistoryMpSellerBuyerCommunication $subject, $result)
    {
        $collection = $result;

        $collection->join(
            ['ce' => 'customer_entity'],
            'main_table.seller_id = ce.entity_id AND ce.store_id = '.$this->getStoreId(),
            []);

        return $collection;
    }

    /**
     * Get Store Id
     *
     * @throws NoSuchEntityException
     */
    public function getStoreId(): int
    {
        return (int)$this->storeManager->getStore()->getId();
    }
}
