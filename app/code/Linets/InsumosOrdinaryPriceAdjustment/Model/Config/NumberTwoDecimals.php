<?php
declare(strict_types = 1);

namespace Linets\InsumosOrdinaryPriceAdjustment\Model\Config;

use Magento\Framework\App\Config\Value;
use Magento\Framework\Exception\LocalizedException;

class NumberTwoDecimals extends Value
{
    /**
     * @return $this|NumberTwoDecimals
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function beforeSave()
    {
        $value = $this->getValue();
        if (preg_match("/^[-]?[0-9]{1,3}([.][0-9]{1,2})?$/", $value)) {
            $validator =  true;
        } else {
            $validator = false;
        };

        if (!$validator) {
            $message = __('Invalid number format, e.g.: 3.55 | -3.05 | 5');
            throw new LocalizedException($message);
        }

        return $this;
    }
}