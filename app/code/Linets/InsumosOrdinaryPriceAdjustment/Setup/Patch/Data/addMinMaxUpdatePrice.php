<?php

namespace Linets\InsumosOrdinaryPriceAdjustment\Setup\Patch\Data;

use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class addMinMaxUpdatePrice implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var ConfigInterface
     */
    private $configInterface;
    /**
     * @var WebsiteRepositoryInterface
     */
    private $websiteRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        ModuleDataSetupInterface   $moduleDataSetup,
        ConfigInterface            $configInterface,
        WebsiteRepositoryInterface $websiteRepository,
        LoggerInterface            $logger
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->configInterface = $configInterface;
        $this->websiteRepository = $websiteRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        try {
            $website = $this->websiteRepository->get(InsumosConstants::WEBSITE_CODE);
            $this->configInterface->saveConfig(
                'catalog/ordinary_price_adjustment/validate_adjustment_percentage',
                1,
                ScopeInterface::SCOPE_WEBSITES,
                $website->getId()
            );

            $this->configInterface->saveConfig(
                'catalog/ordinary_price_adjustment/max_adjustment_percentage',
                4,
                ScopeInterface::SCOPE_WEBSITES,
                $website->getId()
            );

            $this->configInterface->saveConfig(
                'catalog/ordinary_price_adjustment/min_adjustment_percentage',
                '-100',
                ScopeInterface::SCOPE_WEBSITES,
                $website->getId()
            );

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
        $this->moduleDataSetup->endSetup();
    }
}