<?php
/**
 * Copyright © Linets | Frank Cedeno All rights reserved.
 * See COPYING.txt for license details.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Linets_InsumosOrdinaryPriceAdjustment',
    __DIR__
);

