# Mage2 Module Linets InsumosOrdinaryPriceAdjustment

    ``linets/module-insumosordinarypriceadjustment``

- [Main Functionalities](#markdown-header-main-functionalities)
- [Installation](#markdown-header-installation)
- [Configuration](#markdown-header-configuration)
- [Specifications](#markdown-header-specifications)
- [Attributes](#markdown-header-attributes)

## Main Functionalities

Price Modification

## Installation

\* = in production please use the `--keep-generated` option

### Type 1: Zip file

- Unzip the zip file in `app/code/Linets`
- Enable the module by running `php bin/magento module:enable Linets_InsumosOrdinaryPriceAdjustment`
- Apply database updates by running `php bin/magento setup:upgrade`\*
- Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

- Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
- Add the composer repository to the configuration by
  running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
- Install the module composer by running `composer require linets/module-insumosordinarypriceadjustment`
- enable the module by running `php bin/magento module:enable Linets_InsumosOrdinaryPriceAdjustment`
- apply database updates by running `php bin/magento setup:upgrade`\*
- Flush the cache by running `php bin/magento cache:flush`

## Configuration

## Specifications

- Plugin
    - beforeExecute - Webkul\CategoryProductPrice\Controller\Adminhtml\Index\Run >
      Linets\InsumosOrdinaryPriceAdjustment\Plugin\Webkul\CategoryProductPrice\Controller\Adminhtml\Index\Run

## Attributes



