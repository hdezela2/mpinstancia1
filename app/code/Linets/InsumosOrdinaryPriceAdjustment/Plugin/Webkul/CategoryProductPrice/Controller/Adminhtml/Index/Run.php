<?php
/**
 * Copyright © Linets | Frank Cedeno All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types = 1);

namespace Linets\InsumosOrdinaryPriceAdjustment\Plugin\Webkul\CategoryProductPrice\Controller\Adminhtml\Index;

use Formax\AgreementInfo\Helper\Data as AgreementHelper;
use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\ScopeInterface;

class Run
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var ManagerInterface
     */
    protected $messageManager;
    /**
     * @var ResultFactory
     */
    protected $resultFactory;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var AgreementHelper
     */
    private $_agreementHelper;

    /**
     * @param PageFactory          $resultPageFactory
     * @param ManagerInterface     $messageManager
     * @param ResultFactory        $resultFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param AgreementHelper      $agreementHelper
     */
    public function __construct(
        PageFactory          $resultPageFactory,
        ManagerInterface     $messageManager,
        ResultFactory        $resultFactory,
        ScopeConfigInterface $scopeConfig,
        AgreementHelper      $agreementHelper

    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->messageManager = $messageManager;
        $this->resultFactory = $resultFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_agreementHelper = $agreementHelper;
    }

    /**
     * @param \Webkul\CategoryProductPrice\Controller\Adminhtml\Index\Run $subject
     * @param                                                             $result
     * @return Redirect|mixed|void
     */
    public function afterExecute(\Webkul\CategoryProductPrice\Controller\Adminhtml\Index\Run $subject, $result)
    {
        $percentage = (float)$subject->getRequest()->getParam('update_percentage');

        $agreementId = $this->_agreementHelper->getAgreementId();
        $websiteCode = 0;
        if ($agreementId == InsumosConstants::ID_AGREEMENT) {
            $websiteCode = InsumosConstants::WEBSITE_CODE;
        }

        $validatePriceAdjustment = $this->scopeConfig->getValue(
            'catalog/ordinary_price_adjustment/validate_adjustment_percentage',
            ScopeInterface::SCOPE_WEBSITES,
            $websiteCode
        );
        $percentageMax = (float)$this->scopeConfig->getValue(
            'catalog/ordinary_price_adjustment/max_adjustment_percentage',
            ScopeInterface::SCOPE_WEBSITES,
            $websiteCode
        );
        $percentageMin = (float)$this->scopeConfig->getValue(
            'catalog/ordinary_price_adjustment/min_adjustment_percentage',
            ScopeInterface::SCOPE_WEBSITES,
            $websiteCode
        );
        $hasError = false;
        if ($validatePriceAdjustment) {
            if ($percentage < $percentageMin || $percentage > $percentageMax) {
                $hasError = true;
                $this->messageManager->addWarningMessage(__('The percentage must be greater than or equal to %1 and less than or equal to %2', $percentageMin, $percentageMax));
            }
            if (!$this->validateTwoDecimals((string)$percentage)) {
                $hasError = true;
                $this->messageManager->addWarningMessage(__('Invalid number format, e.g.: 3.55 | -3.05 | 5'));
            }
        }

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $hasError ? $resultRedirect->setPath('*/*/modify') : $result;
    }

    /**
     * @param $value
     * @return bool
     */
    public function validateTwoDecimals($value): bool
    {
        return (bool)preg_match("/^[-]?[0-9]{1,2}([.][0-9]{1,2})?$/", $value);
    }
}

