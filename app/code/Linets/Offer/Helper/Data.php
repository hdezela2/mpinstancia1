<?php
declare(strict_types=1);

namespace Linets\Offer\Helper;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Formax\Offer\Helper\Data as OfferDataHelper;
use Magento\Store\Model\StoreManagerInterface;
use Linets\GasSetup\Constants as GasConstants;
use Formax\Offer\Model\ResourceModel\Offer\CollectionFactory as OfferCollectionFactory;
use Formax\Offer\Model\Offer as OfferModel;

class Data extends AbstractHelper
{

    const XML_PATH_ENABLED_DAYS = 'catalog/dccp_offer/enabled_days';

    const XML_PATH_MINIMUM_PRICE_DAYS = 'catalog/dccp_offer/minimum_price_push_days';

    const XML_PATH_DEBUG_OFFERS = 'catalog/dccp_offer/debug_offers';

    const CATEGORY_ENVASADO_NAME = 'GAS LICUADO DE PETRÓLEO EN MODALIDAD ENVASADO';

    const CATEGORY_GRANEL_NAME = 'GAS LICUADO DE PETRÓLEO EN MODALIDAD GRANEL';

    /**
     * @var CollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * @var OfferDataHelper
     */
    private $offerDataHelper;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var OfferCollectionFactory
     */
    private $offerCollectionFactory;

    /**
     * @param Context $context
     * @param CollectionFactory $categoryCollectionFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param TimezoneInterface $timezone
     * @param OfferDataHelper $offerDataHelper
     * @param StoreManagerInterface $storeManager
     * @param OfferCollectionFactory $offerCollectionFactory
     */
    public function __construct(
        Context $context,
        CollectionFactory $categoryCollectionFactory,
        CustomerRepositoryInterface $customerRepository,
        TimezoneInterface $timezone,
        OfferDataHelper $offerDataHelper,
        StoreManagerInterface $storeManager,
        OfferCollectionFactory $offerCollectionFactory
    ) {
        parent::__construct($context);
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->customerRepository = $customerRepository;
        $this->timezone = $timezone;
        $this->offerDataHelper = $offerDataHelper;
        $this->storeManager = $storeManager;
        $this->offerCollectionFactory = $offerCollectionFactory;
    }

    /**
     * @return array
     */
    public function getEnabledDays()
    {
        $enabledDays = $this->scopeConfig->getValue(
            self::XML_PATH_ENABLED_DAYS,
            ScopeInterface::SCOPE_WEBSITES
        );
        $enabledDaysArray = $enabledDays ? explode(",", $enabledDays) : [];

        return $enabledDaysArray;
    }

    /**
     * @return array
     */
    public function getMinimumPriceDays()
    {
        $minimumPriceDays = $this->scopeConfig->getValue(
            self::XML_PATH_MINIMUM_PRICE_DAYS,
            ScopeInterface::SCOPE_WEBSITES
        );
        $minimumPriceDaysArray = $minimumPriceDays ? explode(",", $minimumPriceDays) : [];

        return $minimumPriceDaysArray;
    }

    /**
     * @return boolean
     */
    public function getDebugOffers()
    {
        $debugEnabled = (bool) $this->scopeConfig->getValue(
            self::XML_PATH_DEBUG_OFFERS,
            ScopeInterface::SCOPE_WEBSITES
        );

        return $debugEnabled;
    }

    /**
     *
     * @param ProductInterface $product
     * @return boolean
     */
    public function allowAddToCartForMinPrice($product)
    {
        $envasadoCategoryId = $this->getEnvasadoCategoryId();
        $categories = $product->getCategoryIds();
        $day = $this->timezone->date()->format('w');
        $allowed = true;
        if (in_array($envasadoCategoryId, $categories) && $this->isDateWithinRange($day)) {
            $allowed = false;
        }

        return $allowed;
    }

    /**
     * @return int
     */
    public function getEnvasadoCategoryId()
    {
        return $this->getCategoryIdByName(self::CATEGORY_ENVASADO_NAME);
    }

    /**
     * @param $sellerId
     * @return mixed|string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getSellerStatus($sellerId)
    {
        $customer = $this->customerRepository->getById($sellerId);

        return $customer->getCustomAttribute('wkv_dccp_state_details') ?
                $customer->getCustomAttribute('wkv_dccp_state_details')->getValue() : '';
    }

    /**
     * @return int
     */
    private function getCategoryIdByName($categoryName)
    {
        $categoryCollection = $this->categoryCollectionFactory->create();
        $category = $categoryCollection->addAttributeToSelect('*')
            ->setPageSize(1)
            ->addAttributeToFilter('name', ['like' => '%' . $categoryName . '%'])
            ->getFirstItem();

        return $category->getId();
    }

    /**
     * @return int
     */
    public function getGranelCategoryId()
    {
        return $this->getCategoryIdByName(self::CATEGORY_GRANEL_NAME);
    }

    /**
     * @param int $day
     * @return boolean
     */
    private function isDateWithinRange($day)
    {
        $inRange = false;
        $enabledDays = $this->getMinimumPriceDays();
        if (in_array($day, $enabledDays)) {
            $inRange = true;
        }

        return $inRange;
    }

    /**
     * Retrieve product with special price grouped by seller
     *
     * @param string $productType
     * @param mixed int $productId
     * @param array $visibleProducts
     * @param mixed false/int $sellerId
     * @return array
     */
    public function getProductWithSpecialPriceBySeller($productType, $productId, $visibleProducts, $sellerId = false)
    {
        $offerPrices = $this->offerDataHelper->getProductWithSpecialPriceBySeller($productType, $productId, $sellerId);
        $websiteCode = $this->storeManager->getStore()->getCode();
        if (GasConstants::GAS_WEBSITE_CODE == $websiteCode) {
            $offerPrices = $this->determineLowerPrice($offerPrices, $visibleProducts);
        }

        return $offerPrices;
    }

    /**
     * @param array $offerPrices
     * @param array $visibleProducts
     * @return int
     */
    private function determineLowerPrice($offerPrices, $visibleProducts)
    {
        $sortedProducts = $this->sortByProduct($offerPrices, $visibleProducts);
        $lowestPriceIds = [];
        $lowestPriceId = null;
        $lowestPrice = [];
        foreach ($sortedProducts as $configurableId => $variations) {
            foreach ($variations as $simpleId => $prices) {
                foreach ($prices as $sellerId => $price) {
                    $basePrice = $price['base_price'];
                    $specialPrice = $price['special_price'];
                    $minPrice['price'] = (float) $basePrice;
                    $minPrice['is_special'] = false;
                    if ($specialPrice && ((float) $basePrice > (float) $specialPrice)) {
                        $minPrice['price'] = (float) $specialPrice;
                        $minPrice['is_special'] = true;
                    }
                    if ($this->shouldBeAddedToLowestPricesIds($lowestPriceIds, $minPrice, $lowestPrice, $simpleId, $sellerId, $lowestPriceId)) {
                        $lowestPriceIds[] = $sellerId;
                        $lowestPriceId = $sellerId;
                        $lowestPrice['price'] = $minPrice['price'];
                        $lowestPrice['is_special'] = $minPrice['is_special'];
                    }
                    $offerPrices[$sellerId][$configurableId][$simpleId]['lowest_price'] = 0;
                    $sortedProducts[$configurableId][$simpleId][$sellerId]['lowest_price'] = 0;
                }
                foreach ($lowestPriceIds as $value) {
                    $basePrice = $sortedProducts[$configurableId][$simpleId][$value]['base_price'];
                    $specialPrice = $sortedProducts[$configurableId][$simpleId][$value]['special_price'];
                    $minProvPrice = (float) $basePrice;
                    $isProvSpecial = false;
                    if ($specialPrice && ((float) $basePrice > (float) $specialPrice)) {
                        $minProvPrice = (float) $specialPrice;
                        $isProvSpecial = true;
                    }
                    if ($isProvSpecial && ($lowestPrice['is_special'] === false)) {
                        continue;
                    }
                    if ($minProvPrice == $lowestPrice['price']) {
                        if ($lowestPrice['is_special'] === true) {
                            if ($lowestPriceId == $value) {
                                $offerPrices[$value][$configurableId][$simpleId]['lowest_price'] = 1;
                                $sortedProducts[$configurableId][$simpleId][$value]['lowest_price'] = 1;
                            }
                        } else {
                            $offerPrices[$value][$configurableId][$simpleId]['lowest_price'] = 1;
                            $sortedProducts[$configurableId][$simpleId][$value]['lowest_price'] = 1;
                        }
                    }
                }
                $lowestPriceIds = [];
                $lowestPriceId = null;
                $lowestPrice = [];
            }
        }

        return $offerPrices;
    }

    /**
     * @param array $lowestPriceIds
     * @param array $minPrice
     * @param array $lowestPrice
     * @param int $simpleId
     * @param int $sellerId
     * @param int $lowestPriceId
     * @return boolean
     */
    private function shouldBeAddedToLowestPricesIds($lowestPriceIds, $minPrice, $lowestPrice, $simpleId, $sellerId, $lowestPriceId)
    {
        $shouldBeAdded = false;
        if (empty($lowestPriceIds)) {
            $shouldBeAdded = true;
        } else {
            if ($minPrice['price'] < $lowestPrice['price']) {
                $shouldBeAdded = true;
            } elseif ($minPrice['price'] == $lowestPrice['price']) {
                if ($minPrice['is_special'] === false) {
                    $shouldBeAdded = true;
                } elseif ($lowestPrice['is_special'] === true && $this->isOldestOffer($simpleId, $sellerId, $lowestPriceId)) {
                    $shouldBeAdded = true;
                }
            }
        }

        return $shouldBeAdded;
    }

    /**
     * @param int $simpleId
     * @param int $sellerId
     * @param int $lowestPriceId
     * @return boolean
     */
    private function isOldestOffer($simpleId, $sellerId, $lowestPriceId)
    {
        $isOldest = false;
        $sellerOffer = $this->getOffer($simpleId, $sellerId);
        $lowestOffer = $this->getOffer($simpleId, $lowestPriceId);
        $sellerUpdatedAt = strtotime($sellerOffer->getUpdatedAt());
        $lowestUpdatedAt = strtotime($lowestOffer->getUpdatedAt());
        $sellerBasePrice = (float) $sellerOffer->getBasePrice();
        $lowestBasePrice = (float) $lowestOffer->getBasePrice();
        if ($sellerBasePrice < $lowestBasePrice) {
            $isOldest = true;
        } elseif ($sellerBasePrice == $lowestBasePrice) {
            $isOldest = ($sellerUpdatedAt < $lowestUpdatedAt);
        }

        return $isOldest;
    }

    /**
     * @param int $simpleId
     * @param int $sellerId
     * @return OfferModel
     */
    private function getOffer($simpleId, $sellerId)
    {
        $collection = $this->offerCollectionFactory->create();
        $collection->addFieldToFilter('product_id', $simpleId)
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('status', OfferModel::OFFER_APPLIED);
        $offer = $collection->getFirstItem();

        return $offer;
    }

    /**
     * @param array $visibleProducts
     * @return array
     */
    private function getVisibleProductsIds($visibleProducts)
    {
        $visibleIds = [];
        if (isset($visibleProducts['data'])) {
            foreach ($visibleProducts['data'] as $visibleProduct) {
                if (isset($visibleProduct['jsonResult'])) {
                    foreach ($visibleProduct['jsonResult'] as $id => $options) {
                        $visibleIds[$id][] = $visibleProduct['sellerId'];
                    }
                }
            }
        }

        return $visibleIds;
    }

    /**
     * @param array $offerPrices
     * @param array $visibleProducts
     * @return array
     */
    private function sortByProduct($offerPrices, $visibleProducts)
    {
        $sortedByProducts = [];
        $visibleIds = $this->getVisibleProductsIds($visibleProducts);
        foreach ($offerPrices as $sellerId => $variations) {
            foreach ($variations as $configurableId => $prices) {
                foreach ($prices as $simpleId => $price) {
                    if (array_key_exists($simpleId, $visibleIds)){
                        if (in_array($sellerId, $visibleIds[$simpleId]) &&
                            ($this->getSellerStatus($sellerId) == '70')) {
                            $sortedByProducts[$configurableId][$simpleId][$sellerId] = $price;
                        }
                    }
                }
            }
        }

        return $sortedByProducts;
    }
}
