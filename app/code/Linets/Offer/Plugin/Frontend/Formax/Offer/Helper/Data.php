<?php
declare(strict_types=1);

namespace Linets\Offer\Plugin\Frontend\Formax\Offer\Helper;

use Magento\Store\Model\StoreManagerInterface;
use Linets\GasSetup\Constants as GasConstants;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Linets\Offer\Helper\Data as DataHelper;

class Data
{

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * @var DataHelper
     */
    private $dataHelper;

    /**
     * @param StoreManagerInterface $storeManager
     * @param TimezoneInterface $timezone
     * @param DataHelper $dataHelper
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        TimezoneInterface $timezone,
        DataHelper $dataHelper
    ) {
        $this->storeManager = $storeManager;
        $this->timezone = $timezone;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param \Formax\Offer\Helper\Data $subject
     * @param float $basePrice
     * @param float $specialPrice
     * @param string $originStartDate
     * @param string $startDate
     * @param int $days
     * @param \Magento\Catalog\Api\ProductInterface $product
     * @param string $location
     * @return array|null
     * @throws LocalizedException
     */
    public function beforeValidateOffer(
        \Formax\Offer\Helper\Data $subject,
        $basePrice,
        $specialPrice,
        $originStartDate,
        $startDate,
        $days,
        $product,
        $location = ''
    ) {
        $day = $this->timezone->date()->format('w');
        $hour = (int) $this->timezone->date()->format('H');
        $websiteCode = $this->storeManager->getStore()->getCode();
        $debugModeEnabled = $this->dataHelper->getDebugOffers();
        if (GasConstants::GAS_WEBSITE_CODE == $websiteCode &&
            !$debugModeEnabled &&
            !$this->isDateWithinRange($day, $hour)
        ) {
            throw new LocalizedException(__('No se pueden realizar ofertas especiales en esta fecha para el Convenio Marco Gas'));
        }

        return null;
    }

    /**
     * @param int $day
     * @param int $hour
     * @return boolean
     */
    private function isDateWithinRange($day, $hour)
    {
        $inRange = false;
        $enabledDays = $this->dataHelper->getEnabledDays();
        if (in_array($day, $enabledDays) && ($hour >= 11 && $hour < 18)) {
            $inRange = true;
        }

        return $inRange;
    }

    /**
     * @param \Formax\Offer\Helper\Data $subject
     * @param array $result
     * @return array
     */
    public function afterGetAssociatesData(
        \Formax\Offer\Helper\Data $subject,
        $result
    ) {
        $date = $this->timezone->date();
        $nextSaturday = $date->modify('next saturday');
        $websiteCode = $this->storeManager->getStore()->getCode();
        if (GasConstants::GAS_WEBSITE_CODE == $websiteCode) {
            foreach ($result as $key => $item) {
                $offerActive = $subject->datesAreCurrent($item['start_date'], $item['end_date']);
                if ($offerActive === 0) {
                    $result[$key]['start_date'] = $nextSaturday->format('Y-m-d');
                    $result[$key]['days'] = 3;
                }
            }
        }

        return $result;
    }
}
