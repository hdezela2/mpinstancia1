define([
    'jquery',
    'uiComponent',
    'Magento_Ui/js/modal/confirm',
], function ($, Component) {
    'use strict';

    return Component.extend({
        defaults: {},

        /**
         * Initializes component.
         *
         * @returns {Object} Chainable.
         */
        initialize: function () {
            var self = this;
            $('.confirmation-modal-content').modal({
                title: $.mage.__('Confirmación envío oferta especial semanal'),
                actions: {
                    confirm: function(){
                        $('#wk_mpassignproduct_form').submit();
                    }
                },
                buttons: [{
                    text: $.mage.__('Enviar oferta especial semanal'),
                    class: 'action-primary action-accept',
                    click: function (event) {
                        this.closeModal(event, true);
                        $('#wk_mpassignproduct_form').submit();
                    }
                }],
                modalClass: 'confirm-offer-modal'
            });
            $('button.wk-product-save-btn').on('click', function(event){
                event.preventDefault();
                self.populateTable();
                $('.confirmation-modal-content').modal('openModal');
            });
        },

        populateTable: function () {
            var rowCount = 0,
                self = this;
            $(".offer-lists table tbody tr").remove(); 
            $('table.wk-associated-table tbody tr').each(function (index) {
                let newRow = $('<tr></tr>');
                let specialPrice = $(this).find('.wk-associate-special-price').val();
                if (specialPrice) {
                    let dateStart = $(this).find('.wk-associate-start-date').val(),
                        dateEnd = $(this).find('.wk-associate-end-date').val(),
                        name = $(this).find('td:nth-child(2)').html();
                    rowCount++;
                    let calculatedDateEnd = new Date(dateStart);
                    calculatedDateEnd.setDate(calculatedDateEnd.getDate() + 5);
                    calculatedDateEnd.setMinutes(59);
                    calculatedDateEnd.setHours(23);
                    dateStart = new Date(dateStart);
                    dateStart.setDate(dateStart.getDate() + 1);
                    dateStart.setMinutes(1);
                    dateStart.setHours(0);
                    let formattedDate = self.formatDate(calculatedDateEnd);
//                    if (typeof dateEnd === 'undefined' || dateEnd === '') {
                        dateEnd = formattedDate;
//                    }
                    let dateStartFormatted = self.formatDate(dateStart);
                    newRow.append('<td>' + name + '</td>');
                    newRow.append('<td>' + specialPrice + '</td>');
                    newRow.append('<td>' + dateStartFormatted + '</td>');
                    newRow.append('<td>' + dateEnd + '</td>');
                    $('.offer-lists table tbody').append(newRow);
                }
            });
        },

        formatDate: function (date) {
            var formattedDate;
            let day = date.getDate();
            let month = date.getMonth() + 1;
            let year = date.getFullYear(),
                minutes = date.getMinutes(),
                hours = date.getHours();
            if (hours < 10) {
                hours = "0"+hours;
            }
            if (minutes < 10) {
                minutes = "0"+minutes;
            }
            if (day < 10) {
                day = "0"+day;
            }
            if(month < 10){
                formattedDate = `${year}-0${month}-${day} ${hours}:${minutes}`;
            }else{
                formattedDate = `${year}-${month}-${day} ${hours}:${minutes}`;
            }

            return formattedDate;
        }
    })
})