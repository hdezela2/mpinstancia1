<?php
declare(strict_types=1);

namespace Linets\WeeklyPriceManagement\Ui\Component\Form;

use Linets\WeeklyPriceManagement\Helper\Data;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider as MagentoDataProvider;
use Linets\WeeklyPriceManagement\Model\ResourceModel\WeeklyPrices\Collection as WeeklyPricesCollection;
use Linets\WeeklyPriceManagement\Model\WeeklyPrices;

/**
 * DataProvider component.
 */
class DataProvider extends MagentoDataProvider
{
	/**
	 * @var WeeklyPricesCollection
	 */
	private $weeklyPricesCollection;

	/**
	 * @var Data
	 */
	private $helperData;
	/**
	 * @var \Formax\HideAdminTabs\Helper\Data
	 */
	private $helperFormax;

    /**
     * @var array
     */
    private $loadedData = [];

	/**
	 * DataProvider constructor.
	 * @param $name
	 * @param $primaryFieldName
	 * @param $requestFieldName
	 * @param ReportingInterface $reporting
	 * @param SearchCriteriaBuilder $searchCriteriaBuilder
	 * @param RequestInterface $request
	 * @param FilterBuilder $filterBuilder
	 * @param WeeklyPricesCollection $weeklyPricesCollection
	 * @param Data $helperData
	 * @param \Formax\HideAdminTabs\Helper\Data $helperFormax
	 * @param array $meta
	 * @param array $data
	 */
	public function __construct(
		$name,
		$primaryFieldName,
		$requestFieldName,
		ReportingInterface $reporting,
		SearchCriteriaBuilder $searchCriteriaBuilder,
		RequestInterface $request,
		FilterBuilder $filterBuilder,
		WeeklyPricesCollection $weeklyPricesCollection,
		Data $helperData,
		\Formax\HideAdminTabs\Helper\Data $helperFormax,
		array $meta = [],
		array $data = []
	) {
		parent::__construct(
			$name,
			$primaryFieldName,
			$requestFieldName,
			$reporting,
			$searchCriteriaBuilder,
			$request,
			$filterBuilder,
			$meta,
			$data
		);
		$this->weeklyPricesCollection = $weeklyPricesCollection;
		$this->helperData = $helperData;
		$this->helperFormax = $helperFormax;
	}

    /**
     * @return array
     */
    public function getData(): array
    {
        //Commented problems after upgrade
//        if (isset($this->loadedData)) {
//                return $this->loadedData;
//        }

        $parameters = $this->weeklyPricesCollection
                ->addFieldToFilter('is_active', 1)
                ->getFirstItem();

        $this->loadedData = [];
        $this->loadedData[$parameters->getId()]['update_parameters'] = $parameters->getData();
        $this->loadedData[$parameters->getId()]['update_parameters'][WeeklyPrices::PARITY_CURRENT_WEEK] = '';
        $this->loadedData[$parameters->getId()]['update_parameters'][WeeklyPrices::USD_CURRENT_WEEK] = '';
        $this->loadedData[$parameters->getId()]['update_parameters'][WeeklyPrices::PARITY_LAST_WEEK] =
            $parameters->getData(WeeklyPrices::PARITY_CURRENT_WEEK);
        $this->loadedData[$parameters->getId()]['update_parameters'][WeeklyPrices::USD_LAST_WEEK] =
            $parameters->getData(WeeklyPrices::USD_CURRENT_WEEK);

        return $this->loadedData;
    }

	/**
	 * @return array
	 */
	public function getMeta(): array
	{
		$meta = parent::getMeta();

		if (!$this->helperData->isFirstWeekOfMonth() && $this->request->getParam('id')) {
			$meta['update_parameters']['children']
				['parity_last_week']['arguments']['data']['config']['disabled'] = 1;

			$meta['update_parameters']['children']
				['usd_last_week']['arguments']['data']['config']['disabled'] = 1;
		}

		if (!$this->helperFormax->isAdmin()) {
			$meta['update_parameters']['children']
			['grannel_const']['arguments']['data']['config']['disabled'] = 1;
		}

		return $meta;
	}
}
