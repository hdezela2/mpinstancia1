<?php
declare(strict_types=1);

namespace Linets\WeeklyPriceManagement\Ui\Component\Listing\Column;

use Linets\WeeklyPriceManagement\Model\WeeklyPrices;
use Linets\WeeklyPriceManagement\Model\ResourceModel\WeeklyPrices\Collection as ParamsCollection;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Params extends Column
{

    /**
     * @var ParamsCollection
     */
    protected $weeklyPricesCollection;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param ParamsCollection $weeklyPricesCollection
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ParamsCollection $weeklyPricesCollection,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->weeklyPricesCollection = $weeklyPricesCollection;
    }

    /**
     * @param int $paramsId
     * @return WeeklyPrices
     */
    protected function getParams($paramsId)
    {
        $params = $this->weeklyPricesCollection
            ->addFieldToFilter('entity_id', $paramsId)
            ->getFirstItem();

        return $params;
    }
}
