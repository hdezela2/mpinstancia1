<?php
declare(strict_types=1);

namespace Linets\WeeklyPriceManagement\Ui\Component\Listing\Column;

class GrannelConst extends Params
{

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                if (isset($item['weeklyprices_settings_id']) && !empty($item['weeklyprices_settings_id'])) {
                    $params = $this->getParams($item['weeklyprices_settings_id']);
                    $item[$name]  = $params->getGrannelConst();
                }
            }
        }
        return $dataSource;
    }
}
