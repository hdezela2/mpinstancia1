<?php
declare(strict_types=1);

namespace Linets\WeeklyPriceManagement\Block\Adminhtml\Form;

use DateTime;
use DateTimeZone;
use Exception;
use Linets\PriceManagement\Constants;
use Linets\WeeklyPriceManagement\Model\ResourceModel\WeeklyPrices\Collection as WeeklyPricesCollection;
use Magento\Catalog\Block\Adminhtml\Product\Edit\Button\Generic;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\UiComponent\Context;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class EditButton extends Generic implements ButtonProviderInterface
{
	/**
	 * @var WeeklyPricesCollection
	 */
	private $weeklyPricesCollection;

	/**
	 * EditButton constructor.
	 * @param Context $context
	 * @param Registry $registry
	 * @param WeeklyPricesCollection $weeklyPricesCollection
	 */
	public function __construct(
		Context $context,
		Registry $registry,
		WeeklyPricesCollection $weeklyPricesCollection
	) {
		parent::__construct($context, $registry);
		$this->weeklyPricesCollection = $weeklyPricesCollection;
	}

	/**
	 * Retrieve Save button settings.
	 *
	 * @return array
	 * @throws Exception
	 */
	public function getButtonData(): array
	{
		return [
			'label' => __('Actualizar Precios'),
			'class' => 'save primary',
			'on_click' => sprintf("location.href = '%s';", $this->getUpdateUrl()),
		];
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	private function getUpdateUrl(): string
	{
		return $this->getUrl('*/*/edit', ['id' => $this->getActiveId()]);
	}

	/**
	 * @throws Exception
	 */
	private function getActiveId()
	{
		$currentDate = new DateTime('now', new DateTimeZone(Constants::DEFAULT_TIMEZONE));
		$firstDay = $currentDate->format('Y-m-01 00:00:00');
		$lastDay = $currentDate->format('Y-m-t 23:59:59');

		$item = $this->weeklyPricesCollection
			->addFieldToFilter('is_active', 1)
			->getFirstItem();

		return $item->getEntityId();
	}
}
