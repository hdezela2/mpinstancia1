<?php
declare(strict_types=1);

namespace Linets\WeeklyPriceManagement\Block\Adminhtml\Form;

use Exception;
use Linets\WeeklyPriceManagement\Helper\Data;
use Magento\Catalog\Block\Adminhtml\Product\Edit\Button\Generic;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\UiComponent\Context;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SaveButton extends Generic implements ButtonProviderInterface
{
	const ONLY_BUSINESS_DAYS = 'El reajuste semanal debe ser realizado los días jueves o al siguiente día hábil.';
	const ONLY_BUSINESS_DAYS_WITH_DATE = 'El reajuste semanal debe ser realizado los días jueves o al siguiente día hábil. Próximo día habil: %s';
	const CATALOG_WEEKLYPRICE_MANAGEMENT_DEBUG = 'catalog/weeklyprice_management/debug';

	/**
	 * @var Data
	 */
	private $helper;
	/**
	 * @var ManagerInterface
	 */
	private $messageManager;
	/**
	 * @var RequestInterface
	 */
	private $request;
	/**
	 * @var ScopeConfigInterface
	 */
	private $scopeConfig;

	/**
	 * SaveButton constructor.
	 * @param Context $context
	 * @param Registry $registry
	 * @param Data $helper
	 * @param ManagerInterface $messageManager
	 * @param RequestInterface $request
	 * @param ScopeConfigInterface $scopeConfig
	 */
	public function __construct(
		Context $context,
		Registry $registry,
		Data $helper,
		ManagerInterface $messageManager,
		RequestInterface $request,
		ScopeConfigInterface $scopeConfig
	) {
		$this->helper = $helper;
		$this->messageManager = $messageManager;
		$this->request = $request;
		$this->scopeConfig = $scopeConfig;

		parent::__construct($context, $registry);
	}


	/**
	 * Retrieve Save button settings.
	 *
	 * @return array
	 * @throws Exception
	 */
	public function getButtonData(): array
	{
		$button =  [
			'label' => __('Guardar y Ejecutar'),
			'class' => 'save primary',
			'data_attribute' => [
				'mage-init' => ['button' => ['event' => 'save']],
				'form-role' => 'save',
			]
		];

		if (!$this->request->getParam('id')) {
			return $button;
		}

		$isBusinessDay = $this->helper->isTodayBusinessDay();
		$isTodayThursday = $this->helper->isTodayThursday();
		$lastThursdayWasHoliday = $this->helper->lastThursdayWasHoliday();

		if ($this->scopeConfig->getValue(self::CATALOG_WEEKLYPRICE_MANAGEMENT_DEBUG)) {
			// Modo debug esta activo.
			return $button;
		}

		if ($isBusinessDay) {
			if ($isTodayThursday) {
				// Hoy es jueves y no es feriado
				return $button;
			}
			else if ($lastThursdayWasHoliday) {
				// Hoy es día hábil pero no es jueves, y el último jueves fue feriado
				return $button;
			} else {
				// Es día hábil, pero no es jueves y el jueves anterior no fue feriado.
				$this->messageManager->addWarningMessage(self::ONLY_BUSINESS_DAYS);
				return [];
			}
		} else {
			// No es día hábil.
			$this->messageManager->addWarningMessage(
				sprintf(self::ONLY_BUSINESS_DAYS_WITH_DATE, $isBusinessDay)
			);

			return [];
		}
	}
}
