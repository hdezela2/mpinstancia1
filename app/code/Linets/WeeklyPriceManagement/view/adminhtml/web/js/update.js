define([
    "jquery",
    "jquery/ui",
], function ($) {
    'use strict';

    $.widget('weeklypricesmanagement.update', {
        options: {},
        _create: function () {
            var self = this;
            $(document).ready(function () {
                var skipCount = 0;
                var importUrl = self.options.importUrl;
                var finishUrl = self.options.finishUrl;
                var total = self.options.productCount;
                var completeLabel = self.options.completeLabel;

                if (total > 0) {
                    var productData = self.options.productData;
                    updatePrices(1);
                }

                function updatePrices(count)
                {
                    $.ajax({
                        type: 'post',
                        url: importUrl,
                        async: true,
                        dataType: 'json',
                        data : { 'count': count, 'product': productData[count-1] },

                        success:function (data) {
                            if (data['error'] == 1) {
                                $(".fieldset").append(data['msg']);
                                skipCount++;
                            } else {
                                $(".fieldset").append(data['msg']);
                            }
                            localStorage.removeItem("keySize");
                            var width = (100/total) * count;
                            $(".wk-mu-progress-bar-current").animate({width: width+"%"},'slow', function () {
                                if (total == 1 && skipCount ==1) {
                                    $(".wk-mu-info-bar").addClass("wk-no-padding");
                                    $(".wk-mu-importing-loader").remove();
                                    $(".wk-mu-info-bar-content").text(completeLabel);
                                } else {
                                    if (count == total) {
                                        finishUpdating(count, skipCount);
                                    } else {
                                        count++;
                                        $(".wk-current").text(count);
                                        updatePrices(count);
                                    }
                                }
                            });
                        }
                    });
                }
                function finishUpdating(count, skipCount)
                {
                    $.ajax({
                        type: 'post',
                        url:finishUrl,
                        async: true,
                        dataType: 'json',
                        data : { row:count, skip:skipCount },
                        success:function (data) {
                            $(".fieldset").append(data['msg']);
                            $(".wk-mu-info-bar").addClass("wk-no-padding");
                            $(".wk-mu-info-bar").text(completeLabel);
                        }
                    });
                }
            });
        }
    });

    return $.weeklypricesmanagement.update;
});

