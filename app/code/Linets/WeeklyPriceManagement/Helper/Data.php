<?php
declare(strict_types=1);

namespace Linets\WeeklyPriceManagement\Helper;

use Cleverit\Requestforquote\Helper\Data as CleveritData;
use DateTime;
use DateTimeZone;
use Exception;
use Formax\Offer\Helper\Data as FormaxData;
use Linets\GasSetup\Constants as WebsiteConstants;
use Linets\PriceManagement\Constants;
use Linets\WeeklyPriceManagement\Model\ResourceModel\WeeklyPrices\Collection;
use Linets\WeeklyPriceManagement\Model\WeeklyPricesHistoryFactory;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\MpAssignProduct\Model\AssociatesFactory;
use Webkul\MpAssignProduct\Model\ItemsFactory;
use Zend_Http_Client;
use Linets\Offer\Helper\Data as OfferHelper;
use Formax\Offer\Model\Offer as OfferModel;

class Data extends AbstractHelper
{
	private $usdCurrentWeek;

	private $parityCurrentWeek;

        private $settingsId;

	/**
	 * @var ResourceConnection
	 */
	private $resourceConnection;

	/**
	 * @var ItemsFactory
	 */
	private $assignItems;

	/**
	 * @var AssociatesFactory
	 */
	private $assignAssociates;

	/**
	 * @var ProductFactory
	 */
	private $productFactory;

	/**
	 * @var StoreManagerInterface
	 */
	private $storeManager;

	/**
	 * @var Collection
	 */
	private $weeklyPricesCollection;

	/**
	 * @var WeeklyPricesHistoryFactory
	 */
	private $historyFactory;

	/**
	 * @var StoreRepositoryInterface
	 */
	private $storeRepositoryManager;

	/**
	 * @var CollectionFactory
	 */
	private $productCollectionFactory;

	/**
	 * @var CategoryFactory
	 */
	private $categoryFactory;

	/**
	 * @var FormaxData
	 */
	private $formaxHelper;

	/**
	 * @var ZendClientFactory
	 */
	private $httpClientFactory;

	/**
	 * @var Json
	 */
	private $serializerJson;

        /**
         * @var OfferHelper
         */
        private $offerHelper;

        /**
         * @var float
         */
        private $udsLastWeek;

        /**
         * @var float
         */
        private $parityLastWeek;

        /**
         * @var float
         */
        private $grannelConst;

	/**
	 * Data constructor.
	 * @param Context $context
	 * @param ItemsFactory $assignItems
	 * @param ProductFactory $productFactory
	 * @param AssociatesFactory $assignAssociates
	 * @param ResourceConnection $resourceConnection
	 * @param StoreManagerInterface $storeManager
	 * @param Collection $weeklyPricesCollection
	 * @param WeeklyPricesHistoryFactory $historyFactory
	 * @param StoreRepositoryInterface $storeRepositoryManager
	 * @param CollectionFactory $productCollectionFactory
	 * @param CategoryFactory $categoryFactory
	 * @param FormaxData $formaxHelper
	 * @param ZendClientFactory $httpClientFactory
	 * @param Json $serializerJson
         * @param OfferHelper $offerHelper
	 */
	public function __construct(
		Context $context,
		ItemsFactory $assignItems,
		ProductFactory $productFactory,
		AssociatesFactory $assignAssociates,
		ResourceConnection $resourceConnection,
		StoreManagerInterface $storeManager,
		Collection $weeklyPricesCollection,
		WeeklyPricesHistoryFactory $historyFactory,
		StoreRepositoryInterface $storeRepositoryManager,
		CollectionFactory $productCollectionFactory,
		CategoryFactory $categoryFactory,
		FormaxData $formaxHelper,
		ZendClientFactory $httpClientFactory,
		Json $serializerJson,
            OfferHelper $offerHelper
	) {
		$this->resourceConnection = $resourceConnection;
		$this->assignItems = $assignItems;
		$this->productFactory = $productFactory;
		$this->assignAssociates = $assignAssociates;
		$this->storeManager = $storeManager;
		$this->weeklyPricesCollection = $weeklyPricesCollection;
		$this->historyFactory = $historyFactory;
		$this->storeRepositoryManager = $storeRepositoryManager;
		$this->productCollectionFactory = $productCollectionFactory;
		$this->categoryFactory = $categoryFactory;
		$this->formaxHelper = $formaxHelper;
		$this->httpClientFactory = $httpClientFactory;
		$this->serializerJson = $serializerJson;
            $this->offerHelper = $offerHelper;

		parent::__construct($context);
	}

	/**
	 * @throws LocalizedException
	 */
	public function updateProducts($productData): array
	{
		if ($productData['child'] != "false") {
			return $this->updateProductConfigurable($productData);

		} else {
			return $this->updateProductSimple($productData);
		}
	}


	/**
	 * @throws LocalizedException
	 */
	public function updateProductSimple($productData): array
	{
		$historyData = [];
		$resourceConnection = $this->resourceConnection->getConnection();
		$assignItem = $this->assignItems->create()->load($productData['id']);
		$productId = $assignItem->getProductId();
		$product = $this->productFactory->create()->load($productId);
                $params = $this->weeklyPricesCollection
			->addFieldToFilter('is_active', 1)
			->getFirstItem();
                $this->settingsId = $params->getEntityId();

		$response['name'] = $product->getName();
		$customer = $this->formaxHelper->getCustomerById($assignItem->getSellerId());
		$historyData['seller_name'] = $customer->getName();
                $historyData['seller_rut'] = $customer->getWkvDccpRut();
		$historyData['product_id'] = $productId;
                $historyData['product_sku'] = $product->getSku();
                $historyData['weeklyprices_settings_id'] = $this->settingsId;

		$select = $resourceConnection->select();
		$select->from('dccp_offer_product', ['status']);
		$select->where('product_id = '. $assignItem->getProductId());
		$select->where('start_date <= DATE(NOW())');
		$select->where('end_date >= DATE(NOW())');
		$select->where('status = ' . OfferModel::OFFER_APPLIED);
		$is_offer = $resourceConnection->fetchOne($select);

		if ($is_offer) {
			$select = $resourceConnection->select();
			$select->from('dccp_offer_product', ['base_price']);
			$select->where('product_id = '. $assignItem->getProductId());
			$select->where('start_date <= DATE(NOW())');
			$select->where('end_date >= DATE(NOW())');
			$select->where('status = ' . OfferModel::OFFER_APPLIED);

			$basePrice = $resourceConnection->fetchOne($select);
			$updatedPrice = ($basePrice > 0) ? $this->calculatePrice($basePrice, $product) : 0;

			$resourceConnection->beginTransaction();
			$update_offer = 'UPDATE  dccp_offer_product set base_price = "'.$updatedPrice.'" where product_id = '.(int) $productId.' and status = ' . OfferModel::OFFER_APPLIED;
			$resourceConnection->query($update_offer);
			$resourceConnection->commit();

			$historyData['last_price'] = $basePrice;
			$historyData['current_price'] = $updatedPrice;
			$historyData['usd_price'] = $this->usdCurrentWeek;
			$historyData['usd_byton_price'] = $this->parityCurrentWeek;
                        $historyData['parity_last_week'] = $this->parityLastWeek;
                        $historyData['usd_last_week'] = $this->udsLastWeek;
                        $historyData['grannel_const'] = $this->grannelConst;
		} else {
			$originalPrice = $assignItem->getPrice();
			$updatedPrice = ($originalPrice > 0) ? $this->calculatePrice($originalPrice, $product) : 0;

			$assignItem->setPrice($updatedPrice);
			$assignItem->save();

			$historyData['last_price'] = $originalPrice;
			$historyData['current_price'] = $updatedPrice;
			$historyData['usd_price'] = $this->usdCurrentWeek;
			$historyData['usd_byton_price'] = $this->parityCurrentWeek;
                        $historyData['parity_last_week'] = $this->parityLastWeek;
                        $historyData['usd_last_week'] = $this->udsLastWeek;
                        $historyData['grannel_const'] = $this->grannelConst;
		}

		$this->saveHistory($historyData);

		return $response;
	}

	/**
	 * @throws LocalizedException
	 */
	public function updateProductConfigurable($productData)
	{
		$historyData = [];
        $response = [];
		$assignProducts = $this->assignAssociates->create()
			->getCollection()
			->addFieldToFilter('id', $productData['id']);
                $params = $this->weeklyPricesCollection
			->addFieldToFilter('is_active', 1)
			->getFirstItem();
                $this->settingsId = $params->getEntityId();

		foreach ($assignProducts as $assignProduct) {
			$productId = $assignProduct->getProductId();
			$parentId = $assignProduct['parent_id'];

			$product = $this->productFactory->create()->load($productId);
			$assignItem = $this->assignItems->create()->load($parentId);
			$response['name'] = $product->getName();

			$customer = $this->formaxHelper->getCustomerById($assignItem->getSellerId());
                        $historyData['seller_name'] = $customer->getName();
                        $historyData['seller_rut'] = $customer->getWkvDccpRut();
			$historyData['product_id'] = $productId;
                        $historyData['product_sku'] = $product->getSku();
                        $historyData['weeklyprices_settings_id'] = $this->settingsId;
                        $isOffer = $this->getFromDccpOffer('status', $productId, $parentId);

			if ($isOffer) {
				$historyData = array_merge($historyData, $this->updateOffer($product, $productId, $parentId));

			} else {
				$originalPrice = $assignProduct->getPrice();
				$updatedPrice = ($originalPrice > 0) ? $this->calculatePrice($originalPrice, $product) : 0;

				$assignProduct->setPrice($updatedPrice);
				$assignProduct->save();

				$historyData['last_price'] = $originalPrice;
				$historyData['current_price'] = $updatedPrice;
				$historyData['usd_price'] = $this->usdCurrentWeek;
				$historyData['usd_byton_price'] = $this->parityCurrentWeek;
                                $historyData['parity_last_week'] = $this->parityLastWeek;
                                $historyData['usd_last_week'] = $this->udsLastWeek;
                                $historyData['grannel_const'] = $this->grannelConst;
			}

			$this->saveHistory($historyData);
		}

		return $response;
	}

	/**
	 * @param $basePrice
	 * @param Product $product
	 * @return int
	 */
	public function calculatePrice($basePrice, Product $product): int
	{
		$this->parityCurrentWeek = null;
		$this->usdCurrentWeek = null;
		$updatedPrice = $basePrice;
		$params = $this->weeklyPricesCollection
			->addFieldToFilter('is_active', 1)
			->getFirstItem();

		/**
			A: Valor Paridad Semana Anterior (Toneladas)
			B: Dolar Observado Semana Anterior
			C: Valor Paridad Semana Actual (Toneladas)
			D: Dolar Observado Semana Actual
			E: N° Kilos Envasado
			F: Valor Precio Base
			G: Constante Granel (valor 0,518)

			X: Nuevo Precio Base
		 */
		$coef_A = (float) $params->getParityLastWeek();
		$coef_B = (float) $params->getUsdLastWeek();
		$coef_C = (float) $params->getParityCurrentWeek();
		$coef_D = (float) $params->getUsdCurrentWeek();
		$coef_F = (float) $basePrice;

		$categoriesIds = $product->getCategoryIds();
                $envasadoCategoryId = $this->offerHelper->getEnvasadoCategoryId();
                $granelCategoryId = $this->offerHelper->getGranelCategoryId();

		if (in_array($envasadoCategoryId, $categoriesIds)) {
			$coef_E = $product->getWeight() ? $product->getWeight() : 1;
			// Cálculo Envasado: X= f + ( e * (((c * d) - (a * b)) / 1000))
			$updatedPrice = $coef_F + ( $coef_E * ((($coef_C * $coef_D) - ($coef_A * $coef_B)) / 1000));
		} elseif (in_array($granelCategoryId, $categoriesIds)) {
			$coef_G = (float) $params->getGrannelConst();
			// Cálculo Granel: X= f + (g * (((c * d) - (a * b)) / 1000))
			$updatedPrice = $coef_F + ($coef_G * ((($coef_C * $coef_D) - ($coef_A * $coef_B)) / 1000));
		}

		$this->parityCurrentWeek = $coef_C;
		$this->usdCurrentWeek = $coef_D;
                $this->udsLastWeek = $coef_B;
                $this->parityLastWeek = $coef_A;
                $this->grannelConst = $params->getGrannelConst();

		return (int) $updatedPrice;
	}

	public function getProducts()
	{
		try {
			$gasStore = $this->storeRepositoryManager->get(WebsiteConstants::GAS_STORE_CODE);
			if ($gasStore->getId()) {
				$gasStoreId = $gasStore->getId();
				$response['assignProductIds'] = [];
				$collection = $this->productCollectionFactory->create()
					->addFieldToSelect('*')
					->addStoreFilter($gasStoreId)
					->load();

				if ($allProductIds = $collection->getAllIds()){
					$assignProducts = $this->assignItems->create()
						->getCollection()
						->addFieldToFilter('product_id', ['in' => [$allProductIds]]);
					$i=0;
					foreach ($assignProducts as $assignProductItem) {
						if ($assignProductItem->getType() == 'configurable') {
							$associateAssignProducts = $this->assignAssociates->create()->getCollection()
								->addFieldToFilter('parent_product_id', $assignProductItem->getProductId())
								->addFieldToFilter('parent_id', $assignProductItem->getId());
							foreach ($associateAssignProducts as $associateAssignProductItem) {
								$response['assignProductIds'][$i]['id'] = $associateAssignProductItem->getId();
								$response['assignProductIds'][$i]['child'] = true;
								$i++;
							}
						}
						else {
							$response['assignProductIds'][$i]['id'] = $assignProductItem->getId();
							$response['assignProductIds'][$i]['child'] = false;
							$i++;
						}
					}
				}

				return $response;
			}
		} catch (NoSuchEntityException $e) {
			return [];
		}
	}

	/**
	 * @param array $weeklyPricesHistory
	 * @throws AlreadyExistsException
	 */
	public function saveHistory(array $weeklyPricesHistory)
	{
		$historyFactory = $this->historyFactory->create();
		$historyFactory->setData($weeklyPricesHistory);
		$historyFactory->save();
	}

	/**
	 * @param $date
	 * @return int
	 */
	public function weekOfMonth($date): int
	{
		$firstOfMonth = strtotime(date("Y-m-01", $date));
		return $this->weekOfYear($date) - $this->weekOfYear($firstOfMonth) + 1;
	}

	/**
	 * @param $date
	 * @return int
	 */
	public function weekOfYear($date): int
	{
		$weekOfYear = intval(date("W", $date));
		if (date('n', $date) == "1" && $weekOfYear > 51) {
			$weekOfYear = 0;
		}
		return $weekOfYear;
	}

	/**
	 * @return bool
	 */
	public function isFirstWeekOfMonth(): bool
	{
		// TODO aplicar modo debug
		return $this->weekOfMonth(strtotime(date('d-m-y h:i:s'))) === 1;
	}

	/**
	 * @return bool
	 * @throws Exception
	 */
	public function isTodayThursday()
	{
		$currentDate = new DateTime('now', new DateTimeZone(Constants::DEFAULT_TIMEZONE));
		$day = $currentDate->format('D');

		if ($this->isTodayBusinessDay() && $day === 'Thu') {
			return true;
		}

		return false;
	}

	/**
	 * @throws Exception
	 */
	public function lastThursdayWasHoliday(): bool
	{
		$currentDate = new DateTime('now', new DateTimeZone(Constants::DEFAULT_TIMEZONE));
		$thuBefore = strtotime("last thursday", strtotime($currentDate->format('d-m-Y')));
		$thuBefore = date('d-m-Y', $thuBefore);

		return (!$this->isTodayBusinessDay($thuBefore));
	}

	/**
	 *
	 * @throws Exception
	 */
	public function isTodayBusinessDay($givenDate = null)
	{
		if ($givenDate) {
			$currentDate = new DateTime($givenDate, new DateTimeZone(Constants::DEFAULT_TIMEZONE));
		} else {
			$currentDate = new DateTime('now', new DateTimeZone(Constants::DEFAULT_TIMEZONE));
		}

		$dateMinusOne = date( "d-m-Y", strtotime( $currentDate->format('d-m-Y') . "-1 day"));

		$response = $this->calculateBusinessDate($dateMinusOne);

		if (isset($response['success']) && trim($response['success']) == 'OK' && isset($response['payload'])) {
			$date = $response['payload']['fecha_termino'];
			$dateEndpoint = new DateTime($date);

			return $dateEndpoint->format('d-m-Y') == $currentDate->format('d-m-Y')
				? true
				: $dateEndpoint->format('d-m-y');
		}

		return false;
	}


	/**
	 * @param $startDate
	 * @param int $days
	 * @return array|bool|float|int|mixed|string|null
	 */
	public function calculateBusinessDate($startDate, int $days = 1)
	{
		$response = null;
		try {
			$endpoint = rtrim($this->scopeConfig->getValue(
				'dccp_endpoint/endpoint_holidays/endpoint',
				ScopeInterface::SCOPE_STORE
			), '/');

			if ($endpoint && $startDate) {
				$endpoint = sprintf($endpoint, $startDate, $days);

				$client = $this->httpClientFactory->create();
				$client->setUri($endpoint);
				$client->setConfig(['maxredirects' => 0, 'timeout' => 30]);

				$response = $client->request(Zend_Http_Client::GET)->getBody();

				$this->_logger->info('Holidays Start Date: ' . $startDate . ' Days: ' . $days . ' Service Response: ' . $response);
				$response = $this->serializerJson->unserialize($response);
			} else {
				$this->_logger->error('Define endpoint and params of holidays webservice');
			}
		} catch(Exception $e) {
			$this->_logger->critical('Holidays Error message: ' . $e->getMessage());
		}

		return $response;
	}

        /**
         * @param Product $product
         * @param int $productId
         * @param int $assignId
         * @return array
         */
        private function updateOffer($product, $productId, $assignId)
        {
            $historyData = [];
            $resourceConnection = $this->resourceConnection->getConnection();
            $basePrice = $this->getFromDccpOffer('base_price', $productId, $assignId);
            $updatedPrice = ($basePrice > 0) ? $this->calculatePrice($basePrice, $product) : 0;

            $resourceConnection->beginTransaction();
            $update_offer = 'UPDATE  dccp_offer_product set base_price = "'.$updatedPrice.'" where product_id = '.(int) $productId.' and status = ' . OfferModel::OFFER_APPLIED . ' and assign_id = ' . (int) $assignId;
            $resourceConnection->query($update_offer);
            $resourceConnection->commit();

            $historyData['last_price'] = $basePrice;
            $historyData['current_price'] = $updatedPrice;
            $historyData['usd_price'] = $this->usdCurrentWeek;
            $historyData['usd_byton_price'] = $this->parityCurrentWeek;
            $historyData['parity_last_week'] = $this->parityLastWeek;
            $historyData['usd_last_week'] = $this->udsLastWeek;
            $historyData['grannel_const'] = $this->grannelConst;

            return $historyData;
        }

        /**
         * @param string $field
         * @param int $productId
         * @param int|boolean $assignId
         * @param boolean $fetchAll
         * @return array
         */
        private function getFromDccpOffer($field, $productId, $assignId, $fetchAll = false)
        {
            $resourceConnection = $this->resourceConnection->getConnection();
            $select = $resourceConnection->select();
            $select->from('dccp_offer_product', $field);
            $select->where('product_id = '. $productId);
            $select->where('start_date <= DATE(NOW())');
            $select->where('end_date >= DATE(NOW())');
            $select->where('assign_id = ' . $assignId);
            $select->where('status = ' . OfferModel::OFFER_APPLIED);

            if ($fetchAll) {
                $result = $resourceConnection->fetchAll($select);
            } else {
                $result = $resourceConnection->fetchOne($select);
            }

            return $result;
        }
}
