<?php

namespace Linets\WeeklyPriceManagement\Model;

use Linets\WeeklyPriceManagement\Model\ResourceModel\WeeklyPricesHistory as ResourceModel;
use Magento\Framework\Model\AbstractModel;

class WeeklyPricesHistory extends AbstractModel
{
	/**
	 * @var string
	 */
	protected $_eventPrefix = 'linets_weeklyprices_history_model';

	/**
	 * @inheritdoc
	 */
	protected function _construct()
	{
		$this->_init(ResourceModel::class);
	}
}
