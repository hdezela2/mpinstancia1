<?php

namespace Linets\WeeklyPriceManagement\Model\ResourceModel\WeeklyPrices;

use Linets\WeeklyPriceManagement\Model\ResourceModel\WeeklyPrices as ResourceModel;
use Linets\WeeklyPriceManagement\Model\WeeklyPrices as Model;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	/**
	 * @var string
	 */
	protected $_eventPrefix = 'linets_weeklyprices_collection';

	/**
	 * @inheritdoc
	 */
	protected function _construct()
	{
		$this->_init(Model::class, ResourceModel::class);
	}
}
