<?php

namespace Linets\WeeklyPriceManagement\Model\ResourceModel\WeeklyPricesHistory;

use Linets\WeeklyPriceManagement\Model\ResourceModel\WeeklyPricesHistory as ResourceModel;
use Linets\WeeklyPriceManagement\Model\WeeklyPricesHistory as Model;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
	/**
	 * @var string
	 */
	protected $_eventPrefix = 'linets_weeklyprices_history_collection';

	/**
	 * @inheritdoc
	 */
	protected function _construct()
	{
		$this->_init(Model::class, ResourceModel::class);
	}
}
