<?php

namespace Linets\WeeklyPriceManagement\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class WeeklyPrices extends AbstractDb
{
	/**
	 * @var string
	 */
	protected $_eventPrefix = 'linets_weeklyprices_resource_model';

	/**
	 * @inheritdoc
	 */
	protected function _construct()
	{
		$this->_init('linets_weeklyprices', 'entity_id');
		$this->_useIsObjectNew = true;
	}
}
