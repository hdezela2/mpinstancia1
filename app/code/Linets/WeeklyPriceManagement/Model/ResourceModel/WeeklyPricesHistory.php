<?php

namespace Linets\WeeklyPriceManagement\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class WeeklyPricesHistory extends AbstractDb
{
	/**
	 * @var string
	 */
	protected $_eventPrefix = 'linets_weeklyprices_history_resource_model';

	/**
	 * @inheritdoc
	 */
	protected function _construct()
	{
		$this->_init('linets_weeklyprices_history', 'entity_id');
	}
}
