<?php

namespace Linets\WeeklyPriceManagement\Model;

use Linets\WeeklyPriceManagement\Model\ResourceModel\WeeklyPrices as ResourceModel;
use Magento\Framework\Model\AbstractModel;

class WeeklyPrices extends AbstractModel
{

    const PARITY_LAST_WEEK = 'parity_last_week';

    const USD_LAST_WEEK = 'usd_last_week';

    const PARITY_CURRENT_WEEK = 'parity_current_week';

    const USD_CURRENT_WEEK = 'usd_current_week';

    const GRANNEL_CONST = 'grannel_const';
	/**
	 * @var string
	 */
	protected $_eventPrefix = 'linets_weeklyprices_model';

	/**
	 * @inheritdoc
	 */
	protected function _construct()
	{
		$this->_init(ResourceModel::class);
	}
}
