<?php
declare(strict_types=1);

namespace Linets\WeeklyPriceManagement\Controller\Adminhtml\Index;

use Linets\WeeklyPriceManagement\Helper\Data;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Json\Helper\Data as JsonData;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\InvalidRequestException;

class Modify extends Action implements HttpPostActionInterface, CsrfAwareActionInterface
{
	/**
	 * Authorization level of a basic admin session
	 */
	const ADMIN_RESOURCE = 'Linets_WeeklyPriceManagement::manage';

	/**
	 * @var Data
	 */
	private $helper;

	/**
	 * @var JsonData
	 */
	private $jsonHelper;

	/**
	 * Run constructor.
	 * @param Action\Context $context
	 * @param Data $helper
	 * @param JsonData $jsonHelper
	 */
	public function __construct(
		Action\Context $context,
		Data $helper,
		JsonData $jsonHelper
	) {
		parent::__construct($context);
		$this->helper = $helper;
		$this->jsonHelper = $jsonHelper;
	}

    /**
     * Execute action based on request and return result
     *
     * @return ResponseInterface|ResultInterface|void
     */
	public function execute()
	{
		try {
			$productData = $this->getRequest()->getParam('product');
			$responseData = $this->helper->updateProducts($productData);
			$result['error'] = 0;
			$result['msg'] = '<div class="wk-mu-success wk-mu-box">'.__('Successfully updated '.$responseData['name']).'</div>';
		} catch (\Exception $e) {
			$result['error'] = 1;
			$result['msg'] = '<div class="wk-mu-error wk-mu-box">'.__('Error updating price').'</div>';
		}

		$result = $this->jsonHelper->jsonEncode($result);
		$this->getResponse()->representJson($result);
	}

	/**
	 * @param RequestInterface $request
	 * @return InvalidRequestException|null
	 */
	public function createCsrfValidationException(RequestInterface $request): ? InvalidRequestException
	{
		return null;
	}

	/**
	 * @param RequestInterface $request
	 * @return bool|null
	 */
	public function validateForCsrf(RequestInterface $request): ?bool
	{
		return true;
	}
}
