<?php
declare(strict_types=1);

namespace Linets\WeeklyPriceManagement\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;

class Edit extends Action implements HttpGetActionInterface
{
	/**
	 * Authorization level of a basic admin session
	 */
	const ADMIN_RESOURCE = 'Linets_WeeklyPriceManagement::manage';

	/**
	 * Execute action based on request and return result
	 *
	 * @return ResultInterface|ResponseInterface
	 */
	public function execute()
	{
		return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
	}
}
