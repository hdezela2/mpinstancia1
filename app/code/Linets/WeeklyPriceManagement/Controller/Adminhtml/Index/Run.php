<?php
declare(strict_types=1);

namespace Linets\WeeklyPriceManagement\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;

class Run extends Action implements HttpGetActionInterface
{
	/**
	 * Authorization level of a basic admin session
	 */
	const ADMIN_RESOURCE = 'Linets_WeeklyPriceManagement::manage';

	/**
	 * @var PageFactory
	 */
	private $resultPageFactory;

	/**
	 * Run constructor.
	 * @param Action\Context $context
	 * @param PageFactory $resultPageFactory
	 */
	public function __construct(
		Action\Context $context,
		PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	/**
	 * Execute action based on request and return result
	 *
	 * @return ResultInterface|ResponseInterface
	 */
	public function execute()
	{
		$resultPage = $this->resultPageFactory->create();
		$resultPage->getConfig()->getTitle()->prepend(__('Reading Data'));

		return $resultPage;
	}
}
