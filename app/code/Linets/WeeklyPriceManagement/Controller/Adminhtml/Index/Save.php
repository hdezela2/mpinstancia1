<?php
declare(strict_types=1);

namespace Linets\WeeklyPriceManagement\Controller\Adminhtml\Index;

use Linets\WeeklyPriceManagement\Model\WeeklyPrices;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Save extends Action implements HttpPostActionInterface
{
	/**
	 * Authorization level of a basic admin session
	 */
	const ADMIN_RESOURCE = 'Linets_WeeklyPriceManagement::manage';
	/**
	 * @var WeeklyPrices
	 */
	private $weeklyPrices;

	/**
	 * @var Action\Context
	 */
	private $context;

	/**
	 * Save constructor.
	 * @param Action\Context $context
	 * @param WeeklyPrices $weeklyPrices
	 */
	public function __construct(
		Action\Context $context,
		WeeklyPrices $weeklyPrices
	) {
		parent::__construct($context);
		$this->weeklyPrices = $weeklyPrices;
		$this->context = $context;
	}

	/**
	 * Execute action based on request and return result
	 *
	 * @return ResultInterface|ResponseInterface
	 */
	public function execute()
	{
		$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

		if ($post = $this->_request->getParams()) {
			try {
				if (!array_key_exists('update_parameters', $post)) {
					throw new \Exception('Todos los datos son requeridos');
				}

				$this->weeklyPrices
					->setData($post['update_parameters'])
					->save();

			} catch (\Exception $e) {
				$this->messageManager->addErrorMessage(
					'Hubo un error al intentar guardar: ' . $e->getMessage()
				);
				return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
			}

			$this->messageManager->addSuccessMessage('Los datos han sido actualizados.');

			$url = $this->context->getUrl();
			$url = $url->getUrl('weeklyprices/index/run');

			return $resultRedirect->setUrl($url);

		}

		return $resultRedirect;
	}
}
