<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
	ComponentRegistrar::MODULE,
	'Linets_WeeklyPriceManagement',
	__DIR__
);
