<?php

namespace Linets\ShowMultiSellerBestPrice\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Linets\SellerCentralizedDispatchCost\Model\ResourceModel\DispatchPrice\CollectionFactory as DispatchPriceCollection;
use Magento\Catalog\Model\Product;
use Magento\Eav\Api\AttributeSetRepositoryInterface;


class Helper implements ArgumentInterface
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var DispatchPriceCollection
     */
    private $dispatchPrice;
    protected $product;
    protected $attributeSetRepository;

    /**
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     * @param DispatchPriceCollection $dispatchPrice
     */
    public function __construct(
        StoreManagerInterface   $storeManager,
        ScopeConfigInterface    $scopeConfig,
        DispatchPriceCollection $dispatchPrice,
        Product $product,
        AttributeSetRepositoryInterface $attributeSetRepository
    )
    {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->dispatchPrice = $dispatchPrice;
        $this->product = $product;
        $this->attributeSetRepository = $attributeSetRepository;
    }

    /**
     * @return string
     */
    public function isActive()
    {
        return $this->scopeConfig->getValue(
            'catalog/show_multiseller_best_price/validate_show',
            ScopeInterface::SCOPE_WEBSITES
        );
    }

    /**
     * @return int
     */
    public function getNumberBestOffersVisibles()
    {
        $num = intval($this->scopeConfig->getValue(
            'catalog/show_multiseller_best_price/number_of_best_offers',
            ScopeInterface::SCOPE_WEBSITES)
        );

        if ($num == 0 || empty($num)) {
            $num = 1;
        }

        return $num;
    }

    public function getDispatchPrice($sellerId, $productId, $websiteId)
    {
        try {
            $product = $this->product->load($productId);
            $attributeSetId = $product->getAttributeSetId();
            $collection = $this->dispatchPrice->create();
            $collection->getSelect()
                ->where('seller_id = ?', $sellerId)
                ->where('attribute_set_id = ?', $attributeSetId)
                ->where('website_id = ?', $websiteId);
            $dispatchPriceJson = [];
            foreach($collection->getData() as $dispatchPrice){
                $macrozone = strtolower($dispatchPrice['macrozone']);
                $dPrice = $dispatchPrice['price'];
                $dispatchPriceJson += [$macrozone => $dPrice];
            }
            $dispatchPriceAttrData = 'data-dispatch-prices='.json_encode($dispatchPriceJson);
        } catch (\Exception $ex) {
            return [];
        }
        return $dispatchPriceAttrData;
    }

}
