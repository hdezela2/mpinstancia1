<?php
/**
 * Copyright © Linets All rights reserved.
 * See COPYING.txt for license details.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Linets_ShowMultiSellerBestPrice',
    __DIR__
);