<?php

namespace Linets\ShowMultiSellerBestPrice\Model\Config;

use Magento\Framework\App\Config\Value;
use Magento\Framework\Exception\LocalizedException;

class ValidateNumberInt extends Value
{
    /**
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function beforeSave()
    {
        $value = $this->getValue();
        if (preg_match("/^[0-9]+([0-9]*)?$/", $value)) {
            $validator = true;
        } else {
            $validator = false;
        };

        if (!$validator) {
            $message = __('The number must be an integer.');
            throw new LocalizedException($message);
        }

        return $this;
    }

}