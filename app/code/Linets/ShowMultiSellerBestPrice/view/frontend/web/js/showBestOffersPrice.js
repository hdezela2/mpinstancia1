define([
    'jquery',
    'underscore',
    'mage/translate'
], function ($, _, $t) {
    'use strict';
    $.widget('mage.showBestOffersPrice', {
        _create: function (config, element) {
            let self = this;
            let maxBestOffers = self.options.maxBestOffers;
            let isActiveShowBestOffers = self.options.isActiveShowBestOffers;
            $(document).ready(function () {
                if (isActiveShowBestOffers) {
                    $('#product-options-wrapper .super-attribute-select').change(function () {
                        $('#label-best-price').remove();
                        $('input[type=radio][value=best_price]').prop("checked", true);
                        setTimeout(function () {
                            window.allOffersVisibles = getOffersVisibles();
                            getBestPrice();
                            showTypeOffers(window.allOffersVisibles, 'best_price');
                            showDispatchPrice();
                        }, 500);
                    });
                }
                //Tab offer table
                $('input[type=radio][name=table_best_price]').change(function () {
                    showTypeOffers(window.allOffersVisibles, this.value);
                });
                //Filter Days
                $('#cc-suppliers_filter-by-days').change(function () {
                    $('input#all_prices').prop("checked", true);
                });
            });

            /**
             * To identify that the offers tabs (all Offers and Best offers)
             * @param allOffersVisibles
             * @param type
             */
            function showTypeOffers(allOffersVisibles, type = 'best_price') {
                //Clone Offers Visibles
                let allOffersVisiblesClone = [...allOffersVisibles];
                let maxBestOffersToShow = getMaxShowOffers(allOffersVisibles,maxBestOffers);
                let maxFilter = $('#cc-suppliers_filter-by-days').attr('max');
                //Reset Filter Days
                $('#cc-suppliers_filter-by-days').val(maxFilter);
                $('#days-rangeSelected').text(maxFilter);

                if (type == 'best_price') {
                    let itemsToRemove = allOffersVisiblesClone.splice(maxBestOffersToShow, allOffersVisiblesClone.length);
                    itemsToRemove.forEach(function (item) {
                        $('#suppliers_box .wk-table-product-list>tbody>tr[data-sellerid=' + item.id + ']').hide();
                    });
                    allOffersVisiblesClone.forEach(function (item) {
                        $('#suppliers_box .wk-table-product-list>tbody>tr[data-sellerid=' + item.id + ']').show();
                    });

                } else if (type == 'all_prices') {
                    allOffersVisibles.forEach(function (item) {
                        $('#suppliers_box .wk-table-product-list>tbody>tr[data-sellerid=' + item.id + ']').show();
                    });
                }
            }

            /**
             * Return Offers Visible
             * @returns {*[]}
             */
            function getOffersVisibles() {
                let pList = window.supplierFilters.list;
                let allOffersVisibles = [];
                pList.forEach(function (item) {
                    if (!$('#suppliers_box .wk-table-product-list>tbody>tr[data-sellerid=' + item.id + ']').is(':visible')) {
                        return;
                    }
                    allOffersVisibles.push(item);
                });

                return allOffersVisibles;
            }

            /**
             * Show Label to Best Price in table to offers
             */
            function getBestPrice() {
                let pList = window.supplierFilters.list;
                let bestOfferPrice = 99999999999999;
                let textLabel = $t('Best Price');
                pList.forEach(function (item) {
                    if (!$('#suppliers_box .wk-table-product-list>tbody>tr[data-sellerid=' + item.id + ']').is(':visible')) {
                        return;
                    }
                    bestOfferPrice = item.price < parseInt(bestOfferPrice) ? item.price : bestOfferPrice;
                });
                $('#suppliers_box .wk-table-product-list>tbody>tr>td.wk-ap-price[data-base=' + bestOfferPrice + ']').append("<span id='label-best-price'>" + textLabel + "</span>").css("white-space", "nowrap");
            }

            /**
             *
             * @param allOffersVisibles
             * @param maxBestOffers
             * @returns {*}
             */
            function getMaxShowOffers(allOffersVisibles,maxBestOffers){
                let allOffersVisible = [...allOffersVisibles];
                let maxBestOffersToShow = maxBestOffers
                let firstOffer = allOffersVisible.shift();
                let priceOffer = firstOffer.price;
                allOffersVisible.forEach(function (item) {
                    let priceNewOffer = item.price;
                    if (priceOffer === priceNewOffer){
                        maxBestOffersToShow++;
                    } else{
                        priceOffer = priceNewOffer;
                    }
                });
                return maxBestOffersToShow;
            }

            function showDispatchPrice(){
                let actualMacrozone = $("#product-options-wrapper .super-attribute-select option:selected").text().toLowerCase();
                console.log('actual macrozona' + actualMacrozone);
                $(".wk-table-product-list tbody tr").each(function () {
                    var dataMacrozone = $(this).attr('data-dispatch-prices');
                    console.log('data actual macrozona' + dataMacrozone);
                    if (dataMacrozone){
                        var d = JSON.parse(dataMacrozone);
                        var DispatchPriceMacrozone = d[actualMacrozone];
                        DispatchPriceMacrozone = '$' + formatPrice(DispatchPriceMacrozone);
                        $(this).find("td.wk-ap-dispatch-price span.wk-ap-product-dispatch-price").replaceWith('<span class="wk-ap-product-dispatch-price">'+DispatchPriceMacrozone+'</span>');
                    }
                });
            }
            function formatPrice(price){
                return Number(price).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
            }
        }
    });

    return $.mage.showBestOffersPrice;
});