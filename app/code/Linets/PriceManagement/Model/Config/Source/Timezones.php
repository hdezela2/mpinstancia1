<?php
declare(strict_types=1);

namespace Linets\PriceManagement\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Timezones implements OptionSourceInterface
{
	/**
	 * @return string[]
	 */
	public function toOptionArray(): array
	{
		$index = 0;
		$timeZoneArray = [];
		foreach (timezone_identifiers_list() as $timeZone) {
			$timeZoneArray[$index]['value'] = $timeZone;
			$timeZoneArray[$index]['label'] = $timeZone;
			$index++;
		}
		
		return $timeZoneArray;
	}
}
