<?php
declare(strict_types=1);

namespace Linets\PriceManagement\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Days implements OptionSourceInterface
{
	/**
	 * @return string[]
	 */
	public function toOptionArray(): array
	{
		return [
			['value' => 'Mon', 'label' => 'Lunes'],
			['value' => 'Tue', 'label' => 'Martes'],
			['value' => 'Wed', 'label' => 'Miércoles'],
			['value' => 'Thu', 'label' => 'Jueves'],
			['value' => 'Fri', 'label' => 'Viernes'],
			['value' => 'Sat', 'label' => 'Sábado'],
			['value' => 'Sun', 'label' => 'Domingo']
		];
	}
}
