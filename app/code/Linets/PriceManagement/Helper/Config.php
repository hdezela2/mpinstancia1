<?php
declare(strict_types=1);

namespace Linets\PriceManagement\Helper;

use DateTime;
use DateTimeZone;
use Exception;
use Linets\PriceManagement\Constants;
use Linets\PriceManagement\Constants as PriceManagementConstants;
use Linets\PriceManagement\Model\Config\Source\Days;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;

class Config extends AbstractHelper
{
	/**
	 * @var StoreManagerInterface
	 */
	private $storeManager;

	/**
	 * @var Days
	 */
	private $configDays;

	/**
	 * Index constructor.
	 * @param Context $context
	 * @param StoreManagerInterface $storeManager
	 * @param Days $configDays
	 */
    public function __construct(
		Context $context,
		StoreManagerInterface $storeManager,
		Days $configDays
    ) {
		$this->storeManager = $storeManager;
		$this->configDays = $configDays;

		parent::__construct($context);
	}

	/**
	 * @return mixed
	 */
	public function isFeatureEnable()
	{
		return $this->scopeConfig->getValue(Constants::XML_PATH_PRICE_MANAGEMENT_ENABLE, ScopeInterface::SCOPE_WEBSITE);
	}

	/**
	 * @return mixed
	 */
	public function getSelectedWebsites()
	{
		return $this->scopeConfig->getValue(Constants::XML_PATH_PRICE_MANAGEMENT_WEBSITE, ScopeInterface::SCOPE_WEBSITE);
	}

	/**
	 * @return mixed
	 */
	public function getSelectedDays()
	{
		return $this->scopeConfig->getValue(Constants::XML_PATH_PRICE_MANAGEMENT_DAYS, ScopeInterface::SCOPE_WEBSITE);
	}

	/**
	 * @return mixed
	 */
	public function getSelectedTimezone()
	{
		return $this->scopeConfig->getValue(Constants::XML_PATH_PRICE_MANAGEMENT_TIMEZONE, ScopeInterface::SCOPE_WEBSITE) ?? Constants::DEFAULT_TIMEZONE;
	}

	/**
	 * @return string[]|string[][]
	 */
	public function getAllDays()
	{
		return $this->configDays->toOptionArray();
	}

	/**
	 * @return array
	 */
	public function getSelectedLabels(): array
	{
		$selectedDaysLabels = [];
		$configAllDays = $this->getAllDays();
		$configDays = $this->getSelectedDays() ? explode(',', $this->getSelectedDays()) : [];
		foreach ($configDays as $selectedDay) {
			$valueIndex = array_search($selectedDay, array_column($configAllDays, 'value'));
			array_push($selectedDaysLabels, $configAllDays[$valueIndex]['label']);
		}

		return $selectedDaysLabels;
	}

    /**
     * Return true by default to not override default edition flow.
     * If feature is enabled, it verify current day and configured website.
     * @return bool
     * @throws Exception
     */
    public function verifyCurrentDayAndWebsite(): bool
    {
        $currentWebsite = $this->storeManager->getStore()->getWebsiteId();
        $selectedWebsites = $this->getSelectedWebsites() ? explode(',', $this->getSelectedWebsites()) : [];
        if ($this->isFeatureEnable() && in_array($currentWebsite, $selectedWebsites)) {
            $currentDay = new DateTime('now', new DateTimeZone($this->getSelectedTimezone()));
            $selectedDays = $this->getSelectedDays() ? explode(',', $this->getSelectedDays()) : [];

            return in_array($currentDay->format('D'), $selectedDays);
        }

        return true;
    }
}
