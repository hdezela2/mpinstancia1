<?php

namespace Linets\PriceManagement;

class Constants
{
	const XML_PATH_PRICE_MANAGEMENT_ENABLE = 'catalog/price_management/enable';
	const XML_PATH_PRICE_MANAGEMENT_WEBSITE = 'catalog/price_management/store';
	const XML_PATH_PRICE_MANAGEMENT_TIMEZONE = 'catalog/price_management/timezone';
	const XML_PATH_PRICE_MANAGEMENT_DAYS = 'catalog/price_management/days';

	const DEFAULT_TIMEZONE = 'America/Santiago';
}