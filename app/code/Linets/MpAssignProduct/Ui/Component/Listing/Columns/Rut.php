<?php
declare(strict_types=1);

namespace Linets\MpAssignProduct\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Customer\Model\CustomerFactory;

class Rut extends Column
{

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CustomerFactory $customerFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->customerFactory = $customerFactory;
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
//            var_dump($this->getData());die();
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $sellerId = $item[$fieldName];
//                $sellerId = $item['seller_id'];
//                $customer = $this->customerFactory->create()->load($sellerId);
//                $item[$fieldName] = $sellerId;
            }
        }

        return $dataSource;
    }
}
