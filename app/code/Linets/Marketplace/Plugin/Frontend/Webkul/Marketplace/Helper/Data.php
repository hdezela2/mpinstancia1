<?php
declare(strict_types=1);

namespace Linets\Marketplace\Plugin\Frontend\Webkul\Marketplace\Helper;

use Magento\Store\Model\StoreManagerInterface;
use Linets\VoucherSetup\Model\Constants as VoucherSetupConstants;

class Data
{

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var array
     */
    private $disallowedActions = [
        'requestforquote/seller/allquotedproduct',
        'requestforquote/seller/lists',
        'mpassignproduct/product/view'
    ];

    /**
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Webkul\Marketplace\Helper\Data $subject
     * @param boolean $result
     * @param string $actionName
     * @return boolean
     */
    public function afterIsAllowedAction(
        \Webkul\Marketplace\Helper\Data $subject,
        $result,
        $actionName = ''
    ) {
        if ($this->storeManager->getWebsite()->getCode() === VoucherSetupConstants::WEBSITE_CODE &&
            $subject->isSeller()) {
            if (in_array($actionName, $this->disallowedActions)) {
                $result = false;
            }
        }
        return $result;
    }
}

