<?php

namespace Linets\VehiculosSetUp\Setup\Patch\Data;

use Linets\VehiculosSetUp\Model\Constants;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set as AttributeSetResourceModel;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddAttributeSetVehiculos implements DataPatchInterface
{
    /**
     * @var \Magento\Eav\Model\Entity\Attribute\SetFactory
     */
    private $attributeSetFactory;
    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set
     */
    private $attributeSetResourceModel;
    /**
     * @var \Magento\Catalog\Setup\CategorySetupFactory
     */
    private $categorySetupFactory;
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var \Magento\Eav\Setup\EavSetup
     */
    private $eavSetup;

    /**
     * AddAttributeSetVehiculos constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
     * @param \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set $attributeSetResourceModel
     * @param \Magento\Eav\Setup\EavSetup $eavSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        AttributeSetFactory $attributeSetFactory,
        CategorySetupFactory $categorySetupFactory,
        AttributeSetResourceModel $attributeSetResourceModel,
        EavSetup $eavSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeSetResourceModel = $attributeSetResourceModel;
        $this->eavSetup = $eavSetup;
    }

    /**
     * @inherindoc
     */
    public static function getDependencies(): array
    {
        return [
            AddCilindradaProductAttribute::class,
            AddCombustibleProductAttribute::class,
            AddGamaProductAttribute::class,
            AddMarcaVehiculosProductAttribute::class,
            AddRendimientoCarreteraProductAttribute::class,
            AddRendimientoCiudadProductAttribute::class,
            AddRendimientoMixtoProductAttribute::class,
            AddTransmisionProductAttribute::class,
        ];
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $this->createSet(Constants::ATTRIBUTE_SET_CAMIONETA);
        $this->createSet(Constants::ATTRIBUTE_SET_SUV);
        $this->moduleDataSetup->getConnection()->endSetup();
        return $this;
    }

    /**
     * @param string $setName
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function createSet(string $setName)
    {
        $categorySetup = $this->categorySetupFactory->create(['setup' => $this->moduleDataSetup]);
        $attributeSet = $this->attributeSetFactory->create();
        $entityTypeId = $categorySetup->getEntityTypeId(Product::ENTITY);
        $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
        $data = [
            'attribute_set_name' => $setName,
            'entity_type_id' => $entityTypeId,
            'sort_order' => 200,
        ];
        $attributeSet->setData($data);
        $attributeSet->validate();
        $this->attributeSetResourceModel->save($attributeSet);
        $attributeSet->initFromSkeleton($attributeSetId);
        $this->attributeSetResourceModel->save($attributeSet);
        $this->createGroup($attributeSetId, $entityTypeId);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function createGroup(int $setId, int $entityTypeId)
    {
        $this->eavSetup->addAttributeGroup($entityTypeId, $setId, Constants::ATTRIBUTE_GROUP, 99);
        $attributeGroupId = $this->eavSetup->getAttributeGroupId($entityTypeId, $setId, Constants::ATTRIBUTE_GROUP);
        $attrCodes = [
            AddCilindradaProductAttribute::ATTR_CODE,
            AddCombustibleProductAttribute::ATTR_CODE,
            AddGamaProductAttribute::ATTR_CODE,
            AddMarcaVehiculosProductAttribute::ATTR_CODE,
            AddRendimientoCarreteraProductAttribute::ATTR_CODE,
            AddRendimientoCiudadProductAttribute::ATTR_CODE,
            AddRendimientoMixtoProductAttribute::ATTR_CODE,
            AddTransmisionProductAttribute::ATTR_CODE,
            'macrozona',
            'region',
        ];
        foreach ($attrCodes as $code) {
            $attributeId = $this->eavSetup->getAttributeId($entityTypeId, $code);
            $this->eavSetup->addAttributeToGroup($entityTypeId, $setId, $attributeGroupId, $attributeId);
        }
    }

    /**
     * @inherindoc
     */
    public function getAliases(): array
    {
        return [];
    }
}
