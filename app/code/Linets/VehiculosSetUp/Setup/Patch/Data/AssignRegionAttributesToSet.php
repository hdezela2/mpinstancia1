<?php

namespace Linets\VehiculosSetUp\Setup\Patch\Data;

use Linets\VehiculosSetUp\Model\Constants;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set as AttributeSetResourceModel;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AssignRegionAttributesToSet implements DataPatchInterface
{
    /**
     * @var \Magento\Eav\Model\Entity\Attribute\SetFactory
     */
    private $attributeSetFactory;
    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set
     */
    private $attributeSetResourceModel;
    /**
     * @var \Magento\Catalog\Setup\CategorySetupFactory
     */
    private $categorySetupFactory;
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var \Magento\Eav\Setup\EavSetup
     */
    private $eavSetup;

    /**
     * AddAttributeSetVehiculos constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
     * @param \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set $attributeSetResourceModel
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        AttributeSetFactory $attributeSetFactory,
        CategorySetupFactory $categorySetupFactory,
        AttributeSetResourceModel $attributeSetResourceModel,
        EavSetup $eavSetup
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeSetResourceModel = $attributeSetResourceModel;
        $this->eavSetup = $eavSetup;
    }

    /**
     * @inherindoc
     */
    public static function getDependencies(): array
    {
        return [
            AddAttributeSetVehiculos::class,
            AddShippingRegionIdProductAttribute::class,
            AddShippingRegionNameProductAttribute::class,
        ];
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $categorySetup = $this->categorySetupFactory->create(['setup' => $this->moduleDataSetup]);
        $entityTypeId = $categorySetup->getEntityTypeId(Product::ENTITY);
        /**
         * Get shipping region attributes ids
         */
        $regionIdAttributeId = $this->eavSetup->getAttributeId($entityTypeId, AddShippingRegionIdProductAttribute::ATTR_CODE);
        $regionNameAttributeId = $this->eavSetup->getAttributeId($entityTypeId, AddShippingRegionIdProductAttribute::ATTR_CODE);
        /**
         * Add attribute shipping_region_id to set CAMIONETA
         */
        $setId = $this->eavSetup->getAttributeSetId($entityTypeId, Constants::ATTRIBUTE_SET_CAMIONETA);
        $attributeGroupId = $this->eavSetup->getAttributeGroupId($entityTypeId, $setId, Constants::ATTRIBUTE_GROUP);
        $this->eavSetup->addAttributeToGroup($entityTypeId, $setId, $attributeGroupId, $regionIdAttributeId);
        $this->eavSetup->addAttributeToGroup($entityTypeId, $setId, $attributeGroupId, $regionNameAttributeId);
        /**
         * Add attribute macrozona to set SUV
         */
        $setId = $this->eavSetup->getAttributeSetId($entityTypeId, Constants::ATTRIBUTE_SET_SUV);
        $attributeGroupId = $this->eavSetup->getAttributeGroupId($entityTypeId, $setId, Constants::ATTRIBUTE_GROUP);
        $this->eavSetup->addAttributeToGroup($entityTypeId, $setId, $attributeGroupId, $regionIdAttributeId);
        $this->eavSetup->addAttributeToGroup($entityTypeId, $setId, $attributeGroupId, $regionNameAttributeId);

        $this->moduleDataSetup->getConnection()->endSetup();
        return $this;
    }

    /**
     * @inherindoc
     */
    public function getAliases(): array
    {
        return [];
    }
}
