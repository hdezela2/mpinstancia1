<?php

namespace Linets\VehiculosSetUp\Setup\Patch\Data;

use Linets\VehiculosSetUp\Model\Constants as VehiculosConstants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class StockNumberOfDays implements DataPatchInterface
{
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    private $storeFactory;
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;

    /**
     * StoreConfig constructor.
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    public function __construct(
        StoreFactory $storeFactory,
        WriterInterface $configWriter
    )
    {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
    }

    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class
        ];
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(VehiculosConstants::STORE_VIEW_CODE);
        if ($store->getId()) {
            $websiteId = $store->getWebsiteId();
            $scope = ScopeInterface::SCOPE_WEBSITES;
            $this->configWriter->save(
                VehiculosConstants::XML_PATH_NUMBER_OF_DAYS,
                120,
                $scope,
                $websiteId
            );
            $this->configWriter->save(
                VehiculosConstants::XML_PATH_STOCK_MANAGEMENT_ENABLED,
                true,
                $scope,
                $websiteId
            );
        }
    }

    public function getAliases(): array
    {
        return [];
    }
}
