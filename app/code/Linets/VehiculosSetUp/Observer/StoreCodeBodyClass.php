<?php

namespace Linets\VehiculosSetUp\Observer;

use Linets\InsumosSetup\Model\InsumosConstants;
use Linets\VehiculosSetUp\Model\Constants;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Page\Config;
use Magento\Store\Model\StoreManagerInterface;


class StoreCodeBodyClass implements ObserverInterface
{
    /**
     * @var \Magento\Framework\View\Page\Config
     */
    protected $config;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * StoreCodeBodyClass constructor.
     * @param \Magento\Framework\View\Page\Config $config
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        Config $config,
        StoreManagerInterface $storeManager
    )
    {
        $this->config = $config;
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $store = $this->storeManager->getStore();
        $storeCode = $store->getCode();
        if (in_array($storeCode,[
            Constants::STORE_VIEW_CODE,
            InsumosConstants::STORE_VIEW_CODE
        ])) {
            $this->config->addBodyClass($storeCode);
        }
    }
}