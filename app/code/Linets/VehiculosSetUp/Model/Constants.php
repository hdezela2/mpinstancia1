<?php

namespace Linets\VehiculosSetUp\Model;

class Constants
{
    const ID_AGREEMENT = 5800296;
    const WEBSITE_NAME = 'Vehiculos';
    const WEBSITE_CODE = 'vehiculos202106';
    const STORE_NAME = 'Vehiculos';
    const STORE_CODE = 'vehiculos202106';
    const STORE_VIEW_NAME = 'Vehiculos';
    const STORE_VIEW_CODE = 'vehiculos';
    const ROOT_CATEGORY_NAME = 'Vehículos - Default Category';
    const FRONT_THEME = 'Linets/Vehiculos';
    const FRONT_THEME_DEFAULT = 'Formax/ChileCompra';
    const XML_PATH_SHIPPING_TAX_RULE = 'wktax/general_settings/shipping_rulestaxes_code';
    const XML_PATH_REVIEW_ACTIVE = 'catalog/review/active';
    const XML_PATH_REVIEW_ALLOW_GUEST = 'catalog/review/allow_guest';
    const ATTRIBUTE_SET_CAMIONETA = 'CAMIONETA';
    const ATTRIBUTE_SET_SUV = 'SUV';
    const ATTRIBUTE_GROUP = 'Vehiculos';
    const XML_PATH_MIN_OFFER_DAYS = 'catalog/dccp_offer/ini_range_offer';
    const MIN_OFFER_DAYS = 1;
    const XML_PATH_MAX_OFFER_DAYS = 'catalog/dccp_offer/end_range_offer';
    const MAX_OFFER_DAYS = 4;
    const XML_PATH_REQUEST_FOR_QUOTE_ACCOUNT = 'requestforquote/requestforquote_settings/requestforquote_account';
    const XML_PATH_NUMBER_OF_DAYS = 'stock_management_configuration/general/number_of_days';
    const XML_PATH_STOCK_MANAGEMENT_ENABLED = 'stock_management_configuration/general/enable';
}
