<?php

namespace Linets\InsumosSetup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Linets\InsumosSetup\Api\Setup\AttributeSetInterface;

class AddAttributesToSet implements DataPatchInterface
{

    /**
    * @var \Magento\Framework\Setup\ModuleDataSetupInterface
    */
    private $moduleDataSetup;

    /**
     * @var AttributeSetInterface
     */
    private $attributeSetSetup;

    /**
    * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    */
    public function __construct(
       \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        AttributeSetInterface $attributeSetSetup
    ) {
       $this->moduleDataSetup = $moduleDataSetup;
       $this->attributeSetSetup = $attributeSetSetup;
    }

    /**
    * {@inheritdoc}
    */
    public function apply()
    {
       $this->moduleDataSetup->getConnection()->startSetup();
       $precioMaximoData = [
            'type' => 'text',
            'backend' => '',
            'frontend' => '',
            'label' => 'Precio Maximo',
            'input' => 'text',
            'class' => '',
            'source' => '',
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'default' => '',
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => true,
            'unique' => false,
            'apply_to' => ''
       ];
       $this->attributeSetSetup->createAttribute($this->moduleDataSetup, AttributeSetInterface::PRECIO_MAXIMO_ATTRIBUTE_CODE, $precioMaximoData);
       foreach (AttributeSetInterface::ATTRIBUTE_SETS as $attributeSetName) {
           $attributeSet = $this->attributeSetSetup->getAttributeSetByName($attributeSetName);
           $sortOrder= 100;
           foreach (AttributeSetInterface::REQUIRED_ATTRIBUTES as $attributeCode) {
               $this->attributeSetSetup->addAttributeToAttibuteSet($attributeSet, $attributeCode, $sortOrder);
               $sortOrder += 100;
           }
       }
       $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            \Linets\InsumosSetup\Setup\Patch\Data\AttributeSet::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
