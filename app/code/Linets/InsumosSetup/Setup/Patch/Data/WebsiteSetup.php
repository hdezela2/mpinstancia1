<?php

namespace Linets\InsumosSetup\Setup\Patch\Data;

use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Group as GroupResource;
use Magento\Store\Model\ResourceModel\Store as StoreResourceModel;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\ResourceModel\Website as WebsiteResource;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory as ThemeCollectionFactory;
use Magento\Theme\Model\Theme;

class WebsiteSetup implements DataPatchInterface
{
    /** @var WebsiteFactory */
    protected $websiteFactory;

    /** @var WebsiteResource */
    protected $websiteResourceModel;

    /** @var StoreFactory */
    protected $storeFactory;

    /** @var GroupFactory */
    protected $groupFactory;

    /** @var GroupResource */
    protected $groupResourceModel;

    /** @var \Magento\Store\Model\Store */
    protected $storeModel;
    /** @var StoreResourceModel */
    protected $storeResourceModel;

    /** @var ManagerInterface */
    protected $eventManager;

    /** @var CategoryFactory */
    protected $categoryFactory;

    /** @var ConfigInterface */
    protected $configInterface;

    /** @var ThemeCollectionFactory */
    protected $themeCollectionFactory;

    /**
     * WebsiteSetup constructor.
     * @param \Magento\Store\Model\WebsiteFactory $websiteFactory
     * @param \Magento\Store\Model\ResourceModel\Website $websiteResourceModel
     * @param \Magento\Store\Model\Store $storeModel
     * @param \Magento\Store\Model\ResourceModel\Group $groupResourceModel
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Store\Model\GroupFactory $groupFactory
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface
     * @param \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $themeCollectionFactory
     * @param \Magento\Store\Model\ResourceModel\Store $storeResourceModel
     */
    public function __construct(
        WebsiteFactory $websiteFactory,
        WebsiteResource $websiteResourceModel,
        Store $storeModel,
        GroupResource $groupResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory,
        ManagerInterface $eventManager,
        CategoryFactory $categoryFactory,
        ConfigInterface $configInterface,
        ThemeCollectionFactory $themeCollectionFactory,
        StoreResourceModel $storeResourceModel
    ) {
        $this->websiteFactory = $websiteFactory;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->storeFactory = $storeFactory;
        $this->groupFactory = $groupFactory;
        $this->groupResourceModel = $groupResourceModel;
        $this->storeModel = $storeModel;
        $this->eventManager = $eventManager;
        $this->categoryFactory = $categoryFactory;
        $this->configInterface = $configInterface;
        $this->themeCollectionFactory = $themeCollectionFactory;
        $this->storeResourceModel = $storeResourceModel;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     * @throws AlreadyExistsException
     * @throws \Exception
     */
    public function apply()
    {
        /** Create Website */
        $website = $this->websiteFactory->create();
        $website->load(InsumosConstants::WEBSITE_CODE);
        if (!$website->getId()) {
            $website->setCode(InsumosConstants::WEBSITE_CODE);
            $website->setName(InsumosConstants::WEBSITE_NAME);
            $this->websiteResourceModel->save($website);
        }

        /** CREATE ROOT CATEGORY */
        $category = $this->createOrUpdateRootCategory();

        /** CREATE STORE */
        if ($website->getId()) {
            $group = $this->groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName(InsumosConstants::STORE_NAME);
            $group->setCode(InsumosConstants::STORE_CODE);
            $group->setRootCategoryId($category->getId());
            $group->setDefaultStoreId(0);
            $this->groupResourceModel->save($group);
        }

        /** CREATE STORE VIEW */
        $store = $this->storeFactory->create();
        $store->load(InsumosConstants::STORE_VIEW_CODE);
        if (!$store->getId()) {
            $group = $this->groupFactory->create();
            $group->load(InsumosConstants::STORE_NAME, 'name');
            $store->setCode(InsumosConstants::STORE_VIEW_CODE);
            $store->setName(InsumosConstants::STORE_VIEW_NAME);
            $store->setWebsiteId($website->getId());
            $store->setGroupId($group->getId());
            $store->setIsActive(1);
            $this->storeResourceModel->save($store);
        }

        /** ASSIGN THEME TO STORE VIEW */
        $themes = $this->themeCollectionFactory->create()->loadRegisteredThemes();
        $isThemeAssigned = false;
        $defaultThemeId = false;
        /** @var Theme $theme */
        foreach ($themes as $theme) {
            if ($theme->getArea() == 'frontend' && $theme->getCode() == InsumosConstants::FRONT_THEME_DEFAULT) {
                $defaultThemeId = $theme->getId();
            }
            if ($theme->getArea() == 'frontend' && $theme->getCode() == InsumosConstants::FRONT_THEME) {
                $this->configInterface->saveConfig(
                    'design/theme/theme_id',
                    $theme->getId(),
                    'stores',
                    $store->getId()
                );
                $isThemeAssigned = true;
            }
        }
        if (!$isThemeAssigned && $defaultThemeId) {
            $this->configInterface->saveConfig(
                'design/theme/theme_id',
                $defaultThemeId,
                'stores',
                $store->getId()
            );
        }
    }

    /**
     * @return \Magento\Catalog\Model\Category
     * @throws \Exception
     */
    private function createOrUpdateRootCategory(): Category
    {
        $category = $this->categoryFactory->create();
        $category->setName(InsumosConstants::ROOT_CATEGORY_NAME);
        $category->setIsActive(true);
        $category->setStoreId(0);
        $parentCategory = $this->categoryFactory->create();
        $parentCategory->load(Category::TREE_ROOT_ID);
        $category->setDisplayMode(Category::DM_PRODUCT);
        $category->setPath($parentCategory->getPath());
        $category->save();
        return $category;
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases(): array
    {
        return [];
    }
}
