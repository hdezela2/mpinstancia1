<?php

namespace Linets\InsumosSetup\Setup\Patch\Data;

use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class TaxConfig implements DataPatchInterface
{
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    private $storeFactory;
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;

    /**
     * StoreConfig constructor.
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    public function __construct(
        StoreFactory    $storeFactory,
        WriterInterface $configWriter
    )
    {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
    }

    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class
        ];
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(InsumosConstants::STORE_VIEW_CODE);

        /** add shipping tax rule code */
        $websiteId = $store->getWebsiteId();
        $scope = ScopeInterface::SCOPE_WEBSITES;
        $this->configWriter->save(InsumosConstants::XML_PATH_SHIPPING_TAX_RULE, 'IVA,EXENTO,HONORARIOS', $scope, $websiteId);
    }

    public function getAliases(): array
    {
        return [];
    }
}
