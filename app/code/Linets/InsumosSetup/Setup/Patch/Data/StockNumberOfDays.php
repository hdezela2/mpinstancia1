<?php

namespace Linets\InsumosSetup\Setup\Patch\Data;

use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class StockNumberOfDays implements DataPatchInterface
{
    /**
     * @var StoreFactory
     */
    private $storeFactory;
    /**
     * @var WriterInterface
     */
    private $configWriter;

    public function __construct(
        StoreFactory    $storeFactory,
        WriterInterface $configWriter
    )
    {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
    }

    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class
        ];
    }

    /**
     * @inheritDoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(InsumosConstants::STORE_VIEW_CODE);
        if ($store->getId()) {
            $websiteId = $store->getWebsiteId();
            $scope = ScopeInterface::SCOPE_WEBSITES;
            $this->configWriter->save(
                InsumosConstants::XML_PATH_NUMBER_OF_DAYS,
                4,
                $scope,
                $websiteId
            );
            $this->configWriter->save(
                InsumosConstants::XML_PATH_STOCK_MANAGEMENT_ENABLED,
                true,
                $scope,
                $websiteId
            );
        }
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }

}