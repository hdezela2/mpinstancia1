<?php

namespace Linets\InsumosSetup\Setup\Patch\Data;

use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;

class EnableNotifications implements DataPatchInterface
{
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    private $storeFactory;
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;

    /**
     * StoreConfig constructor.
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    public function __construct(
        StoreFactory    $storeFactory,
        WriterInterface $configWriter
    )
    {
        $this->storeFactory = $storeFactory;
        $this->configWriter = $configWriter;
    }

    public static function getDependencies(): array
    {
        return [
            WebsiteSetup::class
        ];
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {
        $store = $this->storeFactory->create();
        $store->load(InsumosConstants::STORE_VIEW_CODE);
        $configs = [
            'seller_notification/general_settings/enable',
            'seller_notification/general_settings/enable_quote',
            'seller_notification/general_settings/seller_status',
            'seller_notification/general_settings/product_status',
            'seller_notification/general_settings/enable_penalty',
            'seller_notification/general_settings/enable_dispersion',
            'seller_notification/general_settings/price_readjustment',
        ];
        if (!$store->getId()) {
            throw new LocalizedException(__('Error al intentar habilitar las notificaciones para el sitio insumos medicos'));
        }
        $websiteId = $store->getWebsiteId();
        $scope = ScopeInterface::SCOPE_WEBSITES;
        foreach ($configs as $path) {
            $this->configWriter->save(
                $path,
                1,
                $scope,
                $websiteId
            );
        }
    }

    public function getAliases(): array
    {
        return [];
    }
}
