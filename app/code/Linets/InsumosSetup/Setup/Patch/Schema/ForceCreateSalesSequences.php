<?php

namespace Linets\InsumosSetup\Setup\Patch\Schema;

use Linets\InsumosSetup\Model\InsumosConstants;
use Linets\InsumosSetup\Setup\Patch\Data\WebsiteSetup;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Store\Model\StoreFactory;

class ForceCreateSalesSequences implements SchemaPatchInterface
{

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    private $storeFactory;
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $eventManager;

    public function __construct(
        StoreFactory $storeFactory,
        ManagerInterface $eventManager
    )
    {
        $this->storeFactory = $storeFactory;
        $this->eventManager = $eventManager;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply()
    {return;
        $store = $this->storeFactory->create();
        $store->load(InsumosConstants::STORE_VIEW_CODE);
        if ($store->getId()) {
            // Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
            $this->eventManager->dispatch('store_add', ['store' => $store]);
        }
    }

    public static function getDependencies()
    {
        return [
            WebsiteSetup::class
        ];
    }

    public function getAliases()
    {
        return [];
    }


}
