<?php

namespace Linets\InsumosSetup\Model;

class InsumosConstants
{
    const ID_AGREEMENT = 5800305;
    const WEBSITE_NAME = 'Insumos Medicos';
    const WEBSITE_CODE = 'insumos';
    const STORE_NAME = 'Insumos Medicos';
    const STORE_CODE = 'insumos';
    const STORE_VIEW_NAME = 'Insumos Medicos';
    const STORE_VIEW_CODE = 'insumos';
    const ROOT_CATEGORY_NAME = 'DISPOSITIVO E INSUMOS MÉDICOS - Default Category';
    const ATTRIBUTE_GROUP = 'Insumos Medicos';
    const FRONT_THEME = 'Linets/Insumos';
    const FRONT_THEME_DEFAULT = 'Formax/ChileCompra';
    /**
     * Reviews config
     */
    const XML_PATH_REVIEW_ALLOW_GUEST = 'catalog/review/allow_guest';
    const XML_PATH_REVIEW_ACTIVE = 'catalog/review/active';
    /**
     * Max and min days for special offers
     */
    const XML_PATH_MIN_OFFER_DAYS = 'catalog/dccp_offer/ini_range_offer';
    const XML_PATH_MAX_OFFER_DAYS = 'catalog/dccp_offer/end_range_offer';
    const MIN_OFFER_DAYS = 1;
    const MAX_OFFER_DAYS = 4;
    /**
     * Tax config
     */
    const XML_PATH_SHIPPING_TAX_RULE = 'wktax/general_settings/shipping_rulestaxes_code';
    /**
     * Categoría temporal
     */
    const XML_PATH_NUMBER_OF_DAYS = 'stock_management_configuration/general/number_of_days';
    const XML_PATH_STOCK_MANAGEMENT_ENABLED = 'stock_management_configuration/general/enable';

    /**
     * Vendor Attributes
     * @var string[]
     */
    public $vendorAttributes = [
        'wkv_rut_encargado_tecnico' => 'Rut Encargado Técnico',
        'wkv_pe_mz_norte' => 'Plazo de Entrega Macrozona Norte (Días Hábiles)',
        'wkv_pe_mz_centro' => 'Plazo de Entrega Macrozona Centro (Días Hábiles)',
        'wkv_pe_mz_sur' => 'Plazo de Entrega Macrozona Sur (Días Hábiles)',
        'wkv_pe_mz_austral' => 'Plazo de EntregaMacrozona Austral (Días Hábiles)',
    ];

    /**
     * @return array
     */
    public function getVendorAttributes(): array
    {
        return $this->vendorAttributes;
    }

    /**
     * @return array
     */
    public function getVendorAttributeCodes(): array
    {
        return array_keys($this->vendorAttributes);
    }
}
