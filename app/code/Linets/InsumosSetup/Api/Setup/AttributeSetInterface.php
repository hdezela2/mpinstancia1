<?php

namespace Linets\InsumosSetup\Api\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Api\Data\AttributeSetInterface as AttributeSetModelInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\InputException;

interface AttributeSetInterface
{

    const ATTRIBUTE_SETS = [
        'SILLA DE RUEDAS',
        'SUTURA',
        'COLLAR CERVICAL',
        'BOTA ÓRTESIS',
        'TOALLITA IMPREGNADA ALCOHOL',
        'KIT DE RELLENO DE HERIDAS',
        'ALGODÓN',
        'CURACIÓN DE HERIDAS',
        'KIT DE CURACIÓN',
        'ALCOHOL',
        'RECOLECTOR',
        'CINTA ADHESIVA MÉDICA',
        'VENDA',
        'GASA',
        'APÓSITO',
        'HIDROGEL',
        'VENDA CON YESO',
        'SOLUCIÓN PARA EL LAVADO DE HERIDAS'
    ];

    const PRECIO_MAXIMO_ATTRIBUTE_CODE = 'precio_maximo';

    const PRODUCTO_REGULADO_ATTRIBUTE_CODE = 'producto_regulado';

    const REQUIRED_ATTRIBUTES = [
        'macrozona',
        'marca',
        self::PRECIO_MAXIMO_ATTRIBUTE_CODE
    ];

    /**
     * @param ModuleDataSetupInterface $setup
     * @return void
     */
    public function createAttributeSets(ModuleDataSetupInterface $setup);

    /**
     * @param int $parentAttributeSetId
     * @param string $entityTypeId
     * @param string $attributeSetName
     * @return void
     * @throws \Exception
     */
    public function createAttributeSet($parentAttributeSetId, $entityTypeId, $attributeSetName);

    /**
     * @param AttributeSetModelInterface $attributeSet
     * @param string $attributeCode
     * @param int $sortOrder
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function addAttributeToAttibuteSet(
        AttributeSetModelInterface $attributeSet,
        $attributeCode,
        $sortOrder
    );

    /**
     * @param string $attributeSetName
     * @return AttributeSetModelInterface
     * @throws NoSuchEntityException
     */
    public function getAttributeSetByName($attributeSetName);

    /**
     * @param ModuleDataSetupInterface $setup
     * @param string $attributeName
     * @param array $attributeData
     */
    public function createAttribute(ModuleDataSetupInterface $setup, $attributeName, $attributeData);

    /**
     * @param AttributeSetModelInterface $attributeSet
     * @param string $attributeCode
     * @param int $sortOrder
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function addAttributeIfNotExistsToAttibuteSet(
        AttributeSetModelInterface $attributeSet,
        $attributeCode,
        $sortOrder
    );
}
