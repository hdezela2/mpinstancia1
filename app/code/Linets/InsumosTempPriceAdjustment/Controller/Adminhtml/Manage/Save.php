<?php

namespace Linets\InsumosTempPriceAdjustment\Controller\Adminhtml\Manage;

use Linets\InsumosSetup\Model\InsumosConstants;
use Linets\InsumosTempPriceAdjustment\Helper\Data;
use Magento\Backend\App\Action;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemsFactory;

class Save extends Action implements HttpPostActionInterface
{
    /** @var \Linets\InsumosTempPriceAdjustment\Helper\Data */
    protected $helperData;
    /** @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory */
    private $productCollectionFactory;
    /** @var \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory */
    private $assignItems;
    /** @var \Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory */
    private $assignAssociates;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Linets\InsumosTempPriceAdjustment\Helper\Data $helperData
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory $assignItems
     * @param \Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory $assignAssociates
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        Action\Context        $context,
        Data                  $helperData,
        CollectionFactory     $productCollectionFactory,
        ItemsFactory          $assignItems,
        AssociatesFactory     $assignAssociates,
        StoreManagerInterface $storeManager
    )
    {
        $this->helperData = $helperData;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->assignItems = $assignItems;
        $this->assignAssociates = $assignAssociates;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface
     */
    public function execute()
    {
        try {
            $params = $this->getRequest()->getParams();

            $this->validateParams($params);

            $categories = $params['categories'];
            $percentage = $params['percentage'];
            $products = $this->getProducts($categories);

            $success = 0;
            $error = 0;
            foreach ($products['assignProductsIds'] as $productData) {
                $response = $this->helperData->updateProducts($productData, $percentage);
                if ($response['adjusted']) {
                    $success++;
                } else {
                    $error++;
                }
            }

        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('suppliestemppriceadjustment/suppliespriceadjustment/index');
    }

    /**
     * @param array $params
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validateParams(array $params)
    {
        $maxVal = $this->helperData->getConfig('catalog/medical_supplies_temp_price_adjustment/max_adjustment_percentage');
        if (empty($params['percentage'])) {
            throw new LocalizedException(__('The percentage can not be empty'));
        } elseif (!$this->helperData->validatePercentage($params['percentage'])) {
            throw new LocalizedException(__('Percentage is invalid, please check and try again'));
        } elseif ($params['percentage'] > $maxVal) {
            throw new LocalizedException(__('The maximum percetage allowed to update prices is: %1', $maxVal));
        } elseif (!is_array($params['categories']) || count($params['categories']) == 0) {
            throw new LocalizedException(__('Use dot as decimal separator'));
        }
    }

    /**
     * @param array $categories
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProducts(array $categories): array
    {
        $response = [];
        $response['assignProductsIds'] = [];
        $collection = $this->getCollection(InsumosConstants::STORE_CODE, $categories);

        $allProductsIds = $collection->getAllIds();
        $assignProducts = $this->assignItems->create()
            ->addFieldToFilter('product_id', ['in' => [$allProductsIds]]);

        $i = 0;
        foreach ($assignProducts as $assignProduct) {
            if ($assignProduct->getType() == 'configurable') {
                $associateAssignProducts = $this->assignAssociates->create()
                    ->addFieldToFilter('parent_product_id', $assignProduct->getProductId())
                    ->addFieldToFilter('parent_id', $assignProduct->getId());

                foreach ($associateAssignProducts as $associateAssignProduct) {
                    $response['assignProductsIds'][$i]['id'] = $associateAssignProduct->getId();
                    $response['assignProductsIds'][$i]['child'] = true;
                    $i++;
                }
            } else {
                $response['assignProductsIds'][$i]['id'] = $associateAssignProduct->getId();
                $response['assignProductsIds'][$i]['child'] = false;
                $i++;
            }
        }
        return $response;
    }

    /**
     * @param string $storeCode
     * @param $categories
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|\Magento\Framework\Data\Collection\AbstractDb
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCollection(string $storeCode, $categories)
    {
        $store = $this->storeManager->getStore($storeCode);
        if (!$store->getId()) {
            throw new NoSuchEntityException(__('Store with code %s not found', $storeCode ?? 'EMPTY'));
        }
        $collection = $this->productCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addStoreFilter($store->getId())
            ->addCategoriesFilter(array('in' => $categories))
            ->load();
        if (!$collection || $collection->count() == 0) {
            throw new LocalizedException(__('The products collection to update is empty, please verify the options and try again'));
        }
        return $collection;
    }
}
