<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Controller\Adminhtml\Manage;

use Linets\InsumosTempPriceAdjustment\Cron\RevertPriceAdjustments;
use Linets\InsumosTempPriceAdjustment\Model\Source\Status;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Message\Manager;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Ui\Component\MassAction\Filter;
use Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\CollectionFactory;


class MassCancel extends Action implements HttpPostActionInterface
{

    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Linets_InsumosTempPriceAdjustment::massActions_cancel';

    /**
     * @var string
     */
    protected $redirectUrl = '*/*/';

    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;

    /**
     * @var object
     */
    protected $collectionFactory;
    /**
     * @var \Linets\InsumosTempPriceAdjustment\Cron\RevertPriceAdjustments
     */
    private $cron;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Linets\InsumosTempPriceAdjustment\Cron\RevertPriceAdjustments $cron
     * @param \Magento\Framework\Message\Manager $messageManager
     */
    public function __construct(
        Action\Context         $context,
        Filter                 $filter,
        RevertPriceAdjustments $cron,
        Manager                $messageManager,
        CollectionFactory      $collectionFactory
    )
    {
        parent::__construct($context);
        $this->filter = $filter;
        $this->cron = $cron;
        $this->messageManager = $messageManager;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $collection
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        $this->cron->setCollection($collection);
        $this->cron->setStatus(Status::STATUS_CANCELED);
        $this->cron->execute();
        $this->messageManager->addSuccessMessage(__('The selected temporary price adjustments has been canceled successfully.'));
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('suppliestemppriceadjustment/suppliespriceadjustment/index');
        return $resultRedirect;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        return $this->massAction($collection);
    }

    /**
     * @return string
     */
    private function getComponentRefererUrl(): string
    {
        return $this->filter->getComponentRefererUrl() ?: 'suppliestemppriceadjustment/index/';
    }
}