<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Controller\Adminhtml\Manage;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\View\Result\PageFactory;
use Linets\InsumosTempPriceAdjustment\Helper\Data as HelperData;

class Update extends Action implements HttpPostActionInterface
{

    protected $resultPageFactory;

    protected $helperData;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        Context        $context,
        PageFactory $resultPageFactory,
        HelperData $helperData
    )
    {
        $this->helperData = $helperData;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        try {
            $productData = $this->getRequest()->getParams('product');
            $percentage = $this->getRequest()->getParam('percentage');
            $response = $this->helperData->updateProducts('');



        }catch (\Exception $e){

        }

        return $this->resultPageFactory->create();
    }
}

