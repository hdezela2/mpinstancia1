<?php

namespace Linets\InsumosTempPriceAdjustment\Ui\Component\Listing\Columns;

use Magento\Catalog\Model\ResourceModel\Product;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Macrozone extends Column
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    private $productResourceModel;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResourceModel
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface   $context,
        UiComponentFactory $uiComponentFactory,
        Product            $productResourceModel,
        array              $components = [],
        array              $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->productResourceModel = $productResourceModel;
    }


    public function prepareDataSource(array $dataSource)
    {
        $macrozoneAttributeSource = $this->productResourceModel->getAttribute('macrozona')->getSource();

        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $macrozonaId = $item[$fieldName];
                $item[$fieldName] = $macrozoneAttributeSource->getOptionText($macrozonaId);
            }
        }

        return $dataSource;
    }
}