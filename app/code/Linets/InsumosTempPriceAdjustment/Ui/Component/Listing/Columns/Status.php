<?php

namespace Linets\InsumosTempPriceAdjustment\Ui\Component\Listing\Columns;

use Linets\InsumosTempPriceAdjustment\Model\Source\Status as StatusOptions;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Status extends Column
{
    /**
     * @var \Linets\InsumosTempPriceAdjustment\Model\Source\Status
     */
    private $statusOptions;


    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Linets\InsumosTempPriceAdjustment\Model\Source\Status $statusOptions
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface   $context,
        UiComponentFactory $uiComponentFactory,
        StatusOptions      $statusOptions,
        array              $components = [],
        array              $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->statusOptions = $statusOptions;
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$fieldName] = $this->statusOptions->getOptionText($item[$fieldName]) ?? 'NO DEFINIDO';
            }
        }

        return $dataSource;
    }
}