<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Console\Command;

use Linets\InsumosTempPriceAdjustment\Cron\RevertPriceAdjustments as CronRevertPriceAdjustments;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RevertPriceAdjustments extends Command
{
    /**
     * @var \Linets\InsumosTempPriceAdjustment\Cron\RevertPriceAdjustments
     */
    private $cron;
    /**
     * @var JsonSerializer
     */
    private $serializer;

    /**
     * @param \Linets\InsumosTempPriceAdjustment\Cron\RevertPriceAdjustments $cron
     * @param JsonSerializer $serializer
     * @param string|null $name
     */
    public function __construct(
        CronRevertPriceAdjustments $cron,
        JsonSerializer             $serializer,
        string                     $name = null
    )
    {
        parent::__construct($name);
        $this->cron = $cron;
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $output->writeln("<info>Running price adjustment expiration process...</info>>");
        $this->cron->execute();
        $errors = $this->cron->getErrors();
        if (count($errors) > 0) {
            $output->writeln("<comment>The following transactions had errors:</comment>");
            foreach ($errors as $error) {
                $output->writeln('<error>'.$error['message'] . '</error>: ' . $this->serializer->serialize($error['context']));
            }
        }
        $output->writeln("<info>Finished</info>");
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("medsupplies:temp-adjustment:revert");
        $this->setDescription("Run this command to revert expired price adjustments");
        $this->setDefinition([]);
        parent::configure();
    }
}
