# Mage2 Module Linets InsumosTempPriceAdjustment

    ``linets/module-insumostemppriceadjustment``

- [Main Functionalities](#markdown-header-main-functionalities)
- [Installation](#markdown-header-installation)
- [Configuration](#markdown-header-configuration)
- [Specifications](#markdown-header-specifications)
- [Attributes](#markdown-header-attributes)

## Main Functionalities

Module to allow the admin users to perform a price adjustment for 3 months for the CM Medical Supplies

## Installation

\* = in production please use the `--keep-generated` option

### Type 1: Zip file

- Unzip the zip file in `app/code/Linets`
- Enable the module by running `php bin/magento module:enable Linets_InsumosTempPriceAdjustment`
- Apply database updates by running `php bin/magento setup:upgrade`\*
- Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

- Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
- Add the composer repository to the configuration by
  running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
- Install the module composer by running `composer require linets/module-insumostemppriceadjustment`
- enable the module by running `php bin/magento module:enable Linets_InsumosTempPriceAdjustment`
- apply database updates by running `php bin/magento setup:upgrade`\*
- Flush the cache by running `php bin/magento cache:flush`

## Configuration

- Revert Temp Adjustment Prices Schedule (cron_job_schedules/medical_supplies/revert_temp_prices_schedule)

- Adjustment Percentage (catalog/medical_supplies/adjustment_percentage)

## Specifications

- Console Command
    - RevertPriceAdjustments

- Controller
    - adminhtml > suppliestemppriceadjustment/manage/create

- Controller
    - adminhtml > suppliestemppriceadjustment/manage/update

- Controller
    - adminhtml > suppliestemppriceadjustment/manage/run

- Controller
    - adminhtml > suppliestemppriceadjustment/massActions/cancel

- Cronjob
    - linets_insumostemppriceadjustment_revertpriceadjustments

## Attributes



