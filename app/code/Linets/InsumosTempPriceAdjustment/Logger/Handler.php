<?php

namespace Linets\InsumosTempPriceAdjustment\Logger;

use Linets\InsumosTempPriceAdjustment\Logger\Logger;
use Magento\Framework\Logger\Handler\Base;

class Handler extends Base
{
    /** @var int  */
    protected $loggerType = Logger::INFO;

    /** @var string  */
    protected $fileName = '/var/log/supplies_temp_price_adjustment.log';
}