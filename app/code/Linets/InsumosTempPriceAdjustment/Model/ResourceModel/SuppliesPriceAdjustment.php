<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class SuppliesPriceAdjustment extends AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('dccp_supplies_price_adjustment', 'suppliespriceadjustment_id');
    }
}

