<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment;

use Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'suppliespriceadjustment_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Linets\InsumosTempPriceAdjustment\Model\SuppliesPriceAdjustment::class,
            SuppliesPriceAdjustment::class
        );
    }
}

