<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Model;

use Exception;
use Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface;
use Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterfaceFactory;
use Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentSearchResultsInterfaceFactory;
use Linets\InsumosTempPriceAdjustment\Api\SuppliesPriceAdjustmentRepositoryInterface;
use Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment as ResourceSuppliesPriceAdjustment;
use Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\CollectionFactory as SuppliesPriceAdjustmentCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class SuppliesPriceAdjustmentRepository implements SuppliesPriceAdjustmentRepositoryInterface
{

    protected $dataSuppliesPriceAdjustmentFactory;

    protected $suppliesPriceAdjustmentCollectionFactory;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;
    protected $dataObjectHelper;
    protected $dataObjectProcessor;
    protected $extensionAttributesJoinProcessor;
    protected $suppliesPriceAdjustmentFactory;
    private $storeManager;
    private $collectionProcessor;

    /**
     * @param ResourceSuppliesPriceAdjustment $resource
     * @param SuppliesPriceAdjustmentFactory $suppliesPriceAdjustmentFactory
     * @param SuppliesPriceAdjustmentInterfaceFactory $dataSuppliesPriceAdjustmentFactory
     * @param SuppliesPriceAdjustmentCollectionFactory $suppliesPriceAdjustmentCollectionFactory
     * @param SuppliesPriceAdjustmentSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceSuppliesPriceAdjustment                      $resource,
        SuppliesPriceAdjustmentFactory                       $suppliesPriceAdjustmentFactory,
        SuppliesPriceAdjustmentInterfaceFactory              $dataSuppliesPriceAdjustmentFactory,
        SuppliesPriceAdjustmentCollectionFactory             $suppliesPriceAdjustmentCollectionFactory,
        SuppliesPriceAdjustmentSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper                                     $dataObjectHelper,
        DataObjectProcessor                                  $dataObjectProcessor,
        StoreManagerInterface                                $storeManager,
        CollectionProcessorInterface                         $collectionProcessor,
        JoinProcessorInterface                               $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter                        $extensibleDataObjectConverter
    )
    {
        $this->resource = $resource;
        $this->suppliesPriceAdjustmentFactory = $suppliesPriceAdjustmentFactory;
        $this->suppliesPriceAdjustmentCollectionFactory = $suppliesPriceAdjustmentCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataSuppliesPriceAdjustmentFactory = $dataSuppliesPriceAdjustmentFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        SuppliesPriceAdjustmentInterface $suppliesPriceAdjustment
    )
    {
        /* if (empty($suppliesPriceAdjustment->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $suppliesPriceAdjustment->setStoreId($storeId);
        } */

        $suppliesPriceAdjustmentData = $this->extensibleDataObjectConverter->toNestedArray(
            $suppliesPriceAdjustment,
            [],
            SuppliesPriceAdjustmentInterface::class
        );

        $suppliesPriceAdjustmentModel = $this->suppliesPriceAdjustmentFactory->create()->setData($suppliesPriceAdjustmentData);

        try {
            $this->resource->save($suppliesPriceAdjustmentModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the suppliesPriceAdjustment: %1',
                $exception->getMessage()
            ));
        }
        return $suppliesPriceAdjustmentModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    )
    {
        $collection = $this->suppliesPriceAdjustmentCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            SuppliesPriceAdjustmentInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($suppliesPriceAdjustmentId)
    {
        return $this->delete($this->get($suppliesPriceAdjustmentId));
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        SuppliesPriceAdjustmentInterface $suppliesPriceAdjustment
    )
    {
        try {
            $suppliesPriceAdjustmentModel = $this->suppliesPriceAdjustmentFactory->create();
            $this->resource->load($suppliesPriceAdjustmentModel, $suppliesPriceAdjustment->getSuppliespriceadjustmentId());
            $this->resource->delete($suppliesPriceAdjustmentModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the SuppliesPriceAdjustment: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function get($suppliesPriceAdjustmentId)
    {
        $suppliesPriceAdjustment = $this->suppliesPriceAdjustmentFactory->create();
        $this->resource->load($suppliesPriceAdjustment, $suppliesPriceAdjustmentId);
        if (!$suppliesPriceAdjustment->getId()) {
            throw new NoSuchEntityException(__('SuppliesPriceAdjustment with id "%1" does not exist.', $suppliesPriceAdjustmentId));
        }
        return $suppliesPriceAdjustment->getDataModel();
    }
}

