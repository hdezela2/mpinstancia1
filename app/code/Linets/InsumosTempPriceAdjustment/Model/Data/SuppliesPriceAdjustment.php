<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Model\Data;

use Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentExtensionInterface;
use Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface;
use Magento\Framework\Api\AbstractExtensibleObject;

class SuppliesPriceAdjustment extends AbstractExtensibleObject implements SuppliesPriceAdjustmentInterface
{

    /**
     * Get suppliespriceadjustment_id
     * @return string|null
     */
    public function getSuppliespriceadjustmentId()
    {
        return $this->_get(self::SUPPLIESPRICEADJUSTMENT_ID);
    }

    /**
     * Set suppliespriceadjustment_id
     * @param string $suppliespriceadjustmentId
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setSuppliespriceadjustmentId($suppliespriceadjustmentId)
    {
        return $this->setData(self::SUPPLIESPRICEADJUSTMENT_ID, $suppliespriceadjustmentId);
    }

    /**
     * Get seller_id
     * @return int
     */
    public function getSellerId()
    {
        return $this->_get(self::SELLER_ID);
    }

    /**
     * Set seller_id
     * @param int $sellerId
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setSellerId($sellerId)
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        SuppliesPriceAdjustmentExtensionInterface $extensionAttributes
    )
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get category_id
     * @return string|null
     */
    public function getCategories()
    {
        return $this->_get(self::CATEGORIES);
    }

    /**
     * Set category_id
     * @param string $categories
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setCategories($categories)
    {
        return $this->setData(self::CATEGORIES, $categories);
    }

    /**
     * Get product_id
     * @return string|null
     */
    public function getProductId()
    {
        return $this->_get(self::PRODUCT_ID);
    }

    /**
     * Set product_id
     * @param string $productId
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * Get previous_price
     * @return string|null
     */
    public function getPreviousPrice()
    {
        return $this->_get(self::PREVIOUS_PRICE);
    }

    /**
     * Set previous_price
     * @param string $previousPrice
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setPreviousPrice($previousPrice)
    {
        return $this->setData(self::PREVIOUS_PRICE, $previousPrice);
    }

    /**
     * Get updated_price
     * @return string|null
     */
    public function getUpdatedPrice()
    {
        return $this->_get(self::UPDATED_PRICE);
    }

    /**
     * Set updated_price
     * @param string $updatedPrice
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setUpdatedPrice($updatedPrice)
    {
        return $this->setData(self::UPDATED_PRICE, $updatedPrice);
    }

    /**
     * Get percentage
     * @return string|null
     */
    public function getPercentage()
    {
        return $this->_get(self::PERCENTAGE);
    }

    /**
     * Set percentage
     * @param string $percentage
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setPercentage($percentage)
    {
        return $this->setData(self::PERCENTAGE, $percentage);
    }

    /**
     * Get status
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * @return mixed|string|null
     */
    public function getAssociateProductId()
    {
        return $this->_get(self::ASSOCIATE_PRODUCT_ID);
    }

    /**
     * @param $associateProductId
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface|\Linets\InsumosTempPriceAdjustment\Model\Data\SuppliesPriceAdjustment
     */
    public function setAssociateProductId($associateProductId)
    {
        return $this->setData(self::ASSOCIATE_PRODUCT_ID, $associateProductId);
    }

    /**
     * @return mixed|string|null
     */
    public function getExpiresAt()
    {
        return $this->_get(self::EXPIRES_AT);
    }

    /**
     * @param string $expiresAt
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface|\Linets\InsumosTempPriceAdjustment\Model\Data\SuppliesPriceAdjustment
     */
    public function setExpiresAt($expiresAt)
    {
        return $this->setData(self::EXPIRES_AT, $expiresAt);
    }
}

