<?php

namespace Linets\InsumosTempPriceAdjustment\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;

class Status extends AbstractSource implements SourceInterface, OptionSourceInterface
{

    const STATUS_ACTIVE = 'active';
    const STATUS_CANCELED = 'canceled';
    const STATUS_EXPIRED = 'expired';

    protected $options;
    /**
     * Options constructor.
     */
    public function __construct()
    {
        $this->options = [
            self::STATUS_ACTIVE => (string)__('Active'),
            self::STATUS_CANCELED => (string)__('Manually Canceled'),
            self::STATUS_EXPIRED => (string)__('Expired'),
        ];
    }
    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function getOptionArray()
    {
        return $this->options;
    }

    /**
     * @return array|string[]|null
     */
    public function getAllOptions()
    {
        $allOptions = [];
        foreach ($this->options as $k => $v) {
            $allOptions[] = [
                'value' => $k,
                'label' => $v
            ];
        }
        return $allOptions;
    }
}