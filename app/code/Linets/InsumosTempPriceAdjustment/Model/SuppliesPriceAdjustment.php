<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Model;

use Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface;
use Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterfaceFactory;
use Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class SuppliesPriceAdjustment extends AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'linets_insumostemppriceadjustment_suppliespriceadjustment';
    protected $suppliespriceadjustmentDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param SuppliesPriceAdjustmentInterfaceFactory $suppliespriceadjustmentDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment $resource
     * @param \Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context        $context,
        Registry             $registry,
        SuppliesPriceAdjustmentInterfaceFactory $suppliespriceadjustmentDataFactory,
        DataObjectHelper                        $dataObjectHelper,
        ResourceModel\SuppliesPriceAdjustment   $resource,
        Collection                              $resourceCollection,
        array                                   $data = []
    )
    {
        $this->suppliespriceadjustmentDataFactory = $suppliespriceadjustmentDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve suppliespriceadjustment model with suppliespriceadjustment data
     * @return SuppliesPriceAdjustmentInterface
     */
    public function getDataModel()
    {
        $suppliespriceadjustmentData = $this->getData();

        $suppliespriceadjustmentDataObject = $this->suppliespriceadjustmentDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $suppliespriceadjustmentDataObject,
            $suppliespriceadjustmentData,
            SuppliesPriceAdjustmentInterface::class
        );

        return $suppliespriceadjustmentDataObject;
    }
}

