<?php

namespace Linets\InsumosTempPriceAdjustment\Model\Config;

use Linets\InsumosTempPriceAdjustment\Helper\Data;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Zend_Validate_Exception;

class Percentage extends Value
{
    /**
     * @var \Linets\InsumosTempPriceAdjustment\Helper\Data
     */
    private $helper;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Linets\InsumosTempPriceAdjustment\Helper\Data $helper
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context              $context,
        Registry             $registry,
        ScopeConfigInterface $config,
        TypeListInterface    $cacheTypeList,
        Data                 $helper,
        AbstractResource     $resource = null,
        AbstractDb           $resourceCollection = null,
        array                $data = []
    )
    {
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
        $this->helper = $helper;
    }

    /**
     * @return $this
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function beforeSave(): Percentage
    {
        $value = $this->getValue();
        $valid = $this->helper->validatePercentage($value);
        if (!$valid) {
            $message = __('Invalid number format, e.g.: 3.55 | -3.05 | 5');
            throw new LocalizedException($message);
        }

        return $this;
    }
}