<?php


namespace Linets\InsumosTempPriceAdjustment\Setup\Patch\Data;


use Linets\InsumosSetup\Model\InsumosConstants;
use Linets\InsumosSetup\Setup\Patch\Data\UserRoleSetup;
use Linets\SellerCentralizedDispatchCost\Setup\Patch\Data\UserRoleUpdate;
use Magento\Authorization\Model\ResourceModel\Role\Grid\CollectionFactory as RolesResourceModel;
use Magento\Authorization\Model\Role;
use Magento\Authorization\Model\RulesFactory;
use Magento\Framework\Setup\Patch\DataPatchInterface;


class UpdatePermissionsToRole implements DataPatchInterface
{

    protected $rulesFactory;

    protected $roleFactory;

    protected $roleResource;

    public function __construct(
        RulesFactory $rulesFactory,
        Role $roleFactory,
        RolesResourceModel $roleResource
    )
    {
        $this->rulesFactory = $rulesFactory;
        $this->roleFactory = $roleFactory;
        $this->roleResource = $roleResource;

    }


    public function apply()
    {
        $agreementId = InsumosConstants::ID_AGREEMENT;
        $roles = $this->roleResource->create();

        foreach ($roles->getData() as $role) {
            $acls = [
                'Linets_InsumosTempPriceAdjustment::manage_create',
                'Linets_InsumosTempPriceAdjustment::manage_update',
                'Linets_InsumosTempPriceAdjustment::manage_run',
                'Linets_InsumosTempPriceAdjustment::massActions_cancel',
                'Linets_InsumosTempPriceAdjustment::SuppliesPriceAdjustment_view',
                'Linets_InsumosTempPriceAdjustment::suppliestemppriceadjustment',

            ];
            $resources = array_merge(UserRoleSetup::ACL_RESOURCES, UserRoleUpdate::ACL_RESOURCES, $acls);

            if ($role['role_name'] == $agreementId) {
                $this->rulesFactory->create()
                    ->setRoleId($role['role_id'])
                    ->setResources($resources)
                    ->saveRel();
            }
        }

    }

    public static function getDependencies()
    {
        return [
            UserRoleUpdate::class
        ];
    }

    public function getAliases()
    {
        return [];
    }
}