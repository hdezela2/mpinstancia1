<?php

namespace Linets\InsumosTempPriceAdjustment\Block\Adminhtml\Manage\Create;

use Magento\Backend\Block\Widget\Context;

abstract class GenericButton
{
    /** @var \Magento\Backend\Block\Widget\Context */
    protected $context;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Generate url by route and parameters
     *
     * @param string $route
     * @param array $params
     * @return  string
     */
    public function getUrl(string $route = '', array $params = []): string
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}