<?php

namespace Linets\InsumosTempPriceAdjustment\Block\Adminhtml\Manage\Create;

class SaveButton extends GenericButton implements \Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface
{


    /** {@inherindoc} */
    public function getButtonData()
    {
        return [
            'label' => __('Run Update'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}