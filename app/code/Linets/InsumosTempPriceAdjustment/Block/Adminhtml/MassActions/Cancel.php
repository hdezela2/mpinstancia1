<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Block\Adminhtml\MassActions;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;

class Cancel extends Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array                                   $data = []
    )
    {
        parent::__construct($context, $data);
    }
}

