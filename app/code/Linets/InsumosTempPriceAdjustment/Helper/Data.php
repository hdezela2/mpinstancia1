<?php

namespace Linets\InsumosTempPriceAdjustment\Helper;

use Formax\Offer\Model\Offer as OfferModel;
use Linets\InsumosSetup\Model\InsumosConstants;
use Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\CollectionFactory as SuppliesPriceAdjustmentCollectionFactory;
use Linets\InsumosTempPriceAdjustment\Model\SuppliesPriceAdjustmentFactory;
use Linets\PriceAdjustmentRequest\Helper\ApproveOffer;
use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory as ProductFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Store\Api\StoreRepositoryInterface;
use Webkul\MpAssignProduct\Model\AssociatesFactory as AssociatesFactory;
use Webkul\MpAssignProduct\Model\ItemsFactory as ItemsFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollectionFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemsCollectionFactory;

class Data extends AbstractHelper
{
    /**
     * @var \Magento\Store\Api\StoreRepositoryInterface
     */
    protected $storeRepositoryManager;
    /**
     * @var AssociatesFactory
     */
    private $associatesFactory;

    /**
     * @var ItemsFactory
     */
    private $itemsFactory;

    /**
     * @var AssociatesCollectionFactory
     */
    private $associatesCollectionFactory;

    /**
     * @var ItemsCollectionFactory
     */
    private $itemsCollectionFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var SuppliesPriceAdjustmentFactory
     */
    private $suppliesPriceAdjustment;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var SuppliesPriceAdjustmentCollectionFactory
     */
    protected $suppliesPriceAdjustmentCollectionFactory;

    /**
     * @var DispatchPriceManagementInterface
     */
    protected $dispatchPriceManagement;
    /**
     * @var \Linets\PriceAdjustmentRequest\Helper\ApproveOffer
     */
    private $approveOfferHelper;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTime;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Webkul\MpAssignProduct\Model\AssociatesFactory $associatesFactory
     * @param \Magento\Store\Api\StoreRepositoryInterface $storeRepositoryManager
     * @param \Webkul\MpAssignProduct\Model\ItemsFactory $itemsFactory
     * @param \Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory $associatesCollectionFactory
     * @param \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory $itemsCollectionFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Linets\InsumosTempPriceAdjustment\Model\SuppliesPriceAdjustmentFactory $suppliesPriceAdjustment
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\CollectionFactory $suppliesPriceAdjustmentCollectionFactory
     * @param \Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface $dispatchPriceManagement
     * @param \Linets\PriceAdjustmentRequest\Helper\ApproveOffer $approveOfferHelper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        Context                                  $context,
        AssociatesFactory                        $associatesFactory,
        StoreRepositoryInterface                 $storeRepositoryManager,
        ItemsFactory                             $itemsFactory,
        AssociatesCollectionFactory              $associatesCollectionFactory,
        ItemsCollectionFactory                   $itemsCollectionFactory,
        ProductRepositoryInterface               $productRepository,
        SuppliesPriceAdjustmentFactory           $suppliesPriceAdjustment,
        ProductFactory                           $productFactory,
        SuppliesPriceAdjustmentCollectionFactory $suppliesPriceAdjustmentCollectionFactory,
        DispatchPriceManagementInterface         $dispatchPriceManagement,
        ApproveOffer                             $approveOfferHelper,
        DateTime                                 $dateTime,
        ResourceConnection                       $resourceConnection

    )
    {
        $this->associatesFactory = $associatesFactory;
        $this->storeRepositoryManager = $storeRepositoryManager;
        $this->itemsFactory = $itemsFactory;
        $this->associatesCollectionFactory = $associatesCollectionFactory;
        $this->itemsCollectionFactory = $itemsCollectionFactory;
        $this->productRepository = $productRepository;
        $this->suppliesPriceAdjustment = $suppliesPriceAdjustment;
        $this->productFactory = $productFactory;
        $this->suppliesPriceAdjustmentCollectionFactory = $suppliesPriceAdjustmentCollectionFactory;
        $this->dispatchPriceManagement = $dispatchPriceManagement;
        $this->dateTime = $dateTime;
        $this->resourceConnection = $resourceConnection;
        $this->approveOfferHelper = $approveOfferHelper;
        parent::__construct($context);
    }

    /**
     * @param $productData
     * @param $percentage
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateProducts($productData, $percentage)
    {
        if ($productData['child']) {
            return $this->updateProductConfigurable($productData, $percentage);
        } else {
            $response['msg'] = "Deben ser productos configurable";
            return $response;
        }
    }

    /**
     * @param $producData
     * @param $percentage
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function updateProductConfigurable($producData, $percentage): array
    {
        $response = [];
        //calculate expire date
        $dateExpire = $this->dateTime->gmtDate('Y-m-d H:i:s', strtotime('+3 month'));

        $assignAssociateProducts = $this->associatesCollectionFactory->create()
            ->addFilter('id', $producData['id']);

        //Iteration of associated products
        foreach ($assignAssociateProducts as $assignAssociateProduct) {
            //Data to get product associated
            $product = $this->productFactory->create()->load($assignAssociateProduct->getProductId());
            $attributeSetId = $product->getAttributeSetId();
            $websiteId = $this->storeRepositoryManager->get(InsumosConstants::WEBSITE_CODE)->getId();
            $macrozone = $product->getAttributeText('macrozona');

            $assignItem = $this->itemsFactory->create()->load($assignAssociateProduct['parent_id']);
            $dispatchPrice = $this->dispatchPriceManagement->getDispatchPrice($assignItem->getSellerId(), $macrozone, $attributeSetId, $websiteId);
            $resultDispatchPrice = (int)($dispatchPrice ? $dispatchPrice->getPrice() : 0);

            $suppliesAdjustment['seller_id'] = $assignItem->getSellerId();
            $suppliesAdjustment['categories'] = $product->getCategoryIds()[1];
            $suppliesAdjustment['product_id'] = $assignAssociateProduct->getProductId();
            $suppliesAdjustment['associate_product_id'] = $assignAssociateProduct->getId();
            $suppliesAdjustment['percentage'] = $percentage;
            $suppliesAdjustment['status'] = "active";
            $suppliesAdjustment['expires_at'] = $dateExpire;

            $isOffer = $this->getFromDccpOffer('status', $assignAssociateProduct->getProductId(), $assignAssociateProduct['parent_id']);

            if ($isOffer) {
                $resourceConnection = $this->resourceConnection->getConnection();
                $basePrice = $this->getFromDccpOffer('base_price', $assignAssociateProduct->getProductId(), $assignAssociateProduct['parent_id']);
                $calculatedPrices = $basePrice > 0 ? $this->calculatePrice($basePrice, $resultDispatchPrice, $percentage, true) : 0;

                $suppliesAdjustment['previous_price'] = $basePrice;
                $suppliesAdjustment['updated_price'] = $calculatedPrices['updatedPrice'];

                $resourceConnection->beginTransaction();
                $update_offer = 'UPDATE  dccp_offer_product set base_price = "' . $calculatedPrices['updatedPrice'] . '" where product_id = ' . (int)$assignAssociateProduct->getProductId() . ' and status in ('.OfferModel::OFFER_APPLIED.', '.offerModel::OFFER_CREATED.') and assign_id = ' . (int)$assignItem->getId();
                $resourceConnection->query($update_offer);
                $resourceConnection->commit();
            } else {

                $calculatedPrices = $assignAssociateProduct->getBasePrice() > 0 ? $this->calculatePrice($assignAssociateProduct->getBasePrice(), $resultDispatchPrice, $percentage) : 0;
                $suppliesAdjustment['previous_price'] = $assignAssociateProduct->getPrice();
                $suppliesAdjustment['updated_price'] = $calculatedPrices['updatedPrice'];
            }

            $response = $this->saveSuppliesAdjustment($suppliesAdjustment);

            if ($response['adjusted'] && !$isOffer) {
                $this->saveAssignProduct($assignAssociateProduct, $calculatedPrices);
            }
        }

        return $response;
    }

    /**
     * @param $productData
     * @param $percentage
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateProductSimple($productData, $percentage): array
    {
        $response = [];
        $dateExpire = $this->dateTime->gmtDate('Y-m-d H:i:s', strtotime('+3 month'));
        $resourceConnection = $this->resourceConnection->getConnection();
        $assignItem = $this->itemsFactory->create()->load($productData['id']);
        $product = $this->productFactory->create()->load($assignItem->getProductId());

        $select = $resourceConnection->select();
        $select->from('dccp_offer_product', ['status']);
        $select->where('product_id = ' . $assignItem->getProductId());
        $select->where('start_date <= DATE(NOW())');
        $select->where('end_date >= DATE(NOW())');
        $select->where('status = ' . OfferModel::OFFER_APPLIED);
        $is_offer = $resourceConnection->fetchOne($select);

        $attributeSetId = $product->getAttributeSetId();
        $websiteId = $this->storeRepositoryManager->get(InsumosConstants::WEBSITE_CODE)->getId();
        $macrozone = $product->getAttributeText('macrozona');
        $dispatchPrice = $this->dispatchPriceManagement->getDispatchPrice($assignItem->getSellerId(), $macrozone, $attributeSetId, $websiteId);
        $resultDispatchPrice = (int)($dispatchPrice ? $dispatchPrice->getPrice() : 0);

        $suppliesAdjustment['seller_id'] = $assignItem->getSellerId();
        $suppliesAdjustment['categories'] = $product->getCategoryIds()[1];
        $suppliesAdjustment['product_id'] = $assignItem->getProductId();
        $suppliesAdjustment['associate_product_id'] = "";
        $suppliesAdjustment['previous_price'] = $assignItem->getPrice();

        $suppliesAdjustment['percentage'] = $percentage;
        $suppliesAdjustment['status'] = "active";
        $suppliesAdjustment['expires_at'] = $dateExpire;

        if ($is_offer) {
            $select = $resourceConnection->select();
            $select->from('dccp_offer_product', ['base_price']);
            $select->where('product_id = ' . $assignItem->getProductId());
            $select->where('start_date <= DATE(NOW())');
            $select->where('end_date >= DATE(NOW())');
            $select->where('status = ' . OfferModel::OFFER_APPLIED);
            $basePrice = $resourceConnection->fetchOne($select);

            $calculatedPrices = $basePrice > 0 ? $this->calculatePrice($basePrice, $resultDispatchPrice, $percentage) : 0;
            $suppliesAdjustment['previous_price'] = $basePrice;
            $suppliesAdjustment['updated_price'] = $calculatedPrices['updatedPrice'];
            $resourceConnection->beginTransaction();
            $update_offer = 'UPDATE  dccp_offer_product set base_price = "' . $calculatedPrices['percentagePrice'] . '" where product_id = ' . (int)$assignItem->getProductId() . ' and status = ' . OfferModel::OFFER_APPLIED;
            $resourceConnection->query($update_offer);
            $resourceConnection->commit();
        } else {
            $calculatedPrices = $assignItem->getBasePrice() > 0 ? $this->calculatePrice($assignItem->getBasePrice(), $resultDispatchPrice, $percentage) : 0;
            $suppliesAdjustment['updated_price'] = $calculatedPrices['updatedPrice'];
            $suppliesAdjustment['previous_price'] = $assignItem->getPrice();
        }

        $response = $this->saveSuppliesAdjustment($suppliesAdjustment);
        if ($response['adjusted']) {
            $this->saveAssignProduct($assignItem, $calculatedPrices);
        }
        return $response;

    }

    /**
     * @param $priceBase
     * @param $priceDispactch
     * @param $percentage
     * @return array
     */
    public function calculatePrice($priceBase, $priceDispactch, $percentage, $isOffer = false)
    {
        $_percentage = ($percentage > 0 ? $percentage : (-1) * $percentage);
        // IsOffer  we have to subtract the shipping price to get the percentage and then add it again
        $priceBase = $isOffer ? $priceBase - $priceDispactch : $priceBase;
        $percentagePrice = (float)(($priceBase * $_percentage) / 100);
        $updatedPrice = round($percentage > 0 ? ($priceBase + $percentagePrice + $priceDispactch) : ($priceBase - $percentagePrice + $priceDispactch), 0, PHP_ROUND_HALF_DOWN);

        $response['updatedPrice'] = round($updatedPrice, 0, PHP_ROUND_HALF_DOWN);
        $response['percentagePrice'] = round($percentagePrice + $priceBase, 0, PHP_ROUND_HALF_DOWN);

        return $response;
    }

    /**
     * @param $assignProduct
     * @param $calculatedPrices
     */
    public function saveAssignProduct($assignProduct, $calculatedPrices)
    {
        $assignProduct->setPrice($calculatedPrices['updatedPrice']);
        $assignProduct->setBasePrice($calculatedPrices['percentagePrice']);
        $assignProduct->save();
    }

    /**
     * @param $sellerId
     * @param $productId
     * @param $status
     * @return bool
     */
    public function isActiveSuppliesAdjustment($sellerId, $productId, $status)
    {
        $collectionSupplies = $this->suppliesPriceAdjustmentCollectionFactory->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('status', $status);

        return $collectionSupplies->getSize() > 0;
    }

    /**
     * @param array $suppliesData
     * @return array
     * @throws \Exception
     */
    public function saveSuppliesAdjustment(array $suppliesData): array
    {
        $collectionSupplies = $this->suppliesPriceAdjustmentCollectionFactory->create()
            ->addFieldToFilter('seller_id', $suppliesData['seller_id'])
            ->addFieldToFilter('product_id', $suppliesData['product_id'])
            ->addFieldToFilter('status', $suppliesData['status']);
        //$isActiveSuppliesAdjustment = $this->isActiveSuppliesAdjustment($suppliesData['seller_id'],$suppliesData['product_id'],$suppliesData['status']);

        if ($collectionSupplies->getSize() < 1) {
            $suppliesModel = $this->suppliesPriceAdjustment->create();
            $suppliesModel->setData($suppliesData);
            $suppliesModel->save();
            $response['msg'] = "Adjusted Supply Price";
            $response['adjusted'] = true;
        } else {
            $response['msg'] = "Supply price has already registered an adjustment";
            $response['adjusted'] = false;
        }

        return $response;
    }

    /**
     * @param $field
     * @param $productId
     * @param $assignId
     * @param false $fetchAll
     * @return array|string
     */
    public function getFromDccpOffer($field, $productId, $assignId, $fetchAll = false)
    {
        $resourceConnection = $this->resourceConnection->getConnection();
        $select = $resourceConnection->select();
        $select->from('dccp_offer_product', $field);
        $select->where('product_id = ' . $productId);
        $select->where('assign_id = ' . $assignId);
        $select->where('status in ('.OfferModel::OFFER_APPLIED.', '.offerModel::OFFER_CREATED.')');
        if ($fetchAll) {
            $result = $resourceConnection->fetchAll($select);
        } else {
            $result = $resourceConnection->fetchOne($select);
        }

        return $result;
    }

    /**
     * @param string $percentage
     * @return bool
     */
    public function validatePercentage(string $percentage): bool
    {
        $validator = new \Zend_Validate_Regex('/^[-]?[0-9]{1,2}([.][0-9]{1,2})?$/');
        return $validator->isValid($percentage);
    }

    /**
     * @param string $path
     * @return mixed
     */
    public function getConfig(string $path)
    {
        return $this->scopeConfig->getValue($path);
    }

}