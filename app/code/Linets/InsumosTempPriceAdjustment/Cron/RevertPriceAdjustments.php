<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Cron;

use Linets\InsumosSetup\Model\InsumosConstants;
use Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface;
use Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment as SuppliesPriceAdjustmentResource;
use Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\Collection;
use Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\CollectionFactory as SuppliesPriceAdjustmentCollectionFactory;
use Linets\InsumosTempPriceAdjustment\Model\Source\Status;
use Linets\InsumosTempPriceAdjustment\Model\SuppliesPriceAdjustment;
use Linets\PriceAdjustmentRequest\Helper\ApproveOffer;
use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Webkul\MpAssignProduct\Model\AssociatesFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates as AssociatesResourceModel;

class RevertPriceAdjustments
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment
     */
    private $suppliesPriceAdjustmentResource;
    /**
     * @var \Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\Collection
     */
    private $collection;
    /**
     * @var \Linets\PriceAdjustmentRequest\Helper\ApproveOffer
     */
    private $approveOffer;
    /**
     * @var \Webkul\MpAssignProduct\Model\ResourceModel\Associates
     */
    private $associatesResource;
    /**
     * @var \Webkul\MpAssignProduct\Model\AssociatesFactory
     */
    private $associatesFactory;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productFactory;
    /**
     * @var \Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface
     */
    private $dispatchPriceManagement;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var string
     */
    protected $status = Status::STATUS_EXPIRED;

    protected $errors = [];

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\CollectionFactory $collectionFactory
     * @param \Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment $suppliesPriceAdjustmentResource
     * @param \Linets\PriceAdjustmentRequest\Helper\ApproveOffer $approveOffer
     * @param \Webkul\MpAssignProduct\Model\ResourceModel\Associates $associatesResource
     * @param \Webkul\MpAssignProduct\Model\AssociatesFactory $associatesFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface $dispatchPriceManagement
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        LoggerInterface                          $logger,
        SuppliesPriceAdjustmentCollectionFactory $collectionFactory,
        SuppliesPriceAdjustmentResource          $suppliesPriceAdjustmentResource,
        ApproveOffer                             $approveOffer,
        AssociatesResourceModel                  $associatesResource,
        AssociatesFactory                        $associatesFactory,
        ProductFactory                           $productFactory,
        DispatchPriceManagementInterface         $dispatchPriceManagement,
        StoreManagerInterface                    $storeManager
    )
    {
        $this->logger = $logger;
        $this->collectionFactory = $collectionFactory;
        $this->suppliesPriceAdjustmentResource = $suppliesPriceAdjustmentResource;
        $this->approveOffer = $approveOffer;
        $this->associatesResource = $associatesResource;
        $this->associatesFactory = $associatesFactory;
        $this->productFactory = $productFactory;
        $this->dispatchPriceManagement = $dispatchPriceManagement;
        $this->storeManager = $storeManager;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $collection = $this->getColection();
        /** @var \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface $item */
        foreach ($collection as $item) {
            try {
                if (!$item->getAssociateProductId()) {
                    throw new LocalizedException(__('The Item ' . $item->getSuppliespriceadjustmentId() . ' has no associated offer to select'));
                }
                $offer = $this->associatesFactory->create();
                $this->associatesResource->load($offer, $item->getAssociateProductId());
                $product = $this->productFactory->create()->load($item->getProductId());

                //Calculate previous  Prices and update
                $prices = $this->calculatePreviousPrices($item, $product);
                $offer->setPrice($prices['price']);
                $offer->setBasePrice($prices['base_price']);
                $this->associatesResource->save($offer);

                // Update Adjustment Item status, save and continue
                $item->setStatus($this->getStatus());
                $this->suppliesPriceAdjustmentResource->save($item);
                $this->logger->info("Reverted Price ", [
                    $item->getSellerId(),
                    $item->getAssociateProductId(),
                    $product->getName()
                ]);
//                $agreementIdConvenioInusmos = InsumosConstants::ID_AGREEMENT;
                //Sync updated price to MP
//                $this->approveOffer->syncApprovedOffer($item->getSellerId(), $product->getSku(), $offer);
            } catch (\Exception $e) {
                // If there is and error, Execution won't stop, it will be logged to var/log/supplies_temp_price_adjustment.log
                $this->addError($e->getMessage(), $item->getData());
            }
        }
    }

    /**
     * @return \Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\Collection
     */
    public function getColection()
    {
        if (!$this->collection) {
            $this->collection = $this->collectionFactory->create();
            $this->collection->addFieldToFilter('status', ['eq' => Status::STATUS_ACTIVE])
                ->addFieldToFilter('expires_at', ['lteq' => date('Y-m-d 23:59:59')]);
        }

        return $this->collection;
    }

    /**
     * @param \Linets\InsumosTempPriceAdjustment\Model\ResourceModel\SuppliesPriceAdjustment\Collection $collection
     */
    public function setCollection(Collection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @param SuppliesPriceAdjustment $adjustmentItem
     * @param Product $product
     * @return array
     * @throws LocalizedException
     */
    private function calculatePreviousPrices(SuppliesPriceAdjustment $adjustmentItem, Product $product)
    {
        $prices = [];
        //$websiteId = $this->storeManager->getWebsite()->getId();
        $websiteId = $this->storeManager->getStores(true,true)['insumos']->getId();


        $dispatchPrice = $this->dispatchPriceManagement->getDispatchPrice(
            $adjustmentItem->getSellerId(),
            $product->getAttributeText('macrozona'),
            $product->getAttributeSetId(),
            $websiteId
        );

        $prices['base_price'] = $adjustmentItem->getPreviousPrice() - $dispatchPrice->getPrice();
        $prices['price'] = $dispatchPrice->getPrice() + $prices['base_price'];

        return $prices;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @param string $error
     * @param array $context
     */
    public function addError(string $error, array $context)
    {
        $this->errors[] = ['message' => $error, 'context' => $context];
        $this->logger->error($error, $context);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

}
