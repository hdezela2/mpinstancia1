<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Api;

use Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface SuppliesPriceAdjustmentRepositoryInterface
{

    /**
     * Save SuppliesPriceAdjustment
     * @param \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface $suppliesPriceAdjustment
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        SuppliesPriceAdjustmentInterface $suppliesPriceAdjustment
    );

    /**
     * Retrieve SuppliesPriceAdjustment
     * @param string $suppliespriceadjustmentId
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($suppliespriceadjustmentId);

    /**
     * Retrieve SuppliesPriceAdjustment matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete SuppliesPriceAdjustment
     * @param \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface $suppliesPriceAdjustment
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        SuppliesPriceAdjustmentInterface $suppliesPriceAdjustment
    );

    /**
     * Delete SuppliesPriceAdjustment by ID
     * @param string $suppliespriceadjustmentId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($suppliespriceadjustmentId);
}

