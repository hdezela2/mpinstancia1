<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface SuppliesPriceAdjustmentSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get SuppliesPriceAdjustment list.
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface[]
     */
    public function getItems();

    /**
     * Set seller_id list.
     * @param \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

