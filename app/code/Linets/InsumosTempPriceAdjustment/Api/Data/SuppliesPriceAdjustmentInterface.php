<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\InsumosTempPriceAdjustment\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface SuppliesPriceAdjustmentInterface extends ExtensibleDataInterface
{

    const SUPPLIESPRICEADJUSTMENT_ID = 'suppliespriceadjustment_id';
    const SELLER_ID = 'seller_id';
    const CATEGORIES = 'categories';
    const PRODUCT_ID = 'product_id';
    const ASSOCIATE_PRODUCT_ID = 'associate_product_id';
    const PREVIOUS_PRICE = 'previous_price';
    const UPDATED_PRICE = 'updated_price';
    const PERCENTAGE = 'percentage';
    const STATUS = 'status';
    const EXPIRES_AT = 'expires_at';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get suppliespriceadjustment_id
     * @return string|null
     */
    public function getSuppliespriceadjustmentId();

    /**
     * Set suppliespriceadjustment_id
     * @param string $suppliespriceadjustmentId
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setSuppliespriceadjustmentId($suppliespriceadjustmentId);

    /**
     * Get seller_id
     * @return int
     */
    public function getSellerId();

    /**
     * Set seller_id
     * @param int $sellerId
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setSellerId($sellerId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        SuppliesPriceAdjustmentExtensionInterface $extensionAttributes
    );

    /**
     * Get categories
     * @return string|null
     */
    public function getCategories();

    /**
     * Set categories
     * @param string $categories
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setCategories($categories);

    /**
     * Get product_id
     * @return string|null
     */
    public function getProductId();

    /**
     * Set product_id
     * @param string $productId
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setProductId($productId);

    /**
     * Get associate_product_id
     * @return string|null
     */
    public function getAssociateProductId();

    /**
     * Set associate_product_id
     * @param int $associateProductId
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setAssociateProductId($associateProductId);

    /**
     * Get previous_price
     * @return string|null
     */
    public function getPreviousPrice();

    /**
     * Set previous_price
     * @param string $previousPrice
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setPreviousPrice($previousPrice);

    /**
     * Get updated_price
     * @return string|null
     */
    public function getUpdatedPrice();

    /**
     * Set updated_price
     * @param string $updatedPrice
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setUpdatedPrice($updatedPrice);

    /**
     * Get percentage
     * @return string|null
     */
    public function getPercentage();

    /**
     * Set percentage
     * @param string $percentage
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setPercentage($percentage);

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setStatus($status);

    /**
     * Get expires_at
     * @return string
     */
    public function getExpiresAt();

    /**
     * Set expires_at
     * @param string $expiresAt
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setExpiresAt($expiresAt);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \Linets\InsumosTempPriceAdjustment\Api\Data\SuppliesPriceAdjustmentInterface
     */
    public function setUpdatedAt($updatedAt);
}

