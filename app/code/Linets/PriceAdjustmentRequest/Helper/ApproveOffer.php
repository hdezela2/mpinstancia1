<?php


namespace Linets\PriceAdjustmentRequest\Helper;

use Chilecompra\LoginMp\Helper\HelperAdmin;
use Formax\AssignProduct\Helper\MpAssignProduct;
use Formax\StatusChange\Helper\Data;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Webkul\MpAssignProduct\Model\Associates;

class ApproveOffer extends AbstractHelper
{

    /**
     * @var \Formax\AssignProduct\Helper\MpAssignProduct
     */
    private $mpAssignProduct;
    /**
     * @var \Chilecompra\LoginMp\Helper\HelperAdmin
     */
    private $helperAdmin;
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var \Formax\StatusChange\Helper\Data
     */
    private $statusHelper;

    public function __construct(
        Context $context,
        MpAssignProduct $mpAssignProduct,
        HelperAdmin $helperAdmin,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        Data $statusHelper
    )
    {
        parent::__construct($context);
        $this->mpAssignProduct = $mpAssignProduct;
        $this->helperAdmin = $helperAdmin;
        $this->customerRepository = $customerRepository;
        $this->statusHelper = $statusHelper;
    }

    /**
     * @param $sellerId
     * @param string $sku
     * @param \Webkul\MpAssignProduct\Model\Associates $offer
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function syncApprovedOffer($sellerId, string $sku, Associates $offer)
    {
        $adminUser = $this->mpAssignProduct->getCurrentAdminUser();
        if ($adminUser) {
            $adminUserName = $adminUser->getUserName();
            $adminUserData = $this->helperAdmin->getUserData($adminUserName);
            $token = $adminUserData->getData('user_rest_atk');
            $agreementId = $adminUserData->getData('user_rest_id_active_agreement') ?
                $adminUserData->getData('user_rest_id_active_agreement') : '';
        } else {
            $token = $this->mpAssignProduct->getToken();
            $agreementId = $this->mpAssignProduct->getAgreementId();
        }
        $seller = $this->customerRepository->getById($sellerId);
        $organizationId = $seller->getCustomAttribute('user_rest_id') ? $seller->getCustomAttribute('user_rest_id')->getValue() : '';
        $dccpStatus = $this->statusHelper->getDccpStatusSeller();
        $sellerStatus = $seller->getCustomAttribute('wkv_dccp_state_details') ?
            $seller->getCustomAttribute('wkv_dccp_state_details')->getValue() : '';
        $dccpSellerStatus = $dccpStatus[$sellerStatus] ?? 0;
        $params = [
            'idOrganismo' => $organizationId,
            'idProducto' => $sku,
            'idConvenioMarco' => $agreementId,
            'idEstadoProductoProveedorConvenio' => $dccpSellerStatus,
            'tieneStock' => (int)$offer->getQty() > 0,
            'precio' => (float)$offer->getPrice()
        ];
        $response = $this->mpAssignProduct->setAssignProductUpdate($token, $params);
        if(!$response) {
            throw new LocalizedException(__('Error al intentar sincronizar el precio de la oferta con MP'));
        } elseif(isset($response['error']) && !empty($response['error'])) {
            throw new LocalizedException(__('Error al intentar sincronizar el precio de la oferta con MP: ' . $response['error']));
        }
    }

}