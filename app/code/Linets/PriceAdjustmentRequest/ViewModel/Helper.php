<?php

namespace Linets\PriceAdjustmentRequest\ViewModel;

use Linets\VehiculosSetUp\Model\Constants;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;

class Helper implements ArgumentInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * Helper constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    )
    {
        $this->storeManager = $storeManager;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isAllowed(): bool
    {
        $allowed = false;
        if ($this->storeManager->getWebsite()->getCode() == Constants::WEBSITE_CODE) {
            $allowed = true;
        }
        return $allowed;
    }
}