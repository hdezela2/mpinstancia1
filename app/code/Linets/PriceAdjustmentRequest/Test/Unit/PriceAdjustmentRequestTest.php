<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\PriceAdjustmentRequest\Test\Unit;

use PHPUnit\Framework\TestCase;

class PriceAdjustmentRequestTest extends TestCase
{

    /**
     * Is called once before running all test in class
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Is called once after running all test in class
     */
    public static function tearDownAfterClass()
    {
        //teardown
    }

    /**
     * The test itself, every test function must start with 'test'
     */
    public function testCreate()
    {
        $this->assertTrue(true);
    }

    /**
     * The test itself, every test function must start with 'test'
     */
    public function testApprove()
    {
        $this->assertTrue(true);
    }

    /**
     * The test itself, every test function must start with 'test'
     */
    public function testReject()
    {
        $this->assertTrue(true);
    }

    /**
     * Is called before running a test
     */
    protected function setUp()
    {
        //setup
    }

    /**
     * Is called after running a test
     */
    protected function tearDown()
    {
        //teardown
    }
}

