<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\PriceAdjustmentRequest\Controller\Adminhtml\Request;

use Exception;
use Linets\PriceAdjustmentRequest\Helper\ApproveOffer;
use Linets\PriceAdjustmentRequest\Model\Request;
use Linets\PriceAdjustmentRequest\Model\RequestRepository;
use Linets\PriceAdjustmentRequest\Model\Status\Options as StatusOptions;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Webkul\MpAssignProduct\Model\Associates as AssociatesModel;

class InlineEdit extends Action
{

    protected $jsonFactory;
    /**
     * @var \Webkul\MpAssignProduct\Model\Associates
     */
    private $associatesModel;
    /**
     * @var \Linets\PriceAdjustmentRequest\Model\RequestRepository
     */
    private $requestRepository;
    /**
     * @var \Linets\PriceAdjustmentRequest\Helper\ApproveOffer
     */
    private $approveOffer;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        AssociatesModel $associatesModel,
        RequestRepository $requestRepository,
        JsonFactory $jsonFactory,
        ApproveOffer $approveOffer
    )
    {
        parent::__construct($context);
        $this->associatesModel = $associatesModel;
        $this->requestRepository = $requestRepository;
        $this->jsonFactory = $jsonFactory;
        $this->approveOffer = $approveOffer;
    }

    /**
     * Inline edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('No requests to update or reject found, please check the data sent');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $requestId) {
                    /** @var \Linets\PriceAdjustmentRequest\Model\Request $request */
                    $request = $this->_objectManager->create(Request::class)->load($requestId);
                    try {
                        $this->validateStatus($requestId, $request->getStatus(), $postItems[$requestId]['status']);

                        if (StatusOptions::STATUS_APPROVED == $postItems[$requestId]['status']) {
                            $offer = $this->associatesModel->load($request->getOfferId());
                            $offer->setData('price', $request->getNewPrice());
                            $this->approveOffer->syncApprovedOffer($request->getSellerId(), $request->getSku(), $offer);
                            $offer->save();
                        }
                        $request->setData(array_merge($request->getData(), $postItems[$requestId]));
                        $request->save();
                    } catch (Exception $e) {
                        $messages[] = $e->getMessage();
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * @throws \Exception
     */
    public function validateStatus($requestId, $currentStatus, $newStatus)
    {
        if ($currentStatus == $newStatus) {
            throw new \Exception((string)__('The new status for request id: %1 is the same as the old status.', $requestId));

        }
        if ($currentStatus == StatusOptions::STATUS_APPROVED || $currentStatus == StatusOptions::STATUS_REJECTED) {
            throw new \Exception((string)__('You cannot change the status for request %1', $requestId));
        }
    }
}

