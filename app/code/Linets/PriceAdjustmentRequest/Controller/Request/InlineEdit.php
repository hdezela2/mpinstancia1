<?php
declare(strict_types=1);

namespace Linets\PriceAdjustmentRequest\Controller\Request;

use Linets\PriceAdjustmentRequest\Helper\ApproveOffer;
use Linets\PriceAdjustmentRequest\Model\RequestFactory;
use Linets\PriceAdjustmentRequest\Model\RequestRepository;
use Linets\PriceAdjustmentRequest\Model\ResourceModel\Request\CollectionFactory as RequestCollectionFactory;
use Linets\PriceAdjustmentRequest\Model\Status\Options as RequestStatusOptions;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Webkul\Marketplace\Helper\Data;
use Webkul\MpAssignProduct\Model\Associates as AssociatesModel;

class InlineEdit extends Action implements HttpPostActionInterface
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var \Webkul\MpAssignProduct\Model\Associates
     */
    private $associatesModel;
    /**
     * @var \Linets\PriceAdjustmentRequest\Model\RequestFactory
     */
    private $requestFactory;
    /**
     * @var \Linets\PriceAdjustmentRequest\Model\RequestRepository
     */
    private $requestRepository;
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;
    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    private $marketplaceHelper;
    /**
     * @var \Linets\PriceAdjustmentRequest\Model\ResourceModel\Request\CollectionFactory
     */
    private $requestCollectionFactory;
    /**
     * @var \Linets\PriceAdjustmentRequest\Helper\ApproveOffer
     */
    private $approveOffer;

    /**
     * InLineEdit constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Webkul\MpAssignProduct\Model\Associates $associatesModel
     * @param \Linets\PriceAdjustmentRequest\Model\RequestFactory $requestFactory
     * @param \Linets\PriceAdjustmentRequest\Model\ResourceModel\Request\CollectionFactory $requestCollectionFactory
     * @param \Linets\PriceAdjustmentRequest\Model\RequestRepository $requestRepository
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Webkul\Marketplace\Helper\Data $marketplaceHelper
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        AssociatesModel $associatesModel,
        RequestFactory $requestFactory,
        RequestCollectionFactory $requestCollectionFactory,
        RequestRepository $requestRepository,
        ProductRepository $productRepository,
        Data $marketplaceHelper,
        JsonFactory $jsonFactory,
        ApproveOffer $approveOffer
    )
    {
        parent::__construct($context);
        $this->associatesModel = $associatesModel;
        $this->requestFactory = $requestFactory;
        $this->requestCollectionFactory = $requestCollectionFactory;
        $this->requestRepository = $requestRepository;
        $this->productRepository = $productRepository;
        $this->marketplaceHelper = $marketplaceHelper;
        $this->jsonFactory = $jsonFactory;
        $this->approveOffer = $approveOffer;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!count($postItems)) {
            $messages[] = __('Please correct the data sent.');
            $error = true;
        } else {
            foreach (array_keys($postItems) as $offerId) {
                try {
                    $status = RequestStatusOptions::STATUS_PENDING;
                    $comments = '';
                    $offer = $this->associatesModel->load($offerId);
                    $product = $this->productRepository->getById($offer->getProductId());
                    $seller = $this->marketplaceHelper->getSeller();
                    $request = $this->requestFactory->create()->getDataModel();
                    $rut = $this->_objectManager->get(Session::class)->getCustomer()->getData('wkv_dccp_rut');
                    $sellerId = $seller['seller_id'];
                    $newPrice = preg_replace('/\D/', '', $postItems[$offerId]['current_price']);
                    $oldPrice = $offer->getPrice();

                    $this->validatePendingRequestsForOffer($offerId, $sellerId, $seller['shop_title'], $product->getSku());

                    if (!$this->isValidPrice($offer->getPrice(), $newPrice)) {
                        throw new \Exception((string)__('The new price for product %1 is the same as the old', $product->getSku()));
                    }
                    if ($this->shouldAutoApprove($offerId, $sellerId, $offer->getPrice(), $newPrice)) {
                        $status = RequestStatusOptions::STATUS_APPROVED;
                        $offer->setData('price', $newPrice);
                        $this->approveOffer->syncApprovedOffer($sellerId, $product->getSku(), $offer);
                        $offer->save();
                        $comments = 'Aprobacion automatica';
                    }
                    $request->setSeller($seller['shop_title'])
                        ->setSellerRut($rut)
                        ->setSellerId($sellerId)
                        ->setCurrentPrice($oldPrice)
                        ->setNewPrice($newPrice)
                        ->setSku($product->getSku())
                        ->setSkuName($product->getName())
                        ->setWebsite($this->marketplaceHelper->getWebsiteId())
                        ->setStatus($status)
                        ->setOfferId($offerId);
                    if ($comments) {
                        $request->setComments($comments);
                    }
                    $this->requestRepository->save($request);
                } catch (\Exception $e) {
                    $messages[] = $e->getMessage();
                    $error = true;
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * @param $currentPrice
     * @param $newPrice
     * @return bool
     */
    public function isValidPrice($currentPrice, $newPrice): bool
    {
        $result = true;
        if (!is_numeric($newPrice) || $currentPrice == $newPrice) {
            $result = false;
        }
        return $result;
    }

    /**
     * @param $offerId
     * @param $sellerRut
     * @param $sellerName
     * @param $sku
     * @throws \Exception
     */
    public function validatePendingRequestsForOffer($offerId, $sellerId, $sellerName, $sku)
    {
        $collection = $this->requestCollectionFactory->create()
            ->addFieldToFilter('status', RequestStatusOptions::STATUS_PENDING)
            ->addFieldToFilter('seller', $sellerName)
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('offer_id', $offerId);
        if ($collection->count() > 0) {
            throw new \Exception((string)__('You already have a request for price adjustment on product %1', $sku));
        }
    }

    /**
     * @param $offerId
     * @param $sellerRut
     * @param $sku
     * @param $currPrice
     * @param $newPrice
     * @return bool
     * @throws \Exception
     */
    public function shouldAutoApprove($offerId, $sellerId, $currPrice, $newPrice): bool
    {
        $currMont = date('m');
        $collection = $this->requestCollectionFactory->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('offer_id', $offerId);
        $collection->getSelect()
            ->where(new \Zend_Db_Expr("EXTRACT(MONTH FROM created_at) = '$currMont'"))
            ->where(new \Zend_Db_Expr("new_price < current_price"));

        return ($newPrice < $currPrice && $collection->count() == 0);
    }
}