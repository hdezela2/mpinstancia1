define([
    'underscore',
    'Magento_Ui/js/grid/columns/column'
], function (_, Column) {
    'use strict';

    return Column.extend({
        defaults: {
            bodyTmpl: 'Linets_PriceAdjustmentRequest/ui/grid/cells/text'
        },
        /**
         * Retrieves label associated with a provided value.
         *
         * @returns {String}
         */
        getStatusColor: function (row) {
            if (row.status === 'Pendiente') {
                return 'par-status-pending';
            } else if (row.status === 'Aprobado') {
                return 'par-status-approved';
            } else if (row.status === 'Desestimada' || row.status === 'Rechazado/a') {
                return 'par-status-rejected';
            }
            return '';
        }
    });
});