<?php

namespace Linets\PriceAdjustmentRequest\Ui\DataProvider;

use Linets\PriceAdjustmentRequest\Model\Status\Options as RequestStatusOptions;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemCollectionFactory;

class RequestHistoryDataProvider extends AbstractDataProvider
{
    /**
     * @var MarketplaceHelper
     */
    protected $_marketplaceHelper;

    /**
     * @var \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory
     */
    protected $_itemCollectionFactory;

    /**
     * @var EavAttribute
     */
    protected $_eavAttribute;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param MarketplaceHelper $marketplaceHelper
     * @param ItemCollectionFactory $itemCollectionFactory
     * @param EavAttribute $eavAttribute
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        MarketplaceHelper $marketplaceHelper,
        ItemCollectionFactory $itemCollectionFactory,
        EavAttribute $eavAttribute,
        array $meta = [],
        array $data = []
    )
    {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );

        $this->_eavAttribute = $eavAttribute;
        $this->_marketplaceHelper = $marketplaceHelper;
        $this->_itemCollectionFactory = $itemCollectionFactory;

        $customerId = $this->_marketplaceHelper->getCustomerId();
        $attrProductName = $this->_eavAttribute->getIdByCode('catalog_product', 'name');
        $sellercollection = $this->_itemCollectionFactory->create();
        $sellercollection
            ->addFieldToSelect('seller_id');
        $sellercollection->addFieldToFilter('main_table.seller_id', $customerId)
            ->getSelect()
            ->join(
                ['mpaap' => $sellercollection->getResource()->getTable('marketplace_assignproduct_associated_products')],
                'main_table.id = mpaap.parent_id',
                ['price', 'offer_id' => 'id', 'product_id']
            )->join(
                ['e' => $sellercollection->getResource()->getTable('catalog_product_entity')],
                'mpaap.product_id = e.entity_id',
                ['sku']
            )->join(
                ['cpev' => $sellercollection->getResource()->getTable('catalog_product_entity_varchar')],
                'e.row_id = cpev.row_id AND cpev.store_id = 0 AND cpev.attribute_id = ' . $attrProductName,
                ['name' => 'cpev.value']
            )->join(
                ['lpaj' => $sellercollection->getResource()->getTable('linets_priceadjustmentrequest_request')],
                'mpaap.id = lpaj.offer_id', [
                    'current_price','new_price', 'status', 'updated_at', 'comments','request_id'
                ]
            )
            ->where(new \Zend_Db_Expr("lpaj.status != '" . RequestStatusOptions::STATUS_PENDING . "'"));

        $sellercollection->addFilterToMap('product_id', 'mpaap.product_id');
        $sellercollection->addFilterToMap('name', 'cpev.value');
        $sellercollection->setOrder('main_table.created_at', 'desc');

        $this->collection = $sellercollection;
    }
}