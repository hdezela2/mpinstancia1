<?php

namespace Linets\PriceAdjustmentRequest\Ui\DataProvider;

use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemCollectionFactory;

class ProductListDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var MarketplaceHelper
     */
    protected $_marketplaceHelper;

    /**
     * @var \Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory
     */
    protected $_itemCollectionFactory;

    /**
     * @var EavAttribute
     */
    protected $_eavAttribute;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param MarketplaceHelper $marketplaceHelper
     * @param ItemCollectionFactory $itemCollectionFactory
     * @param EavAttribute $eavAttribute
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        MarketplaceHelper $marketplaceHelper,
        ItemCollectionFactory $itemCollectionFactory,
        EavAttribute $eavAttribute,
        array $meta = [],
        array $data = []
    )
    {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );

        $this->_eavAttribute = $eavAttribute;
        $this->_marketplaceHelper = $marketplaceHelper;
        $this->_itemCollectionFactory = $itemCollectionFactory;

        $customerId = $this->_marketplaceHelper->getCustomerId();
        $attrProductName = $this->_eavAttribute->getIdByCode('catalog_product', 'name');
        $sellercollection = $this->_itemCollectionFactory->create();
        $sellercollection
            ->addFieldToSelect('seller_id');
        $sellercollection->addFieldToFilter('main_table.seller_id', $customerId)
            ->getSelect()
            ->join(
                ['mpaap' => $sellercollection->getResource()->getTable('marketplace_assignproduct_associated_products')],
                'main_table.id = mpaap.parent_id AND mpaap.status = 1',
                ['current_price' => 'price', 'offer_id' => 'id', 'product_id']
            )->join(
                ['e' => $sellercollection->getResource()->getTable('catalog_product_entity')],
                'mpaap.product_id = e.entity_id',
                ['sku']
            )->join(
                ['cpev' => $sellercollection->getResource()->getTable('catalog_product_entity_varchar')],
                'e.row_id = cpev.row_id AND cpev.store_id = 0 AND cpev.attribute_id = ' . $attrProductName,
                ['name' => 'cpev.value']
            )->joinLeft(
                ['lpaj' => $sellercollection->getResource()->getTable('linets_priceadjustmentrequest_request')],
                'mpaap.id = lpaj.offer_id',
                [
                    'new_price' => new \Zend_Db_Expr('group_concat(lpaj.new_price ORDER BY lpaj.updated_at DESC)'),
                    'status' => new \Zend_Db_Expr('group_concat(lpaj.status ORDER BY lpaj.updated_at DESC)'),
                    'request_date' => new \Zend_Db_Expr('group_concat(lpaj.updated_at ORDER BY lpaj.updated_at DESC)'),
                    'request_comments' => new \Zend_Db_Expr("group_concat("
                        . "CASE WHEN lpaj.comments != '' THEN lpaj.comments ELSE 'NOCOMMENT' END "
                        . "ORDER BY lpaj.updated_at DESC SEPARATOR '|')")
                ]
            )
            ->group(['main_table.seller_id', 'mpaap.price', 'mpaap.id', 'mpaap.product_id', 'e.sku', 'cpev.value']);

        $sellercollection->addFilterToMap('product_id', 'mpaap.product_id');
        $sellercollection->addFilterToMap('name', 'cpev.value');
        $sellercollection->setOrder('main_table.created_at', 'desc');

        $this->collection = $sellercollection;
    }
}