<?php

namespace Linets\PriceAdjustmentRequest\Ui\Component\Listing\Columns\Frontend;

use Linets\PriceAdjustmentRequest\Model\Status\Options;
use Magento\Framework\Stdlib\BooleanUtils;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Date;

class RequestDate extends Date
{
    const COLUMN_NAME = 'request_date';
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTimeFactory
     */
    private $dateTimeFactory;
    /**
     * @var \Magento\Framework\Stdlib\BooleanUtils
     */
    private $booleanUtils;

    /**
     * RequestDate constructor.
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Framework\Stdlib\BooleanUtils $booleanUtils
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        TimezoneInterface $timezone,
        BooleanUtils $booleanUtils,
        array $components = [],
        array $data = []
    )
    {
        $this->booleanUtils = $booleanUtils;
        parent::__construct($context, $uiComponentFactory, $timezone, $booleanUtils, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     * @throws \Exception
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $date = '';
                $status = '';
                if ($item[self::COLUMN_NAME]) {
                    $value = explode(',', $item[self::COLUMN_NAME])[0];
                    $status = explode(',', $item['status'])[0];
                    $date = $this->timezone->date(new \DateTime($value));
                    $timezone = isset($this->getConfiguration()['timezone'])
                        ? $this->booleanUtils->convert($this->getConfiguration()['timezone'])
                        : true;
                    if (!$timezone) {
                        $date = new \DateTime($value);
                    }
                }
                $item[self::COLUMN_NAME] = $status == Options::STATUS_PENDING ? $date->format('d/m/Y H:i') : '';
            }
        }
        return $dataSource;
    }
}