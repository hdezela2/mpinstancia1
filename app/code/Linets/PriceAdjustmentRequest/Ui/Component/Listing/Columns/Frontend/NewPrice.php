<?php

namespace Linets\PriceAdjustmentRequest\Ui\Component\Listing\Columns\Frontend;

use Linets\PriceAdjustmentRequest\Model\Status\Options;
use Linets\PriceAdjustmentRequest\Model\Status\Options as RequestStatusOptions;
use Magento\Framework\Locale\CurrencyInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class NewPrice extends Column
{
    const COLUMN_NAME = 'new_price';
    const STATUS_COLUMN_NAME = 'status';
    /**
     * @var \Linets\PriceAdjustmentRequest\Model\Status\Options
     */
    private $statusOptions;
    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    private $localeCurrency;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * NewPrice constructor.
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Linets\PriceAdjustmentRequest\Model\Status\Options $statusOptions
     * @param \Magento\Framework\Locale\CurrencyInterface $localeCurrency
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        RequestStatusOptions $statusOptions,
        CurrencyInterface $localeCurrency,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->statusOptions = $statusOptions;
        $this->localeCurrency = $localeCurrency;
        $this->storeManager = $storeManager;
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     * @throws \Zend_Currency_Exception|\Magento\Framework\Exception\NoSuchEntityException
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $store = $this->storeManager->getStore(
                $this->context->getFilterParam('store_id', Store::DEFAULT_STORE_ID)
            );
            $currency = $this->localeCurrency->getCurrency($store->getBaseCurrencyCode());
            foreach ($dataSource['data']['items'] as &$item) {
                $status = explode(',', $item['status'])[0];
                $item[self::COLUMN_NAME] = $item[self::COLUMN_NAME] && $status == Options::STATUS_PENDING ?
                    $currency->toCurrency(sprintf("%f", explode(',', $item[self::COLUMN_NAME])[0]))
                    . ' (' . $this->getStatus(explode(',', $item[self::STATUS_COLUMN_NAME])[0]) . ')' : '';
            }
        }
        return $dataSource;
    }

    /**
     * @param $statusCode
     * @return string
     */
    public function getStatus($statusCode): string
    {
        $status = '';
        if ($statusCode) {
            $status = $this->statusOptions->getOptionText($statusCode);
        }
        return __($status)->getText();
    }
}