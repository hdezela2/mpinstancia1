<?php

namespace Linets\PriceAdjustmentRequest\Ui\Component\Listing\Columns\Adminhtml;

use Linets\PriceAdjustmentRequest\Model\Status\Options as StatusOptions;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Status extends Column
{
    const COLUMN_NAME = 'status';
    /**
     * @var \Linets\PriceAdjustmentRequest\Model\Status\Options
     */
    private $statusOptions;


    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        StatusOptions $statusOptions,
        array $components = [],
        array $data = []
    )
    {
        $this->statusOptions = $statusOptions;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[self::COLUMN_NAME] = $this->statusOptions->getOptionText($item[self::COLUMN_NAME]);
            }
        }
        return $dataSource;
    }
}