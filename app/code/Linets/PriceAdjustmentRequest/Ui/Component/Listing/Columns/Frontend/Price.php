<?php

namespace Linets\PriceAdjustmentRequest\Ui\Component\Listing\Columns\Frontend;

use Magento\Framework\Locale\CurrencyInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class Price extends Column
{
    const COLUMN_NAME = 'current_price';
    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    private $localeCurrency;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CurrencyInterface $localeCurrency,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->localeCurrency = $localeCurrency;
        $this->storeManager = $storeManager;
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Currency_Exception
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[self::COLUMN_NAME] = $this->format($item[self::COLUMN_NAME]);
            }
        }
        return $dataSource;
    }

    /**
     * @throws \Zend_Currency_Exception
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function format(float $price) {
        $store = $this->storeManager->getStore(
            $this->context->getFilterParam('store_id', Store::DEFAULT_STORE_ID)
        );
        $currency = $this->localeCurrency->getCurrency($store->getBaseCurrencyCode());
        return $currency->toCurrency(sprintf("%f", $price));
    }
}