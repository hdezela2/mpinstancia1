<?php

namespace Linets\PriceAdjustmentRequest\Ui\Component\Listing\Columns\Frontend;

class RequestedPrice extends Price
{
    const COLUMN_NAME = 'new_price';
    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Currency_Exception
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[self::COLUMN_NAME] = $this->format($item[self::COLUMN_NAME]);
            }
        }
        return $dataSource;
    }
}