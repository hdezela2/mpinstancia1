<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\PriceAdjustmentRequest\Model\Data;

use Linets\PriceAdjustmentRequest\Api\Data\RequestExtensionInterface;
use Linets\PriceAdjustmentRequest\Api\Data\RequestInterface;
use Magento\Framework\Api\AbstractExtensibleObject;

class Request extends AbstractExtensibleObject implements RequestInterface
{

    /**
     * Get request_id
     * @return string|null
     */
    public function getRequestId()
    {
        return $this->_get(self::REQUEST_ID);
    }

    /**
     * Set request_id
     * @param string $requestId
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setRequestId($requestId)
    {
        return $this->setData(self::REQUEST_ID, $requestId);
    }

    /**
     * Get seller
     * @return string|null
     */
    public function getSeller()
    {
        return $this->_get(self::SELLER);
    }

    /**
     * Set seller
     * @param string $seller
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setSeller($seller)
    {
        return $this->setData(self::SELLER, $seller);
    }

    /**
     * @return mixed|string|null
     */
    public function getSellerRut()
    {
        return $this->_get(self::SELLER_RUT);
    }

    /**
     * @param string $sellerRut
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface|\Linets\PriceAdjustmentRequest\Model\Data\Request
     */
    public function setSellerRut($sellerRut)
    {
        return $this->setData(self::SELLER_RUT, $sellerRut);
    }

    /**
     * @return string|void|null
     */
    public function getSellerId()
    {
        return $this->_get(self::SELLER_ID);
    }

    /**
     * @param string $sellerId
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface|\Linets\PriceAdjustmentRequest\Model\Data\Request
     */
    public function setSellerId($sellerId)
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Linets\PriceAdjustmentRequest\Api\Data\RequestExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        RequestExtensionInterface $extensionAttributes
    )
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get sku
     * @return string|null
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * Set sku
     * @param string $sku
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * Get sku_name
     * @return string|null
     */
    public function getSkuName()
    {
        return $this->_get(self::SKU_NAME);
    }

    /**
     * Set sku_name
     * @param string $skuName
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setSkuName($skuName)
    {
        return $this->setData(self::SKU_NAME, $skuName);
    }

    /**
     * Set offer_id
     *
     * @param int $offerId
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface|void
     */
    public function setOfferId($offerId)
    {
        return $this->setData(self::OFFER_ID, $offerId);
    }

    /**
     * Get offer_id
     *
     * @return int|void|null
     */
    public function getOfferId()
    {
        return $this->_get(self::OFFER_ID);
    }


    /**
     * Get status
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get website
     * @return string|null
     */
    public function getWebsite()
    {
        return $this->_get(self::WEBSITE);
    }

    /**
     * Set website
     * @param string $website
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setWebsite($website)
    {
        return $this->setData(self::WEBSITE, $website);
    }

    /**
     * Get current_price
     * @return string|null
     */
    public function getCurrentPrice()
    {
        return $this->_get(self::CURRENT_PRICE);
    }

    /**
     * Set current_price
     * @param string $currentPrice
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setCurrentPrice($currentPrice)
    {
        return $this->setData(self::CURRENT_PRICE, $currentPrice);
    }

    /**
     * Get new_price
     * @return string|null
     */
    public function getNewPrice()
    {
        return $this->_get(self::NEW_PRICE);
    }

    /**
     * Set new_price
     * @param string $newPrice
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setNewPrice($newPrice)
    {
        return $this->setData(self::NEW_PRICE, $newPrice);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * @return mixed|string|null
     */
    public function getComments()
    {
        return $this->_get(self::COMMENTS);
    }

    /**
     * @param string $comments
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface|\Linets\PriceAdjustmentRequest\Model\Data\Request
     */
    public function setComments($comments)
    {
        return $this->setData(self::COMMENTS, $comments);
    }

}

