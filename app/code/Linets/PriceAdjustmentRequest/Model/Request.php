<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\PriceAdjustmentRequest\Model;

use Linets\PriceAdjustmentRequest\Api\Data\RequestInterface;
use Linets\PriceAdjustmentRequest\Api\Data\RequestInterfaceFactory;
use Linets\PriceAdjustmentRequest\Model\ResourceModel\Request\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Request extends AbstractModel
{

    protected $_eventPrefix = 'linets_priceadjustmentrequest_request';
    protected $dataObjectHelper;

    protected $requestDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param RequestInterfaceFactory $requestDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Linets\PriceAdjustmentRequest\Model\ResourceModel\Request $resource
     * @param \Linets\PriceAdjustmentRequest\Model\ResourceModel\Request\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        RequestInterfaceFactory $requestDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Request $resource,
        Collection $resourceCollection,
        array $data = []
    )
    {
        $this->requestDataFactory = $requestDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve request model with request data
     * @return RequestInterface
     */
    public function getDataModel()
    {
        $requestData = $this->getData();

        $requestDataObject = $this->requestDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $requestDataObject,
            $requestData,
            RequestInterface::class
        );

        return $requestDataObject;
    }
}

