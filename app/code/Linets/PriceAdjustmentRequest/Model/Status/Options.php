<?php


namespace Linets\PriceAdjustmentRequest\Model\Status;


use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;

class Options extends AbstractSource implements SourceInterface, OptionSourceInterface
{

    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';
    /**
     * @var null|array
     */
    protected $options;

    /**
     * Options constructor.
     */
    public function __construct()
    {
        $this->options = [
            self::STATUS_PENDING => (string)__('Pending'),
            self::STATUS_APPROVED => (string)__('Approved'),
            self::STATUS_REJECTED => (string)__('Rejected'),
        ];
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function getOptionArray(): array
    {
        return $this->options;
    }

    /**
     * @return array|string[]|null
     */
    public function getAllOptions(): ?array
    {
        $allOptions = [];
        foreach ($this->options as $k => $v) {
            $allOptions[] = [
                'value' => $k,
                'label' => $v
            ];
        }
        return $allOptions;
    }


}