<?php

namespace Linets\PriceAdjustmentRequest\Model\Seller;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as SellerCollectionFactory;

class Options extends AbstractSource implements SourceInterface, OptionSourceInterface
{
    /**
     * @var null|array
     */
    protected $options;
    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory
     */
    private $sellerCollectionFactory;

    /**
     * Options constructor.
     */
    public function __construct(
        SellerCollectionFactory $sellerCollectionFactory
    )
    {
        $this->sellerCollectionFactory = $sellerCollectionFactory;
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function getOptionArray(): array
    {
        $this->initOptions();
        return $this->options;
    }

    /**
     * initialize options
     */
    public function initOptions()
    {
        if ($this->options) {
            return;
        }
        $sellerCollection = $this->sellerCollectionFactory->create()
            ->addFieldToFilter('is_seller', ['eq' => 1]);
        foreach ($sellerCollection as $seller) {
            $this->options[$seller->getEntityId()] = $seller->getShopTitle();
        }
    }

    /**
     * @return array|string[]|null
     */
    public function getAllOptions(): ?array
    {
        $this->initOptions();
        $allOptions = [];
        foreach ($this->options as $k => $v) {
            $allOptions[] = [
                'value' => $k,
                'label' => $v
            ];
        }
        return $allOptions;
    }


}