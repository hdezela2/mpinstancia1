<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\PriceAdjustmentRequest\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface RequestSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Request list.
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface[]
     */
    public function getItems();

    /**
     * Set seller list.
     * @param \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

