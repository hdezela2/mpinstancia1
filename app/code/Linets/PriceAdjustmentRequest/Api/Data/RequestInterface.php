<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\PriceAdjustmentRequest\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface RequestInterface extends ExtensibleDataInterface
{

    const REQUEST_ID = 'request_id';
    const CREATED_AT = 'created_at';
    const WEBSITE = 'website';
    const CURRENT_PRICE = 'current_price';
    const OFFER_ID = 'offer_id';
    const SKU = 'sku';
    const SKU_NAME = 'sku_name';
    const SELLER = 'seller';
    const SELLER_RUT = 'seller_rut';
    const SELLER_ID = 'seller_id';
    const STATUS = 'status';
    const UPDATED_AT = 'updated_at';
    const NEW_PRICE = 'new_price';
    const COMMENTS = 'comments';

    /**
     * Get request_id
     * @return string|null
     */
    public function getRequestId();

    /**
     * Set request_id
     * @param string $requestId
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setRequestId($requestId);

    /**
     * Get seller
     * @return string|null
     */
    public function getSeller();

    /**
     * Set seller
     * @param string $seller
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setSeller($seller);

    /**
     * Get seller_rut
     * @return string|null
     */
    public function getSellerRut();

    /**
     * Set seller_rut
     * @param string $sellerRut
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setSellerRut($sellerRut);

    /**
     * Get seller_id
     * @return string|null
     */
    public function getSellerId();

    /**
     * Set seller_id
     * @param string $sellerId
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setSellerId($sellerId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Linets\PriceAdjustmentRequest\Api\Data\RequestExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        RequestExtensionInterface $extensionAttributes
    );

    /**
     * Set sku
     * @param int $offerId
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setOfferId($offerId);

    /**
     * Get sku
     * @return int|null
     */
    public function getOfferId();

    /**
     * Set sku
     * @param string $sku
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setSku($sku);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku_name
     * @param string $skuName
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setSkuName($skuName);

    /**
     * Get sku_name
     * @return string|null
     */
    public function getSkuName();

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setStatus($status);

    /**
     * Get website
     * @return string|null
     */
    public function getWebsite();

    /**
     * Set website
     * @param string $website
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setWebsite($website);

    /**
     * Get current_price
     * @return string|null
     */
    public function getCurrentPrice();

    /**
     * Set current_price
     * @param string $currentPrice
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setCurrentPrice($currentPrice);

    /**
     * Get new_price
     * @return string|null
     */
    public function getNewPrice();

    /**
     * Set new_price
     * @param string $newPrice
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setNewPrice($newPrice);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setUpdatedAt($updatedAt);
    /**
     * Get comments
     * @return string|null
     */
    public function getComments();

    /**
     * Set comments
     * @param string $comments
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     */
    public function setComments($comments);
}

