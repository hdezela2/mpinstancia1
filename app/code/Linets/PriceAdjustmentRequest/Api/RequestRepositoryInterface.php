<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\PriceAdjustmentRequest\Api;

use Linets\PriceAdjustmentRequest\Api\Data\RequestInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface RequestRepositoryInterface
{

    /**
     * Save Request
     * @param \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface $request
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        RequestInterface $request
    );

    /**
     * Retrieve Request
     * @param string $requestId
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($requestId);

    /**
     * Retrieve Request matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Linets\PriceAdjustmentRequest\Api\Data\RequestSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Request
     * @param \Linets\PriceAdjustmentRequest\Api\Data\RequestInterface $request
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        RequestInterface $request
    );

    /**
     * Delete Request by ID
     * @param string $requestId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($requestId);
}

