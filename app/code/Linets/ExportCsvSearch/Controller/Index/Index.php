<?php
declare(strict_types=1);

namespace Linets\ExportCsvSearch\Controller\Index;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResponseInterface;
use Linets\ExportCsvSearch\Api\CsvContentInterface;

class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var CsvContentInterface
     */
    protected $csvContent;

    /**
     * @var \Magento\Framework\Math\Random
     */
    protected $random;

    /**
     * @param \Magento\Framework\App\Action\Context            $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Catalog\Model\ProductFactory            $productFactory
     * @param \Magento\Framework\View\Result\LayoutFactory     $resultLayoutFactory
     * @param \Magento\Framework\File\Csv                      $csvProcessor
     * @param \Magento\Framework\App\Filesystem\DirectoryList  $directoryList
     * @param CsvContentInterface                              $csvContent
     * @param \Magento\Framework\Math\Random                   $random
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        CsvContentInterface $csvContent,
        \Magento\Framework\Math\Random $random
    ) {
        $this->fileFactory = $fileFactory;
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
        $this->csvContent = $csvContent;
        $this->random = $random;
        parent::__construct($context);
    }

    /**
     * CSV Create and Download
     *
     * @return ResponseInterface
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function execute()
    {
        $headers[] = [
            'entity_id' => __('Entity ID'),
            'name' => __('Name'),
            'attribute_set' => __('Attribute Set'),
            'type_id' => __('Type ID'),
            'sku' => __('Sku'),
            'price' => __('Price')
        ];

        $fileName = 'product_list_' . $this->random->getUniqueHash() . '.csv';
        $filePath =  $this->directoryList->getPath(DirectoryList::MEDIA) . "/" . $fileName;
        $name = $this->getRequest()->getParam('query');
        $prodId = $this->getRequest()->getParam('prod_id');
        $attributeSetId = $this->getRequest()->getParam('attribute_set_id');
        $marca = $this->getRequest()->getParam('marca');
        $productsContent = $this->csvContent->getCsvContent($name, $attributeSetId, $marca, $prodId);
        $content = array_merge($headers, $productsContent);
        $this->csvProcessor->setEnclosure('"')->setDelimiter(',')->saveData($filePath, $content);

        return $this->fileFactory->create(
            $fileName,
            [
                'type'  => "filename",
                'value' => $fileName,
                'rm'    => true, // True => File will be remove from directory after download.
            ],
            DirectoryList::MEDIA,
            'text/csv',
            null
        );
    }
}

