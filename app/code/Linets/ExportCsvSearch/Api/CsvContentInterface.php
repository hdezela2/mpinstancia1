<?php
declare(strict_types=1);

namespace Linets\ExportCsvSearch\Api;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface CsvContentInterface
{

    /**
     * @return Collection
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getAllProducts($queryString, $attributeSetId, $marcaId, $prodId);

    /**
     * @return boolean
     */
    public function areGetParamValid($queryString, $attributeSetId, $marcaId, $prodId);

    /**
     * @return array
     */
    public function getCsvContent($queryString, $attributeSetId, $marcaId, $prodId);
}
