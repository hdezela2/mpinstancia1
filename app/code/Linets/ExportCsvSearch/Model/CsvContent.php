<?php
declare(strict_types=1);

namespace Linets\ExportCsvSearch\Model;

use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Linets\ExportCsvSearch\Api\CsvContentInterface;
use Formax\AssignProduct\Model\LinkedProductFactory;
use Linets\GasSetup\Constants;
use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\StoreManagerInterface;
use Summa\EmergenciasSetUp\Helper\Data;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as AssignProductCollection;
use Summa\AlimentosSetUp\Helper\Data as AlimentosHelper;
use Summa\ProductPackages\Helper\Data as PackagesHelper;
use Magento\Store\Model\ResourceModel\Group\CollectionFactory as StoreGroupCollection;
use Zend_Db_Expr;

class CsvContent implements CsvContentInterface
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    private $_assignHelper;

    /**
     * @var ProductCollection
     */
    private $_productCollection;

    /**
     * @var AssignProductCollection
     */
    private $_assignProductCollection;

    /**
     * @var CollectionFactory
     */
    private $_mpProductCollection;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    private $_productStatus;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    private $_productList;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var LinkedProductFactory
     */
    private $linkedProductFactory;

    /**
     * @var PackagesHelper
     */
    private $packagesHelper;

    /**
     * @var \Magento\Eav\Api\AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;

    /**
     * @var array
     */
    private $attributeSetList = [];

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    private $priceHelper;

    /**
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     * @param ProductCollection $productCollectionFactory
     * @param CollectionFactory $mpProductCollectionFactory
     * @param Status $productStatus
     * @param AssignProductCollection $assignProductCollection
     * @param TimezoneInterface $timezone
     * @param ResourceConnection $resource
     * @param StoreManagerInterface $storeManager
     * @param PackagesHelper $packagesHelper
     * @param LinkedProductFactory $linkedProductFactory
     * @param StoreGroupCollection $storeGroupCollection
     * @param CategoryFactory $categoryFactory
     * @param \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     */
    public function __construct(
        \Webkul\MpAssignProduct\Helper\Data $helper,
        ProductCollection $productCollectionFactory,
        CollectionFactory $mpProductCollectionFactory,
        Status $productStatus,
        AssignProductCollection $assignProductCollection,
        TimezoneInterface $timezone,
        ResourceConnection $resource,
        StoreManagerInterface $storeManager,
        PackagesHelper $packagesHelper,
        LinkedProductFactory $linkedProductFactory,
        StoreGroupCollection $storeGroupCollection,
        CategoryFactory $categoryFactory,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository,
        \Magento\Framework\Pricing\Helper\Data $priceHelper
    ) {
        $this->timezone = $timezone;
        $this->resource = $resource;
        $this->_storeManager = $storeManager;
        $this->packagesHelper = $packagesHelper;
        $this->linkedProductFactory = $linkedProductFactory;
        $this->_storeGroupCollection = $storeGroupCollection;
        $this->categoryFactory = $categoryFactory;
        $this->_assignHelper = $helper;
        $this->_productCollection = $productCollectionFactory;
        $this->_mpProductCollection = $mpProductCollectionFactory;
        $this->_productStatus = $productStatus;
        $this->_assignProductCollection = $assignProductCollection;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->priceHelper = $priceHelper;
    }

    /**
     * @inheritdoc
     */
    public function getAllProducts($queryString, $attributeSetId, $marcaId, $prodId)
    {
        if (!$this->_productList) {
            $connection = $this->resource->getConnection();
            $websiteId = (int)$this->_storeManager->getStore()->getWebsiteId();
            $currentDate = $connection->quote($this->timezone->date()->format('Y-m-d'));
            $tableConfigurable = $this->resource->getTableName('marketplace_assignproduct_associated_products');
            $tableSimple = $this->resource->getTableName('marketplace_assignproduct_items');
            $tableUserData = $this->resource->getTableName('marketplace_userdata');
            $tableOffer = $this->resource->getTableName('dccp_offer_product');
            $priceCol = $this->_storeManager->getWebsite()->getCode() === Data::WEBSITE_CODE ? 'base_price' : 'price';
            $priceFnc = $this->_storeManager->getWebsite()->getCode() === Data::WEBSITE_CODE ? 'AVG' : 'MIN';
            $rootCategoryId = $this->_storeGroupCollection->create()->addFieldToFilter('website_id', ["eq" => $websiteId])->getFirstItem()->getRootCategoryId();
            $validateActiveSellersOnly = "";
            $websiteCode = $this->_storeManager->getWebsite()->getCode();
            if(in_array($websiteCode, [InsumosConstants::WEBSITE_CODE, AseoRenewalConstants::WEBSITE_CODE])) {
                $validateActiveSellersOnly = "INNER JOIN customer_entity_varchar AS cev ON i.seller_id = cev.entity_id AND cev.attribute_id = 604 AND cev.value = '70' ";
            }


            $minPrice = "(SELECT " . $priceFnc . "(IFNULL(o.base_price, IF(i.type = 'configurable', aso.price, i." . $priceCol . "))) AS custom_price
            FROM " . $tableSimple . " i
            LEFT JOIN " . $tableConfigurable . " aso ON i.id = aso.parent_id
            INNER JOIN " . $tableUserData . " u ON i.seller_id = u.seller_id AND u.is_seller = 1
            $validateActiveSellersOnly
            LEFT JOIN " . $tableOffer . " o ON i.id = o.assign_id AND o.status = 3 AND o.website_id = " . $websiteId .
            " AND o.special_price < o.base_price WHERE i.status = 1 AND IF(i.type = 'configurable', aso.qty > 0, i.qty > 0) AND i.dis_dispersion = 0 AND i.product_id = e.entity_id) AS custom_price";

            if ($this->areGetParamValid($queryString, $attributeSetId, $marcaId, $prodId)) {
                $queryString = str_replace(' ','%', $queryString);
                $customerId = $this->_assignHelper->getCustomerId();
                $sellercollection = $this->_mpProductCollection
                                        ->create()
                                        ->addFieldToFilter('seller_id', ['eq' => $customerId])
                                        ->addFieldToSelect('mageproduct_id');
                $products = [];
                foreach ($sellercollection as $data) {
                    array_push($products, $data->getMageproductId());
                }

                $sellerAssigncollection = $this->_assignProductCollection
                                ->create()
                                ->addFieldToFilter('seller_id', $customerId)
                                ->addFieldToSelect('product_id');

                if ($this->_storeManager->getWebsite()->getCode() == \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE){
                    $linkedProduct = $this->linkedProductFactory->create();
                    $linkedPending = $linkedProduct->getCollection()
                        ->addFieldToFilter('seller_id', $customerId)
                        ->addFieldToFilter('status', ['eq' => 0])
                        ->addFieldToSelect('product_id')
                        ->getItems();
                    foreach ($linkedPending as $data) {
                        array_push($products, $data->getProductId());
                    }
                }

                foreach ($sellerAssigncollection as $data) {
                    array_push($products, $data->getProductId());
                }

                if($this->_storeManager->getWebsite()->getCode() === AlimentosHelper::WEBSITE_CODE)
                {
                    array_push($products, $this->packagesHelper->getAllPackagesIds());
                }

                $allowedTypes = $this->_assignHelper->getAllowedProductTypes();
                $collection = $this->_productCollection
                                    ->create()
                                    ->addFieldToSelect('*')
                                    ->addFieldToFilter('name', ['like' => $queryString.'%']);
                $collection->addFieldToFilter('type_id', ['in' => $allowedTypes]);
                if ($attributeSetId) {
                    $collection->addFieldToFilter('attribute_set_id', $attributeSetId);
                }
                if ($marcaId) {
                    $collection->addFieldToFilter('marca', $marcaId);
                }
                if ($prodId) {
                    $collection->addFieldToFilter('sku', ['like' => '%' . $prodId .'%']);
                }

                if (count($products) > 0) {
                    $collection->addFieldToFilter('entity_id', ['nin' => $products]);
                }

                if ($this->_storeManager->getWebsite()->getCode() == Constants::GAS_WEBSITE_CODE) {
                    // TODO get category value from core_config_data
                    $categoryCollection = $this->categoryFactory->create()->getCollection()
                        ->addAttributeToFilter('name', ['in' => 'GAS LICUADO DE PETRÓLEO EN MODALIDAD GRANEL'])
                        ->setPageSize(1);

                    if ($categoryCollection->getSize()) {
                        $categoryId = $categoryCollection->getFirstItem()->getId();
                        $collection->addCategoriesFilter(['in' => $categoryId]);
                    }
                }

                $collection->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()]);
                $collection->addAttributeToFilter('entity_id', [
                    'nin' => new Zend_Db_Expr('SELECT product_id FROM catalog_product_super_link')
                ]);
                $collection->setOrder('created_at', 'desc');
            } else {
                $collection = $this->_productCollection
                                    ->create()
                                    ->addFieldToSelect('*')
                                    ->addFieldToFilter('row_id', 0);
            }

            $collection->getSelect()->join(
                ['cap' => 'catalog_category_product'],
                'e.entity_id = cap.product_id',
                []
            )->join(
                ['cat' => 'catalog_category_entity'],
                'cap.category_id = cat.entity_id and cat.path like "1/' . (int)$rootCategoryId . '/%"',
                []
            );

            $collection->getSelect()->columns($minPrice);
            $collection->distinct(true);
            $this->_productList = $collection;
        }

        return $this->_productList;
    }

    /**
     * @inheritdoc
     */
    public function areGetParamValid($queryString, $attributeSetId, $marcaId, $prodId)
    {
        $paramsAreValid = false;
        $websiteCode = $this->_storeManager->getWebsite()->getCode();
        if ($queryString != '') {
            $paramsAreValid = true;
        } elseif (in_array($websiteCode, [InsumosConstants::WEBSITE_CODE, AseoRenewalConstants::WEBSITE_CODE])) {
            if ($attributeSetId != '') {
                $paramsAreValid = true;
            } elseif ($marcaId != '') {
                $paramsAreValid = true;
            } elseif ($prodId != '') {
                $paramsAreValid = true;
            }
        }

        return $paramsAreValid;
    }

    /**
     * @inheritdoc
     */
    public function getCsvContent($queryString, $attributeSetId, $marcaId, $prodId)
    {
        $content = [];
        $productCollection = $this->getAllProducts($queryString, $attributeSetId, $marcaId, $prodId);
        foreach ($productCollection as $product) {
            $attributeSetName = $this->getAttributeSetName($product->getAttributeSetId());
            $price = $product->getCustomPrice() > 0 ? $this->priceHelper->currency($product->getCustomPrice(), true, false) : __('New product');
            $content[] = [
                $product->getEntityId(),
                $product->getName(),
                $attributeSetName,
                $product->getTypeId(),
                $product->getSku(),
                $price
            ];
        }

        return $content;
    }

    /**
     * @param int $attributeSetId
     * @return string
     */
    private function getAttributeSetName($attributeSetId)
    {
        if (!isset($this->attributeSetList[$attributeSetId]) || empty($this->attributeSetList[$attributeSetId])) {
            try {
                $attributeSet = $this->attributeSetRepository->get($attributeSetId);
                $this->attributeSetList[$attributeSetId] = $attributeSet->getAttributeSetName();
            } catch (\Exception $ex) {
                $this->attributeSetList[$attributeSetId] = '';
            }
        }

        return $this->attributeSetList[$attributeSetId];
    }
}
