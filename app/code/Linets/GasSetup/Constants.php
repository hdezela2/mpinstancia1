<?php

namespace Linets\GasSetup;

class Constants
{
	const GAS_WEBSITE_CODE = 'gas';
	const GAS_STORE_CODE = 'gas';
	const GAS_STORE_VIEW_CODE = 'gas';

	const GAS_WEBSITE_NAME = 'Gas Website';
	const GAS_STORE_NAME = 'Gas';
	const GAS_STORE_VIEW_NAME = 'Gas';
	const GAS_DEFAULT_CATEGORY_NAME = 'Gas - Default Category';

	const FRONT_THEME_NAME = 'Linets/Gas';
	const ID_AGREEMENT = 5800297;
}