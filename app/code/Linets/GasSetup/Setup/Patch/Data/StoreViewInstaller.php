<?php
declare(strict_types=1);

namespace Linets\GasSetup\Setup\Patch\Data;

use Exception;
use Linets\GasSetup\Constants;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\Store as ModelStore;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Psr\Log\LoggerInterface;

class StoreViewInstaller implements DataPatchInterface
{
	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;

	/**
	 * @var WebsiteFactory
	 */
	private $websiteFactory;

	/**
	 * @var StoreFactory
	 */
	private $storeFactory;

	/**
	 * @var GroupFactory
	 */
	private $groupFactory;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * StoreViewInstaller constructor.
	 * @param ModuleDataSetupInterface $moduleDataSetup
	 * @param WebsiteFactory $websiteFactory
	 * @param StoreFactory $storeFactory
	 * @param GroupFactory $groupFactory
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		WebsiteFactory $websiteFactory,
		StoreFactory $storeFactory,
		GroupFactory $groupFactory,
		LoggerInterface $logger
	) {
		$this->moduleDataSetup = $moduleDataSetup;
		$this->websiteFactory = $websiteFactory;
		$this->storeFactory = $storeFactory;
		$this->groupFactory = $groupFactory;
		$this->logger = $logger;
	}

    /**
     * @return $this
     */
	public function apply()
	{
		$this->moduleDataSetup->startSetup();

		try {
			/** @var \Magento\Store\Model\Website $website */
			$website = $this->websiteFactory->create();
			$website->load(Constants::GAS_WEBSITE_CODE);

			/** @var ModelStore $store */
			$store = $this->storeFactory->create();
			$store->load(Constants::GAS_STORE_CODE);

			if ($website->getId()) {
				$group = $this->groupFactory->create();
				$group->load(Constants::GAS_STORE_VIEW_NAME, 'name');
				$store->setCode(Constants::GAS_STORE_VIEW_CODE);
				$store->setName(Constants::GAS_STORE_VIEW_NAME);
				$store->setWebsiteId($website->getId());
				$store->setGroupId($group->getId());
				$store->setIsActive(1);
				$store->getResource()->save($store);
			}
		} catch (LocalizedException | Exception $e) {
			$this->logger->critical(
				'Store View with code: ' . Constants::GAS_STORE_VIEW_NAME . ' already exists. Log: ' . $e->getMessage()
			);
		}

		$this->moduleDataSetup->endSetup();
        return $this;
	}

	/**
	 * @return string[]
	 */
	public static function getDependencies(): array
	{
		return [
			WebsiteInstaller::class,
			StoreInstaller::class
		];
	}

	/**
	 * @return array
	 */
	public function getAliases()
	{
		return [];
	}
}
