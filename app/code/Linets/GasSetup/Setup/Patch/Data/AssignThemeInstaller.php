<?php
declare(strict_types=1);

namespace Linets\GasSetup\Setup\Patch\Data;

use Linets\GasSetup\Constants;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreFactory;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;
use Magento\Theme\Model\Theme;
use Magento\Theme\Model\Data\Design\Config as DesignConfig;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\App\Config\ReinitableConfigInterface;
use Psr\Log\LoggerInterface;

class AssignThemeInstaller implements DataPatchInterface
{
	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;

	/**
	 * @var ConfigInterface
	 */
	private $configInterface;

	/**
	 * @var CollectionFactory
	 */
	private $collectionFactory;

	/**
	 * @var IndexerRegistry
	 */
	private $indexerRegistry;

	/**
	 * @var ReinitableConfigInterface
	 */
	private $reinitableConfig;

	/**
	 * @var StoreFactory
	 */
	private $storeFactory;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * AssignThemeInstaller constructor.
	 * @param ModuleDataSetupInterface $moduleDataSetup
	 * @param StoreFactory $storeFactory
	 * @param ConfigInterface $configInterface
	 * @param CollectionFactory $collectionFactory
	 * @param IndexerRegistry $indexerRegistry
	 * @param ReinitableConfigInterface $reInitTableConfig
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		StoreFactory $storeFactory,
		ConfigInterface $configInterface,
		CollectionFactory $collectionFactory,
		IndexerRegistry $indexerRegistry,
		ReinitableConfigInterface $reInitTableConfig,
		LoggerInterface $logger
	) {
		$this->moduleDataSetup = $moduleDataSetup;
		$this->storeFactory = $storeFactory;
		$this->configInterface = $configInterface;
		$this->collectionFactory = $collectionFactory;
		$this->indexerRegistry = $indexerRegistry;
		$this->reinitableConfig = $reInitTableConfig;
		$this->logger = $logger;
	}

	/**
	 * @inheritDoc
     *
     * @return $this
	 */
	public function apply()
	{
		$this->moduleDataSetup->startSetup();

		try {
			/** @var Store $store */
			$store = $this->storeFactory->create();
			$store->load(Constants::GAS_STORE_CODE);
			$themes = $this->collectionFactory->create()->loadRegisteredThemes();

			/** @var Theme $theme */
			foreach ($themes as $theme) {
				if ($theme->getArea() == 'frontend' && $theme->getCode() == Constants::FRONT_THEME_NAME) {
					$this->configInterface->saveConfig(
						'design/theme/theme_id',
						$theme->getId(),
						'stores',
						$store->getId()
					);
					$this->reinitableConfig->reinit();
					$this->indexerRegistry->get(DesignConfig::DESIGN_CONFIG_GRID_INDEXER_ID)->reindexAll();
				}
			}
		} catch (\Exception $e) {
			$this->logger->critical(
				'Cant assign theme to store ' . Constants::GAS_STORE_CODE . '. Log: ' . $e->getMessage()
			);
		}

		$this->moduleDataSetup->endSetup();
        return $this;
	}

	/**
	 * @return string[]
	 */
	public static function getDependencies(): array
	{
		return [
			WebsiteInstaller::class,
			StoreInstaller::class,
			StoreViewInstaller::class
		];
	}


	/**
	 * @return array
	 */
	public function getAliases(): array
	{
		return [];
	}
}
