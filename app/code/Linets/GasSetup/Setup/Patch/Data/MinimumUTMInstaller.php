<?php
declare(strict_types=1);

namespace Linets\GasSetup\Setup\Patch\Data;

use Formax\GreatBuy\Helper\Data as HelperData;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Linets\GasSetup\Constants;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class MinimumUTMInstaller implements DataPatchInterface
{
	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;

	/**
	 * @var ConfigInterface
	 */
	private $configInterface;

	/**
	 * @var WebsiteRepositoryInterface
	 */
	private $websiteRepository;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @param ModuleDataSetupInterface $moduleDataSetup
	 * @param ConfigInterface $configInterface
	 * @param WebsiteRepositoryInterface $websiteRepository
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		ConfigInterface $configInterface,
		WebsiteRepositoryInterface $websiteRepository,
		LoggerInterface $logger
	) {
		$this->moduleDataSetup = $moduleDataSetup;
		$this->configInterface = $configInterface;
		$this->websiteRepository = $websiteRepository;
		$this->logger = $logger;
	}

	/**
	 * @inheritDoc
     *
     * @return $this
	 */
	public function apply()
	{
		$this->moduleDataSetup->startSetup();

		try {
			$website = $this->websiteRepository->get(Constants::GAS_WEBSITE_CODE);
			$this->configInterface->saveConfig(
				HelperData::MINIMUM_UTM,
				'0',
				ScopeInterface::SCOPE_WEBSITES,
				$website->getId()
			);
		} catch (\Exception $exception) {
			$this->logger->error($exception->getMessage());
		}

		$this->moduleDataSetup->endSetup();
        return $this;
	}

	/**
	 * @return array
	 */
	public static function getDependencies(): array
	{
		return [];
	}

	/**
	 * @return array
	 */
	public function getAliases(): array
	{
		return [];
	}
}
