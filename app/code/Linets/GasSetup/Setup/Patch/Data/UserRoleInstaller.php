<?php
declare(strict_types=1);

namespace Linets\GasSetup\Setup\Patch\Data;

use Linets\GasSetup\Constants;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\ResourceModel\Role as RoleResourceModel;
use Magento\Authorization\Model\RoleFactory;
use Magento\Authorization\Model\RulesFactory;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\WebsiteRepository;

class UserRoleInstaller implements DataPatchInterface
{
	/**
	 * @var WebsiteRepository
	 */
	protected $websiteRepository;

	/**
	 * @var RoleFactory
	 */
	protected $roleFactory;


	/**
	 * @var RulesFactory
	 */
	protected $rulesFactory;

	/**
	 * @var RoleResourceModel
	 */
	private $roleResourceModel;

	/**
	 * UserRoleInstaller constructor.
	 * @param WebsiteRepository $websiteRepository
	 * @param RoleFactory $roleFactory
	 * @param RulesFactory $rulesFactory
	 * @param RoleResourceModel $roleResourceModel
	 */
	public function __construct(
		WebsiteRepository $websiteRepository,
		RoleFactory $roleFactory,
		RulesFactory $rulesFactory,
		RoleResourceModel $roleResourceModel
	) {
		$this->websiteRepository = $websiteRepository;
		$this->roleFactory = $roleFactory;
		$this->rulesFactory = $rulesFactory;
		$this->roleResourceModel = $roleResourceModel;
	}

	/**
     * @return $this
	 * @throws NoSuchEntityException
	 * @throws AlreadyExistsException
	 */
	public function apply()
	{
		$agreementId = Constants::ID_AGREEMENT;
		$website = $this->websiteRepository->get(Constants::GAS_WEBSITE_CODE);

		/** CREATE USER ROLE */
		$role = $this->roleFactory->create();

		// Set Role Name with the Agreement ID of the new Convenio Marco
		$role->setName($agreementId)
			->setPid(0) //set parent role id of your role
			->setRoleType(RoleGroup::ROLE_TYPE)
			->setUserType(UserContextInterface::USER_TYPE_ADMIN)
			->setGwsIsAll(0) //Doesn't have permission in all Stores
			->setGwsWebsites($website->getId()) //array with the website ids
			->setGwsStoreGroups($website->getId());

		$this->roleResourceModel->save($role);

		/* Now we set that which resources we allow to this role */
		$resource = [
			'Magento_Backend::dashboard',
            'Linets_WeeklyPriceManagement::listing',
			'Webkul_CategoryProductPrice::categoryproductprice',
			'Webkul_CategoryProductPrice::index',

			'Formax_PriceDispersion::pricedispersion',
			'Formax_PriceDispersion::index',

			'Magento_Analytics::analytics',
			'Magento_Analytics::analytics_api',

			'Webkul_Marketplace::marketplace',
			'Webkul_Marketplace::menu',
			'Webkul_Marketplace::seller',
			'Webkul_MpAssignProduct::product',
			'Formax_RegionalCondition::regional_condition',

			'Webkul_Mpshipping::menu',
			'Webkul_Mpshipping::mpshipping',
			'Webkul_Mpshipping::mpshippingset',

			'Webkul_Requestforquote::requestforquote',
			'Webkul_Requestforquote::quote_index',
			'Webkul_Requestforquote::index_index',

			'Magento_Catalog::catalog',
			'Magento_CatalogPermissions::catalog_magento_catalogpermissions',
			'Magento_Catalog::catalog_inventory',
			'Magento_Catalog::products',
			'Magento_PricePermissions::read_product_price',
			'Magento_PricePermissions::edit_product_price',
			'Magento_PricePermissions::edit_product_status',
			'Magento_Catalog::categories',

			'Magento_Customer::customer',
			'Magento_Customer::manage',
			'Magento_Reward::reward_balance',

			'Magento_Reports::report',
			'Magento_Reports::salesroot',
			'Formax_CustomReport::Report',

			'Magento_Backend::system',
			'Magento_Backend::convert',
			'Magento_ImportExport::export'

		];

		/** Array of resource ids which we want to allow this role*/
		$this->rulesFactory->create()
			->setRoleId($role->getId())
			->setResources($resource)
			->saveRel();
        return $this;
	}

	/**
	 * @return string[]
	 */
	public static function getDependencies(): array
	{
		return [
			WebsiteInstaller::class
		];
	}

	/**
	 * @return array
	 */
	public function getAliases(): array
	{
		return [];
	}
}
