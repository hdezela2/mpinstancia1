<?php
declare(strict_types=1);

namespace Linets\GasSetup\Setup\Patch\Data;

use Exception;
use Linets\GasSetup\Constants;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory as MagentoCategoryFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Psr\Log\LoggerInterface;

class StoreInstaller implements DataPatchInterface
{
	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;

	/**
	 * @var StoreFactory
	 */
	private $storeFactory;

	/**
	 * @var Group
	 */
	private $groupResourceModel;

	/**
	 * @var GroupFactory
	 */
	private $groupFactory;

	/**
	 * @var CategoryRepositoryInterface
	 */
	private $categoryRepository;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var WebsiteFactory
	 */
	private $websiteFactory;

	/**
	 * @var MagentoCategoryFactory
	 */
	private $categoryFactory;

	/**
	 * StoreInstaller constructor.
	 * @param ModuleDataSetupInterface $moduleDataSetup
	 * @param WebsiteFactory $websiteFactory
	 * @param StoreFactory $storeFactory
	 * @param Group $groupResourceModel
	 * @param GroupFactory $groupFactory
	 * @param MagentoCategoryFactory $categoryFactory
	 * @param CategoryRepositoryInterface $categoryRepository
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		WebsiteFactory $websiteFactory,
		StoreFactory $storeFactory,
		Group $groupResourceModel,
		GroupFactory $groupFactory,
		MagentoCategoryFactory $categoryFactory,
		CategoryRepositoryInterface $categoryRepository,
		LoggerInterface $logger
	) {
		$this->moduleDataSetup = $moduleDataSetup;
		$this->websiteFactory = $websiteFactory;
		$this->storeFactory = $storeFactory;
		$this->groupResourceModel = $groupResourceModel;
		$this->groupFactory = $groupFactory;
		$this->categoryRepository = $categoryRepository;
		$this->categoryFactory = $categoryFactory;
		$this->logger = $logger;
	}

    /**
     * @return $this
     */
	public function apply()
	{
		$this->moduleDataSetup->startSetup();

		try {
			/** @var \Magento\Store\Model\Website $website */
			$website = $this->websiteFactory->create();
			$website->load(Constants::GAS_WEBSITE_CODE);
			if ($website->getId()) {
				$category = $this->createRootCategory();

				/** @var \Magento\Store\Model\Group $group */
				$group = $this->groupFactory->create();
				$group->setWebsiteId($website->getWebsiteId());
				$group->setName(Constants::GAS_STORE_NAME);
				$group->setCode(Constants::GAS_STORE_CODE);
				$group->setRootCategoryId($category->getId());
				$group->setDefaultStoreId(0);

				$this->groupResourceModel->save($group);
			}
		} catch (Exception | AlreadyExistsException $e) {
			$this->logger->critical(
				'Store with code: ' . Constants::GAS_STORE_CODE . ' already exists. Log: ' . $e->getMessage()
			);
		}

		$this->moduleDataSetup->endSetup();
        return $this;
	}

	/**
	 * @return string[]
	 */
	public static function getDependencies(): array
	{
		return [
			WebsiteInstaller::class
		];
	}

	/**
	 * @return array
	 */
	public function getAliases(): array
	{
		return [];
	}

	/**
	 * @return Category
	 * @throws Exception
	 */
	protected function createRootCategory(): Category
	{
		$category = $this->categoryFactory->create();
		$category->setName(Constants::GAS_DEFAULT_CATEGORY_NAME);
		$category->setIsActive(true);
		$category->setStoreId(0);

		$category->setDisplayMode(Category::DM_PRODUCT);
		$category->setParentId(Category::TREE_ROOT_ID);
		$this->categoryRepository->save($category);

		return $category;
	}

}
