<?php
declare(strict_types=1);

namespace Linets\GasSetup\Setup\Patch\Data;

use Linets\GasSetup\Constants;
use Magento\Authorization\Model\ResourceModel\Role as RoleResourceModel;
use Magento\Authorization\Model\ResourceModel\Role\Grid\CollectionFactory;
use Magento\Authorization\Model\RoleFactory;
use Magento\Authorization\Model\RulesFactory;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class UserRoleUpdateInstaller implements DataPatchInterface
{
	/**
	 * @var RoleFactory
	 */
	protected $roleFactory;

	/**
	 * @var RulesFactory
	 */
	protected $rulesFactory;

	/**
	 * @var RoleResourceModel
	 */
	private $roleResourceModel;

	/**
	 * @var CollectionFactory
	 */
	private $roleCollectionFactory;

	/**
	 * UserRoleUpdateInstaller constructor.
	 * @param CollectionFactory $roleCollectionFactory
	 * @param RulesFactory $rulesFactory
	 * @param RoleResourceModel $roleResourceModel
	 */
	public function __construct(
		CollectionFactory $roleCollectionFactory,
		RulesFactory $rulesFactory,
		RoleResourceModel $roleResourceModel
	) {
		$this->rulesFactory = $rulesFactory;
		$this->roleResourceModel = $roleResourceModel;
		$this->roleCollectionFactory = $roleCollectionFactory;
	}

    /**
     * @return $this
     */
	public function apply()
	{
		$agreementId = Constants::ID_AGREEMENT;
		$roles = $this->roleCollectionFactory->create();

		foreach ($roles->getData() as $role) {
			if ($role['role_name'] == $agreementId) {
				$roleId = $role['role_id'];
			}
		}

		$resource = [
			'Magento_Backend::dashboard',
            'Linets_WeeklyPriceManagement::listing',
			'Webkul_CategoryProductPrice::categoryproductprice',
			'Webkul_CategoryProductPrice::index',

			'Magento_Analytics::analytics',
			'Magento_Analytics::analytics_api',

			'Webkul_Marketplace::marketplace',
			'Webkul_Marketplace::menu',
			'Webkul_Marketplace::seller',
			'Webkul_MpAssignProduct::product',
			'Formax_RegionalCondition::regional_condition',

			'Webkul_Mpshipping::menu',
			'Webkul_Mpshipping::mpshipping',
			'Webkul_Mpshipping::mpshippingset',

			'Webkul_Requestforquote::requestforquote',
			'Webkul_Requestforquote::quote_index',
			'Webkul_Requestforquote::index_index',

			'Magento_Catalog::catalog',
			'Magento_CatalogPermissions::catalog_magento_catalogpermissions',
			'Magento_Catalog::catalog_inventory',
			'Magento_Catalog::products',
			'Magento_PricePermissions::read_product_price',
			'Magento_PricePermissions::edit_product_price',
			'Magento_PricePermissions::edit_product_status',
			'Magento_Catalog::categories',

			'Magento_Customer::customer',
			'Magento_Customer::manage',
			'Magento_Reward::reward_balance',

			'Magento_Reports::report',
			'Magento_Reports::salesroot',
			'Formax_CustomReport::Report',

			'Magento_Backend::system',
			'Magento_Backend::convert',
			'Magento_ImportExport::export'
		];
		if (isset($roleId)) {
			$this->rulesFactory->create()
				->setRoleId($roleId)
				->setResources($resource)
				->saveRel();
		}
        return $this;
	}

	/**
	 * @return string[]
	 */
	public static function getDependencies(): array
	{
		return [
			WebsiteInstaller::class,
			UserRoleInstaller::class
		];
	}

	/**
	 * @return array
	 */
	public function getAliases(): array
	{
		return [];
	}
}
