<?php
declare(strict_types=1);

namespace Linets\GasSetup\Setup\Patch\Data;

use Exception;
use Linets\GasSetup\Constants;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\WebsiteFactory;
use Psr\Log\LoggerInterface;

class WebsiteInstaller implements DataPatchInterface
{
	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;

	/**
	 * @var WebsiteFactory
	 */
	private $websiteFactory;

	/**
	 * @var Website
	 */
	private $websiteResourceModel;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * WebsiteInstaller constructor.
	 * @param ModuleDataSetupInterface $moduleDataSetup
	 * @param WebsiteFactory $websiteFactory
	 * @param Website $websiteResourceModel
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		WebsiteFactory $websiteFactory,
		Website $websiteResourceModel,
		LoggerInterface $logger
	) {
		$this->moduleDataSetup = $moduleDataSetup;
		$this->websiteFactory = $websiteFactory;
		$this->websiteResourceModel = $websiteResourceModel;
		$this->logger = $logger;
	}

    /**
     * @return $this
     */
	public function apply()
	{
		$this->moduleDataSetup->startSetup();
		try {
			/** @var \Magento\Store\Model\Website $website */
			$website = $this->websiteFactory->create();
			$website->load(Constants::GAS_WEBSITE_CODE);
			if (!$website->getId()) {
				$website->setCode(Constants::GAS_WEBSITE_CODE);
				$website->setName(Constants::GAS_WEBSITE_NAME);
				$this->websiteResourceModel->save($website);
			}
		} catch (Exception | AlreadyExistsException $e) {
			$this->logger->critical(
				'Website with code: ' . Constants::GAS_WEBSITE_CODE . ' already exists. Log: ' . $e->getMessage()
			);
		}

		$this->moduleDataSetup->endSetup();
        return $this;
	}

	/**
	 * @return string[]
	 */
	public static function getDependencies(): array
	{
		return [];
	}

	/**
	 * @return array
	 */
	public function getAliases(): array
	{
		return [];
	}
}
