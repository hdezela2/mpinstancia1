<?php
declare(strict_types=1);

namespace Linets\GasSetup\Setup\Patch\Data;

use Linets\GasSetup\Constants;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class EnableStoreSwitchInstaller implements DataPatchInterface
{
	const XML_PATH_ELASTICSEARCH_STORE_SWITCH = 'elasticsearch_storeswitch/general/store_multiselect';

	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;

	/**
	 * @var StoreRepositoryInterface
	 */
	private $storeManager;

	/**
	 * @var ScopeConfigInterface
	 */
	private $config;

	/**
	 * @var WriterInterface
	 */
	private $writer;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * EnableStoreSwitchInstaller constructor.
	 * @param ModuleDataSetupInterface $moduleDataSetup
	 * @param StoreRepositoryInterface $storeManager
	 * @param WriterInterface $writer
	 * @param ScopeConfigInterface $config
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		StoreRepositoryInterface $storeManager,
		WriterInterface $writer,
		ScopeConfigInterface $config,
		LoggerInterface $logger
	) {
		$this->moduleDataSetup = $moduleDataSetup;
		$this->storeManager = $storeManager;
		$this->writer = $writer;
		$this->config = $config;
		$this->logger = $logger;
	}

    /**
     * @return $this
     */
	public function apply()
	{
		$this->moduleDataSetup->startSetup();

		try {
			$gasStore = $this->storeManager->get(Constants::GAS_STORE_CODE);
			if ($gasStore->getId()) {
				$gasStoreId = $gasStore->getId();
				$actualValues = $this->config->getValue(
					self::XML_PATH_ELASTICSEARCH_STORE_SWITCH,
					ScopeInterface::SCOPE_STORE
				);
				$this->writer->save(
					self::XML_PATH_ELASTICSEARCH_STORE_SWITCH,
					$actualValues . ',' . $gasStoreId
				);
			}
		} catch (NoSuchEntityException $e) {
			$this->logger->critical('Cant set ' . Constants::GAS_STORE_CODE . ' store into StoreSwitch config');
		}

		$this->moduleDataSetup->endSetup();
        return $this;
	}

	public static function getDependencies(): array
	{
		return [];
	}

	public function getAliases(): array
	{
		return [];
	}


}
