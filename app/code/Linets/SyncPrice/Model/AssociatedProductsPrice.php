<?php

namespace Linets\SyncPrice\Model;

use Magento\Framework\App\ResourceConnection;

class AssociatedProductsPrice
{


    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resource;

    public function __construct(
        ResourceConnection $resourceConnection
    )
    {
        $this->resource = $resourceConnection;
    }

    public function update()
    {
        $response = true;
        $query = "UPDATE catalog_product_entity_decimal e "
            . "INNER JOIN catalog_product_entity cpe ON e.row_id = cpe.row_id "
            . "INNER JOIN catalog_product_relation cpr ON cpe.entity_id = cpr.child_id "
            . "INNER JOIN catalog_product_entity prn ON cpr.parent_id = prn.row_id "
            . "INNER JOIN (select a.row_id, min(d.price) price "
            . "FROM catalog_product_entity a "
            . "INNER JOIN catalog_product_relation b ON a.row_id = b.parent_id "
            . "INNER JOIN catalog_product_entity c ON b.child_id = c.entity_id "
            . "INNER JOIN marketplace_assignproduct_associated_products d ON c.entity_id = d.product_id "
            . "WHERE a.type_id = 'configurable' "
            . "GROUP BY a.row_id) T ON prn.row_id = T.row_id "
            . "SET e.value = T.price "
            . "WHERE e.attribute_id = 232 and e.store_id = 0;";
        $connection = $this->resource->getConnection();
        try {
            $connection->beginTransaction();
            $connection->query($query);
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            $response = false;
        }
        return $response;
    }

}