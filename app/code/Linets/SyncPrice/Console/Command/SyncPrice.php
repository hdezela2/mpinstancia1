<?php
/**
 * Copyright © LInets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SyncPrice\Console\Command;

use Linets\SyncPrice\Model\AssociatedProductsPrice;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Console\Cli;

class SyncPrice extends Command
{
    /**
     * @var \Linets\SyncPrice\Model\AssociatedProductsPrice
     */
    private $associatedProductsPrice;

    public function __construct(
        AssociatedProductsPrice $associatedProductsPrice,
        string $name = null
    )
    {
        $this->associatedProductsPrice = $associatedProductsPrice;
        parent::__construct($name);
    }

    /**
     * {@inheritdoc}
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $output->writeln('Running synconization...');
        $this->associatedProductsPrice->update();
        $output->writeln('Done');
        return Cli::RETURN_SUCCESS;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("product:associated-products:syncprice-offers-to-simples");
        $this->setDescription("Sync prices for configurable products childs and its associated offers");
        $this->setDefinition([]);
        parent::configure();
    }
}

