<?php
/**
 * Copyright © LInets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\SyncPrice\Cron;

use Linets\SyncPrice\Model\AssociatedProductsPrice;

class SyncPrice
{

    protected $logger;
    /**
     * @var \Linets\SyncPrice\Model\AssociatedProductsPrice
     */
    private $associatedProductsPrice;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        AssociatedProductsPrice $associatedProductsPrice
    )
    {
        $this->logger = $logger;
        $this->associatedProductsPrice = $associatedProductsPrice;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->associatedProductsPrice->update();
        $this->logger->addInfo("Cronjob SyncPrice is executed.");
    }
}

