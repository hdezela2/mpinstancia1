<?php
declare(strict_types=1);

namespace Linets\AssignProduct\Setup\Patch\Data;

use Formax\AssignProduct\Helper\MpAssignProduct;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Area;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Webkul\MpAssignProduct\Model\ItemsFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\Grid\Collection;

class AssignAssociatesInstaller implements DataPatchInterface
{
    /**
	 * @var ModuleDataSetupInterface
	 */
    protected $moduleDataSetup;

	/**
	 * @var Collection
	 */
	private $itemsCollection;

	/**
	 * @var MpAssignProduct
	 */
	private $mpAssignProduct;

	/**
	 * @var State
	 */
	private $state;

	/**
	 * @var ResourceConnection
	 */
	private $resource;

	/**
	 * PopulateInitialQuestions constructor.
	 * @param ModuleDataSetupInterface $moduleDataSetup
	 * @param Collection $itemsCollection
	 * @param MpAssignProduct $mpAssignProduct
	 * @param State $state
	 * @param ResourceConnection $resource
	 */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
		Collection $itemsCollection,
		MpAssignProduct $mpAssignProduct,
		State $state,
		ResourceConnection $resource
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
		$this->itemsCollection = $itemsCollection;
		$this->mpAssignProduct = $mpAssignProduct;
		$this->state = $state;
		$this->resource = $resource;
	}

	/**
	 * @throws LocalizedException
     *
     * @return $this
	 */
	public function apply()
	{
		$this->state->setAreaCode(Area::AREA_ADMINHTML);

		$collection = $this->itemsCollection->addFieldToFilter('type', Configurable::TYPE_CODE);
		$associates = $collection->getItems();
		$connection = $this->resource->getConnection();

		foreach ($associates as $associate) {
			if (empty($associate->getAssociates())) {
				$assignId = $associate->getId();

				$product = $this->mpAssignProduct->getProduct($associate->getProductId());
				$model = $this->mpAssignProduct->getAssignDataByAssignId($assignId);

				$associatesData = $this->mpAssignProduct->setProductAssociates($assignId, $product, $model, true);

				$table = $this->resource->getTableName('marketplace_assignproduct_items');
				$query = "UPDATE `" . $table . "` SET `associates` = '". $associatesData. "' WHERE id = $assignId ";
				$connection->query($query);
			}
		}
        return $this;
	}

    /**
     * @return array
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }
}
