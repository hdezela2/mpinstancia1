<?php
declare(strict_types=1);

namespace Linets\AssignProduct\Plugin\Frontend\Magento\Sales\Api\Data;

use Linets\GasSetup\Constants;
use Psr\Log\LoggerInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;

class Order
{

    /**
     * @var WebsiteRepositoryInterface
     */
    public $websiteRepository;

    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @param LoggerInterface $logger
     * @param WebsiteRepositoryInterface $websiteRepository
     */
    public function __construct(
        LoggerInterface $logger,
        WebsiteRepositoryInterface $websiteRepository
    ) {
        $this->logger = $logger;
        $this->websiteRepository = $websiteRepository;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param boolean $result
     * @return boolean
     */
    public function afterCanReorder(
        \Magento\Sales\Api\Data\OrderInterface $order,
        $result
    ) {
        $gasWebsiteId = $this->getWebsiteId();
        if ($order->getStore()->getWebsiteId() == $gasWebsiteId) {
            $result = true;
        }
        return $result;
    }

    /**
     * @return int|null
     */
    private function getWebsiteId()
    {
        try {
            $website = $this->websiteRepository->get(Constants::GAS_WEBSITE_CODE);
            $websiteId = (int) $website->getId();
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
            $websiteId = null;
        }

        return $websiteId;
    }
}
