<?php
declare(strict_types=1);

namespace Linets\AssignProduct\Helper;

use Formax\RegionalCondition\Model\RegionalCondition;
use Magento\Eav\Model\AttributeSetRepository;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;

class Data extends AbstractHelper
{

    const PRICE_ALERT_MSSG_PATH = 'mpassignproduct/settings/price_msg';
    /**
     * @var \Magento\Eav\Model\AttributeSetRepository
     */
    private $attrSetRepository;

    /**
     * @var AttributeRepositoryInterface $_attribute
     */
    private $_attribute;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        AttributeSetRepository $attrSetRepository,
        AttributeRepositoryInterface $attribute
    ) {
        $this->_attribute           = $attribute;
        $this->attrSetRepository    = $attrSetRepository;
        parent::__construct($context);
    }

    /**
     * @return string
     */
    public function getPriceAlertMessage()
    {
        return $this->scopeConfig->getValue(self::PRICE_ALERT_MSSG_PATH, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param int $attrSetId
     * @param \Formax\RegionalCondition\Model\RegionalCondition $rc
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getDeliveryDays(int $attrSetId, RegionalCondition $rc): int
    {
        $attrSet = $this->attrSetRepository->get($attrSetId);
        if (stripos($attrSet->getAttributeSetName(), 'envasado')) {
            $days = $rc->getMaxDaysPackingDispatch();
        } elseif (stripos($attrSet->getAttributeSetName(), 'granel')) {
            $days = $rc->getMaxDaysBulkDispatch();
        } else {
            $days = $rc->getMaximumDelivery();
        }
        return (int)$days;
    }

    /**
     * Method get Attribute
     *
     * @param int $entityTypeCode
     * @param string $attributeCode
     * @return int
     */
    public function getAttribute($entityTypeCode, $attributeCode)
    {
        try {
            $attribute = $this->_attribute->get($entityTypeCode, $attributeCode);
            $id = $attribute->getAttributeId();
            return $id;
        } catch (\Exception $err) {
            return 0;
        }
    }
}

