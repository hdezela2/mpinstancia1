/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper'
], function ($, wrapper) {
    'use strict';

    return function (targetModule) {

        var reloadPrice = targetModule.prototype._reloadPrice;
        var reloadPriceWrapper = wrapper.wrap(reloadPrice, function (original) {
            var result = original();
            let ids, names, shipping_regions = '';

            if ($('.additional-attributes-wrapper table .shipping-regions').length) {
                $('.additional-attributes-wrapper table .shipping-regions').remove();
            }
            if ($('.filter-option.cc-region-filter select option:enabled').length > 1) {
                $('.filter-option.cc-region-filter select option:enabled').not(':first-child').each(function () {
                    $(this).prop('disabled',true);
                });
            }

            if (this.simpleProduct) {
                ids = [];
                if (this.options.spConfig.region_ids[this.simpleProduct]) {
                    ids = this.options.spConfig.region_ids[this.simpleProduct].split(',');
                }
                names = [];
                if (this.options.spConfig.region_names[this.simpleProduct]) {
                    names = this.options.spConfig.region_names[this.simpleProduct].split(',');
                }

                for (let i = 0; i < names.length; i++) {
                    shipping_regions += names[i] + '<br>';
                }
                $('<tr class="shipping-regions"><th class="col label">Regiones de despacho</th><td>' + shipping_regions + '</td></tr>')
                    .insertAfter($('.additional-attributes-wrapper table tr:last-child'));
                if ($('#cc-supplier_filter-by-region').length) {
                    for (let i = 0; i < ids.length; i++) {
                        $('#cc-supplier_filter-by-region option[value=' + ids[i].trim() + ']').attr('disabled', false);
                    }
                }
                $('.filter-option.cc-region-filter select').val(null);
            }/* else {
                $('#cc-supplier_filter-by-region option').attr('disabled', false);
            }*/

            //return original value
            return result;
        });

        targetModule.prototype._reloadPrice = reloadPriceWrapper;
        return targetModule;
    };
});
