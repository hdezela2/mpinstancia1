<?php
/**
 * Copyright © Linets 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\ProductToSellerRegionFilter\Plugin\Frontend\Magento\ConfigurableProduct\Block\Product\View\Type;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Json\DecoderInterface;
use Magento\Framework\Json\EncoderInterface;

class Configurable
{
    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    private $jsonEncoder;
    /**
     * @var \Magento\Framework\Json\DecoderInterface
     */
    private $jsonDecoder;
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;

    /**
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Json\DecoderInterface $jsonDecoder
     */
    public function __construct(
        EncoderInterface $jsonEncoder,
        DecoderInterface $jsonDecoder,
        ProductRepository $productRepository
    )
    {
        $this->jsonDecoder = $jsonDecoder;
        $this->jsonEncoder = $jsonEncoder;
        $this->productRepository = $productRepository;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetJsonConfig(
        \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject,
        $result
    )
    {
        $config = $this->jsonDecoder->decode($result);
        foreach ($subject->getAllowProducts() as $simpleProduct) {
            $product = $this->productRepository->get($simpleProduct->getSku());
            $config['region_ids'][$simpleProduct->getId()] = $product->getResource()
                ->getAttribute('shipping_region_id')
                ->getFrontend()
                ->getValue($product);
            $config['region_names'][$simpleProduct->getId()] = $product->getResource()
                ->getAttribute('shipping_region_name')
                ->getFrontend()
                ->getValue($product);
        }

        return $this->jsonEncoder->encode($config);
    }
}

