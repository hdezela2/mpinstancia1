<?php

namespace Summa\CurrencyConverter\Model\Currency\Import;

use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Json\Helper\Data;
use Magento\Directory\Model\Currency\Import\AbstractImport;
use Magento\Store\Model\ScopeInterface;

class Fcc extends AbstractImport
{
    const CURRENCY_CONVERTER_URL = 'dccp_endpoint/greatbuy/currency_endpoint';
    const CURRENCY_CONVERTER_DELAY = 'currency/fcc/delay';
    const CURRENCY_CONVERTER_TIMEOUT = 'currency/fcc/timeout';

    /**
     * Json Helper
     * @var Data
     */
    protected $_jsonHelper;

    /**
     * Http Client Factory
     * @var ZendClientFactory
     */
    protected $_httpClientFactory;

    /**
     * Core scope config
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * Initialize dependencies
     * @param CurrencyFactory $currencyFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param ZendClientFactory $httpClientFactory
     * @param Data $jsonHelper
     */
    public function __construct(
        CurrencyFactory $currencyFactory,
        ScopeConfigInterface $scopeConfig,
        ZendClientFactory $httpClientFactory,
        Data $jsonHelper
    )
    {
        parent::__construct($currencyFactory);
        $this->_scopeConfig = $scopeConfig;
        $this->_httpClientFactory = $httpClientFactory->create();
        $this->_jsonHelper = $jsonHelper;
    }

    /**
     * Get URL
     * @return mixed
     */
    protected function getURL()
    {
        return $this->_scopeConfig->getValue(
            self::CURRENCY_CONVERTER_URL,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get Time Out
     * @return mixed
     */
    protected function getTimeOut()
    {
        return $this->_scopeConfig->getValue(
            self::CURRENCY_CONVERTER_TIMEOUT,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get Delay
     * @return mixed
     */
    protected function getDelay()
    {
        return $this->_scopeConfig->getValue(
            self::CURRENCY_CONVERTER_DELAY,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Retrieve rate
     * @param string $currencyFrom
     * @param string $currencyTo
     * @return float|null
     */
    protected function _convert($currencyFrom, $currencyTo)
    {
        $result = null;
        try {
            sleep($this->getDelay());
            $response = $this->_httpClientFactory->setUri($this->getURL())
                ->setConfig(['timeout' => $this->getTimeOut()])
                ->request('GET')
                ->getBody();
            $data = $this->_jsonHelper->jsonDecode($response);
            if ($data['success'] == 'OK') {
                foreach ($data['payload'] as $payload) {
                    if ($payload['moneda'] == $currencyTo) {
                        $result = 1 / $payload['valorPeso'];
                    } elseif ($currencyFrom == $payload['moneda']) {
                        $result = $payload['valorPeso'];
                    }
                }
            } else {
                $this->_messages[] = __('The service is not available at this time.');
                return null;
            }
        } catch (\Exception $e) {
            $this->_messages[] = __($e->getMessage());
        }
        return round($result, 4);
    }
}