<?php

namespace Summa\CombustiblesProductAttributes\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
    private $attributeSetFactory;
    private $categorySetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory,  AttributeSetFactory $attributeSetFactory, CategorySetupFactory $categorySetupFactory )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        $attributeSet = $this->attributeSetFactory->create();
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
        $data = [
            'attribute_set_name' => 'Default Combustibles',
            'entity_type_id' => $entityTypeId,
            'sort_order' => 202,
        ];
        $attributeSet->setData($data);
        $attributeSet->validate();
        $attributeSet->save();
        $attributeSet->initFromSkeleton($attributeSetId);
        $attributeSet->save();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);


//        attributo con configuraciones en false porque no se muestra en FE, ni se busca ni nada.
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'tipo_combustible',
            [
                'type'          => 'int',
                'backend'       => '',
                'frontend'      => '',
                'label'         => 'Tipo de Combustible',
                'input'         => 'select',
                'note'          => 'Tipo de Combustible',
                'class'         => '',
                'source'        => 'Summa\CombustiblesProductAttributes\Model\Config\Source\Options',
                'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible'       => true,
                'required'      => false,
                'user_defined'  => true,
                'default'       => '1',
                'searchable'    => false,
                'filterable'    => true,
                'comparable'    => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique'        => false,
                'apply_to'      => '',
                'option' => [
                    'values' => [],
                ],
            ]
        );
        //-------------- ADD attribute to set and group
        $entity_type_id = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attribute_set_name = 'Default Combustibles';
        $group_name = 'Product Details';


        $attribute_set_id=$eavSetup->getAttributeSetId($entity_type_id, $attribute_set_name);
        $attribute_group_id=$eavSetup->getAttributeGroupId($entity_type_id, $attribute_set_id, $group_name);

        $attributeCodeArray = ['region', 'tipo_combustible'];

        foreach ($attributeCodeArray as $attribute_code){
            $attribute_id=$eavSetup->getAttributeId($entity_type_id, $attribute_code);

            $eavSetup->addAttributeToSet($entity_type_id,$attribute_set_id, $attribute_group_id, $attribute_id);
            $eavSetup->addAttributeToGroup(
                $entity_type_id,
                $attribute_set_id,
                $attribute_group_id,
                $attribute_id,
                20
            );
        };

        $setup->endSetup();
    }
}