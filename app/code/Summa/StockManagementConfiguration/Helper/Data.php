<?php

namespace Summa\StockManagementConfiguration\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_STOCK_MANAGEMENT = 'stock_management_configuration/';

    public function getConfigValue($field, $websiteId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_WEBSITE, $websiteId
        );
    }

    public function getGeneralConfig($code, $websiteId = null)
    {
        $value = $this->getConfigValue(self::XML_PATH_STOCK_MANAGEMENT .'general/'. $code, $websiteId);
        return $this->getConfigValue(self::XML_PATH_STOCK_MANAGEMENT .'general/'. $code, $websiteId);
    }

    public function getDateDifference($firstDate, $secondDate)
    {
        $first = date_create($firstDate);
        $second = date_create($secondDate);
        $diffInYears = date_diff($first, $second)->format('%r%y');
        return $diffInYears * 12 + date_diff($first, $second)->format('%r%m');
    }

    public function getStockDisableText($websiteId)
    {
        return $this->getGeneralConfig('display_text', $websiteId);
    }
}