<?php

namespace Summa\MacrozoneFactors\Model\Import;

use Exception;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\ImportExport\Helper\Data as ImportHelper;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\Entity\AbstractEntity;
//use \Magento\ImportExport\Model\Import\AbstractEntity;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\ImportExport\Model\ResourceModel\Import\Data;
use \Magento\Framework\App\ResourceConnection;

class Macrozones extends AbstractEntity
{
    const ENTITY_CODE = 'macrozones';
    const TABLE = 'macrozones_factors';
    const ENTITY_ID_COLUMN = 'email';

    protected $needColumnCheck = true;
    protected $logInHistory = true;
    //protected $_permanentAttributes = ['id'];
    protected $resource;
    protected $serializer;
    protected $validColumnNames =
        [
            'email',
            'website',
            'store',
            'macrozone',
            'weight_range',
            'urban_factor',
            'rural_factor'
        ];

    protected $connection;

    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Eav\Model\Config $config,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        Json $serializer
    )
    {
        //parent::__construct($jsonHelper, $importExportData, $importData, $config, $resource, $resourceHelper, $string, $errorAggregator);
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->resource = $resource;
        $this->connection = $resource->getConnection(ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
        $this->serializer = $serializer;
        $this->initMessageTemplates();
    }

    public function getEntityTypeCode()
    {
        return static::ENTITY_CODE;
    }

    public function getValidColumnNames()
    {
        return $this->validColumnNames;
    }

    protected function _importData(): bool
    {
        switch ($this->getBehavior())
        {
            case Import::BEHAVIOR_DELETE:
                $this->deleteEntity();
                break;
            case Import::BEHAVIOR_REPLACE:
                $this->saveAndReplaceEntity();
                break;
            case Import::BEHAVIOR_APPEND:
                $this->saveAndReplaceEntity();
                break;
        }
        return true;
    }

    private function deleteEntity(): bool
    {
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch())
        {
            foreach ($bunch as $rowNum => $rowData)
            {
                $this->validateRow($rowData, $rowNum);
                if (!$this->getErrorAggregator()->isRowInvalid($rowNum))
                {
                    $rowId = $rowData[static::ENTITY_ID_COLUMN];
                    $rows[] = $rowId;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated())
                {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }

        if ($rows)
        {
            return $this->deleteEntityFinish(array_unique($rows));
        }

        return false;
    }

    protected function _saveValidatedBunches()
    {
        $source = $this->_getSource();
        $currentDataSize = 0;
        $bunchRows = [];
        $startNewBunch = false;
        $nextRowBackup = [];
        $maxDataSize = $this->_resourceHelper->getMaxDataSize();
        $bunchSize = $this->_importExportData->getBunchSize();
        $skuSet = [];

        $source->rewind();
        $this->_dataSourceModel->cleanBunches();

        while ($source->valid() || $bunchRows) {
            if ($startNewBunch || !$source->valid()) {
                $this->_dataSourceModel->saveBunch($this->getEntityTypeCode(), $this->getBehavior(), $bunchRows);

                $bunchRows = $nextRowBackup;
                $currentDataSize = strlen($this->serializer->serialize($bunchRows));
                $startNewBunch = false;
                $nextRowBackup = [];
            }
            if ($source->valid()) {
                try {
                    $rowData = $source->current();
                } catch (\InvalidArgumentException $e) {
                    $this->addRowError($e->getMessage(), $this->_processedRowsCount);
                    $this->_processedRowsCount++;
                    $source->next();
                    continue;
                }

                $this->_processedRowsCount++;

                if ($this->validateRow($rowData, $source->key())) {
                    // add row to bunch for save
                    $rowData = $this->_prepareRowForDb($rowData);
                    $rowSize = strlen($this->jsonHelper->jsonEncode($rowData));

                    $isBunchSizeExceeded = $bunchSize > 0 && count($bunchRows) >= $bunchSize;

                    if ($currentDataSize + $rowSize >= $maxDataSize || $isBunchSizeExceeded) {
                        $startNewBunch = true;
                        $nextRowBackup = [$source->key() => $rowData];
                    } else {
                        $bunchRows[$source->key()] = $rowData;
                        $currentDataSize += $rowSize;
                    }
                }
                $source->next();
            }
        }
        $this->_processedEntitiesCount = count($skuSet);

        return $this;
    }

    private function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch())
        {
            $entityList = [];
            foreach ($bunch as $rowNum => $row)
            {
                if (!$this->validateRow($row, $rowNum))
                {
                    continue;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated())
                {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }

                $rowId = $row[static::ENTITY_ID_COLUMN];
                $rows[] = $rowId;
                $columnValues = [];

                foreach ($this->getAvailableColumns() as $columnKey)
                {
                    $columnValues[$columnKey] = $row[$columnKey];
                }

                $entityList[$rowId][] = $columnValues;
                $this->countItemsCreated += (int) !isset($row[static::ENTITY_ID_COLUMN]);
                $this->countItemsUpdated += (int) isset($row[static::ENTITY_ID_COLUMN]);
            }

            if (Import::BEHAVIOR_REPLACE === $behavior)
            {
                if ($rows && $this->deleteEntityFinish(array_unique($rows)))
                {
                    $this->saveEntityFinish($entityList);
                }
            }
            elseif (Import::BEHAVIOR_APPEND === $behavior)
            {
                $this->saveEntityFinish($entityList);
            }
        }
    }

    /**
     * @param array $entityData
     * @return bool
     */
    private function saveEntityFinish(array $entityData): bool
    {
        if ($entityData)
        {
            $tableName = $this->connection->getTableName(static::TABLE);
            $rows = [];

            foreach ($entityData as $entityRows)
            {
                foreach ($entityRows as $row)
                {
                    $rows[] = $row;
                }
            }

            if ($rows)
            {
                $this->connection->insertOnDuplicate($tableName, $rows, $this->getAvailableColumns());
                return true;
            }

            return false;
        }
        return false;
    }

    private function deleteEntityFinish(array $entityIds): bool
    {
        if ($entityIds)
        {
            try
            {
                $this->countItemsDeleted += $this->connection->delete($this->connection->getTableName(static::TABLE), $this->connection->quoteInto(static::ENTITY_ID_COLUMN . ' IN (?)', $entityIds));
                return true;
            }
            catch (Exception $e)
            {
                return false;
            }
        }
        return false;
    }

    private function getAvailableColumns(): array
    {
        return $this->validColumnNames;
    }

    public function validateRow(array $rowData, $rowNum) : bool
    {
        $email = $rowData['email'] ?? '';
        if (!$email)
        {
            $this->addRowError('email', $rowNum);
        }

        $website = $rowData['website'] ?? '';
        if(!$website)
        {
            $this->addRowError('website', $rowNum);
        }

        $store = $rowData['store'] ?? '';
        if(!$store)
        {
            $this->addRowError('store', $rowNum);
        }

        $macrozone = $rowData['macrozone'] ?? '';
        if(!$macrozone)
        {
            $this->addRowError('macrozone', $rowNum);
        }

        $range = $rowData['weight_range'] ?? '';
        if(!$range)
        {
            $this->addRowError('weight_range', $rowNum);
        }

        if (isset($this->_validatedRows[$rowNum]))
        {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    private function initMessageTemplates()
    {
        $this->addMessageTemplate(
            'email',
            __('The seller cannot be empty.')
        );

        $this->addMessageTemplate(
            'website',
            __('The website cannot be empty.')
        );

        $this->addMessageTemplate(
            'store',
            __('The store cannot be empty.')
        );

        $this->addMessageTemplate(
            'macrozone',
            __('The macrozone cannot be empty.')
        );

        $this->addMessageTemplate(
            'weight_range',
            __('The weight range cannot be empty')
        );
    }
}
