<?php

namespace Summa\MacrozoneFactors\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;

class Macrozone extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'summa_macrozonefactors_macrozone';
    protected $_cacheTag = 'summa_macrozonefactors_macrozone';
    protected $_eventPrefix = 'summa_macrozonefactors_macrozone';

    public function __construct(Context $context, \Magento\Framework\Registry $registry, AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = [])
    {
        $this->_init('Summa\MacrozoneFactors\Model\ResourceModel\Macrozone');
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
