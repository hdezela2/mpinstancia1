<?php

namespace Summa\MacrozoneFactors\Model\ResourceModel\Macrozone;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'summa_macrozonefactors_macrozone';
    protected $_eventObject = 'macrozone_collection';

    protected function _construct()
    {
        $this->_init('Summa\MacrozoneFactors\Model\Macrozone', 'Summa\MacrozoneFactors\Model\ResourceModel\Macrozone');
    }
}
