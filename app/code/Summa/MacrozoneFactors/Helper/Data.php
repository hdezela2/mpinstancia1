<?php

namespace Summa\MacrozoneFactors\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\ScopeInterface;
use Summa\MacrozoneFactors\Model\Macrozone;
use Summa\MacrozoneFactors\Model\MacrozoneFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Store\Model\StoreManagerInterface as StoreManager;

class Data extends AbstractHelper
{
    protected $macrozoneFactory;
    protected $storeManager;
    protected $customer;

    public function __construct(Context $context, MacrozoneFactory $factory, CustomerFactory $customer, StoreManager $manager)
    {
        parent::__construct($context);
        $this->macrozoneFactory = $factory;
        $this->customer = $customer;
        $this->storeManager = $manager;
    }

    public function getMacrozoneFactorsBySeller($wkSellerId)
    {
        $websiteCode = $this->getWebsiteCode();
        $data = [];
        $seller = $this->getSeller($wkSellerId);
        $model = $this->macrozoneFactory->create();
        $collection = $model->getCollection()
            ->addFieldToFilter('email', $seller->getEmail())
            ->addFieldToFilter('website', $websiteCode)
            ->getData();
        foreach ($collection as $macrozoneFactor)
        {
            if(!isset($data[$macrozoneFactor['macrozone']]))
                $data[$macrozoneFactor['macrozone']] = [];

            $data[$macrozoneFactor['macrozone']][] = $macrozoneFactor;
        }
        return $data;
    }

    private function getSeller($id)
    {
        if ($id !== null) {
            return $this->customer->create()->load($id);
        }

        return null;
    }

    public function getWebsiteCode(): ?string
    {
        try
        {
            $websiteCode = $this->storeManager->getWebsite()->getCode();
        }
        catch (LocalizedException $localizedException)
        {
            $websiteCode = null;
            $this->_logger->error($localizedException->getMessage());
        }
        return $websiteCode;
    }
}
