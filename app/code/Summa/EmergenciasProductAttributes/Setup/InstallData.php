<?php
/**
 * Created by PhpStorm.
 * User: diegosana
 * Date: 18/12/19
 * Time: 09:54
 */

namespace Summa\EmergenciasProductAttributes\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

//
//        $eavSetup->removeAttribute(
//            \Magento\Catalog\Model\Product::ENTITY,
//            'estado_situacion');
//        $eavSetup->removeAttribute(
//            \Magento\Catalog\Model\Product::ENTITY,
//            'region');


        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'estado_situacion',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'ESTADO DE SITUACIÓN',
                'input' => 'select',
                'class' => '',
                'source' => 'Summa\EmergenciasProductAttributes\Model\Config\Source\SituationOptions',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => ''
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'region',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'REGIÓN',
                'input' => 'select',
                'class' => '',
                'source' => 'Summa\EmergenciasProductAttributes\Model\Config\Source\AllregionOptions',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => ''
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'linked_products',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'backend' => '',
                'frontend' => '',
                'label' => 'LINKED PRODUCTS',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => ''
            ]
        );
    }
}