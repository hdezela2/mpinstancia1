<?php
/**
 * Created by PhpStorm.
 * User: diegosana
 * Date: 18/12/19
 * Time: 10:04
 */

namespace Summa\EmergenciasProductAttributes\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class SituationOptions extends AbstractSource
{
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
            ['label'=>'', 'value'=>''],
            ['label'=>'Prevención', 'value'=>'1'],
            ['label'=>'Emergencia', 'value'=>'2'],
            ['label'=>'Normal', 'value'=>'3'],
            ['label'=>'Retiro En Tienda', 'value'=>'4']
        ];
        return $this->_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }
}