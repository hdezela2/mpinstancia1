<?php
/**
 * Created by PhpStorm.
 * User: diegosana
 * Date: 18/12/19
 * Time: 10:04
 */

namespace Summa\EmergenciasProductAttributes\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Zend\Text\Table\Table;

class AllregionOptions extends AbstractSource
{
    protected $_allregion;

    public function __construct(
        \Magento\Directory\Model\Config\Source\Allregion $allregion
    )
    {
        $this->_allregion = $allregion;
    }

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        if(!$this->_options) {
            $options = $this->_allregion->toOptionArray()[1]['value'];
            array_unshift($options, ['label'=>'', 'value'=>'']);
            $this->_options = $options;
        }
        return $this->_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }

    /**
     * Retrieve flat column definition
     *
     * @return array
     */
    public function getFlatColumns()
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        return [
            $attributeCode => [
                'unsigned' => false,
                'default' => null,
                'extra' => null,
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => true,
                'comment' => 'Custom Attribute Options  ' . $attributeCode . ' column',
            ],
        ];
    }
}