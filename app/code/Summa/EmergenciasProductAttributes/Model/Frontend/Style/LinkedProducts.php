<?php

namespace Summa\EmergenciasProductAttributes\Model\Frontend\Style;

class LinkedProducts extends \Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend
{
    public function getValue(\Magento\Framework\DataObject $object)
    {
        $linkedSkus = parent::getValue($object);

        if (!is_string($linkedSkus)) {
            return null;
        }

        $skus = explode(',', $linkedSkus);
        if (is_array($skus) && count($skus) >= 1) {
            return implode(' - ', $skus);
        }

        return null;
    }

}