<?php
/**
 * Created by PhpStorm.
 * User: diegosana
 * Date: 17/12/19
 * Time: 11:28
 */

namespace Summa\CombustiblesSetUp\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Cms\Model\BlockFactory;

class UpgradeData implements UpgradeDataInterface
{


    private $blockFactory;

    private $storeRepository;

    public function __construct(
        \Magento\Store\Api\StoreRepositoryInterface $storeRepository,
        BlockFactory $blockFactory
    )
    {
        $this->storeRepository= $storeRepository;
        $this->blockFactory = $blockFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            $store = $this->storeRepository->get('combustibles');
            $websiteId = 0;
            if ($store->getId()){
                $websiteId = $store->getId();
            }
            $cmsBlockData = [
                'title' => 'How to use this page',
                'identifier' => 'how-to-use-this',
                'content' => "<h1>Write your custom cms block content.......</h1>",
                'is_active' => 1,
                'stores' => [$websiteId],
                'sort_order' => 3
            ];

            $this->blockFactory->create()->setData($cmsBlockData)->save();

        }
    }
}