<?php

namespace Summa\OfferService\Model;

use Braintree\Exception;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Webapi\Rest\Request;
use Summa\OfferService\Api\UpdateOffer as UpdateOfferInterface;

class UpdateOffer implements UpdateOfferInterface
{
    protected $productFactory;

    protected $offerFactory;

    protected $helper;

    protected $logger;

    protected $productMapping = [];
    protected $customerMapping = [];
    protected $assignProductMapping = [];
    protected $assignProductData = [];
    //protected $marketplaceProductMapping = [];

    const ID = 'email';
    const SKU = 'sku';
    const PRODUCT_TYPE = 'product_type';
    const SKU_ASSIGN = 'sku_assign';
    const QTY = 'qty';
    const IN_STOCK = 'in_stock';
    const PRICE = 'price';
    const CONFIGURABLE_VARIATIONS = 'configurable_variations';
    const BASE_PRICE = 'base_price';
    const SHIPPING_PRICE = 'shipping_price';
    const ASSEMBLY_PRICE = 'assembly_price';
    protected $_permanentAttributes = [self::ID];


    /** @var \Magento\Framework\App\ResourceConnection */
    protected $_resource;

    /** @var \Magento\Framework\Model\ResourceModel\Iterator */
    protected $_iterator;

    /** @var \Magento\Eav\Model\Entity\AttributeFactory */
    protected $_entityAttribute;

    /**
     * @var Request
     */
    private $request;

    public function __construct(
        Request $request,
        \Webkul\MpAssignProduct\Model\ItemsFactory $offerFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Psr\Log\LoggerInterface $logger,
        \Cf\AssignProductsMass\Helper\Todo $helper,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Model\ResourceModel\Iterator $iterator,
        \Magento\Eav\Model\Entity\AttributeFactory $entityAttribute
    ) {
        $this->productFactory = $productFactory;
        $this->offerFactory = $offerFactory;
        $this->request = $request;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->_resource = $resource;
        $this->_entityAttribute = $entityAttribute;
        $this->_iterator = $iterator;
    }

    public function updateOffer($offerData)
    {
        $this->getProductMapping($offerData);
        $this->getCustomerMapping($offerData);
        $this->getAssignProductMapping();
        //$this->getMarketplaceProductMapping();

        $ary_response = [];
        foreach ($offerData as $value)
        {
	    $this->logger->info('Start Item API');
	    $this->logger->info(print_r($offerData,1));
            $item = $value->toArray();

            try{
                $result = $this->helper->_saveAssign(
                    $item,
                    $this->productMapping,
                    $this->customerMapping,
                    $this->assignProductMapping,
                    true
                );

                if(!$result['error']){
                    $valid = [
                        "code" => "200",
                        "message" => "Record saved successfully.",
                        "sku" => $value->getSku(),
                        "seller" => $value->getEmail(),
                    ];
                    $ary_response[] = $valid;
                }else{
                    $rowError = [
                        "code" => '301',
                        "message" => $result['return'],
                        "sku" => $value->getSku(),
                        "seller" => $value->getEmail(),
                    ];
                    $ary_response[] = $rowError;
                }
            }catch (Exception $e){
                $rowError = [
                    "code" => '301',
                    "message" => $e,
                    "sku" => $value->getSku(),
                    "seller" => $value->getEmail(),
                ];
                $ary_response[] = $rowError;
            }
        }
        $this->logger->info(json_encode($ary_response));
        return $ary_response;
    }

    public function getProductMapping($offerData)
    {
        $entityList = [];
        foreach ($offerData as $key => $value) {
            $rowTitle = $value->getEmail();
            $listTitle[$key] = $rowTitle;
            $entityList[$rowTitle][] = [
                self::ID => $value->getEmail(),
                self::SKU => $value->getSku(),
                self::PRODUCT_TYPE => $value->getProductType(),
                self::SKU_ASSIGN => $value->getSkuAssign(),
                self::QTY => $value->getQty(),
                self::IN_STOCK => $value->getInStock(),
                self::PRICE => $value->getPrice(),
                self::CONFIGURABLE_VARIATIONS => $value->getConfigurableVariations(),
                self::BASE_PRICE => $value->getBasePrice(),
                self::ASSEMBLY_PRICE => $value->getAssemblyPrice(),
                self::SHIPPING_PRICE => $value->getShippingPrice()
            ];
        }

        $entityList = array_values($entityList);

        $skus = [];
        $configurableVariations = [];

        if (!empty($entityList)){
            foreach ($entityList as $value) {
                $skus[] = array_column($value, 'sku');
                $configurableVariations[] = array_column($value, 'configurable_variations');
            }
        }
        $skus = array_merge(...$skus);
        $configurableVariations = array_merge(...$configurableVariations);
        $configurableVariations = array_filter($configurableVariations);

        if ($configurableVariations) {
            foreach ($configurableVariations as $variation) {
                $__products = explode('|', $variation);
                foreach ($__products as $key => $value) {
                    $_item = explode(',', $value);
                    foreach ($_item as $key1 => $value1) {
                        $__item = explode('=', $value1);
                        if (trim($__item[0]) == 'sku') {
                            $skus[] = trim($__item[1]);
                        }
                    }
                }
            }
        }

        if ($skus) {
            $connection = $this->_resource->getConnection();
            $cpe = $this->_resource->getTableName('catalog_product_entity');
            $voucherAttribute = $this->_entityAttribute->create()->loadByCode(\Magento\Catalog\Model\Product::ENTITY, 'voucher_product_type');
            $isquimicoAttribute = $this->_entityAttribute->create()->loadByCode(\Magento\Catalog\Model\Product::ENTITY, 'is_quimico');
            $select = $connection->select()
                ->from(['e' => $cpe], ['entity_id', 'sku', 'type_id', 'row_id'])
                ->joinLeft(
                    ["vpt" => $voucherAttribute->getBackend()->getTable()],
                    "vpt.row_id=e.row_id AND vpt.attribute_id={$voucherAttribute->getId()}",
                    ['voucher_product_type' => 'vpt.value']
                )
                ->joinLeft(
                    ["iq" => $isquimicoAttribute->getBackend()->getTable()],
                    "iq.row_id=e.row_id AND iq.attribute_id={$isquimicoAttribute->getId()}",
                    ['is_quimico' => 'iq.value']
                )
                ->where('sku IN(?)', $skus);
            $this->_iterator->walk(
                $select,
                [[$this, 'productMappingCallback']]
            );
        }
    }

    public function productMappingCallback($args)
    {
        $row = $args['row'];
        $this->productMapping[$row['sku']] = [
            'sku' => $row['sku'],
            'row_id' => $row['row_id'],
            'product_id' => $row['entity_id'],
            'product_type' => $row['type_id'],
            'voucher_product_type' => $row['voucher_product_type'],
            'is_quimico' => $row['is_quimico']
        ];
        return $this;
    }

    public function getCustomerMapping($offerData)
    {
        $emailsArr = [];
        foreach ($offerData as $key => $value) {
            $emailsArr[] = $value->getEmail();
        }
        if (count($emailsArr)) {
            $connection = $this->_resource->getConnection();
            $ce = $this->_resource->getTableName('customer_entity');
            $select = $connection->select()
                ->from(['e' => $ce], ['email', 'entity_id', 'website_id'])
                ->where('email IN(?)', $emailsArr);
            $this->_iterator->walk(
                $select,
                [[$this, 'customerMappingCallback']]
            );
        }
        return $this;
    }

    public function customerMappingCallback($args)
    {
        $row = $args['row'];
        $this->customerMapping[$row['email']] = [
            'customer_id' => $row['entity_id'],
            'customer_website' => $row['website_id']
        ];
        return $this;
    }

    public function getAssignProductMapping()
    {
        $productIds = array_column($this->productMapping, 'product_id');
        $customerIds = array_column($this->customerMapping, 'customer_id');

        if ($productIds && $customerIds) {
            $connection = $this->_resource->getConnection();
            $mai = $this->_resource->getTableName('marketplace_assignproduct_items');
            $select = $connection->select()
                ->from(['e' => $mai], ['product_id', 'seller_id', 'id', 'base_price', 'price', 'shipping_price', 'assembly_price', 'image', 'qty', 'status'])
                ->where('product_id IN(?)', $productIds)
                ->where('seller_id IN(?)', $customerIds);
            $this->_iterator->walk(
                $select,
                [[$this, 'assignProductMappingCallback']]
            );
        }
    }

    public function assignProductMappingCallback($args)
    {
        $row = $args['row'];
        $this->assignProductMapping[$row['product_id']][$row['seller_id']] = $row['id'];
        $this->assignProductData[$row['id']] = [
            'price' => $row['price'],
            'base_price' => $row['base_price'],
            'shipping_price' => $row['shipping_price'],
            'assembly_price' => $row['assembly_price'],
            'qty' => $row['qty'],
            'status' => $row['status'],
            'image' => $row['image']
        ];
        return $this;
    }

//    public function getMarketplaceProductMapping()
//    {
//        $productIds = array_column($this->productMapping, 'product_id');
//
//        if ($productIds) {
//            $connection = $this->_resource->getConnection();
//            $mp = $this->_resource->getTableName('marketplace_product');
//            $select = $connection->select()
//                ->from(['e' => $mp], ['mageproduct_id', 'seller_id'])
//                ->where('mageproduct_id IN(?)', $productIds);
//            $this->_iterator->walk(
//                $select,
//                [[$this, 'marketplaceProductMappingCallback']]
//            );
//        }
//    }
//
//    public function marketplaceProductMappingCallback($args)
//    {
//        $row = $args['row'];
//        $this->marketplaceProductMapping[$row['mageproduct_id']] = $row['seller_id'];
//        return $this;
//    }
}
