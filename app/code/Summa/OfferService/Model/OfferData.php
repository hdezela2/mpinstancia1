<?php

namespace Summa\OfferService\Model;

use Summa\OfferService\Api\Data\OfferData as OfferDataInterface;

class OfferData implements OfferDataInterface
{
    protected $email;
    protected $sku;
    protected $productType;
    protected $skuAssign;
    protected $status;
    protected $qty;
    protected $inStock;
    protected $price;
    protected $configurableVariations;
    protected $basePrice;
    protected $assemblyPrice;
    protected $shippingPrice;

    /**
     * @param $sku
     * @return $this
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
        return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $productType
     * @return $this
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
        return $this;
    }

    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param $skuAssign
     * @return $this
     */
    public function setSkuAssign($skuAssign)
    {
        $this->skuAssign = $skuAssign;
        return $this;
    }

    public function getSkuAssign()
    {
        return $this->skuAssign;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->skuAssign;
    }

    /**
     * @param $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
        return $this;
    }

    public function getQty()
    {
        return $this->qty;

    }

    /**
     * @param $inStock
     * @return $this
     */
    public function setInStock($inStock)
    {
        $this->inStock = $inStock;
        return $this;
    }

    public function getInStock()
    {
        return $this->inStock;
    }

    /**
     * @param $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param $configurableVariations
     * @return $this
     */
    public function setConfigurableVariations($configurableVariations)
    {
        $this->configurableVariations = $configurableVariations;
        return $this;
    }

    public function getConfigurableVariations()
    {
        return $this->configurableVariations;
    }

    public function setBasePrice($basePrice)
    {
        $this->basePrice = $basePrice;
    }

    public function getBasePrice()
    {
        return $this->basePrice;
    }

    public function setAssemblyPrice($assemblyPrice)
    {
        $this->assemblyPrice = $assemblyPrice;
    }

    public function getAssemblyPrice()
    {
        return $this->assemblyPrice;
    }

    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;
    }

    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    public function toArray()
    {
        return array(
            'email' => $this->email,
            'sku' => $this->sku,
            'product_type' => $this->productType,
            'sku_assign' => $this->skuAssign,
            'status' => $this->status,
            'in_stock' => $this->inStock,
            'qty' => $this->qty,
            'price' => $this->price,
            'configurable_variations' => $this->configurableVariations,
            'base_price' => $this->basePrice,
            'assembly_price' => $this->assemblyPrice,
            'shipping_price' => $this->shippingPrice
        );
    }

}
