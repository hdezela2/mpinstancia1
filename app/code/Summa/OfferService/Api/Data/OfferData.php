<?php
namespace Summa\OfferService\Api\Data;

interface OfferData
{
    const EMAIL = 'email';
    const SKU = 'sku';
    const PRODUCT_TYPE = 'product_type';
    const SKU_ASSIGN = 'sku_assign';
    const STATUS = 'status';
    const QTY = 'qty';
    const IN_STOCK = 'in_stock';
    const PRICE = 'price';
    const CONFIGURABLE_VARIATIONS = 'configurable_variations';
    const BASE_PRICE = 'base_price';
    const ASSEMBLY_PRICE = 'assembly_price';
    const SHIPPING_PRICE = 'shipping_price';

    /**
     * Set the sku.
     *
     * @api
     * @return string
     */
    public function setSku($sku);
    /**
     * Gets the sku.
     *
     * @api
     * @return string
     */
    public function getSku();

    /**
     * @param $email
     * @return string
     */
    public function setEmail($email);

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param $productType
     * @return string
     */
    public function setProductType($productType);

    /**
     * @return mixed
     */
    public function getProductType();

    /**
     * @param $skuAssign
     * @return mixed
     */
    public function setSkuAssign($skuAssign);

    /**
     * @return string
     */
    public function getSkuAssign();

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status);

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @param $qty
     * @return string
     */
    public function setQty($qty);

    /**
     * @return string
     */
    public function getQty();

    /**
     * @param $inStock
     * @return string
     */
    public function setInStock($inStock);

    /**
     * @return mixed
     */
    public function getInStock();

    /**
     * @param $price
     * @return mixed
     */
    public function setPrice($price);

    /**
     * @return mixed
     */
    public function getPrice();

    /**
     * @param $configurableVariations
     * @return string
     */
    public function setConfigurableVariations($configurableVariations);

    /**
     * @return string
     */
    public function getConfigurableVariations();

    /**
     * @param $basePrice
     * @return mixed
     */
    public function setBasePrice($basePrice);

    /**
     * @return mixed
     */
    public function getBasePrice();

    /**
     * @param $assemblyPrice
     * @return mixed
     */
    public function setAssemblyPrice($assemblyPrice);

    /**
     * @return mixed
     */
    public function getAssemblyPrice();

    /**
     * @param $shippingPrice
     * @return mixed
     */
    public function setShippingPrice($shippingPrice);

    /**
     * @return mixed
     */
    public function getShippingPrice();

    //agregar otras funciones de valores de la oferta.

}