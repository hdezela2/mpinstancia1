<?php
namespace Summa\OfferService\Api;

interface UpdateOffer
{
    /**
     * @api
     * @param \Summa\OfferService\Api\Data\OfferData[] $offerData
     * @return array
     */
    public function updateOffer($offerData);
}