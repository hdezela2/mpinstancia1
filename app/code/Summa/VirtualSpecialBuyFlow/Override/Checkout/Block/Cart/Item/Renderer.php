<?php

namespace Summa\VirtualSpecialBuyFlow\Override\Checkout\Block\Cart\Item;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Message\InterpretationStrategyInterface;
use Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface;
use Summa\VirtualSpecialBuyFlow\Helper\Data as VirtualSpecialHelper;
use Formax\VoucherBuyFlow\Helper\Data as VoucherHelper;

class Renderer extends \Formax\VoucherBuyFlow\Override\Checkout\Block\Cart\Item\Renderer

{

    /**
     * @var VirtualSpecialHelper
     */
    protected $virtualSpecialHelper;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $assignProductHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Helper\Product\Configuration $productConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param InterpretationStrategyInterface $messageInterpretationStrategy
     * @param \Webkul\MpAssignProduct\Helper\Data $assignProductHelper
     * @param VirtualSpecialHelper $virtualSpecialHelper
     * @param array $data
     * @param ItemResolverInterface|null $itemResolver
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Product\Configuration $productConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Module\Manager $moduleManager,
        InterpretationStrategyInterface $messageInterpretationStrategy,
        \Webkul\MpAssignProduct\Helper\Data $assignProductHelper,
        VirtualSpecialHelper $virtualSpecialHelper,
        VoucherHelper $voucherHelper,
        array $data = [],
        ItemResolverInterface $itemResolver = null
    ) {
        parent::__construct(
            $context,
            $productConfig,
            $checkoutSession,
            $imageBuilder,
            $urlHelper,
            $messageManager,
            $priceCurrency,
            $moduleManager,
            $messageInterpretationStrategy,
            $assignProductHelper,
            $voucherHelper,
            $data,
            $itemResolver
        );

        $this->assignProductHelper = $assignProductHelper;
        $this->virtualSpecialHelper = $virtualSpecialHelper;
    }

    /**
     * Get row total
     *
     * @param object $item
     * @return false|string
     */
    public function getRowTotal($item)
    {
        $price = false;
        if ($this->virtualSpecialHelper->isCombustibleWebsite()) {
            $productId = $item->getProduct()->getId();
            $product = $this->virtualSpecialHelper->getCurrentProduct($productId);
            if ($product->getTypeId() == 'virtual') {
                $options = $item->getBuyRequest()->getData();
                $price = 0;
                if (array_key_exists('mpassignproduct_id', $options)) {
                    $mpAssignId = $options['mpassignproduct_id'];
                    $price = $this->assignProductHelper->getAssignProductPrice($mpAssignId);
                    $amount = $this->virtualSpecialHelper->getAmount($item);
                    $price = $item->getQty()*$amount;
                }
                $price = $this->priceCurrency->format($price);
            }
        }else{
            return parent::getRowTotal($item);
        }

        return $price;
    }

    /**
     * Get unit price total
     *
     * @param object $item
     * @return false|string
     */
    public function getUnitPrice($item)
    {
        $price = false;
        $productId = $item->getProduct()->getId();
        $product = $this->virtualSpecialHelper->getCurrentProduct($productId);

        if ($this->virtualSpecialHelper->isCombustibleWebsite() && $product->getTypeId() == 'virtual')
        {
            $options = $item->getBuyRequest()->getData();
            $price = 0;
            if (array_key_exists('mpassignproduct_id', $options))
            {
                $mpAssignId = $options['mpassignproduct_id'];
                $price = $this->assignProductHelper->getAssignProductPrice($mpAssignId);
            }
        }else{
            return parent::getUnitPrice($item);
        }

        return $price;


    }

    /**
     * Get earned profit
     *
     * @param object $item
     * @return false|string
     */
    public function getProfit($item)
    {
        $profit = false;
        $productId = $item->getProduct()->getId();
        $product = $this->virtualSpecialHelper->getCurrentProduct($productId);

        if ($this->virtualSpecialHelper->isCombustibleWebsite()
            && $product->getTypeId() == 'virtual') {
            $options = $item->getBuyRequest()->getData();
            $price = 0;
            if (array_key_exists('mpassignproduct_id', $options)) {
                $mpAssignId = $options['mpassignproduct_id'];
                $price = $this->assignProductHelper->getAssignProductPrice($mpAssignId);
                $amount = $this->virtualSpecialHelper->getAmount($item);
                $price = ($amount+(($price*$amount)/100))*$item->getQty();
                $profit = __('Pay %1 and earn %2', $this->priceCurrency->format($amount*$item->getQty()), $this->priceCurrency->format($price));
            }
        }else{
            return parent::getProfit($item);
        }

        return $profit;
    }

    /**
     * Get amount
     *
     * @param object $item
     * @return float
     */
    public function getAmount($item)
    {
        if ($this->virtualSpecialHelper->isCombustibleWebsite()){
            return $this->virtualSpecialHelper->getAmount($item);
        }
        return parent::getAmount($item);

    }
}
