<?php

namespace Summa\VirtualSpecialBuyFlow\Plugin\Model\Quote;

use Magento\Quote\Model\Quote\TotalsCollector as Collector;
use Summa\VirtualSpecialBuyFlow\Helper\Data as VirtualSpecialHelper;
use Webkul\MpAssignProduct\Helper\Data as AssignProductHelper;

class TotalsCollector
{
    /**
     * @var VirtualSpecialHelper
     */
    protected $VirtualSpecialHelper;

    /**
     * @var AssignProductHelper
     */
    protected $assignProductHelper;

    /**
     * TotalsCollector constructor.
     * @param VirtualSpecialHelper $virtualHelper
     * @param AssignProductHelper $assignProductHelper
     */
    public function __construct(
        VirtualSpecialHelper $virtualHelper,
        AssignProductHelper $assignProductHelper
    ) {
        $this->VirtualSpecialHelper = $virtualHelper;
        $this->assignProductHelper = $assignProductHelper;
    }

    public function aroundCollect(
        Collector $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote $quote
    ) {
        if ($this->VirtualSpecialHelper->isCombustibleWebsite()) {
            foreach ($quote->getAllItems() as $item) {
                $product = $item->getProduct();
                if ($product->getTypeId() == 'virtual') {
                    $options = $item->getBuyRequest()->getData();
                    $amount = $this->VirtualSpecialHelper->getAmount($item);
                    if (array_key_exists('mpassignproduct_id', $options)) {
                        $amount = $this->VirtualSpecialHelper->getAmount($item);
                    }
                    if ($amount > 0) {
                        $item->setCustomPrice($amount);
                        $item->setOriginalCustomPrice($amount);
                        $item->setRowTotal($amount*$item->getQty());
                        $item->getProduct()->setIsSuperMode(true);
                    }
                }
            }
        }

        $result = $proceed($quote);

        return $result;
    }
}
