<?php

namespace Summa\VirtualSpecialBuyFlow\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;
use Summa\VirtualSpecialBuyFlow\Helper\Data as VirtualSpecialHelper;

class CheckoutCartProductAddAfterObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
    * @var Json
    */
    private $serializer;

    /**
     * @var VirtualSpecialHelper
     */
    private $virtualHelper;

    /**
     * CheckoutCartProductAddAfterObserver constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param VirtualSpecialHelper $virtualSpecialHelper
     * @param Json|null $serializer
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        VirtualSpecialHelper $virtualSpecialHelper,
        json $serializer = null
    )
    {
        $this->virtualHelper = $virtualSpecialHelper;
        $this->request = $request;
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
    }

    /**
     * Add order information into GA block to render on checkout success pages
     *
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(EventObserver $observer)
    {
        if ($this->virtualHelper->isCombustibleWebsite()) {
            /* @var \Magento\Quote\Model\Quote\Item $item */
            $item = $observer->getQuoteItem();
            $product = $item->getProduct();
            $price = 0;

            if ($product->getTypeId() == 'virtual') {
                $data = $product->getTypeInstance(true)->getOrderOptions($product);
                $options = isset($data['info_buyRequest']) ? $data : ['info_buyRequest' => []];

                $price = (int)$this->request->getParam('combustible_amount');
                if ($price > 0 && is_array($options) && count($options) > 0) {
                    array_merge($options['info_buyRequest'], ['combustible_amount' => $price]);
                    $item->setProductOptions($options);
                }

                if ((int)$options['info_buyRequest']['combustible_amount'] == 0) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('You must enter an amount.'));
                }
            }
        }
    }
}