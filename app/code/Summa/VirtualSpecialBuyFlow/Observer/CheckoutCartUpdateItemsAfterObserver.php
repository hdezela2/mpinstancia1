<?php

namespace Summa\VirtualSpecialBuyFlow\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;
use Summa\VirtualSpecialBuyFlow\Helper\Data as VirtualSpecialHelper;

class CheckoutCartUpdateItemsAfterObserver implements ObserverInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var VirtualSpecialHelper
     */
    private $virtualHelper;

    /**
     * CheckoutCartProductAddAfterObserver constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param VirtualSpecialHelper $virtualSpecialHelper
     * @param Json|null $serializer
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        VirtualSpecialHelper $virtualSpecialHelper,
        json $serializer = null
    )
    {
        $this->virtualHelper = $virtualSpecialHelper;
        $this->request = $request;
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
    }

    /**
     * * Add order information into GA block to render on checkout success pages
     *
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(EventObserver $observer)
    {
        if ($this->virtualHelper->isCombustibleWebsite()) {
            $quoteItems = $observer->getCart()->getQuote()->getAllVisibleItems();
            $post = $this->request->getPostValue();
            $additionalOptions = [];

            if (isset($post['cart']) && is_array($post['cart'])) {
                foreach ($post['cart'] as $keyItemToUpdate => $itemToUpdate) {
                    foreach ($quoteItems as $item) {
                        if ((int)$item->getId() === (int)$keyItemToUpdate) {
                            $product = $item->getProduct();
                            if ($product->getTypeId() == 'virtual') {
                                $price = isset($itemToUpdate['amount']) ? (int)$itemToUpdate['amount'] : 0;
                                if ($price > 0) {
                                    if ($additionalOption = $item->getOptionByCode('info_buyRequest')) {
                                        $additionalOptions = (array) $this->serializer->unserialize($additionalOption->getValue());
                                        $newOptions = [];

                                        foreach ($additionalOptions as $key => $option) {
                                            $newOptions[$key] = $key == 'combustible_amount' ? $price : $option;
                                        }

                                        $item->addOption([
                                            'code' => 'info_buyRequest',
                                            'value' => $this->serializer->serialize($newOptions)
                                        ]);

                                    }
                                } else {
                                    $error = __('You must enter an amount.');
                                    throw new \Magento\Framework\Exception\LocalizedException($error);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}