<?php

namespace Summa\VirtualSpecialBuyFlow\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Summa\VirtualSpecialBuyFlow\Helper\Data as VirtualSpecialHelper;
use Psr\Log\LoggerInterface;

class MpAssignItemsBeforeObserver implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var VirtualSpecialHelper
     */
    private $virtualHelper;

    /**
     * @param VirtualSpecialHelper $virtualSpecialHelper
     * @param LoggerInterface $logger
     */
    public function __construct(
        VirtualSpecialHelper $virtualSpecialHelper,
        LoggerInterface $logger
    ) {
        $this->virtualHelper = $virtualSpecialHelper;
        $this->logger = $logger;
    }

    /**
     * add notification status change
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        if ($this->virtualHelper->isCombustibleWebsite()) {
            $model = $observer->getEvent()->getObject();
            try {
                $price = str_replace(',', '.', $model->getData('price'));
                $model->setData('price', $price);
            } catch (\Exception $e) {
                $this->logger->error('Notification AssignProductItemsSaveAfter - ' . $e->getMessage());
            }
        }
    }
}
