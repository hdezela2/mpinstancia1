<?php

namespace Summa\VirtualSpecialBuyFlow\Helper;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Chilecompra\CombustiblesRenewal\Model\Constants;

/**
 * Data helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
    * @var \Magento\Framework\Registry
    */
    protected $registry;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * @var \Summa\CombustiblesSetUp\Helper\Data
     */
    protected $combustibleHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\Registry $registry
     * @param Json $serializer
     */

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Summa\CombustiblesSetUp\Helper\Data $combustibleHelper
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param Json $serializer
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Summa\CombustiblesSetUp\Helper\Data $combustibleHelper,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        Json $serializer
    ) {
        $this->storeManager = $storeManager;
        $this->registry = $registry;
        $this->productRepository = $productRepository;
        $this->serializer = $serializer;
        $this->combustibleHelper = $combustibleHelper;
        parent::__construct($context);
    }

    /**
     * Get Store code
     *
     * @return string
     */
    public function getCurrentStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }

    /**
     * Get current product item
     *
     * @param int $id
     * @return \Magento\Catalog\Model\ProductRepository
     */
    public function getCurrentProduct($id)
    {
        $key = 'voucher_product_' . $id;
        if ($this->isCombustibleWebsite()){
            $key = 'combustible_product_' . $id;
        }
        $product = $this->registry->registry($key);

        if ($product === null) {
            $product = $this->productRepository->getById($id);
            $this->registry->register($key, $product);
        }

        return $product;
    }

    /**
     * Get custom amount typed from item quote
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return int
     */
    public function getAmount($item)
    {
        $product = $item->getProduct();
        $data = $product->getTypeInstance(true)->getOrderOptions($product);
        $amount = isset($data['info_buyRequest']['combustible_amount'])
            ? (int)$data['info_buyRequest']['combustible_amount'] : 0;

        return $amount;
    }

    public function getCombustibleWebsiteCode(){
        return $this->combustibleHelper->getCombustibleWebsiteCode();
    }

    public function getCombustibleRenewalWebsiteCode(){
        return Constants::WEBSITE_CODE;
    }

    public function getWebsite(){
        return $this->combustibleHelper->getWebsite();
    }

    public function isCombustibleWebsite(){
        return  in_array($this->getCurrentStoreCode(), [$this->getCombustibleWebsiteCode(), $this->getCombustibleRenewalWebsiteCode()]);
    }

}
