<?php
namespace Summa\TemporaryPriceAdjustment\Block\Adminhtml\TemporaryProductPrice;

use Magento\Backend\Block\Widget\Form\Container;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Edit extends Container
{
    protected $_coreRegistry = null;

    public function __construct(
        Context $context,
        CollectionFactory $categoryCollectionFactory,
        Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_productPrice';
        $this->_blockGroup = 'Webkul_CategoryProductPrice';

        parent::_construct();

        $this->buttonList->update('save', 'onclick', 'return confirmFunction(\'save\')');
        $this->buttonList->update('save', 'data_attribute', [
            'mage-init' => ['button' => ['event' => 'mysave', 'target' => '#edit_form']],
        ]);
    }

    public function getHeaderText()
    {
        return __('Temporary Update Product Price');
    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getFormHtml()
    {
        // get the current form as html content.
        $html = parent::getFormHtml();
        //Append the phtml file after the form content.
        $html .= $this->setTemplate('Summa_TemporaryPriceAdjustment::category-tree.phtml')->toHtml();
        return $html;
    }

    public function getCategoriesTree()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $collection */
        $collection = $this->categoryCollectionFactory->create()->addAttributeToSelect('*');

        $sellerCategory = [
            Category::TREE_ROOT_ID => [
                'value' => Category::TREE_ROOT_ID,
                'optgroup' => null,
            ],
        ];

        foreach ($collection as $category) {
            $catId = $category->getId();
            $catParentId = $category->getParentId();
            foreach ([$catId, $catParentId] as $categoryId) {
                if (!isset($sellerCategory[$categoryId])) {
                    $sellerCategory[$categoryId] = ['value' => $categoryId];
                }
            }

            $sellerCategory[$catId]['is_active'] = $category->getIsActive();
            $sellerCategory[$catId]['label'] = $category->getName();
            $sellerCategory[$catParentId]['optgroup'][] = &$sellerCategory[$catId];
        }
        return json_encode($sellerCategory[Category::TREE_ROOT_ID]['optgroup']);

    }

    public function getSaveUrl()
    {
        return $this->getUrl('*/*/run');
    }
}