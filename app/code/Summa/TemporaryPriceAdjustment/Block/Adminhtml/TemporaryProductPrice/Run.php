<?php
namespace Summa\TemporaryPriceAdjustment\Block\Adminhtml\TemporaryProductPrice;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;

class Run extends Template
{
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}
