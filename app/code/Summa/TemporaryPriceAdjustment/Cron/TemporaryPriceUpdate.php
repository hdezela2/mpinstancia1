<?php

namespace Summa\TemporaryPriceAdjustment\Cron;

class TemporaryPriceUpdate
{

    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var \Summa\TemporaryPriceAdjustment\Helper\Data
     */
    protected $helper;

    /**
     * @var \Summa\TemporaryPriceAdjustment\Logger\Logger $logger
     */
    protected $logger;

    /**
     * @var \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceUpdateFactory
     */
    protected $updateAdjustment;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $dateTime;

    /**
     * @var \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceAdjustmentFactory
     */
    protected $adjustment;

    /**
     * @var \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceAdjustment\CollectionFactory
     */
    protected $adjustmentCollectionFactory;

    /**
     * TemporaryPriceUpdate constructor.
     * @param \Magento\Framework\App\State $appState
     * @param \Summa\TemporaryPriceAdjustment\Logger\Logger $logger
     * @param \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceAdjustment\CollectionFactory $adjustmentCollection
     * @param \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceUpdateFactory $updateAdjustment
     * @param \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceAdjustmentFactory $adjustment
     * @param \Summa\TemporaryPriceAdjustment\Helper\Data $helper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $dateTime
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        \Summa\TemporaryPriceAdjustment\Logger\Logger $logger,
        \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceAdjustment\CollectionFactory $adjustmentCollection,
        \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceUpdateFactory $updateAdjustment,
        \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceAdjustmentFactory $adjustment,
        \Summa\TemporaryPriceAdjustment\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $dateTime
    ) {

        $this->logger = $logger;
        $this->appState = $appState;
        $this->dateTime = $dateTime;
        $this->adjustmentCollectionFactory = $adjustmentCollection;
        $this->updateAdjustment = $updateAdjustment;
        $this->adjustment = $adjustment;
        $this->helper = $helper;
    }

    public function execute()
    {
        $date = $this->dateTime->date()->format("Y-m-d");
        $collectionRestore = $this->adjustmentCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('status', \Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_RUNNIG)
            ->addFieldToFilter('end_time', ['lt' => $date]); //todas las que tenemos que restaurar.


        foreach ($collectionRestore as $item) {

            try {
                $itemUpdate = $this->adjustment->create()->load($item['entity_id']);
                $updateToRestore = $this->updateAdjustment->create()->load($item['entity_id'], 'adjustment_id');

                if ($updateToRestore){
                    $result = $this->helper->restoreAdjustment($updateToRestore);
                    $msg = isset($result['msg']) ? $result['msg'] : '';

                    if (!$result['error']) {
                        $this->logger->info(' Task Success update temporary product offer => ' . $msg);
                        $itemUpdate->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_DONE);
                        $itemUpdate->save();

                    } else {
                        $this->logger->critical(' Task Error update temporary product offer => ' . $msg);
                        $itemUpdate->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_ERROR);
                        $itemUpdate->setComment($msg);
                        $itemUpdate->save();
                    }
                }else{
                    $this->logger->critical(' Task Error update temporary product offer - The Adjustment To Restore doesnt exist');
                    $itemUpdate->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_ERROR);
                    $itemUpdate->setComment('The Adjustment To Restore doesnt exist');
                    $itemUpdate->save();
                }
            } catch (\Exception $e) {
                $info = ' Task Error updating temporary product offer => itemID:' .
                    $itemUpdate->getEntityId(). ' ProductSKU:'. $itemUpdate->getSku(). ' Seller:'. $itemUpdate->getEmail();
                $this->logger->info($info);
                $this->logger->critical($info);
                $this->logger->critical('Exception message => ' . $e->getMessage());
                $itemUpdate->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_ERROR);
                $itemUpdate->setComment($info);
                $itemUpdate->save();
            }

        }

        $collectionToApply = $this->adjustmentCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('status', \Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_TOAPPLY)
            ->addFieldToFilter('start_time', ['lteq' => $date]); //todas las que tenemos que ejecutar.
        foreach ($collectionToApply as $item) {

            try {
                $adjustmentToApply = $this->adjustment->create()->load($item['entity_id']);

                $result = $this->helper->ApplyAdjustment($adjustmentToApply);
                $msg = isset($result['msg']) ? $result['msg'] : '';

                if (!$result['error']) {
                    $this->logger->info(' Task Success update linked product offer => ' . $msg);
                    if ($adjustmentToApply){
                        $adjustmentToApply->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_RUNNIG);
                        $adjustmentToApply->save();
                    }
                } else {
                    $this->logger->critical(' Task Error creating temporary product offer => ' . $msg);
                    $adjustmentToApply->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_ERROR);
                    $adjustmentToApply->setComment($msg);
                    $adjustmentToApply->save();
                }
            } catch (\Exception $e) {
                $info = ' Task Error updating temporary product offer => itemID:' .
                    $adjustmentToApply->getEntityId(). ' ProductSKU:'. $adjustmentToApply->getSku(). ' Seller:'. $adjustmentToApply->getEmail();
                $this->logger->info($info);
                $this->logger->critical($info);
                $this->logger->critical('Exception message => ' . $e->getMessage());
                $adjustmentToApply->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_ERROR);
                $adjustmentToApply->setComment($info);
                $adjustmentToApply->save();
            }

        }
        return $this;
    }
}