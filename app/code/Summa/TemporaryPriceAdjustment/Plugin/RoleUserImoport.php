<?php

namespace Summa\TemporaryPriceAdjustment\Plugin;

use Formax\AgreementInfo\Model\ResourceModel\Agreement\CollectionFactory as AgreementFactory;


class RoleUserImoport

{
    /**
     * @var \Magento\ImportExport\Model\Import\ConfigInterface
     */
    protected $_importConfig;

    protected $session;

    protected $agreementFactory;

    public function __construct(
        \Magento\ImportExport\Model\Import\ConfigInterface $importConfig,
        \Magento\Backend\Model\Auth\Session $session,
        AgreementFactory $agreementFactory
    )
    {
        $this->_importConfig = $importConfig;
        $this->session = $session;
        $this->agreementFactory=$agreementFactory;
    }

    public function aftertoOptionArray(\Magento\ImportExport\Model\Source\Import\Entity $subject, $result)

    {
        $adminSession = $this->session;
        $user = $adminSession->getUser()->getData('user_rest_id_active_agreement');
        $collection = $this->agreementFactory->create();
        $jp=false;
        foreach($collection as $convenio) {
            if ($user == $convenio->getData('agreement_id') ) {
                $jp=true;
            }
        }
        $options = [];
        $options[] = ['label' => __('-- Please Select --'), 'value' => ''];
        foreach ($this->_importConfig->getEntities() as $entityName => $entityConfig) {
           if ($jp && $entityName != "temporarypriceadjustment") {
              continue;
           }
            $options[] = ['label' => __($entityConfig['label']), 'value' => $entityName];
        }
        return $options;


    }


}
