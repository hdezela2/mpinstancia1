<?php

namespace Summa\TemporaryPriceAdjustment\Controller\Adminhtml\Adjustment;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Run extends Action
{
    /** Temporary Price Adjustment - Maximum Percentage */
    const XML_PATH_PERCENTAGE = 'summa_temporary_update/days/percentage';

    /** @var PageFactory */
    protected $resultPageFactory;

    /** @var JsonHelper */
    protected $jsonHelper;

    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonHelper $jsonHelper,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Summa_TemporaryPriceAdjustment::index');
    }

    public function execute()
    {
        $categories = $this->getRequest()->getParam('categories');
        $percentage = $this->getRequest()->getParam('update_percentage');

        $scope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE;
        $maximumPercentageAllowed = $this->scopeConfig->getValue(self::XML_PATH_PERCENTAGE, $scope);
        if ($maximumPercentageAllowed > 0 && $percentage > $maximumPercentageAllowed) {
            $this->messageManager->addError(__('Maximum percentage allowed to set is %1%', $maximumPercentageAllowed));
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('*/*/modify');
        }

        if (!empty($categories) && $percentage != '') {
            $resultPage = $this->resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->prepend(__('Reading Data'));
            return $resultPage;
        }
        $this->messageManager->addError(__('Please Fill Data'));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/modify');
    }
}
