<?php

namespace Summa\TemporaryPriceAdjustment\Controller\Adminhtml\Adjustment;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResourceConnection;
use Summa\TemporaryPriceAdjustment\Helper\Data;

class Save extends \Magento\Backend\App\Action
{
    /** @var \Magento\Framework\View\Result\PageFactory */
    private $resultPageFactory;

    protected $_categoryCollectionFactory;

    protected $_productRepository;

    protected $_registry;

    /** @var Data */
    protected $_temporaryPriceAdjustmentHelper;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $_storeManager;

    /** @var \Magento\Customer\Api\CustomerRepositoryInterface */
    protected $_customerRepository;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Webkul\MpAssignProduct\Model\ItemsFactory $assignItems,
        \Webkul\MpAssignProduct\Model\AssociatesFactory $assignAssociates,
        \Webkul\CategoryProductPrice\Model\ProductPriceFactory $productPrice,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProductType,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Registry $registry,
        Data $temporaryPriceAdjustmentHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        array $data = []
    ) {
        parent::__construct($context);
        $this->productFactory = $productFactory;
        $this->assignAssociates = $assignAssociates;
        $this->assignItems = $assignItems;
        $this->categoryFactory = $categoryFactory;
        $this->jsonHelper = $jsonHelper;
        $this->configurableProductType = $configurableProductType;
        $this->date = $date;
        $this->productPrice = $productPrice;
        $this->resultPageFactory = $resultPageFactory;
        $this->resourceConnection = $resourceConnection;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->_registry = $registry;
        $this->_temporaryPriceAdjustmentHelper = $temporaryPriceAdjustmentHelper;
        $this->_storeManager = $storeManager;
        $this->_customerRepository = $customerRepository;
    }

    public function execute()
    {
        $productData = $this->getRequest()->getParam('product');
        $pricePercentage = $this->getRequest()->getParam('pricePercentage');

        try {
            $responseData = $this->updatePrice($productData, $pricePercentage);
            $result['error'] = 0;
            $result['msg'] = '<div class="wk-mu-success wk-mu-box">'.__('Successfully updated '.$responseData['name']).'</div>';
        } catch (\Exception $e) {
            $result['error'] = 1;
            $result['msg'] = '<div class="wk-mu-error wk-mu-box">'.__('Error in updating price').'</div>';
        }
        
        $result = $this->jsonHelper->jsonEncode($result);
        $this->getResponse()->representJson($result);
    }

    public function updatePrice($productData, $percentage)
    {
        //for CM with Configurable products
        if ($productData['child'] != "false") {
            $assignProducts = $this->assignAssociates->create()->getCollection()->addFieldToFilter('id', $productData['id']);
            foreach ($assignProducts as $assignProduct) {
                $childProduct = $this->productFactory->create()->load($assignProduct->getProductId());
                $parentProduct = $this->productFactory->create()->load($assignProduct->getParentProductId());

                // get seller email
                $assignItem = $this->assignItems->create()->load($assignProduct['parent_id']);
                $sellerId = $assignItem->getSellerId();
                $customer = $this->_customerRepository->getById($sellerId);
                $email = $customer->getEmail();

                // get skus
                $childSku = $childProduct->getSku();
                $parentSku = $parentProduct->getSku();

                // get original price
                $originalPrice = $assignProduct->getPrice();

                // get new temporary price
                if ($originalPrice > 0) {
                    $newTemporaryPrice = round((($percentage*$originalPrice/100) + $originalPrice),0,PHP_ROUND_HALF_DOWN);
                } else {
                    $newTemporaryPrice = 0;
                }

                // get website of the product
                $website = $childProduct->getWebsiteIds()[0];
                $websiteCode = $this->_storeManager->getWebsite($website)->getCode();

                // collect data to add new temporary price adjustment
                $temporaryAdjustmentData = [
                    'email' => $email,
                    'sku' => $parentSku,
                    'website' => $websiteCode,
                    'new_temporary_price' => $newTemporaryPrice,
                    'sku_assign' => $childSku
                ];

                // add new temporary price adjustment
                $this->_temporaryPriceAdjustmentHelper->addTemporaryPriceAdjustment($temporaryAdjustmentData);
            }
            $data['name'] = $childProduct->getName();
            return $data;
        }

        // for CM with no configurable products
        $assignItem = $this->assignItems->create()->load($productData['id']);
        $product = $this->productFactory->create()->load($assignItem->getProductId());

        // get seller email
        $sellerId = $assignItem->getSellerId();
        $customer = $this->_customerRepository->getById($sellerId);
        $email = $customer->getEmail();

        // get skus
        $parentSku = $product->getSku();

        // get original price
        $originalPrice = $assignItem->getPrice();

        // get new temporary price
        if ($originalPrice > 0) {
            $newTemporaryPrice = round((($percentage*$originalPrice/100) + $originalPrice),0,PHP_ROUND_HALF_DOWN);
        } else {
            $newTemporaryPrice = 0;
        }

        // get website of the product
        $website = $product->getWebsiteIds()[0];
        $websiteCode = $this->_storeManager->getWebsite($website)->getCode();

        // collect data to add new temporary price adjustment
        $temporaryAdjustmentData = [
            'email' => $email,
            'sku' => $parentSku,
            'website' => $websiteCode,
            'new_temporary_price' => $newTemporaryPrice
        ];

        // add new temporary price adjustment
        $this->_temporaryPriceAdjustmentHelper->addTemporaryPriceAdjustment($temporaryAdjustmentData);

        $data['name'] = $product->getName();
        return $data;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Summa_TemporaryPriceAdjustment::index');
    }
}