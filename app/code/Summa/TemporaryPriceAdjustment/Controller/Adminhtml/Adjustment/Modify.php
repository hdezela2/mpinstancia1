<?php

namespace Summa\TemporaryPriceAdjustment\Controller\Adminhtml\Adjustment;

class Modify extends \Magento\Backend\App\Action
{
    /** @var \Magento\Framework\View\Result\PageFactory */
    private $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Summa_TemporaryPriceAdjustment::index');
        $resultPage->getConfig()->getTitle()->prepend(__('Update Product Prices Temporarily'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Summa_TemporaryPriceAdjustment::index');
    }
}