<?php
namespace Summa\TemporaryPriceAdjustment\Controller\Adminhtml\Adjustment;

use Magento\Backend\App\Action\Context;

class Finish extends \Magento\Backend\App\Action
{
    public function __construct(
        Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = [];
        $total = (int) $this->getRequest()->getParam('row');
        $skipCount = (int) $this->getRequest()->getParam('skip');
        $total = $total - $skipCount;
        $msg = '<div class="wk-mu-success wk-mu-box">';
        $msg .= __('Total %1 Product(s) Price Updated.', $total);
        $msg .= '</div>';
        $msg .= '<div class="wk-mu-note wk-mu-box">';
        $msg .= __('Finished Execution.');
        $msg .= '</div>';
        $result['msg'] = $msg;
        $result = $this->jsonHelper->jsonEncode($result);
        $this->getResponse()->representJson($result);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Summa_TemporaryPriceAdjustment::index');
    }
}
