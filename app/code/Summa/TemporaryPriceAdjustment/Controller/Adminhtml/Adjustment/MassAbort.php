<?php

namespace Summa\TemporaryPriceAdjustment\Controller\Adminhtml\Adjustment;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;

class MassAbort extends Action
{

    /**
        * @var Filter
        */
    private $filter;

    /**
     * MassAbort constructor.
     * @param Context $context
     * @param Filter $filter
     * @param \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceAdjustment\CollectionFactory $adjustmentCollection
     * @param \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceUpdateFactory $updateAdjustment
     * @param \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceAdjustmentFactory $adjustment
     * @param \Summa\TemporaryPriceAdjustment\Helper\Data $helper
     * @param \Magento\Framework\Stdlib\DateTime $dateTime
     */
    public function __construct(
        Context $context,
        Filter $filter,
        \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceAdjustment\CollectionFactory $adjustmentCollection,
        \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceUpdateFactory $updateAdjustment,
        \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceAdjustmentFactory $adjustment,
        \Summa\TemporaryPriceAdjustment\Helper\Data $helper,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\State $appState,
        \Summa\TemporaryPriceAdjustment\Logger\Logger $logger,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
    ) {
        parent::__construct($context);
        $this->logger = $logger;
        $this->appState = $appState;
        $this->adjustmentCollectionFactory = $adjustmentCollection;
        $this->updateAdjustment = $updateAdjustment;
        $this->adjustment = $adjustment;
        $this->helper = $helper;
        $this->request = $request;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    /**
     * Execute action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $selectedItemIds = $this->request->getPostValue('selected');
        $collectionRestore = $this->adjustmentCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('entity_id', ['in' => $selectedItemIds])
            ->addFieldToFilter('status', \Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_RUNNIG);
        
        foreach ($collectionRestore as $item) {

            try {
                $itemUpdate = $this->adjustment->create()->load($item['entity_id']);
                $updateToRestore = $this->updateAdjustment->create()->load($item['entity_id'], 'adjustment_id');
                
                if ($updateToRestore){
                    $result = $this->helper->restoreAdjustment($updateToRestore);
                    $msg = isset($result['msg']) ? $result['msg'] : '';

                    if (!$result['error']) {
                        $this->logger->info('Success update temporary product offer => ' . $msg);
                        $itemUpdate->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_DONE);
                        $itemUpdate->save();
                        $this->messageManager->addSuccess(__("Temporary Price Readjustment has been successfully cancelled"));
                        $resultRedirect->setPath('temporarypriceadjustment/adjustment/index');
                        return $resultRedirect;
                    } else {
                        $this->logger->critical('Error in update temporary product offer => ' . $msg);
                        $itemUpdate->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_ERROR);
                        $itemUpdate->setComment($msg);
                        $itemUpdate->save();
                        $this->messageManager->addError(__("There is some error in Temporary Price Readjustment"));
                        $resultRedirect->setPath('temporarypriceadjustment/adjustment/index');
                        return $resultRedirect;
                    }
                    $output->writeln($msg);
                }else{
                    //no existe el elemento con el cual restaurar
                    //editar el Update con error
                    //hacer log del mensaje.
                    $this->logger->critical('Update temporary product offer - The Adjustment To Restore doesnt exist');
                    $itemUpdate->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_ERROR);
                    $itemUpdate->setComment('The Adjustment To Restore doesnt exist');
                    $itemUpdate->save();
                    $this->messageManager->addError(__("Update Temporary Product Offer - The Adjustment To Restore doesnt exist"));
                    $resultRedirect->setPath('temporarypriceadjustment/adjustment/index');
                    return $resultRedirect;
                }
            } catch (\Exception $e) {
                $info = 'Error updating temporary product offer => itemID:' .
                    $itemUpdate->getEntityId(). ' ProductSKU:'. $itemUpdate->getSku(). ' Seller:'. $itemUpdate->getEmail();
                $this->logger->info($info);
                $this->logger->critical($info);
                $this->logger->critical('Exception message => ' . $e->getMessage());
                $itemUpdate->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_ERROR);
                $itemUpdate->setComment($info);
                $itemUpdate->save();
                $this->messageManager->addError(__("Error updating temporary product offer => itemID".$itemUpdate->getEntityId(). " ProductSKU:". $itemUpdate->getSku(). " Seller:". $itemUpdate->getEmail()));
                $resultRedirect->setPath('temporarypriceadjustment/adjustment/index');
                return $resultRedirect;
            }
        }
    }
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Summa_TemporaryPriceAdjustment::index');
    }
}
