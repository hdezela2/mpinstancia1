<?php

namespace Summa\TemporaryPriceAdjustment\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Summa\TemporaryPriceAdjustment\Model\TemporaryPriceAdjustmentFactory;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;


class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const STATUS_TOAPPLY = 1;
    const STATUS_RUNNIG = 2;
    const STATUS_DONE = 0;
    const STATUS_ERROR = 3;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customer;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $helperAssignProduct;

    /**
     * @var \Webkul\MpAssignProduct\Model\ItemsFactory
     */
    protected $itemsFactory;

    /**
     * @var \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceUpdate\CollectionFactory
     */
    protected $updateCollection;

    /**
     * @var \Formax\Offer\Model\OfferFactory
     */
    protected $offerFactory;

    protected $logger;
    protected $updateAdjustment;
    protected $_connection;

    /** @var ScopeConfigInterface */
    protected $_scopeConfigInterface;

    /** @var TemporaryPriceAdjustmentFactory */
    protected $_temporaryPriceAdjustmentFactory;

    /**
     * @var TimezoneInterface
     */
    private $dateTime;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Customer\Model\CustomerFactory $customer
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Webkul\MpAssignProduct\Model\ItemsFactory $itemsFactory
     * @param \Webkul\MpAssignProduct\Helper\Data $helperAssignProduct
     * @param \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceUpdateFactory $updateAdjustment
     * @param \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceUpdate\CollectionFactory $updateCollection
     * @param \Formax\Offer\Model\OfferFactory $offerFactory
     * @param TimezoneInterface $dateTime
     * @param \Summa\TemporaryPriceAdjustment\Logger\Logger $logger
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context                                                      $context,
        \Magento\Customer\Model\CustomerFactory                                                    $customer,
        \Magento\Catalog\Api\ProductRepositoryInterface                                            $productRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface                                          $customerRepository,
        \Magento\Framework\App\ResourceConnection                                                  $resourceConnection,
        \Webkul\MpAssignProduct\Model\ItemsFactory                                                 $itemsFactory,
        \Webkul\MpAssignProduct\Helper\Data                                                        $helperAssignProduct,
        \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceUpdateFactory                          $updateAdjustment,
        \Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceUpdate\CollectionFactory $updateCollection,
        \Formax\Offer\Model\OfferFactory                                                           $offerFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface                                       $dateTime,
        \Summa\TemporaryPriceAdjustment\Logger\Logger                                              $logger,
        ScopeConfigInterface                                                                       $scopeConfigInterface,
        \Magento\Store\Model\StoreManagerInterface                                                 $storeManager,
        TemporaryPriceAdjustmentFactory        $temporaryPriceAdjustmentFactory
    ) {

        parent::__construct($context);
        $this->customer = $customer;
        $this->storeManager = $storeManager;
        $this->helperAssignProduct = $helperAssignProduct;
        $this->productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
        $this->updateAdjustment = $updateAdjustment;
        $this->updateCollection = $updateCollection;
        $this->connection = $resourceConnection->getConnection();
        $this->itemsFactory = $itemsFactory;
        $this->offerFactory = $offerFactory;
        $this->dateTime = $dateTime;
        $this->_scopeConfigInterface = $scopeConfigInterface;
        $this->logger = $logger;
        $this->_temporaryPriceAdjustmentFactory = $temporaryPriceAdjustmentFactory;
    }

    public function addTemporaryPriceAdjustment($data)
    {
        $numberOfDays = $this->_scopeConfigInterface->getValue('summa_temporary_update/days/number_of_days');

        $startDate = date_create(date("Y-m-d"));
        $endDate = date_create(date("Y-m-d"));
        date_add($endDate, date_interval_create_from_date_string($numberOfDays." days"));
        $endDate = date_format($endDate, "Y-m-d");

        $temporaryPriceAdjustment = $this->_temporaryPriceAdjustmentFactory->create();
        $temporaryPriceAdjustment->setEmail($data['email']);
        $temporaryPriceAdjustment->setSku($data['sku']);
        if (isset($data['sku_assign'])) {
            $temporaryPriceAdjustment->setSkuAssign($data['sku_assign']);
        }
        $temporaryPriceAdjustment->setWebsite($data['website']);
        $temporaryPriceAdjustment->setNewTemporaryPrice($data['new_temporary_price']);
        $temporaryPriceAdjustment->setStartTime($startDate);
        $temporaryPriceAdjustment->setEndTime($endDate);
        $temporaryPriceAdjustment->setStatus(self::STATUS_TOAPPLY);
        $temporaryPriceAdjustment->save();
    }

    public function getAvailableStatuses()
    {
        return [
            self::STATUS_TOAPPLY => __('To apply'),
            self::STATUS_RUNNIG => __('Running'),
            self::STATUS_DONE => __('Done'),
            self::STATUS_ERROR => __('Error')];
    }

    public function restoreAdjustment($updateToRestore){
        $result = ['error' => false, 'msg' => ''];

        if (!$updateToRestore->getId()) {
            $result['error'] = true; $result['msg'] .= __('problem with the ajustment to restore');
        }

        // update in table marketplace_assignproduct_items
        $assignOffer = $this->itemsFactory->create()->load($updateToRestore->getAssignId());
        if ($assignOffer->getId() > 0) {
            //casos especiales en otros websites.
            if ($assignOffer->getType() === 'configurable') {
                $associate = $this->helperAssignProduct->getAssociateByProduct($assignOffer->getId(), $updateToRestore->getProductId());
                if (!$associate) {
                    $updateToRestore->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_ERROR);
                    $updateToRestore->save();
                    $result['error'] = true; $result['msg'] .= __('error generated when trying to load associate offer %1, UpdateId: ', $updateToRestore->getAssignId());
                    return $result;
                }
                $associate->setPrice($updateToRestore->getOriginalPrice());
                $associate->save();
            } else {
                $assignOffer->setPrice($updateToRestore->getOriginalPrice());
                $assignOffer->save();
            }
            $updateToRestore->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_DONE);
            $updateToRestore->save();
        }else{
            $updateToRestore->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_ERROR);
            $updateToRestore->save();
            $result['error'] = true; $result['msg'] .= __('error generated when trying to load offer %1, UpdateId: ', $updateToRestore->getAssignId());
        }

        return $result;
    }

    public function hasTemporaryPriceUpdate($itemId){

        $collection = $this->updateCollection->create()
            ->addFieldToFilter('status', \Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_RUNNIG)
            ->addFieldToFilter('assign_id',$itemId);


        return $collection->getSize() != 0;
    }

    public function ApplyAdjustment($adjustmentToApply){
        $result = ['error' => false, 'msg' => ''];
        $currentDate = $this->dateTime->date()->format("Y-m-d");

        if (($adjustmentToApply->getStartTime()==null)
            || ($adjustmentToApply->getEndTime()==null)
            || ($adjustmentToApply->getStartTime() >= $adjustmentToApply->getEndTime())){
            $result = ['error' => true, 'msg' => 'Error: La fecha de inicio '.$adjustmentToApply->getStartTime().' es mayor que la fecha de finalización '. $adjustmentToApply->getEndTime()];
            return $result;
        }

        if (($currentDate > $adjustmentToApply->getEndTime())){
            $result = ['error' => true, 'msg' => 'Error: La fecha de inicio '.$adjustmentToApply->getStartTime().' es menor a la fecha de ejecución del proceso de reajuste '.$currentDate.'.'];
            return $result;
        }

        //validar la info del Adjustmen
        $product = $this->productRepository->get($adjustmentToApply->getSku());
        $productId =$product->getId();

        if (!$productId){ //error con el producto
            $result = ['error' => true, 'msg' => 'Error  al consultar el Id del producto - SKU: '.$adjustmentToApply->getSku() ];
            return $result;
        }
        try {
            $customer = $this->customerRepository->get($adjustmentToApply->getEmail());
            //$customer = $this->_getCustomerByEmail($adjustmentToApply->getEmail());
        } catch (\Exception $e) {
            $info = 'error loading customer seller:'. $adjustmentToApply->getEmail(). ' - '.  $e->getMessage();
            $this->logger->info($info);
            $this->logger->critical($info);
            $this->logger->critical('Exception message => ' . $e->getMessage());
            $result = ['error' => true, $info ];
            return $result;
        }
        $sellerId = $customer->getId();

        if (!$sellerId){ //error con el seller
            $result = ['error' => true, 'msg' => 'Error al consultar el Id del proveedor - email:' .$adjustmentToApply->getEmail() ];
            return $result;
        }

        $websiteId = $this->getWebsiteId($adjustmentToApply->getWebsite());

        if (!$websiteId){ //error con el website
            $result = ['error' => true, 'msg' => 'Error: No existe el website Id - Website:'. $adjustmentToApply->getWebsite()];
            return $result;
        }

        $assignItem = $this->helperAssignProduct->getAssignItem($productId, $sellerId);
        //error con la oferta
        if (!$assignItem->getId()){
            $result = ['error' => true, 'msg' => 'Error: La ejecución del proceso de reajuste no encuentra el producto para la oferta o está deshabilitado. Id Producto: '.$productId.'; Vendedor: '.$sellerId.'; SKU: '.$adjustmentToApply->getSku().'; Correo vendedor: '.$adjustmentToApply->getEmail()];
            return $result;
        }

        //aca depende del website que precio tomamos.
        $price = $assignItem->getPrice();

        // en caso de ser configurable
        if ($assignItem->getType() === 'configurable') {
            //validar la info del Adjustmen
            $productChild = $this->productRepository->get($adjustmentToApply->getSkuAssign());
            $productId = $productChild->getId();

            if (!$productId){ //error con el producto
                $result = ['error' => true, 'msg' => 'error  al consultar el Id del producto hijo - SKU: '.$adjustmentToApply->getSkuAssign() ];
                return $result;
            }

            $associate = $this->helperAssignProduct->getAssociateByProduct($assignItem->getId(), $productId);
            if (!$associate) {
                $result = ['error' => true, 'msg' => 'error  al consultar el Associate - SKU: '.$adjustmentToApply->getSkuAssign() ];
                return $result;
            }
            $price = $associate->getPrice();
        }

        //verificar que no tenga ofertas especiales //si tiene cancelarlas
        try {
            $specialOffer = $this->offerFactory->create()->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('assign_id', $assignItem->getId())
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('status', 1)
                ->getFirstItem();
            if ($specialOffer && $specialOffer->getId()){ //exite la oferta especial.
                if ($specialOffer->getEndDate() >= $currentDate){
                    $specialOffer->setStatus(0);
                    $specialOffer->save();
                    $price = $assignItem->getBasePrice(); //recuperamos el precio base configurado en la oferta
                    //$this->cancelSpecialOffer($specialOffer); equivalente a poner status en 0
                    // en caso de ser configurable
                    if ($assignItem->getType() === 'configurable' && !empty($associate)) {
                        $price = $associate->getPrice();
                    }
                }
            }
        } catch (\Exception $e) {
            $info = 'error when trying to cancel the special offer => itemID:' .
                $assignItem->getId(). ' ProductID:'. $productId. ' Seller:'. $adjustmentToApply->getEmail();
            $this->logger->info($info);
            $this->logger->critical($info);
            $this->logger->critical('Exception message => ' . $e->getMessage());
            $result = ['error' => true, $info ];
            return $result;
        }

        //crear Update en tabla de adjustmens
        $newUpdate = $this->updateAdjustment->create();
        $newUpdate->setProductId($productId);
        $newUpdate->setSellerId($sellerId);
        $newUpdate->setAssignId($assignItem->getId());
        $newUpdate->setAdjustmentId($adjustmentToApply->getEntityId());
        $newUpdate->setWebsiteId($websiteId);
        $newUpdate->setStatus(self::STATUS_RUNNIG);
        $newUpdate->setOriginalPrice($price);

        //update offer
        $adjustmentToApply->setStatus(\Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_RUNNIG);
        $adjustmentToApply->save();
        $newUpdate->save();

         // en caso de ser configurable
        if ($assignItem->getType() === 'configurable' && !empty($associate)) {
            $associate->setPrice($adjustmentToApply->getNewTemporaryPrice());
            $associate->save();
        } else {
            $assignItem->setPrice($adjustmentToApply->getNewTemporaryPrice());
            $assignItem->save();
        }

        //todo: voucher? combustibles? emergencias? otros
        return $result;
    }

    /**
     * Get website code
     *
     * @param mixed $website
     * @return string
     */
    protected function getWebsiteCode($website = null)
    {
        return $this->storeManager->getWebsite($website)->getCode();
    }

    /**
     * Get website Id
     *
     * @param mixed $website
     * @return string
     */
    protected function getWebsiteId($website = null)
    {
        return $this->storeManager->getWebsite($website)->getId();
    }

    protected function _getCustomerByEmail($email)
    {
        $email = $this->connection->quote($email);
        $_result =  $this->connection->fetchAll("SELECT * FROM customer_entity WHERE email = " . $email);
        if ($_result) {
            return $_result[0]['entity_id'];
        } else {
            return false;
        }
    }
}