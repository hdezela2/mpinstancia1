<?php

namespace Summa\TemporaryPriceAdjustment\Model\VendorAttribute\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class GroupStatus
 */
class AdjustmentStatus implements OptionSourceInterface
{
    /**
     * @var \Summa\TemporaryPriceAdjustment\Helper\Data
     */
    protected $helper;

    /**
     * AdjustmentStatus constructor.
     * @param \Summa\TemporaryPriceAdjustment\Helper\Data $helperAdjustment
     */
    public function __construct(
        \Summa\TemporaryPriceAdjustment\Helper\Data $helperAdjustment
    )
    {
        $this->helper = $helperAdjustment;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->helper->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
