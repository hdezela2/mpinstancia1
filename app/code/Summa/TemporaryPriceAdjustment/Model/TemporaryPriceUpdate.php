<?php

namespace Summa\TemporaryPriceAdjustment\Model;

use \Magento\Framework\Model\AbstractModel;

class TemporaryPriceUpdate extends AbstractModel
{
    const CACHE_TAG = 'summa_temporary_price_update';
    protected $_cacheTag = 'summa_temporary_price_update';
    protected $_eventPrefix = 'summa_temporary_price_update';

    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init('Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceUpdate');
    }


}

