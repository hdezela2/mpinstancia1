<?php

namespace Summa\TemporaryPriceAdjustment\Model;

use \Magento\Framework\Model\AbstractModel;

class TemporaryPriceAdjustment extends AbstractModel
{
    const CACHE_TAG = 'summa_temporary_price_adjustment';
    protected $_cacheTag = 'summa_temporary_price_adjustment';
    protected $_eventPrefix = 'summa_temporary_price_adjustment';

    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init('Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceAdjustment');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

}
