<?php

namespace Summa\TemporaryPriceAdjustment\Model\Import;

use Exception;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\ImportExport\Helper\Data as ImportHelper;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\Entity\AbstractEntity;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\ImportExport\Model\ResourceModel\Import\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Webkul\MpAssignProduct\Helper\Data as assignProductHelper;

/**
* Class Courses
*/
class TemporaryPriceAdjustment extends AbstractEntity
{
    const ENTITY_CODE = 'temporarypriceadjustment';
    const TABLE = 'temporary_price_adjustment';

    const ENTITY_ID_COLUMN = 'email';
    const STATUS_COLUMN = 'status';

    /**
    * If we should check column names
    */
    protected $needColumnCheck = true;

    /**
    * Need to log in import history
    */
    protected $logInHistory = true;

    /**
    * Valid column names
    */
    protected $validColumnNames = [
        'email',
        'sku',
        'sku_assign',
        'website',
        'new_temporary_price',
        'start_time',
        'end_time'
    ];

    /**
    * @var AdapterInterface
    */
    protected $connection;

    /**
    * @var ResourceConnection
    */
    private $resource;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $helperAssignProduct;
    /**
    * Courses constructor.
    *
    * @param JsonHelper $jsonHelper
    * @param ImportHelper $importExportData
    * @param Data $importData
    * @param ResourceConnection $resource
    * @param Helper $resourceHelper
    * @param ProcessingErrorAggregatorInterface $errorAggregator
    */
    public function __construct(
        JsonHelper $jsonHelper,
        ImportHelper $importExportData,
        Data $importData,
        ResourceConnection $resource,
        Helper $resourceHelper,
        ProcessingErrorAggregatorInterface $errorAggregator,
        ScopeConfigInterface $scopeConfigInterface,
        ProductRepositoryInterface $productRepository,
        CustomerRepositoryInterface $customerRepository,
        assignProductHelper $helperAssignProduct
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->resource = $resource;
        $this->connection = $resource->getConnection(ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
        $this->helperAssignProduct = $helperAssignProduct;
        $this->initMessageTemplates();
    }

    /**
    * Entity type code getter.
    *
    * @return string
    */
    public function getEntityTypeCode()
    {
        return static::ENTITY_CODE;
    }

    /**
    * Get available columns
    *
    * @return array
    */
    public function getValidColumnNames(): array
    {
        return $this->validColumnNames;
    }

    /**
    * Row validation
    *
    * @param array $rowData
    * @param int $rowNum
    *
    * @return bool
    */
    public function validateRow(array $rowData, $rowNum): bool
    {
        $email = $rowData['email'] ?? '';
        if (!$email)
        {
            $this->addRowError('email', $rowNum);
        }

        $website = $rowData['website'] ?? '';
        if(!$website)
        {
            $this->addRowError('website', $rowNum);
        }

        $sku = $rowData['sku'] ?? '';
        if(!$sku)
        {
            $this->addRowError('sku', $rowNum);
        }

        $newTemporaryPrice = $rowData['new_temporary_price'] ?? '';
        if(!$newTemporaryPrice)
        {
            $this->addRowError('new_temporary_price', $rowNum);
        }
        // Validate New Temporary Price
        if ($this->validateTemporaryPrice($rowData['sku'], $rowData['sku_assign'], $rowData['email'], $rowData['new_temporary_price'])) {
            $this->addRowError('new_temporary_price percentage cannot exceed 4%', $rowNum);
        }

        $startTime = $rowData['start_time'] ?? '';
        if(!$startTime)
        {
            $this->addRowError('start_time', $rowNum);
        }

        if ($rowData['website'] != 'emergencias202109') {
            $endTime = $rowData['end_time'] ?? '';
            if(!$endTime)
            {
                $this->addRowError('end_time', $rowNum);
            }
        }

        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;

        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    /**
     *
     */
    private function initMessageTemplates()
    {
        $this->addMessageTemplate(
            'email',
            __('The seller cannot be empty.')
        );

        $this->addMessageTemplate(
            'sku',
            __('The product_id cannot be empty.')
        );

        $this->addMessageTemplate(
            'website',
            __('The website_code cannot be empty.')
        );

        $this->addMessageTemplate(
            'new_temporary_price',
            __('The temporary_price cannot be empty.')
        );

        $this->addMessageTemplate(
            'start_time',
            __('The start_date cannot be empty')
        );

        $this->addMessageTemplate(
            'end_time',
            __('The end_date cannot be empty')
        );
    }

    /**
    * Import data
    *
    * @return bool
    *
    * @throws Exception
    */
    protected function _importData(): bool
    {
        switch ($this->getBehavior()) {
            case Import::BEHAVIOR_DELETE:
                $this->deleteEntity();
            break;
            case Import::BEHAVIOR_REPLACE:
                $this->saveAndReplaceEntity();
            break;
            case Import::BEHAVIOR_APPEND:
                $this->saveAndReplaceEntity();
            break;
        }

        return true;
    }

    /**
    * Delete entities
    *
    * @return bool
    */
    private function deleteEntity(): bool
    {
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);

                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowId = $rowData[static::ENTITY_ID_COLUMN];
                    $rows[] = $rowId;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }

        if ($rows) {
            return $this->deleteEntityFinish(array_unique($rows));
        }

        return false;
    }

    /**
    * Save and replace entities
    *
    * @return void
    */
    private function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];

            foreach ($bunch as $rowNum => $row) {
                if (!$this->validateRow($row, $rowNum)) {
                    continue;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);

                    continue;
                }

                $rowId = $row[static::ENTITY_ID_COLUMN];
                $rows[] = $rowId;
                $columnValues = [];

                foreach ($this->getAvailableColumns() as $columnKey) {
                    $columnValues[$columnKey] = $row[$columnKey];
                }

                if (!isset($row[static::STATUS_COLUMN])){
                    $columnValues[static::STATUS_COLUMN] = \Summa\TemporaryPriceAdjustment\Helper\Data::STATUS_TOAPPLY;
                }
                //TODO - check if this line is really needed here
                //$columnValues[$columnKey] = $row[$columnKey];

                $entityList[$rowId][] = $columnValues;
                $this->countItemsCreated += (int) !isset($row[static::ENTITY_ID_COLUMN]);
                $this->countItemsUpdated += (int) isset($row[static::ENTITY_ID_COLUMN]);
            }

            if (Import::BEHAVIOR_REPLACE === $behavior) {
                if ($rows && $this->deleteEntityFinish(array_unique($rows))) {
                    $this->saveEntityFinish($entityList);
                }
            } elseif (Import::BEHAVIOR_APPEND === $behavior) {
                $this->saveEntityFinish($entityList);
            }
        }
    }

    /**
    * Save entities
    *
    * @param array $entityData
    *
    * @return bool
    */
    private function saveEntityFinish(array $entityData): bool
    {
        if ($entityData) {
            $tableName = $this->connection->getTableName(static::TABLE);
            $rows = [];

            $numberOfDays = $this->scopeConfigInterface->getValue('summa_temporary_update/days/number_of_days');

            foreach ($entityData as $entityRows) {
                foreach ($entityRows as $row) {
                    $startDate = date_create($row['start_time']);
                    $endDate = date_create($row['end_time']);
                    $row['start_time'] = date_format($startDate,"Y-m-d");

                    if ($row['website'] == 'emergencias202109') {
                        date_add($startDate, date_interval_create_from_date_string($numberOfDays." days"));
                        $row['end_time'] = date_format($startDate, "Y-m-d");
                    } else {
                        $row['end_time'] = date_format($endDate,"Y-m-d");
                    }
                    $rows[] = $row;
                }
            }

            if ($rows) {
                $this->connection->insertOnDuplicate($tableName, $rows, $this->getAvailableColumns());

                return true;
            }

            return false;
        }
        return false;
    }

    /**
    * Delete entities
    *
    * @param array $entityIds
    *
    * @return bool
    */
    private function deleteEntityFinish(array $entityIds): bool
    {
        if ($entityIds) {
            try {
                $this->countItemsDeleted += $this->connection->delete(
                    $this->connection->getTableName(static::TABLE),
                    $this->connection->quoteInto(static::ENTITY_ID_COLUMN . ' IN (?)', $entityIds)
                );

            return true;
            } catch (Exception $e) {
                return false;
            }
        }

        return false;
    }

    /**
    * Get available columns
    *
    * @return array
    */
    private function getAvailableColumns(): array
    {
        return $this->validColumnNames;
    }
    
    public function validateTemporaryPrice($sku, $assignSku, $email, $newTemporaryPrice)
    {
        $product = $this->productRepository->get($sku);
        $customer = $this->customerRepository->get($email);
        $productChild = $this->productRepository->get($assignSku);

        $assignItem = $this->helperAssignProduct->getAssignItem($product->getId(), $customer->getId());

        $associate = $this->helperAssignProduct->getAssociateByProduct($assignItem->getId(), $productChild->getId());

        if ($newTemporaryPrice > (($associate->getPrice()) * 4 / 100) + $associate->getPrice()) {
            return true;
        } else {
            return false;
        }
    }
}
