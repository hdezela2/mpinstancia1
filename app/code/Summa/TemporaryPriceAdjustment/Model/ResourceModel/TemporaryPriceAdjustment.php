<?php

namespace Summa\TemporaryPriceAdjustment\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class TemporaryPriceAdjustment extends AbstractDb
{

    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('temporary_price_adjustment', 'entity_id');
    }


}