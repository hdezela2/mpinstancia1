<?php

namespace Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceUpdate;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'summa_temporary_price_update';
    protected $_eventObject = 'temporary_price_update_collection';

    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Summa\TemporaryPriceAdjustment\Model\TemporaryPriceUpdate', 'Summa\TemporaryPriceAdjustment\Model\ResourceModel\TemporaryPriceUpdate');
    }
}
