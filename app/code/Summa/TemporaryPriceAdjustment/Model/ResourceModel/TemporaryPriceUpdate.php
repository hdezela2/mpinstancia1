<?php

namespace Summa\TemporaryPriceAdjustment\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class TemporaryPriceUpdate extends AbstractDb
{


    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('temporary_price_adjustment_update', 'entity_id');
    }


}

