<?php

namespace Summa\FormatCurrency\Model\System\Config\Backend;

use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value as ConfigValue;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Summa\FormatCurrency\Helper\Data as DataHelper;

class PriceFormat extends ConfigValue
{
    /**
     * @var DataHelper
     */
    protected $dataHelper;

    /**
     * PriceFormat constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $config
     * @param TypeListInterface $cacheTypeList
     * @param DataHelper $dataHelper
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        DataHelper $dataHelper,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * Process data after load
     * @return ConfigValue|void
     * @throws LocalizedException
     */
    protected function _afterLoad()
    {
        $value = $this->getValue();
        $value = $this->dataHelper->makeArrayFieldValue($value);
        $this->setValue($value);
    }

    /**
     * Prepare data before save
     * @return void
     */
    public function beforeSave()
    {
        $value = $this->getValue();
        $value = $this->dataHelper->makeStorableArrayFieldValue($value);
        $this->setValue($value);
    }
}
