<?php

namespace Summa\FormatCurrency\Plugin\Directory\Model;

use Magento\Directory\Model\Currency;
use Summa\FormatCurrency\Helper\Data as DataHelper;

class CurrencyPlugin
{
    /**
     * @var DataHelper
     */
    protected $dataHelper;

    /**
     * Initialize dependencies
     * @param DataHelper $dataHelper
     */
    public function __construct(DataHelper $dataHelper)
    {
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param Currency $subject
     * @param $price
     * @param array $options
     * @return array
     */
    public function beforeFormatTxt(Currency $subject, $price, $options = [])
    {
        if (!$this->dataHelper->isEnabled()) {
            return [$price, $options];
        }

        $currencyCode = $subject->getCode();
        if (!empty($currencyCode)) {
            $customOptions = $this->dataHelper->getConfigValue($currencyCode);
            if (!is_null($customOptions)) {
                $options = $customOptions + $options;
            }
        }
        return [$price, $options];
    }
}
