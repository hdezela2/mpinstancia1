<?php

namespace Summa\FormatCurrency\Plugin\Framework;

use Magento\Framework\App\ScopeResolverInterface;
use Magento\Framework\Currency;
use Summa\FormatCurrency\Helper\Data as DataHelper;

class CurrencyPlugin
{
    /**
     * @var DataHelper
     */
    protected $dataHelper;

    /**
     * @var ScopeResolverInterface
     */
    protected $scopeResolver;

    /**
     * Initialize dependencies
     * @param DataHelper $dataHelper
     * @param ScopeResolverInterface $scopeResolver
     */
    public function __construct(
        DataHelper $dataHelper,
        ScopeResolverInterface $scopeResolver
    ) {
        $this->dataHelper = $dataHelper;
        $this->scopeResolver = $scopeResolver;
    }

    /**
     * After to currency plugin method in order to modify price format
     * @param Currency $subject
     * @param \Closure $closure
     * @param int|float $value
     * @param array $options
     * @return string
     */
    public function aroundToCurrency(
        Currency $subject,
        \Closure $closure,
        $value = null,
        array $options = array()
    ) {
        if (!$this->dataHelper->isEnabled()) {
            $result = $closure($value, $options);
            return $result;
        }

        /**
         * Force the usage of the en_US locale in order to avoid formatting problem
         */
        $options['locale'] = 'en_US';
        $result = $closure($value, $options);
        $currencyCode = !empty($options['currency'])
            ? $options['currency']
            : $this->scopeResolver->getScope()->getCurrentCurrencyCode();
        if (!empty($currencyCode)) {
            return $this->dataHelper->postProcess($result, $currencyCode);
        }
        return $result;
    }
}
