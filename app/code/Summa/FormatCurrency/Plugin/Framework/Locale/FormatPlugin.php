<?php

namespace Summa\FormatCurrency\Plugin\Framework\Locale;

use Closure;
use Magento\Framework\App\ScopeResolverInterface;
use Magento\Framework\Locale\Format;
use Summa\FormatCurrency\Helper\Data as DataHelper;

class FormatPlugin
{
    /**
     * @var DataHelper
     */
    protected $dataHelper;

    /**
     * @var ScopeResolverInterface
     */
    protected $scopeResolver;

    /**
     * Initialize dependencies
     * @param DataHelper $dataHelper
     * @param ScopeResolverInterface $scopeResolver
     */
    public function __construct(
        DataHelper $dataHelper,
        ScopeResolverInterface $scopeResolver
    ) {
        $this->dataHelper = $dataHelper;
        $this->scopeResolver = $scopeResolver;
    }

    /**
     * Set currency options
     * @param Format $subject
     * @param Closure $closure
     * @param string $localeCode Locale code
     * @param string $currencyCode Currency code
     * @return array
     */
    public function aroundGetPriceFormat(
        Format $subject,
        Closure $closure,
        $localeCode = null,
        $currencyCode = null
    ) {
        $result = $closure($localeCode, $currencyCode);
        if (!$this->dataHelper->isEnabled()) {
            return $result;
        }

        if (!$currencyCode) {
            $currencyCode = $this->scopeResolver->getScope()->getCurrentCurrencyCode();
        }

        if (!empty($currencyCode)) {
            $customOptions = $this->dataHelper->getConfigValue($currencyCode);
            if (!is_null($customOptions)) {
                $result = [
                    'groupSymbol'   => $customOptions['group'],
                    'decimalSymbol' => $customOptions['decimal'],
                    'position'      => $customOptions['position'],
                    'precision'     => $customOptions['precision'],
                    'requiredPrecision' => $customOptions['requiredPrecision']
                ] + $result;
            }
        }
        return $result;
    }
}
