<?php

namespace Summa\FormatCurrency\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\BlockInterface;
use Zend\I18n\Translator\Plural\Symbol;

class PriceFormat extends AbstractFieldArray
{
    /**
     * @var Currency
     */
    protected $currencyRenderer;

    /**
     * @var SymbolPosition
     */
    protected $symbolPositionRenderer;

    /**
     * @return BlockInterface|Currency
     * @throws LocalizedException
     */
    protected function getCurrencyRenderer()
    {
        if (!$this->currencyRenderer) {
            $this->currencyRenderer = $this->getLayout()->createBlock(
                \Summa\FormatCurrency\Block\Adminhtml\Form\Field\Currency::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
            $this->currencyRenderer->setData('extra_params','style="width: 150px;"');
            $this->currencyRenderer->setClass('locale_select');
        }
        return $this->currencyRenderer;
    }

    /**
     * @return BlockInterface|SymbolPosition
     * @throws LocalizedException
     */
    protected function getSymbolPositionRenderer()
    {
        if (!$this->symbolPositionRenderer) {
            $this->symbolPositionRenderer = $this->getLayout()->createBlock(
                'Summa\FormatCurrency\Block\Adminhtml\Form\Field\SymbolPosition',
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
            $this->symbolPositionRenderer->setData('extra_params','style="width: 100px;"');
            $this->symbolPositionRenderer->setClass('format_type_select');
        }
        return $this->symbolPositionRenderer;
    }

    /**
     * @throws LocalizedException
     */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'currency',
            [
                'label'    => __('Currency'),
                'renderer' => $this->getCurrencyRenderer(),
            ]
        );

        $this->addColumn(
            'group',
            [
                'label'    => __('Thousand separator'),
            ]
        );

        $this->addColumn(
            'decimal',
            [
                'label'    => __('Decimal separator'),
            ]
        );

        $this->addColumn(
            'precision',
            [
                'label'    => __('Number of decimals'),
            ]
        );

        $this->addColumn(
            'position',
            [
                'label'    => __('Symbol position'),
                'renderer' => $this->getSymbolPositionRenderer(),
            ]
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Format');
    }

    /**
     * @param DataObject $row
     * @throws LocalizedException
     */
    protected function _prepareArrayRow(DataObject $row)
    {
        $optionExtraAttr = [];
        $optionExtraAttr['option_' . $this->getCurrencyRenderer()->calcOptionHash($row->getData('currency'))] =
            'selected="selected"';
        $optionExtraAttr['option_' . $this->getSymbolPositionRenderer()->calcOptionHash($row->getData('position'))] =
            'selected="selected"';
        $row->setData(
            'option_extra_attrs',
            $optionExtraAttr
        );
    }
}
