<?php

namespace Summa\FormatCurrency\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Math\Random as MathRandom;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    /**
     * Config paths
     */
    const XML_PATH_ENABLED        = 'summa_format_currency/general/enabled';
    const XML_PATH_CURRENCY_FORMAT = 'summa_format_currency/general/formats';

    /**
     * @var MathRandom
     */
    protected $mathRandom;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param Context $context
     * @param MathRandom $mathRandom
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        MathRandom $mathRandom,
        StoreManagerInterface $storeManager
    ) {
        $this->mathRandom = $mathRandom;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Get Currency Code By Store
     * @param $storeId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getCurrencyCodeByStore($storeId)
    {
        return $this->storeManager->getStore($storeId)->getCurrentCurrency()->getCode();
    }

    /**
     * Generate a storable representation of a value
     * @param int|float|string|array $value
     * @return string
     */
    protected function serializeValue($value)
    {
        if (is_array($value)) {
            return serialize($value);
        } else {
            return '';
        }
    }

    /**
     * Create a value from a storable representation
     * @param int|float|string $value
     * @return array
     */
    protected function unserializeValue($value)
    {
        if (is_string($value) && !empty($value)) {
            return unserialize($value);
        } else {
            return [];
        }
    }

    /**
     * Check whether value is in form retrieved by _encodeArrayFieldValue()
     * @param string|array $value
     * @return bool
     */
    protected function isEncodedArrayFieldValue($value)
    {
        if (!is_array($value)) {
            return false;
        }
        unset($value['__empty']);
        foreach ($value as $row) {
            if (!is_array($row)
                || !array_key_exists('currency', $row)
                || !array_key_exists('group', $row)
                || !array_key_exists('decimal', $row)
                || !array_key_exists('position', $row)
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Encode value to be used in \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
     * @param array $value
     * @return array
     * @throws LocalizedException
     */
    protected function encodeArrayFieldValue(array $value)
    {
        $result = [];
        foreach ($value as $row) {
            $resultId = $this->mathRandom->getUniqueHash('_');
            $result[$resultId] = $row;
        }
        return $result;
    }

    /**
     * Decode value from used in \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
     * @param array $value
     * @return array
     */
    protected function decodeArrayFieldValue(array $value)
    {
        $result = [];
        unset($value['__empty']);
        foreach ($value as $row) {
            if (!is_array($row)
                || !array_key_exists('currency', $row)
                || !array_key_exists('group', $row)
                || !array_key_exists('decimal', $row)
                || !array_key_exists('position', $row)
            ) {
                continue;
            }
            $result[] = $row;
        }
        return $result;
    }

    /**
     * Check if enabled
     * @return bool
     */
    public function isEnabled()
    {
        if (!$this->isModuleOutputEnabled()) {
            return false;
        }
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Retrieve price format from config
     * @param $currencyCode
     * @return array|null
     */
    public function getConfigValue($currencyCode)
    {
        if (empty($currencyCode)) {
            return null;
        }

        $value = $this->scopeConfig->getValue(
            self::XML_PATH_CURRENCY_FORMAT,
            ScopeInterface::SCOPE_STORE);
        if (empty($value)) {
            return null;
        }

        $value = $this->unserializeValue($value);
        if ($this->isEncodedArrayFieldValue($value)) {
            $value = $this->decodeArrayFieldValue($value);
        }

        $result = null;
        foreach ($value as $row) {
            if ($row['currency'] == $currencyCode) {
                $result = [
                    'group'    => $row['group'],
                    'decimal'  => $row['decimal'],
                    'position' => (int)$row['position'],
                    'precision' => (int)$row['precision'],
                    'requiredPrecision' => (int)$row['precision']
                ];
                break;
            }
        }
        return $result;
    }

    /**
     * Make value readable by \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
     * @param string|array $value
     * @return array
     * @throws LocalizedException
     */
    public function makeArrayFieldValue($value)
    {
        $value = $this->unserializeValue($value);
        if (!$this->isEncodedArrayFieldValue($value)) {
            $value = $this->encodeArrayFieldValue($value);
        }
        return $value;
    }

    /**
     * Make value ready for store
     * @param string|array $value
     * @return string
     */
    public function makeStorableArrayFieldValue($value)
    {
        if ($this->isEncodedArrayFieldValue($value)) {
            $value = $this->decodeArrayFieldValue($value);
        }
        $value = $this->serializeValue($value);
        return $value;
    }

    /**
     * Price post process
     * @param string $price
     * @param string $currencyCode
     * @return string
     */
    public function postProcess($price, $currencyCode)
    {
        $currencyOptions = $this->getConfigValue($currencyCode);
        if (is_null($currencyOptions)) {
            return $price;
        }
        $map = ['decimal' => '.', 'group' => ','];
        $price = strtr($price, array_flip($map));
        $price = strtr($price, $currencyOptions);
        return $price;
    }
}
