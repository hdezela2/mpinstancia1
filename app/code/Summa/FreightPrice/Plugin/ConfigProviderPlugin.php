<?php

namespace Summa\FreightPrice\Plugin;

use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Quote\Api\CartItemRepositoryInterface as QuoteItemRepository;

class ConfigProviderPlugin extends \Magento\Framework\Model\AbstractModel
{
    private $checkoutSession;
    private $quoteItemRepository;
    protected $scopeConfig;
    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    private $helperOffer;
    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $priceCurrency;

    public function __construct(
        \Webkul\MpAssignProduct\Helper\Data $helperOffer,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        CheckoutSession $checkoutSession,
        QuoteItemRepository $quoteItemRepository,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->helperOffer = $helperOffer;
        $this->priceCurrency = $priceCurrency;
        $this->checkoutSession = $checkoutSession;
        $this->quoteItemRepository = $quoteItemRepository;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {
        if (in_array($this->helperOffer->getStore()->getCode(), [\Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE, ConstantsEmergency202109::WEBSITE_CODE])) {
            //with this onlý generate the info in that website.
            $quoteId = $this->checkoutSession->getQuote()->getId();
            if ($quoteId) {
                $quoteItems = $this->quoteItemRepository->getList($quoteId);
                $i=0;
                foreach ($quoteItems as $index => $quoteItem) {
                    $quoteItemId = $quoteItem['item_id'];
                    $result['quoteItemData'][$i]['freight'] = $this->priceCurrency->convertAndFormat($this->helperOffer->getFreightPrice($quoteItemId), false);
                    $i++;
                }

            }
        }
        return $result;
    }

}