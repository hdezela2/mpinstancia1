define([
    'jquery',
    'uiComponent'
], function ($,Component) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/summary/item/details'
        },

        /**
         * @param {Object} quoteItem
         * @return {String}
         */
        getValue: function (quoteItem) {
            return quoteItem.name;
        },

        /**
         * @param {Object} quoteItem
         * @return {String}
         */
        getFreight: function (quoteItem) {
            var freight;

            $.each(window.checkoutConfig.quoteItemData, function(index, item) {
                if (quoteItem.item_id == item.item_id){
                    freight = item.freight;
                }
            });

            return freight;
        }
    });
});
