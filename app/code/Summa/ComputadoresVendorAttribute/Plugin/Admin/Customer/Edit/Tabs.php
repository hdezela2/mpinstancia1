<?php
namespace Summa\ComputadoresVendorAttribute\Plugin\Admin\Customer\Edit;

use Magento\Backend\Model\Auth\Session;

class Tabs
{

    const ID_AGREEMENT_COMPUTERS = 5800272;

    /**
     * @var Session
     */
    private $authSession;

    /**
     * Tabs constructor.
     * @param Session $authSession
     */
    public function __construct(
        Session $authSession
    ) {
        $this->authSession = $authSession;
    }

    /**
     * @param \Webkul\MpVendorAttributeManager\Block\Adminhtml\Customer\Edit\Tabs $subject
     * @param $result
     * @return string
     */


    public function afterGetCustomerAttribtues(
        \Webkul\MpVendorAttributeManager\Block\Adminhtml\Customer\Edit\Tabs $subject,
        $result)
    {

        //Computadores atributos admin filtrados

        $currentAdmin = $this->authSession->getUser();
        $currentRole = $currentAdmin->getRole();

        if ($currentAdmin && $currentRole &&  $currentRole->getRoleName() == self::ID_AGREEMENT_COMPUTERS){


            $comp_attributes = [
                'wkv_dccp_business_name',
                'wkv_dccp_rut',
                'wkv_dccp_id',
                'wkv_dccp_state_details',
                'wkv_dccp_name',
                'wkv_dccp_phone',
                'wkv_dccp_vendor_cellphone',
                'wkv_dccp_email',
                'wkv_vendor_back_name',
                'wkv_dccp_vendor_back_phone',
                'wkv_dccp_vendor_bcphone',
                'wkv_dccp_vendor_back_email',
                'wkv_dccp_lega_rep_name',
                'wkv_dccp_address',
                'wkv_dccp_commune',
                'wkv_dccp_region'
                ];

            $result->addFieldToFilter("attribute_code", ["in" => $comp_attributes]);
        }

        return $result;
    }
}
