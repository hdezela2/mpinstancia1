<?php

namespace Summa\EscritorioSetUp\Setup;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Store\Model\Store;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;

/* For get RoleType and UserType for create Role   */;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;



class InstallData implements InstallDataInterface
{
    const FRONT_THEME = 'Summa/Escritorio';
    /**
     * @var WebsiteFactory
     */
    private $websiteFactory;
    /**
     * @var Website
     */
    private $websiteResourceModel;
    /**
     * @var StoreFactory
     */
    private $storeFactory;
    /**
     * @var GroupFactory
     */
    private $groupFactory;
    /**
     * @var Group
     */
    private $groupResourceModel;
    /**
     * @var Store
     */
    private $storeResourceModel;
    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;
    /**
     * @var ConfigInterface
     */
    private $configInterface;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;



    /**
     * RoleFactory
     * @var roleFactory
     */
    private $roleFactory;

    /**
     * RulesFactory
     * @var rulesFactory
     */
    private $rulesFactory;


    /**
     * InstallData constructor.
     * @param WebsiteFactory $websiteFactory
     * @param Website $websiteResourceModel
     * @param Store $storeResourceModel
     * @param Group $groupResourceModel
     * @param StoreFactory $storeFactory
     * @param GroupFactory $groupFactory
     * @param ManagerInterface $eventManager
     * @param CategoryFactory $categoryFactory
     * @param ConfigInterface $configInterface
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        WebsiteFactory $websiteFactory,
        Website $websiteResourceModel,
        Store $storeResourceModel,
        Group $groupResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory,
        ManagerInterface $eventManager,
        CategoryFactory $categoryFactory,
        ConfigInterface $configInterface,
        CollectionFactory $collectionFactory,

        \Magento\Authorization\Model\RoleFactory $roleFactory,
        \Magento\Authorization\Model\RulesFactory $rulesFactory
    ) {
        $this->websiteFactory = $websiteFactory;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->storeFactory = $storeFactory;
        $this->groupFactory = $groupFactory;
        $this->groupResourceModel = $groupResourceModel;
        $this->storeResourceModel = $storeResourceModel;
        $this->eventManager = $eventManager;
        $this->categoryFactory = $categoryFactory;
        $this->configInterface = $configInterface;
        $this->collectionFactory = $collectionFactory;

        $this->roleFactory = $roleFactory;
        $this->rulesFactory = $rulesFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $escritorioId = \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_DESKITEMS;

        ##### CREATE WEBSITE #####
        /** @var \Magento\Store\Model\Website $website */
        $website = $this->websiteFactory->create();
        $website->load('Escritorio');
        if(!$website->getId()){
            $website->setCode('escritorio');
            $website->setName('Escritorio Website');
            $this->websiteResourceModel->save($website);
        }

        ##### CREATE ROOT CATEGORY #####
        $category = $this->createOrUpdateRootCategory();

        ##### CREATE STORE #####
        if($website->getId()){
            /** @var \Magento\Store\Model\Group $group */
            $group = $this->groupFactory->create();
            $group->load('Escritorio Store', 'name');
            if(!$group->getId()){
                $group->setWebsiteId($website->getWebsiteId());
                $group->setName('Escritorio Store');
                $group->setCode('escritorio');
                $group->setRootCategoryId($category->getId());
                $group->setDefaultStoreId(0);
                $this->groupResourceModel->save($group);
            }

        }

        ##### CREATE STORE VIEW #####
        /** @var  \Magento\Store\Model\Store $store */
        $store = $this->storeFactory->create();
        $store->load('escritorio');
        if(!$store->getId()){
            $group = $this->groupFactory->create();
            $group->load('Escritorio Store', 'name');
            $store->setCode('escritorio');
            $store->setName('Escritorio Store View');
            $store->setWebsiteId($website->getId());
            $store->setGroupId($group->getId());
            $store->setIsActive(1);
            $store->save();
            // Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
            $this->eventManager->dispatch('store_add', ['store' => $store]);
        }

        ##### ASSIGN THEME TO STORE VIEW #####
        $themes = $this->collectionFactory->create()->loadRegisteredThemes();
        /**
         * @var \Magento\Theme\Model\Theme $theme
         */
        foreach ($themes as $theme) {
            if ($theme->getArea() == 'frontend' && $theme->getCode() == self::FRONT_THEME) {
                $this->configInterface->saveConfig('design/theme/theme_id', $theme->getId(), 'stores', $store->getId());
            }
        }


        ##### CREATE WAREHOUSE ROLE #####
        $role = $this->roleFactory->create();
        //Set Role Name Which the ID Convenio
        $role->setName($escritorioId)
            ->setPid(0) //set parent role id of your role
            ->setRoleType(RoleGroup::ROLE_TYPE)
            ->setUserType(UserContextInterface::USER_TYPE_ADMIN)
            ->setGwsIsAll(0) //Doesn't have permission in all Stores
            ->setGwsWebsites($website->getId()) //array with the website ids
            ->setGwsStoreGroups($website->getId());

        $role->save();
        /* Now we set that which resources we allow to this role */
        $resource=[
            'Magento_Backend::dashboard',

            'Webkul_CategoryProductPrice::categoryproductprice',
            'Webkul_CategoryProductPrice::index',

            'Formax_PriceDispersion::pricedispersion',
            'Formax_PriceDispersion::index',

            'Magento_Analytics::analytics',
            'Magento_Analytics::analytics_api',

            'Webkul_Marketplace::marketplace',
            'Webkul_Marketplace::menu',
            'Webkul_Marketplace::seller',
            'Webkul_MpAssignProduct::product',
            'Formax_RegionalCondition::regional_condition',

            'Webkul_Mpshipping::menu',
            'Webkul_Mpshipping::mpshipping',
            'Webkul_Mpshipping::mpshippingset',

            'Webkul_Requestforquote::requestforquote',
            'Webkul_Requestforquote::quote_index',
            'Webkul_Requestforquote::index_index',

            'Magento_Catalog::catalog',
            'Magento_CatalogPermissions::catalog_magento_catalogpermissions',
            'Magento_Catalog::catalog_inventory',
            'Magento_Catalog::products',
            'Magento_PricePermissions::read_product_price',
            'Magento_PricePermissions::edit_product_price',
            'Magento_PricePermissions::edit_product_status',
            'Magento_Catalog::categories',

            'Magento_Customer::customer',
            'Magento_Customer::manage',
            'Magento_Reward::reward_balance',

            'Magento_Reports::report',
            'Magento_Reports::salesroot',
            'Formax_CustomReport::Report',

            'Magento_Backend::system',
            'Magento_Backend::convert',
            'Magento_ImportExport::export'

        ];
        /* Array of resource ids which we want to allow this role*/
        $this->rulesFactory->create()->setRoleId($role->getId())->setResources($resource)->saveRel();
    }

    /**
     * @return \Magento\Catalog\Model\Category
     * @throws \Exception
     */
    private function createOrUpdateRootCategory()
    {
        $category = $this->categoryFactory->create();
        $category->setName('Escritorio - Default Category');
        $category->setIsActive(true);
        $category->setStoreId(0);
        $parentCategory = $this->categoryFactory->create();
        $parentCategory->load(\Magento\Catalog\Model\Category::TREE_ROOT_ID);
        $category->setDisplayMode(\Magento\Catalog\Model\Category::DM_PRODUCT);
        $category->setPath($parentCategory->getPath());
        $category->save();

        return $category;
    }
}