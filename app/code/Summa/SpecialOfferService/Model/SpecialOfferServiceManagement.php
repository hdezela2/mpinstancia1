<?php

namespace Summa\SpecialOfferService\Model;

use Exception;
use Formax\Offer\Model\OfferFactory;
use Summa\SpecialOfferService\Helper\Data;
use Magento\Framework\Webapi\Exception as WebapiException;
use \Psr\Log\LoggerInterface;

class SpecialOfferServiceManagement implements \Summa\SpecialOfferService\Api\SpecialOfferInterface
{
    /** @var Data */
    protected $helper;

    /** @var OfferFactory */
    protected $offerFactory;

    protected $logger;

    /**
     * SpecialOfferServiceManagement constructor.
     * @param Data $helper
     * @param OfferFactory $offerFactory
     */
    public function __construct(
        Data $helper,
        OfferFactory $offerFactory,
        LoggerInterface $logger
    )
    {
        $this->helper = $helper;
        $this->offerFactory = $offerFactory;
        $this->logger = $logger;
    }

    /**
     * @param mixed $offerData
     * @return array
     * @throws Exception
     */
    public function save($offerData)
    {
        $offerCount = 0;
        $ary_response = [];
        $this->logger->info('Start Item API');
        foreach ($offerData as $item)
        {
           try{
                $result = $this->saveSpecialOffer($item);

                if(!$result['error']){
                    $valid = [
                        "code" => "200",
                        "message" => "Record saved successfully.",
                        "sku" => $item['sku'],
                        "seller" => $item['email'],
                    ];
                    $ary_response[] = $valid;
                    $offerCount++;
                    $this->logger->info($offerCount .  '- Special offer saved succesfully: ' . $item['sku']);
                }else{
                    $rowError = [
                        "code" => '301',
                        "message" => $result['return'],
                        "sku" => $item['sku'],
                        "seller" => $item['email'],
                    ];
                    $ary_response[] = $rowError;
                    $this->logger->info('There was an error saving the special offer: ' . $item['sku']);
                }
            }catch (Exception $e){
                $rowError = [
                    "code" => '301',
                    "message" => $e->getMessage(),
                    "sku" => $item['sku'],
                    "seller" => $item['email'],
                ];
                $ary_response[] = $rowError;
            }
        }

        $this->logger->info(__('%1 offer/s has been saved successfully - total rows %2', $offerCount, count($offerData)));
        return $ary_response;
    }

    protected function saveSpecialOffer($data){
        $result = $this->helper->validateSpecialOffer($data);

        if (!$result['status'])
        {
            return ['error' => true, 'return' => $result['message']];
        }
        try{
            $offer = $this->offerFactory->create();
            $offer->setParentProductId($result['parent_product_id']);
            $offer->setProductId($result['product_id']);
            $offer->setAssignId($result['assign_id']);
            $offer->setSellerId($result['seller_id']);
            $offer->setWebsiteId($result['website_id']);
            $offer->setStartDate($data['start_date']);
            $offer->setEndDate($data['end_date']);
            $offer->setDays($data['days']);
            $offer->setSpecialPrice($data['special_price']);
            $offer->setBasePrice($data['base_price']);
            $offer->setStatus($data['status']);
            $offer->save();
            return ['error' => false, 'return' =>__('Special Offer is saved successfully.')];
        }catch (Exception $e){
            return ['error' => true, 'return' => __($e->getMessage())];
        }
        $this->logger->info(json_encode($ary_response));
        return $ary_response;

    }
}