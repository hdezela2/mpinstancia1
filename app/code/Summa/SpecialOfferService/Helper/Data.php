<?php

namespace Summa\SpecialOfferService\Helper;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\NoSuchEntityException;

class Data extends \Formax\Offer\Helper\Data
{
    const REST_API_REQUIRE_PARAMS = [
        'email',
        'sku',
        'parent_sku',
        'base_price',
        'special_price',
        'start_date',
        'end_date',
        'days',
        'status'
    ];

    /**
     * Validate Special Offer
     * @param array $params
     * @return array
     * @throws Exception
     */
    public function validateSpecialOffer(array $params) : array
    {
        $storeId = null;
        $minDays = 0;
        $maxDays = 0;
        $validateParams = $this->validateParams($params);
        if (!$validateParams['status']) {
            return [
                'status' => false,
                'message' => __("'%1' are required fields.", implode("', '", $validateParams['result']))
            ];
        }

        if (empty($params['sku'])) {
            return [
                'status' => false,
                'message' => __("'%1' field must not be empty.", 'sku')
            ];
        }

        if (empty($params['email'])) {
            return [
                'status' => false,
                'message' => __("'%1' field must not be empty.", 'email')
            ];
        }

        $validator = new \Zend\Validator\GreaterThan(['min' => 0]);
        if (!$validator->isValid($params['base_price'])) {
            return [
                'status' => false,
                'message' => __("Field '%1' must be only numbers greater than zero. sku: '%2'", 'base_price', $params['sku'])
            ];
        }

        $validator = new \Zend\Validator\GreaterThan(['min' => 0]);
        if (!$validator->isValid($params['special_price'])) {
            return [
                'status' => false,
                'message' => __("Field '%1' must be only numbers greater than zero. sku: '%2'", 'special_price', $params['sku'])
            ];
        }



        $validator = new \Zend\Validator\Date();
        if (!$validator->isValid($params['start_date'])) {
            return [
                'status' => false,
                'message' => __("Field 'start_date' must be a valid format date: 'Y-m-d'. sku: '%1'", $params['sku'])
            ];
        }

        $currentDate = $this->_timezone->date();
        $endDate = new \DateTime( $params['end_date']);
        if ($endDate->format('Y-m-d') <= $currentDate->format('Y-m-d')) {
            return [
                'status' => false,
                'message' => __("Field 'end_date' must be greater than current date. sku: '%1'", $params['sku'])
            ];
        }

        $validator = new \Zend\Validator\GreaterThan(['min' => 0]);
        if (!$validator->isValid($params['days'])) {
            return [
                'status' => false,
                'message' => __("Field '%1' must be only numbers greater than zero. sku: '%2'", 'days', $params['sku'])
            ];
        }

        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        if (!isSet($params['website'])){
            $storeId = $this->_storeManager->getStore()->getId();
            $minDays = $this->showMinimumDayOffer();
            $maxDays = $this->showMaximumDayOffer();
        }

        if (isSet($params['website'])){
            $websiteId =  $this->_storeManager->getWebsite($params['website'])->getId();
            $storeId = $this->_storeManager->getWebsite($params['website'])->getStoreId();
            $minDays = $this->showMinimumDayOffer($websiteId);
            $maxDays = $this->showMaximumDayOffer($websiteId);
        }

        $specialPrice = $this->getMinSpecialPrice($params['base_price'], false, $storeId);
        if ($params['special_price'] > $specialPrice) {
            return [
                'status' => false,
                'message' => __("Field 'special_price' must be equal or less than '%1'. sku: '%2'", $specialPrice, $params['sku'])
            ];
        }

        $validator = new \Zend\Validator\Between(['min' => $minDays, 'max' => $maxDays]);
        if (!$validator->isValid($params['days'])) {
            return [
                'status' => false,
                'message' => __("Field 'days' must be between '%1' and '%2'. sku: '%3'", $minDays, $maxDays, $params['sku'])
            ];
        }

        if (isset($params['parent_sku'])){
            $parentProduct = $this->getProduct($params['parent_sku']);
            if (!$parentProduct) {
                return [
                    'status' => false,
                    'message' => __("Configurable product with sku: '%1', does not exist.", $params['parent_sku'])
                ];
            }
        }
        $product = $this->getProduct($params['sku']);
        if (!$product) {
            return [
                'status' => false,
                'message' => __("Product with sku: '%1', does not exist.", $params['sku'])
            ];
        }

        $seller = $this->getSellerByEmail($params['email']);
        if (!$seller->getId()) {
            return [
                'status' => false,
                'message' => __("Customer with email: '%1', does not exist. sku: '%2'", $params['email'], $params['sku'])
            ];
        }

        $sellerInfo = $this->_assignHelper->getSellerInfo($seller->getId());
        if ($sellerInfo->getIsSeller() == '0') {
            return [
                'status' => false,
                'message' => __("Customer with email: '%1', is not a seller. sku: '%2'", $params['email'], $params['sku'])
            ];
        }

        if (isset($params['parent_sku']) && !empty($parentProduct)){
            //es configurable...
            $assignProduct = $this->getAssignItem($parentProduct->getId(), $seller->getId());
        }else{
            $assignProduct = $this->getAssignItem($product->getId(), $seller->getId());
        }
        if (!$assignProduct->getId()) {
            return [
                'status' => false,
                'message' => __("Customer assign product: '%1', not found. sku: '%2'", $params['email'], $params['sku'])
            ];
        }

        $allowedProductTypes = $this->_assignHelper->getAllowedProductTypes();
        if (!in_array($product->getTypeId(), $allowedProductTypes)) {
            return [
                'status' => false,
                'message' => __("Product type: '%1', is not allowed. sku: '%2'", $params['email'], $params['sku'])
            ];
        }

        $parentProductId = !empty($parentProduct) ? $parentProduct->getId() : $product->getId();
        $hasSpecialOffer = $this->getSpecialOffer($seller->getId(), $product->getId(), $parentProductId, $assignProduct->getId());
        if ($hasSpecialOffer->getId()) {
            return [
                'status' => false,
                'message' => __("Customer %1 has enable special offer for the SKU: '%2'", $params['email'], $params['sku'])
            ];
        }
        return [
            'status' => true,
            'seller_id' => $seller->getId(),
            'website_id' => $websiteId,
            'product_id' => $product->getId(),
            'parent_product_id' => $parentProductId,
            'assign_id' => $assignProduct->getId(),
            'product_type' => $product->getTypeId()
        ];
    }

    /**
     * Validate Params
     * @param array $item
     * @return array
     */
    protected function validateParams(array $item) : array
    {
        $data = [];
        foreach ($item as $key => $value) {
            array_push($data, $key);
        }

        $result = array_diff(self::REST_API_REQUIRE_PARAMS, $data);
        if ($result) {
            return [
                'status' => false,
                'result' => array_values($result)
            ];
        }

        return ['status' => true];
    }

    /**
     * Get Seller By Email
     * @param string $email
     * @return Customer
     */
    protected function getSellerByEmail(string $email) : Customer
    {
        $customer = $this->_customerFactory->create();
        $customer->loadByEmail($email);

        return $customer;
    }

    /**
     * Get Product
     * @param string $sku
     * @return ProductInterface|null
     */
    protected function getProduct(string $sku) : ?ProductInterface
    {
        $product = null;
        if ($sku) {
            try {
                $product = $this->_productRepositoryInterface->get($sku);
            } catch (NoSuchEntityException $e) {}
        }

        return $product;
    }

    /**
     * @param $productId
     * @param $sellerId
     * @return \Magento\Framework\DataObject
     */
    public function getAssignItem(string $productId, string $sellerId)
    {
        $collection = $this->_itemsCollection
            ->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('product_id', $productId);

        $assignProduct = $collection->getFirstItem();
        return $assignProduct;
    }

    /**
     * @param $sellerId
     * @param $productId
     * @param $parentProductId
     * @param $assignId
     * @return \Magento\Framework\DataObject
     */
    public function getSpecialOffer($sellerId, $productId, $parentProductId, $assignId){
        $now = new \DateTime();

        $offers = $this->_offerCollection->create();
        $offers->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('parent_product_id',$parentProductId)
            ->addFieldToFilter('assign_id', $assignId)
            ->addFieldToFilter('end_date', ['gteq' => $now->format('Y-m-d H:i:s')]);

        return $offers->getFirstItem();
    }
}
