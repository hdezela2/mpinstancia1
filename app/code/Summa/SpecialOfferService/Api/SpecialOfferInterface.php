<?php

namespace Summa\SpecialOfferService\Api;

interface SpecialOfferInterface
{
    /**
     * @param mixed $offerData
     * @return array
     */
    public function save($offerData);

}