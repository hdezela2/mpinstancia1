define([
        'jquery',
        'Magento_Ui/js/modal/modal',
        'Magento_Checkout/js/model/step-navigator'
    ], function($, modal, stepNavigator){
        var PopupInfo = {
            initProcess: function(config) {
                var options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: config.title,
                    buttons: [{
                        text: $.mage.__('Continue'),
                        class: '',
                        click: function () {
                            this.closeModal();
                        }
                    }]
                };

                var popup = modal(options, $(config.modalContainer));

                $(window).on('hashchange', function () {
                    if (stepNavigator.getActiveItemIndex() === 1){
                        $(config.modalContainer).modal('openModal');
                    }
                });
            }
        };

        return {
            'popupinfo-init': PopupInfo.initProcess
        };
    }
);