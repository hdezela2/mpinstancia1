<?php
namespace Summa\EmergenciasCheckout\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;

class Popup extends \Magento\Framework\View\Element\Template
{
    const PATH_XML_CONFIG_CHECKOUT_PAYMENT_POPUP_ACTIVE = 'summa/popup/active';
    const PATH_XML_CONFIG_CHECKOUT_PAYMENT_POPUP_TITLE = 'summa/popup/title';
    const PATH_XML_CONFIG_CHECKOUT_PAYMENT_POPUP_TEXT = 'summa/popup/text';

    public function isActive(){
        return $this->_scopeConfig->isSetFlag(
            self::PATH_XML_CONFIG_CHECKOUT_PAYMENT_POPUP_ACTIVE,
            ScopeInterface::SCOPE_WEBSITE
        );
    }

    public function getText(){
        return $this->_scopeConfig->getValue(
            self::PATH_XML_CONFIG_CHECKOUT_PAYMENT_POPUP_TEXT,
            ScopeInterface::SCOPE_WEBSITE
        );
    }

    public function getTitle(){
        return $this->_scopeConfig->getValue(
            self::PATH_XML_CONFIG_CHECKOUT_PAYMENT_POPUP_TITLE,
            ScopeInterface::SCOPE_WEBSITE
        );
    }
}