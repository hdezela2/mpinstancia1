<?php
namespace Summa\EmergenciasVendorAttribute\Plugin\Admin\Customer\Edit;

use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Magento\Backend\Model\Auth\Session;


class Tabs
{

    const ID_AGREEMENT_EMERGENCIAS = 5800262;

    /**
     * @var Session
     */
    private $authSession;

    /**
     * Tabs constructor.
     * @param Session $authSession
     */
    public function __construct(
        Session $authSession
    ) {
        $this->authSession = $authSession;
    }

    /**
     * @param \Webkul\MpVendorAttributeManager\Block\Adminhtml\Customer\Edit\Tabs $subject
     * @param $result
     * @return string
     */


    public function afterGetCustomerAttribtues(
        \Webkul\MpVendorAttributeManager\Block\Adminhtml\Customer\Edit\Tabs $subject,
        $result)
    {

        $currentAdmin = $this->authSession->getUser();
        $currentRole = $currentAdmin->getRole();

        if ( $currentAdmin && $currentRole &&
             in_array(
                 $currentRole->getRoleName(),
                 [self::ID_AGREEMENT_EMERGENCIAS, ConstantsEmergency202109::ID_AGREEMENT_EMERGENCY_2021_09]
             )
        ) {
            $emergency_attributes = [
                'wkv_dccp_business_name',
                'wkv_dccp_rut',
                'wkv_dccp_id',
                'wkv_dccp_state_details',
                'wkv_dccp_name',
                'wkv_dccp_phone',
                'wkv_dccp_vendor_cellphone',
                'wkv_dccp_email',
                'wkv_vendor_back_name',
                'wkv_dccp_vendor_back_phone',
                'wkv_dccp_vendor_bcphone',
                'wkv_dccp_vendor_back_email',
                'wkv_dccp_vendor_maxterm',
                'wkv_dccp_replac_period',
                'wkv_dccp_shop_train',
                'wkv_dccp_free_tech_asist',
                'wkv_dccp_asist_tech_phone',
                'wkv_dccp_services_url'
            ];

            $result->addFieldToFilter("attribute_code", ["in" => $emergency_attributes]);
        }

        return $result;
    }
}
