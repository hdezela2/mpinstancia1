<?php

namespace Summa\ProductAttributes\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
    protected $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {


        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            $attributes = [
                'max_base_price' => "Max Base Price",
                'max_shipping_price' => "Max Shipping Price"
            ];

            $setup->startSetup();
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            foreach ($attributes as $key => $attribute) {
                $eavSetup->updateAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    $key,
                    'is_filterable',
                    false
                );
                $eavSetup->updateAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    $key,
                    'is_visible_on_front',
                    false
                );
            }

            $setup->endSetup();
        }
    }
}