<?php
namespace Summa\ProductAttributes\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $attributes = [
            'max_base_price' => "Max Base Price",
            'max_shipping_price' => "Max Shipping Price"
        ];

        foreach ($attributes as $attribute) {
            $eavSetup->removeAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $attribute);
        }

        foreach ($attributes as $attribute => $label) {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $attribute,
                [
                    'type' => 'decimal',
                    'size' => '(12,4)',
                    'backend' => '',
                    'frontend' => '',
                    'label' => $label,
                    'input' => 'text',
                    'group' => 'Product Details',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => 1,
                    'required' => 0,
                    'user_defined' => 1,
                    'default' => '',
                    'searchable' => 1,
                    'filterable' => 1,
                    'comparable' => 0,
                    'visible_on_front' => 1,
                    'used_in_product_listing' => 1,
                    'unique' => 0,
                    'apply_to' => ''
                ]
            );
        }
    }
}