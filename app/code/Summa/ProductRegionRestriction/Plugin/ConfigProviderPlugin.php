<?php

namespace Summa\ProductRegionRestriction\Plugin;

use Magento\Checkout\Model\DefaultConfigProvider;
use Magento\Catalog\Api\ProductRepositoryInterface;

class ConfigProviderPlugin
{
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function afterGetConfig(DefaultConfigProvider $subject, array $result)
    {
        foreach ($result['quoteItemData'] as $index => $itemData) {
            $product = $this->productRepository->get($itemData['sku']);
            $result['quoteItemData'][$index]['region'] = $product->getRegion();

            $shippingRegionIds = $product->getShippingRegionId();
            if ($shippingRegionIds) {
                $arrayRegionIdOptionIds = explode(',', $shippingRegionIds);

                $regionArray = [];
                foreach ($arrayRegionIdOptionIds as $optionId) {
                    $regionIdOptionText = $product->getResource()->getAttribute('shipping_region_id')->getSource()->getOptionText($optionId);
                    $regionArray[] = $regionIdOptionText;
                }
                $result['quoteItemData'][$index]['region_array'] = $regionArray;
            }
        }

        return $result;
    }

}