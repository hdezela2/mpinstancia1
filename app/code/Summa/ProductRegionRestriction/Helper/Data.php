<?php

namespace Summa\ProductRegionRestriction\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
	const XML_PATH_ENABLED = 'summa_product_region_restriction/general/enabled';

	protected $customerSession;

	public function __construct(
		Context $context,
		Session $customerSession
    ) {
		$this->customerSession = $customerSession;
        parent::__construct($context);
	}

	public function getConfigValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_WEBSITE, $storeId
		);
	}

	public function isEnabled()
	{
		return $this->getConfigValue(self::XML_PATH_ENABLED);
	}

	public function getAddressesRegionsIds()
	{
		$data = [];
		$customer = $this->customerSession->getCustomer();
		foreach ($customer->getAddresses() as $address) {
			$data[$address->getId()] = $address->getRegionId();
		}

		return $data;
	}

}