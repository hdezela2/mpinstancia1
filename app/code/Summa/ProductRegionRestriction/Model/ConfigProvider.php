<?php

namespace Summa\ProductRegionRestriction\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Summa\ProductRegionRestriction\Helper\Data;

class ConfigProvider implements ConfigProviderInterface
{
    protected $helper;

    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    public function getConfig()
    {
        return [
            'isProductRegionRestrictionEnabled' => !!$this->helper->isEnabled(),
        ];
    }
}