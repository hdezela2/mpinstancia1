define(
    [
        'uiComponent',
        'jquery',
        'ko',
        'Magento_Ui/js/modal/modal',
        'mage/url',
        'Magento_Checkout/js/model/quote'
    ],
    function(
        Component,
        $,
        ko,
        modal,
        urlBuilder,
        quote
    ) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Summa_ProductRegionRestriction/popup'
            },

            initialize: function () {
                var self = this;
                this._super();

                var options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: $.mage.__('¡Advertencia!'),
                    buttons: [{
                        text: $.mage.__('Volver'),
                        class: 'action-primary',
                        click: function () {
                            var url = urlBuilder.build("checkout/cart");
                            window.location.href = url;
                        }
                    }]
                };

                if (window.checkoutConfig.isProductRegionRestrictionEnabled) {
                    $(document).on('click', '#co-shipping-method-form button', function(e) {
                        var region = null;
                        var allSame = true;

                        window.checkoutConfig.quoteItemData.forEach(function(i) {
                            if (!i.region || !allSame) {
                                return;
                            }

                            if (!region) {
                                region = i.region;
                                return;
                            }

                            if (region !== i.region) {
                                allSame = false;
                            }
                        });

                        if (!allSame) {
                            e.preventDefault();
                            e.stopPropagation();

                            $("#product-region-restriction-mixed").modal(options).modal("openModal");
                        } else if (region && quote.shippingAddress().regionId !== region) {
                            e.preventDefault();
                            e.stopPropagation();

                            $("#product-region-restriction-invalid-address").modal(options).modal("openModal");
                        }
                    });
                }
            },
        });
    }
);
