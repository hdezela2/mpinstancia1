<?php

namespace Summa\ProductPackages\Helper;

use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as MpProductCollection;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as SellerCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Webkul\MpAssignProduct\Model\ResourceModel\Items\CollectionFactory as ItemsFactory;
use \Magento\Framework\Message\ManagerInterface as MessageManager;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use Webkul\MpAssignProduct\Helper\Data as MpaHelper;
use Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Checkout\Helper\Cart as CartHelper;
use \Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    protected $customerRepositoryInterface;
    protected $productRepositoryInterface;
    protected $searchCriteriaBuilder;
    protected $mpProductCollection;
    protected $productCollection;
    protected $sellerCollection;
    protected $messageManager;
    protected $itemsFactory;
    protected $storeManager;
    protected $cartHelper;

    public function __construct
    (
        Context $context,
        CustomerRepositoryInterface $customerRepository,
        ProductCollection $productCollection,
        SellerCollection $sellerCollection,
        MpProductCollection  $mpProductCollection,
        StoreManagerInterface $storeManager,
        ItemsFactory $itemsFactory,
        CartHelper $cartHelper,
        MessageManager $messageManager
    )
    {
        $this->customerRepositoryInterface = $customerRepository;
        $this->mpProductCollection = $mpProductCollection;
        $this->productCollection = $productCollection;
        $this->sellerCollection = $sellerCollection;
        $this->messageManager = $messageManager;
        $this->storeManager = $storeManager;
        $this->itemsFactory = $itemsFactory;
        $this->cartHelper = $cartHelper;
        parent::__construct($context);
    }

    public function sellsPackages($sellerId)
    {
        $customerRepository = $this->customerRepositoryInterface->getById($sellerId);
        return $customerRepository->getCustomAttribute('wkv_dccp_sell_boxes_bags') ? $customerRepository->getCustomAttribute('wkv_dccp_sell_boxes_bags')->getValue() : false;
    }

    /*TODO: Ver el caso del carrito con producto simple
            Ver el caso del carrito que ya tiene paquetes
    */

    public function getPackagesBySellerId($sellerId, $skus)
    {
        $itemsCollection = $this->itemsFactory->create();
        $products = $this->getProducts($skus);
        $ids = [];
        foreach ($products as $product)
        {
            $ids[] = $product['entity_id'];
        }
        $itemsCollection->addFieldToFilter('product_id', ['in' => $ids ]);
        $itemsCollection->addFieldToFilter('seller_id', $sellerId);
        $itemsCollection->addFieldToFilter('status', 1);
        $data = $this->addUrl($itemsCollection->getData(), $products);
        return $data;
    }

    private function addUrl($items, $products)
    {
        $data = [];
        foreach ($products as $product)
        {
            foreach ($items as $item)
            {
                if($item['product_id'] == $product['entity_id'])
                {
                    $item['addurl'] = $this->cartHelper->getAddUrl($this->getProduct($product['entity_id']));
                    $item['name'] = $product['name'];
                    $data[] = $item;
                }
            }
        }
        return $data;
    }

    private function getProduct($id)
    {
        $collection = $this->productCollection->create();
        return $collection->addFieldToFilter('entity_id', $id)->getFirstItem();
    }

    private function getProducts($skus)
    {
        $collection = $this->getProductCollection();
        $collection->addFieldToFilter('type_id', 'simple');
        $collection->addStoreFilter($this->getWebsite());
        $collection->addAttributeToFilter('region', ['in' => $this->getRegions($skus)]);
        $collection->addAttributeToFilter('is_package', "1");
        $collection->addAttributeToFilter('name',  array('notnull' => true));
        return $collection->getData();
    }

    private function getRegions($skus)
    {
        $regions = [];
        $collection = $this->getProductCollection();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('sku', ['in' => $skus]);
        foreach ($collection->getItems() as $product)
        {
            $regions[] = $product->getRegion();
        }
        return $regions;
    }

    public function isProductAPackage($sku)
    {
        $collection = $this->getProductCollection();
        $collection->addAttributeToFilter('sku', $sku);
        $collection->addAttributeToFilter('is_package', "1");
        return count($collection->getItems()) > 0;
    }

    private function getWebsite()
    {
        return $this->storeManager->getStore(true);
    }

    public function arePackagesInItems($items, $sellerId)
    {
        $productIds = $this->getProductIdsFromItems($items);
        $collection = $this->getProductCollection();
        $collection->addFieldToFilter('entity_id', ['in' => $productIds]);
        $collection->addAttributeToFilter('is_package', "1");
        return $this->filterPackagesBySeller($collection, $sellerId);
    }

    private function filterPackagesBySeller($collection, $sellerId)
    {
        $ids = [];
        foreach ($collection->getItems() as $product)
        {
            $ids[] = $product->getEntityId();
        }
        $itemsCollection = $this->itemsFactory->create();
        $itemsCollection->addFieldToFilter('product_id', ['in' => $ids]);
        $itemsCollection->addFieldToFilter('seller_id', $sellerId);
        return json_encode(count($itemsCollection->getItems()) > 0);
    }

    public function areOnlyPackagesInItems($items, $proIdsArray)
    {
        $productIds = [];
        foreach ($items as $offer) {
            if (array_key_exists($offer->getId(), $proIdsArray)){
                $productIds[] = $offer->getProductId();
            }
        }
        $response = new \stdClass();
        $collection = $this->getProductCollection();
        $collection->addFieldToFilter('entity_id', ['in' => $productIds]);
        $collection->addAttributeToFilter('is_package', "1");
        if(count($productIds) == count($collection->getData()))
        {
            $response->areOnlyPackagesInItems = true;
            $this->messageManager->addErrorMessage(__("No es posible comprar solo envoltorios"));
            $response->message = "No es posible comprar solo envoltorios";
        }
        else
        {
            $response->areOnlyPackagesInItems = false;
            $response->message = "Ok";
        }
        return json_encode($response);
    }

    private function getProductCollection()
    {
        $collection = $this->productCollection->create();
        $collection->addAttributeToSelect('*');
        return $collection;
    }

    private function getProductIdsFromItems($items)
    {
        $productIds = [];
        foreach ($items as $item)
        {
            $productIds[] = $item->getProductId();
        }
        return $productIds;
    }

    public function getAllPackagesIds()
    {
        $ids = [];
        $collection = $this->getProductCollection();
        $collection->addAttributeToFilter('is_package', "1");
        foreach ($collection->getItems() as $product)
        {
            $ids[] = $product->getEntityId();
        }
        return $ids;
    }

    public function getSkuByItem($item)
    {
        if($item->getProduct()->getTypeId() == 'quote')
        {
            $composedSku = $item->getProduct()->getSku();
            $sku = explode('-', $composedSku)[1];
        }
        else
        {
            $sku = $item->getSku();
        }

        return $sku;
    }
}