<?php

namespace Summa\ProductPackages\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
    private $attributeSetFactory;
    private $categorySetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory,  AttributeSetFactory $attributeSetFactory, CategorySetupFactory $categorySetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        $attributeSet = $this->attributeSetFactory->create();
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
        $data =
        [
            'attribute_set_name' => 'Packages',
            'entity_type_id' => $entityTypeId,
            'sort_order' => 204
        ];
        $attributeSet->setData($data);
        $attributeSet->validate();
        $attributeSet->save();
        $attributeSet->initFromSkeleton($attributeSetId);
        $attributeSet->save();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            'catalog_product',
            'is_package',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Es envoltorio',
                'input' => 'boolean',
                'note' => 'El producto es un envoltorio',
                'class' => '',
                'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'searchable' => false,
                'filterable' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
            ]
        );

        $eavSetup->addAttribute(
            'catalog_product',
            'capacity',
            [
                'type' => 'text',
                'backend' => '',
                'frontend' => '',
                'label' => 'Capacidad',
                'input' => 'text',
                'note' => 'Capacidad del envoltorio',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 0,
                'searchable' => false,
                'filterable' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
            ]
        );

        $groupName = 'Product Details';
        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetName = 'Packages';
        $attributeSetId = $eavSetup->getAttributeSetId($entityTypeId, $attributeSetName);
        $attribute_group_id = $eavSetup->getAttributeGroupId($entityTypeId, $attributeSetId, $groupName);

        $attributeCodeArray = ['region', 'is_package', 'capacity'];

        foreach ($attributeCodeArray as $attribute_code)
        {
            $attribute_id=$eavSetup->getAttributeId($entityTypeId, $attribute_code);

            $eavSetup->addAttributeToSet($entityTypeId,$attributeSetId, $attribute_group_id, $attribute_id);
            $eavSetup->addAttributeToGroup(
                $entityTypeId,
                $attributeSetId,
                $attribute_group_id,
                $attribute_id,
                20
            );
        };

        $setup->endSetup();
    }
}