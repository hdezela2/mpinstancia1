<?php

namespace Summa\ProductPackages\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;


class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Config $eavConfig
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }


    /**
     * Returns true if attribute exists and false if it doesn't exist
     *
     * @param string $field
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isProductAttributeExists($field)
    {
        $attr = $this->eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $field);

        return ($attr && $attr->getId()) ? true : false;
    }


    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.2', '<'))
        {


            $setup->startSetup();
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            if($this->isProductAttributeExists('capacity')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'capacity');
            }

            $eavSetup->addAttribute(
                'catalog_product',
                'capacity',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Capacidad',
                    'input' => 'text',
                    'note' => 'Capacidad del envoltorio',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => 0,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'apply_to' => '',
                ]
            );

            //-------------- ADD attribute to set and group

            $groupName = 'Product Details';
            $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $attributeSetName = 'Packages';
            $attributeSetId = $eavSetup->getAttributeSetId($entityTypeId, $attributeSetName);
            $attributeGroupId = $eavSetup->getAttributeGroupId($entityTypeId, $attributeSetId, $groupName);

            $attributeCodeArray = ['capacity'];

            foreach ($attributeCodeArray as $attribute_code)
            {
                $attributeId = $eavSetup->getAttributeId($entityTypeId, $attribute_code);

                $eavSetup->addAttributeToSet($entityTypeId,$attributeSetId, $attributeGroupId, $attributeId);
                $eavSetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $attributeGroupId,
                    $attributeId,
                    20
                );
            };

            $setup->endSetup();
        }
    }
}