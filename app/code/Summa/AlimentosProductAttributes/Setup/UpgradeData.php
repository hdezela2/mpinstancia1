<?php

namespace Summa\AlimentosProductAttributes\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
    protected $eavSetupFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $_eavConfig;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Config $eavConfig
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;

        $this->_eavConfig = $eavConfig;
    }

    /**
     * Returns true if attribute exists and false if it doesn't exist
     *
     * @param string $field
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isProductAttributeExists($field)
    {
        $attr = $this->_eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $field);

        return ($attr && $attr->getId()) ? true : false;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context){
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $setup->startSetup();
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            //-------------- REMOVE OLD ATTRIBUTES
            if($this->isProductAttributeExists('alto_azucar')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'alto_azucar');
            }
            if($this->isProductAttributeExists('alto_calorias')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'alto_calorias');
            }
            if($this->isProductAttributeExists( 'alto_grasas')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'alto_grasas');
            }
            if($this->isProductAttributeExists( 'alto_sodio')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'alto_sodio');
            }
            if($this->isProductAttributeExists( 'total_azucares')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'total_azucares');
            }
            if($this->isProductAttributeExists( 'total_energia')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'total_energia');
            }
            if($this->isProductAttributeExists( 'total_grasas')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'total_grasas');
            }
            if($this->isProductAttributeExists( 'total_hidratos')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'total_hidratos');
            }
            if($this->isProductAttributeExists( 'total_proteinas')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'total_proteinas');
            }
            if($this->isProductAttributeExists( 'total_sodio')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'total_sodio');
            }

            //-------------- CREATE NEW ATTRIBUTES
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'alto_azucar',
                [
                    'type'          => 'int',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'ALTO EN AZUCARES',
                    'input'         => 'boolean',
                    'class'         => '',
                    'source'        => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => false,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'alto_calorias',
                [
                    'type'          => 'int',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'ALTO EN CALORIAS',
                    'input'         => 'boolean',
                    'class'         => '',
                    'source'        => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => false,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'alto_grasas',
                [
                    'type'          => 'int',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'ALTO EN GRASAS SATURADAS',
                    'input'         => 'boolean',
                    'class'         => '',
                    'source'        => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => false,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'alto_sodio',
                [
                    'type'          => 'int',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'ALTO EN SODIO',
                    'input'         => 'boolean',
                    'class'         => '',
                    'source'        => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => false,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'total_azucares',
                [
                    'type'          => 'text',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'AZUCARES TOTALES (G)',
                    'input'         => 'text',
                    'class'         => '',
                    'source'        => '',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => false,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'total_energia',
                [
                    'type'          => 'text',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'ENERGIA (KCAL)',
                    'input'         => 'text',
                    'class'         => '',
                    'source'        => '',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => false,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'total_grasas',
                [
                    'type'          => 'text',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'GRASAS TOTALES (G)',
                    'input'         => 'text',
                    'class'         => '',
                    'source'        => '',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => false,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'total_hidratos',
                [
                    'type'          => 'text',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'H. DE C DISP. (G)',
                    'input'         => 'text',
                    'class'         => '',
                    'source'        => '',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => false,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'total_proteinas',
                [
                    'type'          => 'text',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'PROTEINAS (G)',
                    'input'         => 'text',
                    'class'         => '',
                    'source'        => '',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => false,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'total_sodio',
                [
                    'type'          => 'text',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'SODIO (MG)',
                    'input'         => 'text',
                    'class'         => '',
                    'source'        => '',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => false,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );

            // Add attributes to set and group
            $attributeCodeArray = ['alto_azucar', 'alto_calorias', 'alto_grasas', 'alto_sodio', 'total_azucares', 'total_energia', 'total_grasas', 'total_hidratos', 'total_proteinas', 'total_sodio' ]; 
            $entity_type_id = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $group_name = 'Nutritional';
            $attributeSetsCodeArray = [
                'Default Alimentos',
                'AGUA MINERAL CON GAS',
                'LECHUGA FRESCA',
                'CORTE ASADO CARNICERO ',
            ];

            foreach ($attributeSetsCodeArray as $attribute_set_name) {
                $attribute_set_id = $eavSetup->getAttributeSetId($entity_type_id, $attribute_set_name);
                $eavSetup->addAttributeGroup($entity_type_id, $attribute_set_id, $group_name, 100);
                $attribute_group_id = $eavSetup->getAttributeGroupId($entity_type_id, $attribute_set_id, $group_name);

                foreach ($attributeCodeArray as $attribute_code) {
                    $attribute_id = $eavSetup->getAttributeId($entity_type_id, $attribute_code);
                    $eavSetup->addAttributeToSet($entity_type_id, $attribute_set_id, $attribute_group_id, $attribute_id);
                    $eavSetup->addAttributeToGroup(
                        $entity_type_id,
                        $attribute_set_id,
                        $attribute_group_id,
                        $attribute_id,
                        20
                    );
                };
            };

            $setup->endSetup();
        }

        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $setup->startSetup();
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            //-------------- REMOVE OLD ATTRIBUTES
            if($this->isProductAttributeExists('childs_skus')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'childs_skus');
            }

            //-------------- CREATE NEW ATTRIBUTES
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'childs_skus',
                [
                    'type'          => 'text',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'Childs SKUs',
                    'input'         => 'text',
                    'class'         => '',
                    'source'        => '',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => true,
                    'filterable'    => false,
                    'comparable'    => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );

            // Add attributes to set and group
            $attribute_code = 'childs_skus'; 
            $entity_type_id = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $group_name = 'General';
            $attributeSetsCodeArray = [
                'Default Alimentos',
                'AGUA MINERAL CON GAS',
                'LECHUGA FRESCA',
                'CORTE ASADO CARNICERO ',
            ];

            foreach ($attributeSetsCodeArray as $attribute_set_name) {
                $attribute_set_id = $eavSetup->getAttributeSetId($entity_type_id, $attribute_set_name);
                $attribute_group_id = $eavSetup->getAttributeGroupId($entity_type_id, $attribute_set_id, $group_name);

                $attribute_id = $eavSetup->getAttributeId($entity_type_id, $attribute_code);
                $eavSetup->addAttributeToSet($entity_type_id, $attribute_set_id, $attribute_group_id, $attribute_id);
                $eavSetup->addAttributeToGroup(
                    $entity_type_id,
                    $attribute_set_id,
                    $attribute_group_id,
                    $attribute_id,
                    20
                );
            };

            $setup->endSetup();
        }
    }
}