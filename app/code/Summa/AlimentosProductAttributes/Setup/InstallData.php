<?php
namespace Summa\AlimentosProductAttributes\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory;

class InstallData implements InstallDataInterface
{
    protected $eavSetupFactory;
    protected $attributeFactory;
    protected $categorySetupFactory;
    protected $attributeSetFactory;

    public function __construct(SetFactory $attributeSetFactory, EavSetupFactory $eavSetupFactory, Attribute $attributeFactory, CategorySetupFactory $categorySetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeFactory = $attributeFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        // Create Default Alimentos attribute set
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);

        $attributeSet = $this->attributeSetFactory->create();
        $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
        $data = [
            'attribute_set_name' => 'Default Alimentos',
            'entity_type_id' => $entityTypeId,
            'sort_order' => 203,
        ];
        $attributeSet->setData($data);
        $attributeSet->validate();
        $attributeSet->save();
        $attributeSet->initFromSkeleton($attributeSetId);
        $attributeSet->save();

        $defaultAttributeSetId = $attributeSet->getId();

        // Create Agua, Lechuga and Corte attribute sets
        $attributeSets = [
            'AGUA MINERAL CON GAS',
            'LECHUGA FRESCA',
            'CORTE ASADO CARNICERO ',
        ];

        foreach ($attributeSets as $attrSetCode) {
            $data = [
                'attribute_set_name' => $attrSetCode,
                'entity_type_id' => $entityTypeId,
                'sort_order' => 204,
            ];
            $attributeSet = $this->attributeSetFactory->create();
            $attributeSet->setData($data);
            $attributeSet->validate();
            $attributeSet->save();
            $attributeSet->initFromSkeleton($defaultAttributeSetId);
            $attributeSet->save();
        }

        // Create origen 
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $values = ['values' => [
            "NACIONAL",
            "IMPORTADA",
        ]];

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'origen_importacion',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Origen',
                'input' => 'select',
                'note' => '',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => '',
                'option' => $values
            ]
        );

        // Create marinado 
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $values = ['values' => [
            "MARINADO",
            "SIN MARINAR",
        ]];

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'marinado',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Marinado',
                'input' => 'select',
                'note' => '',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => '',
                'option' => $values
            ]
        );

        // Add attributes to set and group
        $entity_type_id = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $group_name = 'Product Details';
        $attributeCodeArray = [
            'AGUA MINERAL CON GAS' => ['region', 'marca'],
            'LECHUGA FRESCA' => ['region'],
            'CORTE ASADO CARNICERO ' => ['region', 'marinado', 'origen_importacion'],
        ];

        foreach ($attributeCodeArray as $attribute_set_name => $attributeCodes) {
            $attribute_set_id = $eavSetup->getAttributeSetId($entity_type_id, $attribute_set_name);
            $attribute_group_id = $eavSetup->getAttributeGroupId($entity_type_id, $attribute_set_id, $group_name);

            foreach ($attributeCodes as $attribute_code) {
                $attribute_id = $eavSetup->getAttributeId($entity_type_id, $attribute_code);
                $eavSetup->addAttributeToSet($entity_type_id, $attribute_set_id, $attribute_group_id, $attribute_id);
                $eavSetup->addAttributeToGroup(
                    $entity_type_id,
                    $attribute_set_id,
                    $attribute_group_id,
                    $attribute_id,
                    20
                );
            };
        };

        $marcas = [
            "CACHANTUN",
            "NESTLE",
            "BENEDICTINO",
        ];
        $attributeInfo = $this->attributeFactory->getCollection()
            ->addFieldToFilter('attribute_code', ['eq'=>"marca"])
            ->getFirstItem();

        $option = [];
        $option['attribute_id'] = $attributeInfo->getAttributeId();

        foreach($marcas as $value){
            $option['value'][$value][0] = $value;
        }

        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup->addAttributeOption($option);

        $setup->endSetup();
    }
}
