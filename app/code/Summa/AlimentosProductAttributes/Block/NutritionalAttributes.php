<?php
namespace Summa\AlimentosProductAttributes\Block;

use Magento\Catalog\Model\Product;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Catalog\Block\Product\View\Attributes;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;

class NutritionalAttributes extends Attributes
{
    /**
     * @var CollectionFactory
     */
    protected $groupCollectionFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PriceCurrencyInterface $priceCurrency
     * @param CollectionFactory $groupCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        PriceCurrencyInterface $priceCurrency,
        CollectionFactory $groupCollectionFactory,
        array $data = []
    ) {
        $this->groupCollectionFactory = $groupCollectionFactory;
        parent::__construct($context, $registry, $priceCurrency, $data);
    }

    /**
     * $excludeAttr is optional array of attribute codes to exclude them from additional data array
     *
     * @param array $excludeAttr
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAdditionalData(array $excludeAttr = [])
    {
        $data = [];
        $product = $this->getProduct();

        $attributeSetId = $product->getAttributeSetId();

        $group = $this->groupCollectionFactory->create()
            ->setAttributeSetFilter($attributeSetId)
            ->addFieldToFilter('attribute_group_code', 'nutritional')
            ->setSortOrder()
            ->getFirstItem();

        if (!$group->getId()) {
            return $data;
        }

        $attributes = $product->getAttributes($group->getId(), true);
        foreach ($attributes as $attribute) {
            if ($this->isVisibleOnFrontend($attribute, $excludeAttr)) {
                $value = $attribute->getFrontend()->getValue($product);

                if ($value instanceof Phrase) {
                    $value = (string)$value;
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = $this->priceCurrency->convertAndFormat($value);
                }

                if (is_string($value) && strlen($value)) {
                    $data[$attribute->getAttributeCode()] = [
                        'label' => __($attribute->getStoreLabel()),
                        'value' => $value,
                        'code' => $attribute->getAttributeCode(),
                    ];
                }
            }
        }
        return $data;
    }
}
