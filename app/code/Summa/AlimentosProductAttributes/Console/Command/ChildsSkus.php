<?php

namespace Summa\AlimentosProductAttributes\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Store\Model\StoreFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Console\Cli;

class ChildsSkus extends Command
{
    protected $storeFactory;

    protected $eavConfig;

    protected $eavSetupFactory;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    public function __construct(
        \Magento\Framework\App\State $appState,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Eav\Model\Config $eavConfig,
        StoreFactory $storeFactory,
        EavSetupFactory $eavSetupFactory,
        $name = null
    ) {

        $this->appState = $appState;
        $this->storeFactory = $storeFactory;
        $this->eavConfig = $eavConfig;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('product:childskus');
        $this->setDescription("Update Products childs_skus attribute");

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $eavSetup = $this->eavSetupFactory->create();
        $store = $this->storeFactory->create();
        $store->load('alimentos');
        $attributeCode = 'childs_skus';
        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeId = $eavSetup->getAttributeId($entityTypeId, $attributeCode);

        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addFilter('type_id', 'configurable');
        $collection->addStoreFilter($store->getId());

        foreach ($collection as $product) {
            try {
                $attributeSetId = $product->getAttributeSetId();

                $attributes = $this->eavConfig->getEntityAttributeCodes(
                    \Magento\Catalog\Model\Product::ENTITY,
                    $product
                );

                if (!in_array('child_skus', $attributes)) {
                    // Add attributes to set and group
                    $groupName = 'General';

                    $attributeGroupId = $eavSetup->getAttributeGroupId($entityTypeId, $attributeSetId, $groupName);

                    $eavSetup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId);
                    $eavSetup->addAttributeToGroup(
                        $entityTypeId,
                        $attributeSetId,
                        $attributeGroupId,
                        $attributeId,
                        20
                    );
                }
                $children = $product->getTypeInstance()->getUsedProducts($product);
                $childsSkus = [];
                foreach ($children as $child){
                    $childsSkus[] = $child->getSku();
                }

                $product->setChildsSkus(implode(' ', $childsSkus));
                $product->save();

                $output->writeln('Product '.$product->getSku().' updated');
            } catch (\Exception $e) {
                $output->writeln('Error with product '.$product->getSku().'. Message: '.$e->getMessage());
            }
        }
        return Cli::RETURN_SUCCESS;
    }
}
