<?php
/**
 * Created by PhpStorm.
 * User: diegosana
 * Date: 23/12/19
 * Time: 15:23
 */

namespace Summa\ComputadoresSetUp\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    const WEBSITE_CODE = 'computadores';

    /**
     * @var \Magento\Store\Model\ResourceModel\Website\CollectionFactory
     */
    protected $_websiteCollectionFactory;
    /**
     * @var \Magento\Store\Model\ResourceModel\Website
     */
    protected $_website = null;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;
    /**
     * Data constructor.
     * @param Context $context
     * @param \Magento\Store\Model\ResourceModel\Website\CollectionFactory $websiteCollectionFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     */
    public function __construct(
        Context $context,
        \Magento\Store\Model\ResourceModel\Website\CollectionFactory $websiteCollectionFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->_websiteCollectionFactory = $websiteCollectionFactory;
        $this->_customerFactory = $customerFactory;
        $this->_resource = $resource;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Store\Model\ResourceModel\Website|null
     */
    public function getWebsite()
    {
        if (!$this->_website) {
            $this->_website = $this->_websiteCollectionFactory->create()->addFieldToFilter('code', self::WEBSITE_CODE)->getFirstItem();
        }

        return $this->_website;
    }

    public function isComputersSeller($sellerId)
    {
        $query = "select website_id from customer_entity where entity_id = " . (int) $sellerId;
        $result = $this->_resource->getConnection()->query($query);
        if ($result->rowCount() === 1 && $this->getWebsite()->getId() == $result->fetchAll()[0]['website_id']) {
            return true;
        }

        return false;
    }

}