# Mage2 Module Summa EmergenciasSetUp

    ``summa/module-emergenciassetup``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Creación de categorías, website, store y store view.

Creación de root category

Agrega rangos de peso "dccp_weight"

Agrega attributos de productos "estado_situacion" y "region"

## Installation

 - Unzip the zip file in `app/code/Summa`
 - Enable the module by running `php bin/magento module:enable Summa_EmergenciasSetUp`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer



## Configuration




## Specifications




## Attributes



