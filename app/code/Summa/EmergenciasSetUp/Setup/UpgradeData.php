<?php
/**
 * Created by PhpStorm.
 * User: diegosana
 * Date: 17/12/19
 * Time: 11:28
 */

namespace Summa\EmergenciasSetUp\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Store\Model\WebsiteFactory;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var WebsiteFactory
     */
    private $websiteFactory;

    public function __construct(
        WebsiteFactory $websiteFactory
    )
    {
        $this->websiteFactory = $websiteFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            /** @var \Magento\Store\Model\Website $website */
            $website = $this->websiteFactory->create();
            $website->load('emergencias');

            if($website->getId()) {

                $querys[] = "insert into dccp_weight (dimension_label, weight_from, weight_to, website_id) values ('Pequeño', 0.0001, 34.9999, ".$website->getId().");";
                $querys[] = "insert into dccp_weight (dimension_label, weight_from, weight_to, website_id) values ('Mediano', 35.0000, 99.9999, ".$website->getId().");";
                $querys[] = "insert into dccp_weight (dimension_label, weight_from, weight_to, website_id) values ('Grande', 100.0000, 249.9999, ".$website->getId().");";
                $querys[] = "insert into dccp_weight (dimension_label, weight_from, weight_to, website_id) values ('Kilo Adicional', 250.0000, 99999999.0000, ".$website->getId().");";

                foreach ($querys as $query) {
                    $setup->getConnection()->query($query);
                }
            }
        }
    }
}