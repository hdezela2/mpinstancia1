<?php

namespace Summa\RegionalCondition\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

class UpdateValidationDataEmergencia implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var int
     */
    protected $websiteId;

    /**
     * @var \Summa\EmergenciasSetUp\Helper\Data
     */
    protected $emergencyHelper;

    /**
     * UpdateCustomerAttributesMetadata constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        \Summa\EmergenciasSetUp\Helper\Data $emergencyHelper,
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->emergencyHelper = $emergencyHelper;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->websiteId = $this->emergencyHelper->getWebsite()->getId();
    }

    /**
     * {@inheritdoc}
     *
     * @return $this
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $validations = [
            [
                'postname' => 'enable_storepickup',
                'label' => 'Retiro en tienda',
                'website_id' => $this->websiteId,
                'validation' => 'none',
                'regex_value' => null,
                'required' => 1,
                'value_must_be' => 'greather_equal',
                'max_length' => 0,
                'format_value' => 'none',
                'sort_order' => 7
            ],
            [
                'postname' => 'time_maximum_emergency_delivery',
                'label' => 'Tiempo Maximo de entrega en Emergencia',
                'website_id' => $this->websiteId,
                'validation' => 'regex',
                'regex_value' => "/^(4[0-8]|[1-3][0-9]|[1-9])$/",
                'required' => 1,
                'value_must_be' => 'lower_equal',
                'max_length' => 2,
                'format_value' => 'number',
                'sort_order' => 13
            ],
            [
                'postname' => 'time_maximum_contingency_delivery',
                'label' => 'Tiempo Maximo de entrega en Contingencia',
                'website_id' => $this->websiteId,
                'validation' => 'regex',
                'regex_value' => "/^(7[0-2]|[1-6][0-9]|[1-9])$/",
                'required' => 1,
                'value_must_be' => 'lower_equal',
                'max_length' => 2,
                'format_value' => 'number',
                'sort_order' => 14
            ],
            [
                'postname' => 'time_maximum_prevention_delivery',
                'label' => 'Tiempo Maximo de entrega en Prevencion',
                'website_id' => $this->websiteId,
                'validation' => 'regex',
                'regex_value' => "/^(1[0-5]|[1-9])$/",
                'required' => 1,
                'value_must_be' => 'lower_equal',
                'max_length' => 2,
                'format_value' => 'number',
                'sort_order' => 15
            ]
        ];

        /**
         * Insert validations
         */
        foreach ($validations as $validation) {
            $connection->update(
                $this->moduleDataSetup->getTable('dccp_validation_regional_condition'),
                [
                    'postname' => $validation['postname'],
                    'label' => $validation['label'],
                    'website_id' => $validation['website_id'],
                    'validation' => $validation['validation'],
                    'regex_value' => $validation['regex_value'],
                    'required' => $validation['required'],
                    'value_must_be' => $validation['value_must_be'],
                    'max_length' => $validation['max_length'],
                    'format_value' => $validation['format_value'],
                    'sort_order' => $validation['sort_order']
                ],
                ["website_id = " . $this->websiteId . " AND postname = '" . $validation['postname'] . "'"]
            );
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '1.0.1';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
