<?php

namespace Summa\RegionalCondition\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

class UpdateValidationDataAlimentos implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var int
     */
    protected $websiteId;

    /**
     * @var \Summa\AlimentosSetUp\Helper\Data
     */
    protected $alimentosHelper;


    /**
     * UpdateValidationDataAlimentos constructor.
     * @param \Summa\AlimentosSetUp\Helper\Data $alimentosHelper
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        \Summa\AlimentosSetUp\Helper\Data $alimentosHelper,
        ModuleDataSetupInterface $moduleDataSetup)
    {
        $this->alimentosHelper = $alimentosHelper;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->websiteId = $this->alimentosHelper->getWebsite()->getId();
    }

    /**
     * @return $this
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $validations =
            [
                [
                    'postname' => 'maximum_amount',
                    'label' => 'Monto máximo orden de compra',
                    'website_id' => $this->websiteId,
                    'validation' => 'numeric_ilimited',
                    'regex_value' => null,
                    'required' => 1,
                    'value_must_be' => 'greather_equal',
                    'max_length' => 0,
                    'format_value' => 'price',
                    'sort_order' => 5
                ],
                [
                    'postname' => 'time_maximum_emergency_delivery',
                    'label' => 'Tiempo Maximo de entrega en Emergencia',
                    'website_id' => $this->websiteId,
                    'validation' => 'regex',
                    'regex_value' => "/^(4[0-8]|[1-3][0-9]|[1-9])$/",
                    'required' => 1,
                    'value_must_be' => 'lower_equal',
                    'max_length' => 2,
                    'format_value' => 'number',
                    'sort_order' => 13
                ]
            ];
        $UpdateValidations =
            [
                [
                    'postname' => 'maximum_delivery',
                    'label' => 'Plazo de entrega máximo (días hábiles)',
                    'website_id' => $this->websiteId,
                    'validation' => 'regex',
                    'regex_value' => "/^[0-3]*$/",
                    'required' => 1,
                    'value_must_be' => 'lower_equal',
                    'max_length' => 1,
                    'format_value' => 'number',
                    'sort_order' => 5
                ]
            ];

        foreach ($validations as $validation)
        {
            $connection->insertForce(
                $this->moduleDataSetup->getTable('dccp_validation_regional_condition'),
                [
                    'postname' => $validation['postname'],
                    'label' => $validation['label'],
                    'website_id' => $validation['website_id'],
                    'validation' => $validation['validation'],
                    'regex_value' => $validation['regex_value'],
                    'required' => $validation['required'],
                    'value_must_be' => $validation['value_must_be'],
                    'max_length' => $validation['max_length'],
                    'format_value' => $validation['format_value'],
                    'sort_order' => $validation['sort_order']
                ]
            );
        }
        foreach ($UpdateValidations as $validation) {
            $connection->update(
                $this->moduleDataSetup->getTable('dccp_validation_regional_condition'),
                [
                    'postname' => $validation['postname'],
                    'label' => $validation['label'],
                    'website_id' => $validation['website_id'],
                    'validation' => $validation['validation'],
                    'regex_value' => $validation['regex_value'],
                    'required' => $validation['required'],
                    'value_must_be' => $validation['value_must_be'],
                    'max_length' => $validation['max_length'],
                    'format_value' => $validation['format_value'],
                    'sort_order' => $validation['sort_order']
                ],
                ["website_id = " . $this->websiteId . " AND postname = '" . $validation['postname'] . "'"]
            );
        }
        return $this;
    }

    public static function getDependencies()
    {
        return [];
    }

    public static function getVersion()
    {
        return '1.0.1';
    }

    public function getAliases()
    {
        return [];
    }
}
