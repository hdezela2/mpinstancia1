<?php

namespace Summa\RegionalCondition\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

class InitializeValidationDataAlimentos implements DataPatchInterface, PatchVersionInterface
{
    private $moduleDataSetup;
    private $websiteId;
    private $alimentosHelper;

    public function __construct(\Summa\AlimentosSetUp\Helper\Data $alimentosHelper, ModuleDataSetupInterface $moduleDataSetup)
    {
        $this->alimentosHelper = $alimentosHelper;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->websiteId = $this->alimentosHelper->getWebsite()->getId();
    }

    /**
     * @return $this
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $validations =
            [
                [
                    'postname' => 'contact_name',
                    'label' => 'Nombre del contacto',
                    'website_id' => $this->websiteId,
                    'validation' => 'letter',
                    'regex_value' => null,
                    'required' => 1,
                    'value_must_be' => 'none',
                    'max_length' => 255,
                    'format_value' => 'none',
                    'sort_order' => 2
                ],
                [
                    'postname' => 'contact_phone',
                    'label' => 'Teléfono de contacto',
                    'website_id' => $this->websiteId,
                    'validation' => 'regex',
                    'regex_value' => "/^[269][0-9]*$/", //"/^[\+\d]+(?:[\d-.\s()]*)$/",
                    'required' => 1,
                    'value_must_be' => 'none',
                    'max_length' => 10,
                    'format_value' => 'none',
                    'sort_order' => 3
                ],
                [
                    'postname' => 'contact_email',
                    'label' => 'Correo de contacto',
                    'website_id' => $this->websiteId,
                    'validation' => 'email',
                    'regex_value' => null,
                    'required' => 1,
                    'value_must_be' => 'none',
                    'max_length' => 150,
                    'format_value' => 'none',
                    'sort_order' => 4
                ],
                [
                    'postname' => 'maximum_delivery',
                    'label' => 'MONTO MÁXIMO ORDEN DE COMPRA',
                    'website_id' => $this->websiteId,
                    'validation' => 'regex',
                    'regex_value' => "/^[0-3]*$/",
                    'required' => 1,
                    'value_must_be' => 'lower_equal',
                    'max_length' => 1,
                    'format_value' => 'number',
                    'sort_order' => 5
                ],
                [
                    'postname' => 'excluded_comunes',
                    'label' => 'Comunas excluidas',
                    'website_id' => $this->websiteId,
                    'validation' => 'none',
                    'regex_value' => "",
                    'required' => 1,
                    'value_must_be' => 'lower_equal_list',
                    'max_length' => 2000,
                    'format_value' => 'none',
                    'sort_order' => 6
                ]
            ];

        foreach ($validations as $validation)
        {
            $connection->insertForce(
                $this->moduleDataSetup->getTable('dccp_validation_regional_condition'),
                [
                    'postname' => $validation['postname'],
                    'label' => $validation['label'],
                    'website_id' => $validation['website_id'],
                    'validation' => $validation['validation'],
                    'regex_value' => $validation['regex_value'],
                    'required' => $validation['required'],
                    'value_must_be' => $validation['value_must_be'],
                    'max_length' => $validation['max_length'],
                    'format_value' => $validation['format_value'],
                    'sort_order' => $validation['sort_order']
                ]
            );
        }
        return $this;
    }

    public static function getDependencies()
    {
        return [];
    }

    public static function getVersion()
    {
        return '1.0.1';
    }

    public function getAliases()
    {
        return [];
    }
}
