<?php

namespace Summa\RegionalCondition\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

class InitializeValidationDataComputadores implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var int
     */
    protected $websiteId;

    /**
     * @var \Summa\ComputadoresSetUp\Helper\Data
     */
    protected $compHelper;

    /**
     * UpdateCustomerAttributesMetadata constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        \Summa\ComputadoresSetUp\Helper\Data $compHelper,
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->compHelper = $compHelper;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->websiteId = $this->compHelper->getWebsite()->getId();;
    }

    /**
     * {@inheritdoc}
     *
     * @return $this
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $validations = [
            [
                'postname' => 'contact_name',
                'label' => 'Nombre del contacto',
                'website_id' => $this->websiteId,
                'validation' => 'letter',
                'regex_value' => null,
                'required' => 1,
                'value_must_be' => 'none',
                'max_length' => 255,
                'format_value' => 'none',
                'sort_order' => 2
            ],
            [
                'postname' => 'contact_phone',
                'label' => 'Teléfono de contacto',
                'website_id' => $this->websiteId,
                'validation' => 'regex',
                'regex_value' => "/^[269][0-9]*$/", //"/^[\+\d]+(?:[\d-.\s()]*)$/",
                'required' => 1,
                'value_must_be' => 'none',
                'max_length' => 10,
                'format_value' => 'none',
                'sort_order' => 3
            ],
            [
                'postname' => 'contact_email',
                'label' => 'Correo de contacto',
                'website_id' => $this->websiteId,
                'validation' => 'email',
                'regex_value' => null,
                'required' => 1,
                'value_must_be' => 'none',
                'max_length' => 150,
                'format_value' => 'none',
                'sort_order' => 4
            ]
        ];

        /**
         * Insert validations
         */
        foreach ($validations as $validation) {
            $connection->insertForce(
                $this->moduleDataSetup->getTable('dccp_validation_regional_condition'),
                [
                    'postname' => $validation['postname'],
                    'label' => $validation['label'],
                    'website_id' => $validation['website_id'],
                    'validation' => $validation['validation'],
                    'regex_value' => $validation['regex_value'],
                    'required' => $validation['required'],
                    'value_must_be' => $validation['value_must_be'],
                    'max_length' => $validation['max_length'],
                    'format_value' => $validation['format_value'],
                    'sort_order' => $validation['sort_order']
                ]
            );
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '1.0.1';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
