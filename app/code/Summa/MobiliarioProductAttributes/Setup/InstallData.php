<?php

namespace Summa\MobiliarioProductAttributes\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    private $eavSetupFactory;
    private $attributeSetFactory;
    private $categorySetupFactory;

    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory,
        \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        $attributeSet = $this->attributeSetFactory->create();
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
        $data = [
            'attribute_set_name' => 'Default Mobiliario',
            'entity_type_id' => $entityTypeId,
            'sort_order' => 203,
        ];
        $attributeSet->setData($data);
        $attributeSet->validate();
        $attributeSet->save();
        $attributeSet->initFromSkeleton($attributeSetId);
        $attributeSet->save();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'requires_assembly',
            [
                'type'          => 'int',
                'backend'       => '',
                'frontend'      => '',
                'label'         => 'Requiere Armado',
                'note'          => 'Requiere Armado',
                'input'         => 'boolean',
                'class'         => '',
                'source'        => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible'       => true,
                'required'      => false,
                'user_defined'  => true,
                'default'       => '0',
                'searchable'    => false,
                'filterable'    => true,
                'comparable'    => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique'        => false,
                'apply_to'      => ''
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'assembly_included',
            [
                'type'          => 'int',
                'backend'       => '',
                'frontend'      => '',
                'label'         => 'Incluye costo de Armado',
                'note'          => 'Incluye costo de Armado',
                'input'         => 'boolean',
                'class'         => '',
                'source'        => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible'       => true,
                'required'      => false,
                'user_defined'  => true,
                'default'       => '0',
                'searchable'    => true,
                'filterable'    => true,
                'comparable'    => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique'        => false,
                'apply_to'      => ''
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'assembly_difficulty',
            [
                'type'          => 'int',
                'backend'       => '',
                'frontend'      => '',
                'label'         => 'Dificultad de Armado',
                'input'         => 'select',
                'note'          => 'Dificultad de Armado',
                'class'         => '',
                'source'        => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
                'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible'       => true,
                'required'      => false,
                'user_defined'  => true, //esto permite que no se agregue a todos los attributeset
                'default'       => '',
                'searchable'    => true,
                'filterable'    => true,
                'comparable'    => true,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique'        => false,
                'apply_to'      => '',
                'option' => [
                    'values' => [],
                ],
            ]
        );

        //-------------- ADD attribute to set and group
        $entity_type_id = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attribute_set_name = 'Default Mobiliario';
        $group_name = 'Product Details';


        $attribute_set_id=$eavSetup->getAttributeSetId($entity_type_id, $attribute_set_name);
        $attribute_group_id=$eavSetup->getAttributeGroupId($entity_type_id, $attribute_set_id, $group_name);

        $attributeCodeArray = [
            'assembly_included',
            'requires_assembly',
            'assembly_difficulty',
            'linked_products',
            'gamma'
        ];

        foreach ($attributeCodeArray as $attribute_code){
            $attribute_id=$eavSetup->getAttributeId($entity_type_id, $attribute_code);

            $eavSetup->addAttributeToSet($entity_type_id,$attribute_set_id, $attribute_group_id, $attribute_id);
            $eavSetup->addAttributeToGroup(
                $entity_type_id,
                $attribute_set_id,
                $attribute_group_id,
                $attribute_id,
                20
            );
        };

        $setup->endSetup();

    }
}