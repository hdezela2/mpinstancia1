<?php

namespace Summa\RegionalConditionExpansion\Controller\Seller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use \Magento\Customer\Model\Session;
use Webkul\Marketplace\Helper\Data as Helper;

class RegionalForm extends Action
{
    protected $customerSession;
    protected $resultPageFactory;
    protected $helper;

    public function __construct(Context $context, PageFactory $pageFactory, Session $customerSession, Helper $helper)
    {
        $this->customerSession = $customerSession;
        $this->resultPageFactory = $pageFactory;
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->_objectManager->get('Magento\Customer\Model\Url')->getLoginUrl();
        if (!$this->customerSession->authenticate($loginUrl))
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);

        return parent::dispatch($request);
    }

    public function execute()
    {
        if($this->helper->isSeller() == 1)
        {
            $resultPage = $this->resultPageFactory->create();
            if($this->helper->getIsSeparatePanel())
            {
                $resultPage->addHandle('regionalconditionexpansion_layout2_seller_regionalform');
            }
            $resultPage->getConfig()->getTitle()->set(__('Regional Condition Expansion'));
            return $resultPage;
        }
        else
        {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
}