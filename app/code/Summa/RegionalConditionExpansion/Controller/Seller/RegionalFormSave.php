<?php

namespace Summa\RegionalConditionExpansion\Controller\Seller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use \Magento\Customer\Model\Session;
use Webkul\Marketplace\Helper\Data as Helper;
use Summa\RegionalConditionExpansion\Model\RegionalConditionRequest;
use Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory;
use Summa\RegionalConditionExpansion\Model\RegionalConditionAttribute;
use Summa\RegionalConditionExpansion\Model\RegionalConditionAttributeFactory;
use Summa\RegionalConditionExpansion\Logger\Logger;

class RegionalFormSave extends Action
{
    /**
     * @var RegionalConditionRequestFactory
     */
    protected $regionalConditionRequestFactory;

    /**
     * @var RegionalConditionAttributeFactory
     */
    protected $regionalConditionAttributeFactory;

    /**
     * @var
     */
    protected $request;

    /**
     * @var \Summa\RegionalConditionExpansion\Helper\Data
     */
    protected $helperdata;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * RegionalFormSave constructor.
     * @param Context $context
     * @param RegionalConditionRequestFactory $regionalConditionRequestFactory
     * @param RegionalConditionAttributeFactory $regionalConditionAttributeFactory
     * @param \Summa\RegionalConditionExpansion\Helper\Data $data
     * @param Logger $logger
     */
    public function __construct(
        Context $context,
        RegionalConditionRequestFactory $regionalConditionRequestFactory,
        RegionalConditionAttributeFactory $regionalConditionAttributeFactory,
        \Summa\RegionalConditionExpansion\Helper\Data $data,
        Logger $logger
    )
    {
        $this->regionalConditionRequestFactory = $regionalConditionRequestFactory;
        $this->regionalConditionAttributeFactory = $regionalConditionAttributeFactory;
        $this->logger = $logger;
        parent::__construct($context);
        $this->helperdata = $data;
    }

    public function execute()
    {
        $this->request = $this->getRequest()->getPostValue();
        $regionalRequest = $this->persistRequest();
        $attributesSaved = $this->persistAttribute($regionalRequest->getEntityId());
        if ($attributesSaved['error']){
            $this->logger->info('Solicitud de expansion regional guardada con errores Error: '.$attributesSaved['error_message']);
            $regionalRequest->setProcessed(1);
            $regionalRequest->save();
            $this->messageManager->addError(__('We can\'t save you regional condition expansion request'));
        }else{
            $this->messageManager->addSuccess(__('Data was saved succesfully.'));
        }
        $this->_redirect('regionalconditionexpansion/seller/regionalForm');
        $this->logger->info('Solicitud de expansion regional guardada - Datos solicitud: ' . json_encode($regionalRequest->getData()) . " Datos atributos: " . json_encode($attributesSaved['saved']));
        return;
    }

    private function persistAttribute($regionalConditionId)
    {
        $attributes = $this->getAttributesByWebsite($this->request['website_id']);
        $result['error'] = false;
        try{
            foreach ($attributes as $attribute)
            {
                $regionalAttribute = $this->regionalConditionAttributeFactory->create();
                $regionalAttribute->setData([
                    'reg_cond_request_id' => $regionalConditionId,
                    'validation_reg_condition_id' => null,
                    'key_input_name' => $attribute['postname'],
                    'value' => $this->request[$attribute['postname']]
                ]);
                $attr = $regionalAttribute->save();
                $result['saved'][] = $attr->getData();
            }
        }catch (\Exception $e){
            $result['error'] = true;
            $result['error_message'] = $e->getMessage();
            //ocurrio un error durante el guardado del Item
            return $result;
        }
        return $result;
    }

    private function persistRequest()
    {
        $conditionRequest = $this->regionalConditionRequestFactory->create();
        $conditionRequest->setData([
            'region_id' => $this->request['region'],
            'seller_id' => $this->request['seller_id'],
            'website_id' => $this->request['website_id'],
            'processed' => 0
        ]);
        return $conditionRequest->save();
    }

    public function getAttributesByWebsite($website)
    {
        return $this->helperdata->getAttributesByWebsite($website);
    }

}