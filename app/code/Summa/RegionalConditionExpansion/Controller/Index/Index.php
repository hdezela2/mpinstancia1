<?php
namespace Summa\RegionalConditionExpansion\Controller\Index;

use Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;
    protected $postFactory;

    /**
     * @var RegionalConditionRequestFactory
     */
    private $testFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        RegionalConditionRequestFactory $testFactory
    )
    {
        $this->pageFactory = $pageFactory;
        $this->testFactory = $testFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->testFactory->create();
        $collection = $post->getCollection();
        foreach($collection as $item){
            echo "<pre>";
            print_r($item->getData());
            print_r($item->getRegionalAttributes());

            echo "</pre>";
        }

        exit();
        return $this->pageFactory->create();
    }
}
