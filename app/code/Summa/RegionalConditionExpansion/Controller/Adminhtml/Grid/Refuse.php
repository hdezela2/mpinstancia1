<?php

/**
 * Webkul Grid List Controller.
 * @category  Webkul
 * @package   Webkul_Grid
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Summa\RegionalConditionExpansion\Controller\Adminhtml\Grid;

use Magento\Framework\Controller\ResultFactory;
use Summa\RegionalConditionExpansion\Logger\Logger;

class Refuse extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory
     */
    private $gridFactory;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Refuse constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory $gridFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Logger $logger,
        \Magento\Framework\Registry $coreRegistry,
        \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory $gridFactory
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->gridFactory = $gridFactory;
        $this->logger = $logger;
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $rowId = (int)$this->getRequest()->getParam('id');

        $rowData = $this->gridFactory->create();
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        if ($rowId) {
            $rowData = $rowData->load($rowId);
//            $rowTitle = $rowData->getTitle();
            if (!$rowData->getEntityId()) {
                $this->messageManager->addError(__('row data no longer exist.'));
                $this->_redirect('grid/grid/rowdata');
                return;
            }
        }

        $this->cancelRequest($rowData);
        $this->coreRegistry->register('row_data', $rowData);
//        return $this->_redirect('*/regionalconditionexpansion/grid/index');
        return $resultRedirect->setPath('regionalconditionexpansion/grid/index');
    }

    public function cancelRequest ($newRegionalConditionReq){

        $newRegionalConditionReq->setProcessed(1);
        $newRegionalConditionReq->save();
        $this->messageManager->addNotice(__('request cancelado por el JP'));

        $this->logger->info('Solicitud de expansion regional rechazada por el JP - Datos: '. json_encode($newRegionalConditionReq->getData()) .' Attributes from Convenio: '. json_encode($newRegionalConditionReq->getRegionalAttributes()) );
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Summa_RegionalConditionExpansion::Refuse');
    }
}
