<?php

/**
 * Grid Record Index Controller.
 * @category  Summa
 * @package   Summa_Grid
 * @author    Summa
 * @copyright Copyright (c) 2010-2017 Summa Software Private Limited (https://summa.com)
 * @license   https://store.summa.com/license.html
 */
namespace Summa\RegionalConditionExpansion\Controller\Adminhtml\Grid;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Mapped eBay Order List page.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Summa_RegionalConditionExpansion::grid_list');
        $resultPage->getConfig()->getTitle()->prepend(__('Regional condition expansion requests'));
        return $resultPage;
    }

    /**
     * Check Order Import Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Summa_RegionalConditionExpansion::grid_list');
    }
}