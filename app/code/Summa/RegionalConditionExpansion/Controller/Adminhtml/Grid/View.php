<?php

namespace Summa\RegionalConditionExpansion\Controller\Adminhtml\Grid;

use Magento\Framework\Controller\ResultFactory;

class View extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory
     */
    private $requesFactory;

    /**
     * View constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory $requesFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory $requesFactory
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->requesFactory = $requesFactory;
    }

    public function execute()
    {
        try
        {
            $rowId = (int) $this->getRequest()->getParam('id');
            $rowData = $this->requesFactory->create();
            if (!$this->coreRegistry->registry('current_request'))
            {
                $this->coreRegistry->register('current_request', $rowId);
            }

            /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
            if ($rowId)
            {
                $rowData = $rowData->load($rowId);
                if (!$rowData->getEntityId())
                {
                    $this->messageManager->addError(__('row data no longer exist.'));
                    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultRedirect->setPath('*/*/index');
                    return $resultRedirect;
                }
            }
            $this->coreRegistry->register('row_data', $rowData);
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $resultPage->getConfig()->getTitle()->prepend(__('View Row Data '));
            return $resultPage;

        }
        catch (\Magento\Framework\Exception\LocalizedException $e)
        {
            $this->messageManager->addError($e);
        }
        catch (\Exception $e)
        {
            $this->messageManager->addException($e, __('Unable to read system report data to display.'));
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('*/*/index');
        return $resultRedirect;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Summa_RegionalConditionExpansion::View');
    }
}