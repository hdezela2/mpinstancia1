<?php

namespace Summa\RegionalConditionExpansion\Controller\Adminhtml\Grid;

use Magento\Framework\Controller\ResultFactory;
use Summa\RegionalConditionExpansion\Logger\Logger;

class Approve extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory
     */
    private $gridFactory;

    /**
     * @var \Summa\RegionalConditionExpansion\Helper\Data
     */
    private $helper;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * Approve constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory $gridFactory
     * @param Logger $logger
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory $gridFactory,
        \Summa\RegionalConditionExpansion\Helper\Data $helper,
        Logger $logger
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->gridFactory = $gridFactory;
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * Mapped Grid List page.
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $rowId = (int) $this->getRequest()->getParam('id');
        $rowData = $this->gridFactory->create();
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        if ($rowId) {
            $rowData = $rowData->load($rowId);
            if (!$rowData->getEntityId()) {
                $this->messageManager->addError(__('row data no longer exist.'));
                $this->_redirect('grid/grid/rowdata');
                return;
            }
        }

        $response = $this->helper->addNewRegionalCondition($rowData);
        if ($response['error']){
            $this->messageManager->addError(__($response['msg']));
        }else{
            $this->messageManager->addNotice(__('The region was assigned to the seller'));
        }
        $this->coreRegistry->register('row_data', $rowData);
        $this->logger->info('Solicitud de expansion regional aprobada - Datos solicitud: ' . json_encode($response));
        return $resultRedirect->setPath('regionalconditionexpansion/grid/index');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Summa_RegionalConditionExpansion::Approve');
    }
}
