#Regional Condition Expansion Module

##Notas

El modulo nuevo se llama RegionalConditionExpansion
Tiene 2 dependencias:
- Webkul_Marketplace
- Formax_RegionalCondition

Hace uso del esquema declarativo y crea las tablas:
- regional_condition_expansion
- regional_condition_expansion_attributes

Tambien cuenta con un Logger definido en la inyeccion de dependencias
Cuenta con una definicion de ACL y se debe configurar el acceso desde el admin para cada website

Agrega elementos al menu del admin, del jp y del seller

Se agregaron las configuraciones:
- modulo habilitado
- Aprobacion JP requerida

 y sus valores por defecto estan apagados ambos
 
 
 El formulario se define en el theme por website
 Se desarrollo un block especifico para este formulario para aportar varias de las funcionalidades requeridas como mostrar solo las regiones no asignadas y no en solicitudes
 Se desarrollo controlador para validar y procesar el formulario y persistir la solicitud.
 
 Se desarrollo una grilla usando UI_Components
 La grilla permite filtrar las solicitudes, aprobarlos y recharzarlas asi como acceder a una vista detallada de cada solicitud con los controles adecuados.