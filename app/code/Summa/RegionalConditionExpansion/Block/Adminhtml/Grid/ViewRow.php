<?php

namespace Summa\RegionalConditionExpansion\Block\Adminhtml\Grid;

use \Magento\Backend\Block\Widget\Form\Container;
use \Magento\Backend\Block\Widget\Context;
use \Magento\Framework\Registry;

class ViewRow extends Container
{
    protected $coreRegistry;

    public function __construct(Context $context, array $data = [], Registry $registry)
    {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Summa_RegionalConditionExpansion';
        $this->_controller = 'adminhtml_grid';
        parent::_construct();
        if($this->shouldDisplayButton(0))
        {
            $this->buttonList->update('save', 'label', __('Approve'));
            $this->addButton(
                'refuse',
                [
                    'label' => __('Refuse'),
                    'class' => 'save secundary',
                    'onclick' => 'deleteConfirm(\'' . __(
                            'Are you sure you want to do this?'
                        ) . '\', \'' . $this->getRefuseUrl() . '\', {data: {}})'
                ],
                1
            );
        }
        else
        {
            $this->buttonList->remove('save');
            $this->buttonList->remove('refuse');
        }
        $this->buttonList->remove('reset');
        $this->buttonList->remove('delete');
    }

    protected function getRefuseUrl()
    {
        return $this->getUrl('*/*/refuse', [$this->_objectId => (int)$this->getRequest()->getParam($this->_objectId)]);
    }

    protected function shouldDisplayButton($state)
    {
        $model = $this->coreRegistry->registry('row_data');
        return $model->getProcessed() == $state && $this->_isAllowedAction('Summa_RegionalConditionExpasion::Approve');
    }

    public function getHeaderText()
    {
        return __('Add RoW Data');
    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getFormActionUrl()
    {
        if ($this->hasFormActionUrl()) {
            return $this->getData('form_action_url');
        }

        return $this->getUrl('*/*/approve');
    }
}