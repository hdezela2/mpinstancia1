<?php

namespace Summa\RegionalConditionExpansion\Block\Adminhtml\Grid\Edit;

use \Magento\Backend\Block\Widget\Form\Generic as GenericForm;
use Summa\RegionalConditionExpansion\ViewModel\RegionalDetails;
use Summa\RegionalConditionExpansion\Helper\Data;

class Form extends GenericForm
{
    protected $regionalDetails;
    protected $formValues;
    protected $helper;
    protected $attributeLabels;

    public function __construct
    (
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = [],
        RegionalDetails $regionalDetails,
        Data $helper
    )
    {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->pageConfig->getTitle()->set(__('Regional Condition Expansion'));
        $this->regionalDetails = $regionalDetails;
        $this->helper = $helper;
    }

    protected function _prepareForm()
    {
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        $model = $this->_coreRegistry->registry('row_data');
        $this->formValues = $this->regionalDetails->getRegionalConditionRequestById($model->getEntityId());
        $this->attributeLabels = $this->helper->getAttributesByWebsite($this->formValues['website']['value']);

        $form = $this->_formFactory->create(
            ['data' =>
                [
                    'id' => 'edit_form',
                    'enctype' => 'multipart/form-data',
                    'action' => $this->getData('action'),
                    'method' => 'post'
                ]
            ]
        );
        $form->setHtmlIdPrefix('srce_');
        if ($model->getEntityId())
        {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Edit Row Data'), 'class' => 'fieldset-wide']
            );
            $fieldset->addField('id', 'hidden',
                [
                    'name' => 'id',
                    'value' => $model->getEntityId()
                ]);
        }
        else
        {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Add Row Data'), 'class' => 'fieldset-wide']
            );
        }

        $fieldset->addField
        (
            'Region',
            'text',
            [
                'name' => 'region',
                'label' => __('Region'),
                'id' => 'region',
                'title' => __('Region'),
                'class' => 'required-entry disabled',
                'required' => true,
                'value' => $this->formValues['region']
            ]
        );

        $fieldset->addField
        (
            'Vendedor',
            'text',
            [
                'name' => 'seller',
                'label' => __('Vendedor'),
                'id' => 'seller',
                'title' => __('Seller'),
                'class' => 'required-entry disabled',
                'required' => true,
                'value' => $this->formValues['seller']['label']
            ]
        );

        $fieldset->addField
        (
            'Website',
            'text',
            [
                'name' => 'website',
                'label' => __('Website'),
                'id' => 'website',
                'title' => __('Website'),
                'class' => 'required-entry disabled',
                'required' => true,
                'value' => $this->formValues['website']['label']
            ]
        );

        $fieldset->addField
        (
            'Created_At',
            'date',
            [
                'name' => 'created_at',
                'label' => __('Solicitado en'),
                'date_format' => $dateFormat,
                'time_format' => 'H:mm:ss',
                'class' => 'required-entry disabled',
                'style' => 'width:200px',
                'value' => $this->formValues['created_at']
            ]
        );

        $fieldset->addField
        (
            'Processed',
            'text',
            [
                'name' => 'processed',
                'label' => __('Estado'),
                'id' => 'processed',
                'title' => __('Processed'),
                'class' => 'required-entry disabled',
                'required' => true,
                'value' => $this->formValues['processed']
            ]
        );

        foreach ($this->formValues['attributes'] as $key => $data)
        {
            $fieldset->addField
            (
                $key,
                'text',
                [
                    'name' => $key,
                    'label' => __($this->getAttributeLabel($key)),
                    'id' => $key,
                    'title' => __($key),
                    'class' => 'required-entry disabled',
                    'required' => true,
                    'value' => $data['value']
                ]
            );
        }

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

    private function getAttributeLabel($key)
    {
        $column = array_column($this->attributeLabels, 'postname');
        $key = array_search($key, $column);
        return $this->attributeLabels[$key]['label'];
    }

}
