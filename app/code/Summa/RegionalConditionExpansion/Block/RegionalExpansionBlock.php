<?php

namespace Summa\RegionalConditionExpansion\Block;

use \Magento\Framework\View\Element\Template;
use Summa\RegionalConditionExpansion\Helper\Data;
use \Magento\Store\Model\StoreManagerInterface as StoreManager;
use \Magento\Framework\UrlInterface;
use Formax\RegionalCondition\Block\RegionalCondition\Form as RegionalBlock;
use Formax\RegionalCondition\Block\RegionalCondition\Container as ContainerBlock;
use Summa\EmergenciasSetUp\Helper\Data as EmergenciasHelper;
use Summa\RegionalConditionExpansion\Model\RegionalConditionRequest;
use Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory;

class RegionalExpansionBlock extends Template
{
    private $helper;
    private $storeManager;
    private $urlBuilder;
    private $regionalBlock;
    private $containerBlock;
    protected $regionalConditionRequestFactory;
    private $regions;

    public function __construct(
        Template\Context $context, array $data = [],
        Data $helper,
        StoreManager $storeManager,
        UrlInterface $urlBuilder,
        RegionalBlock $regionalBlock,
        ContainerBlock $containerBlock,
        RegionalConditionRequestFactory $regionalConditionRequestFactory
    )
    {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->storeManager = $storeManager;
        $this->urlBuilder = $urlBuilder;
        $this->regionalBlock = $regionalBlock;
        $this->containerBlock = $containerBlock;
        $this->regionalConditionRequestFactory = $regionalConditionRequestFactory;
    }

    public function shouldRenderButton()
    {
        $websiteId = $this->storeManager->getStore()->getWebsiteId();
        return $this->helper->getGeneralConfig('enable', $websiteId) === "1";
    }

    public function getWebsiteId()
    {
        return $this->storeManager->getStore()->getWebsiteId();
    }

    public function getCurrentUrl()
    {
        return $this->urlBuilder->getCurrentUrl();
    }

    public function getRegionalConditionbySeller()
    {

        return $this->regionalBlock->getHelper()->getRegionalConditionbySeller();
    }

    public function getNonAssignedRegions()
    {
        $regions = $this->getRegions();
        $assignedRegions = $this->getAssignedRegions();
        $nonAssignedRegions = [];
        $regionalRequest = $this->getRegionalConditionRequest();
        foreach ($regions as $id)
        {
            if(!empty($id['value']))
            {
                if(!in_array($id['value'], $assignedRegions) && !in_array($id['value'], $regionalRequest))
                    $nonAssignedRegions[] = $id;
            }
        }
        return $nonAssignedRegions;
    }

    private function getRegionalConditionRequest($wkSellerId = null)
    {
        $result = [];
        $collection = $this->regionalConditionRequestFactory->create()->getCollection();
        $sellerId = $wkSellerId === null ? $this->getSellerId() : $wkSellerId;
        $collection->addFilter('seller_id', $sellerId, '=');
        $collection->addFilter('website_id', $this->getWebsiteId());
        $collection->addFilter('processed', 0);
        if($collection && is_array($collection->getData()))
        {
            foreach ($collection->getData() as $value)
            {
                $result[] =  $value['region_id'];
            }
        }
        return $result;
    }

    public function getAssignedRegions()
    {
        $assignedRegions = $this->regionalBlock->getHelper()->getRegionalConditionbySeller();
        return !empty($assignedRegions['region_id']) ? $assignedRegions['region_id'] : [];
    }

    public function getPostValue($key, $index = -1, $subIndex = -1)
    {
        return $this->regionalBlock->getHelper()->getPostValue($key, $index, $subIndex);
    }

    public function getRegions()
    {
        return $this->regionalBlock->getRegions();
    }

    public function getYesNo()
    {
        return $this->regionalBlock->getYesNo();
    }

    public function getField($data, $key, $index = -1, $subIndex = -1)
    {
        return $this->regionalBlock->getHelper()->getField($data, $key, $index, $subIndex);
    }

    public function getPostUrl()
    {
        return $this->getUrl('regionalconditionexpansion/seller/regionalformsave');
    }

    public function getSellerId()
    {
        return $this->containerBlock->getSellerId();
    }

    public function getDccpSellerId()
    {
        return $this->containerBlock->getDccpSellerId();
    }
}
