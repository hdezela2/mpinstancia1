<?php

namespace Summa\RegionalConditionExpansion\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;


interface RegCondRequestInterface extends ExtensibleDataInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return void
     */
    public function setId($id);

}