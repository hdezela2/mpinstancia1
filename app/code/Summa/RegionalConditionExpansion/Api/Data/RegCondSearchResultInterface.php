<?php

namespace Summa\RegionalConditionExpansion\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface RegCondSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Summa\RegionalConditionExpansion\Api\Data\RegCondRequestInterface[]
     */
    public function getItems();

    /**
     * @param \Summa\RegionalConditionExpansion\Api\Data\RegCondRequestInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}