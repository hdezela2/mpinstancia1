<?php

/**
 * Grid Ui Component Action.
 * @category  Webkul
 * @package   Webkul_Grid
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Summa\RegionalConditionExpansion\Ui\Component\Listing\Grid\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Attributes extends Column
{
    /**
     * @var \Summa\RegionalConditionExpansion\Model\ResourceModel\RegionalConditionAttribute\CollectionFactory
     */
    protected $attributeCollectionFactory;

    /**
     * @var \Summa\RegionalConditionExpansion\Helper\Data
     */
    Protected $helperdata;

    protected $levelsByWebsite;

    /**
     * Attributes constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Summa\RegionalConditionExpansion\Helper\Data $helperExpansion
     * @param \Summa\RegionalConditionExpansion\Model\ResourceModel\RegionalConditionAttribute\CollectionFactory $AttributeCollectionFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Summa\RegionalConditionExpansion\Helper\Data $helperExpansion,
        \Summa\RegionalConditionExpansion\Model\ResourceModel\RegionalConditionAttribute\CollectionFactory $AttributeCollectionFactory,
        array $components = [],
        array $data = []
    )
    {
        $this->attributeCollectionFactory = $AttributeCollectionFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->helperdata = $helperExpansion;
    }


    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items']))
        {
            foreach ($dataSource['data']['items'] as & $item)
            {
                $item['adittional_attributes'] = $this->getOthersAttributes($item);
            }
        }
        return $dataSource;
    }

    public function getOthersAttributes($item)
    {
        $entityId = $item['entity_id'];
        $websiteId =  $item['website_id'];
        $attributes = $this->attributeCollectionFactory->create()
            ->addRegionalConditionRequestFilter($entityId)
            ->addFieldToSelectAttributesInfo();
//            ->addFieldToSelect(['entity_id', 'order_id', 'status','transaction_id'])
//            ->addFieldToFilter('status', ['eq' => Grid::STATUS_SENT]);

        $attributesLevel = $this->getLevelsByWebsite($websiteId);

        $stringResult = '<ul>';
        foreach ($attributes as $attribute)
        {
            $stringResult = $stringResult . "<li>" . $attributesLevel[$attribute['key_input_name']]. ": ". $attribute['value']. "</li>";
        }
        $stringResult =$stringResult . '</ul>';
        //        $transaction = $transactionsCollection->getItemByColumnValue('order_id', floatval($incrementId));

        return $stringResult;
    }

    public function getLevelsByWebsite($websiteId)
    {
        if (!isset($this->levelsByWebsite[$websiteId]))
        {
            $attributesList = $this->helperdata->getAttributesByWebsite($websiteId);
            foreach ($attributesList as $attribuiteAux)
            {
                $this->levelsByWebsite[$websiteId][$attribuiteAux['postname']] = $attribuiteAux['label'];
            }
        }
        return $this->levelsByWebsite[$websiteId];
    }
}