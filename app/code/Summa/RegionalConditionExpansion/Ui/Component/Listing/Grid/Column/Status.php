<?php

namespace Summa\RegionalConditionExpansion\Ui\Component\Listing\Grid\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Summa\RegionalConditionExpansion\Model\RegionalConditionRequest;
use Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory;
use \Magento\Framework\Option\ArrayInterface;

class Status implements ArrayInterface
{
    protected $regionalConditionRequestFactory;

    public function __construct(RegionalConditionRequestFactory $factory)
    {
        $this->regionalConditionRequestFactory = $factory;
    }

    /*    public function prepareDataSource(array $dataSource)
        {
            return $dataSource;
        }*/

    public function toOptionArray()
    {
        $options = [];
        $options[] = ['value' => '0', 'label' => 'No Procesado'];
        $options[] = ['value' => '1', 'label' => 'Procesado'];
        return $options;
    }
}
