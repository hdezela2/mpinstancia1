<?php
/**
 * Grid Ui Component Action.
 * @category  Webkul
 * @package   Webkul_Grid
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Summa\RegionalConditionExpansion\Ui\Component\Listing\Grid\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class Action extends Column
{
    /** Url path */
    const ROW_VIEW_URL = 'regionalconditionexpansion/grid/view';
    const ROW_APPROVE_URL = 'regionalconditionexpansion/grid/approve';
    const ROW_REFUSE_URL = 'regionalconditionexpansion/grid/refuse';

    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * Action constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param UrlInterface $urlBuilder
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = array(),
        UrlInterface $urlBuilder,
        array $data = array())
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                if (isset($item['entity_id'])) {
                    $item[$name]['view'] = [
                        'href' => $this->urlBuilder->getUrl(
                            self::ROW_VIEW_URL,
                            ['id' => $item['entity_id']]
                        ),
                        'label' => __('View')
                    ];
                }
                if (isset($item['entity_id']) && $item['processed'] == 0) {
                    $item[$name]['approve'] = [
                        'href' => $this->urlBuilder->getUrl(
                            self::ROW_APPROVE_URL,
                            ['id' => $item['entity_id']]
                        ),
                        'label' => __('Approve')
                    ];
                    $item[$name]['refuse'] = [
                        'href' => $this->urlBuilder->getUrl(
                            self::ROW_REFUSE_URL,
                            ['id' => $item['entity_id']]
                        ),
                        'label' => __('Refuse'),
                        'confirm' => [
                            'title'   => __('Refuse this Regional Condition Request'),
                            'message' => __('Are you sure you want to refuse this coverage extension request?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}

