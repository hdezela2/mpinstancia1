<?php

namespace Summa\RegionalConditionExpansion\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    /**
     * @var int
     */
    const CUSTOM_ERROR_CODE = 555;

    const XML_PATH_REGIONAL_CONDITION_EXPANSION = "regional_condition_expansion/";

    /**
     * @var \Formax\RegionalCondition\Model\ValidationRegionalConditionFactory
     */
    protected $validationRegionalCondition;

    /**
     * @var \Formax\RegionalCondition\Model\RegionalConditionSellerFactory
     */
    protected $regionalConditionSellerFactory;

    /**
     * @var \Formax\RegionalCondition\Model\RegionalConditionFactory
     */
    protected $regionalConditionFactory;

    /**
     * @var \Formax\Storepickup\Model\StorepickupFactory
     */
    protected $storepickupFactory;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Formax\RegionalCondition\Model\ResourceModel\ValidationRegionalCondition\CollectionFactory
     */
    protected $validationConllectionFactory;

    protected $logger;

    /**
     * Data constructor.
     */
    public function __construct(
        \Formax\RegionalCondition\Model\RegionalConditionSellerFactory $regionalConditionSellerFactory,
        \Summa\RegionalConditionExpansion\Logger\Logger $logger,
        \Formax\RegionalCondition\Model\RegionalConditionFactory $regionalConditionFactory,
        \Formax\Storepickup\Model\StorepickupFactory $storepickupFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Formax\RegionalCondition\Model\ValidationRegionalConditionFactory $validationRegionalCondition,
        \Formax\RegionalCondition\Model\ResourceModel\ValidationRegionalCondition\CollectionFactory $validationConllectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->validationRegionalCondition = $validationRegionalCondition;
        $this->regionalConditionSellerFactory = $regionalConditionSellerFactory;
        $this->regionalConditionFactory = $regionalConditionFactory;
        $this->storepickupFactory = $storepickupFactory;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->scopeConfig = $scopeConfig;
        $this->validationConllectionFactory = $validationConllectionFactory;
        $this->logger = $logger;
    }

    public function getConfigValue($field, $websiteId = null)
    {
        return $this->scopeConfig->getValue($field, ScopeInterface::SCOPE_WEBSITE, $websiteId);
    }

    public function getGeneralConfig($code, $websiteId = null)
    {
//        $value = $this->getConfigValue(self::XML_PATH_REGIONAL_CONDITION_EXPANSION .'general/'. $code, $websiteId);
        return $this->getConfigValue(self::XML_PATH_REGIONAL_CONDITION_EXPANSION .'general/'. $code, $websiteId);
    }

    /**
     * @param \Summa\RegionalConditionExpansion\Model\RegionalConditionRequest  $newRegionalConditionReq
     * @return array
     * @throws \Exception
     */
    public function addNewRegionalCondition($newRegionalConditionReq)
    {
        $result = ['error' => false, 'msg' => ''];
        $valRegion = $this->validationRegionalCondition->create()->validateRegionExist(
            $newRegionalConditionReq->getSellerId(),
            $newRegionalConditionReq->getWebsiteId(),
            [$newRegionalConditionReq->getRegionId()]
        );

        if (!$valRegion)
        {
            $newRegionalCondition = $this->regionalConditionFactory->create();

            //obtengo los atributos de la region
            $regionalAttributes  = $newRegionalConditionReq->getRegionalAttributes();

            $validation = $this->validateRegionalConditionAttributes($newRegionalConditionReq, $regionalAttributes );
            $this->setDefaultValues($newRegionalConditionReq, $newRegionalCondition);

            if (!$validation['error']) {
                foreach ($regionalAttributes as $key => $value) {
                    $newRegionalCondition->setData($key, $value['value']);
                    //si viene por parametro, se pisa el valor por defecto.
                }
                $newRegionalCondition->save();
                $newRegionalConditionReq->setProcessed(1);
                $newRegionalConditionReq->save();
                $regionalConditionId = (int)$newRegionalCondition->getId();
                if ($regionalConditionId <= 0) {
                    $result['error'] = true;
                    $result['msg'] = 'An error occured saving the information.';

                    $message = __('An error occured saving the information.');
                    throw new \Exception($message, self::CUSTOM_ERROR_CODE);
                }
            }
            else {
                $message = $validation['msg'];
                $result = $validation;
                throw new \Exception($message, self::CUSTOM_ERROR_CODE);
            }

            //            Assing to seller?
            $regionalConditionSeller = $this->regionalConditionSellerFactory->create()->load($regionalConditionId, 'regional_condition_id');
            $regionalConditionSeller->addData([
                'regional_condition_id' => $regionalConditionId,
                'seller_id'             => $newRegionalConditionReq->getSellerId(),
                'dccp_seller_id'        => $this->getDccpSellerId($newRegionalConditionReq->getSellerId()),
                'website_id'            => $newRegionalConditionReq->getWebsiteId()
            ])->save();

            //No se hace load ya que se esta creando y no debe existir
            /*$storepickup = $this->storepickupFactory->create()->loadUniqueRow(
                $newRegionalConditionReq->getRegionId(),
                $newRegionalConditionReq->getSellerId(),
                $newRegionalConditionReq->getWebsiteId()
            );*/

            $valueEnableStorepickup = 0;
            if (isset($regionalAttributes['enable_storepickup'])) {
                $valueEnableStorepickup = (int)$regionalAttributes['enable_storepickup'];
            }
            $storepickup = $this->storepickupFactory->create();

            $storepickup->addData([
                'dest_region_id' => $newRegionalConditionReq->getRegionId(),
                'seller_id' => $newRegionalConditionReq->getSellerId(),
                'website_id' => $newRegionalConditionReq->getWebsiteId(),
                'active' => $valueEnableStorepickup
            ])->save();
        }
        else
        {
            $result['error'] = true;
            $result['msg'] = 'La region ya está asignada al Seller';
        }

        return $result;

    }

    public function validateRegionalConditionAttributes($newRegionalConditionReq, $regianlAttributes){
        $location = 'pedido de ampliasion de covertura';
//        if (isset($this->regionArray[$post['region_id'][$i]])) {
//            $location = __('(Location - Row: %1).', $this->regionArray[$newRegionalConditionReq->getRegionId()]->getDefaultName());
//        }
        foreach ($regianlAttributes as $key => $value) {
            $originData = null;
            $newValue = trim($value['value']);
//            $newValue = in_array(strtolower($newValue), $this->unlimitedText) ? \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE : $newValue;
            $validation = $this->validationRegionalCondition->create()
                ->validateData($key, $newValue, $originData, $location);
            if ($validation['error']) {
                return $validation;
            }
        }
    }

    /**
     * Retrive DCCP Seller Id logged In
     * @return string
     */
    public function getDccpSellerId($customerId)
    {
        $customerRepository = $this->customerRepositoryInterface->getById($customerId);
        return $customerRepository->getCustomAttribute('user_rest_id') ?
            $customerRepository->getCustomAttribute('user_rest_id')->getValue() : false;
    }


    public function getAttributesByWebsite($websiteId)
    {
        $listValidations = $this->validationConllectionFactory->create()
            ->addWebsiteFilter($websiteId)
            ->addFieldToSelect(['postname', 'label']);

        $arrayKeys = [];
        foreach ($listValidations as $elem)
        {
            $arrayKeys[] =
                [
                    'postname' => $elem->getPostname(),
                    'label' => $elem->getLabel()
                ];
        }
        return  $arrayKeys;
    }

    public function setDefaultValues($newRegionalConditionReq, $newRegionalCondition)
    {
        $newRegionalCondition->setRegionId($newRegionalConditionReq->getRegionId());
    }
}