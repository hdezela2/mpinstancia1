<?php

namespace Summa\RegionalConditionExpansion\Model;

use Magento\Framework\Model\AbstractModel;
use \Summa\RegionalConditionExpansion\Api\Data\RegCondRequestExtensionInterface;
use \Summa\RegionalConditionExpansion\Api\Data\RegCondRequestInterface as RegCondRequestInterface;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;

class RegionalConditionRequest extends AbstractModel implements RegCondRequestInterface
{
    const CACHE_TAG = 'summa_chilecompra_regionalcondition_request';

    protected $_cacheTag = 'summa_chilecompra_regionalcondition_request';

    protected $_eventPrefix = 'summa_chilecompra_regionalcondition_request';

    protected $regionalAttributes = [];

    protected $customDataFactory;

    /**
     * RegionalConditionRequest constructor.
     * @param RegionalConditionAttributeFactory $customDataFactory
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Summa\RegionalConditionExpansion\Model\RegionalConditionAttributeFactory $customDataFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->customDataFactory = $customDataFactory;
        parent::__construct($context, $registry, $resource,  $resourceCollection, $data);
    }

    protected function _construct()
    {
        $this->_init('Summa\RegionalConditionExpansion\Model\ResourceModel\RegionalConditionRequest');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    public function getRegionalAttributes()
    {
        if (empty($this->regionalAttributes)){
            $this->loadRegionalAttributes();
        }
        return $this->regionalAttributes;
    }

    protected function loadRegionalAttributes(){
        $aux = $this->customDataFactory->create();
        $collection = $aux->getCollection()->addRegionalConditionRequestFilter($this->getEntityId())->addFieldToSelectAttributesInfo();
        foreach($collection as $item){
            $this->regionalAttributes[$item['key_input_name']]=['value' => $item['value'], 'validation' => $item['validation_reg_condition_id']];
        }
    }
}
