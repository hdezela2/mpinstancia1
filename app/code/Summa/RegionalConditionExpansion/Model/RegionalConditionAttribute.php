<?php

namespace Summa\RegionalConditionExpansion\Model;

class RegionalConditionAttribute extends \Magento\Framework\Model\AbstractModel
{
    const CACHE_TAG = 'summa_chilecompra_regionalcondition_attribute';

    protected $_cacheTag = 'summa_chilecompra_regionalcondition_attribute';

    protected $_eventPrefix = 'summa_chilecompra_regionalcondition_attribute';

    protected function _construct()
    {
        $this->_init('Summa\RegionalConditionExpansion\Model\ResourceModel\RegionalConditionAttribute');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}