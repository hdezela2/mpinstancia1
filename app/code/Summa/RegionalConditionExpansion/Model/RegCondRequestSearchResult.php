<?php

namespace Summa\RegionalConditionExpansion\Model;

use Magento\Framework\Api\SearchResults;
use Summa\RegionalConditionExpansion\Api\Data\RegCondSearchResultInterface;

class RegCondRequestSearchResult extends SearchResults implements RegCondSearchResultInterface
{

}