<?php
namespace Summa\RegionalConditionExpansion\Model\ResourceModel\RegionalConditionRequest;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'summa_chilecompra_regionalcondition_request_collection';
    protected $_eventObject = 'regionalcondition_request_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Summa\RegionalConditionExpansion\Model\RegionalConditionRequest', 'Summa\RegionalConditionExpansion\Model\ResourceModel\RegionalConditionRequest');
    }


    /**
     * Filter by website_id
     *
     * @param int|array $websiteId
     * @return $this
     */
    public function addWebsiteFilter($websiteId)
    {
        if (!empty($websiteId)) {
            if (is_array($websiteId)) {
                $this->addFieldToFilter('main_table.website_id', ['in' => $websiteId]);
            } else {
                $this->addFieldToFilter('main_table.website_id', $websiteId);
            }
        }

        return $this;
    }

    /**
     * Filter by region_id
     *
     * @param int|array $regionId
     * @return $this
     */
    public function addRegionFilter($regionId)
    {
        if (!empty($regionId)) {
            if (is_array($regionId)) {
                $this->addFieldToFilter('main_table.region_id', ['in' => $regionId]);
            } else {
                $this->addFieldToFilter('main_table.region_id', $regionId);
            }
        }

        return $this;
    }

    /**
     * Filter by seller_id
     *
     * @param int|array $sellerId
     * @return $this
     */
    public function addSellerFilter($sellerId)
    {
        if (!empty($sellerId)) {
            if (is_array($sellerId)) {
                $this->addFieldToFilter('main_table.seller_id', ['in' => $sellerId]);
            } else {
                $this->addFieldToFilter('main_table.seller_id', $sellerId);
            }
        }

        return $this;
    }
}