<?php
namespace Summa\RegionalConditionExpansion\Model\ResourceModel;


class RegionalConditionAttribute extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('regional_condition_expansion_attributes', 'entity_id');
    }

}