<?php
namespace Summa\RegionalConditionExpansion\Model\ResourceModel\RegionalConditionAttribute;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'summa_chilecompra_regionalcondition_attribute_collection';
    protected $_eventObject = 'regionalcondition_attribute_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Summa\RegionalConditionExpansion\Model\RegionalConditionAttribute', 'Summa\RegionalConditionExpansion\Model\ResourceModel\RegionalConditionAttribute');
    }


    /**
     * Filter by regionalConditionRequestId
     *
     * @param int|array $regionalConditionRequestId
     * @return $this
     */
    public function addRegionalConditionRequestFilter($regionalConditionRequestId)
    {
        if (!empty($regionalConditionRequestId)) {
            if (is_array($regionalConditionRequestId)) {
                $this->addFieldToFilter('main_table.reg_cond_request_id', ['in' => $regionalConditionRequestId]);
            } else {
                $this->addFieldToFilter('main_table.reg_cond_request_id', $regionalConditionRequestId);
            }
        }

        return $this;
    }

    public function addFieldToSelectAttributesInfo()
    {
        $this->addFieldToSelect(['key_input_name', 'value', 'validation_reg_condition_id']);
        return $this;
    }

}