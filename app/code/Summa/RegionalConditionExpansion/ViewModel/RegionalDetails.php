<?php

namespace Summa\RegionalConditionExpansion\ViewModel;

use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Sales\Api\Data\OrderAddressInterfaceFactory;
use Formax\RegionalCondition\Model\Source\Sellers;
use Formax\RegionalCondition\Model\Source\Regions;
use Magento\Customer\Model\Customer\Attribute\Source\Website;

/**
 * View model for batch details page.
 *
 * @package Temando\Shipping\ViewModel
 * @author  Rhodri Davies <rhodri.davies@temando.com>
 * @license https://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link    https://www.temando.com/
 */
class RegionalDetails implements ArgumentInterface
{
    protected $regions;
    protected $sellers;
    protected $displayableData;
    protected $websites;

    /**
     * @var \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory
     */
    private $requestFactory;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory $requesFactory
     * @param Regions $regions
     * @param Sellers $sellers
     * @param Website $website
     * @param ManagerInterface $messageManager
     */
    public function __construct
    (
        \Magento\Backend\App\Action\Context $context,
        \Summa\RegionalConditionExpansion\Model\RegionalConditionRequestFactory $requesFactory,
        Regions $regions,
        Sellers $sellers,
        Website $website,
        ManagerInterface $messageManager
    ) {
        $this->requestFactory = $requesFactory;
        $this->regions = $regions->toOptionArray();
        $this->sellers = $sellers->toOptionArray();
        $this->websites = $website->getAllOptions();
        $this->messageManager = $messageManager;
    }

    public function getRegionalConditionRequestById($requestId)
    {
        if ($requestId)
        {
            $rowData = $this->requestFactory->create();
            $rowData = $rowData->load($requestId);
            if (!$rowData->getEntityId())
            {
                $this->messageManager->addError(__('row data no longer exist.'));
                return null;
            }
            return $this->getDisplayableData($rowData);
        }
        return null;
    }

    protected function getDisplayableData($row)
    {
        $this->displayableData['entity_id'] = $row->getEntityId();
        $this->displayableData['region'] = $this->getDisplayableValue($this->regions, $row->getRegionId(), 'value', 'title');
        $this->displayableData['seller'] = $this->getDisplayableValue($this->sellers, $row->getSellerId(), 'value');
        $this->displayableData['website'] = $this->getDisplayableValue($this->websites, $row->getWebsiteId(), 'value');
        $this->displayableData['created_at'] = $row->getCreatedAt();
        $this->displayableData['processed'] = $row->getProcessed() == 1 ? "Procesado" : "No procesado";
        $this->displayableData['attributes'] = $row->getRegionalAttributes();
        return $this->displayableData;
    }

    private function getDisplayableValue($array, $id, $columnName, $returnKey=null)
    {
        $column = array_column($array, $columnName);
        $key = array_search($id, $column);
        return $result = empty($returnKey) ? $array[$key] : $array[$key][$returnKey];
    }
}
