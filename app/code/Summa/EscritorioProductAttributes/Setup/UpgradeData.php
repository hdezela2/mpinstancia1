<?php
namespace Summa\EscritorioProductAttributes\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
    protected $eavSetupFactory;
    protected $_attributeFactory;
    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
        \Magento\Eav\Model\Config $eavConfig
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->_attributeFactory = $attributeFactory;
        $this->eavConfig = $eavConfig;

    }

    /**
     * Returns true if attribute exists and false if it doesn't exist
     *
     * @param string $field
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isProductAttributeExists($field)
    {
        $attr = $this->eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $field);

        return ($attr && $attr->getId()) ? true : false;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '2.0.1', '<')) {
            $setup->startSetup();
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            if(!$this->isProductAttributeExists('macrozona')) {
                $values = ['values' => [

                    "Macrozona Austral",
                    "Macrozona Centro",
                    "Macrozona Metropolitana",
                    "Macrozona Norte",
                    "Macrozona Sur"
                ]];


                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'macrozona',
                    [
                        'type' => 'int',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Macrozona',
                        'input' => 'select',
                        'note' => '',
                        'class' => '',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => true, //esto permite que no se agregue a todos los attributeset
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to' => '',
                        'option' => $values
                    ]
                );
            }


            //-------------- ADD attribute to set and group
            $entity_type_id = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $attribute_set_name = 'Default Escritorio';
            $group_name = 'Product Details';

            $attribute_set_id = $eavSetup->getAttributeSetId($entity_type_id, $attribute_set_name);
            $attribute_group_id = $eavSetup->getAttributeGroupId($entity_type_id, $attribute_set_id, $group_name);

            $attributeCodeArray =
                [
                    'max_base_price',
                    'macrozona',
                    'marca'
                ];

            foreach ($attributeCodeArray as $attribute_code) {
                $attribute_id = $eavSetup->getAttributeId($entity_type_id, $attribute_code);

                $eavSetup->addAttributeToSet($entity_type_id, $attribute_set_id, $attribute_group_id, $attribute_id);
                $eavSetup->addAttributeToGroup(
                    $entity_type_id,
                    $attribute_set_id,
                    $attribute_group_id,
                    $attribute_id,
                    20
                );
            };

            $setup->endSetup();
        }

        if (version_compare($context->getVersion(), '2.0.2', '<'))

        {
            $marcas = [
                "AUCA",
                "CHAMEX",
                "DINON",
                "DURACELL",
                "EQUALIT",
                "ISOFIT",
                "REM"
            ];
            $attributeInfo=$this->_attributeFactory->getCollection()
                ->addFieldToFilter('attribute_code',['eq'=>"marca"])
                ->getFirstItem();
            $attribute_id = $attributeInfo->getAttributeId();

            $option=array();
            $option['attribute_id'] = $attributeInfo->getAttributeId();
            foreach($marcas as $key=>$value){
                $option['value'][$value][0]=$value;
            }

            $eavSetup = $this->eavSetupFactory->create();
            $eavSetup->addAttributeOption($option);
        }

    }
}



