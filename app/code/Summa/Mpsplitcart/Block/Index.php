<?php

namespace Summa\Mpsplitcart\Block;

/**
 * Mpsplitcart Block
 */
class Index extends \Webkul\Mpsplitcart\Block\Index
{
    /**
     * @var \Webkul\Mpsplitcart\Helper\Data
     */
    private $helper;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cartModel;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    private $priceHelper;

    /**
     * @var \Magento\Multishipping\Helper\Data
     */
    private $multishippingHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Formax\GreatBuy\Controller\Validate\Index
     */
    protected $formaxValidate;

    const XML_PATH_CHECKOUT_MULTIPLE_MINIMUM_UTM = 'dccp_endpoint/greatbuy/multicheckoutminimumutm';

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Formax\GreatBuy\Controller\Validate\Index $formaxValidateUtm,
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Mpsplitcart\Helper\Data $helper,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Multishipping\Helper\Data $multishippingHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $helper,
            $cart,
            $priceHelper,
            $multishippingHelper
        );
        $this->helper = $helper;
        $this->cartModel = $cart;
        $this->multishippingHelper = $multishippingHelper;
        $this->priceHelper = $priceHelper;
        $this->scopeConfig = $scopeConfig;
        $this->formaxValidate = $formaxValidateUtm;
    }

    /**
     * afterGetSectionData
     * updates the result from checkout session
     */
    public function multicheckoutStatusMimumUtm($cartTotal) {
        $result = $this->multicheckoutStatus();
        if ($result && $this->getConfigMultishippinCheckoutMinimumUtm()){
            $minimumValue = $this->multishippinCheckoutMinimum();
            $result = (float)$cartTotal > (float)$minimumValue;
        }
        return $result;
    }

    public function multishippinCheckoutMinimum(){
        return $this->formaxValidate->getCLPValue() * $this->getConfigMultishippinCheckoutMinimumUtm();
    }

    public function getConfigMultishippinCheckoutMinimumUtm(){
        $configValue = $this->scopeConfig->getValue(
            self::XML_PATH_CHECKOUT_MULTIPLE_MINIMUM_UTM,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return $configValue;
    }
}
