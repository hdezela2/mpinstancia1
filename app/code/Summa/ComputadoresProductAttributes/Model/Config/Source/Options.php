<?php

namespace Summa\ComputadoresProductAttributes\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\DB\Ddl\Table;

class Options extends AbstractSource
{
    public function getAllOptions()
    {
        $this->_options =
        [
            [ 'label' => 'CON SISTEMA OPERATIVO', 'value' => '1' ],
            [ 'label' => 'SIN SISTEMA OPERATIVO', 'value' => '0']
        ];
        return $this->_options;
    }

    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option)
        {
            if ($option['value'] == $value)
            {
                return $option['label'];
            }
        }
        return false;
    }
}