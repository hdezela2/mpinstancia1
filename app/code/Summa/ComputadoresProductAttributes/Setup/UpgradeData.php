<?php

namespace Summa\ComputadoresProductAttributes\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
    protected $eavSetupFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $_eavConfig;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Config $eavConfig
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;

        $this->_eavConfig = $eavConfig;
    }

    /**
     * Returns true if attribute exists and false if it doesn't exist
     *
     * @param string $field
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isProductAttributeExists($field)
    {
        $attr = $this->_eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $field);

        return ($attr && $attr->getId()) ? true : false;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context){
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $setup->startSetup();
            $eav_setup = $this->eavSetupFactory->create(['setup' => $setup]);

            //-------------- REMOVE OLD ATTRIBUTES
            if($this->isProductAttributeExists('os')) {
                $eav_setup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'os');
            }
            if($this->isProductAttributeExists('gamma')) {
                $eav_setup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'gamma');
            }
            if($this->isProductAttributeExists( 'brand')) {
                $eav_setup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'brand');
            }
            if($this->isProductAttributeExists( 'model')) {
                $eav_setup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'model');
            }

            //-------------- CREATE NEW ATTRIBUTES
            $eav_setup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'model',
                [
                    'type'          => 'text',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'Modelo',
                    'input'         => 'text',
                    'class'         => '',
                    'source'        => '',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => true,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eav_setup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'brand',
                [
                    'type'          => 'text',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'Marca',
                    'input'         => 'text',
                    'class'         => '',
                    'source'        => '',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => true,
                    'filterable'    => false,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique'        => false,
                    'apply_to'      => '',
                ]
            );
            $eav_setup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'gamma',
                [
                    'type'          => 'int',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'Gama',
                    'input'         => 'select',
                    'note'          => 'Gama de producto',
                    'class'         => '',
                    'source'        => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '',
                    'searchable'    => true,
                    'filterable'    => true,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique'        => false,
                    'apply_to'      => '',
                    'option' => [
                        'values' => [],
                    ],
                ]
            );
            $eav_setup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'with_os',
                [
                    'type'          => 'int',
                    'backend'       => '',
                    'frontend'      => '',
                    'label'         => 'Sistema Operativo',
                    'input'         => 'select',
                    'note'          => 'Sistema Operativo',
                    'class'         => '',
                    'source'        => 'Summa\ComputadoresProductAttributes\Model\Config\Source\Options',
                    'global'        => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'visible'       => true,
                    'required'      => false,
                    'user_defined'  => true,
                    'default'       => '1',
                    'searchable'    => true,
                    'filterable'    => true,
                    'comparable'    => true,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique'        => false,
                    'apply_to'      => '',
                    'option' => [
                        'values' => [],
                    ],
                ]
            );
            //-------------- ADD attribute to set and group
            $entity_type_id = $eav_setup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $attribute_set_name = 'Default Computadores';
            $group_name = 'Product Details';


            $attribute_set_id=$eav_setup->getAttributeSetId($entity_type_id, $attribute_set_name);
            $attribute_group_id=$eav_setup->getAttributeGroupId($entity_type_id, $attribute_set_id, $group_name);

            $attributeCodeArray = ['model', 'gamma', 'brand', 'with_os'];

            foreach ($attributeCodeArray as $attribute_code){
                $attribute_id=$eav_setup->getAttributeId($entity_type_id, $attribute_code);

                $eav_setup->addAttributeToSet($entity_type_id,$attribute_set_id, $attribute_group_id, $attribute_id);
                $eav_setup->addAttributeToGroup(
                    $entity_type_id,
                    $attribute_set_id,
                    $attribute_group_id,
                    $attribute_id,
                    20
                );
            };

            $setup->endSetup();

        }
    }
}