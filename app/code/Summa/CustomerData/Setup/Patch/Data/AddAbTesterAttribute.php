<?php

namespace Summa\CustomerData\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;

class AddAbTesterAttribute implements  DataPatchInterface
{
    private $moduleDataSetup;
    private $eavSetupFactory;
    private $eavConfig;

    public function __construct(ModuleDataSetupInterface $moduleDataSetup, EavSetupFactory $eavSetupFactory, Config $eavConfig)
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'abtester',
            [
                'type'          => 'int',
                'label'         => 'A/B Tester',
                'input'         => 'select',
                'required'      => false,
                'visible'       => true,
                'user_defined'  => false,
                'position'      => 452,
                'source'        => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'system'        => 0,
                'default'       => 0,
            ]
        );
        $abtester = $this->eavConfig->getAttribute(Customer::ENTITY, 'abtester');
        $abtester->setData('used_in_forms', ['adminhtml_customer']);
        $abtester->save();
        return $this;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }
}
