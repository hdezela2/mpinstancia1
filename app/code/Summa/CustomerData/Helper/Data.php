<?php

namespace Summa\CustomerData\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Summa\CustomerData\Logger\Logger;

class Data extends AbstractHelper
{
    const XML_PATH_STOCK_MANAGEMENT = 'customer_data/';
    protected $allowedIPs;
    protected $customerRepository;
    protected $_storeManager;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var false|string[]
     */
    private $allowedIds;

    public function __construct(
        Context $context,
        CustomerRepositoryInterface $customerRepository,
        StoreManagerInterface $storeManager,
        Logger $logger)
    {
        $this->customerRepository = $customerRepository;
        $this->_storeManager = $storeManager;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    public function getAttributeValue($customerId)
    {
        $customer = $this->customerRepository->getById($customerId);
        if($customer)
            $value = $customer->getCustomAttribute("abtester") != null ? $customer->getCustomAttribute("abtester")->getValue() : 0;
        else
            $value = 0;

        return $value;
    }

    public function shouldRenderData($customerIp, $customerId, $websiteId = null)
    {
        $isAbTester = false;

        $this->allowedIPs = explode(',', $this->getGeneralConfig('ip', $websiteId));
        $this->allowedIds = explode(',', $this->getGeneralConfig('customerids', $websiteId));

        $customer = $this->customerRepository->getById($customerId);
        $userRestId = $customer->getCustomAttribute("user_rest_id") != null ? $customer->getCustomAttribute("user_rest_id")->getValue() : "";

        $this->_logger->info("Customer Id: " . $customerId . " -> User Rest Id: " . $userRestId);

        $isAbTester = (!empty($this->allowedIds) && in_array($userRestId, $this->allowedIds)) ||
                      (!empty($this->allowedIPs) && in_array($customerIp, $this->allowedIPs));

        $this->_logger->info("Customer is A/B Tester: " . ($isAbTester ? "Yes": "No"));

        return $isAbTester;
    }

    public function getConfigValue($field, $websiteId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_WEBSITE, $websiteId
        );
    }

    public function getGeneralConfig($code, $websiteId = null)
    {
        //$value = $this->getConfigValue(self::XML_PATH_STOCK_MANAGEMENT .'general/'. $code, $websiteId);
        return $this->getConfigValue(self::XML_PATH_STOCK_MANAGEMENT .'general/'. $code, $websiteId);
    }

    public function isModuleEnable($website=null)
    {
        $websiteId = $website == null ? $this->_storeManager->getStore()->getWebsiteId() : $website;
        return $this->getGeneralConfig('enable', $websiteId) == 1;
    }

}
