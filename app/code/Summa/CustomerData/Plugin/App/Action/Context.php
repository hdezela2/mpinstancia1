<?php

namespace Summa\CustomerData\Plugin\App\Action;

use Summa\CustomerData\Model\Customer\Context as CustomerSessionContext;
use \Magento\Customer\Model\Session;
use \Magento\Framework\App\Http\Context as HttpContext;
use \Magento\Framework\App\ActionInterface;
use \Magento\Framework\App\RequestInterface;

class Context
{
    protected $customerSession;
    protected $httpContext;

    public function __construct(Session $customerSession, HttpContext $httpContext)
    {
        $this->customerSession = $customerSession;
        $this->httpContext = $httpContext;
    }

    public function aroundDispatch(ActionInterface $subject, \Closure $proceed, RequestInterface $request)
    {
        $customerId = $this->customerSession->getCustomerId();
        if(!$customerId)
        {
            $customerId = 0;
        }
        $this->httpContext->setValue(CustomerSessionContext::CONTEXT_CUSTOMER_ID, $customerId, false);
        return $proceed($request);
    }
}