<?php

namespace Summa\CustomerData\Plugin\Customer\Model;

use Magento\Customer\Model\Context as CustomerContext;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Http\Context;

class Session
{
    private $httpContext;

    public function __construct(Context $context)
    {
        $this->httpContext = $context;
    }

    public function afterIsLoggedIn(CustomerSession $subject, $isLoggedIn)
    {
        return $isLoggedIn ?: $this->httpContext->getValue(CustomerContext::CONTEXT_AUTH);
    }
}