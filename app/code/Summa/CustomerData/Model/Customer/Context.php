<?php

namespace Summa\CustomerData\Model\Customer;

class Context
{
    const CONTEXT_CUSTOMER_ID = 'customer_id';
}