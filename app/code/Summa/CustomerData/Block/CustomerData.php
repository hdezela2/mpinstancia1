<?php

namespace Summa\CustomerData\Block;

use \Magento\Framework\App\ResourceConnection;
use \Magento\Framework\View\Element\Template;
use \Magento\Customer\Model\Session;
use \Magento\Framework\App\Http\Context as HttpContext;
use \Magento\Framework\App\Helper\Context;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use Summa\CustomerData\Helper\Data;

class CustomerData extends Template
{
    protected $customerSession;
    protected $httpContext;
    protected $resource;
    protected $helper;
    protected $customerRepository;

    public function __construct(
        Template\Context $context, 
        array $data = [], 
        Session $customerSession, 
        ResourceConnection $resource, 
        HttpContext $httpContext, 
        CustomerRepositoryInterface $customerRepository, 
        Data $helper)
    {
        $this->customerSession = $customerSession;
        $this->resource = $resource;
        $this->httpContext = $httpContext;
        $this->helper = $helper;
        $this->customerRepository = $customerRepository;
        parent::__construct($context, $data);
    }

    public function shouldDisplayData()
    {
        if(!$this->isModuleEnable())
            return true;

        $customerId = $this->httpContext->getValue(\Summa\CustomerData\Model\Customer\Context::CONTEXT_CUSTOMER_ID);
        $abtester = !$customerId ? 0 : $this->helper->getAttributeValue($customerId);
        return $abtester == 1;
    }

    public function isModuleEnable($website=null)
    {
        $websiteId = $website == null ? $this->_storeManager->getStore()->getWebsiteId() : $website;
        return $this->helper->getGeneralConfig('enable', $websiteId) == 1;
    }

    public function getSellerAlias($sellerId){
        $sellerAlias = null;

        if($this->isModuleEnable()){
            $customer = $this->customerRepository->getById($sellerId);
            $sellerAlias = $customer->getCustomAttribute("seller_alias") != null ? $customer->getCustomAttribute("seller_alias")->getValue() : null;
        }

        return $sellerAlias;
    }
}
