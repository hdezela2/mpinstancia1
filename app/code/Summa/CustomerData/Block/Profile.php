<?php

namespace Summa\CustomerData\Block;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Catalog\Block\Product\AbstractProduct;
use Webkul\Marketplace\Helper\Data as MpHelper;
use Webkul\Marketplace\Model\FeedbackFactory;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory;
use Webkul\Marketplace\Model\ProductFactory as MpProductModel;
use Magento\Catalog\Model\ProductFactory;
use Summa\CustomerData\Block\CustomerData;

class Profile extends \Webkul\Marketplace\Block\Profile
{
    protected $customerBlock;
     /**
     * @param Context                                   $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Framework\Url\Helper\Data        $urlHelper
     * @param Customer                                  $customer
     * @param \Magento\Framework\Stdlib\StringUtils     $stringUtils
     * @param MpHelper                                  $mpHelper
     * @param FeedbackFactory                           $feedbackModel
     * @param CollectionFactory                         $mpProductCollection
     * @param MpProductModel                            $mpProductModel
     * @param ProductFactory                            $productFactory
     * @param array                                     $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        Customer $customer,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Stdlib\StringUtils $stringUtils,
        MpHelper $mpHelper,
        FeedbackFactory $feedbackModel,
        CollectionFactory $mpProductCollection,
        MpProductModel $mpProductModel,
        ProductFactory $productFactory,
        CustomerData $customerBlock,
        array $data = []
    )
    {
        parent::__construct($context, $postDataHelper, $urlHelper, $customer, $session, $stringUtils, $mpHelper, $feedbackModel, $mpProductCollection, $mpProductModel, $productFactory, $data);
        $this->customerBlock = $customerBlock;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $partner = $this->getProfileDetail();
        if ($partner)
        {
            if (!$this->customerBlock->shouldDisplayData()) {
                $alias = $this->customerBlock->getSellerAlias($partner->getSellerId());

                if ($alias){
                    $title = 'Proveedor N&deg; '.$alias;
                } else {
                    $title = 'Proveedor Oculto';
                }
            } else {
                $title = $partner->getShopTitle();
            }
            
            if (!$title)
            {
                $title = __('Marketplace Seller Profile');
            }

            $this->pageConfig->getTitle()->set($title);
            $description = $partner->getMetaDescription();
            if ($description)
            {
                $this->pageConfig->setDescription($description);
            }
            else
            {
                $this->pageConfig->setDescription($this->stringUtils->substr($partner->getCompanyDescription(), 0, 255));
            }
            $keywords = $partner->getMetaKeywords();
            if ($keywords) {
                $this->pageConfig->setKeywords($keywords);
            }

            $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
            if ($pageMainTitle && $title) {
                $pageMainTitle->setPageTitle($title);
            }

            $this->pageConfig->addRemotePageAsset(
                $this->_urlBuilder->getCurrentUrl(''),
                'canonical',
                ['attributes' => ['rel' => 'canonical']]
            );
        }

        return $this;
    }
}
