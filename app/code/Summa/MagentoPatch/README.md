# This module applies fixes to magento core modules

## List of patches:

### Order save overwrite Order Items `info_buyRequest` value

Causing issues when completing the order, the data required on configurable products to be reordered is removed.

**Issue:** https://github.com/magento/magento2/issues/22431
**Fix:** https://github.com/magento/magento2/pull/28483