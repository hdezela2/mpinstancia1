<?php
namespace Summa\MagentoPatch\Model\Order;

use Magento\Sales\Api\Data\OrderItemInterface;

class ItemRepository extends \Magento\Sales\Model\Order\ItemRepository
{
    /**
     * Perform persist operations for one entity
     * 
     * REWRITE
     * Issue: https://github.com/magento/magento2/issues/22431
     * Fix: https://github.com/magento/magento2/pull/28483
     *
     * @param OrderItemInterface $entity
     * @return OrderItemInterface
     */
    public function save(OrderItemInterface $entity)
    {
        if ($entity->getProductOption()) {
            $entity->setProductOptions($this->getItemProductOptions($entity));
        }

        $this->metadata->getMapper()->save($entity);
        $this->registry[$entity->getEntityId()] = $entity;
        return $this->registry[$entity->getEntityId()];
    }

    /**
     * Return product options
     *
     * @param OrderItemInterface $entity
     * @return array
     */
    private function getItemProductOptions(OrderItemInterface $entity): array
    {
        $request = $this->getBuyRequest($entity);
        $productOptions = $entity->getProductOptions();
        $productOptions['info_buyRequest'] = $productOptions && !empty($productOptions['info_buyRequest'])
            ? array_merge($productOptions['info_buyRequest'], $request->toArray())
            : $request->toArray();

        return $productOptions;
    }
}
