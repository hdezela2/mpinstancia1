<?php

namespace Summa\ProductOfferLimit\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\ResourceConnection;

class Data extends AbstractHelper
{
    const XML_PATH_ENABLED = 'summa_product_offer_limit/general/enabled';
    const XML_PATH_LIMIT = 'summa_product_offer_limit/general/limit';
    const TABLE_NAME = 'marketplace_assignproduct_items';

    /**
     * @var ResourceConnection
     */
    protected $_resourceConnection;

    /**
     * Data constructor.
     * @param Context $context
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        Context $context,
        ResourceConnection $resourceConnection
    )
    {
        parent::__construct($context);
        $this->_resourceConnection = $resourceConnection;
    }

    /**
     * Is module enable for the scope
     * @return bool
     */
    public function isEnabled()
    {
        if (!$this->isModuleOutputEnabled()) {
            return false;
        }
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ENABLED,
            ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * Get Config Value
     * @return mixed
     */
    public function getConfigValue()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_LIMIT,
            ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * Get Seller Offers
     * @param $sellerId
     * @return string
     */
    public function getSellerOffers($sellerId)
    {
        $table = $this->_resourceConnection->getConnection()->getTableName(self::TABLE_NAME);
        $sql = 'SELECT COUNT(*) FROM '.$table.' 
        WHERE seller_id = '.$sellerId.' 
        AND MONTH(created_at) = MONTH(CURRENT_DATE()) 
        AND YEAR(created_at) = YEAR(CURRENT_DATE())';

        return $this->_resourceConnection->getConnection()->fetchOne($sql);
    }
}