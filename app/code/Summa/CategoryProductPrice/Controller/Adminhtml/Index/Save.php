<?php

namespace Summa\CategoryProductPrice\Controller\Adminhtml\Index;

use Formax\Offer\Model\Offer;
use Linets\AseoRenewalSetup\Model\AseoRenewalConstants;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable as ConfigurableProductType;
use Linets\InsumosSetup\Model\InsumosConstants;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Registry;
use Formax\AgreementInfo\Helper\Data as AgreementHelper;
use Linets\SellerCentralizedDispatchCost\Api\DispatchPriceManagementInterface;
use Magento\Catalog\Model\ResourceModel\Product as productResourceModel;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Result\PageFactory;
use Summa\CategoryProductPrice\Helper\Data as CategoryProductPriceHelper;
use Webkul\CategoryProductPrice\Model\ProductPriceFactory;
use Webkul\MpAssignProduct\Model\AssociatesFactory;
use Webkul\MpAssignProduct\Model\ResourceModel\Associates\CollectionFactory as AssociatesCollectionFactory;
use Webkul\MpAssignProduct\Model\ItemsFactory;
use Intellicore\EmergenciasRenewal\Constants as EmergenciasRenewalConstants;
use Linets\InsumosTempPriceAdjustment\Helper\Data as DataHelperInsumosTempPrice;
use Summa\FormatCurrency\Helper\Data;

class Save extends \Webkul\CategoryProductPrice\Controller\Adminhtml\Index\Save
{
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /** @var ProductFactory */
    protected $productFactory;

    /** @var AssociatesFactory */
    protected $assignAssociates;

    /**
     * @var AssociatesCollectionFactory
     */
    protected $_associatesCollection;

    /** @var ItemsFactory */
    protected $assignItems;

    /** @var CategoryFactory */
    protected $categoryFactory;

    /** @var JsonHelper */
    protected $jsonHelper;

    /** @var ConfigurableProductType */
    protected $configurableProductType;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /** @var ProductPriceFactory */
    protected $productPrice;

    /** @var ResourceConnection */
    protected $connection;

    /** @var string */
    protected $offerProductTable;

    /** @var string */
    protected $assignproductItemsTable;

    /** @var string */
    protected $associatedProductItemsTable;

    /** @var CategoryProductPriceHelper */
    protected $_categoryProductPriceHelper;

    /**
     * @var string
     */
    protected $updatePriceLogTable;

    /**
     * @var AgreementHelper
     */
    private $_agreementHelper;
    /**
     * @var DispatchPriceManagementInterface
     */
    protected $dispatchPriceManagement;
    /**
     * @var
     */
    protected $productResourceModel;

    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepositoryManager;

    protected $dataHelperPriceTemp;

    /**
     * @var Data
     */
    private $dataHelperCurrency;

    /**
     * @param Data $dataHelperCurrency
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory
     * @param Context $context
     * @param ProductFactory $productFactory
     * @param ItemsFactory $assignItems
     * @param AssociatesFactory $assignAssociates
     * @param AssociatesCollectionFactory $associatesCollection
     * @param ProductPriceFactory $productPrice
     * @param ConfigurableProductType $configurableProductType
     * @param JsonHelper $jsonHelper
     * @param DateTime $date
     * @param CategoryFactory $categoryFactory
     * @param PageFactory $resultPageFactory
     * @param ResourceConnection $resourceConnection
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param ProductRepository $productRepository
     * @param Registry $registry
     * @param CategoryProductPriceHelper $categoryProductPriceHelper
     * @param AgreementHelper $agreementHelper
     * @param DispatchPriceManagementInterface $dispatchPriceManagement
     * @param productResourceModel $productResourceModel
     * @param StoreRepositoryInterface $storeRepositoryManager
     * @param array $data
     */
    public function __construct(
        \Summa\FormatCurrency\Helper\Data $dataHelperCurrency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        Context $context,
        ProductFactory $productFactory,
        ItemsFactory $assignItems,
        AssociatesFactory $assignAssociates,
        AssociatesCollectionFactory $associatesCollection,
        ProductPriceFactory $productPrice,
        ConfigurableProductType $configurableProductType,
        JsonHelper $jsonHelper,
        DateTime $date,
        CategoryFactory $categoryFactory,
        PageFactory $resultPageFactory,
        ResourceConnection $resourceConnection,
        CategoryCollectionFactory $categoryCollectionFactory,
        ProductRepository $productRepository,
        Registry $registry,
        CategoryProductPriceHelper $categoryProductPriceHelper,
        AgreementHelper $agreementHelper,
        DispatchPriceManagementInterface $dispatchPriceManagement,
        productResourceModel $productResourceModel,
        StoreRepositoryInterface $storeRepositoryManager,
        DataHelperInsumosTempPrice $dataHelperPriceTemp,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $productFactory,
            $assignItems,
            $assignAssociates,
            $productPrice,
            $configurableProductType,
            $jsonHelper,
            $date,
            $categoryFactory,
            $resultPageFactory,
            $resourceConnection,
            $categoryCollectionFactory,
            $productRepository,
            $registry,
            $data
        );

        $this->_agreementHelper = $agreementHelper;
        $this->dispatchPriceManagement = $dispatchPriceManagement;
        $this->productResourceModel = $productResourceModel;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->productFactory = $productFactory;
        $this->assignAssociates = $assignAssociates;
        $this->_associatesCollection = $associatesCollection;
        $this->assignItems = $assignItems;
        $this->storeRepositoryManager = $storeRepositoryManager;
        $this->categoryFactory = $categoryFactory;
        $this->jsonHelper = $jsonHelper;
        $this->configurableProductType = $configurableProductType;
        $this->date = $date;
        $this->productPrice = $productPrice;
        $this->dataHelperCurrency = $dataHelperCurrency;
        $this->_categoryProductPriceHelper = $categoryProductPriceHelper;
        $this->dataHelperPriceTemp = $dataHelperPriceTemp;
        $this->connection = $resourceConnection->getConnection();
        $this->offerProductTable = $resourceConnection->getTableName('dccp_offer_product');
        $this->assignProductItemsTable = $resourceConnection->getTableName('marketplace_assignproduct_items');
        $this->associatedProductItemsTable = $resourceConnection->getTableName('marketplace_assignproduct_associated_products');
        $this->updatePriceLogTable = $resourceConnection->getTableName('category_product_price');
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page|void
     */
    public function execute()
    {
        $productData = $this->getRequest()->getParam('product');
        $pricePercentage = $this->getRequest()->getParam('pricePercentage');
        $allCategories = $this->getRequest()->getParam('allCategories');
        $productPriceHistoryId = $this->getRequest()->getParam('productPriceHistoryId');
        $count = $this->getRequest()->getParam('count');
        $newIndex = $this->getRequest()->getParam('newIndex');
        try {
            $responseData = $this->updatePrice($productData, $pricePercentage, $allCategories);
            $result['error'] = 0;
            $result['msg'] = '<div class="wk-mu-success wk-mu-box">' . __('Successfully updated ' . $responseData['name']) . '</div>';
        } catch (\Exception $e) {
            $result['error'] = 1;
            $result['msg'] = '<div class="wk-mu-error wk-mu-box">' . __('Error in updating price') . '</div>';
        }
        $this->_categoryProductPriceHelper->updateProductPriceHistory($productPriceHistoryId, $count, $productData, $newIndex);
        $result = $this->jsonHelper->jsonEncode($result);
        $this->getResponse()->representJson($result);
    }

    protected function isProductEmergencias($websiteCode)
    {
        return \Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE == $websiteCode;
    }

    protected function isProductMobiliario($websiteCode)
    {
        return \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE == $websiteCode;
    }

    /**
     * @param $websiteCode
     * @return bool
     */
    protected function isProductInsumos($websiteCode)
    {
        return \Linets\InsumosSetup\Model\InsumosConstants::WEBSITE_CODE == $websiteCode;
    }

    protected function getCurrencyprecision($codeConvenioProducto)
    {
        if (!$this->dataHelperCurrency->isEnabled()) {
            return 0;
        }
        $currencyCode = $this->storeManager->getStore($codeConvenioProducto)->getCurrentCurrency()->getCode();

        if (!empty($currencyCode)) {
            $customOptions = $this->dataHelperCurrency->getConfigValue($currencyCode);
            if (!is_null($customOptions)) {
                return $customOptions['precision'];
            }
        }
    }

    protected function productConvenio($product)
    {
        $productWebsites = $product->getWebsiteIds();

        $websites = \Chilecompra\LoginMp\Helper\Helper::CONVENIOS_WEBSITES;
        foreach ($productWebsites as $id) {
            $websiteCore = $this->storeManager->getWebsite($id)->getCode();
            if (isset($websites[$websiteCore])) {
                return $websites[$websiteCore];
            }
        }
    }

    /**
     * @param $productData
     * @param $percentage
     * @param $allCategories
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updatePrice($productData, $percentage, $allCategories)
    {
        $websiteCode = $this->storeManager->getWebsite()->getCode();
        $isInsumos = $websiteCode == InsumosConstants::WEBSITE_CODE;
        $isEmergenciaRenewal = $websiteCode == EmergenciasRenewalConstants::WEBSITE_CODE;
        $approvedRequiredSites = [InsumosConstants::WEBSITE_CODE];
        /** @var TYPE_NAME $codeConvenioProducto */
        $codeConvenioProducto = '';
        $pricePercentage = $this->getRequest()->getParam('pricePercentage');
        $product = $this->productFactory->create();

        if (!$isEmergenciaRenewal) {
            $assignItem = $this->assignItems->create()->load($productData['id']);
            $product->setId($assignItem->getProductId());
        } else {
            if ($productData['child']) {
                $collection = $this->_associatesCollection->create();
                $collection
                    ->addFieldToFilter('main_table.id', ['eq' => $productData['id']])
                    ->getSelect()->join(
                        ["mai" => $collection->getTable('marketplace_assignproduct_items')],
                        "main_table.parent_id = mai.id",
                        ['seller_id']
                    );
                $assignItem = $collection->getFirstItem();
                $product->setId($assignItem->getParentProductId());
            } else {
                $assignItem = $this->assignItems->create()->load($productData['id']);
                $product->setId($assignItem->getProductId());
            }
            $codeConvenioProducto = $this->_categoryProductPriceHelper->productConvenio($product);
        }

        $agreementId = $this->_agreementHelper->getAgreementId();

        $isChild = false;
        if (isset($productData['child'])) {
            $isChild = (is_string($productData['child'])) ? $productData['child'] == "true" : $productData['child'] == true;
        }
        if ($isChild) {
            //si es un producto configurable - tenemos en el Item el Id de la tabla AssignproductAssociated.
            $assignProducts = $this->assignAssociates->create()->getCollection();
            $assignProducts->addFieldToFilter('main_table.id', $productData['id']);
            $assignProducts->getSelect()
                ->joinInner(
                    ['mai' => $this->assignProductItemsTable],
                    'mai.id = main_table.parent_id',
                    ['seller_id']
                );
            //despues del JOIN puede pasar que tenga mas de un assignId resultante? entiendo que no!
            foreach ($assignProducts as $assignProduct) {
                //consulto si tiene una offerta especial activa (precio con descuento - info de otra tabla
                $productModel = $this->productFactory->create();
                $productId = $assignProduct->getProductId();
                $productModel->setId($productId);

                $select = $this->connection->select();
                $codeConvenioProducto = $this->productConvenio($productModel);

                if (!$isEmergenciaRenewal) {
                    $select->from($this->offerProductTable, ['*'])
                        ->where('product_id = ?', $assignProduct->getProductId())
                        ->where('seller_id = ?', $assignProduct->getSellerId())
                        ->where('status in ('.Offer::OFFER_APPLIED.', '.Offer::OFFER_CREATED.')');

                    $offer = $this->connection->fetchOne($select);

                    //Special Offer
                    if ($offer) {
                        $offerRow = $this->connection->fetchRow($select);
                        $websiteId = $offerRow['website_id'];
                        $websiteCode = $this->storeManager->getWebsite($websiteId)->getCode();
                        if ($websiteCode == InsumosConstants::WEBSITE_CODE) {
                            $codeConvenioProducto = $this->productConvenio($productModel);
                            $precision = $this->getCurrencyPrecision($codeConvenioProducto);
                            $shippingPriceInsumos = 0;
                            $basePriceInsumos = 0;
                            if ($offerRow['base_price'] > 0) {
                                $basePriceInsumos = $offerRow['base_price'];
                                // Get product
                                $attrSetId = $this->productResourceModel->getAttributeRawValue($assignProduct->getProductId(), 'attribute_set_id', \Magento\Store\Model\Store::DEFAULT_STORE_ID);
                                $attrSetId = $attrSetId['attribute_set_id'];
                                $macrozonaId = $this->productResourceModel->getAttributeRawValue($assignProduct->getProductId(), 'macrozona', \Magento\Store\Model\Store::DEFAULT_STORE_ID);
                                $macrozona = $this->productResourceModel->getAttribute('macrozona')->getSource()->getOptionText($macrozonaId);

                                $websiteId = $this->storeRepositoryManager->get(InsumosConstants::WEBSITE_CODE)->getId();
                                // Get dispatch Price
                                $resultDispatch = $this->dispatchPriceManagement->getDispatchPrice(
                                    $assignProduct->getSellerId(),
                                    $macrozona,
                                    $attrSetId,
                                    $websiteId
                                );
                                $shippingPriceInsumos = (int)($resultDispatch ? $resultDispatch->getPrice() : 0);
                                $calcutaledPricesAdjustment = $this->dataHelperPriceTemp->calculatePrice($basePriceInsumos, $shippingPriceInsumos, $percentage, $offer);
                                $updatedPrice = $calcutaledPricesAdjustment['updatedPrice'];
                            } else {
                                $updateBasePriceInsumos = 0;
                                $updatedPrice = $updateBasePriceInsumos + $shippingPriceInsumos;
                            }
                            $update = ['base_price' => $updatedPrice];
                            $where = ['id = ?' => $offerRow['id']]; //Aca se puede Usar $offer->getId()
                            $this->connection->update($this->offerProductTable, $update, $where);

                            $updatedPrice = ['original_price' => $basePriceInsumos, 'updated_price' => $updatedPrice];
                        } else {
                            if ($offerRow['status'] == \Formax\Offer\Model\Offer::OFFER_CREATED) {
                                $this->updatePriceNoOffer($assignProduct, $percentage, $codeConvenioProducto,
                                    $productData['child']);
                            }
                            $updatedPrice = $this->_categoryProductPriceHelper->updatePriceOffer(
                                $offerRow['base_price'], $assignProduct, $percentage, $codeConvenioProducto);
                        }
                    } else {
                        $updatedPrice = $this->updatePriceNoOffer($assignProduct, $percentage, $codeConvenioProducto, $productData['child']);
                    }

                    $this->updateLog(
                        $productModel,
                        $updatedPrice['updated_price'],
                        $assignProduct->getSellerId(),
                        $pricePercentage,
                        $updatedPrice['original_price'],
                        $allCategories
                    );
                } else {
                    $basePrice = 0;
                    $select->from($this->offerProductTable, ['base_price'])
                        ->where('product_id = ?', $assignProduct->getProductId())
                        ->where('start_date >= DATE(NOW())')
                        ->where('end_date <= DATE(NOW())')
                        ->where('status = 3');
                    $basePrice = $this->connection->fetchOne($select);

                    if ($basePrice) {
                        $prices = $this->_categoryProductPriceHelper->updatePriceOffer($basePrice, $assignProduct, $percentage, $codeConvenioProducto);
                    } else {
                        $prices = $this->_categoryProductPriceHelper->updatePriceNoOffer($assignProduct, $percentage, $codeConvenioProducto, true);
                    }

                    $this->_categoryProductPriceHelper->updateLog(
                        $product,
                        $prices['updated_price'],
                        $assignItem->getSellerId(),
                        $pricePercentage,
                        $prices['original_price'],
                        $allCategories
                    );
                }
            }

            if (!$isEmergenciaRenewal) {
                $productName = $product->getResource()->getAttributeRawValue(
                    $productModel->getId(),
                    'name',
                    \Magento\Store\Model\Store::DEFAULT_STORE_ID
                );
            } else {
                $productName = $product->getResource()->getAttributeRawValue(
                    $product->getId(),
                    'name',
                    \Magento\Store\Model\Store::DEFAULT_STORE_ID
                );
            }


            $data['name'] = $productName;
        } else {
            $basePrice = 0;
            $select = $this->connection->select();
            //faltaba filtrar por el vendedor (pueden existir varias ofertas pero de distinto vendedor con distintos precios base.
            //Los filtros invertidos en el campo de la fecha?
            if (!$isEmergenciaRenewal) {
                $select->from($this->offerProductTable, ['*'])
                    ->where('product_id = ?', $assignItem->getProductId())
                    ->where('seller_id = ?', $assignItem->getSellerId())
                    ->where('start_date >= DATE(NOW())')
                    ->where('end_date <= DATE(NOW())')
                    ->where('status = 1');

                $offer = $this->connection->fetchOne($select);

                if ($offer) {
                    $updatedPrice = $this->updatePriceOffer($offer, $percentage, $codeConvenioProducto);
                } else {
                    $updatedPrice = $this->updatePriceNoOffer($assignItem, $percentage, $codeConvenioProducto);
                }
            } else {
                $select->from($this->offerProductTable, ['base_price'])
                    ->where('product_id = ?', $assignItem->getProductId())
                    ->where('seller_id = ?', $assignItem->getSellerId())
                    ->where('start_date >= DATE(NOW())')
                    ->where('end_date <= DATE(NOW())')
                    ->where('status = 3');

                $basePrice = $this->connection->fetchOne($select);

                if ($basePrice) {
                    $prices = $this->_categoryProductPriceHelper->updatePriceOffer($basePrice, $assignItem, $percentage, $codeConvenioProducto);
                } else {
                    $prices = $this->_categoryProductPriceHelper->updatePriceNoOffer($assignItem, $percentage, $codeConvenioProducto);
                }
            }

            $productName = $product->getResource()->getAttributeRawValue(
                $product->getId(),
                'name',
                \Magento\Store\Model\Store::DEFAULT_STORE_ID
            );
            $data['name'] = $productName;

            if (!$isEmergenciaRenewal) {
                $this->updateLog(
                    $product,
                    $updatedPrice['updated_price'],
                    $assignItem->getSellerId(),
                    $pricePercentage,
                    $updatedPrice['original_price'],
                    $allCategories
                );
            } else {
                $this->_categoryProductPriceHelper->updateLog(
                    $product,
                    $prices['updated_price'],
                    $assignItem->getSellerId(),
                    $pricePercentage,
                    $prices['original_price'],
                    $allCategories
                );
            }
        }

        return $data;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updatePriceNoOffer($assignItem, $percentage, $codeConvenioProducto, $isChild = false)
    {
        $precision = $this->getCurrencyPrecision($codeConvenioProducto);
        $originalPrice = $assignItem->getPrice();
        if ($this->isProductEmergencias($codeConvenioProducto)) {
            $basePriceEmergencia = $assignItem->getBasePrice();
            $shippinPriceEmergencia = $assignItem->getShippingPrice();
            if ($basePriceEmergencia > 0) {
                $updateBasePriceEmergencia = round((($percentage * $basePriceEmergencia / 100) + $basePriceEmergencia), $precision, PHP_ROUND_HALF_DOWN);
                $updatedPrice = $updateBasePriceEmergencia + $shippinPriceEmergencia;
            } else {
                $updateBasePriceEmergencia = 0;
                $updatedPrice = $updateBasePriceEmergencia + $shippinPriceEmergencia;
            }
            $update = ['price' => $updatedPrice, 'base_price' => $updateBasePriceEmergencia];
        } else if ($this->isProductMobiliario($codeConvenioProducto)) {
            $basePriceMobiliario = $assignItem->getBasePrice();
            $assemblyPriceMobiliario = $assignItem->getAssemblyPrice();
            if ($basePriceMobiliario > 0) {
                $updateBasePriceMobiliario = round((($percentage * $basePriceMobiliario / 100) + $basePriceMobiliario), $precision, PHP_ROUND_HALF_DOWN);
                $updatedPrice = $updateBasePriceMobiliario + $assemblyPriceMobiliario;
            } else {
                $updateBasePriceMobiliario = 0;
                $updatedPrice = $updateBasePriceMobiliario + $assemblyPriceMobiliario;
            }
            $update = ['price' => $updatedPrice, 'base_price' => $updateBasePriceMobiliario];
        } else if ($this->isProductInsumos($codeConvenioProducto)) {
            $basePriceInsumos = $assignItem->getBasePrice();

            // Get product
            $product = $this->productFactory->create();
            $this->productResourceModel->load($product, $assignItem->getData('product_id'));

            $attributeSetId = $product->getAttributeSetId();
            $macrozone = $product->getAttributeText('macrozona');
            $websiteId = $this->storeRepositoryManager->get(InsumosConstants::WEBSITE_CODE)->getId();

            // Get dispatch Price
            $resultDispatch = $this->dispatchPriceManagement->getDispatchPrice($assignItem->getSellerId(), $macrozone, $attributeSetId, $websiteId);
            $shippingPriceInsumos = (int)($resultDispatch ? $resultDispatch->getPrice() : 0);

            if ($basePriceInsumos > 0) {
                $updateBasePriceInsumos = round((($percentage * $basePriceInsumos / 100) + $basePriceInsumos), $precision, PHP_ROUND_HALF_DOWN);
                $updatedPrice = $updateBasePriceInsumos + $shippingPriceInsumos;
            } else {
                $updateBasePriceInsumos = 0;
                $updatedPrice = $updateBasePriceInsumos + $shippingPriceInsumos;
            }
            $update = ['price' => $updatedPrice, 'base_price' => $updateBasePriceInsumos];
        } else {
            if ($originalPrice > 0) {
                $updatedPrice = round((($percentage * $originalPrice / 100) + $originalPrice), $precision, PHP_ROUND_HALF_DOWN);
            } else {
                $updatedPrice = 0;
            }
            $update = ['price' => $updatedPrice];
        }
        $where = ['id = ?' => $assignItem->getId()];

        if ($isChild) {
            //acá se hace un Update SQL en lugar de un save por cuestiones de performance?
            $this->connection->update($this->associatedProductItemsTable, $update, $where);
        } else {
            $this->connection->update($this->assignProductItemsTable, $update, $where);
        }

        return ['original_price' => $originalPrice, 'updated_price' => $updatedPrice];
    }

    /**
     * @param $product
     * @param $itemPrice
     * @param $sellerId
     * @param $pricePercentage
     * @param $originalPrice
     * @param $allCategories
     * @return void
     */
    public function updateLog($product, $itemPrice, $sellerId, $pricePercentage, $originalPrice, $allCategories)
    {
        $categoryIds = $product->getCategoryIds($product);
        $allCategories = implode(',', $categoryIds);
        $parentProductId = $this->configurableProductType->getParentIdsByChild($product->getId());
        if ($parentProductId) {
            $parentProduct = $this->productFactory->create();
            $parentProduct->setId($parentProductId[0]);
            $categoryIds = $parentProduct->getCategoryIds($parentProduct);
            $allCategories = implode(',', $categoryIds);
        }

        $this->connection->insert(
            $this->updatePriceLogTable,
            [
                'magecategory_id' => $allCategories,
                'mageproduct_id' => $product->getId(),
                'mageproduct_price' => $originalPrice,
                'updatedproduct_price' => $itemPrice,
                'update_percentage' => $pricePercentage,
                'seller_id' => $sellerId,
                'created_at' => $this->date->gmtDate(),
                'updated_at' => $this->date->gmtDate()
            ]
        );
    }
}
