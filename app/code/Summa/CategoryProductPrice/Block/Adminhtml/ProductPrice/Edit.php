<?php
namespace Summa\CategoryProductPrice\Block\Adminhtml\ProductPrice;

use Formax\AgreementInfo\Helper\Data as AgreementHelper;
use Magento\Backend\Block\Widget\Context;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\ResourceModel\Group\CollectionFactory as StoreGroupCollection;

class Edit extends \Webkul\CategoryProductPrice\Block\Adminhtml\ProductPrice\Edit
{
    /** @var \Magento\Framework\Registry */
    protected $_coreRegistry = null;

    /** @var AgreementHelper */
    private $_helper;

    /** @var StoreGroupCollection */
    private $_storeGroupCollection;

    /** @var CollectionFactory */
    private $categoryCollectionFactory;

    /**
     * @param Context $context
     * @param CollectionFactory $categoryCollectionFactory
     * @param Registry $registry
     * @param AgreementHelper $agreementHelper
     * @param StoreGroupCollection $storeGroupColection
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $categoryCollectionFactory,
        Registry $registry,
        AgreementHelper $agreementHelper,
        StoreGroupCollection $storeGroupColection,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_helper = $agreementHelper;
        $this->_storeGroupCollection = $storeGroupColection;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context, $categoryCollectionFactory, $registry, $data);
    }

    public function getCategoriesTree()
    {
        $collection = $this->categoryCollectionFactory->create()->addAttributeToSelect('*');

        $agreementId = $this->_helper->getAgreementId();
        if ($agreementId == \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_DEPOT) {
            $rootCategoryId = $this->_storeGroupCollection->create()->addFieldToFilter('website_id', ["eq" => 1])->getFirstItem()->getRootCategoryId();
            $collection->addAttributeToFilter(
                array(
                    array('attribute' => 'path', 'like' => '1/'.$rootCategoryId.'/%'),
                    array('attribute' => 'path', 'eq' => '1/'.$rootCategoryId)
                )
            );
        }

        if ($agreementId == \Chilecompra\LoginMp\Helper\Helper::ID_AGREEMENT_FURNITURE) {
            $collection->addAttributeToFilter('name', ['nlike' => '%armado']);
        }

        $sellerCategory = [
            Category::TREE_ROOT_ID => [
                'value' => Category::TREE_ROOT_ID,
                'optgroup' => null,
            ],
        ];

        foreach ($collection as $category) {
            $catId = $category->getId();
            $catParentId = $category->getParentId();
            foreach ([$catId, $catParentId] as $categoryId) {
                if (!isset($sellerCategory[$categoryId])) {
                    $sellerCategory[$categoryId] = ['value' => $categoryId];
                }
            }

            $sellerCategory[$catId]['is_active'] = $category->getIsActive();
            $sellerCategory[$catId]['label'] = $category->getName();
            $sellerCategory[$catParentId]['optgroup'][] = &$sellerCategory[$catId];
        }
        return json_encode($sellerCategory[Category::TREE_ROOT_ID]['optgroup']);

    }
}
