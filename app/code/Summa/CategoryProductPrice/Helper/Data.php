<?php
namespace Summa\CategoryProductPrice\Helper;

use Chilecompra\LoginMp\Helper\Helper as ConveniosHelper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;
use Cleverit\CategoryProductPrice\Model\Repository\ProductPriceHistoryRepository;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable as ConfigurableProductType;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Summa\FormatCurrency\Helper\Data as HelperCurrency;

class Data extends AbstractHelper
{
    /** @var string */
    protected $_offerProductTable;

    /** @var string */
    protected $_associatedProductItemsTable;

    /** @var string */
    protected $_updatePriceLogTable;

    /** @var ResourceConnection */
    protected $_connection;

    /** @var StoreManagerInterface */
    protected $_storeManager;

    /** @var ProductFactory */
    protected $_productFactory;

    /** @var ConfigurableProductType */
    protected $_configurableProductType;

    /** @var ProductPriceHistoryRepository */
    protected $_productPriceHistoryRepository;

    /** @var HelperCurrency */
    protected $_dataHelperCurrency;

    /** @var DateTime */
    protected $_date;

    protected $_assignProductItemsTable;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        HelperCurrency $dataHelperCurrency,
        ProductFactory $productFactory,
        ConfigurableProductType $configurableProductType,
        DateTime $date,
        ResourceConnection $resourceConnection,
        ProductRepository $productRepository,
        ProductPriceHistoryRepository $productPriceHistoryRepository
    ) {
        $this->_storeManager = $storeManager;
        $this->_productFactory = $productFactory;
        $this->_configurableProductType = $configurableProductType;
        $this->_date = $date;
        $this->_dataHelperCurrency = $dataHelperCurrency;
        $this->_connection = $resourceConnection->getConnection();
        $this->_offerProductTable = $resourceConnection->getTableName('dccp_offer_product');
        $this->_assignProductItemsTable = $resourceConnection->getTableName('marketplace_assignproduct_items');
        $this->_associatedProductItemsTable = $resourceConnection->getTableName('marketplace_assignproduct_associated_products');
        $this->_updatePriceLogTable = $resourceConnection->getTableName('category_product_price');
        $this->_productPriceHistoryRepository = $productPriceHistoryRepository;
        parent::__construct($context);
    }

    public function updatePriceOffer($basePrice, $assignItem, $percentage, $codeConvenioProducto)
    {
        $precision = $this->getCurrencyPrecision($codeConvenioProducto);
        $originalPrice = $basePrice;
        if ($basePrice > 0) {
            $updatedPrice = round((($percentage*$basePrice/100) + $basePrice),$precision,PHP_ROUND_HALF_DOWN);
        } else {
            $updatedPrice = 0;
        }

        $update = ['base_price' => $updatedPrice];
        $where = ['product_id = ?' => $assignItem->getProductId(), 'status in ('.\Formax\Offer\Model\Offer::OFFER_CREATED.', '.\Formax\Offer\Model\Offer::OFFER_APPLIED.')'];
        $this->_connection->update($this->_offerProductTable, $update, $where);

        return ['original_price' => $originalPrice, 'updated_price' => $updatedPrice];
    }

    public function updatePriceNoOffer($assignItem, $percentage, $codeConvenioProducto, $isChild=false)
    {
        $precision = $this->getCurrencyPrecision($codeConvenioProducto);
        $originalPrice = $assignItem->getPrice();
        if ($this->isProductEmergencias($codeConvenioProducto)) {
            if ($isChild) {
                $basePriceEmergencia = $assignItem->getPrice();
                $shippinPriceEmergencia = 0;
            } else {
                $basePriceEmergencia = $assignItem->getBasePrice();
                $shippinPriceEmergencia  = $assignItem->getShippingPrice();
            }
            if ($basePriceEmergencia > 0 ) {
                $updateBasePriceEmergencia = round((($percentage*$basePriceEmergencia/100) + $basePriceEmergencia),$precision,PHP_ROUND_HALF_DOWN);
                $updatedPrice = $updateBasePriceEmergencia + $shippinPriceEmergencia;
            } else {
                $updateBasePriceEmergencia = 0;
                $updatedPrice = $updateBasePriceEmergencia + $shippinPriceEmergencia;
            }
            if ($isChild) {
                $update = ['price' => $updatedPrice];
            } else {
                $update = ['price' => $updatedPrice, 'base_price' => $updateBasePriceEmergencia];
            }
        } else if ($this->isProductMobiliario($codeConvenioProducto)) {
            $basePriceMobiliario = $assignItem->getBasePrice();
            $assemblyPriceMobiliario  = $assignItem->getAssemblyPrice();
            if ($basePriceMobiliario > 0 ) {
                $updateBasePriceMobiliario = round((($percentage*$basePriceMobiliario/100) + $basePriceMobiliario),$precision,PHP_ROUND_HALF_DOWN);
                $updatedPrice = $updateBasePriceMobiliario + $assemblyPriceMobiliario;
            } else {
                $updateBasePriceMobiliario = 0;
                $updatedPrice = $updateBasePriceMobiliario + $assemblyPriceMobiliario;
            }
            $update = ['price' => $updatedPrice, 'base_price' => $updateBasePriceMobiliario];
        } else {
            if ($originalPrice > 0) {
                $updatedPrice = round((($percentage*$originalPrice/100) + $originalPrice),$precision,PHP_ROUND_HALF_DOWN);
            } else {
                $updatedPrice = 0;
            }
            $update = ['price' => $updatedPrice];
        }
        $where = ['id = ?' => $assignItem->getId()];
        if ($isChild){
            //acá se hace un Update SQL en lugar de un save por cuestiones de performance?
            $this->_connection->update($this->_associatedProductItemsTable, $update, $where);
        } else {
            $this->_connection->update($this->_assignProductItemsTable, $update, $where);
        }
        return ['original_price' => $originalPrice, 'updated_price' => $updatedPrice];
    }

    protected function getCurrencyPrecision($codeConvenioProducto){
        if (!$this->_dataHelperCurrency->isEnabled()) {
            return 0;
        }
        $currencyCode =  $this->_storeManager->getStore($codeConvenioProducto)->getCurrentCurrency()->getCode();

        if (!empty($currencyCode)) {
            $customOptions = $this->_dataHelperCurrency->getConfigValue($currencyCode);
            if (!is_null($customOptions)) {
                return $customOptions['precision'];
            }
        }
    }

    protected function isProductEmergencias($websiteCode)
    {
        return in_array($websiteCode,[\Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE, \Intellicore\EmergenciasRenewal\Constants::WEBSITE_CODE]);
    }

    protected function isProductMobiliario($websiteCode)
    {
        return \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE == $websiteCode;
    }

    public function updateLog($product, $itemPrice, $sellerId, $pricePercentage, $originalPrice, $allCategories)
    {
        $categoryIds = $product->getCategoryIds($product);
        $allCategories = implode(',', $categoryIds);
        $parentProductId = $this->_configurableProductType->getParentIdsByChild($product->getId());
        if ($parentProductId) {
            $parentProduct = $this->_productFactory->create();
            $parentProduct->setId($parentProductId[0]);
            $categoryIds = $parentProduct->getCategoryIds($parentProduct);
            $allCategories = implode(',', $categoryIds);
        }

        $this->_connection->insert(
            $this->_updatePriceLogTable,
            [
                'magecategory_id' => $allCategories,
                'mageproduct_id' => $product->getId(),
                'mageproduct_price' => $originalPrice,
                'updatedproduct_price' => $itemPrice,
                'update_percentage' => $pricePercentage,
                'seller_id' => $sellerId,
                'created_at' => $this->_date->gmtDate(),
                'updated_at' => $this->_date->gmtDate()
            ]
        );
    }

    /**
     * Get CM of Product
     * @param $product
     * @return string
     * @throws LocalizedException
     */
    public function productConvenio($product)
    {
        $productWebsites = $product->getWebsiteIds();

        $websites = ConveniosHelper::CONVENIOS_WEBSITES;
        foreach ($productWebsites as $id){
            $websiteCode = $this->_storeManager->getWebsite($id)->getCode();
            if (isset($websites[$websiteCode])){
                return $websites[$websiteCode];
            }
        }
    }

    public function updateProductPriceHistory($productPriceHistoryId, $count, $productData, $newIndex)
    {
        $productPriceHistory = $this->_productPriceHistoryRepository->getById($productPriceHistoryId);
        $assignProductIds = $productPriceHistory->getAssignProductIds();

        $countAssignProductIdsBefore = count($assignProductIds);

        // remove the actual assign product item id from the asignProductIds and update the assignProductIds value on the database
        $assignProductId = $productData['id'];
        foreach ($assignProductIds as $key => $value) {
            if ($value['id'] == $assignProductId) {
                unset($assignProductIds[$key]);
            }
        }

        $countAssignProductIdsAfter = count($assignProductIds);

        if ($countAssignProductIdsBefore != $countAssignProductIdsAfter) {
            $productPriceHistory->setAssignProductIds($assignProductIds);
        }
        $productCountActual = $count + $newIndex;
        $productPriceHistory->setProductCountActual($productCountActual);
        $this->_productPriceHistoryRepository->save($productPriceHistory);
    }
}
