<?php

namespace Summa\UntradedProducts\Helper;

use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    const ID_AGREEMENT_ASEO = 5800266;
    const CODE_AGREEMENT_ASEO = 'convenio_aseo';
    const CODE_STORE_AGREEMENT_ASEO = 'aseo';

    const ID_AGREEMENT_EMERGENCY = 5800262;
    const CODE_AGREEMENT_EMERGENCY = 'emergencias';
    const CODE_STORE_AGREEMENT_EMERGENCY = 'emergencias';

    const ID_AGREEMENT_EMERGENCY_2021_09 = ConstantsEmergency202109::ID_AGREEMENT_EMERGENCY_2021_09;
    const CODE_AGREEMENT_EMERGENCY_2021_09 = ConstantsEmergency202109::WEBSITE_CODE;
    const CODE_STORE_AGREEMENT_EMRGENCY_2021_09 = ConstantsEmergency202109::STORE_CODE;

    const ID_AGREEMENT_COMPUTERS = 5800272;
    const CODE_AGREEMENT_COMPUTERS = 'computadores';
    const CODE_STORE_AGREEMENT_COMPUTERS = 'computadores';

    const ID_AGREEMENT_VOUCHER = 5800271;
    const CODE_AGREEMENT_VOUCHER = 'convenio_voucher';
    const CODE_STORE_AGREEMENT_VOUCHER = 'voucher';

    const ID_AGREEMENT_SOFTWARE = 5800275;
    const CODE_AGREEMENT_SOFTWARE = 'convenio_software';
    const CODE_STORE_AGREEMENT_SOFTWARE = 'software';

    const ID_AGREEMENT_FUELS = 5800276;
    const CODE_AGREEMENT_FUELS = 'combustibles';
    const CODE_STORE_AGREEMENT_FUELS = 'combustibles';

    const ID_AGREEMENT_COMBUSTIBLES202110 = 5802326;
    const CODE_AGREEMENT_COMBUSTIBLES202110 = 'combustibles202110';
    const CODE_STORE_AGREEMENT_COMBUSTIBLES202110 = 'combustibles202110';

    const ID_AGREEMENT_DESKITEMS = 5800280;
    const CODE_AGREEMENT_DESKITEMS = 'escritorio';
    const CODE_STORE_AGREEMENT_DESKITEMS = 'escritorio';

    const ID_AGREEMENT_FURNITURE = 5800277;
    const CODE_AGREEMENT_FURNITURE = 'mobiliario';
    const CODE_STORE_AGREEMENT_FURNITURE = 'mobiliario';

    const ID_AGREEMENT_DEPOT = 5800254;
    const CODE_AGREEMENT_DEPOT = 'base';
    const CODE_STORE_AGREEMENT_DEPOT = 'ferreteria';

    const MAP_AGREMENT_RELATION = [
        self::ID_AGREEMENT_DEPOT => self::CODE_AGREEMENT_DEPOT,
        self::ID_AGREEMENT_COMPUTERS => self::CODE_AGREEMENT_COMPUTERS,
        self::ID_AGREEMENT_EMERGENCY => self::CODE_AGREEMENT_EMERGENCY,
        self::ID_AGREEMENT_EMERGENCY_2021_09 => self::CODE_AGREEMENT_EMERGENCY_2021_09,
        self::ID_AGREEMENT_ASEO => self::CODE_AGREEMENT_ASEO,
        self::ID_AGREEMENT_VOUCHER => self::CODE_AGREEMENT_VOUCHER,
        self::ID_AGREEMENT_SOFTWARE => self::CODE_AGREEMENT_SOFTWARE,
        self::ID_AGREEMENT_FUELS => self::CODE_AGREEMENT_FUELS,
        self::ID_AGREEMENT_COMBUSTIBLES202110 => self::CODE_AGREEMENT_COMBUSTIBLES202110,
        self::ID_AGREEMENT_FURNITURE => self::CODE_AGREEMENT_FURNITURE,
        self::ID_AGREEMENT_DESKITEMS => self::CODE_AGREEMENT_DESKITEMS
    ];


    protected $mapAgreetmenIdAndWebsite;

    protected $websiteRepository;

    /**
     * @var \Summa\UntradedProducts\Model\UntradedProductsFactory
     */
    protected $_UntradedProductsFactory;
    /**
     * __construct
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param  \Summa\UntradedProducts\Model\UntradedProductsFactory $UntradedProductsFactory

     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Summa\UntradedProducts\Model\UntradedProductsFactory $UntradedProductsFactory,
        \Magento\Store\Api\WebsiteRepositoryInterface $websiteRepository

    ) {

        $this->_UntradedProductsFactory = $UntradedProductsFactory;
        $this->websiteRepository = $websiteRepository;
        parent::__construct($context);
    }
    /**
     * Untraded Assign Product
     *
     * @param int $assignId
     * @param int $status [optional]
     */
    public function untradedProduct($assignId, $status)
    {
        $assignProduct = $this->getAssignProduct($assignId);
        if ($assignProduct->getId() > 0) {
            if ($status == 1) {
                $data = [];
                $data['status'] = 5;
                $data['seller_id'] = $assignProduct->getSellerId();
                $data['product_id'] = $assignProduct->getProductId();
                $data['qty'] = $assignProduct->getQty();
                $assignProduct->addData($data)->save();
            }
        }
        return $assignProduct;
    }

    /**
     * Get Assign Product by AssignId
     *
     * @param int $assignId
     *
     * @return object
     */
    public function getAssignProduct($assignId)
    {
        $assignProduct = $this->_UntradedProductsFactory->create()->load($assignId);
        return $assignProduct;
    }


    protected function getWebsiteId($code)
    {
        try {
            $website = $this->websiteRepository->get($code);
            return (int)$website->getId();
        } catch (\Exception $exception) {
            $this->_logger->error($exception->getMessage());
        }
    }

    public function getGeneratedMap()
    {
        $this->mapAgreetmenIdAndWebsite = [];
        foreach (self::MAP_AGREMENT_RELATION as $key => $value){
            $this->mapAgreetmenIdAndWebsite[$key] = $this->getWebsiteId($value);
        }

        return $this->mapAgreetmenIdAndWebsite;
    }
}
