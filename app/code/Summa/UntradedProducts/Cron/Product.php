<?php

namespace Summa\UntradedProducts\Cron;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Ui\Component\MassAction\Filter;
use Formax\AgreementInfo\Model\ResourceModel\Agreement\CollectionFactory as AgreementFactory;
use Summa\UntradedProducts\Model\UntradedProductsFactory;
use Summa\UntradedProducts\Model\ResourceModel\UntradedProducts\CollectionFactory as UntProductsFactory;
use Zend\Http\Client;
class Product  extends AbstractHelper

{
    protected $client;
    protected $logger;
    protected $untradedProductsFactory;
    protected $scopeConfig;
    protected $agreementFactory;
    protected $untProductsFactory;

    /**
     * @var ProductCollection
     */
    private $_productCollection;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var \Summa\UntradedProducts\Helper\Data
     */
    protected $helper;

    /**
     * Product constructor.
     * @param Context $context
     * @param \Magento\Framework\Logger\Monolog $logger
     * @param Client $client
     * @param Filter $filter
     * @param UntradedProductsFactory $untradedProductsfactory
     * @param AgreementFactory $agreementFactory
     * @param UntProductsFactory $untProductsFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Logger\Monolog $logger,
        \Zend\Http\Client $client,
        \Magento\Ui\Component\MassAction\Filter $filter,
        UntradedProductsFactory $untradedProductsfactory,
        AgreementFactory $agreementFactory,
        \Summa\UntradedProducts\Helper\Data $helper,
        ProductCollection  $productCollection,
        UntProductsFactory $untProductsFactory
    ) {
        $this->logger = $logger;
        $this->client = $client;
        $this->untradedProductsFactory=$untradedProductsfactory;
        $this->filter = $filter;
        $this->agreementFactory=$agreementFactory;
        $this->_productCollection = $productCollection;
        $this->untProductsFactory=$untProductsFactory;
        $this->helper = $helper;

        parent::__construct($context);
    }

    /**
     * Gets user information from DCCP WS.
     *
     *
     * @return array $response Server Response Payload.
     */
    public function execute()
    {
        $mapWebsiteId = $this->helper->getGeneratedMap();

        $endpoint = rtrim(
            $this->scopeConfig->getValue(
                'untraded_products/general/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            '/'
        );

        if (empty($endpoint)) {
            $this->logger->critical(__("Empty user endpoint.
                Go to Stores->Configuration->Endpoints DCCP Login"));
        }elseif (filter_var($endpoint, FILTER_VALIDATE_URL) === false) {
            $this->logger->critical(__("Wrong endpoint. Not a valid URL.
                Go to Stores->Configuration->Endpoints DCCP Login"));
        }else{
            {

                $collection = $this->untProductsFactory->create();
                $this->logger->addInfo('Start to remove all items - List with ' . $collection->getSize(). ' Items');
                if($collection->getData()> 0) {
                    foreach ($collection as $item) {
                        //$this->logger->addInfo('row eliminada: ItemID '.$item->getProductId().'  - ConvenioId '.$item->getConveniomarcoId());
                        $item->delete();
                    }
                }

                $collection = $this->agreementFactory->create();
                $this->logger->addInfo('Start add New items int the List');
                foreach($collection as $convenio) {
                    /*
                     * Puebas Alimentos.
                     * $payLoad = [
                        'idConvenioMarco' => 5800279,
                        'idTipoProducto' => "",
                        'fechaUltimaOC' => ""
                    ];*/
                    $payLoad = [
                        'idConvenioMarco' => $convenio->getData('agreement_id'),
                        'idTipoProducto' => "",
                        'fechaUltimaOC' => ""
                    ];

                    $data   = json_encode($payLoad);
                    $client = new Client();
                    $client->setUri($endpoint);
                    $httpHeaders = [
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '
                    ];
                    $client->setHeaders($httpHeaders);
                    $client->setRawBody($data);
                    $client->setParameterGet($payLoad);
                    $response = json_decode($client->send()->getBody());

                    $this->logger->addInfo('Convenio ' . $convenio->getData('agreement_id'));

                    $limitTEST = 0;
                    if (isset($response->payload) && $response->success == 'OK') {
                        $this->logger->addInfo('Items ' . json_encode($response->payload));
                        foreach ($response->payload as $product) {
                            try {
                                $typeProduct = $this->getTypeproduct($product->idProducto);
                                $uProducts= $this->untradedProductsFactory->create();
                                $uProducts->setData('product_id', $product->idProducto);
                                $uProducts->setData('conveniomarco_id', $product->idConvenioMarco);
                                $uProducts->setData('fecha_revision', $product->fechaRevision);
                                $uProducts->setData('tipoproducto_id',$product->idTipoProducto);
                                $uProducts->setData('licitacion', $product->licitacion);
                                $uProducts->setData('tipo_producto', $product->tipoProducto);
                                $uProducts->setData('tipo_producto_magento', implode($typeProduct->getProductTypeIds()));
                                $uProducts->setData('producto', $product->producto);
                                $uProducts->setData('marca', $product->marca);
                                $uProducts->setData('modelo', $product->modelo);
                                $uProducts->setData('created_at', $product->fechaCreacion);
                                $uProducts->setData('updated_at', '');
                                $uProducts->setData('meses_creacion', $product->mesesCreacion);
                                $uProducts->setData('fecha_ultima_oc', $product->fechaUltimaOC);
                                $uProducts->setData('meses_ultima_oc', $product->mesesUltimaOC);
                                $uProducts->setData('ofertas', $product->ofertas);
                                $uProducts->setData('no_transada', $product->noTransada);
                                $uProducts->setData('en_convenio', $product->enConvenio);
                                $uProducts->setData('en_tienda', $product->enTienda);
                                $uProducts->setData('gc_invitando', $product->gcInvitando);
                                $uProducts->setData('gc_cerrada', $product->gcCerrada);
                                $uProducts->setData('ocultar', $product->ocultar);
                                $uProducts->setData('activo', $product->activo);
                                $uProducts->setData('estado', 0);
                                $uProducts->setData('website_id', $mapWebsiteId[$product->idConvenioMarco]);
                                $uProducts->save();

                                $limitTEST = $limitTEST +1;

                                if ($limitTEST  == 5){
                                    break;
                                }

                            } catch (\Exception $e) {
                                $info = 'Error updating product during UntradedProducts => itemInfo:' . json_encode($product);
                                $this->logger->info($info);
                                $this->logger->critical($info);
                                $this->logger->critical('Exception message => ' . $e->getMessage());
                            }
                        }
                    } else {
                        $context = [
                            "endpoint" => $endpoint,
                            "response" => $response
                        ];
                        $this->logger->error('Invalid token or server response', $context);
                    }
                }
            }
        }
        return $this;
    }

    public function getTypeProduct($sku)
    {
        $collection = $this->_productCollection->create();
        $collection->addAttributeToFilter('sku',$sku);
        $collection->getColumnValues('type_id');
        return $collection;
    }


}
