<?php

namespace Summa\UntradedProducts\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /*
         * Create table 'untraded_products'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('untraded_products'))
            ->addColumn(
                'row_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true,'auto_increment' => true,
                ],
                'Entity ID'
            )
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Product ID'
            )

            ->addColumn(
                'conveniomarco_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Convenio Marco ID'
            )
            ->addColumn(
                'fecha_revision',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [],
                'Fecha Revision'
            )
            ->addColumn(
                'tipoproducto_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Tipo de Producto ID'
            )
            ->addColumn(
                'licitacion',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => '', 'LENGTH' =>255],
                'Licitacion'
            )
            ->addColumn(
                'tipo_producto',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => '', 'LENGTH' =>255],
                'Tipo producto'
            )
            ->addColumn(
                'tipo_producto_magento',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => '', 'LENGTH' =>255],
                'Tipo producto Magento'
            )
            ->addColumn(
                'producto',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => '', 'LENGTH' =>255],
                'Producto'
            )
            ->addColumn(
                'marca',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => '', 'LENGTH' =>255],
                'Marca'
            )
            ->addColumn(
                'modelo',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => '', 'LENGTH' =>255],
                'Modelo'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                null,
                [],
                'Created At'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false],
                'Update Time'
            )
            ->addColumn(
                'meses_creacion',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Meses Creacion'
            )
            ->addColumn(
                'fecha_ultima_oc',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                null,
                ['nullable'=> false],
                'Fecha Ultima OC'
            )
            ->addColumn(
                'meses_ultima_oc',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Meses Ultima OC'
            )
            ->addColumn(
                'ofertas',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Ofertas'
            )
            ->addColumn(
                'no_transada',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                [ 'identity' => false, 'nullable' => false],
                'No transada'
            )
            ->addColumn(
                'en_convenio',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                [ 'identity' => false, 'nullable' => false],
                'En Convenio'
            )
            ->addColumn(
                'en_tienda',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                [ 'identity' => false, 'nullable' => false],
                'En Tienda'
            )
            ->addColumn(
                'gc_invitando',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                [ 'identity' => false, 'nullable' => false],
                'GC Invitando'
            )
            ->addColumn(
                'gc_cerrada',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                [ 'identity' => false, 'nullable' => false],
                'GC Cerrada'
            )
            ->addColumn(
                'ocultar',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                [ 'identity' => false, 'nullable' => false],
                'Ocultar'
            )
            ->addColumn(
                'activo',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                [ 'identity' => false, 'nullable' => false],
                'Activo'
            )
            ->addColumn(
                'estado',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                [ 'identity' => false, 'nullable' => false],
                'Estado'
            )

            ->setComment('Untraded Products');
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}        