<?php

namespace Summa\UntradedProducts\Console\Command;

use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Http\Client;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Ui\Component\MassAction\Filter;
use Formax\AgreementInfo\Model\ResourceModel\Agreement\CollectionFactory as AgreementFactory;
use Summa\UntradedProducts\Model\UntradedProductsFactory;
use Summa\UntradedProducts\Model\ResourceModel\UntradedProducts\CollectionFactory as UntProductsFactory;

class ProductListUpdate extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;


    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * @var Client|\Zend\Http\Client
     */
    protected $client;

    /**
     * @var UntradedProductsFactory|\Summa\UntradedProducts\Model\UntradedProductsFactory
     */
    protected $untradedProductsFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var \AgreementFactory|AgreementFactory
     */
    protected $agreementFactory;
    /**
     * @var UntProductsFactory|\UntProductsFactory
     */
    protected $untProductsFactory;

    /**
    protected $adjustmentCollectionFactory;

    /**
     * @var \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceUpdateFactory
     */
    protected $updateAdjustment;

    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $dateTime;

    /**
     * @var \Summa\TemporaryPriceAdjustment\Model\TemporaryPriceAdjustmentFactory
     */
    protected $adjustment;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var \Summa\UntradedProducts\Helper\Data
     */
    protected $helper;

    /**
     * ProductListUpdate constructor.
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Framework\Logger\Monolog $logger
     * @param Client $client
     * @param Filter $filter
     * @param UntradedProductsFactory $untradedProductsfactory
     * @param AgreementFactory $agreementFactory
     * @param UntProductsFactory $untProductsFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Summa\UntradedProducts\Helper\Data $helper
     * @param null $name
     */
    public function __construct(
        \Magento\Framework\App\State $appState,
        \Magento\Framework\Logger\Monolog $logger,
        Client $client,
        Filter $filter,
        UntradedProductsFactory $untradedProductsfactory,
        AgreementFactory $agreementFactory,
        UntProductsFactory $untProductsFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Summa\UntradedProducts\Helper\Data $helper,
        $name = null
    ) {
        $this->logger = $logger;
        $this->client = $client;
        $this->appState = $appState;
        $this->untradedProductsFactory=$untradedProductsfactory;
        $this->filter = $filter;
        $this->agreementFactory=$agreementFactory;
        $this->untProductsFactory=$untProductsFactory;
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('product:untradedproduct:list');
        $this->setDescription("Update untraded product list");

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mapWebsiteId = $this->helper->getGeneratedMap();
        $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);

        $this->logger->addInfo('Lamada a servicio no transado');
        $endpoint = rtrim(
            $this->scopeConfig->getValue(
                'untraded_products/general/endpoint',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            '/'
        );

        if (empty($endpoint)) {
            $this->logger->critical(__("Empty user endpoint.
                Go to Stores->Configuration->Endpoints DCCP Login"));
        }elseif (filter_var($endpoint, FILTER_VALIDATE_URL) === false) {
            $this->logger->critical(__("Wrong endpoint. Not a valid URL.
                Go to Stores->Configuration->Endpoints DCCP Login"));
        }else{

            $collection = $this->untProductsFactory->create();
            $this->logger->addInfo('Start to remove all items - List with ' . $collection->getSize(). ' Items');
            if($collection->getData()> 0) {
                foreach ($collection as $item) {
                    //$this->logger->addInfo('row eliminada: ItemID '.$item->getProductId().'  - ConvenioId '.$item->getConveniomarcoId());
                    $item->delete();
                    //mejora posible realizar un truncate.
                }
            }

            $collection = $this->agreementFactory->create();
            $this->logger->addInfo('Start add New items int the List');
            foreach($collection as $convenio) {
                $payLoad = [
                    'idConvenioMarco' => $convenio->getData('agreement_id'),
                    'idTipoProducto' => "",
                    'fechaUltimaOC' => ""
                ];

                $data   = json_encode($payLoad);
                $client = new Client();
                $client->setUri($endpoint);
                $httpHeaders = [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '
                ];
                $client->setHeaders($httpHeaders);
                $client->setRawBody($data);
                $client->setParameterGet($payLoad);
                $response = json_decode($client->send()->getBody());

                $this->logger->addInfo('Convenio ' . $convenio->getData('agreement_id'));

                if (isset($response->payload) && $response->success == 'OK') {
                    $this->logger->addInfo('Items ' . json_encode($response->payload));
                    foreach ($response->payload as $product) {
                        try {
                            $uProducts= $this->untradedProductsFactory->create();
                            $uProducts->setData('product_id', $product->idProducto);
                            $uProducts->setData('conveniomarco_id', $product->idConvenioMarco);
                            $uProducts->setData('fecha_revision', $product->fechaRevision);
                            $uProducts->setData('tipoproducto_id',$product->idTipoProducto);
                            $uProducts->setData('licitacion', $product->licitacion);
                            $uProducts->setData('tipo_producto', $product->tipoProducto);
                            $uProducts->setData('tipo_producto_magento', 'Simple');
                            $uProducts->setData('producto', $product->producto);
                            $uProducts->setData('marca', $product->marca);
                            $uProducts->setData('modelo', $product->modelo);
                            $uProducts->setData('created_at', $product->fechaCreacion);
                            $uProducts->setData('updated_at', '');
                            $uProducts->setData('meses_creacion', $product->mesesCreacion);
                            $uProducts->setData('fecha_ultima_oc', $product->fechaUltimaOC);
                            $uProducts->setData('meses_ultima_oc', $product->mesesUltimaOC);
                            $uProducts->setData('ofertas', $product->ofertas);
                            $uProducts->setData('no_transada', $product->noTransada);
                            $uProducts->setData('en_convenio', $product->enConvenio);
                            $uProducts->setData('en_tienda', $product->enTienda);
                            $uProducts->setData('gc_invitando', $product->gcInvitando);
                            $uProducts->setData('gc_cerrada', $product->gcCerrada);
                            $uProducts->setData('ocultar', $product->ocultar);
                            $uProducts->setData('activo', $product->activo);
                            $uProducts->setData('estado', 0);
                            $uProducts->setData('website_id', $mapWebsiteId[$product->idConvenioMarco]);
                            $uProducts->save();

                        } catch (\Exception $e) {
                            $info = 'Error updating product during UntradedProducts => itemInfo:' . json_encode($product);
                            $this->logger->info($info);
                            $this->logger->critical($info);
                            $this->logger->critical('Exception message => ' . $e->getMessage());
                        }
                    }
                } else{
                    $context = [
                        "endpoint" => $endpoint,
                        "response" => $response
                    ];
                    $this->logger->error('Invalid token or server response', $context);
                }
            }
        }

    }
}
