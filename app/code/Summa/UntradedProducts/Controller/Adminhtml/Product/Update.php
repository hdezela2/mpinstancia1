<?php

namespace Summa\UntradedProducts\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\Result\JsonFactory;
use Summa\UntradedProducts\Model\ResourceModel\UntradedProducts\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;

class Update extends Action
{
    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var ProductCollection
     */
    private $_productCollection;


    /**
     * @var \Webkul\MpAssignProduct\Helper\Data
     */
    protected $_assignHelper;
    protected $productRepository;

    /**
     * @param Context     $context
     * @param JsonFactory $resultJsonFactory
     * @param \Webkul\MpAssignProduct\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Webkul\MpAssignProduct\Helper\Data $helper,
        ProductCollection  $productCollection,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository


    )   {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->_assignHelper = $helper;
        $this->_productCollection = $productCollection;
        $this->productRepository = $productRepository;
        parent::__construct($context);

    }

    /**
     * Execute action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $done = 0;
            foreach ($collection as $item) {
                if ($item->getData('estado') == 0) {
                    $productId =$item->getData('product_id');
                    $product = $this->getProductCollection($productId);
                    $productStatus = $this->productRepository->getById(implode($product->getAllIds()));
                    $productStatus->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
                    $productStatus->save();
                    if (implode($product->getProductTypeIds()) == 'configurable') {
                       //para que se utiliza esta varible? merge
                        $assignProduct = $this->_assignHelper->disApproveUntradedConfigProduct($productId, 1, 1);
                    }
                    else {
                        $data = [];
                        $data['estado'] = 5;
                        $item->addData($data)->save();
                        $assignProduct = $this->_assignHelper->disApproveUntradedProduct($product->getAllIds());
                    }
                    ++$done;
                }
            }
            if ($done) {
                $this->messageManager->addSuccess(__('A total of %1 record(s) were modified.', $done));
            }

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        return $resultRedirect->setPath('untradedproducts/index/index');
    }

    public function getProductCollection($sku)
    {
        $collection = $this->_productCollection->create();
        $collection->addAttributeToFilter('sku',$sku);
        $collection->getColumnValues('entity_id');
        return $collection;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Summa_UntradedProducts::update');
    }
}
