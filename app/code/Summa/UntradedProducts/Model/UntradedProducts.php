<?php
namespace Summa\UntradedProducts\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;

class UntradedProducts extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'untraded_products';

    protected $_cacheTag = 'untraded_products';

    protected $_eventPrefix = 'untraded_products';

    protected function _construct()
    {
        $this->_init('Summa\UntradedProducts\Model\ResourceModel\UntradedProducts');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}