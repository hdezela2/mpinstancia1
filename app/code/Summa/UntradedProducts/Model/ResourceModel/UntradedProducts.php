<?php
namespace Summa\UntradedProducts\Model\ResourceModel;

use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManager;

class UntradedProducts extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @var StoreManager
     */
    private $_storeManager;

    /**
     * @var int
     */
    private $_store;

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        StoreManager $storeManager
    )
    {
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('untraded_products', 'row_id');
    }

    /**
     * Load an object using 'identifier' field if there's no field specified and value is not numeric.
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param mixed $value
     * @param string $field
     *
     * @return $this
     */
    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        if (!is_numeric($value) && $field === null) {
            $field = 'identifier';
        }

        return parent::load($object, $value, $field);
    }

    /**
     * Set store model.
     *
     * @param int $store
     *
     * @return $this
     */
    public function setStore($store)
    {
        $this->_store = $store;

        return $this;
    }

    /**
     * Retrieve store model.
     *
     * @return StoreInterface
     */
    public function getStore()
    {
        return $this->_storeManager->getStore($this->_store);
    }

}
