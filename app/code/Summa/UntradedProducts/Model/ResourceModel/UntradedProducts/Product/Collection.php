<?php

namespace Summa\UntradedProducts\Model\ResourceModel\UntradedProducts\Product;

class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $adminSession;

    public function __construct(
        \Magento\Backend\Model\Auth\Session $adminSession,
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,

        $mainTable = 'untraded_products',
        $resourceModel = '\Summa\UntradedProducts\Model\ResourceModel\UntradedProducts',
        \Magento\Framework\App\RequestInterface $request
    )
    {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
        $this->adminSession = $adminSession;
    }

    public function _beforeLoad()
    {

        $this->adminSession;
        $websiteList = $this->adminSession->getUser()->getRole()->getGwsWebsites();

        //add filter by websites into the role configuration
        $this->addFieldToFilter('main_table.website_id', array('in' => $websiteList ));

        return parent::_beforeLoad();
    }

}

