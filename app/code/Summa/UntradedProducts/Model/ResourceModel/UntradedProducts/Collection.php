<?php
namespace Summa\UntradedProducts\Model\ResourceModel\UntradedProducts;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'row_id';
    protected $_eventPrefix = 'untraded_products_collection';
    protected $_eventObject = 'untradedproducts_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Summa\UntradedProducts\Model\UntradedProducts',
            'Summa\UntradedProducts\Model\ResourceModel\UntradedProducts');
    }


}