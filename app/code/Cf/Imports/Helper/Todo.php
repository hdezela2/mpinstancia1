<?php
namespace Cf\Imports\Helper;

use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;

class Todo extends \Magento\Framework\App\Helper\AbstractHelper{
    protected $_storeManager;
    protected $_connection;
    protected $_hashPasswod;
    protected $_customer;
    protected $_customerFactory;
    protected $_sellerFactory;
    protected $_sellerCollectionFactory;
    protected $_date;
    protected $_seller;
    protected $vendorAttributeCollectionFactory;
    protected $helper;
    protected $attributeFactory;
    protected $_customerRepository;
    protected $_customerMapper;
    protected $_dataObjectHelper;
    private Filesystem $filesystem;
    private UploaderFactory $fileUploaderFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Customer\Api\Data\CustomerInterfaceFactory $customer,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Encryption\EncryptorInterface $hashPasswod,
        \Webkul\Marketplace\Model\SellerFactory $sellerFactory,
        \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $sellerCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\Marketplace\Model\Seller $seller,
        \Webkul\MpVendorAttributeManager\Model\ResourceModel\VendorAttribute\CollectionFactory $vendorAttributeCollectionFactory,
        \Webkul\MpVendorAttributeManager\Helper\Data $helper,
        \Magento\Customer\Model\AttributeFactory $attributeFactory,
        \Magento\Customer\Model\Customer\Mapper $customerMapper,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        Filesystem $filesystem,
        UploaderFactory $fileUploaderFactory
    ){
        $this->_storeManager = $storeManager;
        $this->_customer = $customer;
        $this->_customerFactory = $customerFactory;
        $this->_date = $date;
        $this->_hashPasswod = $hashPasswod;
        $this->_sellerFactory = $sellerFactory;
        $this->_seller = $seller;
        $this->_sellerCollectionFactory = $sellerCollectionFactory;
        $this->helper = $helper;
        $this->attributeFactory = $attributeFactory;
        $this->_connection = $resourceConnection->getConnection();
        $this->vendorAttributeCollectionFactory = $vendorAttributeCollectionFactory;
        $this->_customerRepository = $customerRepository;
        $this->_customerMapper = $customerMapper;
        $this->_dataObjectHelper = $dataObjectHelper;
        parent::__construct($context);
        $this->filesystem = $filesystem;
        $this->fileUploaderFactory = $fileUploaderFactory;
    }

    private function getCustomerWebsiteId($_websiteString){
        return $this->_storeManager->getWebsite($_websiteString)->getWebsiteId();
    }
    private function getCustomerStoreId($_storeString){
        return $this->_storeManager->getStore($_storeString)->getId();
    }

    public function _saveCustomer($_customer){
        try {
            if(!isset($_customer['email']) || !$this->_vldEmail($_customer['email'])){
                return ['error' => true, 'return' => 'Attribute is required or format email not valid.'];
            }
            #
            $_customerId = $this->_vldCustomerEmail($_customer['email']);
            if($_customerId){
                return ['error' => false, 'return' => $_customerId];
            }else{
                #
                $_customerData = $this->_vldData($_customer, 'customer');
                if($_customerData['error']){
                    return ['error' => true, 'return' => 'Attributes is required to create account: '.json_encode($_customerData['fields'])];
                }

                $customer   = $this->_customerFactory->create();
                $customer->setWebsiteId($this->getCustomerWebsiteId($_customer['_website']));
                $customer->setStoreId($this->getCustomerStoreId($_customer['_store']));
                $customer->setEmail($_customer['email']);
                $customer->setFirstname($_customer['firstname']);
                $customer->setLastname($_customer['lastname']);
                $customer->setPassword($_customer['password']);
                $customer->save();
                # $customer->sendNewAccountEmail();
                if ($customer->getId()) {
                    $_extras = [];
                    foreach ($_customer as $key => $value) {
                        if($key != 'email' &&
                           $key != 'firstname' &&
                           $key != 'lastname' &&
                           $key != 'password'){
                            $_extras[$key] = $value;
                        }
                    }
                    #
                    if(count($_extras) > 0){
                      $_customerData = $this->_customerRepository->getById($customer->getId());
                      foreach ($_extras as $key => $value) {
                         $_customerData->setCustomAttribute($key, $value);
                      }
                      $this->_customerRepository->save($_customerData);
                    }
                    return ['error' => false, 'return' => $customer->getId()];
                }
            }
        } catch (\Exception $e) {
            return ['error' => true, 'return' => $e->getMessage()];
        }
        return ['error' => true, 'return' => 'User already exists.'];
    }

    public function _saveBecomeSeller($_shop_, $idClient){
        try {
            $_shop_url = !isset($_shop_['shop_url']) ? '' : $_shop_['shop_url'];
            $_shopId = $this->_vldUrlShop($_shop_url);
            if($_shopId){
                return ['error' => false, 'return' => $_shopId];
            }
            #create shop
            $_shopData = $this->_vldData($_shop_, 'shop');
            if($_shopData['error']){
                return ['error' => true, 'return' => 'Attributes is required to create shop: '.json_encode($_shopData['fields'])];
            }
            #

            #
            $autoId = 0;
            $collection = $this->_sellerCollectionFactory->create();
            $collection->addFieldToFilter('seller_id', $idClient);
            foreach ($collection as $value) {
                $autoId = $value->getId();
                break;
            }

            $seller = $this->_sellerFactory->create()->load($autoId);

            if( isset($_shop_['shop_title']) && !is_null($_shop_['shop_title']) && (strlen($_shop_['shop_title']) > 1) ):
                $shop_title = $_shop_['shop_title'];
                $seller->setData('shop_title', $shop_title);
            endif;

            if( isset($_shop_['is_seller']) && !is_null($_shop_['is_seller']) ):
                $seller->setData('is_seller', ($_shop_['is_seller'] * 1));
            endif;

            if( isset($_shop_['shop_url']) && !is_null($_shop_['shop_url']) ):
                $seller->setData('shop_url', $_shop_['shop_url']);
            endif;


            $seller->setData('seller_id', $idClient);
            $seller->setCreatedAt($this->_date->gmtDate());
            $seller->setUpdatedAt($this->_date->gmtDate());
            $seller->setAdminNotification(1);
            $_seller = $seller->save();
            if ($_seller->getId()){
                return ['error' => false, 'return' => $_seller->getId()];
            }
        } catch (\Exception $e) {
            return ['error' => true, 'return' => $e->getMessage()];
        }
        return ['error' => true, 'return' => 'Seller not exists.'];
    }


    public function _saveSeller($seller, $customerId){
        try {
            $_sellerData = $this->_vldData($seller, 'seller');
            if($_sellerData['error']){
                return ['error' => true, 'return' => 'Attributes is required to update seller attributes: '.json_encode($_sellerData['fields'])];
            }
            #
            $vendorAttributeCollection = $this->vendorAttributeCollectionFactory->create()->getVendorAttributeCollection();
            $error = [];
            $sellerData = $seller;
            $sellerData = $this->setBooleanData($sellerData);
            foreach ($vendorAttributeCollection as $vendorAttribute) {
                foreach ($sellerData as $attributeCode => $attributeValue) {
                    if ($attributeCode==$vendorAttribute->getAttributeCode()) {
                        if ($vendorAttribute->getIsRequired() && empty($attributeValue)) {
                            $error[] = $vendorAttribute->getAttributeCode();
                        }
                    }
                }
            }
            if (!empty($error)) {
                return ['error' => true, 'return' => 'Attributes is required: '.json_encode($error)];
            } else {
                $savedCustomerData = $this->_customerRepository->getById($customerId);
                $saveData = $this->_customerMapper->toFlatArray($savedCustomerData);
                $customer = $this->_customer->create();

                $sellerData = array_merge(
                    $this->_customerMapper->toFlatArray($savedCustomerData),
                    $sellerData
                );
                $sellerData['id'] = $customerId;
                /*$path = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)
                             ->getAbsolutePath('vendorfiles/');*/

                $this->_dataObjectHelper->populateWithArray(
                    $customer,
                    $sellerData,
                    '\Magento\Customer\Api\Data\CustomerInterface'
                );
                $this->_customerRepository->save($customer);
                return ['error' => false, 'return' => __('OK')];
            }
        } catch (\Exception $e) {
            return ['error' => true, 'return' => $e->getMessage()];
        }
    }


    protected function _vldCustomerEmail($email){
        $_result =  $this->_connection->fetchAll("SELECT * FROM customer_entity WHERE email = '$email'");
        if($_result){
            return $_result[0]['entity_id'];
        }else{ return false;
        }
    }

    protected function _vldEmail($email){
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          return false;
        }
        return true;
    }


    protected function _vldUrlShop($shop_url){
        $_result =  $this->_connection->fetchAll("SELECT * FROM marketplace_userdata WHERE shop_url = '$shop_url'");
        if($_result){
            return $_result[0]['entity_id'];
        }else{ return false;
        }
    }

    public function _getValidColumnNames(){
        $_data = $this->_getAttrCustomerImport();
        $_attributes = [];
        foreach ($_data as $item) { $_attributes[] = $item['attribute_code'];}
        $_attributes[] = 'shop_url';
        $_attributes[] = 'shop_title';
        $_attributes[] = 'is_seller';
        return $_attributes;
    }


    public function _getOptionsAttributes(){
        $_data = $this->_getAttrCustomerImport();
        $_attribute = [];
        foreach ($_data as $item) {
            $is_required = $item['is_required'];
            if($is_required == 0){
                $is_required = $item['wkv_required_field'];
            }
            $_group = !is_null($item['wkv_id']) ?  'seller' :  'customer';
            if(!is_null($item['wkv_id']) && $item['wkv_id'] == 'shop'){
                $_group = 'shop';
            }
            ##
            if($item['frontend_input'] == 'select' ||  $item['frontend_input'] == 'multiselect'){
                $__options = [];
                foreach ($this->_getOptions($item['attribute_code']) as $_item) {
                    $__options[] = $_item['value'];
                }
                $_attribute[$item['attribute_code']] = array('type' => $item['frontend_input'],
                                                             'is_unique' => $item['is_unique'],
                                                             'is_required' => $is_required,
                                                             'options' => $__options,
                                                             'group' => $_group);
            }elseif($item['frontend_input'] == 'boolean'){
                $_attribute[$item['attribute_code']] = array('type' => $item['frontend_input'],
                                                             'is_unique' => $item['is_unique'],
                                                             'is_required' => $is_required,
                                                             'options' => array('Sí', 'No'),
                                                             'group' => $_group);
            }elseif($item['frontend_input'] == 'select' && $item['attribute_code'] == 'website_id'){

            }elseif($item['frontend_input'] == 'select' && $item['attribute_code'] == 'store_id'){

            }elseif($item['frontend_input'] == 'select' && $item['attribute_code'] == 'group_id'){

            }elseif($item['frontend_input'] == 'text'){
                $_attribute[$item['attribute_code']] = array('type' => $item['frontend_input'],
                                                             'is_unique' => $item['is_unique'],
                                                             'is_required' => $is_required,
                                                             'group' => $_group);
            }elseif($item['frontend_input'] == 'date'){
                $_attribute[$item['attribute_code']] = array('type' => $item['frontend_input'],
                                                             'is_unique' => $item['is_unique'],
                                                             'is_required' => $is_required,
                                                             'group' => $_group);
            }else{
            }
        }
        return $_attribute;
    }


    public function _getOptions($_code, $store = 'base'){
        return $this->_connection->fetchAll("SELECT
                                                eav_ov.value,
                                                eav_ov.option_id
                                             FROM eav_attribute eav
                                             INNER JOIN eav_attribute_option eav_op
                                             ON eav.attribute_id = eav_op.attribute_id
                                             INNER JOIN eav_attribute_option_value eav_ov
                                             ON eav_op.option_id = eav_ov.option_id
                                             WHERE eav.attribute_code = '$_code' AND eav_ov.store_id = 0");
    }

    protected function _getAttrCustomerImport($website = 'base'){
        $attributes = $this->_connection->fetchAll("
                                            SELECT
                                               _eav.*,
                                               cea.is_visible,
                                               cea.is_system,
                                               cea.input_filter,
                                               cea.validate_rules,
                                               wkv_attr.entity_id AS wkv_id,
                                               wkv_attr.required_field AS wkv_required_field,
                                               wkv_attr.show_in_front AS wkv_show_in_front,
                                               wkv_attr.wk_attribute_status AS wkv_attribute_status,
                                               wkv_attr.attribute_used_for AS wkv_attribute_used_for
                                            FROM  (SELECT
                                                        eav.attribute_id,
                                                        eav.attribute_code,
                                                        eav.frontend_input,
                                                        eav.is_required,
                                                        eav.is_unique
                                                   FROM eav_attribute eav
                                                   INNER JOIN eav_entity_type eavt ON eav.entity_type_id = eavt.entity_type_id
                                                   WHERE eavt.entity_type_code = 'customer') AS _eav
                                            INNER JOIN customer_eav_attribute cea ON _eav.attribute_id = cea.attribute_id
                                            LEFT JOIN (SELECT
                                                           ceaw.attribute_id,
                                                           ceaw.is_visible,
                                                           sw.website_id,
                                                           sw.code
                                                        FROM customer_eav_attribute_website ceaw
                                                        INNER JOIN store_website sw ON  ceaw.website_id = sw.website_id
                                                        WHERE code = '$website') AS wbs ON _eav.attribute_id = wbs.attribute_id
                                            LEFT JOIN marketplace_vendor_attribute AS wkv_attr ON wkv_attr.attribute_id = _eav.attribute_id
                                            WHERE 1;");
        $_attributes = $attributes;
        foreach ($_attributes as $key => $value) {
            if($value['attribute_code'] == 'created_in' ||
               $value['attribute_code'] == 'created_at' ||
               $value['attribute_code'] == 'prefix' ||
               $value['attribute_code'] == 'middlename' ||
               $value['attribute_code'] == 'suffix' ||
               $value['attribute_code'] == 'dob' ||
               $value['attribute_code'] == 'rp_token' ||
               $value['attribute_code'] == 'rp_token_created_at' ||
               $value['attribute_code'] == 'password_hash' ||
               $value['attribute_code'] == 'default_billing' ||
               $value['attribute_code'] == 'default_shipping' ||
               $value['attribute_code'] == 'taxvat' ||
               $value['attribute_code'] == 'confirmation' ||
               $value['attribute_code'] == 'disable_auto_group_change' ||
               $value['attribute_code'] == 'updated_at' ||
               $value['attribute_code'] == 'failures_num' ||
               $value['attribute_code'] == 'first_failure' ||
               $value['attribute_code'] == 'lock_expires' ||
               $value['attribute_code'] == 'reward_update_notification' ||
               $value['attribute_code'] == 'reward_warning_notification' ||
               $value['attribute_code'] == 'allowed_shipping' ||
               $value['attribute_code'] == 'virtual_cart' ||
               $value['attribute_code'] == 'group_id' ||
               $value['attribute_code'] == 'user_rest_atk' ||
               $value['attribute_code'] == 'user_rest_email'
            ){
                unset($attributes[$key]);
            }

            if($value['wkv_attribute_used_for'] != 2 && $value['wkv_attribute_used_for'] != null){
                unset($attributes[$key]);
            }
        }
        #
        //shop: shop_url
        $attributes[] = [
            'attribute_id' => 0,
            'attribute_code' => 'shop_url',
            'frontend_input' => 'text',
            'is_required' => 0,
            'is_unique' => 1,
            'is_visible' => 1,
            'is_system' => 0,
            'input_filter' => 'trim',
            'validate_rules' => null,
            'wkv_id' => 'shop',
            'wkv_required_field' => null,
            'wkv_show_in_front' => null,
            'wkv_attribute_status' => null,
            'wkv_attribute_used_for' => null,
        ];
        //shop: shop_title
        $attributes[] = [
            'attribute_id' => 0,
            'attribute_code' => 'shop_title',
            'frontend_input' => 'text',
            'is_required' => 0,
            'is_unique' => 1,
            'is_visible' => 1,
            'is_system' => 0,
            'input_filter' => 'trim',
            'validate_rules' => null,
            'wkv_id' => 'shop',
            'wkv_required_field' => null,
            'wkv_show_in_front' => null,
            'wkv_attribute_status' => null,
            'wkv_attribute_used_for' => null,
        ];
        //shop: shop_title   is_seller
        $attributes[] = [
            'attribute_id' => 0,
            'attribute_code' => 'is_seller',
            'frontend_input' => 'text',
            'is_required' => 0,
            'is_unique' => 0,
            'is_visible' => 1,
            'is_system' => 0,
            'input_filter' => 'trim',
            'validate_rules' => null,
            'wkv_id' => 'shop',
            'wkv_required_field' => null,
            'wkv_show_in_front' => null,
            'wkv_attribute_status' => null,
            'wkv_attribute_used_for' => null,
        ];
        //shop: is_seller
        $attributes[] = [
            'attribute_id' => 0,
            'attribute_code' => 'is_seller',
            'frontend_input' => 'text',
            'is_required' => 0,
            'is_unique' => 0,
            'is_visible' => 1,
            'is_system' => 0,
            'input_filter' => 'trim',
            'validate_rules' => null,
            'wkv_id' => 'shop',
            'wkv_required_field' => null,
            'wkv_show_in_front' => null,
            'wkv_attribute_status' => null,
            'wkv_attribute_used_for' => null,
        ];
        //customer: password
        $attributes[] = [
            'attribute_id' => 0,
            'attribute_code' => 'password',
            'frontend_input' => 'text',
            'is_required' => 1,
            'is_unique' => 0,
            'is_visible' => 1,
            'is_system' => 0,
            'input_filter' => 'trim',
            'validate_rules' => null,
            'wkv_id' => null,
            'wkv_required_field' => null,
            'wkv_show_in_front' => null,
            'wkv_attribute_status' => null,
            'wkv_attribute_used_for' => null,
        ];
        // customer: _website
        $attributes[] = [
            'attribute_id' => 0,
            'attribute_code' => '_website',
            'frontend_input' => 'text',
            'is_required' => 1,
            'is_unique' => 0,
            'is_visible' => 1,
            'is_system' => 0,
            'input_filter' => 'trim',
            'validate_rules' => null,
            'wkv_id' => null,
            'wkv_required_field' => null,
            'wkv_show_in_front' => null,
            'wkv_attribute_status' => null,
            'wkv_attribute_used_for' => null,
        ];
        // customer: _store
        $attributes[] = [
            'attribute_id' => 0,
            'attribute_code' => '_store',
            'frontend_input' => 'text',
            'is_required' => 1,
            'is_unique' => 0,
            'is_visible' => 1,
            'is_system' => 0,
            'input_filter' => 'trim',
            'validate_rules' => null,
            'wkv_id' => null,
            'wkv_required_field' => null,
            'wkv_show_in_front' => null,
            'wkv_attribute_status' => null,
            'wkv_attribute_used_for' => null,
        ];

        return $attributes;
    }

    public function _getAttrGroup(){
        $_data = $this->_getAttrCustomerImport();
        $_attributes = [];
        foreach ($_data as $item) {
            if($item['wkv_id'] == 'shop'){
                $_attributes['shop'][] = ['attribute_id' => $item['attribute_id'], 'attribute_code' => $item['attribute_code'], 'frontend_input' => $item['frontend_input'] ];
            }

            if($item['wkv_id'] > 0){
                $_attributes['seller'][] = ['attribute_id' => $item['attribute_id'], 'attribute_code' => $item['attribute_code'], 'frontend_input' => $item['frontend_input']];
            }

            if(!$item['wkv_id']){
                $_attributes['client'][] = ['attribute_id' => $item['attribute_id'], 'attribute_code' => $item['attribute_code'], 'frontend_input' => $item['frontend_input']];
            }
        }
        return $_attributes;
    }


    protected function setBooleanData($sellerData){
        $customerAttributeType = [0,1];
        $booleanAttributes = $this->vendorAttributeCollectionFactory->create()
                                ->getVendorAttributeCollection()
                                ->addFieldToFilter("frontend_input", "boolean")
                                ->addFieldToFilter("wk_attribute_status", 1)
                                ->addFieldToFilter("attribute_used_for", ["in" => $customerAttributeType]);

        if ($booleanAttributes->getSize()) {
            foreach ($booleanAttributes as $attribute) {
                $attributeCode = $attribute->getAttributeCode();
                $attributeValue = (boolean)$this->_getRequest()->getParam($attributeCode, false);
                $sellerData[$attributeCode] = $attributeValue;
            }
        }
        return $sellerData;
    }

    protected function uploadFileForAttribute($attributeCode){
        $path = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)
                         ->getAbsolutePath('vendorfiles/');
        $attributeType = $this->attributeFactory->create()
                            ->load($attributeCode, "attribute_code")
                            ->getFrontendInput();

        $allowedExtensions =  explode(',', $this->helper->getConfigData('allowede_'.$attributeType.'_extension'));
        $uploader = $this->fileUploaderFactory->create(['fileId' => $attributeCode]);
        $uploader->setAllowedExtensions($allowedExtensions);
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(true);
        $result = $uploader->save($path.$attributeType);
        return $result;
    }

    protected function _vldData($_data, $type){
        $_erros = ['error' => false, 'fields' => []];
        $_attrRequired = [];
        foreach ($this->getAttrFilter($type) as $key => $item) {
            if($key != 'website_id' && $key != 'store_id'){
                if($item['is_required'] == 1){
                    if(!isset($_data[$key])){
                        $_erros['fields'][] = $key.' is required.';
                        $_erros['error'] = true;
                    }elseif(strlen($_data[$key]) <= 0){
                        $_erros['fields'][] = $key.' is required.';
                        $_erros['error'] = true;
                    }
                }
            }
        }
        return $_erros;
    }

    protected function getAttrFilter($type){
        $_attributes = [];
        $data = $this->_getOptionsAttributes();
        foreach ($data as $key => $item) {
            if(isset($item['group']) && $item['group'] == $type){
                $_attributes[$key] = $data[$key];
            }
        }
        return $_attributes;
    }

}
?>
