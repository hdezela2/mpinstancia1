<?php
namespace Cf\Imports\Model\Import;

use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;

class Seller extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity{
    const ID = 'email';
    protected $_permanentAttributes = [self::ID];
    protected $needColumnCheck = true;
    protected $validColumnNames = [];
    protected $masterAttributeCode = 'email';
    protected $logInHistory = true;
    protected $_validators = [];
    protected $_connection;
    protected $_resource;
    protected $_helperTodo;
    private $serializer;
    protected $_bunchSize;
    protected $string;
    protected $_uniqueAttributes = [];

    /**
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Cf\Imports\Helper\Todo $todoHelper
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_helperTodo = $todoHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->_resource = $resource;
        $this->string = $string;
        $this->_connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
    }


    public function getValidColumnNames() {
        return $this->validColumnNames;
    }

    /**
     * Entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode() {
        return 'imports_seller';
    }

    /**
     * Validate data
     *
     * @return ProcessingErrorAggregatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validateData(){
        if (!$this->_dataValidated) {
            $this->getErrorAggregator()->clear();
            $absentColumns = array_diff($this->_permanentAttributes, $this->getSource()->getColNames());
            $this->addErrors('Column code does not exist: '.json_encode($absentColumns), $absentColumns);
            $columnNumber = 0;
            $emptyHeaderColumns = [];
            $invalidColumns = [];
            $invalidAttributes = [];
            foreach ($this->getSource()->getColNames() as $columnName) {
                $columnNumber++;
                if (!$this->isAttributeParticular($columnName)) {
                    if (trim($columnName) == '') {
                        $emptyHeaderColumns[] = $columnNumber;
                    } elseif (!preg_match('/^[a-z_][a-z0-9_]*$/', $columnName)) {
                        $invalidColumns[] = $columnName;
                    /*} elseif ($this->needColumnCheck && !in_array($columnName, $this->getValidColumnNames())) {*/
                    } elseif ($this->needColumnCheck && !in_array($columnName, $this->_helperTodo->_getValidColumnNames())) {
                        $invalidAttributes[] = $columnName;
                    }
                }
            }
            $this->addErrors('Invalid Attribute : '.json_encode($invalidAttributes), $invalidAttributes);
            $this->addErrors('Empty column of the header: '.json_encode($emptyHeaderColumns), $emptyHeaderColumns);
            $this->addErrors('Invalid Column Name: '.json_encode($invalidColumns), $invalidColumns);

            if (!$this->getErrorAggregator()->getErrorsCount()) {
                $this->_saveValidatedBunches();
                $this->_dataValidated = true;
            }
        }
        return $this->getErrorAggregator();
    }


    /**
     * Add error with corresponding current data source row number.
     *
     * @param string $errorCode Error code or simply column name
     * @param int $errorRowNum Row number.
     * @param string $colName OPTIONAL Column name.
     * @param string $errorMessage OPTIONAL Column name.
     * @param string $errorLevel
     * @param string $errorDescription
     * @return $this
     */
    public function addRowError(
        $errorCode = null,
        $errorRowNum = null,
        $colName = null,
        $errorMessage = null,
        $errorLevel = ProcessingError::ERROR_LEVEL_CRITICAL,
        $errorDescription = null
    ) {
        $errorCode = (string)$errorCode;
        $this->getErrorAggregator()->addError(
            $errorCode,
            $errorLevel,
            $errorRowNum,
            $colName,
            $errorMessage,
            $errorDescription
        );

        return $this;
    }


    /**
     * Validate data rows and save bunches to DB
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _saveValidatedBunches(){
        $source = $this->getSource();
        $bunchRows = [];
        $startNewBunch = false;
        $source->rewind();
        $this->_dataSourceModel->cleanBunches();
        $masterAttributeCode = $this->getMasterAttributeCode();
        $_options = $this->_helperTodo->_getOptionsAttributes();
        $rowData = [];
        while ($source->valid() || count($bunchRows) || isset($entityGroup)) {
            if ($startNewBunch || !$source->valid()) {
                /* If the end approached add last validated entity group to the bunch */
                if (!$source->valid() && isset($entityGroup)) {
                    foreach ($entityGroup as $key => $value) {
                        $bunchRows[$key] = $value;
                    }
                    unset($entityGroup);
                }

                $this->_dataSourceModel->saveBunch($this->getEntityTypeCode(), $this->getBehavior(), $bunchRows);

                $bunchRows = [];
                $startNewBunch = false;
            }



            if ($source->valid()) {
                $valid = true;
                try {
                    $rowData = $source->current();
                    foreach ($rowData as $attrName => $element) {
                        if (!mb_check_encoding($element, 'UTF-8')) {
                            $valid = false;
                            $this->addRowError(
                                'Invalid characters.',
                                $this->_processedRowsCount,
                                $attrName
                            );
                        }

                        /*add validation custom*/
                        if(isset($_options[$attrName])){
                           $this->isAttributeValid($attrName, $_options[$attrName], $rowData, $this->_processedRowsCount);
                        }

                    }
                } catch (\InvalidArgumentException $e) {
                    $valid = false;
                    $this->addRowError($e->getMessage(), $this->_processedRowsCount);
                }

                if (!$valid) {
                    $this->_processedRowsCount++;
                    $source->next();
                    continue;
                }

                if (isset($rowData[$masterAttributeCode]) && trim($rowData[$masterAttributeCode])) {
                    /* Add entity group that passed validation to bunch */
                    if (isset($entityGroup)) {
                        foreach ($entityGroup as $key => $value) {
                            $bunchRows[$key] = $value;
                        }
                        $productDataSize = strlen($this->getSerializer()->serialize($bunchRows));

                        /* Check if the new bunch should be started */
                        /*$isBunchSizeExceeded = ($this->_bunchSize > 0 && count($bunchRows) >= $this->_bunchSize);
                        $startNewBunch = $productDataSize >= $this->_maxDataSize || $isBunchSizeExceeded;*/
                    }

                    /* And start a new one */
                    $entityGroup = [];
                }

                if (isset($entityGroup) && $this->validateRow($rowData, $source->key())) {
                    /* Add row to entity group */
                    $entityGroup[$source->key()] = $this->_prepareRowForDb($rowData);
                } elseif (isset($entityGroup)) {
                    /* In case validation of one line of the group fails kill the entire group */
                    unset($entityGroup);
                }

                $this->_processedRowsCount++;
                $source->next();
            }
        }


        return $this;
    }

    /**
     * Get Serializer instance
     *
     * Workaround. Only way to implement dependency and not to break inherited child classes
     *
     * @return Json
     * @deprecated 100.2.0
     */
    private function getSerializer()
    {
        if (null === $this->serializer) {
            $this->serializer = ObjectManager::getInstance()->get(Json::class);
        }
        return $this->serializer;
    }

    /**
     * @return string the master attribute code to use in an import
     */
    public function getMasterAttributeCode()
    {
        return $this->masterAttributeCode;
    }


    /**
     * Change row data before saving in DB table
     *
     * @param array $rowData
     * @return array
     */
    protected function _prepareRowForDb(array $rowData)
    {
        /**
         * Convert all empty strings to null values, as
         * a) we don't use empty string in DB
         * b) empty strings instead of numeric values will product errors in Sql Server
         */
        foreach ($rowData as $key => $val) {
            if ($val === '') {
                $rowData[$key] = null;
            }
        }
        return $rowData;
    }


    /**
     * Row validation.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum) {
        $title = false;
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;

        if (!isset($rowData[self::ID]) || empty($rowData[self::ID])) {
            $this->addRowError('Is empty.', $rowNum);
            return false;
        }
        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }



    /**
     * Create advanced question data from raw data.
     *
     * @throws \Exception
     * @return bool Result of operation.
     */
    protected function _importData() {
        if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            //$this->deleteEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            $this->replaceEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $this->getBehavior()) {
            $this->saveEntity();
        }
        return true;
    }


    public function saveEntity() {
        $this->saveAndReplaceEntity();
        return $this;
    }

    public function replaceEntity() {
        $this->saveAndReplaceEntity();
        return $this;
    }

    public function deleteEntity() {
        $ids = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);
                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowId = $rowData[self::ID];
                    $ids[] = $rowId;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }
        if ($ids) {
            /**
             * Since we are not supposed to delete entities
             * and self::TABLE_ENTITY is undefined, I'm
             * commenting out the code below
             */
            //$this->deleteEntityFinish(array_unique($ids),self::TABLE_ENTITY);
        }
        return $this;
    }

    /**
     * Save and replace question
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function saveAndReplaceEntity() {
        $behavior = $this->getBehavior();
        $ids = [];
        $_headers = $this->_helperTodo->_getValidColumnNames();
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError('Is empty.', $rowNum);
                    continue;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }

                $rowId= $rowData[self::ID];
                $ids[] = $rowId;
                ##
                $_data = [];
                foreach ($_headers as $i => $value) {
                    if(isset($rowData[$value])){
                        $_data[$value] = $rowData[$value];
                    }
                }

                $entityList[$rowId][] = $_data;
            }



            if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                if ($ids) {
                    //if ($this->deleteEntityFinish(array_unique($ids), self::TABLE_ENTITY)) {
                        //$this->saveEntityFinish($entityList);
                    //}
                }
            } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                $this->saveEntityFinish($entityList);
            }

        }
        return $this;
    }

    /**
     * Save question
     *
     * @param array $priceData
     * @param string $table
     * @return $this
     */
    protected function saveEntityFinish(array $entityData) {
        if ($entityData) {
            $_attributesG = $this->_helperTodo->_getAttrGroup();
            $_attributesClient = $_attributesG['client'];
            $_attributesSeller = $_attributesG['seller'];
            $_attributesShop = $_attributesG['shop'];
            foreach ($entityData as $row) {
                foreach ($row as $key => $item) {
                    $_client = [];
                    foreach ($_attributesClient as $i => $_attr) {
                       if(isset($item[$_attr['attribute_code']])){
                            $_client[$_attr['attribute_code']] = $item[$_attr['attribute_code']];
                       }
                    }
                    #
                    $_seller = [];
                    foreach ($_attributesSeller as $i1 => $_attr) {
                        if(isset($item[$_attr['attribute_code']])){

                            if($_attr['frontend_input'] == 'select'){
                                $_isValidValue = false;
                                $_options = $this->_helperTodo->_getOptions($_attr['attribute_code']);

                                foreach ($_options as $_item) {
                                    if(strtolower($item[$_attr['attribute_code']]) ==  strtolower($_item['value'])){
                                        $_isValidValue = true;
                                        $_seller[$_attr['attribute_code']] = $_item['option_id'];
                                    }
                                }

                                if(!$_isValidValue){
                                    $_seller[$_attr['attribute_code']] = 1;
                                    $_outError = 'El valor ' . $item[$_attr['attribute_code']] . ' del atributo ' . $_attr['attribute_code'] . ' es inválido.';
                                    $this->addRowError($_outError);
                                }

                            }else{
                                $_seller[$_attr['attribute_code']] = $item[$_attr['attribute_code']];
                            }


                        }
                    }

                    #
                    $_shop = [];
                    foreach ($_attributesShop as $i1 => $_attr) {
                        if(isset($item[$_attr['attribute_code']])){
                            $_shop[$_attr['attribute_code']] = $item[$_attr['attribute_code']];
                        }
                    }
                    #
                    $idCustomer = $this->_helperTodo->_saveCustomer($_client);
                    if(!$idCustomer['error']){
                        $idSeller = $this->_helperTodo->_saveBecomeSeller($_shop, $idCustomer['return']);
                        if(!$idSeller['error']){
                            $sellerUpd = $this->_helperTodo->_saveSeller($_seller, $idCustomer['return']);
                            if(!$sellerUpd['error']){
                            }else{
                                $this->addRowError($sellerUpd['return'], $key, 'seller_attributes');
                            }
                        }else{
                            $this->addRowError($idSeller['return'], $key, 'shop_url');
                        }
                    }else{
                        $this->addRowError($idCustomer['return'], $key, 'email');
                    }
                }
            }
        }
        return $this;
    }

    protected function deleteEntityFinish(array $ids, $table) {
        if ($table && $ids) {
            try {
                $this->countItemsDeleted += $this->_connection->delete(
                    $this->_connection->getTableName($table),
                    $this->_connection->quoteInto('entity_id IN (?)', $ids)
                );
                return true;
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Check one attribute. Can be overridden in child.
     *
     * @param string $attrCode Attribute code
     * @param array $attrParams Attribute params
     * @param array $rowData Row data
     * @param int $rowNum
     * @return boolean
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function isAttributeValid($attrCode, array $options, array $rowData, $rowNum){
        switch ($options['type']) {
            case 'varchar':
                $val = $this->string->cleanString($rowData[$attrCode]);
                $valid = $this->string->strlen($val) < self::DB_MAX_VARCHAR_LENGTH;
                break;
            case 'decimal':
                $val = trim($rowData[$attrCode]);
                $valid = (double)$val == $val;
                break;
            case 'select':
                $valid = true;
                break;
                /*$valid = false;
                $_indexValue =  array_search($rowData[$attrCode], $options['options']);
                if($_indexValue):
                    $valid = isset($options['options'][$_indexValue]);
                endif;
                break;*/
            case 'multiselect':
                $valid = isset($options['options'][strtolower($rowData[$attrCode])]);
                break;
            case 'int':
                $val = trim($rowData[$attrCode]);
                $valid = (int)$val == $val;
                break;
            case 'datetime':
                $val = trim($rowData[$attrCode]);
                $valid = strtotime($val) !== false;
                break;
            case 'text':
                $val = $this->string->cleanString($rowData[$attrCode]);
                $valid = $this->string->strlen($val) < self::DB_MAX_TEXT_LENGTH;
                break;
            default:
                $valid = true;
                break;
        }

        if (!$valid) {
            $this->addRowError('Invalid Attribute Code: '.$attrCode, $rowNum, $attrCode);
        }

        if (isset($options['is_required']) &&
            $options['is_required'] == 1 &&
            strlen(trim($rowData[$attrCode])) <= 0) {
            $this->addRowError('The value '.$attrCode.' is required.', $rowNum, $attrCode);
        }

        if (isset($options['is_unique']) &&
            $options['is_unique'] == 1) {
            if (isset($this->_uniqueAttributes[$attrCode][$rowData[$attrCode]])) {
                $this->addRowError('Duplicate value: '.$attrCode, $rowNum, $attrCode);
                return false;
            }
            $this->_uniqueAttributes[$attrCode][$rowData[$attrCode]] = true;
        }

        return (bool)$valid;
    }


    /**
     * Returns attributes all values in label-value or value-value pairs form. Labels are lower-cased.
     *
     * @param \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute
     * @param array $indexValAttrs OPTIONAL Additional attributes' codes with index values.
     * @return array
     */
    public function getAttributeOptions(
        \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute,
        $indexValAttrs = []
    ) {
        $options = [];

        if ($attribute->usesSource()) {
            $indexValAttrs = array_merge($indexValAttrs, $this->_indexValueAttributes);
            $index = in_array($attribute->getAttributeCode(), $indexValAttrs) ? 'value' : 'label';
            $attribute->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID);

            try {
                foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                    $value = is_array($option['value']) ? $option['value'] : [$option];
                    foreach ($value as $innerOption) {
                        if (strlen($innerOption['value'])) {
                            $options[strtolower($innerOption[$index])] = $innerOption['value'];
                        }
                    }
                }
            } catch (\Exception $e) {
            }
        }
        return $options;
    }

}
