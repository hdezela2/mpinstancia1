<?php
namespace Cf\AssignProductsMass\Model\Import;

use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;

class AssignProducts extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity{
    const ID = 'email';
    const SKU = 'sku';
    const PRODUCT_TYPE = 'product_type';
    const SKU_ASSIGN = 'sku_assign';
    const QTY = 'qty';
    const IN_STOCK = 'in_stock';
    const PRICE = 'price';
    const CONFIGURABLE_VARIATIONS = 'configurable_variations';
    const BASE_PRICE = 'base_price';
    const SHIPPING_PRICE = 'shipping_price';
    const ASSEMBLY_PRICE = 'assembly_price';
    protected $_permanentAttributes = [self::ID];
    protected $needColumnCheck = true;
    protected $validColumnNames = [
        self::ID,
        self::SKU,
        self::PRODUCT_TYPE,
        self::SKU_ASSIGN,
        self::QTY,
        self::IN_STOCK,
        self::PRICE,
        self::CONFIGURABLE_VARIATIONS,
        self::BASE_PRICE,
        self::SHIPPING_PRICE,
        self::ASSEMBLY_PRICE
    ];
    protected $masterAttributeCode = self::ID;
    protected $logInHistory = true;
    protected $_validators = [];
    protected $_resource;
    protected $_helperTodo;
    private $serializer;
    protected $_bunchSize;
    protected $string;
    protected $_uniqueAttributes = [];
    protected $iterator;
    protected $entityAttribute;
    protected $productMapping = [];
    protected $customerMapping = [];
    protected $assignProductMapping = [];
    protected $assignProductData = [];
    //protected $marketplaceProductMapping = [];

    /**
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Cf\AssignProductsMass\Helper\Todo $todoHelper,
        \Magento\Framework\Model\ResourceModel\Iterator $iterator,
        \Magento\Eav\Model\Entity\AttributeFactory $entityAttribute
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_helperTodo = $todoHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->_resource = $resource;
        $this->string = $string;
        $this->errorAggregator = $errorAggregator;
        $this->iterator = $iterator;
        $this->entityAttribute = $entityAttribute;
    }


    public function getValidColumnNames() {
        return $this->validColumnNames;
    }

    /**
     * Entity type code getter.
     *
     * @return strings
     */
    public function getEntityTypeCode() {
        return 'assign_products';
    }

    /**
     * Validate data
     *
     * @return ProcessingErrorAggregatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validateData(){
        if (!$this->_dataValidated) {
            $this->getErrorAggregator()->clear();
            $absentColumns = array_diff($this->_permanentAttributes, $this->getSource()->getColNames());
            $this->addErrors('Column code does not exist: '.json_encode($absentColumns), $absentColumns);
            $columnNumber = 0;
            $emptyHeaderColumns = [];
            $invalidColumns = [];
            $invalidAttributes = [];
            foreach ($this->getSource()->getColNames() as $columnName) {
                $columnNumber++;
                if (!$this->isAttributeParticular($columnName)) {
                    if (trim($columnName) == '') {
                        $emptyHeaderColumns[] = $columnNumber;
                    } elseif (!preg_match('/^[a-z][a-z0-9_]*$/', $columnName)) {
                        $invalidColumns[] = $columnName;
                    } elseif ($this->needColumnCheck && !in_array($columnName, $this->getValidColumnNames())) {
                    /*} elseif ($this->needColumnCheck && !in_array($columnName, $this->_helperTodo->_getValidColumnNames())) {*/
                        $invalidAttributes[] = $columnName;
                    }
                }
            }
            $this->addErrors('Invalid Attribute : '.json_encode($invalidAttributes), $invalidAttributes);
            $this->addErrors('Empty column of the header: '.json_encode($emptyHeaderColumns), $emptyHeaderColumns);
            $this->addErrors('Invalid Column Name: '.json_encode($invalidColumns), $invalidColumns);

            if (!$this->getErrorAggregator()->getErrorsCount()) {
                $this->_saveValidatedBunches();
                $this->_dataValidated = true;
            }
        }
        return $this->getErrorAggregator();
    }


    /**
     * Add error with corresponding current data source row number.
     *
     * @param string $errorCode Error code or simply column name
     * @param int $errorRowNum Row number.
     * @param string $colName OPTIONAL Column name.
     * @param string $errorMessage OPTIONAL Column name.
     * @param string $errorLevel
     * @param string $errorDescription
     * @return $this
     */
    public function addRowError(
        $errorCode = null,
        $errorRowNum = null,
        $colName = null,
        $errorMessage = null,
        $errorLevel = ProcessingError::ERROR_LEVEL_CRITICAL,
        $errorDescription = null
    ) {
        $errorCode = (string)$errorCode;
        $this->getErrorAggregator()->addError(
            $errorCode,
            $errorLevel,
            $errorRowNum,
            $colName,
            $errorMessage,
            $errorDescription
        );

        return $this;
    }


    /**
     * Validate data rows and save bunches to DB
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _saveValidatedBunches(){
        $source = $this->getSource();
        $bunchRows = [];
        $startNewBunch = false;
        $source->rewind();
        $this->_dataSourceModel->cleanBunches();
        $masterAttributeCode = $this->getMasterAttributeCode();
        $rowData = [];
        //$_options = $this->_helperTodo->_getOptionsAttributes();
        while ($source->valid() || count($bunchRows) || isset($entityGroup)) {
            if ($startNewBunch || !$source->valid()) {
                /* If the end approached add last validated entity group to the bunch */
                if (!$source->valid() && isset($entityGroup)) {
                    foreach ($entityGroup as $key => $value) {
                        $bunchRows[$key] = $value;
                    }
                    unset($entityGroup);
                }

                $this->_dataSourceModel->saveBunch($this->getEntityTypeCode(), $this->getBehavior(), $bunchRows);

                $bunchRows = [];
                $startNewBunch = false;
            }



            if ($source->valid()) {
                $valid = true;
                try {
                    $rowData = $source->current();
                    foreach ($rowData as $attrName => $element) {
                        if (!mb_check_encoding($element, 'UTF-8')) {
                            $valid = false;
                            $this->addRowError(
                                'Invalid characters.',
                                $this->_processedRowsCount,
                                $attrName
                            );
                        }

                        /*add validation custom*/
                        /*if(isset($_options[$attrName])){
                           $this->isAttributeValid($attrName, $_options[$attrName], $rowData, $this->_processedRowsCount);
                        }*/

                    }
                } catch (\InvalidArgumentException $e) {
                    $valid = false;
                    $this->addRowError($e->getMessage(), $this->_processedRowsCount);
                }

                if (!$valid) {
                    $this->_processedRowsCount++;
                    $source->next();
                    continue;
                }

                if (isset($rowData[$masterAttributeCode]) && trim($rowData[$masterAttributeCode])) {
                    /* Add entity group that passed validation to bunch */
                    if (isset($entityGroup)) {
                        foreach ($entityGroup as $key => $value) {
                            $bunchRows[$key] = $value;
                        }
                        $productDataSize = strlen($this->getSerializer()->serialize($bunchRows));

                        /* Check if the new bunch should be started */
                        /*$isBunchSizeExceeded = ($this->_bunchSize > 0 && count($bunchRows) >= $this->_bunchSize);
                        $startNewBunch = $productDataSize >= $this->_maxDataSize || $isBunchSizeExceeded;*/
                    }

                    /* And start a new one */
                    $entityGroup = [];
                }

                if (isset($entityGroup) && $this->validateRow($rowData, $source->key())) {
                    /* Add row to entity group */
                    $entityGroup[$source->key()] = $this->_prepareRowForDb($rowData);
                } elseif (isset($entityGroup)) {
                    /* In case validation of one line of the group fails kill the entire group */
                    unset($entityGroup);
                }

                $this->_processedRowsCount++;
                $source->next();
            }
        }


        return $this;
    }

    /**
     * Get Serializer instance
     *
     * Workaround. Only way to implement dependency and not to break inherited child classes
     *
     * @return Json
     * @deprecated 100.2.0
     */
    private function getSerializer()
    {
        if (null === $this->serializer) {
            $this->serializer = ObjectManager::getInstance()->get(Json::class);
        }
        return $this->serializer;
    }

    /**
     * @return string the master attribute code to use in an import
     */
    public function getMasterAttributeCode()
    {
        return $this->masterAttributeCode;
    }


    /**
     * Change row data before saving in DB table
     *
     * @param array $rowData
     * @return array
     */
    protected function _prepareRowForDb(array $rowData)
    {
        /**
         * Convert all empty strings to null values, as
         * a) we don't use empty string in DB
         * b) empty strings instead of numeric values will product errors in Sql Server
         */
        foreach ($rowData as $key => $val) {
            if ($val === '') {
                $rowData[$key] = null;
            }
        }
        return $rowData;
    }


    /**
     * Row validation.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum) {
        $title = false;
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;

        if (!isset($rowData[self::ID]) || empty($rowData[self::ID])) {
            $this->addRowError('Is empty.', $rowNum);
            return false;
        }
        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }



    /**
     * Create advanced question data from raw data.
     *
     * @throws \Exception
     * @return bool Result of operation.
     */
    protected function _importData() {
        if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            //$this->deleteEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            $this->replaceEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $this->getBehavior()) {
            $this->saveEntity();
        }
        return true;
    }


    public function saveEntity() {
        $this->saveAndReplaceEntity();
        return $this;
    }

    public function replaceEntity() {
        $this->saveAndReplaceEntity();
        return $this;
    }

    public function deleteEntity() {
        /*$ids = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);
                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowId = $rowData[self::ID];
                    $ids[] = $rowId;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }
        if ($ids) {
            $this->deleteEntityFinish(array_unique($ids),self::TABLE_ENTITY);
        }
        return $this;*/
    }

    /**
     * Save and replace question
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function saveAndReplaceEntity() {
        $behavior = $this->getBehavior();
        $ids = [];
        //$_headers = $this->_helperTodo->_getValidColumnNames();
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError('Is empty.', $rowNum);
                    continue;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }

                //BASE_PRICE & SHIPPING_PRICE - convenio Emergencia
                //BASE_PRICE & ASSEMBLY_PRICE - convenio Mobiliario

                $rowTtile= $rowData[self::ID];
                $listTitle[] = $rowTtile;
                $entityList[$rowTtile][] = [
                    self::ID => $rowData[self::ID],
                    self::SKU => $rowData[self::SKU],
                    self::PRODUCT_TYPE => $rowData[self::PRODUCT_TYPE],
                    self::SKU_ASSIGN => $rowData[self::SKU_ASSIGN],
                    self::QTY => $rowData[self::QTY],
                    self::IN_STOCK => $rowData[self::IN_STOCK],
                    self::PRICE => $rowData[self::PRICE],
                    self::CONFIGURABLE_VARIATIONS => $rowData[self::CONFIGURABLE_VARIATIONS],
                    self::BASE_PRICE => isset($rowData[self::BASE_PRICE]) ? $rowData[self::BASE_PRICE] : null,
                    self::SHIPPING_PRICE => isset($rowData[self::SHIPPING_PRICE]) ? $rowData[self::SHIPPING_PRICE] : null,
                    self::ASSEMBLY_PRICE => isset($rowData[self::ASSEMBLY_PRICE]) ? $rowData[self::ASSEMBLY_PRICE] : null
                ];
            }

            if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                if ($ids) {
                    //if ($this->deleteEntityFinish(array_unique($ids), self::TABLE_ENTITY)) {
                        //$this->saveEntityFinish($entityList);
                    //}
                }
            } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                $this->saveEntityFinish($entityList);
            }

        }
        return $this;
    }

    /**
     * Save question
     *
     * @param array $priceData
     * @param string $table
     * @return $this
     */
    protected function saveEntityFinish(array $entityData) {
        if ($entityData) {
            $this->getProductMapping($entityData);
            $this->getCustomerMapping($entityData);
            $this->getAssignProductMapping();
            // $this->getMarketplaceProductMapping();

            foreach ($entityData as $row) {
                foreach ($row as $key => $item) {
                    $result = $this->_helperTodo->_saveAssign(
                        $item,
                        $this->productMapping,
                        $this->customerMapping,
                        $this->assignProductMapping,
                        true
                    );
                    if(!$result['error']){
                    }else{
                        $this->addRowError($result['return'], $key, 'email');
                    }
                }
            }
        }
        return $this;
    }

    protected function deleteEntityFinish(array $ids, $table) {
        /*if ($table && $ids) {
            try {
                $this->countItemsDeleted += $this->_connection->delete(
                    $this->_connection->getTableName($table),
                    $this->_connection->quoteInto('entity_id IN (?)', $ids)
                );
                return true;
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }*/
    }

    /**
     * Check one attribute. Can be overridden in child.
     *
     * @param string $attrCode Attribute code
     * @param array $attrParams Attribute params
     * @param array $rowData Row data
     * @param int $rowNum
     * @return boolean
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function isAttributeValid($attrCode, array $options, array $rowData, $rowNum){
        switch ($options['type']) {
            case 'varchar':
                $val = $this->string->cleanString($rowData[$attrCode]);
                $valid = $this->string->strlen($val) < self::DB_MAX_VARCHAR_LENGTH;
                break;
            case 'decimal':
                $val = trim($rowData[$attrCode]);
                $valid = (double)$val == $val;
                break;
            case 'select':
            case 'multiselect':
                $valid = isset($options['options'][strtolower($rowData[$attrCode])]);
                break;
            case 'int':
                $val = trim($rowData[$attrCode]);
                $valid = (int)$val == $val;
                break;
            case 'datetime':
                $val = trim($rowData[$attrCode]);
                $valid = strtotime($val) !== false;
                break;
            case 'text':
                $val = $this->string->cleanString($rowData[$attrCode]);
                $valid = $this->string->strlen($val) < self::DB_MAX_TEXT_LENGTH;
                break;
            default:
                $valid = true;
                break;
        }

        if (!$valid) {
            $this->addRowError('Invalid Attribute Code: '.$attrCode, $rowNum, $attrCode);
        }

        if (isset($options['is_required']) &&
            $options['is_required'] == 1 &&
            strlen(trim($rowData[$attrCode])) <= 0) {
            $this->addRowError('The value '.$attrCode.' is required.', $rowNum, $attrCode);
        }

        if (isset($options['is_unique']) &&
            $options['is_unique'] == 1) {
            if (isset($this->_uniqueAttributes[$attrCode][$rowData[$attrCode]])) {
                $this->addRowError('Duplicate value: '.$attrCode, $rowNum, $attrCode);
                return false;
            }
            $this->_uniqueAttributes[$attrCode][$rowData[$attrCode]] = true;
        }

        return (bool)$valid;
    }


    /**
     * Returns attributes all values in label-value or value-value pairs form. Labels are lower-cased.
     *
     * @param \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute
     * @param array $indexValAttrs OPTIONAL Additional attributes' codes with index values.
     * @return array
     */
    public function getAttributeOptions(
        \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute,
        $indexValAttrs = []
    ) {
        $options = [];

        if ($attribute->usesSource()) {
            $indexValAttrs = array_merge($indexValAttrs, $this->_indexValueAttributes);
            $index = in_array($attribute->getAttributeCode(), $indexValAttrs) ? 'value' : 'label';
            $attribute->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID);

            try {
                foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                    $value = is_array($option['value']) ? $option['value'] : [$option];
                    foreach ($value as $innerOption) {
                        if (strlen($innerOption['value'])) {
                            $options[strtolower($innerOption[$index])] = $innerOption['value'];
                        }
                    }
                }
            } catch (\Exception $e) {
            }
        }
        return $options;
    }

    public function getProductMapping($entityData)
    {
        $entityData = array_values($entityData);

        $skus = [];
        $configurableVariations = [];
        foreach ($entityData as $value) {
            $skus[] = array_column($value, 'sku');
            $configurableVariations[] = array_column($value, 'configurable_variations');
        }

        $skus = array_merge(...$skus);
        $configurableVariations = array_merge(...$configurableVariations);
        $configurableVariations = array_filter($configurableVariations);

        if ($configurableVariations) {
            //sku=1586853,qty=100,price=20000 | sku=1586854,qty=100,price=20000
            //sku=1586855,qty=50,price=20000 | sku=1586856,qty=40,price=20000
            foreach ($configurableVariations as $variation) {
                $__products = explode('|', $variation);
                foreach ($__products as $key => $value) {
                    $_item = explode(',', $value);
                    foreach ($_item as $key1 => $value1) {
                        $__item = explode('=', $value1);
                        if (trim($__item[0]) == 'sku') {
                            $skus[] = trim($__item[1]);
                        }
                    }
                }
            }

        }

        if ($skus) {
            $connection = $this->_resource->getConnection();
            $cpe = $this->_resource->getTableName('catalog_product_entity');
            $voucherAttribute = $this->entityAttribute->create()->loadByCode(\Magento\Catalog\Model\Product::ENTITY, 'voucher_product_type');
            $isquimicoAttribute = $this->entityAttribute->create()->loadByCode(\Magento\Catalog\Model\Product::ENTITY, 'is_quimico');
            $select = $connection->select()
                ->from(['e' => $cpe], ['entity_id', 'sku', 'type_id', 'row_id'])
                ->joinLeft(
                    ["vpt" => $voucherAttribute->getBackend()->getTable()],
                    "vpt.row_id=e.row_id AND vpt.attribute_id={$voucherAttribute->getId()}",
                    ['voucher_product_type' => 'vpt.value']
                )
                ->joinLeft(
                    ["iq" => $isquimicoAttribute->getBackend()->getTable()],
                    "iq.row_id=e.row_id AND iq.attribute_id={$isquimicoAttribute->getId()}",
                    ['is_quimico' => 'iq.value']
                )
                ->where('sku IN(?)', $skus);
            $this->iterator->walk(
                $select,
                [[$this, 'productMappingCallback']]
            );
        }
    }

    public function productMappingCallback($args)
    {
        $row = $args['row'];
        $this->productMapping[$row['sku']] = [
            'sku' => $row['sku'],
            'row_id' => $row['row_id'],
            'product_id' => $row['entity_id'],
            'product_type' => $row['type_id'],
            'voucher_product_type' => $row['voucher_product_type'],
            'is_quimico' => $row['is_quimico']
        ];
        return $this;
    }

    public function getCustomerMapping($entityData)
    {
        $emailsArr = array_keys($entityData);
        if (count($emailsArr)) {
            $connection = $this->_resource->getConnection();
            $ce = $this->_resource->getTableName('customer_entity');
            $select = $connection->select()
                ->from(['e' => $ce], ['email', 'entity_id', 'website_id'])
                ->where('email IN(?)', $emailsArr);
            $this->iterator->walk(
                $select,
                [[$this, 'customerMappingCallback']]
            );
        }
        return $this;
    }

    public function customerMappingCallback($args)
    {
        $row = $args['row'];
        $this->customerMapping[$row['email']] = [
            'customer_id' => $row['entity_id'],
            'customer_website' => $row['website_id']
        ];
        return $this;
    }

    public function getAssignProductMapping()
    {
        $productIds = array_column($this->productMapping, 'product_id');
        $customerIds = array_column($this->customerMapping, 'customer_id');

        if ($productIds && $customerIds) {
            $connection = $this->_resource->getConnection();
            $mai = $this->_resource->getTableName('marketplace_assignproduct_items');
            $select = $connection->select()
                ->from(['e' => $mai], ['product_id', 'seller_id', 'id', 'base_price', 'price', 'shipping_price', 'assembly_price', 'image', 'qty', 'status'])
                ->where('product_id IN(?)', $productIds)
                ->where('seller_id IN(?)', $customerIds);
            $this->iterator->walk(
                $select,
                [[$this, 'assignProductMappingCallback']]
            );
        }
    }

    public function assignProductMappingCallback($args)
    {
        $row = $args['row'];
        $this->assignProductMapping[$row['product_id']][$row['seller_id']] = $row['id'];
        $this->assignProductData[$row['id']] = [
            'price' => $row['price'],
            'base_price' => $row['base_price'],
            'shipping_price' => $row['shipping_price'],
            'assembly_price' => $row['assembly_price'],
            'qty' => $row['qty'],
            'status' => $row['status'],
            'image' => $row['image']
        ];
        return $this;
    }

//    public function getMarketplaceProductMapping()
//    {
//        $productIds = array_column($this->productMapping, 'product_id');
//
//        if ($productIds) {
//            $connection = $this->_resource->getConnection();
//            $mp = $this->_resource->getTableName('marketplace_product');
//            $select = $connection->select()
//                ->from(['e' => $mp], ['mageproduct_id', 'seller_id'])
//                ->where('mageproduct_id IN(?)', $productIds);
//            $this->iterator->walk(
//                $select,
//                [[$this, 'marketplaceProductMappingCallback']]
//            );
//        }
//    }
//
//    public function marketplaceProductMappingCallback($args)
//    {
//        $row = $args['row'];
//        $this->marketplaceProductMapping[$row['mageproduct_id']] = $row['seller_id'];
//        return $this;
//    }
}
