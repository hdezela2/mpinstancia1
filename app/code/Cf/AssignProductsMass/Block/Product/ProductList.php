<?php
namespace Cf\AssignProductsMass\Block\Product;
class ProductList{
    public function beforeToHtml(\Webkul\MpAssignProduct\Block\Product\ProductList $block)
    {
        $block->setTemplate('Cf_AssignProductsMass::productlist_view.phtml');
    }
}