<?php
namespace Cf\AssignProductsMass\Helper;

use Intellicore\EmergenciasRenewal\Constants as ConstantsEmergency202109;

class Todo extends \Magento\Framework\App\Helper\AbstractHelper{
    protected $_storeManager;
    protected $_connection;
    protected $_hashPasswod;
    protected $_customer;
    protected $_customerFactory;
    protected $_sellerFactory;
    protected $_sellerCollectionFactory;
    protected $_date;
    protected $_seller;
    protected $vendorAttributeCollectionFactory;
    protected $helper;
    protected $attributeFactory;
    protected $_customerRepository;
    protected $_customerMapper;
    protected $_dataObjectHelper;
    protected $_assignHelper;
    protected $_websiteCollectionFactory;
    protected $_websites;
    protected $_messageManager;


    /**
     * @param Context     $context
     * @param storeManager     $storeManager
     * @param messageManager $messageManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Customer\Api\Data\CustomerInterfaceFactory $customer,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Encryption\EncryptorInterface $hashPasswod,
        \Webkul\Marketplace\Model\SellerFactory $sellerFactory,
        \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $sellerCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\Marketplace\Model\Seller $seller,
        \Webkul\MpVendorAttributeManager\Model\ResourceModel\VendorAttribute\CollectionFactory $vendorAttributeCollectionFactory,
        \Webkul\MpVendorAttributeManager\Helper\Data $helper,
        \Magento\Customer\Model\AttributeFactory $attributeFactory,
        \Magento\Customer\Model\Customer\Mapper $customerMapper,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Webkul\MpAssignProduct\Helper\Data $helperAssign,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Store\Model\ResourceModel\Website\CollectionFactory $websiteCollectionFactory
    ){
        $this->_storeManager = $storeManager;
        $this->_assignHelper = $helperAssign;
        $this->_customer = $customer;
        $this->_customerFactory = $customerFactory;
        $this->_date = $date;
        $this->_hashPasswod = $hashPasswod;
        $this->_sellerFactory = $sellerFactory;
        $this->_seller = $seller;
        $this->_sellerCollectionFactory = $sellerCollectionFactory;
        $this->helper = $helper;
        $this->attributeFactory = $attributeFactory;
        $this->_connection = $resourceConnection->getConnection();
        $this->_customerRepository = $customerRepository;
        $this->_customerMapper = $customerMapper;
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_messageManager = $messageManager;
        $this->_websiteCollectionFactory = $websiteCollectionFactory;
        $this->_setWebsites();
        parent::__construct($context);
    }

    public function _saveAssign(
        $data,
        $productMapping,
        $customerMapping,
        $assignProductMapping,
        $isFromApi = false
    ) {
        $helper = $this->_assignHelper;
        $data['image'] = '';

        if (array_key_exists(trim($data['sku']), $productMapping)) {
            $productId = $productMapping[trim($data['sku'])]['product_id'];
            $data['description'] = $data['sku_assign'];
            $data['product_id'] = $productId;
            $data['product_condition'] = 1;
            $sellerId = array_key_exists($data['email'], $customerMapping) ? $customerMapping[$data['email']]['customer_id'] : false;
            $websiteId = array_key_exists($data['email'], $customerMapping) ? $customerMapping[$data['email']]['customer_website'] : false;

            if (isset($this->_websites[$websiteId]) && in_array($this->_websites[$websiteId], [\Summa\EmergenciasSetUp\Helper\Data::WEBSITE_CODE])) {
                if ($data['shipping_price'] == null && $data['base_price'] == null) {
                    return ['error' => true, 'return' => __('Invalid rows "base_price" and "shipping_price"')];
                }

                $finalPrice = null;
                if ($data['shipping_price'] && $data['shipping_price'] > 0) {
                    if ($data['base_price'] && $data['base_price'] > 0) {
                        $finalPrice = $data['base_price'] + $data['shipping_price'];
                    } else {
                        $finalPrice = $data['shipping_price'];
                    }
                }
                if($finalPrice) {
                    $data['price'] = $finalPrice;
                }
            }elseif (isset($this->_websites[$websiteId]) && $this->_websites[$websiteId] === \Summa\MobiliarioSetUp\Helper\Data::WEBSITE_CODE) {
                if ($data['assembly_price'] == null && $data['base_price'] == null) {
                    return ['error' => true, 'return' => __('Invalid rows "base_price" and "assembly_price"')];
                }

                $finalPrice = null;
                if ($data['base_price'] && $data['base_price'] > 0) {
                    if ($data['assembly_price'] && $data['assembly_price'] > 0) {
                        $finalPrice = $data['base_price'] + $data['assembly_price'];
                    } else {
                        $finalPrice = $data['base_price'];
                    }
                }
                if($finalPrice) {
                    $data['price'] = $finalPrice;
                }
            }

            if(!isset($sellerId) || empty($sellerId) || $sellerId == false){
                return ['error' => true, 'return' => __('Vendor %1 does not exist', $data['email'])];
            }

            $productType = $productMapping[trim($data['sku'])]['product_type'];

            // unset($data['sku']);
            unset($data['email']);
            unset($data['sku_assign']);
            unset($data['product_type']);

            if ($productType == 'configurable') {
                //sku=1586853,qty=100,price=20000 |
                //sku=1586854,qty=100,price=20000
                $__products = explode('|', $data['configurable_variations']);
                $_products = [];
                foreach ($__products as $key => $value) {
                    $_item = explode(',', $value);
                    $_product = [];
                    foreach ($_item as $key => $value1) {
                        $__item = explode('=', $value1);
                        if (trim($__item[0]) == 'sku' ||
                            trim($__item[0]) == 'qty' ||
                            trim($__item[0]) == 'price'||
                            trim($__item[0]) == 'status'||
                            trim($__item[0]) == 'base_price') {
                            if (trim($__item[0]) == 'sku') {
                                $_product['id'] = trim($__item[1]);
                            } else {
                                $_product[trim($__item[0])] = trim($__item[1]);
                            }
                        }
                    }

                    if (count($_product) == 3 || count($_product) == 4 || count($_product) == 5) {
                        $productSku = str_replace("\xef\xbb\xbf", '', $_product['id']);
                        $indexProduct = $productMapping[$productSku]['product_id'];
                        $_products[$indexProduct] = $_product;
                    }
                }

                if (count($_products) > 0) {
                    $data['products'] = $_products;
                }
            }

            unset($data['configurable_variations']);
            $assign_id = false;
            if (array_key_exists($productId, $assignProductMapping)) {
                if (array_key_exists($sellerId, $assignProductMapping[$productId])) {
                    $assign_id = $assignProductMapping[$productId][$sellerId];
                }
            }

            if ($assign_id) {
                $data['assign_id'] = $assign_id;
            }

            $result = $helper->validateData($data, $productType, false, true);
            if ($result['error']) {
                return ['error' => true, 'return' => __($result['msg'])];
            }

            if (array_key_exists('assign_id', $data)) {
                $flag = 1;
                $data['del'] = 0;
            } else {
                $flag = 0;
                $data['del'] = 0;
            }
            /* Commented out because there is no information of WHY they modified the parameters on this function
            * Why they pass $productMapping, $assignProductData, and $marketplaceProductMapping to the processAssignProduct() function??
            $result = $helper->processAssignProduct($data, $productType, $flag, $sellerId, $data['in_stock'], $productMapping, $assignProductData, $marketplaceProductMapping);
            */
            /** @var $helper \Formax\AssignProduct\Helper\MpAssignProduct */
            $result = $helper->processAssignProduct($data, $productType, $flag, $sellerId, $data['in_stock'], $isFromApi);
            if ($result['assign_id'] > 0) {
                $helper->processProductStatus($result);
                return ['error' => false, 'return' => __('Product is saved successfully.')];
            } else {
                return ['error' => true, 'return' => __('There was some error while processing your request.')];
            }

        } else {
            return ['error' => true, 'return' => __('Product with SKU %1 does not exist', $data['sku'])];
        }
    }

    protected function _getAssignId($pId, $sellerId)
    {
        $sql = "SELECT id FROM marketplace_assignproduct_items
        WHERE product_id = " . (int)$pId . " AND seller_id = " . (int)$sellerId;
        $_result = $this->_connection->fetchOne($sql);

        if ($_result) {
            return $_result;
        } else {
            return false;
        }
    }

    protected function _getSellerId($email)
    {
        $email = $this->_connection->quote($email);
        $_result =  $this->_connection->fetchOne("SELECT entity_id FROM customer_entity WHERE email = " . $email);
        if ($_result) {
            return $_result;
        } else {
            return false;
        }
    }

    /**
     * Check if SKU exists.
     *
     * @return boolean
     */
    protected function _SkuExists($sku)
    {
        $sku = $this->_connection->quote($sku);
        $_result =  $this->_connection->fetchAll("SELECT * FROM catalog_product_entity WHERE sku = " . $sku);
        if ($_result) {
            return true;
        } else {
            return false;
        }
    }

    protected function _getProductBySku($sku)
    {
        $sku = $this->_connection->quote($sku);
        $_result =  $this->_connection->fetchAll("SELECT * FROM catalog_product_entity WHERE sku = " . $sku);
        if ($_result) {
            return $_result[0]['entity_id'];
        } else {
            return false;
        }
    }

    /**
     * Retrive product ID by SKU
     *
     * @param string $sku
     * @return mixed int/bool
     */
    protected function _getProductIdBySku($sku)
    {
        $sku = $this->_connection->quote($sku);
        if (!empty($sku)) {
            return $this->_connection->fetchOne('SELECT entity_id FROM catalog_product_entity WHERE sku = ' . $sku);
        }

        return false;
    }

    protected function _getCustomerByEmail($email)
    {
        $email = $this->_connection->quote($email);
        $_result =  $this->_connection->fetchAll("SELECT * FROM customer_entity WHERE email = " . $email);
        if ($_result) {
            return $_result[0]['entity_id'];
        } else {
            return false;
        }
    }

    protected function _getCustomerWebsiteIdByEmail($email){
        $_result =  $this->_connection->fetchAll("SELECT * FROM customer_entity WHERE email = '$email'");
        if($_result){
            return $_result[0]['website_id'];
        }else{
            return false;
        }
    }

    protected function _setWebsites()
    {
        if (!$this->_websites) {
            $websites = $this->_websiteCollectionFactory->create()->getItems();
            foreach ($websites as $website) {
                $this->_websites[$website->getId()] = $website->getCode();
            }
        }
    }

    protected function _vldEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }
}
