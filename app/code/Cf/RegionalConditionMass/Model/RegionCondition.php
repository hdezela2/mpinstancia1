<?php 
namespace Cf\RegionalConditionMass\Model;
class RegionCondition extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface{
	
	const CACHE_TAG = 'cf_regional_conditions';
	protected $_cacheTag = 'cf_regional_conditions';
	protected $_eventPrefix = 'cf_regional_conditions';

	public function _construct(){
		$this->_init("Cf\RegionalConditionMass\Model\ResourceModel\RegionCondition");
	}


	public function getIdentities(){
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues(){
		$values = [];
		return $values;
	}
}
 ?>