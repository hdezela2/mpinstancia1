<?php 
namespace Cf\RegionalConditionMass\Model;
class RegionConditionSeller extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface{
	const CACHE_TAG = 'cf_regional_conditions_seller';
	protected $_cacheTag = 'cf_regional_conditions_seller';
	protected $_eventPrefix = 'cf_regional_conditions_seller';

	public function _construct(){
		$this->_init("Cf\RegionalConditionMass\Model\ResourceModel\RegionConditionSeller");
	}


	public function getIdentities(){
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues(){
		$values = [];
		return $values;
	}
}
 ?>