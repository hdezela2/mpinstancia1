<?php 
namespace Cf\RegionalConditionMass\Model\ResourceModel;
class RegionCondition extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb{
  
  public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
  ){
		parent::__construct($context);
  }

  public function _construct(){
     $this->_init("dccp_regional_condition","id");
  }
}
 ?>