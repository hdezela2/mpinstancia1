<?php 
namespace Cf\RegionalConditionMass\Model\ResourceModel;
class CollectionSeller extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	public function _construct(){
		$this->_init("Cf\RegionalConditionMass\Model\RegionConditionSeller","Cf\RegionalConditionMass\Model\ResourceModel\RegionConditionSeller");
	}
}
 ?>