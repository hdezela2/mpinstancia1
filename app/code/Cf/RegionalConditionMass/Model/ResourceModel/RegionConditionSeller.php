<?php 
namespace Cf\RegionalConditionMass\Model\ResourceModel;
class RegionConditionSeller extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb{
  
  public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
  ){
		parent::__construct($context);
  }

  public function _construct(){
     $this->_init("dccp_regional_condition_seller","id");
  }
  
}
 ?>