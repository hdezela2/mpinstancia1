<?php 
namespace Cf\RegionalConditionMass\Model\ResourceModel;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	public function _construct(){
		$this->_init("Cf\RegionalConditionMass\Model\RegionCondition","Cf\RegionalConditionMass\Model\ResourceModel\RegionCondition");
	}
}
 ?>