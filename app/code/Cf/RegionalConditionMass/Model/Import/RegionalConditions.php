<?php

namespace Cf\RegionalConditionMass\Model\Import;

use Exception;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;

class RegionalConditions extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity
{
    const REGION_ID = 'region_id';
    const CONTACT_NAME = 'contact_name';
    const CONTACT_PHONE = 'contact_phone';
    const CONTACT_EMAIL = 'contact_email';
    const MAXIMUM_AMOUNT = 'maximum_amount';
    const MAXIMUM_QTY = 'maximum_qty';
    const ENABLE_STOREPICKUP = 'enable_storepickup';
    const DAYS_STOREPICKUP = 'days_storepickup';
    const MAXIMUM_DELIVERY = 'maximum_delivery';
    const ENABLE_EMERGENCY_STOREPICKUP = 'enable_emergency_storepickup';
    const DAYS_EMERGENCY_STOREPICKUP = 'days_emergency_storepickup';
    const MAXIMUM_EMERGENCY_DELIVERY = 'maximum_emergency_delivery';
    const URBAN_FACTOR = 'urban_factor';
    const RURAL_FACTOR = 'rural_factor';
    const COMUNA_STOREPICKUP = 'comuna_storepickup';
    const SELLER_ID = 'seller_id';
    const DCCP_SELLER_ID = 'dccp_seller_id';
    const WEBSITE_ID = 'website_id';
    const FOOD_BENEFIT_ADMINISTRATION = 'food_benefit_administration';
    const BENEFIT_CARD = 'benefit_card';
    const TIME_MAX_EMERGENCY_DELIVERY = 'time_maximum_emergency_delivery';
    const TIME_MAX_CONTINGENCY_DELIVERY = 'time_maximum_contingency_delivery';
    const TIME_MAX_PREVENTION_DELIVERY = 'time_maximum_prevention_delivery';
    const SHOP_ADDRESS_TAKEOUT = 'shop_address_takeout';
    const EXCLUDED_COMUNES = 'excluded_comunes';
    const MAX_DAYS_PACKAGING_DISPATCH = 'max_days_packing_dispatch';
    const MAX_DAYS_BULK_DISPATCH = 'max_days_bulk_dispatch';
    const PRODUCTS_AVAILABILTY_TERM_EMERGENCY_HOURS = 'products_availabilty_term_emergency_hours';
    const PRODUCTS_REPLACEMENT_TERM_EMERGENCY_DAYS = 'products_replacement_term_emergency_days';
    const PRODUCTS_REPLACEMENT_TERM_PREVENTION_DAYS = 'products_replacement_term_prevention_days';
    const REGIONAL_PERCENTAGE = 'regional_percentage';

    protected $_permanentAttributes = [self::REGION_ID];
    protected $needColumnCheck = true;
    protected $validColumnNames = [
        self::REGION_ID,
        self::CONTACT_NAME,
        self::CONTACT_PHONE,
        self::CONTACT_EMAIL,
        self::MAXIMUM_AMOUNT,
        self::MAXIMUM_QTY,
        self::ENABLE_STOREPICKUP,
        self::DAYS_STOREPICKUP,
        self::MAXIMUM_DELIVERY,
        self::ENABLE_EMERGENCY_STOREPICKUP,
        self::DAYS_EMERGENCY_STOREPICKUP,
        self::SELLER_ID,
        self::DCCP_SELLER_ID,
        self::WEBSITE_ID,
        self::REGIONAL_PERCENTAGE,
        self::MAXIMUM_EMERGENCY_DELIVERY,
        self::TIME_MAX_EMERGENCY_DELIVERY,
        self::TIME_MAX_CONTINGENCY_DELIVERY,
        self::TIME_MAX_PREVENTION_DELIVERY,
        self::URBAN_FACTOR,
        self::RURAL_FACTOR,
        self::COMUNA_STOREPICKUP,
        self::FOOD_BENEFIT_ADMINISTRATION,
        self::BENEFIT_CARD,
        self::EXCLUDED_COMUNES,
        self::SHOP_ADDRESS_TAKEOUT,
		self::MAX_DAYS_PACKAGING_DISPATCH,
		self::MAX_DAYS_BULK_DISPATCH,
        self::SHOP_ADDRESS_TAKEOUT,
        self::PRODUCTS_AVAILABILTY_TERM_EMERGENCY_HOURS,
        self::PRODUCTS_REPLACEMENT_TERM_EMERGENCY_DAYS,
        self::PRODUCTS_REPLACEMENT_TERM_PREVENTION_DAYS
    ];
    #
    protected $_regionalConditionTb = [
        self::REGION_ID,
        self::CONTACT_NAME,
        self::CONTACT_PHONE,
        self::CONTACT_EMAIL,
        self::MAXIMUM_AMOUNT,
        self::MAXIMUM_QTY,
        self::ENABLE_STOREPICKUP,
        self::DAYS_STOREPICKUP,
        self::MAXIMUM_DELIVERY,
        self::ENABLE_EMERGENCY_STOREPICKUP,
        self::DAYS_EMERGENCY_STOREPICKUP,
        self::MAXIMUM_EMERGENCY_DELIVERY,
        self::TIME_MAX_EMERGENCY_DELIVERY,
        self::TIME_MAX_CONTINGENCY_DELIVERY,
        self::TIME_MAX_PREVENTION_DELIVERY,
        self::URBAN_FACTOR,
        self::RURAL_FACTOR,
        self::COMUNA_STOREPICKUP,
        self::SELLER_ID,
        self::FOOD_BENEFIT_ADMINISTRATION,
        self::BENEFIT_CARD,
        self::SHOP_ADDRESS_TAKEOUT,
        self::EXCLUDED_COMUNES,
		self::MAX_DAYS_PACKAGING_DISPATCH,
		self::MAX_DAYS_BULK_DISPATCH,
        self::EXCLUDED_COMUNES,
        self::PRODUCTS_AVAILABILTY_TERM_EMERGENCY_HOURS,
        self::PRODUCTS_REPLACEMENT_TERM_EMERGENCY_DAYS,
        self::PRODUCTS_REPLACEMENT_TERM_PREVENTION_DAYS
    ];
    protected $_regionalConditionSellerTb = [
        self::SELLER_ID,
        self::DCCP_SELLER_ID,
        self::WEBSITE_ID,
        self::REGIONAL_PERCENTAGE,
    ];
    #
    protected $masterAttributeCode = self::REGION_ID;
    protected $logInHistory = true;
    protected $_validators = [];
    protected $_resource;
    protected $_helperTodo;
    private $serializer;
    protected $_bunchSize;
    protected $string;
    protected $_regionalConditionFactory;
    protected $_regionalConditionSellerFactory;
    protected $_uniqueAttributes = [];
    private AdapterInterface $dbConnection;

    /**
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Cf\RegionalConditionMass\Helper\Todo $todoHelper,
        \Cf\RegionalConditionMass\Model\RegionConditionFactory $regionalConditionFactory,
        \Cf\RegionalConditionMass\Model\RegionConditionSellerFactory $regionalConditionSellerFactory
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_helperTodo = $todoHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->_resource = $resource;
        $this->dbConnection = $this->_resource->getConnection();
        $this->string = $string;
        $this->_regionalConditionFactory = $regionalConditionFactory;
        $this->_regionalConditionSellerFactory = $regionalConditionSellerFactory;
        $this->errorAggregator = $errorAggregator;
    }


    public function getValidColumnNames()
    {
        return $this->validColumnNames;
    }

    /**
     * Entity type code getter.
     *
     * @return strings
     */
    public function getEntityTypeCode()
    {
        return 'regional_conditions';
    }

    /**
     * Validate data
     *
     * @return ProcessingErrorAggregatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validateData()
    {
        if (!$this->_dataValidated) {
            $this->getErrorAggregator()->clear();
            $absentColumns = array_diff($this->_permanentAttributes, $this->getSource()->getColNames());
            $this->addErrors('Column code does not exist: ' . json_encode($absentColumns), $absentColumns);
            $columnNumber = 0;
            $emptyHeaderColumns = [];
            $invalidColumns = [];
            $invalidAttributes = [];
            foreach ($this->getSource()->getColNames() as $columnName) {
                $columnNumber++;
                if (!$this->isAttributeParticular($columnName)) {
                    if (trim($columnName) == '') {
                        $emptyHeaderColumns[] = $columnNumber;
                    } elseif (!preg_match('/^[a-z][a-z0-9_]*$/', $columnName)) {
                        $invalidColumns[] = $columnName;
                    } elseif ($this->needColumnCheck && !in_array($columnName, $this->getValidColumnNames())) {
                        /*} elseif ($this->needColumnCheck && !in_array($columnName, $this->_helperTodo->_getValidColumnNames())) {*/
                        $invalidAttributes[] = $columnName;
                    }
                }
            }
            $this->addErrors('Invalid Attribute : ' . json_encode($invalidAttributes), $invalidAttributes);
            $this->addErrors('Empty column of the header: ' . json_encode($emptyHeaderColumns), $emptyHeaderColumns);
            $this->addErrors('Invalid Column Name: ' . json_encode($invalidColumns), $invalidColumns);

            if (!$this->getErrorAggregator()->getErrorsCount()) {
                $this->_saveValidatedBunches();
                $this->_dataValidated = true;
            }
        }
        return $this->getErrorAggregator();
    }


    /**
     * Add error with corresponding current data source row number.
     *
     * @param string $errorCode Error code or simply column name
     * @param int $errorRowNum Row number.
     * @param string $colName OPTIONAL Column name.
     * @param string $errorMessage OPTIONAL Column name.
     * @param string $errorLevel
     * @param string $errorDescription
     * @return $this
     */
    public function addRowError(
        $errorCode = null,
        $errorRowNum = null,
        $colName = null,
        $errorMessage = null,
        $errorLevel = ProcessingError::ERROR_LEVEL_CRITICAL,
        $errorDescription = null
    ) {
        $errorCode = (string)$errorCode;
        $this->getErrorAggregator()->addError(
            $errorCode,
            $errorLevel,
            $errorRowNum,
            $colName,
            $errorMessage,
            $errorDescription
        );

        return $this;
    }


    /**
     * Validate data rows and save bunches to DB
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _saveValidatedBunches()
    {
        $source = $this->getSource();
        $bunchRows = [];
        $startNewBunch = false;
        $source->rewind();
        $this->_dataSourceModel->cleanBunches();
        $masterAttributeCode = $this->getMasterAttributeCode();
        //$_options = $this->_helperTodo->_getOptionsAttributes();
        $rowData = [];
        while ($source->valid() || count($bunchRows) || isset($entityGroup)) {
            if ($startNewBunch || !$source->valid()) {
                /* If the end approached add last validated entity group to the bunch */
                if (!$source->valid() && isset($entityGroup)) {
                    foreach ($entityGroup as $key => $value) {
                        $bunchRows[$key] = $value;
                    }
                    unset($entityGroup);
                }

                $this->_dataSourceModel->saveBunch($this->getEntityTypeCode(), $this->getBehavior(), $bunchRows);

                $bunchRows = [];
                $startNewBunch = false;
            }



            if ($source->valid()) {
                $valid = true;
                try {
                    $rowData = $source->current();
                    foreach ($rowData as $attrName => $element) {
                        if (!mb_check_encoding($element, 'UTF-8')) {
                            $valid = false;
                            $this->addRowError(
                                'Invalid characters.',
                                $this->_processedRowsCount,
                                $attrName
                            );
                        }

                        /*add validation custom*/
                        /*if(isset($_options[$attrName])){
                           $this->isAttributeValid($attrName, $_options[$attrName], $rowData, $this->_processedRowsCount);
                        }*/

                    }
                } catch (\InvalidArgumentException $e) {
                    $valid = false;
                    $this->addRowError($e->getMessage(), $this->_processedRowsCount);
                }

                if (!$valid) {
                    $this->_processedRowsCount++;
                    $source->next();
                    continue;
                }

                if (isset($rowData[$masterAttributeCode]) && trim($rowData[$masterAttributeCode])) {
                    /* Add entity group that passed validation to bunch */
                    if (isset($entityGroup)) {
                        foreach ($entityGroup as $key => $value) {
                            $bunchRows[$key] = $value;
                        }
                        $productDataSize = strlen($this->getSerializer()->serialize($bunchRows));

                        /* Check if the new bunch should be started */
                        /*$isBunchSizeExceeded = ($this->_bunchSize > 0 && count($bunchRows) >= $this->_bunchSize);
                        $startNewBunch = $productDataSize >= $this->_maxDataSize || $isBunchSizeExceeded;*/
                    }

                    /* And start a new one */
                    $entityGroup = [];
                }

                if (isset($entityGroup) && $this->validateRow($rowData, $source->key())) {
                    /* Add row to entity group */
                    $entityGroup[$source->key()] = $this->_prepareRowForDb($rowData);
                } elseif (isset($entityGroup)) {
                    /* In case validation of one line of the group fails kill the entire group */
                    unset($entityGroup);
                }

                $this->_processedRowsCount++;
                $source->next();
            }
        }


        return $this;
    }

    /**
     * Get Serializer instance
     *
     * Workaround. Only way to implement dependency and not to break inherited child classes
     *
     * @return Json
     * @deprecated 100.2.0
     */
    private function getSerializer()
    {
        if (null === $this->serializer) {
            $this->serializer = ObjectManager::getInstance()->get(Json::class);
        }
        return $this->serializer;
    }

    /**
     * @return string the master attribute code to use in an import
     */
    public function getMasterAttributeCode()
    {
        return $this->masterAttributeCode;
    }


    /**
     * Change row data before saving in DB table
     *
     * @param array $rowData
     * @return array
     */
    protected function _prepareRowForDb(array $rowData)
    {
        /**
         * Convert all empty strings to null values, as
         * a) we don't use empty string in DB
         * b) empty strings instead of numeric values will product errors in Sql Server
         */
        foreach ($rowData as $key => $val) {
            if ($val === '') {
                $rowData[$key] = null;
            }
        }
        return $rowData;
    }


    /**
     * Row validation.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum)
    {
        $title = false;
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;

        if (!isset($rowData[self::REGION_ID]) || empty($rowData[self::REGION_ID])) {
            $this->addRowError('Is empty.', $rowNum);
            return false;
        }
        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }



    /**
     * Create advanced question data from raw data.
     *
     * @throws Exception
     * @return bool Result of operation.
     */
    protected function _importData()
    {
        if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            //$this->deleteEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            $this->replaceEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $this->getBehavior()) {
            $this->saveEntity();
        }
        return true;
    }



    public function saveEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }

    public function replaceEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }

    public function deleteEntity()
    {
        /*$ids = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);
                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowId = $rowData[self::REGION_ID];
                    $ids[] = $rowId;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }
        if ($ids) {
            $this->deleteEntityFinish(array_unique($ids),self::TABLE_ENTITY);
        }
        return $this;*/
    }

    /**
     * Save and replace question
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $ids = [];
        //$_headers = $this->_helperTodo->_getValidColumnNames();
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError('Is empty.', $rowNum);
                    continue;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }

                if (array_key_exists("excluded_comunes", $rowData)) {
                    $excludedComunes = $rowData[self::EXCLUDED_COMUNES];
                } else {
                    $excludedComunes = '';
                }

                if (array_key_exists("shop_address_takeout", $rowData)) {
                    $shopAddressTakeout = $rowData[self::SHOP_ADDRESS_TAKEOUT];
                } else {
                    $shopAddressTakeout = '';
                }

                $rowTtile= $rowData[self::REGION_ID];
                $listTitle[] = $rowTtile;
                $entityList[$rowTtile][] =
                [
                    self::REGION_ID => $rowData[self::REGION_ID],
                    self::CONTACT_NAME => $rowData[self::CONTACT_NAME],
                    self::CONTACT_PHONE => $rowData[self::CONTACT_PHONE],
                    self::CONTACT_EMAIL => $rowData[self::CONTACT_EMAIL],
                    self::MAXIMUM_AMOUNT => $this->columnValidOnArray($rowData, self::MAXIMUM_AMOUNT, 0),
                    self::MAXIMUM_QTY => $this->columnValidOnArray($rowData, self::MAXIMUM_QTY, 0),
                    self::ENABLE_STOREPICKUP => $this->columnValidOnArray($rowData,self::ENABLE_STOREPICKUP, 0),
                    self::DAYS_STOREPICKUP => $this->columnValidOnArray($rowData, self::DAYS_STOREPICKUP, 0),
                    self::MAXIMUM_DELIVERY => $rowData[self::MAXIMUM_DELIVERY],
                    self::ENABLE_EMERGENCY_STOREPICKUP => $this->columnValidOnArray($rowData, self::ENABLE_EMERGENCY_STOREPICKUP, 0),
                    self::DAYS_EMERGENCY_STOREPICKUP => $this->columnValidOnArray($rowData, self::DAYS_EMERGENCY_STOREPICKUP, 0),
                    self::MAXIMUM_EMERGENCY_DELIVERY => $this->columnValidOnArray($rowData, self::MAXIMUM_EMERGENCY_DELIVERY, 0),
                    self::EXCLUDED_COMUNES => $excludedComunes,

                    self::TIME_MAX_EMERGENCY_DELIVERY => $this->columnValidOnArray($rowData, self::TIME_MAX_EMERGENCY_DELIVERY, 0),
                    self::TIME_MAX_CONTINGENCY_DELIVERY => $this->columnValidOnArray($rowData, self::TIME_MAX_CONTINGENCY_DELIVERY, 0),
                    self::TIME_MAX_PREVENTION_DELIVERY => $this->columnValidOnArray($rowData, self::TIME_MAX_PREVENTION_DELIVERY, 0),

                    self::URBAN_FACTOR => $this->columnValidOnArray($rowData, self::URBAN_FACTOR, NULL),
                    self::RURAL_FACTOR => $this->columnValidOnArray($rowData, self::RURAL_FACTOR, NULL),
                    self::COMUNA_STOREPICKUP => $this->columnValidOnArray($rowData, self::COMUNA_STOREPICKUP, NULL),
                    self::FOOD_BENEFIT_ADMINISTRATION => $this->columnValidOnArray($rowData, self::FOOD_BENEFIT_ADMINISTRATION, 0),
                    self::BENEFIT_CARD => $this->columnValidOnArray($rowData, self::BENEFIT_CARD, 0),
                    self::SELLER_ID => $rowData[self::SELLER_ID],
                    self::WEBSITE_ID => $rowData[self::WEBSITE_ID],
                    self::SHOP_ADDRESS_TAKEOUT => $shopAddressTakeout,

                    self::PRODUCTS_AVAILABILTY_TERM_EMERGENCY_HOURS => $rowData[self::PRODUCTS_AVAILABILTY_TERM_EMERGENCY_HOURS],
                    self::PRODUCTS_REPLACEMENT_TERM_EMERGENCY_DAYS => $rowData[self::PRODUCTS_REPLACEMENT_TERM_EMERGENCY_DAYS],
                    self::PRODUCTS_REPLACEMENT_TERM_PREVENTION_DAYS => $rowData[self::PRODUCTS_REPLACEMENT_TERM_PREVENTION_DAYS],

                ];

                $rowTtile = $rowData[self::REGION_ID];
                $listTitle[] = $rowTtile;
                $entityList[$rowTtile][] =
                    [
                        self::REGION_ID => $rowData[self::REGION_ID],
                        self::CONTACT_NAME => $rowData[self::CONTACT_NAME],
                        self::CONTACT_PHONE => $rowData[self::CONTACT_PHONE],
                        self::CONTACT_EMAIL => $rowData[self::CONTACT_EMAIL],
                        self::MAXIMUM_AMOUNT => $this->columnValidOnArray($rowData, self::MAXIMUM_AMOUNT, 0),
                        self::MAXIMUM_QTY => $this->columnValidOnArray($rowData, self::MAXIMUM_QTY, 0),
                        self::ENABLE_STOREPICKUP => $this->columnValidOnArray($rowData, self::ENABLE_STOREPICKUP, 0),
                        self::DAYS_STOREPICKUP => $this->columnValidOnArray($rowData, self::DAYS_STOREPICKUP, 0),
                        self::MAXIMUM_DELIVERY => $rowData[self::MAXIMUM_DELIVERY],
                        self::ENABLE_EMERGENCY_STOREPICKUP => $this->columnValidOnArray($rowData, self::ENABLE_EMERGENCY_STOREPICKUP, 0),
                        self::DAYS_EMERGENCY_STOREPICKUP => $this->columnValidOnArray($rowData, self::DAYS_EMERGENCY_STOREPICKUP, 0),
                        self::MAXIMUM_EMERGENCY_DELIVERY => $this->columnValidOnArray($rowData, self::MAXIMUM_EMERGENCY_DELIVERY, 0),
                        self::EXCLUDED_COMUNES => $this->columnValidOnArray($rowData, self::EXCLUDED_COMUNES, 0),

                        self::TIME_MAX_EMERGENCY_DELIVERY => $this->columnValidOnArray($rowData, self::TIME_MAX_EMERGENCY_DELIVERY, 0),
                        self::TIME_MAX_CONTINGENCY_DELIVERY => $this->columnValidOnArray($rowData, self::TIME_MAX_CONTINGENCY_DELIVERY, 0),
                        self::TIME_MAX_PREVENTION_DELIVERY => $this->columnValidOnArray($rowData, self::TIME_MAX_PREVENTION_DELIVERY, 0),

                    self::URBAN_FACTOR => $this->columnValidOnArray($rowData, self::URBAN_FACTOR, NULL),
                    self::RURAL_FACTOR => $this->columnValidOnArray($rowData, self::RURAL_FACTOR, NULL),
                    self::COMUNA_STOREPICKUP => $this->columnValidOnArray($rowData, self::COMUNA_STOREPICKUP, NULL),
                    self::FOOD_BENEFIT_ADMINISTRATION => $this->columnValidOnArray($rowData, self::FOOD_BENEFIT_ADMINISTRATION, 0),
                    self::BENEFIT_CARD => $this->columnValidOnArray($rowData, self::BENEFIT_CARD, 0),
                    self::SELLER_ID => $rowData[self::SELLER_ID],
                    self::WEBSITE_ID => $rowData[self::WEBSITE_ID],
                    self::SHOP_ADDRESS_TAKEOUT => $this->columnValidOnArray($rowData, self::SHOP_ADDRESS_TAKEOUT, 0),

                        self::PRODUCTS_AVAILABILTY_TERM_EMERGENCY_HOURS => $rowData[self::PRODUCTS_AVAILABILTY_TERM_EMERGENCY_HOURS],
                        self::PRODUCTS_REPLACEMENT_TERM_EMERGENCY_DAYS => $rowData[self::PRODUCTS_REPLACEMENT_TERM_EMERGENCY_DAYS],
                        self::PRODUCTS_REPLACEMENT_TERM_PREVENTION_DAYS => $rowData[self::PRODUCTS_REPLACEMENT_TERM_PREVENTION_DAYS],

                        self::MAX_DAYS_PACKAGING_DISPATCH => $this->columnValidOnArray($rowData, self::MAX_DAYS_PACKAGING_DISPATCH, 0),
                        self::MAX_DAYS_BULK_DISPATCH => $this->columnValidOnArray($rowData, self::MAX_DAYS_BULK_DISPATCH, 0),

                        self::REGIONAL_PERCENTAGE => $this->columnValidOnArray($rowData, self::REGIONAL_PERCENTAGE, 0.00),
                    ];
            }

            if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                if ($ids) {
                    //if ($this->deleteEntityFinish(array_unique($ids), self::TABLE_ENTITY)) {
                    //$this->saveEntityFinish($entityList);
                    //}
                }
            } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                $this->saveEntityFinish($entityList);
            }

        }
        return $this;
    }

    /**
     * Optional columns values
     *
     * @param array $rowData
     * @param string $key
     * @param integer|null $fallback
     * @return $this or $fallback
     */
    private function columnValidOnArray($rowData, $key, $fallback)
    {
        return array_key_exists($key, $rowData) ? $rowData[$key] : $fallback;
    }

    /**
     * Save question
     *
     * @param array $priceData
     * @param string $table
     * @return $this
     */
    protected function saveEntityFinish(array $entityData)
    {
        $this->dbConnection->beginTransaction();
        if ($entityData) {
            foreach ($entityData as $row) {
                foreach ($row as $key => $item) {
                    list($_d_region_cond, $_seller_email) = $this->_helperTodo->_prepareDataRegionalCond($item, $this->_regionalConditionTb);
                    list($_d_region_cond_seller, $_seller_email_cond) = $this->_helperTodo->_prepareDataRegionalCond($item, $this->_regionalConditionSellerTb);
                    $_d_region_cond_seller['dccp_seller_id'] = $this->_helperTodo->_getUserRestId($_seller_email);
                    //Prepare value for maximum_amount - if UNLIMITED_TEXT value is UNLIMITED_VALUE
                    $_d_region_cond['maximum_amount'] = !in_array(strtolower(trim($_d_region_cond['maximum_amount'])), \Formax\RegionalCondition\Helper\Data::UNLIMITED_TEXT)
                        ? (int)$_d_region_cond['maximum_amount'] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;
                    //Prepare value for maximum_qty - if UNLIMITED_TEXT value is UNLIMITED_VALUE
                    $_d_region_cond['maximum_qty'] = !in_array(strtolower(trim($_d_region_cond['maximum_qty'])), \Formax\RegionalCondition\Helper\Data::UNLIMITED_TEXT)
                        ? (int)$_d_region_cond['maximum_qty'] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;
                    //Prepare value for maximum_delivery - if UNLIMITED_TEXT value is UNLIMITED_VALUE
                    $_d_region_cond['maximum_delivery'] = !in_array(strtolower(trim($_d_region_cond['maximum_delivery'])), \Formax\RegionalCondition\Helper\Data::UNLIMITED_TEXT)
                        ? (int)$_d_region_cond['maximum_delivery'] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_TEXT;

                    //Prepare value for maximum_amount - if UNLIMITED_TEXT value is UNLIMITED_VALUE
                    $_d_region_cond['maximum_emergency_delivery'] = !in_array(strtolower(trim($_d_region_cond['maximum_emergency_delivery'])), \Formax\RegionalCondition\Helper\Data::UNLIMITED_TEXT)
                        ? (int)$_d_region_cond['maximum_emergency_delivery'] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;

                    // Prepare value for maximum_amount - if UNLIMITED_TEXT value is UNLIMITED_VALUE - Emergencia Summa
                    $_d_region_cond['time_maximum_emergency_delivery'] = !in_array(strtolower(trim($_d_region_cond['time_maximum_emergency_delivery'])), \Formax\RegionalCondition\Helper\Data::UNLIMITED_TEXT)
                        ? (int)$_d_region_cond['time_maximum_emergency_delivery'] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;

                    // Prepare value for maximum_amount - if UNLIMITED_TEXT value is UNLIMITED_VALUE - Emergencia Summa
                    $_d_region_cond['time_maximum_contingency_delivery'] = !in_array(strtolower(trim($_d_region_cond['time_maximum_contingency_delivery'])), \Formax\RegionalCondition\Helper\Data::UNLIMITED_TEXT)
                        ? (int)$_d_region_cond['time_maximum_contingency_delivery'] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;

                    // Prepare value for maximum_amount - if UNLIMITED_TEXT value is UNLIMITED_VALUE - Emergencia Summa
                    $_d_region_cond['time_maximum_prevention_delivery'] = !in_array(strtolower(trim($_d_region_cond['time_maximum_prevention_delivery'])), \Formax\RegionalCondition\Helper\Data::UNLIMITED_TEXT)
                        ? (int)$_d_region_cond['time_maximum_prevention_delivery'] : \Formax\RegionalCondition\Helper\Data::UNLIMITED_VALUE;

                    $_d_region_cond['excluded_comunes'] = !in_array(strtolower(trim($_d_region_cond['excluded_comunes'])), \Formax\RegionalCondition\Helper\Data::UNLIMITED_TEXT)
                        ? $_d_region_cond['excluded_comunes'] : '';

                    //var_dump($this->_helperTodo->_getUserRestId($_d_region_cond['contact_email']));
                    $_validation = $this->_helperTodo->_getRegionalCond($_d_region_cond['region_id'], $_d_region_cond['seller_id']);
                    if (!$_validation) {
                        $_regionalConditionId = $this->_saveRegionCondition($_d_region_cond, $key);
                        $this->_saveRegionConditionSeller($_d_region_cond_seller, $_regionalConditionId, $key);
                    } else {
                        $this->_updateRegionCondition($_validation['region_cond'], $_d_region_cond, $key);
                        $this->_updateRegionConditionSeller($_validation['region_cond_seller'], $_d_region_cond_seller, $key);
                    }
                    //Preparing data to save into table dccp_storepickup
                    $query2 = 'INSERT INTO  ' . $this->_resource->getTableName('dccp_storepickup')
                        . '(dest_region_id, seller_id, website_id, active) VALUES '
                        . '(:region, :seller, :website, :active) ON DUPLICATE KEY UPDATE active = :active';
                    $bind2 = [
                        'region' => (int)$_d_region_cond['region_id'],
                        'seller' => $_d_region_cond['seller_id'],
                        'website' => $_d_region_cond_seller['website_id'],
                        'active' => (int)$_d_region_cond['enable_storepickup']
                    ];
                    $this->dbConnection->query($query2, $bind2);

                }
            }
        }
        $this->dbConnection->commit();
        return $this;
    }

    protected function _saveRegionCondition($data, $key)
    {
        try {
            $model = $this->_regionalConditionFactory->create();
            $model->setData($data);
            $model->save();
            return $model->getId();
        } catch (Exception $e) {
            $this->addRowError($e->getMessage(), $key, 'seller_id');
        }
    }

    protected function _saveRegionConditionSeller($data, $regionCondId, $key)
    {
        try {
            $data['regional_condition_id'] = $regionCondId;
            $model = $this->_regionalConditionSellerFactory->create();
            $model->setData($data);
            $model->save();
            return $model->getId();
        } catch (Exception $e) {
            $this->addRowError($e->getMessage(), $key, 'seller_id');
        }
    }

    protected function _updateRegionCondition($id, $data, $key)
    {
        try {
            $model = $this->_regionalConditionFactory->create();
            $_model = $model->load($id);
            $_model->setData($data);
            $_model->setId($id)->save();
            return $_model->getId();
        } catch (Exception $e) {
            $this->addRowError($e->getMessage(), $key, 'seller_id');
        }
    }

    protected function _updateRegionConditionSeller($id, $data, $key)
    {
        try {
            unset($data['seller_id']);
            unset($data['regional_condition_id']);
            unset($data['website_id']);
            $model = $this->_regionalConditionSellerFactory->create();
            $_model = $model->load($id);
            $_model->setData($data);
            $_model->setId($id)->save();
            return $_model->getId();
        } catch (Exception $e) {
            $this->addRowError($e->getMessage(), $key, 'seller_id');
        }
    }

    protected function deleteEntityFinish(array $ids, $table)
    {
        /*if ($table && $ids) {
            try {
                $this->countItemsDeleted += $this->_connection->delete(
                    $this->_connection->getTableName($table),
                    $this->_connection->quoteInto('entity_id IN (?)', $ids)
                );
                return true;
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }*/
    }

    /**
     * Check one attribute. Can be overridden in child.
     *
     * @param string $attrCode Attribute code
     * @param array $attrParams Attribute params
     * @param array $rowData Row data
     * @param int $rowNum
     * @return boolean
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function isAttributeValid($attrCode, array $options, array $rowData, $rowNum)
    {
        switch ($options['type']) {
            case 'varchar':
                $val = $this->string->cleanString($rowData[$attrCode]);
                $valid = $this->string->strlen($val) < self::DB_MAX_VARCHAR_LENGTH;
                break;
            case 'decimal':
                $val = trim($rowData[$attrCode]);
                $valid = (float)$val == $val;
                break;
            case 'select':
            case 'multiselect':
                $valid = isset($options['options'][strtolower($rowData[$attrCode])]);
                break;
            case 'int':
                $val = trim($rowData[$attrCode]);
                $valid = (int)$val == $val;
                break;
            case 'datetime':
                $val = trim($rowData[$attrCode]);
                $valid = strtotime($val) !== false;
                break;
            case 'text':
                $val = $this->string->cleanString($rowData[$attrCode]);
                $valid = $this->string->strlen($val) < self::DB_MAX_TEXT_LENGTH;
                break;
            default:
                $valid = true;
                break;
        }

        if (!$valid) {
            $this->addRowError('Invalid Attribute Code: ' . $attrCode, $rowNum, $attrCode);
        }

        if (
            isset($options['is_required']) &&
            $options['is_required'] == 1 &&
            strlen(trim($rowData[$attrCode])) <= 0
        ) {
            $this->addRowError('The value ' . $attrCode . ' is required.', $rowNum, $attrCode);
        }

        if (
            isset($options['is_unique']) &&
            $options['is_unique'] == 1
        ) {
            if (isset($this->_uniqueAttributes[$attrCode][$rowData[$attrCode]])) {
                $this->addRowError('Duplicate value: ' . $attrCode, $rowNum, $attrCode);
                return false;
            }
            $this->_uniqueAttributes[$attrCode][$rowData[$attrCode]] = true;
        }

        return (bool)$valid;
    }


    /**
     * Returns attributes all values in label-value or value-value pairs form. Labels are lower-cased.
     *
     * @param \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute
     * @param array $indexValAttrs OPTIONAL Additional attributes' codes with index values.
     * @return array
     */
    public function getAttributeOptions(
        \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute,
        $indexValAttrs = []
    ) {
        $options = [];

        if ($attribute->usesSource()) {
            $indexValAttrs = array_merge($indexValAttrs, $this->_indexValueAttributes);
            $index = in_array($attribute->getAttributeCode(), $indexValAttrs) ? 'value' : 'label';
            $attribute->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID);

            try {
                foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                    $value = is_array($option['value']) ? $option['value'] : [$option];
                    foreach ($value as $innerOption) {
                        if (strlen($innerOption['value'])) {
                            $options[strtolower($innerOption[$index])] = $innerOption['value'];
                        }
                    }
                }
            } catch (Exception $e) {
            }
        }
        return $options;
    }

}
