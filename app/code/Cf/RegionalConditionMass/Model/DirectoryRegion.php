<?php 
namespace Cf\RegionalConditionMass\Model;
class DirectoryRegion extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface{
	const CACHE_TAG = 'cf_regional_condition_mass_directory_country_region';
	protected $_cacheTag = 'cf_regional_condition_mass_directory_country_region';
	protected $_eventPrefix = 'cf_regional_condition_mass_directory_country_region';

	public function _construct(){
		$this->_init("Cf\RegionalConditionMass\Model\ResourceModel\DirectoryRegion");
	}
	
	public function getIdentities(){
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues(){
		$values = [];
		return $values;
	}
}
 ?>