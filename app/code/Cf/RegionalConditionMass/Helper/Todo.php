<?php
namespace Cf\RegionalConditionMass\Helper;
class Todo extends \Magento\Framework\App\Helper\AbstractHelper{
    protected $_storeManager;
    protected $_connection;
    protected $_customer;
    protected $_customerFactory;
    protected $_date;
    protected $_customerRepository;
    protected $_customerMapper;
    protected $_directoryRegion;
    protected $_websiteCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Customer\Api\Data\CustomerInterfaceFactory $customer,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Customer\Model\Customer\Mapper $customerMapper,
        \Cf\RegionalConditionMass\Model\DirectoryRegionFactory $directoryRegion,
        \Magento\Store\Model\ResourceModel\Website\CollectionFactory $websiteCollectionFactory){
        $this->_storeManager = $storeManager;
        $this->_customer = $customer;
        $this->_customerFactory = $customerFactory;
        $this->_date = $date;
        $this->_connection = $resourceConnection->getConnection();
        $this->_customerRepository = $customerRepository;
        $this->_customerMapper = $customerMapper;
        $this->_directoryRegion = $directoryRegion;
        $this->_websiteCollectionFactory = $websiteCollectionFactory;
        parent::__construct($context);
    }

    public function _prepareDataRegionalCond($item, $columns){
        $_cols = [];
        $_seller_email = null;
        foreach ($columns as $key1 => $value1) {
            if(isset($item[$value1])){
                if($value1 == 'seller_id'){
                    $_seller_email = $item[$value1];
                    $_cols[$value1] = $this->_getCustomerId($item[$value1]);
                }elseif($value1 == 'website_id'){
                    $_cols[$value1] = $this->getCustomerWebsiteId($item[$value1]);
                }else{
                    $_cols[$value1] = $item[$value1];
                }
                #
            }else{
                /* Bussiness rules */                
                if(($value1 == 'rural_factor') || ($value1 == 'urban_factor')){
                    $_cols[$value1] = NULL;
                }else{
                    $_cols[$value1] = 0;
                }
            }            
        }
        return [$_cols, $_seller_email];
    }

    private function getCustomerWebsiteId($_websiteString){
        return $this->_storeManager->getWebsite($_websiteString)->getWebsiteId();
    }
    private function getCustomerStoreId($_storeString){
        return $this->_storeManager->getStore($_storeString)->getId();
    }

    public function _getCustomerId($email){
        $_customer = $this->_customerRepository->get($email);
        if($_customer){
            return $_customer->getId();
        }
        return false;
    }

    public function _getUserRestId($email){
        $customer = $this->_customerRepository->get($email);
        return $customer->getCustomAttribute('user_rest_id')->getValue();
    }
    public function _getRegionalCond($regionId, $sellerId){
        $_result =  $this->_connection->fetchAll("SELECT rc.id AS region_cond, 
                                                         rcs.id AS region_cond_seller 
                                                  FROM dccp_regional_condition rc 
                                                  INNER JOIN dccp_regional_condition_seller rcs 
                                                  ON rc.id =  rcs.regional_condition_id 
                                                  WHERE rc.region_id = $regionId AND rcs.seller_id = $sellerId");
        if($_result){
            return array('region_cond' => $_result[0]['region_cond'], 
                         'region_cond_seller' => $_result[0]['region_cond_seller']);
        }else{ 
            return false;
        }
    }

    protected function _getRegionId($_code){
        $region = $this->_directoryRegion->create()->getCollection()->addFieldToFilter('region_id', $_code);
        return $region->getData();
    }

    protected function _vldEmail($email){
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          return false;
        }
        return true;
    }

}
?>
