<?php

// FUNCION PARA LOGUEAR
function logData($msg) {
    $logfile = "var/log/mcds52.log";
    $fh = fopen($logfile, 'a') or die("No se puede crear/abrir el archivo para loguear");
    fwrite($fh, date('Y-m-d H:i:s').": ".$msg.PHP_EOL);
    fclose($fh);
}

// FUNCION PARA LOGUEAR ERROR
function logError($msg) {
    $logfile = "var/log/mcds52-error.log";
    $fh = fopen($logfile, 'a') or die("No se puede crear/abrir el archivo para loguear");
    fwrite($fh, date('Y-m-d H:i:s').": ".$msg.PHP_EOL);
    fclose($fh);
}

// LOGUEAMOS INICIO DEL PROCESO
logData("INFO: Iniciando proceso de eliminación de duplicados");

// VALIDAMOS SI ESTAMOS EN EL LUGAR CORRECTO
if(!file_exists("app/etc/env.php")) {
    logError("ERROR: No se encuentra env.php, asegurar que el script esté en la raíz de Magento");
    die();
}

// LEEMOS EL ENV DE MAGENTO PARA SACAR CREDENCIALES
$env = require_once("app/etc/env.php");
if(!isset($env["db"]["connection"]["default"]["host"])) {
    logError("ERROR: No se pudo cargar env.php");
    die();
}

// CONEXION A BASE DE DATOS
function sql($query) {
    global $env;
    //SETEAMOS DEFAULT
    $retorno['estado'] = false;
    $retorno['total'] = 0;
    $retorno['id'] = 0;
    $retorno['data'] = "";
    $retorno['query'] = $query;
    //CONEXION A LA BASE DE DATOS
    $db = new mysqli($env["db"]["connection"]["default"]["host"],$env["db"]["connection"]["default"]["username"],$env["db"]["connection"]["default"]["password"],$env["db"]["connection"]["default"]["dbname"]);
    if($db->connect_errno > 0){
        //SI HAY ERROR, LO RETORNO EN LA DATA
        $retorno['data'] = $db->connect_error;
        logError("ERROR QUERY: ".$db->connect_error." | ".$query,basename(__FILE__),__LINE__);
    } else {
        $db->set_charset("utf8mb4");
        mysqli_begin_transaction($db);
        try {
            $resultado = $db->multi_query($query);
            mysqli_commit($db);
            //NO HAY ERROR, RETORNO LA DATA
            $retorno['estado'] = true;
            if(isset($db->affected_rows)) { $retorno['total'] = $db->affected_rows; }
            if(isset($db->insert_id)) { $retorno['id'] = $db->insert_id; }
            $retorno['data'] = [];
            do {
                if ($result = $db->store_result()) {
                    while ($row = $result->fetch_row()) {
                        array_push($retorno['data'], $row);
                    }
                }
            } while ($db->next_result());
        } catch (mysqli_sql_exception $exception) {
            mysqli_rollback($db);
            //RETORNO EL ERROR DEL QUERY COMO DATA
            $retorno['data'] = $db->errno." - ".$exception;
            logError("ERROR QUERY: ".$db->errno." | ".$query,basename(__FILE__),__LINE__);
        }
        $db->close();
        return $retorno;
    }
}

// VALIDAMOS QUE NOS PODEMOS CONECTAR A LA DB
$dbChk = sql("SELECT `value` FROM `core_config_data` WHERE `path` = 'general/locale/timezone'");
if(!$dbChk['estado']) {
    logError("ERROR: No se pudo conectar a la base de datos");
    die();
}

// CHEQUEO DE SANIDAD, SI EXISTE ALGUNA DE LAS TABLAS TEMPORALES, YA CORRE EL PROCESO Y CERRAMOS ESTE
$concuChk = sql("SHOW TABLES LIKE 'THE_LAST'");
if($concuChk['total'] > 0) {
    logData("INFO: Existen tablas temporales, se cancela el proceso");
    die();
}

// TENEMOS TRES BLOQUES DE QUERIES, EL COMUN DEL COMIENZO, EL DE BORRAR, Y EL DE SALVAR; SE HACEN DOS QUERIES: UNA PARA BORRAR (COMUN+BORRAR) Y OTRA PARA SALVAR (COMUN+SALVAR) / AL FINAL SE ELIMINAN LAS TEMPORALES
$qComun = "
    CREATE TEMPORARY TABLE
        TEMP_PCP_CONTEO
    SELECT
        CONCAT(seller_id, '-', product_id) 'LLAVE',
        COUNT(1) 'CONTEO',
        NOW() AS 'FECHA_PROCESO'
    FROM
        marketplace_assignproduct_items
    GROUP BY
        CONCAT(seller_id, '-', product_id)
    HAVING
        COUNT(1) > 1
    ORDER BY
        (2) DESC;

    CREATE TEMPORARY TABLE
        TEMP_PCP_DETALLE
    SELECT 
        marketplace_assignproduct_items.ID AS 'ID_ASIGNACIÓN',
        CONCAT(marketplace_assignproduct_items.seller_id, '-', marketplace_assignproduct_items.product_id) AS 'LLAVE_ANALISIS',
        marketplace_assignproduct_items.seller_id AS 'ID_MAGENTO_VENDEDOR',
        customer_entity.email AS 'IDENTIFICADOR_MAGENTO',
        store.name AS 'TIENDA',
        marketplace_assignproduct_items.product_id AS 'ID_MAGENTO_PRODUCTO',
        marketplace_assignproduct_items.has_offer,
        marketplace_assignproduct_items.status,
        marketplace_assignproduct_items.qty,
        REPLACE((marketplace_assignproduct_items.price), '.0000', '')  as price,
        marketplace_assignproduct_items.created_at,
        marketplace_assignproduct_items.updated_at,
        marketplace_assignproduct_items.dis_dispersion,
        TEMP_PCP_CONTEO.CONTEO AS 'CONTEO_LLAVES',
        TEMP_PCP_CONTEO.FECHA_PROCESO AS 'FECHA_DETECCION',
        CASE
            WHEN marketplace_assignproduct_items.has_offer = '1' AND marketplace_assignproduct_items.status = '1' AND marketplace_assignproduct_items.qty > '0' AND marketplace_assignproduct_items.dis_dispersion = '0' THEN 'VISIBLE_C_HAS'
            WHEN marketplace_assignproduct_items.has_offer = '0' AND marketplace_assignproduct_items.status = '1' AND marketplace_assignproduct_items.qty > '0' AND marketplace_assignproduct_items.dis_dispersion = '0' THEN 'VISIBLE_S_HAS'
            WHEN marketplace_assignproduct_items.has_offer = '1' AND marketplace_assignproduct_items.status = '1' AND marketplace_assignproduct_items.qty = '0' AND marketplace_assignproduct_items.dis_dispersion = '0' THEN 'SIN_STOCK_C_HAS'
            WHEN marketplace_assignproduct_items.has_offer = '0' AND marketplace_assignproduct_items.status = '1' AND marketplace_assignproduct_items.qty = '0' AND marketplace_assignproduct_items.dis_dispersion = '0' THEN 'SIN_STOCK_S_HAS'
            WHEN marketplace_assignproduct_items.status <> '1' THEN 'NO_VISIBLE'
        ELSE
            'REVISAR'
        END AS
            'VEREDICTO'
    FROM
        marketplace_assignproduct_items
    INNER JOIN
        customer_entity ON marketplace_assignproduct_items.seller_id = customer_entity.entity_id
    INNER JOIN
        TEMP_PCP_CONTEO ON TEMP_PCP_CONTEO.LLAVE = (CONCAT(marketplace_assignproduct_items.seller_id, '-', marketplace_assignproduct_items.product_id))
    INNER JOIN
        store ON store.website_id = customer_entity.website_id;

    CREATE TEMPORARY TABLE
        TEMP_PCP_UNO_NO_ELIMINAR 
    SELECT DISTINCT
        A.LLAVE
    FROM
        TEMP_PCP_CONTEO A
    LEFT JOIN
        TEMP_PCP_DETALLE E ON A.LLAVE = E.LLAVE_ANALISIS AND E.VEREDICTO = 'NO_VISIBLE'
    GROUP BY
        A.LLAVE, A.CONTEO
    HAVING
        (A.CONTEO - (COUNT(E.ID_ASIGNACIÓN))) = '1';

    CREATE TEMPORARY TABLE
        TEMP_PCP_SOLO_ELIMINAR 
    SELECT DISTINCT
        A.LLAVE
    FROM
        TEMP_PCP_CONTEO A
    LEFT JOIN
        TEMP_PCP_DETALLE E ON A.LLAVE = E.LLAVE_ANALISIS AND E.VEREDICTO = 'NO_VISIBLE'
    GROUP BY
        A.LLAVE, A.CONTEO
    HAVING
        (A.CONTEO - (COUNT(E.ID_ASIGNACIÓN))) = '0';

    CREATE TEMPORARY TABLE
        TEMP_PCP_SOLO_ELIMINAR_IDENTICOS 
    SELECT DISTINCT
        LLAVE_ANALISIS
    FROM
        TEMP_PCP_DETALLE
    WHERE
        LLAVE_ANALISIS IN (SELECT * FROM TEMP_PCP_SOLO_ELIMINAR)
    GROUP BY
        LLAVE_ANALISIS, has_offer, status, price, CONTEO_LLAVES, VEREDICTO
    HAVING
        CONTEO_LLAVES = count(1);

    CREATE TEMPORARY TABLE
        TEMP_PCP_SOLO_ELIMINAR_IDENTICOS_MAX_ID
    SELECT
        LLAVE_ANALISIS,
        MAX(ID_ASIGNACIÓN) AS 'MAX_ID'
    FROM
        TEMP_PCP_DETALLE WHERE LLAVE_ANALISIS IN (SELECT * FROM TEMP_PCP_SOLO_ELIMINAR_IDENTICOS)
    GROUP BY
        LLAVE_ANALISIS;

    CREATE TEMPORARY TABLE
        TEMP_PCP_SOLO_ELIMINAR_NO_IDENTICOS
    SELECT DISTINCT 
        LLAVE_ANALISIS
    FROM
        TEMP_PCP_DETALLE WHERE LLAVE_ANALISIS IN (SELECT * FROM TEMP_PCP_SOLO_ELIMINAR)
    GROUP BY
        LLAVE_ANALISIS,
        has_offer,
        status,
        price,
        CONTEO_LLAVES,
        VEREDICTO
    HAVING
        CONTEO_LLAVES <> count(1);

    CREATE TEMPORARY TABLE
        TEMP_PCP_SOLO_ELIMINAR_NO_IDENTICOS_MAX_ID
    SELECT
        LLAVE_ANALISIS,
        MAX(ID_ASIGNACIÓN) AS 'MAX_ID'
    FROM
        TEMP_PCP_DETALLE WHERE LLAVE_ANALISIS IN (SELECT * FROM TEMP_PCP_SOLO_ELIMINAR_NO_IDENTICOS)
    GROUP BY
        LLAVE_ANALISIS;

    CREATE TEMPORARY TABLE
        TEMP_PCP_MULTIPLES_VISIBLES
    SELECT DISTINCT
        A.LLAVE
    FROM
        TEMP_PCP_CONTEO A
    LEFT JOIN
        TEMP_PCP_DETALLE E ON A.LLAVE = E.LLAVE_ANALISIS AND E.VEREDICTO = 'NO_VISIBLE'
    GROUP BY
        A.LLAVE,
        A.CONTEO
    HAVING
        (A.CONTEO - (COUNT(E.ID_ASIGNACIÓN))) NOT IN ('1', '0');

    CREATE TEMPORARY TABLE
        TEMP_PCP_VARIOS_VISIBLES_IDENTICOS
    SELECT DISTINCT
        LLAVE_ANALISIS
    FROM
        TEMP_PCP_DETALLE
    WHERE
        LLAVE_ANALISIS IN (SELECT * FROM TEMP_PCP_MULTIPLES_VISIBLES)
    GROUP BY
        LLAVE_ANALISIS,
        has_offer,
        status,
        price,
        CONTEO_LLAVES,
        VEREDICTO
    HAVING
        CONTEO_LLAVES = count(1);

    CREATE TEMPORARY TABLE
        TEMP_PCP_VARIOS_VISIBLES_IDENTICOS_MAX_ID
    SELECT
        LLAVE_ANALISIS,
        MAX(ID_ASIGNACIÓN) AS 'MAX_ID'
    FROM
        TEMP_PCP_DETALLE WHERE LLAVE_ANALISIS IN (SELECT * FROM TEMP_PCP_VARIOS_VISIBLES_IDENTICOS)
    GROUP BY
        LLAVE_ANALISIS;

    CREATE TEMPORARY TABLE
        TEMP_PCP_VARIOS_VISIBLES_NO_IDENTICOS
    SELECT DISTINCT
        LLAVE_ANALISIS
    FROM
        TEMP_PCP_DETALLE
    WHERE
        LLAVE_ANALISIS IN (SELECT * FROM TEMP_PCP_MULTIPLES_VISIBLES)
    AND
        LLAVE_ANALISIS NOT IN (SELECT * FROM TEMP_PCP_VARIOS_VISIBLES_IDENTICOS);

    CREATE TEMPORARY TABLE
        THE_LAST
    SELECT
        A.LLAVE_ANALISIS,
        B.VEREDICTO,
        max(ID_ASIGNACIÓN) AS 'id_max'
    FROM
        TEMP_PCP_VARIOS_VISIBLES_NO_IDENTICOS A
    INNER JOIN
        TEMP_PCP_DETALLE B on A.LLAVE_ANALISIS = B.LLAVE_ANALISIS
    GROUP BY
        A.LLAVE_ANALISIS, B.VEREDICTO;

    CREATE TEMPORARY TABLE
        THE_LAST_1
    SELECT
        *
    FROM
        THE_LAST
    WHERE
        THE_LAST.VEREDICTO = 'VISIBLE_C_HAS';

    CREATE TEMPORARY TABLE
        THE_LAST_2
    SELECT
        *
    FROM
        THE_LAST
    WHERE
        THE_LAST.VEREDICTO = 'VISIBLE_S_HAS'
    AND
        LLAVE_ANALISIS NOT IN (SELECT LLAVE_ANALISIS FROM THE_LAST_1);

    CREATE TEMPORARY TABLE
        THE_LAST_3
    SELECT
        *
    FROM
        THE_LAST
    WHERE
        THE_LAST.VEREDICTO = 'SIN_STOCK_C_HAS'
    AND
        LLAVE_ANALISIS NOT IN (SELECT LLAVE_ANALISIS FROM THE_LAST_1) 
    AND
        LLAVE_ANALISIS NOT IN (SELECT LLAVE_ANALISIS FROM THE_LAST_2);

    CREATE TEMPORARY TABLE
        THE_LAST_4
    SELECT
        *
    FROM
        THE_LAST
    WHERE
        THE_LAST.VEREDICTO = 'SIN_STOCK_S_HAS' 
    AND
        LLAVE_ANALISIS NOT IN (SELECT LLAVE_ANALISIS FROM THE_LAST_1) 
    AND
        LLAVE_ANALISIS NOT IN (SELECT LLAVE_ANALISIS FROM THE_LAST_2) 
    AND
        LLAVE_ANALISIS NOT IN (SELECT LLAVE_ANALISIS FROM THE_LAST_3);

    CREATE TEMPORARY TABLE
        THE_LAST_5
    SELECT
        *
    FROM
        THE_LAST
    WHERE
        THE_LAST.VEREDICTO = 'NO_VISIBLE'  
    AND
        LLAVE_ANALISIS NOT IN (SELECT LLAVE_ANALISIS FROM THE_LAST_1) 
    AND
        LLAVE_ANALISIS NOT IN (SELECT LLAVE_ANALISIS FROM THE_LAST_2) 
    AND
        LLAVE_ANALISIS NOT IN (SELECT LLAVE_ANALISIS FROM THE_LAST_3)  
    AND
        LLAVE_ANALISIS NOT IN (SELECT LLAVE_ANALISIS FROM THE_LAST_4);


    CREATE TEMPORARY TABLE
        THE_LAST_CONSOLIDADO
    SELECT
        LLAVE_ANALISIS, ID_MAX FROM THE_LAST_1 
    UNION SELECT
        LLAVE_ANALISIS, ID_MAX FROM THE_LAST_2 
    UNION SELECT
        LLAVE_ANALISIS, ID_MAX FROM THE_LAST_3 
    UNION SELECT
        LLAVE_ANALISIS, ID_MAX FROM THE_LAST_4 
    UNION SELECT
        LLAVE_ANALISIS, ID_MAX FROM THE_LAST_5;";

$qBorrar = "
    CREATE TEMPORARY TABLE
        PROCESO_BORRAR_1 
    SELECT
        'BORRAR' AS 'DETALLE',
        'UNO VISIBLE EN MAGENTO' AS 'PARTE', 
        TEMP_PCP_DETALLE.*
    FROM
        TEMP_PCP_DETALLE
    WHERE 
        LLAVE_ANALISIS IN (SELECT LLAVE FROM TEMP_PCP_UNO_NO_ELIMINAR)
    AND
        VEREDICTO = 'NO_VISIBLE';

    CREATE TEMPORARY TABLE
        PROCESO_BORRAR_2 
    SELECT
        'BORRAR' AS 'DETALLE',
        'NO VISIBLES IDENTICOS' AS 'PARTE',
        DT.*
    FROM
        TEMP_PCP_DETALLE DT 
    INNER JOIN
        TEMP_PCP_SOLO_ELIMINAR_IDENTICOS_MAX_ID Q ON Q.LLAVE_ANALISIS = DT.LLAVE_ANALISIS
    AND
        DT.ID_ASIGNACIÓN <> Q.MAX_ID;

    CREATE TEMPORARY TABLE
        PROCESO_BORRAR_3
    SELECT
        'BORRAR' AS 'DETALLE',
        'NO VISIBLES NO IDENTICOS' AS 'PARTE',
        DT.*
    FROM
        TEMP_PCP_DETALLE DT
    INNER JOIN
        TEMP_PCP_SOLO_ELIMINAR_NO_IDENTICOS_MAX_ID Q ON Q.LLAVE_ANALISIS = DT.LLAVE_ANALISIS
    AND
        DT.ID_ASIGNACIÓN <> Q.MAX_ID;

    CREATE TEMPORARY TABLE
        PROCESO_BORRAR_4 
    SELECT
        'BORRAR' AS 'DETALLE',
        'VARIOS VISIBLES IDENTICOS' AS 'PARTE',
        DT.*
    FROM
        TEMP_PCP_DETALLE DT
    INNER JOIN
        TEMP_PCP_VARIOS_VISIBLES_IDENTICOS_MAX_ID Q ON Q.LLAVE_ANALISIS = DT.LLAVE_ANALISIS
    AND
        DT.ID_ASIGNACIÓN <> Q.MAX_ID;

    CREATE TEMPORARY TABLE
        PROCESO_BORRAR_5
    SELECT
        'BORRAR' AS 'DETALLE',
        'VARIOS VISIBLES NO IDENTICOS' AS 'PARTE',
        DT.*
    FROM
        TEMP_PCP_DETALLE DT
    INNER JOIN
        THE_LAST_CONSOLIDADO Q ON Q.LLAVE_ANALISIS = DT.LLAVE_ANALISIS
    AND
        DT.ID_ASIGNACIÓN <> Q.ID_MAX;

    SELECT * FROM PROCESO_BORRAR_1
    UNION
    SELECT * FROM PROCESO_BORRAR_2
    UNION
    SELECT * FROM PROCESO_BORRAR_3
    UNION
    SELECT * FROM PROCESO_BORRAR_4
    UNION
    SELECT * FROM PROCESO_BORRAR_5;
";

$qSalvar = "
    CREATE TEMPORARY TABLE
        PROCESO_SALVADO_1
    SELECT
        'SALVADO' AS 'DETALLE',
        'UNO VISIBLE EN MAGENTO' AS 'PARTE',
        TEMP_PCP_DETALLE.*
    FROM
        TEMP_PCP_DETALLE
    WHERE
        LLAVE_ANALISIS IN (SELECT LLAVE FROM TEMP_PCP_UNO_NO_ELIMINAR)
    AND
        VEREDICTO <> 'NO_VISIBLE';

    CREATE TEMPORARY TABLE
        PROCESO_SALVADO_2
    SELECT
        'SALVADO' AS 'DETALLE',
        'NO VISIBLES IDENTICOS' AS 'PARTE',
        DT.*
    FROM
        TEMP_PCP_DETALLE DT
    INNER JOIN
        TEMP_PCP_SOLO_ELIMINAR_IDENTICOS_MAX_ID Q ON Q.LLAVE_ANALISIS = DT.LLAVE_ANALISIS
    AND
        DT.ID_ASIGNACIÓN = Q.MAX_ID;

    CREATE TEMPORARY TABLE
        PROCESO_SALVADO_3
    SELECT
        'SALVADO' AS 'DETALLE',
        'NO VISIBLES NO IDENTICOS' AS 'PARTE',
        DT.*
    FROM
        TEMP_PCP_DETALLE DT
    INNER JOIN
        TEMP_PCP_SOLO_ELIMINAR_NO_IDENTICOS_MAX_ID Q ON Q.LLAVE_ANALISIS = DT.LLAVE_ANALISIS
    AND
        DT.ID_ASIGNACIÓN = Q.MAX_ID;

    CREATE TEMPORARY TABLE
        PROCESO_SALVADO_4
    SELECT
        'SALVADO' AS 'DETALLE',
        'VARIOS VISIBLES IDENTICOS' AS 'PARTE',
        DT.*
    FROM
        TEMP_PCP_DETALLE DT
    INNER JOIN
        TEMP_PCP_VARIOS_VISIBLES_IDENTICOS_MAX_ID Q ON Q.LLAVE_ANALISIS = DT.LLAVE_ANALISIS
    AND
        DT.ID_ASIGNACIÓN = Q.MAX_ID;

    CREATE TEMPORARY TABLE
        PROCESO_SALVADO_5
    SELECT
        'SALVADO' AS 'DETALLE',
        'VARIOS VISIBLES NO IDENTICOS' AS 'PARTE',
        DT.*
    FROM
        TEMP_PCP_DETALLE DT
    INNER JOIN
        THE_LAST_CONSOLIDADO Q ON Q.LLAVE_ANALISIS = DT.LLAVE_ANALISIS
    AND
        DT.ID_ASIGNACIÓN = Q.ID_MAX;

    SELECT * FROM PROCESO_SALVADO_1 
    UNION 
    SELECT * FROM PROCESO_SALVADO_2
    UNION 
    SELECT * FROM PROCESO_SALVADO_3
    UNION 
    SELECT * FROM PROCESO_SALVADO_4
    UNION 
    SELECT * FROM PROCESO_SALVADO_5;
";

$qLimpia = "
    DROP TABLE IF EXISTS TEMP_PCP_CONTEO;
    DROP TABLE IF EXISTS TEMP_PCP_DETALLE;
    DROP TABLE IF EXISTS TEMP_PCP_UNO_NO_ELIMINAR;
    DROP TABLE IF EXISTS TEMP_PCP_SOLO_ELIMINAR;
    DROP TABLE IF EXISTS TEMP_PCP_SOLO_ELIMINAR_IDENTICOS;
    DROP TABLE IF EXISTS TEMP_PCP_SOLO_ELIMINAR_IDENTICOS_MAX_ID;
    DROP TABLE IF EXISTS TEMP_PCP_SOLO_ELIMINAR_NO_IDENTICOS;
    DROP TABLE IF EXISTS TEMP_PCP_SOLO_ELIMINAR_NO_IDENTICOS_MAX_ID;
    DROP TABLE IF EXISTS TEMP_PCP_MULTIPLES_VISIBLES;
    DROP TABLE IF EXISTS TEMP_PCP_VARIOS_VISIBLES_IDENTICOS;
    DROP TABLE IF EXISTS TEMP_PCP_VARIOS_VISIBLES_IDENTICOS_MAX_ID;
    DROP TABLE IF EXISTS TEMP_PCP_VARIOS_VISIBLES_NO_IDENTICOS;
    DROP TABLE IF EXISTS THE_LAST;
    DROP TABLE IF EXISTS THE_LAST_1;
    DROP TABLE IF EXISTS THE_LAST_2;
    DROP TABLE IF EXISTS THE_LAST_3;
    DROP TABLE IF EXISTS THE_LAST_4;
    DROP TABLE IF EXISTS THE_LAST_5;
    DROP TABLE IF EXISTS THE_LAST_CONSOLIDADO;
    DROP TABLE IF EXISTS PROCESO_BORRAR_1;
    DROP TABLE IF EXISTS PROCESO_BORRAR_2;
    DROP TABLE IF EXISTS PROCESO_BORRAR_3;
    DROP TABLE IF EXISTS PROCESO_BORRAR_4;
    DROP TABLE IF EXISTS PROCESO_BORRAR_5;
    DROP TABLE IF EXISTS PROCESO_SALVADO_1;
    DROP TABLE IF EXISTS PROCESO_SALVADO_2;
    DROP TABLE IF EXISTS PROCESO_SALVADO_3;
    DROP TABLE IF EXISTS PROCESO_SALVADO_4;
    DROP TABLE IF EXISTS PROCESO_SALVADO_5;
";

// HACEMOS EL PRIMER QUERY DE BORRAR
$pBorrar = sql($qComun.$qBorrar);

// HACEMOS EL PRIMER QUERY DE SALVAR
$pSalvar = sql($qComun.$qSalvar);

// LIMPIAMOS CUALQUIER TABLA QUE HAYA QUEDADO
$pLimpiar = sql($qLimpia);

// HACEMOS UN ECHO CON LO ENCONTRADO
logData("INFO: Se han encontrado ".$pBorrar['total']." registros a borrar y ".$pSalvar['total']." registros a salvar");

// ARRANCAMOS UN CSV
$csv = '"DETALLE","PARTE","ID_ASIGNACIÓN","LLAVE_ANALISIS","ID_MAGENTO_VENDEDOR","IDENTIFICADOR_MAGENTO","TIENDA","ID_MAGENTO_PRODUCTO","has_offer","status","qty","price","created_at","updated_at","dis_dispersion","CONTEO_LLAVES","FECHA_DETECCION","VEREDICTO","QUERY"'.PHP_EOL;

// PASEAMOS POR LOS REGISTROS A BORRAR
if($pBorrar['total'] > 0) {
    foreach($pBorrar['data'] as $br) {
        $err = "";
        $fBorrar = "DELETE FROM `marketplace_assignproduct_items` WHERE `id` = '".$br[2]."' AND `seller_id` = '".$br[4]."' AND `product_id` = '".$br[7]."'";
        $exec = sql($fBorrar);
        if(!$exec['estado']) { $err = "ERROR - REVISAR LOG: "; }
        $csv .= '"'.$br[0].'","'.$br[1].'","'.$br[2].'","'.$br[3].'","'.$br[4].'","'.$br[5].'","'.$br[6].'","'.$br[7].'","'.$br[8].'","'.$br[9].'","'.$br[10].'","'.$br[11].'","'.$br[12].'","'.$br[13].'","'.$br[14].'","'.$br[15].'","'.$br[16].'","'.$br[17].'","'.$err.$fBorrar.'"'.PHP_EOL;
    }
}

// PASEAMOS POR LOS REGISTROS A SALVAR
if($pSalvar['total'] > 0) {
    foreach($pSalvar['data'] as $sl) {
        $csv .= '"'.$sl[0].'","'.$sl[1].'","'.$sl[2].'","'.$sl[3].'","'.$sl[4].'","'.$sl[5].'","'.$sl[6].'","'.$sl[7].'","'.$sl[8].'","'.$sl[9].'","'.$sl[10].'","'.$sl[11].'","'.$sl[12].'","'.$sl[13].'","'.$sl[14].'","'.$sl[15].'","'.$sl[16].'","'.$sl[17].'","NULO"'.PHP_EOL;
    }
}

// CERRAMOS EL PROCESO
if($pBorrar['total'] == 0 && $pSalvar['total'] == 0) {
    logData("INFO: Fin del proceso de eliminación de duplicados - no se ejecutó ninguna acción.");
} else {
    logData("INFO: Fin del proceso de eliminación de duplicados - sigue el CSV del resultado.".PHP_EOL.$csv.PHP_EOL);
}
